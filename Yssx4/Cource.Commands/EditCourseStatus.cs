﻿using ENode.Commanding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Commands
{
    public class EditCourseStatus: Command<long>
    {
        public long CourseId { get; set; }
    }
}
