﻿using ENode.Commanding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Commands
{
    public class CourseAuditStatusEdit : Command<long>
    {
        public long CourseId { get; set; }

        public int AuditStatus { get; set; }

        /// <summary>
        /// 审核描述
        /// </summary>
        public string Description { get; set; }
    }
}
