﻿using ENode.Commanding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Commands
{
    public class CreateCourse : Command<long>
    {
        public long CourseId { get; set; }
        public string CourseName { get; set; }

        /// <summary>
        /// 特点副标
        /// </summary>
        public string CourseTitle { get; set; }

        /// <summary>
        /// 课程类型
        /// </summary>
        public long CourseTypeId { get; set; }

        /// <summary>
        /// 知识点
        /// </summary>
        public long KnowledgePointId { get; set; }

        /// <summary>
        /// 学历标识
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 出版社
        /// </summary>
        public string PublishingHouse { get; set; }

        /// <summary>
        /// 出版时间
        /// </summary>
        public string PublishingDate { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long TenantId { get; set; }

        public long CreateBy { get; set; }
    }
}
