﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using Yssx.Framework.Auth;
using Yssx.Framework.Utils;
using Yssx.Framework.Redis;

namespace OneAndX.Services
{
    /// <summary>
    /// 登录服务
    /// </summary>
    public class LoginService : ILoginService
    {
        #region 账号密码登陆
        /// <summary>
        /// 账号密码登陆
        /// </summary>
        /// <param name="dto">用户信息</param>
        /// <returns></returns>
        public async Task<ResponseContext<UserTicket>> UserPasswordLogin(UserLoginInDto dto, string loginIp, PermissionRequirement _requirement)
        {
            return await Task.Run(() =>
            {
                var md5 = dto.Password.Md5();
                var user = DbContext.SxFreeSql.Select<SxYssxUser>().Where(u => u.MobilePhone == dto.MobilePhone).First();

                if (user == null) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "该手机号没有注册!" };
                if (user.Status == 0) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户禁用!" };
                if (user.IsDelete == CommonConstants.IsDelete) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "用户删除!" };
                if (user.Password != md5) return new ResponseContext<UserTicket> { Code = CommonConstants.ErrorCode, Msg = "密码不正确!" };

                var school = DbContext.SxFreeSql.Select<SxYssxSchool>().Where(x => x.Id == user.TenantId).First();
                var ticket = user == null ? null : new UserTicket()
                {
                    Photo = user.Photo,
                    Name = System.Web.HttpUtility.UrlDecode(user.NikeName, Encoding.UTF8),
                    RealName = user.RealName,
                    ClientId = user.ClientId,
                    Id = user.Id,
                    TenantId = user.TenantId,
                    Type = (int)user.UserType,
                    MobilePhone = user.MobilePhone,
                    SchoolName = school == null ? "" : school.Name,
                    SchoolType = school == null ? -1 : (int)school.Type,
                    LoginType = 0,
                };

                ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);
                var log = new SxYssxUserLoginLog
                {
                    AccessToken = ticket.AccessToken,
                    Id = IdWorker.NextId(),
                    LoginIp = loginIp,
                    LoginTime = DateTime.Now,
                    UserId = ticket.Id
                };
                DbContext.SxFreeSql.Insert<SxYssxUserLoginLog>(log).ExecuteAffrows();

                if (dto.MobilePhone.Trim() == "15812345678")
                {
                    string fixToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwODAyOTI1MzQ5MTA5NzgiLCJuYW1lIjoiMTExMTEiLCJyZWFsTmFtZSI6IjExMTExIiwi" +
                    "Y2xpZW50SWQiOiIyZDAzOGMzYi1mMDA1LTQ4ZTAtODNhOC1jMmUwMTYxOWJlNzQiLCJ1c2VyVHlwZSI6IjAiLCJ0ZW5hbnRJZCI6IjEwNzg5Nzc5MTE1Mzc2NjUiLCJ0aW1lc3Rhb" +
                    "XAiOiI2MzczMDI2MDI2ODEzNTY3MDQiLCJsb2dpblR5cGUiOiIwIiwic2Nob29sVHlwZSI6IjIiLCJhY2NvdW50IjoiMTU4MTIzNDU2NzgiLCJzY2hvb2xOYW1lIjoiWOivgeS5p" +
                    "uWtpumZoiJ9.LqjPlRvod9JAR8L63BywvSLacLhNeEL49467-bmRWuk";

                    //token写入实训Redis
                    RedisHelperExtension.SxInstance.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-0", fixToken);
                    //token写入行业赛Redis
                    RedisHelperExtension.HysInstance.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-0", fixToken);
                    //token写入竞赛Redis
                    RedisHelperExtension.JsInstance.HSet(CacheKeys.UserLoginTokenHashKey, $"{ticket.Id}", fixToken);

                    ticket.AccessToken = fixToken;
                }
                else
                {
                    //token写入实训Redis
                    RedisHelperExtension.SxInstance.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);
                    //token写入行业赛Redis
                    RedisHelperExtension.HysInstance.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);
                    //token写入竞赛Redis
                    RedisHelperExtension.JsInstance.HSet(CacheKeys.UserLoginTokenHashKey, $"{ticket.Id}", ticket.AccessToken);
                }

                return new ResponseContext<UserTicket> { Code = CommonConstants.SuccessCode, Data = ticket };
            });
        }
        #endregion

        /// <summary>
        /// 登录考试
        /// </summary>
        /// <returns></returns>   
        public async Task<ResponseContext<ExamCurrentBaseInfo>> LoginExam(string examCardNumber, string examPassword, string loginIp, PermissionRequirement _requirement)
        {
            var result = new ResponseContext<ExamCurrentBaseInfo> { Code = CommonConstants.BadRequest, Data = null, Msg = "请求无效" };
            //修改
            var user = await DbContext.FreeSql.GetRepository<SignUp>()
                .Where(m => m.ExamCardNumber == examCardNumber && m.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync();

            if (user == null || user.ExamPassword != examPassword)
            {
                result.Msg = "准考证号或密码输入不正确，请重试！";
                return result;
            }
            //获取考试信息
            var examPlan = await DbContext.FreeSql.GetRepository<ExamPlan>()
                  .Where(m => m.Id == user.ExamPlanId && m.IsDelete == CommonConstants.IsNotDelete)
                  .FirstAsync();

            if (examPlan == null)
            {
                result.Msg = "考试不存在。";
                return result;
            }

            var ticket = user == null ? null : new UserTicket()
            {
                Photo = user.Photo,
                Name = System.Web.HttpUtility.UrlDecode(user.Name, Encoding.UTF8),
                RealName = user.Name,
                ClientId = user.ClientId,
                Id = user.Id,
                TenantId = user.TenantId,
                Type = (int)user.UserType,
                MobilePhone = "",//user.Phone
                SchoolName = "",
                SchoolType = -1,
                LoginType = 0,
            };

            ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);

            //云实训
            var log = new SxYssxUserLoginLog
            {
                AccessToken = ticket.AccessToken,
                Id = IdWorker.NextId(),
                LoginIp = loginIp,
                LoginTime = DateTime.Now,
                UserId = ticket.Id
            };
            //竞赛
            var jslog = new JsSkillUserLoginLog
            {
                AccessToken = ticket.AccessToken,
                Id = IdWorker.NextId(),
                LoginIp = loginIp,
                LoginTime = DateTime.Now,
                UserId = ticket.Id
            };

            DbContext.SxFreeSql.Insert(log).ExecuteAffrows();
            DbContext.JSFreeSql.Insert(jslog).ExecuteAffrows();

            //token写入实训Redis
            RedisHelperExtension.SxInstance.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);
            //token写入行业赛Redis
            //RedisHelperExtension.HysInstance.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);
            //token写入竞赛Redis
            RedisHelperExtension.JsInstance.HSet(CacheKeys.UserLoginTokenHashKey, $"{ticket.Id}", ticket.AccessToken);

            ExamCurrentBaseInfo examInfo = new ExamCurrentBaseInfo
            {
                ExamPlanId = examPlan.Id,
                Ticket = ticket,
                ExamCardNumber = user.ExamCardNumber,
                IDNumber = user.IDNumber,
                Name = user.Name,
                BeginTime = examPlan.BeginTime,
                EndTime = examPlan.EndTime,
                Certificate = examPlan.Certificate,
                CertificateLevel = examPlan.CertificateLevel,
                Status = ExamPlanService.GetExamStatus(examPlan.BeginTime, examPlan.EndTime),
            };

            return new ResponseContext<ExamCurrentBaseInfo> { Code = CommonConstants.SuccessCode, Data = examInfo, Msg = "" };
        }

        /// <summary>
        /// 获取考生准考证信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<ExamStudentBaseInfoDto>> GetLoginExam(string examCardNumber = "")
        {
            //修改
            var users = await DbContext.FreeSql.GetRepository<SignUp>(m => m.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.ExamPlanId == 1083292783706245)
                //.InnerJoin<ExamPlan>((a, b) => a.ExamPlanId == b.Id && b.IsDelete == CommonConstants.IsNotDelete && b.Id == 1083292783706245)
                .InnerJoin<ExamUserGrade>((a, c) => a.Id != c.UserId && c.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrWhiteSpace(examCardNumber), m => m.ExamCardNumber != examCardNumber)
                .ToListAsync<ExamStudentBaseInfoDto>();

            ExamStudentBaseInfoDto user;
            if (users != null && users.Count > 1)
            {
                Random ran = new Random();
                int RandKey = ran.Next(0, users.Count - 1);

                user = users[RandKey];
            }
            else
                user = users[0];

            return new ResponseContext<ExamStudentBaseInfoDto> { Code = CommonConstants.SuccessCode, Data = user, Msg = "" };
        }
    }
}
