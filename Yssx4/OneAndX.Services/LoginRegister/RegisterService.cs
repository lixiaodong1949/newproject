﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using Yssx.Framework.Auth;
using Yssx.Framework.Utils;
using Yssx.Framework.Redis;
using Tas.Common.Configurations;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Core;
using Aliyun.Acs.Dysmsapi.Model.V20170525;

namespace OneAndX.Services
{
    /// <summary>
    /// 注册服务
    /// </summary>
    public class RegisterService : IRegisterService
    {
        #region 验证手机号码是否已注册
        /// <summary>
        /// 验证手机号码是否已注册
        /// </summary>
        /// <param name="mobilePhone">手机号码</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> VerifyPhoneNumber(string mobilePhone)
        {
            if (DbContext.SxFreeSql.Select<SxYssxUser>().Any(x => x.MobilePhone == mobilePhone && x.IsDelete == CommonConstants.IsNotDelete))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码已在云上实训注册!", Data = false };
            }
            if (DbContext.HysFreeSql.Select<HysYssxUser>().Any(x => x.MobilePhone == mobilePhone && x.IsDelete == CommonConstants.IsNotDelete))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码已在行业赛注册!", Data = false };
            }
            if (DbContext.JSFreeSql.Select<JsYssxUser>().Any(x => x.MobilePhone == mobilePhone && x.IsDelete == CommonConstants.IsNotDelete))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码已在技能竞赛注册!", Data = false };
            }

            return await Task.Run(() =>
            {
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
        #endregion

        #region 获取验证码
        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="type">1:找回密码 2:更换手机 其他时候忽略此参数</param>
        /// <returns></returns>
        public static ResponseContext<bool> SendSms(string phone, int? type)
        {
            bool state = false;
            var sxUser = DbContext.SxFreeSql.Select<SxYssxUser>().Where(x => x.MobilePhone == phone).First();

            if (type == 1)
            {
                if (sxUser == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码未注册!", Data = false };
            }

            if (type == 2)
            {
                if (sxUser != null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "该手机号码已存在，请重新输入!", Data = false };
            }

            String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）
            String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
            String accessKeyId = AppSettingConfig.GetSection("accessKeyId") ?? "LTAI5ArzTDAPOll8";//你的accessKeyId，参考本文档步骤2
            String accessKeySecret = AppSettingConfig.GetSection("accessKeySecret") ?? "ZFiPSOKiPPNAE9lydt5GHv4cAt3T7D";//你的accessKeySecret，参考本文档步骤2
            int minutes = 0;
            if (!Int32.TryParse(AppSettingConfig.GetSection("SmsMinutes"), out minutes))
            {
                minutes = 15;
            }

            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            //IAcsClient client = new DefaultAcsClient(profile);
            // SingleSendSmsRequest request = new SingleSendSmsRequest();
            //初始化ascClient,暂时不支持多region（请勿修改）
            bool isNeverExpire = false;
            //DefaultProfile defa = new DefaultProfile("cn-hangzhou").AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain, isNeverExpire);
            DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();
            try
            {
                //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式，发送国际/港澳台消息时，接收号码格式为00+国际区号+号码，如“0085200000000”
                request.PhoneNumbers = phone;
                //必填:短信签名-可在短信控制台中找到
                request.SignName = AppSettingConfig.GetSection("SignName") ?? "云上实训";
                //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
                request.TemplateCode = AppSettingConfig.GetSection("TemplateCode") ?? "SMS_180052960";
                //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                string verifyCode = IdWorker.Number(4, true);
                request.TemplateParam = "{\"code\":\"" + verifyCode + "\"}"; //"{\"name\":\"Tom\",\"code\":\"123\"}";
                //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
                //request.OutId = "yourOutId";
                //请求失败这里会抛ClientException异常
                SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);

                state = sendSmsResponse.Message == "OK";
                if (state)
                {
                    SxYssxVerifyCode entity = new SxYssxVerifyCode { Id = IdWorker.NextId(), Code = verifyCode, MobilePhone = phone, CreateTime = DateTime.Now, ExpirTime = DateTime.Now.AddMinutes(minutes) };
                    DbContext.SxFreeSql.Insert(entity).ExecuteAffrows();
                }
            }
            catch (Exception ex)
            {
                //return ex.Message;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state };
        }
        #endregion

        #region 校验注册验证码
        /// <summary>
        /// 校验注册验证码
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CheckVerificationCode(MobilePhoneVerificationCodeDto dto)
        {
            var code = DbContext.SxFreeSql.Select<SxYssxVerifyCode>().Where(m => m.MobilePhone == dto.MobilePhone).OrderByDescending(x => x.CreateTime).First();
            if (code == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有找到验证码，请确认手机号码是否正确!", Data = false };
            }

            else if (code.Code != dto.VerificationCode)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "验证码错误!", Data = false };
            }
            else if (DateTime.Now > code.ExpirTime)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "验证码过期!", Data = false };
            }
            return await Task.Run(() =>
            {
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "验证成功!", Data = true };
            });
        }
        #endregion

        #region 注册用户
        /// <summary>
        /// 注册用户
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<SxYssxUser>> RegisteredUsers(UserRegisterDto dto)
        {
            var state = false;
            if (dto.Password != dto.AgainPassword)
            {
                return new ResponseContext<SxYssxUser> { Code = CommonConstants.ErrorCode, Msg = "密码不一致!", Data = null };
            }

            long userId = IdWorker.NextId();
            Guid clientId = Guid.NewGuid();

            UserTypeEnums userType = UserTypeEnums.OrdinaryUser;
            if (dto.UserType == 4)
            {
                userType = UserTypeEnums.Specialist;
            }

            #region 初始化用户

            SxYssxUser sxUser = new SxYssxUser
            {
                Id = userId,
                MobilePhone = dto.MobilePhone,
                UserName = dto.Name,
                NikeName = System.Web.HttpUtility.UrlEncode(dto.Name, Encoding.UTF8),
                RealName = dto.Name,
                Password = dto.Password.Md5(),
                UserType = dto.UserType,
                Status = 1,
                UpdateTime = DateTime.Now,
                CreateTime = DateTime.Now,
                ClientId = clientId,
                RegistrationType = 1,
                TenantId = CommonConstants.OneXTenantId,
            };

            HysYssxUser hysUser = new HysYssxUser
            {
                Id = userId,
                MobilePhone = dto.MobilePhone,
                UserName = dto.Name,
                NikeName = System.Web.HttpUtility.UrlEncode(dto.Name, Encoding.UTF8),
                RealName = dto.Name,
                Password = dto.Password.Md5(),
                UserType = dto.UserType,
                Status = 1,
                UpdateTime = DateTime.Now,
                CreateTime = DateTime.Now,
                ClientId = clientId,
                RegistrationType = 1,
                TenantId = CommonConstants.OneXTenantId,
            };

            JsYssxUser jsUser = new JsYssxUser
            {
                Id = userId,
                MobilePhone = dto.MobilePhone,
                UserName = dto.Name,
                NikeName = System.Web.HttpUtility.UrlEncode(dto.Name, Encoding.UTF8),
                RealName = dto.Name,
                Password = dto.Password.Md5(),
                UserType = userType,
                Status = Status.Enable,
                Sex = Sex.None,
                UpdateTime = DateTime.Now,
                CreateTime = DateTime.Now,
                ClientId = clientId,
                RegistrationType = 1,
                TenantId = CommonConstants.OneXTenantId,
            };

            #endregion

            int sxCount = await DbContext.SxFreeSql.Insert(sxUser).ExecuteAffrowsAsync();
            int hysCount = await DbContext.HysFreeSql.Insert(hysUser).ExecuteAffrowsAsync();
            int jsCount = await DbContext.JSFreeSql.Insert(jsUser).ExecuteAffrowsAsync();

            if (sxCount > 0 && hysCount > 0 && jsCount > 0)
                state = true;

            sxUser.NikeName = dto.Name;
            return new ResponseContext<SxYssxUser>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state ? sxUser : null,
                Msg = state ? "注册成功!" : "注册失败!"
            };
        }
        #endregion

    }
}
