﻿using OneAndX.Dto;
using OneAndX.Dto.UserPratice;
using OneAndX.IServices;
using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;

namespace OneAndX.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class UserPracticeService : IUserPracticeService
    {


        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<PraticeCaseListResponseDto>>> GetCaseListPageByCertificate(PraticeCaseQueryRequestDto queryDto)
        {
            long totalCount = 0;
            //按条件查询
            var selectData = await DbContext.FreeSql.GetRepository<ExamCase>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.Certificate == queryDto.Certificate)
                .WhereIf(queryDto.CertificateLevel > 0, e => e.CertificateLevel == queryDto.CertificateLevel)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Name), e => e.TrainingName.Contains(queryDto.Name))
                .Where(e => e.IsCanTraining == true)
                .Count(out totalCount)
                .OrderBy(m => m.TrainingSort)
                .ToListAsync<PraticeCaseListResponseDto>();


            return new ResponseContext<List<PraticeCaseListResponseDto>>(selectData);
        }
        //通关记录新增
        public async Task<ResponseContext<bool>> AddGradeRecord(GradeRecordRequestDto model)
        {
            return await Task.Run(() =>
            {
                var result = new ResponseContext<bool>(true);
                GradeRecord gradeRecord = model.MapTo<GradeRecord>();
                gradeRecord.Id = IdWorker.NextId();
                try
                {
                    DbContext.FreeSql.Insert(gradeRecord).ExecuteAffrows();
                }
                catch (Exception ex)
                {
                    result.Data = false;
                    result.Msg = ex.Message;
                }
                return result;
            });
        }
        /// <summary>
        /// 通关记录查询(自主训练certificate传null)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="caseId">自主训练是根据userid+caseid查询</param>
        /// <param name="certificate">考前自测是根据userid+certificate查询</param>
        /// <returns></returns>
        public async Task<ResponseContext<List<GradeRecordRequestDto>>> GetGradeRecordList(long userId, long caseId, Certificate? certificate, int examType)
        {
            var result = await DbContext.FreeSql.Select<GradeRecord>()
                .Where(c => c.IsDelete == CommonConstants.IsNotDelete)
                .Where(c => c.UserId == userId && c.ExamType == examType)
                .WhereIf(certificate > 0, c => c.Certificate == certificate)
                .WhereIf(caseId > 0, c => c.CaseId == caseId)
                .OrderByDescending(c => c.CreateTime)
                .ToListAsync<GradeRecordRequestDto>(a => new GradeRecordRequestDto
                {
                    CaseId = a.CaseId,
                    CaseName = a.CaseName,
                    CaseYear = a.CaseYear,
                    Certificate = a.Certificate,
                    CertificateLevel = a.CertificateLevel,
                    ExamId = a.ExamId,
                    ExamType = a.ExamType,
                    GradeId = a.GradeId,
                    RollUpType = a.RollUpType,
                    SourceSystem = a.SourceSystem,
                    UserId = a.UserId,
                    TotalScore = a.TotalScore,
                    SubmitTime = a.CreateTime,
                });

            return new ResponseContext<List<GradeRecordRequestDto>>(CommonConstants.SuccessCode, null, result);
        }
        /// <summary>
        /// 获取题目信息
        /// </summary>
        /// <param name="caseSourceSystem"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<QuestionInfoDto>>> GetQuestionInfo(CaseSourceSystem caseSourceSystem, long caseId)
        {
            var questionSql = $"select Id as QuestionId,QuestionType,Title,Content from yssx_topic where caseId={caseId} and ParentId=0 and isdelete=0";
            var questionInfo = new List<QuestionInfoDto>();
            switch (caseSourceSystem)
            {
                case CaseSourceSystem.JS:
                    questionInfo = await DbContext.JSFreeSql.Ado.QueryAsync<QuestionInfoDto>(questionSql);
                    break;
                case CaseSourceSystem.HYS:
                    questionInfo = await DbContext.HysFreeSql.Ado.QueryAsync<QuestionInfoDto>(questionSql);
                    break;
                case CaseSourceSystem.YSSX:
                    questionInfo = await DbContext.SxFreeSql.Ado.QueryAsync<QuestionInfoDto>(questionSql);
                    break;
                default:
                    break;
            }
            return new ResponseContext<List<QuestionInfoDto>>(questionInfo);

        }

        public async Task<ResponseContext<List<AllCaseDto>>> GetAllCaseList(long userId, PraticeCaseQueryDto dto)
        {
            var selectData = await DbContext.FreeSql.GetRepository<ExamCase>()
                .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(dto.Certificate > 0, e => e.Certificate == dto.Certificate)
                .WhereIf(dto.CertificateLevel > 0, e => e.CertificateLevel == dto.CertificateLevel)
                .WhereIf(!string.IsNullOrWhiteSpace(dto.Name), e => e.TrainingName.Contains(dto.Name)).OrderBy(x => x.Certificate).OrderBy(x => x.CertificateLevel).OrderBy(x => x.TrainingSort)
                .ToListAsync();

            //if (dto.Certificate > 0 || dto.CertificateLevel > 0)
            //{
            //    selectData = selectData.OrderBy(x => x.Certificate).ThenBy(x => x.CertificateLevel).ThenBy(x => x.TrainingSort).ToList();
            //}
            //1078977911537665
            string jsSql = $"select c.id CaseId,c.RollUpType,c.CaseYear,p.id ExamId from yssx_case c left join exam_paper p on c.id=p.caseid where p.TenantId=1078977911537665";

            string sql2 = $"select Id GradeId,Status,ExamId from exam_student_grade where TenantId=1078977911537665 and userid={userId}";
            string sql3 = $"select Id GradeId, CASE Status WHEN 'Wait' THEN 0 WHEN 'Started' THEN 1 WHEN 'End' THEN 2 else -1 end Status, ExamId from exam_student_grade where TenantId = 1078977911537665 and userid = {userId}";
            //if (dto.Status > -1)
            //{
            //    sql2 = $"{sql2} and status={dto.Status}";
            //    sql3 = $"{sql3} and status={dto.Status}";
            //}

            List<PraticStatusDto> jsList = await DbContext.JSFreeSql.Ado.QueryAsync<PraticStatusDto>(jsSql);

            // List<PraticStatusDto> hysList = await DbContext.HysFreeSql.Ado.QueryAsync<PraticStatusDto>(jsSql);

            List<PraticStatusDto> sxList = await DbContext.SxFreeSql.Ado.QueryAsync<PraticStatusDto>(jsSql);

            List<PraticStatusDto> gradeJsList = await DbContext.JSFreeSql.Ado.QueryAsync<PraticStatusDto>(sql3);
            List<PraticStatusDto> gradeSxList = await DbContext.SxFreeSql.Ado.QueryAsync<PraticStatusDto>(sql2);

            List<PraticStatusDto> dtoList = new List<PraticStatusDto>();
            List<PraticStatusDto> gradeLList = new List<PraticStatusDto>();

            dtoList.AddRange(jsList);
            //dtoList.AddRange(hysList);
            dtoList.AddRange(sxList);
            gradeLList.AddRange(gradeJsList);
            gradeLList.AddRange(gradeSxList);

            gradeLList.ForEach(x =>
            {
                PraticStatusDto p_dto = dtoList.Where(a => a.ExamId == x.ExamId).FirstOrDefault();
                if (p_dto != null)
                {
                    x.CaseId = p_dto.CaseId;
                }
            });

            List<AllCaseDto> retVal = new List<AllCaseDto>();

            retVal = selectData.Select(x => new AllCaseDto
            {
                Name = x.Name,
                SourceSystem = x.SourceSystem,
                Level = x.Level,
                SourceCaseId = x.SourceCaseId,
                CertificateLevel = x.CertificateLevel,
                Certificate = x.Certificate,
                TrainingName = x.TrainingName,
                Id = x.Id,

            }).ToList();

            retVal.ForEach(x =>
            {
                PraticStatusDto d = dtoList.Where(a => a.CaseId == x.SourceCaseId).FirstOrDefault();
                if (d != null)
                {
                    x.ExamId = d.ExamId;
                    x.RollUpType = d.RollUpType;
                    x.CaseYear = d.CaseYear;
                    x.Year = d.CaseYear;
                    x.SourceCaseId = d.CaseId;
                }
            });


            retVal.ForEach(x =>
            {
                PraticStatusDto d = gradeLList.Where(a => a.CaseId == x.SourceCaseId).OrderByDescending(xb => xb.Status).FirstOrDefault();
                if (d != null)
                {
                    x.GradeId = d.GradeId;
                    x.Status = d.Status;
                }
            });

            if (dto.Status > -1)
            {
                retVal = retVal.FindAll(x => x.Status == dto.Status);
            }



            return new ResponseContext<List<AllCaseDto>> { Data = retVal };
        }

        public async Task<ResponseContext<List<RandomCaseDto>>> GetRandomCaseList(Certificate cf, CertificateLevel cfl)
        {
            var selectData = await DbContext.FreeSql.GetRepository<ExamCase>()
                 .Where(e => e.IsDelete == CommonConstants.IsNotDelete && e.Certificate == cf && e.CertificateLevel == cfl)
                 .ToListAsync();

            string jsSql = $"select c.id CaseId,c.RollUpType,c.CaseYear,p.id ExamId from yssx_case c left join exam_paper p on c.id=p.caseid where p.TenantId=1078977911537665 and ExamType='PracticeTest'";

            string sql = "select c.id CaseId,c.RollUpType,c.CaseYear,p.id ExamId from yssx_case c left join exam_paper p on c.id=p.caseid where p.TenantId=1078977911537665";

            List<PraticStatusDto> jsList = await DbContext.JSFreeSql.Ado.QueryAsync<PraticStatusDto>(jsSql);

            List<PraticStatusDto> sxList = await DbContext.SxFreeSql.Ado.QueryAsync<PraticStatusDto>(sql);

            List<PraticStatusDto> dtoList = new List<PraticStatusDto>();
            dtoList.AddRange(jsList);
            dtoList.AddRange(sxList);

            List<RandomCaseDto> retVal = new List<RandomCaseDto>();

            retVal = selectData.Select(x => new RandomCaseDto
            {
                Name = x.Name,
                SourceSystem = x.SourceSystem,
                CaseId = x.SourceCaseId,

            }).ToList();

            retVal.ForEach(x =>
            {
                PraticStatusDto d = dtoList.Where(a => a.CaseId == x.CaseId).FirstOrDefault();
                if (d != null)
                {
                    x.ExamId = d.ExamId;
                    x.RollUpType = d.RollUpType;
                    x.CaseYear = d.CaseYear;
                }
            });


            return new ResponseContext<List<RandomCaseDto>> { Data = retVal };
        }
    }
}
