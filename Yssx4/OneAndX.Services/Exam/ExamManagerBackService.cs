﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using Yssx.Framework.Auth;
using Yssx.Framework.Utils;
using Yssx.Framework.Redis;

namespace OneAndX.Services
{
    /// <summary>
    /// 考试管理服务(后台)
    /// </summary>
    public class ExamManagerBackService : IExamManagerBackService
    {
        #region 获取某等级证书考场列表
        /// <summary>
        /// 获取某等级证书考场列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CertificateExamRoomViewModel>>> GetCertificateExamRoomList(CertificateExamRoomQuery query)
        {
            ResponseContext<List<CertificateExamRoomViewModel>> response = new ResponseContext<List<CertificateExamRoomViewModel>>();
            if (query == null || !query.CertificateType.HasValue || !query.CertificateLevel.HasValue)
                return response;

            //查看某等级证书考场列表
            var selectData = await DbContext.FreeSql.Select<CertificateDetails>().From<ExaminationSiteDetails, ExaminationSite>(
                (a, b, c) =>
                a.InnerJoin(x => x.ExaminationRoomId == b.Id)
                .InnerJoin(x => b.ExaminationSiteId == c.Id && x.ExaminationSiteId == c.Id))
                .Where((a, b, c) => a.Certificate == (Certificate)query.CertificateType.Value && a.CertificateLevel == (CertificateLevel)query.CertificateLevel.Value
                    && c.IsDelete == CommonConstants.IsNotDelete)
                .OrderByDescending((a, b, c) => c.CreateTime)
                .ToListAsync((a, b, c) => new
                {
                    Province = c.Province,
                    City = c.City,
                    ExamSiteId = c.Id,
                    ExamSiteName = c.ExaminationSiteName,
                    SpecificAddress = c.SpecificAddress,
                    ExamSiteCode = c.ExaminationSiteCode,
                    ExamRoomId = b.Id,
                    ExamRoomName = b.ExaminationRoomName,
                    SeatingCapacity = b.SeatingCapacity
                });

            List<CertificateExamRoomViewModel> proCityDatas = new List<CertificateExamRoomViewModel>();

            //省市列表
            var proCityList = selectData.Select(x => new { x.Province, x.City }).Distinct().ToList();

            proCityList.ForEach(x =>
            {
                CertificateExamRoomViewModel proCityData = new CertificateExamRoomViewModel();
                proCityData.Province = x.Province;
                proCityData.City = x.City;

                //考点列表
                var siteList = selectData.Where(a => a.Province == x.Province && a.City == x.City).Select(a =>
                 new { a.ExamSiteId, a.ExamSiteName, a.SpecificAddress, a.ExamSiteCode }).Distinct().ToList();

                List<CerExamSiteViewModel> siteDatas = new List<CerExamSiteViewModel>();

                siteList.ForEach(y =>
                {
                    CerExamSiteViewModel siteData = new CerExamSiteViewModel();
                    siteData.ExamSiteId = y.ExamSiteId;
                    siteData.ExamSiteName = y.ExamSiteName;
                    siteData.SpecificAddress = y.SpecificAddress;
                    siteData.ExamSiteCode = y.ExamSiteCode;

                    //考场列表
                    var roomList = selectData.Where(a => a.ExamSiteId == y.ExamSiteId).Select(a =>
                      new { a.ExamRoomId, a.ExamRoomName, a.SeatingCapacity }).ToList();

                    List<CerExamRoomViewModel> roomDatas = new List<CerExamRoomViewModel>();

                    roomList.ForEach(z =>
                    {
                        CerExamRoomViewModel roomData = new CerExamRoomViewModel();
                        roomData.ExamRoomId = z.ExamRoomId;
                        roomData.ExamRoomName = z.ExamRoomName;
                        roomData.SeatingCapacity = z.SeatingCapacity;

                        roomDatas.Add(roomData);
                    });

                    siteData.ExamRoomList = roomDatas;

                    siteDatas.Add(siteData);
                });

                proCityData.ExamSiteList = siteDatas;

                proCityDatas.Add(proCityData);
            });

            response.Data = proCityDatas;

            return response;
        }
        #endregion

        #region 分配考试计划考场
        /// <summary>
        /// 分配考试计划考场
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddPlanExamRoom(PlanExamRoomDto dto)
        {
            if (dto == null || dto.ExamRoomList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "请选择考场!" };

            //获取考试计划
            ExamPlan examPlan = await DbContext.FreeSql.GetRepository<ExamPlan>().Where(x => x.Id == dto.PlanId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (examPlan == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该考试计划!" };

            var roomIds = dto.ExamRoomList.Select(x => x.ExamRoomId).ToList();

            var alReadyList = await DbContext.FreeSql.GetRepository<OneXPlanExamRoom>().Where(x => x.ExamPlanId == dto.PlanId && roomIds.Contains(x.ExamRoomId)
                && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (alReadyList.Count > 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "不能分配重复的考场!" };

            bool state = true;

            List<OneXPlanExamRoom> roomDatas = new List<OneXPlanExamRoom>();

            dto.ExamRoomList.ForEach(x =>
            {
                OneXPlanExamRoom room = new OneXPlanExamRoom
                {
                    Id = IdWorker.NextId(),
                    ExamPlanId = dto.PlanId,
                    ExamSiteId = x.ExamSiteId,
                    ExamRoomId = x.ExamRoomId,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                };

                roomDatas.Add(room);
            });

            var resultList = await DbContext.FreeSql.GetRepository<OneXPlanExamRoom>().InsertAsync(roomDatas);
            state = resultList != null;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取已分配考试计划考场
        /// <summary>
        /// 获取已分配考试计划考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<DistributeExamRoomViewModel>>> GetDistributeExamRoomList(long planId)
        {
            ResponseContext<List<DistributeExamRoomViewModel>> response = new ResponseContext<List<DistributeExamRoomViewModel>>();

            //查看考试计划考场列表
            var selectData = await DbContext.FreeSql.Select<OneXPlanExamRoom>().From<ExaminationSiteDetails, ExaminationSite>(
                (a, b, c) =>
                a.InnerJoin(x => x.ExamRoomId == b.Id)
                .InnerJoin(x => x.ExamSiteId == c.Id && b.ExaminationSiteId == c.Id))
                .Where((a, b, c) => a.ExamPlanId == planId && a.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete)
                .OrderByDescending((a, b, c) => c.CreateTime)
                .ToListAsync((a, b, c) => new
                {
                    Province = c.Province,
                    City = c.City,
                    ExamSiteId = c.Id,
                    ExamSiteName = c.ExaminationSiteName,
                    SpecificAddress = c.SpecificAddress,
                    ExamSiteCode = c.ExaminationSiteCode,
                    ExamRoomId = b.Id,
                    ExamRoomName = b.ExaminationRoomName,
                    SeatingCapacity = b.SeatingCapacity
                });

            List<DistributeExamRoomViewModel> proCityDatas = new List<DistributeExamRoomViewModel>();

            //省市列表
            var proCityList = selectData.Select(x => new { x.Province, x.City }).Distinct().ToList();

            List<string> citys = proCityList.Select(x => x.City).ToList();

            //获取报名城市和人数
            var signUpList = await DbContext.FreeSql.GetRepository<SignUp>().Where(x => x.ExamPlanId == planId && citys.Contains(x.City)
                        && x.IsDelete == CommonConstants.IsNotDelete).GroupBy(x => x.City).ToListAsync(x => new { x.Key, Count = x.Count() });

            proCityList.ForEach(x =>
            {
                DistributeExamRoomViewModel proCityData = new DistributeExamRoomViewModel();
                proCityData.Province = x.Province;
                proCityData.City = x.City;
                //已报考人数
                proCityData.SignUpNumber = signUpList.FirstOrDefault(s => s.Key == proCityData.City)?.Count ?? 0;

                //考点列表
                var siteList = selectData.Where(a => a.Province == x.Province && a.City == x.City).Select(a =>
                 new { a.ExamSiteId, a.ExamSiteName, a.SpecificAddress, a.ExamSiteCode }).Distinct().ToList();

                List<DisExamSiteViewModel> siteDatas = new List<DisExamSiteViewModel>();

                siteList.ForEach(y =>
                {
                    DisExamSiteViewModel siteData = new DisExamSiteViewModel();
                    siteData.ExamSiteId = y.ExamSiteId;
                    siteData.ExamSiteName = y.ExamSiteName;
                    siteData.SpecificAddress = y.SpecificAddress;
                    siteData.ExamSiteCode = y.ExamSiteCode;

                    //考场列表
                    var roomList = selectData.Where(a => a.ExamSiteId == y.ExamSiteId).Select(a =>
                      new { a.ExamRoomId, a.ExamRoomName, a.SeatingCapacity }).ToList();

                    List<DisExamRoomViewModel> roomDatas = new List<DisExamRoomViewModel>();

                    roomList.ForEach(z =>
                    {
                        DisExamRoomViewModel roomData = new DisExamRoomViewModel();
                        roomData.ExamRoomId = z.ExamRoomId;
                        roomData.ExamRoomName = z.ExamRoomName;
                        roomData.SeatingCapacity = z.SeatingCapacity;

                        roomDatas.Add(roomData);
                    });

                    siteData.ExamRoomList = roomDatas;

                    siteDatas.Add(siteData);
                });

                proCityData.ExamSiteList = siteDatas;

                proCityDatas.Add(proCityData);
            });

            response.Data = proCityDatas;

            return response;
        }
        #endregion

        #region 删除已分配考试计划考场或考点
        /// <summary>
        /// 删除已分配考试计划考场或考点
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteDistributeExamSiteOrRoom(DeleteDistributeExamSiteOrRoomDto dto)
        {
            if (dto == null || dto.SiteIdsOrRoomIds.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "请选择考场!" };

            //获取考试计划
            ExamPlan examPlan = await DbContext.FreeSql.GetRepository<ExamPlan>().Where(x => x.Id == dto.PlanId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (examPlan == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该考试计划!" };

            List<OneXPlanExamRoom> deleteList = new List<OneXPlanExamRoom>();

            //考场
            if (dto.DeleteDataType == 0)
            {
                deleteList = await DbContext.FreeSql.GetRepository<OneXPlanExamRoom>().Where(x => x.ExamPlanId == dto.PlanId && dto.SiteIdsOrRoomIds.Contains(x.ExamRoomId)
                      && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            }
            //考点
            if (dto.DeleteDataType == 1)
            {
                deleteList = await DbContext.FreeSql.GetRepository<OneXPlanExamRoom>().Where(x => x.ExamPlanId == dto.PlanId && dto.SiteIdsOrRoomIds.Contains(x.ExamSiteId)
                      && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            }

            //删除数据集
            deleteList.ForEach(x =>
            {
                x.IsDelete = CommonConstants.IsDelete;
                x.UpdateTime = DateTime.Now;
            });

            bool state = DbContext.FreeSql.GetRepository<OneXPlanExamRoom>().UpdateDiy.SetSource(deleteList).UpdateColumns(x =>
                   new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

    }
}
