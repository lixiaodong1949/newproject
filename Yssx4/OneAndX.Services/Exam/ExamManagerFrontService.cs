﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using Yssx.Framework.Auth;
using Yssx.Framework.Utils;
using Yssx.Framework.Redis;
using Yssx.Framework.Entity;

namespace OneAndX.Services
{
    /// <summary>
    /// 考试管理服务(前台)
    /// </summary>
    public class ExamManagerFrontService : IExamManagerFrontService
    {
        #region 获取考试计划列表
        /// <summary>
        /// 获取考试计划列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<ExamPlanViewModel>>> GetExamPlanList(ExamPlanQuery query)
        {
            ResponseContext<List<ExamPlanViewModel>> response = new ResponseContext<List<ExamPlanViewModel>>();
            if (query == null)
                return response;

            //查看考试计划列表
            //报名已结束
            var select = DbContext.FreeSql.Select<ExamPlan>().From<OneXPlanExamSchedule>((a, b) => a.LeftJoin(x => x.Id == b.ExamPlanId))
                 .Where((a, b) => a.ApplyEndTime < DateTime.Now && a.IsDelete == CommonConstants.IsNotDelete);

            if (query.CertificateType.HasValue)
                select.Where((a, b) => a.Certificate == (Certificate)query.CertificateType.Value);
            if (query.CertificateLevel.HasValue)
                select.Where((a, b) => a.CertificateLevel == (CertificateLevel)query.CertificateLevel.Value);

            response.Data = await select.OrderBy((a, b) => a.CreateTime)
                .ToListAsync((a, b) => new ExamPlanViewModel
                {
                    PlanId = a.Id,
                    Name = a.Name,
                    CertificateType = (int)a.Certificate,
                    CertificateLevel = (int)a.CertificateLevel,
                    TotalMinutes = a.TotalMinutes,
                    BeginTime = a.BeginTime,
                    EndTime = a.EndTime,
                    IsDistributedExamRoom = b.IsDistributedExamRoom,
                    IsCreatedExamCard = b.IsCreatedExamCard,
                    IsCreatedExamPaper = b.IsCreatedExamPaper,
                });

            return response;
        }
        #endregion

        #region 获取已报名考试计划考场
        /// <summary>
        /// 获取已报名考试计划考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<SignUpExamRoomViewModel>>> GetSignUpExamRoomList(long planId)
        {
            ResponseContext<List<SignUpExamRoomViewModel>> response = new ResponseContext<List<SignUpExamRoomViewModel>>();

            //获取考试计划
            ExamPlan examPlan = await DbContext.FreeSql.GetRepository<ExamPlan>().Where(x => x.Id == planId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (examPlan == null)
                return response;

            //获取报名列表
            var signUpList = await DbContext.FreeSql.GetRepository<SignUp>().Where(x => x.ExamPlanId == planId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            //省市列表
            var proCityList = signUpList.Select(x => new { x.Province, x.City }).Distinct().ToList();
            //城市列表
            var citys = proCityList.Select(x => x.City).ToList();

            //查看报名城市下的考场列表
            var selectData = await DbContext.FreeSql.Select<OneXPlanExamRoom>().From<ExaminationSiteDetails, ExaminationSite>(
                (a, b, c) =>
                a.InnerJoin(x => x.ExamRoomId == b.Id)
                .InnerJoin(x => x.ExamSiteId == c.Id && b.ExaminationSiteId == c.Id))
                .Where((a, b, c) => a.ExamPlanId == planId && citys.Contains(c.City) && a.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete)
                .OrderByDescending((a, b, c) => c.Sort)
                .ToListAsync((a, b, c) => new
                {
                    Province = c.Province,
                    City = c.City,
                    ExamSiteId = c.Id,
                    ExamSiteName = c.ExaminationSiteName,
                    SpecificAddress = c.SpecificAddress,
                    ExamSiteCode = c.ExaminationSiteCode,
                    SiteSort = c.Sort,
                    ExamRoomId = b.Id,
                    ExamRoomName = b.ExaminationRoomName,
                    SeatingCapacity = b.SeatingCapacity,
                    RoomSort = b.Sort,
                });

            List<SignUpExamRoomViewModel> proCityDatas = new List<SignUpExamRoomViewModel>();

            proCityList.ForEach(x =>
            {
                SignUpExamRoomViewModel proCityData = new SignUpExamRoomViewModel();
                proCityData.Province = x.Province;
                proCityData.City = x.City;
                //已报考人数
                proCityData.SignUpNumber = signUpList.Where(s => s.City == x.City).Count();

                //考点列表
                var siteList = selectData.Where(a => a.Province == x.Province && a.City == x.City).Select(a =>
                 new { a.ExamSiteId, a.ExamSiteName, a.SpecificAddress, a.ExamSiteCode, a.SiteSort }).Distinct().OrderBy(a => a.SiteSort).ToList();

                List<SuExamSiteViewModel> siteDatas = new List<SuExamSiteViewModel>();

                siteList.ForEach(y =>
                {
                    SuExamSiteViewModel siteData = new SuExamSiteViewModel();
                    siteData.ExamSiteId = y.ExamSiteId;
                    siteData.ExamSiteName = y.ExamSiteName;
                    siteData.SpecificAddress = y.SpecificAddress;
                    siteData.ExamSiteCode = y.ExamSiteCode;
                    siteData.SiteSort = y.SiteSort;

                    //考场列表
                    var roomList = selectData.Where(a => a.ExamSiteId == y.ExamSiteId).Select(a =>
                      new { a.ExamRoomId, a.ExamRoomName, a.SeatingCapacity, a.RoomSort }).OrderBy(a => a.RoomSort).ToList();

                    List<SuExamRoomViewModel> roomDatas = new List<SuExamRoomViewModel>();

                    roomList.ForEach(z =>
                    {
                        SuExamRoomViewModel roomData = new SuExamRoomViewModel();
                        roomData.ExamRoomId = z.ExamRoomId;
                        roomData.ExamRoomName = z.ExamRoomName;
                        roomData.SeatingCapacity = z.SeatingCapacity;
                        roomData.RoomSort = z.RoomSort;

                        roomDatas.Add(roomData);
                    });

                    siteData.ExamRoomList = roomDatas;

                    siteDatas.Add(siteData);
                });

                proCityData.ExamSiteList = siteDatas;

                proCityDatas.Add(proCityData);
            });

            response.Data = proCityDatas;

            return response;
        }
        #endregion

        #region 随机分配考生考场
        /// <summary>
        /// 随机分配考生考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<int>> RandomAllotExamRoom(long planId, long userId)
        {
            #region 查询数据

            //获取考试计划
            ExamPlan examPlan = await DbContext.FreeSql.GetRepository<ExamPlan>().Where(x => x.Id == planId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (examPlan == null)
                return new ResponseContext<int> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "没有该考试计划!" };

            //获取考试计划进度
            OneXPlanExamSchedule selectSchedule = await DbContext.FreeSql.GetRepository<OneXPlanExamSchedule>().Where(x => x.ExamPlanId == planId).FirstAsync();
            if (selectSchedule != null && selectSchedule.IsDistributedExamRoom)
                return new ResponseContext<int> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "考场已分配,请刷新页面!" };

            //获取报名列表
            var signUpList = await DbContext.FreeSql.GetRepository<SignUp>().Where(x => x.ExamPlanId == planId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            if (signUpList.Count == 0)
                return new ResponseContext<int> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "没有考生!" };

            //省市列表
            var proCityList = signUpList.Select(x => new { x.Province, x.City }).Distinct().ToList();
            //城市列表
            var citys = proCityList.Select(x => x.City).ToList();

            //查看报名城市下的考场列表
            var selectData = await DbContext.FreeSql.Select<OneXPlanExamRoom>().From<ExaminationSiteDetails, ExaminationSite>(
                (a, b, c) =>
                a.InnerJoin(x => x.ExamRoomId == b.Id)
                .InnerJoin(x => x.ExamSiteId == c.Id && b.ExaminationSiteId == c.Id))
                .Where((a, b, c) => a.ExamPlanId == planId && citys.Contains(c.City) && a.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete)
                .OrderByDescending((a, b, c) => c.CreateTime)
                .ToListAsync((a, b, c) => new
                {
                    Province = c.Province,
                    City = c.City,
                    ExamSiteId = c.Id,
                    ExamSiteName = c.ExaminationSiteName,
                    SpecificAddress = c.SpecificAddress,
                    ExamSiteCode = c.ExaminationSiteCode,
                    SiteSort = c.Sort,
                    ExamRoomId = b.Id,
                    ExamRoomName = b.ExaminationRoomName,
                    SeatingCapacity = b.SeatingCapacity,
                    RoomSort = b.Sort,
                });

            #endregion

            #region 随机分配座位号

            List<SignUp> peopleList = new List<SignUp>();
            List<OneXPlanExamAllotRoom> allotRoomList = new List<OneXPlanExamAllotRoom>();

            //按城市分配考场
            citys.ForEach(x =>
            {
                //获取城市下报考人列表
                var examPepples = signUpList.Where(a => a.City == x).ToList();
                //获取城市下考场列表
                var roomList = selectData.Where(a => a.City == x).OrderBy(a => a.RoomSort).ToList();

                //分配座位号
                foreach (var item in roomList)
                {
                    if (examPepples.Count == 0)
                        break;

                    OneXPlanExamAllotRoom allotRoom = new OneXPlanExamAllotRoom();
                    allotRoom.Id = IdWorker.NextId();
                    allotRoom.ExamPlanId = planId;
                    allotRoom.ExamSiteId = item.ExamSiteId;
                    allotRoom.ExamRoomId = item.ExamRoomId;
                    allotRoom.CreateBy = userId;
                    allotRoom.CreateTime = DateTime.Now;
                    allotRoom.UpdateBy = userId;
                    allotRoom.UpdateTime = DateTime.Now;

                    Random random = new Random();

                    int tempCount = examPepples.Count;
                    //如果报考人数大于座位号
                    if (examPepples.Count > item.SeatingCapacity)
                        tempCount = item.SeatingCapacity;
                    else
                        tempCount = examPepples.Count;

                    for (int i = 0; i < tempCount; i++)
                    {
                        int k = random.Next(0, examPepples.Count); //取得大于等于0且小于数据总数的一个随机数

                        examPepples[k].ExamSiteId = item.ExamSiteId;
                        examPepples[k].ExamRoomId = item.ExamRoomId;
                        examPepples[k].SeatNumber = i + 1;
                        examPepples[k].UpdateBy = userId;
                        examPepples[k].UpdateTime = DateTime.Now;

                        if ((i + 1) % 2 > 0)
                            examPepples[k].SeatType = (int)SeatType.OddNumber;
                        else
                            examPepples[k].SeatType = (int)SeatType.EvenNumber;

                        peopleList.Add(examPepples[k]);

                        examPepples.RemoveAt(k);
                    }
                    //已分配座位数
                    allotRoom.AllotedSeating = tempCount;

                    allotRoomList.Add(allotRoom);
                }
            });

            #endregion

            #region 更新数据

            bool state = false;

            OneXPlanExamSchedule schedule = new OneXPlanExamSchedule
            {
                Id = IdWorker.NextId(),
                ExamPlanId = planId,
                IsDistributedExamRoom = true,
                IsCreatedExamCard = false,
                IsCreatedExamPaper = false,
            };

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    if (peopleList.Count > 0)
                        //更新报名人员信息
                        state = uow.GetRepository<SignUp>().UpdateDiy.SetSource(peopleList).UpdateColumns(x => new
                        {
                            x.ExamSiteId,
                            x.ExamRoomId,
                            x.SeatNumber,
                            x.SeatType,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrows() > 0;

                    //添加考试计划分配考场列表
                    if (allotRoomList.Count > 0)
                        uow.GetRepository<OneXPlanExamAllotRoom>().Insert(allotRoomList);

                    //添加考试计划进度表
                    uow.GetRepository<OneXPlanExamSchedule>().Insert(schedule);

                    if (state)
                        uow.Commit();
                    else
                        uow.Rollback();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            return new ResponseContext<int> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = signUpList.Count, Msg = state ? "成功" : "操作失败!" };

            #endregion
        }
        #endregion

        #region 获取已随机分配考场
        /// <summary>
        /// 获取已随机分配考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<SignUpExamRoomViewModel>>> GetAllotedExamRoomList(long planId)
        {
            ResponseContext<List<SignUpExamRoomViewModel>> response = new ResponseContext<List<SignUpExamRoomViewModel>>();

            //获取考试计划
            ExamPlan examPlan = await DbContext.FreeSql.GetRepository<ExamPlan>().Where(x => x.Id == planId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (examPlan == null)
                return response;

            //获取报名列表
            var signUpList = await DbContext.FreeSql.GetRepository<SignUp>().Where(x => x.ExamPlanId == planId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            //省市列表
            var proCityList = signUpList.Select(x => new { x.Province, x.City }).Distinct().ToList();
            //城市列表
            var citys = proCityList.Select(x => x.City).ToList();

            //查看报名城市下的考场列表
            var selectData = await DbContext.FreeSql.Select<OneXPlanExamAllotRoom>().From<ExaminationSiteDetails, ExaminationSite>(
                (a, b, c) =>
                a.InnerJoin(x => x.ExamRoomId == b.Id)
                .InnerJoin(x => x.ExamSiteId == c.Id && b.ExaminationSiteId == c.Id))
                .Where((a, b, c) => a.ExamPlanId == planId && citys.Contains(c.City) && a.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete)
                .OrderBy((a, b, c) => c.Sort)
                .ToListAsync((a, b, c) => new
                {
                    Province = c.Province,
                    City = c.City,
                    ExamSiteId = c.Id,
                    ExamSiteName = c.ExaminationSiteName,
                    SpecificAddress = c.SpecificAddress,
                    ExamSiteCode = c.ExaminationSiteCode,
                    SiteSort = c.Sort,
                    ExamRoomId = b.Id,
                    ExamRoomName = b.ExaminationRoomName,
                    RoomSort = b.Sort,
                    SeatingCapacity = a.AllotedSeating,//实际分配的座位数
                });

            List<SignUpExamRoomViewModel> proCityDatas = new List<SignUpExamRoomViewModel>();

            proCityList.ForEach(x =>
            {
                SignUpExamRoomViewModel proCityData = new SignUpExamRoomViewModel();
                proCityData.Province = x.Province;
                proCityData.City = x.City;
                //已报考人数
                proCityData.SignUpNumber = signUpList.Where(s => s.City == x.City).Count();

                //考点列表
                var siteList = selectData.Where(a => a.Province == x.Province && a.City == x.City).Select(a =>
                 new { a.ExamSiteId, a.ExamSiteName, a.SpecificAddress, a.ExamSiteCode, a.SiteSort }).Distinct().ToList();

                List<SuExamSiteViewModel> siteDatas = new List<SuExamSiteViewModel>();

                siteList.ForEach(y =>
                {
                    SuExamSiteViewModel siteData = new SuExamSiteViewModel();
                    siteData.ExamSiteId = y.ExamSiteId;
                    siteData.ExamSiteName = y.ExamSiteName;
                    siteData.SpecificAddress = y.SpecificAddress;
                    siteData.ExamSiteCode = y.ExamSiteCode;
                    siteData.SiteSort = y.SiteSort;

                    //考场列表
                    var roomList = selectData.Where(a => a.ExamSiteId == y.ExamSiteId).Select(a =>
                      new { a.ExamRoomId, a.ExamRoomName, a.SeatingCapacity, a.RoomSort }).ToList();

                    List<SuExamRoomViewModel> roomDatas = new List<SuExamRoomViewModel>();

                    roomList.ForEach(z =>
                    {
                        SuExamRoomViewModel roomData = new SuExamRoomViewModel();
                        roomData.ExamRoomId = z.ExamRoomId;
                        roomData.ExamRoomName = z.ExamRoomName;
                        roomData.SeatingCapacity = z.SeatingCapacity;
                        roomData.RoomSort = z.RoomSort;

                        roomDatas.Add(roomData);
                    });

                    siteData.ExamRoomList = roomDatas;

                    siteDatas.Add(siteData);
                });

                proCityData.ExamSiteList = siteDatas;

                proCityDatas.Add(proCityData);
            });

            response.Data = proCityDatas;

            return response;
        }
        #endregion

        #region 随机生成准考证和密码
        /// <summary>
        /// 随机生成准考证和密码
        /// </summary>
        /// <param name="planId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<int>> RandomCreateExamCardNumber(long planId, long userId)
        {
            //获取考试计划
            ExamPlan examPlan = await DbContext.FreeSql.GetRepository<ExamPlan>().Where(x => x.Id == planId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (examPlan == null)
                return new ResponseContext<int> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "没有该考试计划!" };

            //获取考试计划进度
            OneXPlanExamSchedule schedule = await DbContext.FreeSql.GetRepository<OneXPlanExamSchedule>().Where(x => x.ExamPlanId == planId).FirstAsync();
            if (schedule == null)
                return new ResponseContext<int> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "考试计划进度不存在!" };
            if (!schedule.IsDistributedExamRoom)
                return new ResponseContext<int> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "请先分配考场!" };
            if (schedule.IsCreatedExamCard)
                return new ResponseContext<int> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "准考证已生成,请刷新页面!" };

            //获取报名列表
            var signUpList = await DbContext.FreeSql.GetRepository<SignUp>().Where(x => x.ExamPlanId == planId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (signUpList.Count == 0)
                return new ResponseContext<int> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "没有考生!" };

            string examDate = examPlan.BeginTime.ToString("yyyyMMdd");

            string cerCode = null;
            switch (examPlan.Certificate)
            {
                case Certificate.CBGK:
                    cerCode = "001";
                    break;
                case Certificate.DSJTRZ:
                    cerCode = "002";
                    break;
                case Certificate.CWGL:
                    cerCode = "003";
                    break;
                default:
                    break;
            }

            string levelCode = null;
            switch (examPlan.CertificateLevel)
            {
                case CertificateLevel.Primary:
                    levelCode = "01";
                    break;
                case CertificateLevel.Intermediate:
                    levelCode = "02";
                    break;
                case CertificateLevel.Advanced:
                    levelCode = "03";
                    break;
                default:
                    break;
            }

            foreach (var item in signUpList)
            {
                if (string.IsNullOrEmpty(item.IDNumber) || item.IDNumber.Length < 15)
                    return new ResponseContext<int> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "身份证错误!" };
                string idNum = null;
                //身份证遇X字母往前取一位
                if (item.IDNumber.Substring(item.IDNumber.Length - 1, 1) == "X" || item.IDNumber.Substring(item.IDNumber.Length - 1, 1) == "x")
                    idNum = item.IDNumber.Substring(item.IDNumber.Length - 5, 4);
                else
                    idNum = item.IDNumber.Substring(item.IDNumber.Length - 4, 4);

                Random random = new Random();
                string ranNumber = random.Next(10, 100).ToString();
                string password = random.Next(1000, 10000).ToString();

                //准考证号
                item.ExamCardNumber = string.Format("{0}{1}{2}{3}{4}", examDate, cerCode, levelCode, idNum, ranNumber);
                //密码
                item.ExamPassword = password;

                item.UpdateBy = userId;
                item.UpdateTime = DateTime.Now;
            }

            schedule.IsCreatedExamCard = true;

            bool state = false;

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    if (signUpList.Count > 0)
                        //更新报名人员信息
                        state = uow.GetRepository<SignUp>().UpdateDiy.SetSource(signUpList).UpdateColumns(x => new
                        {
                            x.ExamCardNumber,
                            x.ExamPassword,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrows() > 0;

                    //更新考试计划进度表
                    state = uow.GetRepository<OneXPlanExamSchedule>().UpdateDiy.SetSource(schedule).UpdateColumns(x => new
                    {
                        x.IsCreatedExamCard
                    }).ExecuteAffrows() > 0;

                    if (state)
                        uow.Commit();
                    else
                        uow.Rollback();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            return new ResponseContext<int> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = signUpList.Count, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取考生信息列表
        /// <summary>
        /// 获取考生信息列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<ExamineeInfoViewModel>> GetExamineeInfoList(ExamineeInfoQuery query)
        {
            var result = new PageResponse<ExamineeInfoViewModel>();
            if (query == null)
                return result;

            //查询报名列表
            var select = DbContext.FreeSql.Select<SignUp>().From<ExaminationSite, ExaminationSiteDetails>(
                (a, b, c) => a.LeftJoin(x => x.ExamSiteId == b.Id)
                .LeftJoin(x => x.ExamRoomId == c.Id))
                 .Where((a, b, c) => a.ExamPlanId == query.PlanId && a.IsDelete == CommonConstants.IsNotDelete);

            if (query.ExamSiteId.HasValue)
                select.Where((a, b, c) => a.ExamSiteId == query.ExamSiteId.Value);
            if (query.ExamRoomId.HasValue)
                select.Where((a, b, c) => a.ExamRoomId == query.ExamRoomId.Value);
            if (!string.IsNullOrEmpty(query.Keyword))
                select.Where((a, b, c) => a.Name.Contains(query.Keyword) || a.IDNumber.Contains(query.Keyword) || a.ExamCardNumber.Contains(query.Keyword));

            var totalCount = select.ToList().Count();

            result.Data = await select.Page(query.PageIndex, query.PageSize)
                .ToListAsync((a, b, c) => new ExamineeInfoViewModel
                {
                    Name = a.Name,
                    Sex = a.Sex,
                    IDNumber = a.IDNumber,
                    Photo = a.Photo,
                    Phone = a.Phone,
                    ExamCardNumber = a.ExamCardNumber,
                    ExamPassword = a.ExamPassword,
                    ExamSiteName = b.ExaminationSiteName,
                    ExamRoomName = c.ExaminationRoomName,
                    SeatNumber = a.SeatNumber,
                    SiteSort = b.Sort,
                    RoomSort = c.Sort,
                });

            result.Data = result.Data.OrderBy(x => x.SiteSort).ThenBy(x => x.RoomSort).ThenBy(x => x.SeatNumber).ToList();

            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 随机生成试卷
        /// <summary>
        /// 随机生成试卷
        /// </summary>
        /// <param name="planId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RandomCreateExamPaper(long planId, long userId)
        {
            //获取考试计划
            ExamPlan examPlan = await DbContext.FreeSql.GetRepository<ExamPlan>().Where(x => x.Id == planId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (examPlan == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该考试计划!" };

            //获取考试计划进度
            OneXPlanExamSchedule schedule = await DbContext.FreeSql.GetRepository<OneXPlanExamSchedule>().Where(x => x.ExamPlanId == planId).FirstAsync();
            if (schedule == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "考试计划进度不存在!" };
            if (!schedule.IsDistributedExamRoom || !schedule.IsCreatedExamCard)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "请先分配考场或准考证!" };
            if (schedule.IsCreatedExamPaper)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "试卷已生成,请刷新页面!" };

            //查询考试计划下证书案例 (开启状态)
            List<ExamCase> examCaseList = await DbContext.FreeSql.GetRepository<ExamCase>().Where(x => x.Certificate == examPlan.Certificate
                && x.CertificateLevel == examPlan.CertificateLevel && x.Status == Status.Enable && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            if (examCaseList.Count < 2)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "试卷最少需要两套!" };

            Random random = new Random();

            //奇数偶数抽取排序
            int rank = random.Next(1, 3);

            //随机抽取A案例
            int k = random.Next(0, examCaseList.Count);
            ExamCase aCase = examCaseList[k];
            examCaseList.RemoveAt(k);

            //随机抽取B案例
            k = random.Next(0, examCaseList.Count);
            ExamCase bCase = examCaseList[k];
            examCaseList.RemoveAt(k);

            //座位号为奇数考生试卷
            ExamCase oddExamCase = new ExamCase();
            //座位号为偶数考生试卷
            ExamCase evenExamCase = new ExamCase();

            //先生成奇数试卷
            if (rank == 1)
            {
                oddExamCase = aCase;
                evenExamCase = bCase;
            }
            else
            {
                oddExamCase = bCase;
                evenExamCase = aCase;
            }

            //生成座位号为奇数考生试卷
            ResponseContext<bool> responseOdd = await CreateExamPaper(oddExamCase, examPlan, (int)SeatType.OddNumber, schedule, userId);

            //生成座位号为偶数考生试卷
            ResponseContext<bool> responseEven = await CreateExamPaper(evenExamCase, examPlan, (int)SeatType.EvenNumber, schedule, userId);

            bool state = false;

            if (responseOdd.Data && responseEven.Data)
                state = true;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 生成对应系统的试卷信息
        /// <summary>
        /// 生成对应系统的试卷信息
        /// </summary>
        /// <param name="examCase">试卷案例</param>
        /// <param name="examPlan">考试计划</param>
        /// <param name="serviceSeatType">1:奇数 2:偶数</param>
        /// <param name="schedule">考试计划进度</param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> CreateExamPaper(ExamCase examCase, ExamPlan examPlan, int serviceSeatType, OneXPlanExamSchedule schedule, long userId)
        {
            #region 初始化考试计划试卷关系表

            bool state = false;

            //试卷Id
            long examId = IdWorker.NextId();

            OneXPlanExamPaperRelation relationData = new OneXPlanExamPaperRelation();
            relationData.Id = IdWorker.NextId();
            relationData.ExamPlanId = examPlan.Id;
            relationData.ExamId = examId;
            relationData.Name = examCase.Name;
            relationData.ServiceSeatType = serviceSeatType;
            relationData.CaseId = examCase.SourceCaseId;
            relationData.BeginTime = examPlan.BeginTime;
            relationData.EndTime = examPlan.EndTime;
            relationData.TotalMinutes = examPlan.TotalMinutes;
            relationData.SourceSystem = examCase.SourceSystem;
            relationData.CreateBy = userId;
            relationData.CreateTime = DateTime.Now;
            relationData.UpdateBy = userId;
            relationData.UpdateTime = DateTime.Now;

            #endregion

            #region 竞赛

            //竞赛
            if (examCase.SourceSystem == CaseSourceSystem.JS)
            {
                //案例
                JsYssxCase caseData = await DbContext.JSFreeSql.GetRepository<JsYssxCase>().Where(x => x.Id == examCase.SourceCaseId).FirstAsync();

                //题目总数
                long totalTopicCount = await DbContext.JSFreeSql.GetRepository<JsYssxTopic>().Where(x => x.CaseId == examCase.SourceCaseId && x.ParentId == 0
                 && x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
                //题目总分
                decimal totalScore = await DbContext.JSFreeSql.GetRepository<JsYssxTopic>().Where(x => x.CaseId == examCase.SourceCaseId && x.ParentId == 0
                   && x.IsDelete == CommonConstants.IsNotDelete).SumAsync(x => x.Score);

                //试卷
                JsExamPaper examPaper = new JsExamPaper()
                {
                    Id = examId,
                    TaskId = examPlan.Id,
                    CanShowAnswerBeforeEnd = false,
                    CaseId = examCase.SourceCaseId,
                    Name = examCase.Name,
                    ExamType = ExamType.PracticeTest,
                    CompetitionType = CompetitionType.TeamCompetition,
                    TotalScore = totalScore,
                    TotalQuestionCount = (int)totalTopicCount,
                    TenantId = CommonConstants.OneXTenantId,
                    ExamSourceType = ExamSourceType.ProTask,
                    BeginTime = examPlan.BeginTime,
                    EndTime = examPlan.EndTime,
                    TotalMinutes = examPlan.TotalMinutes,
                    IsRelease = true,
                    CreateBy = userId,
                    CreateTime = DateTime.Now,
                    UpdateBy = userId,
                    UpdateTime = DateTime.Now,
                };

                var returnData = await DbContext.JSFreeSql.GetRepository<JsExamPaper>().InsertAsync(examPaper);
                state = returnData != null;

                relationData.RollUpType = caseData.RollUpType;
                relationData.CaseYear = caseData.CaseYear;
                relationData.TotalScore = totalScore;
                relationData.TotalQuestionCount = (int)totalTopicCount;
            }

            #endregion

            #region 行业赛

            //行业赛
            if (examCase.SourceSystem == CaseSourceSystem.HYS)
            {
                //案例
                HysYssxCase caseData = await DbContext.HysFreeSql.GetRepository<HysYssxCase>().Where(x => x.Id == examCase.SourceCaseId).FirstAsync();

                //题目总数
                long totalTopicCount = await DbContext.HysFreeSql.GetRepository<HysYssxTopic>().Where(x => x.CaseId == examCase.SourceCaseId && x.ParentId == 0
                 && x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
                //题目总分
                decimal totalScore = await DbContext.HysFreeSql.GetRepository<HysYssxTopic>().Where(x => x.CaseId == examCase.SourceCaseId && x.ParentId == 0
                   && x.IsDelete == CommonConstants.IsNotDelete).SumAsync(x => x.Score);

                //试卷
                HysExamPaper examPaper = new HysExamPaper()
                {
                    Id = examId,
                    TaskId = examPlan.Id,
                    CanShowAnswerBeforeEnd = false,
                    CaseId = examCase.SourceCaseId,
                    Name = examCase.Name,
                    ExamType = ExamType.PracticeTest,
                    CompetitionType = CompetitionType.TeamCompetition,
                    TotalScore = totalScore,
                    TotalQuestionCount = (int)totalTopicCount,
                    TenantId = CommonConstants.OneXTenantId,
                    ExamSourceType = ExamSourceType.ProTask,
                    BeginTime = examPlan.BeginTime,
                    EndTime = examPlan.EndTime,
                    TotalMinutes = examPlan.TotalMinutes,
                    IsRelease = true,
                    CreateBy = userId,
                    CreateTime = DateTime.Now,
                    UpdateBy = userId,
                    UpdateTime = DateTime.Now,
                };

                var returnData = await DbContext.HysFreeSql.GetRepository<HysExamPaper>().InsertAsync(examPaper);
                state = returnData != null;

                relationData.RollUpType = caseData.RollUpType;
                relationData.CaseYear = caseData.CaseYear;
                relationData.TotalScore = totalScore;
                relationData.TotalQuestionCount = (int)totalTopicCount;
            }

            #endregion

            #region 云实训

            //云实训
            if (examCase.SourceSystem == CaseSourceSystem.YSSX)
            {
                //案例
                SxYssxCase caseData = await DbContext.SxFreeSql.GetRepository<SxYssxCase>().Where(x => x.Id == examCase.SourceCaseId).FirstAsync();

                //题目总数
                long totalTopicCount = await DbContext.SxFreeSql.GetRepository<SxYssxTopic>().Where(x => x.CaseId == examCase.SourceCaseId && x.ParentId == 0
                 && x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
                //题目总分
                decimal totalScore = await DbContext.SxFreeSql.GetRepository<SxYssxTopic>().Where(x => x.CaseId == examCase.SourceCaseId && x.ParentId == 0
                   && x.IsDelete == CommonConstants.IsNotDelete).SumAsync(x => x.Score);

                //试卷
                SxExamPaper examPaper = new SxExamPaper()
                {
                    Id = examId,
                    TaskId = examPlan.Id,
                    CanShowAnswerBeforeEnd = false,
                    CaseId = examCase.SourceCaseId,
                    Name = examCase.Name,
                    ExamType = ExamType.PracticeTest,
                    CompetitionType = CompetitionType.TeamCompetition,
                    TotalScore = totalScore,
                    TotalQuestionCount = (int)totalTopicCount,
                    ExamSourceType = ExamSourceType.ProTask,
                    BeginTime = examPlan.BeginTime,
                    EndTime = examPlan.EndTime,
                    TotalMinutes = examPlan.TotalMinutes,
                    TenantId = CommonConstants.OneXTenantId,
                    IsRelease = true,
                    CreateBy = userId,
                    CreateTime = DateTime.Now,
                    UpdateBy = userId,
                    UpdateTime = DateTime.Now,
                };

                var returnData = await DbContext.SxFreeSql.GetRepository<SxExamPaper>().InsertAsync(examPaper);
                state = returnData != null;

                relationData.RollUpType = caseData.RollUpType;
                relationData.CaseYear = caseData.CaseYear;
                relationData.TotalScore = totalScore;
                relationData.TotalQuestionCount = (int)totalTopicCount;
            }

            #endregion

            #region 添加考试计划关系、更新考试计划进度

            if (state)
            {
                schedule.IsCreatedExamPaper = true;

                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //添加考试计划关系
                        uow.GetRepository<OneXPlanExamPaperRelation>().Insert(relationData);

                        //更新考试计划进度表
                        state = uow.GetRepository<OneXPlanExamSchedule>().UpdateDiy.SetSource(schedule).UpdateColumns(x => new
                        {
                            x.IsCreatedExamPaper
                        }).ExecuteAffrows() > 0;

                        if (state)
                            uow.Commit();
                        else
                            uow.Rollback();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                    }
                }
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };

            #endregion
        }
        #endregion

        #region 获取考生成绩列表
        /// <summary>
        /// 获取考生成绩列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<ExamineeGradeViewModel>> GetExamineeGradeList(ExamineeGradeQuery query)
        {
            var result = new PageResponse<ExamineeGradeViewModel>();
            result.Data = new List<ExamineeGradeViewModel>();
            if (query == null)
                return result;

            //查询报名列表
            var select = DbContext.FreeSql.Select<SignUp>().From<ExaminationSite, ExaminationSiteDetails, OneXPlanExamPaperRelation, ExamUserGrade>(
                (a, b, c, d, e) => a.LeftJoin(x => x.ExamSiteId == b.Id)
                .LeftJoin(x => x.ExamRoomId == c.Id)
                .LeftJoin(x => x.ExamPlanId == d.ExamPlanId && x.SeatType == d.ServiceSeatType)
                .LeftJoin(x => d.ExamId == e.ExamId && x.Id == e.UserId && e.IsDelete == CommonConstants.IsNotDelete))
                 .Where((a, b, c, d, e) => a.ExamPlanId == query.PlanId && a.IsDelete == CommonConstants.IsNotDelete);

            if (query.ExamSiteId.HasValue)
                select.Where((a, b, c, d, e) => a.ExamSiteId == query.ExamSiteId.Value);
            if (query.ExamRoomId.HasValue)
                select.Where((a, b, c, d, e) => a.ExamRoomId == query.ExamRoomId.Value);
            if (!string.IsNullOrEmpty(query.Keyword))
                select.Where((a, b, c, d, e) => a.Name.Contains(query.Keyword) || a.IDNumber.Contains(query.Keyword) || a.ExamCardNumber.Contains(query.Keyword));

            var totalCount = select.ToList().Count();

            var sourceData = await select.OrderByDescending((a, b, c, d, e) => a.CreateTime).Page(query.PageIndex, query.PageSize)
                .ToListAsync((a, b, c, d, e) => new
                {
                    Name = a.Name,
                    Sex = a.Sex,
                    IDNumber = a.IDNumber,
                    Photo = a.Photo,
                    Phone = a.Phone,
                    ExamCardNumber = a.ExamCardNumber,
                    ExamSiteName = b.ExaminationSiteName,
                    ExamRoomName = c.ExaminationRoomName,
                    SeatNumber = a.SeatNumber,
                    TotalScore = e.Score,
                    IsPass = e.IsPass,
                    SubmitTime = e.CreateTime,
                    ExamId = e.ExamId,
                    GradeId = e.GradeId,
                    ExamName = d.Name,
                    CaseId = d.CaseId,
                    RollUpType = d.RollUpType,
                    CaseYear = d.CaseYear,
                    SourceSystem = d.SourceSystem,
                });

            sourceData.ForEach(x =>
            {
                ExamineeGradeViewModel model = new ExamineeGradeViewModel
                {
                    Name = x.Name,
                    Sex = x.Sex,
                    IDNumber = x.IDNumber,
                    Photo = x.Photo,
                    Phone = x.Phone,
                    ExamCardNumber = x.ExamCardNumber,
                    ExamSiteName = x.ExamSiteName,
                    ExamRoomName = x.ExamRoomName,
                    SeatNumber = x.SeatNumber,
                    TotalScore = x.TotalScore,
                    IsPass = x.IsPass,
                    SubmitTime = x.SubmitTime,
                    ExamId = x.ExamId,
                    GradeId = x.GradeId,
                    ExamName = x.ExamName,
                    CaseId = x.CaseId,
                    RollUpType = x.RollUpType,
                    CaseYear = x.CaseYear,
                    SourceSystem = x.SourceSystem,
                };

                result.Data.Add(model);
            });

            result.Data = result.Data.OrderByDescending(x => x.TotalScore).ToList();

            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 获取考试计划试卷信息列表
        /// <summary>
        /// 获取考试计划试卷信息列表
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<PlanExamPaperInfoViewModel>>> GetPlanExamPaperInfoList(long planId)
        {
            ResponseContext<List<PlanExamPaperInfoViewModel>> response = new ResponseContext<List<PlanExamPaperInfoViewModel>>();

            //获取考试计划
            ExamPlan examPlan = await DbContext.FreeSql.GetRepository<ExamPlan>().Where(x => x.Id == planId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (examPlan == null)
                return response;

            //获取考试计划试卷列表
            response.Data = await DbContext.FreeSql.GetRepository<OneXPlanExamPaperRelation>().Where(x => x.ExamPlanId == planId && x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new PlanExamPaperInfoViewModel
                {
                    ExamId = x.ExamId,
                    Name = x.Name,
                    ServiceSeatType = x.ServiceSeatType,
                    CaseId = x.CaseId,
                    RollUpType = x.RollUpType,
                    CaseYear = x.CaseYear,
                    SourceSystem = x.SourceSystem,
                });

            response.Data.ForEach(x =>
            {
                x.SeatCount = (int)DbContext.FreeSql.GetRepository<SignUp>().Where(a => a.ExamPlanId == planId && a.SeatType == x.ServiceSeatType &&
                    a.IsDelete == CommonConstants.IsNotDelete).Count();
            });

            return response;
        }
        #endregion

        #region 获取正式考试试卷信息
        /// <summary>
        /// 获取正式考试试卷信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<OfficialExamPaperInfoViewModel>> GetOfficialExamPaperInfoList(long userId)
        {
            ResponseContext<OfficialExamPaperInfoViewModel> response = new ResponseContext<OfficialExamPaperInfoViewModel>();

            //获取报名用户
            var signUser = await DbContext.FreeSql.GetRepository<SignUp>().Where(x => x.Id == userId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (signUser == null)
            {
                response.Msg = "用户不存在!";
                return response;
            }

            //获取考试计划试卷列表
            response.Data = await DbContext.FreeSql.GetRepository<OneXPlanExamPaperRelation>().Where(x => x.ExamPlanId == signUser.ExamPlanId && x.ServiceSeatType == signUser.SeatType
                && x.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync(x => new OfficialExamPaperInfoViewModel
                {
                    ExamId = x.ExamId,
                    Name = x.Name,
                    CaseId = x.CaseId,
                    RollUpType = x.RollUpType,
                    CaseYear = x.CaseYear,
                    BeginTime = x.BeginTime,
                    EndTime = x.EndTime,
                    TotalMinutes = x.TotalMinutes,
                    SourceSystem = x.SourceSystem,
                });

            if (response.Data != null || response.Data.ExamId > 0)
            {
                if (response.Data.SourceSystem == CaseSourceSystem.JS)
                {
                    var gradeData = await DbContext.JSFreeSql.GetRepository<JsExamStudentGrade>().Where(x => x.ExamId == response.Data.ExamId && x.UserId == userId
                    && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                    if (gradeData != null)
                        response.Data.GradeId = gradeData.Id;
                }
                if (response.Data.SourceSystem == CaseSourceSystem.HYS)
                {
                    var gradeData = await DbContext.HysFreeSql.GetRepository<HysExamStudentGrade>().Where(x => x.ExamId == response.Data.ExamId && x.UserId == userId
                    && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                    if (gradeData != null)
                        response.Data.GradeId = gradeData.Id;
                }
                if (response.Data.SourceSystem == CaseSourceSystem.YSSX)
                {
                    var gradeData = await DbContext.SxFreeSql.GetRepository<SxExamStudentGrade>().Where(x => x.ExamId == response.Data.ExamId && x.UserId == userId
                    && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                    if (gradeData != null)
                        response.Data.GradeId = gradeData.Id;
                }
            }

            return response;
        }
        #endregion

    }
}
