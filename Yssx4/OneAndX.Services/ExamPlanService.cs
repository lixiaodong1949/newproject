﻿using AspectCore.DynamicProxy;
using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;

namespace OneAndX.Services
{
    /// <summary>
    /// 试卷案例信息
    /// </summary>
    public class ExamPlanService : IExamPlanService
    {

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Add(ExamPlanAddDto dto, long currentUserId)
        {
            var data = dto.MapTo<ExamPlan>();
            data.Id = IdWorker.NextId();
            data.CreateBy = currentUserId;

            data.ApplyBeginTime = data.ApplyBeginTime.Date;// DateTime.Parse(data.ApplyBeginTime.ToString("yyyy-MM-dd"));
            data.ApplyEndTime = data.ApplyEndTime.Date;// DateTime.Parse(data.ApplyEndTime.ToString("yyyy-MM-dd"));

            var repo_ec = DbContext.FreeSql.GetRepository<ExamPlan>(e => e.IsDelete == CommonConstants.IsNotDelete);

            await repo_ec.InsertAsync(data);

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = data.Id.ToString() };
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Update(ExamPlanUpdateDto queryDto, long currentUserId)
        {
            var repo_ec = DbContext.FreeSql.GetRepository<ExamPlan>(e => e.IsDelete == CommonConstants.IsNotDelete);

            var ecase = await repo_ec.Where(e => e.Id == queryDto.Id).FirstAsync();

            if (ecase == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.BadRequest, Data = false, Msg = "试卷信息不存在。" };
            }

            ecase.Name = queryDto.Name;
            ecase.ApplyBeginTime = queryDto.ApplyBeginTime.Date;// DateTime.Parse(queryDto.ApplyBeginTime.ToString("yyyy-MM-dd"));
            ecase.ApplyEndTime = queryDto.ApplyEndTime.Date;//DateTime.Parse(queryDto.ApplyEndTime.ToString("yyyy-MM-dd"));
            ecase.BeginTime = queryDto.BeginTime;
            ecase.EndTime = queryDto.EndTime;
            ecase.Certificate = queryDto.Certificate;
            ecase.CertificateLevel = queryDto.CertificateLevel;
            ecase.TotalMinutes = queryDto.TotalMinutes;
            ecase.UpdateBy = currentUserId;
            ecase.UpdateTime = DateTime.Now;
            ecase.Sort = queryDto.Sort;

            //await repo_ec.UpdateDiy.SetSource(ecase).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();//修改内容

            await repo_ec.UpdateDiy.SetSource(ecase).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();//修改内容

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = queryDto.Id.ToString() };
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Delete(long id, long currentUserId)
        {
            //删除
            await DbContext.FreeSql.GetRepository<ExamPlan>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(e => new ExamPlan
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUserId,
                }).Where(e => e.Id == id).ExecuteAffrowsAsync();

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = id.ToString() };
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<PageResponse<ExamPlanListPageResponseDto>> GetListPage(ExamPlanQueryRequestDto queryDto)
        {
            long totalCount = 0;
            //按条件查询
            var selectData = await DbContext.FreeSql.GetRepository<ExamPlan>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(queryDto.Certificate > 0, e => e.Certificate == queryDto.Certificate)
                .WhereIf(queryDto.CertificateLevel > 0, e => e.CertificateLevel == queryDto.CertificateLevel)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Name), e => e.Name.Contains(queryDto.Name))
                .WhereIf(queryDto.Status.HasValue, e => e.Status == queryDto.Status.Value)
                .WhereIf(queryDto.BeginTime.HasValue, e => e.BeginTime >= queryDto.BeginTime.Value)
                .WhereIf(queryDto.EndTime.HasValue, e => e.EndTime <= queryDto.EndTime.Value)
                .Count(out totalCount)
                .OrderByDescending(m => m.CreateTime)
                .Page(queryDto.PageIndex, queryDto.PageSize)
                .ToListAsync<ExamPlanListPageResponseDto>();

            if (selectData != null && selectData.Count > 0)
            {
                selectData.ForEach(e =>
                {
                    e.Status = GetExamStatus(e.BeginTime, e.EndTime);//设置考试的状态
                });
            }

            var data = new PageResponse<ExamPlanListPageResponseDto>
            {
                Code = CommonConstants.SuccessCode,
                Data = selectData,
                PageIndex = queryDto.PageIndex,
                PageSize = queryDto.PageSize,
                RecordCount = totalCount,
            };

            return data;
        }

        /// <summary>
        /// 查询考试计划基本信息列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CertificateExamPlan>>> GetExamPlanBaseInfoList()
        {
            var datetime = DateTime.Now.Date;// DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));

            //按条件查询
            var selectData = await DbContext.FreeSql.GetRepository<ExamPlan>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.ApplyBeginTime <= datetime && datetime <= e.ApplyEndTime)//查询已经开始报名的数据和报名未结束的考试信息
                .ToListAsync<ExamPlanBaseInfo>();

            List<CertificateExamPlan> entitys = null;
            if (selectData != null && selectData.Count > 0)
            {
                entitys = new List<CertificateExamPlan>();
                foreach (var item in selectData.GroupBy(e => e.Certificate))
                {
                    var d = item.ToList();
                    //if (d != null && d.Count > 0)
                    //{
                    //    d.ForEach(e =>
                    //    {
                    //        e.Status = GetExamStatus(e.BeginTime, e.EndTime);//设置考试的状态
                    //    });
                    //}

                    CertificateExamPlan examPlanBaseInfo = new CertificateExamPlan
                    {
                        Certificate = item.Key,
                        ExamPlan = d,
                    };
                    entitys.Add(examPlanBaseInfo);
                }
            }

            var data = new ResponseContext<List<CertificateExamPlan>>
            {
                Code = CommonConstants.SuccessCode,
                Data = entitys,
            };

            return data;
        }

        /// <summary>
        /// 获取考试状态
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        internal static ExamStatus GetExamStatus(DateTime beginTime, DateTime endTime)
        {
            var examStatus = ExamStatus.Wait;
            var currdatetime = DateTime.Now;//当前时间

            if (currdatetime < beginTime)//当前时间小于考试开始时间 考试未开始
            {
                examStatus = ExamStatus.Wait;
            }
            else if (currdatetime >= beginTime && currdatetime <= endTime)//当前时间大于等于考试开始时间 并且 当前时间小于等于考试结束时间 考试开始
            {
                examStatus = ExamStatus.Started;
            }
            else if (currdatetime > endTime)//当前时间大于考试结束时间 考试结束
            {
                examStatus = ExamStatus.End;
            }
            return examStatus;
        }
    }
}
