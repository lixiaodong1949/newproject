﻿using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;

namespace OneAndX.Services
{
    /// <summary>
    /// 资源管理
    /// </summary>
    public class ResourceManagService : IResourceManag
    {
        #region 考点管理（后台）
        /// <summary>
        /// 添加 更新 考点
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateExaminationSite(ExaminationSiteDto model, long Id)
        {
            return await Task.Run(() =>
            {
            long opreationId = Id;//操作人ID

            if (model.Id != 0)
            {
                //修改
                var examinationSite = DbContext.FreeSql.Select<ExaminationSite>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (examinationSite == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

                examinationSite.ExaminationSiteName = model.ExaminationSiteName;
                examinationSite.Province = model.Province == null?"": model.Province;
                examinationSite.City = model.City == null?"": model.City;
                examinationSite.Area = model.Area == null?"": model.Area;
                examinationSite.SpecificAddress = model.SpecificAddress== null?"": model.SpecificAddress;
                examinationSite.Contacts = model.Contacts;
                examinationSite.ContactUs = model.ContactUs;
                examinationSite.ExaminationSiteCode = model.ExaminationSiteCode;
                examinationSite.Sort = model.Sort;
                examinationSite.UpdateBy = opreationId;
                examinationSite.UpdateTime = DateTime.Now;
                var i = DbContext.FreeSql.Update<ExaminationSite>().SetSource(examinationSite).UpdateColumns(m => new { m.ExaminationSiteName, m.Province, m.City, m.SpecificAddress, m.Contacts, m.ContactUs, m.ExaminationSiteCode, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                if (i == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };

                var items = model.ExaminationSiteDetails;
                var ii = DbContext.FreeSql.GetRepository<ExaminationSiteDetails>().UpdateDiy.SetSource(items).ExecuteAffrows();
                if (ii == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改(考场与座位,可考模块，可考等级)失败" };

                    //删除 对应证书记录
                    var t5 = DbContext.FreeSql.Delete<CertificateDetails>().Where(a => a.ExaminationSiteId == model.Id).ExecuteAffrows();
                    //重新 生成证书记录
                    var certificateDetailsList = new List<CertificateDetails>();
                    for (int j = 0; j < items.Count; j++)
                    {
                        //拆分模块
                        var examinablemodules = items[j].Examinablemodules.Split(',', '，');
                        //拆分等级
                        var examinationlevel = items[j].Examinationlevel.Split(',', '，');
                        for (int jj = 0; jj < examinablemodules.Length; jj++)
                        {
                            for (int jjj = 0; jjj < examinationlevel.Length; jjj++)
                            {
                                //组装证书
                                var certificateDetails = new CertificateDetails
                                {
                                    Certificate = (Certificate)int.Parse(examinablemodules[jj]),
                                    CertificateLevel = (CertificateLevel)int.Parse(examinationlevel[jjj]),
                                    ExaminationRoomId = items[j].Id,
                                    ExaminationSiteId = items[j].ExaminationSiteId,
                                    Id = IdWorker.NextId(),
                                };
                                certificateDetailsList.Add(certificateDetails);
                            }
                        }
                    }
                    var entity = DbContext.FreeSql.GetRepository<CertificateDetails>().Insert(certificateDetailsList);
                    if (entity == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "保存生成证书失败" };

                }
                else
                {
                    //保存
                    var examinationSite = new ExaminationSite
                    {
                        Id = IdWorker.NextId(),
                        ExaminationSiteName = model.ExaminationSiteName,
                        Province = model.Province == null ? "" : model.Province,
                        City = model.City == null ? "" : model.City,
                        Area=model.Area == null ? "" : model.Area,
                        SpecificAddress = model.SpecificAddress == null ? "" : model.SpecificAddress,
                        Contacts = model.Contacts,
                        ContactUs = model.ContactUs,
                        ExaminationSiteCode = model.ExaminationSiteCode,
                        Sort = model.Sort,
                        CreateBy = opreationId,
                        CreateTime = DateTime.Now
                    };
                    examinationSite = DbContext.FreeSql.GetRepository<ExaminationSite>().Insert(examinationSite);
                    if (examinationSite == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "保存失败" };

                    var examinationSiteDetails = model.ExaminationSiteDetails;
                    examinationSiteDetails.ForEach(x => { x.Id = IdWorker.NextId(); x.ExaminationSiteId = examinationSite.Id; });
                    var items = DbContext.FreeSql.GetRepository<ExaminationSiteDetails>().Insert(examinationSiteDetails);
                    if (items == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "保存(考场与座位,可考模块，可考等级)失败" };

                    //生成证书记录
                    var certificateDetailsList = new List<CertificateDetails>();
                    for (int j = 0; j < items.Count; j++)
                    {
                        //拆分模块
                        var examinablemodules = items[j].Examinablemodules.Split(',', '，');
                        //拆分等级
                        var examinationlevel = items[j].Examinationlevel.Split(',', '，');
                        for (int jj = 0; jj < examinablemodules.Length; jj++)
                        {
                            for (int jjj = 0; jjj < examinationlevel.Length; jjj++)
                            {
                                //组装证书
                                var certificateDetails = new CertificateDetails
                                {
                                    Certificate = (Certificate)int.Parse(examinablemodules[jj]),
                                    CertificateLevel = (CertificateLevel)int.Parse(examinationlevel[jjj]),
                                    ExaminationRoomId = items[j].Id,
                                    ExaminationSiteId = items[j].ExaminationSiteId,
                                    Id= IdWorker.NextId(),
                                };
                                certificateDetailsList.Add(certificateDetails);
                            }
                        }
                    }
                    var entity = DbContext.FreeSql.GetRepository<CertificateDetails>().Insert(certificateDetailsList);
                    if (entity == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "保存生成证书失败" };
                }

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 查找 考点列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<ExaminationSiteResponse>> FindExaminationSite(ExaminationSiteRequest model)
        {
            return await Task.Run(() =>
            {

                var selectData = DbContext.FreeSql.GetRepository<ExaminationSite>().Where(m => m.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.ExaminationSiteName), x => x.ExaminationSiteName.Contains(model.ExaminationSiteName)).OrderBy(m => m.Sort);
                var totalCount = selectData.Count();
                var sql = selectData.Page(model.PageIndex, model.PageSize).ToSql("Id,ExaminationSiteName,CONCAT(Province,City,Area,SpecificAddress) ExaminationAddress,ExaminationSiteCode,(SELECT COUNT(*) FROM 1x_ExaminationSite_Details WHERE ExaminationSiteId=a.id ) ExaminationNumber,(SELECT SUM(SeatingCapacity) FROM 1x_ExaminationSite_Details WHERE ExaminationSiteId=a.id ) SeatingCapacity,Contacts,ContactUs,Sort");
                var items = DbContext.FreeSql.Ado.Query<ExaminationSiteResponse>(sql).Select(x => new ExaminationSiteResponse
                {
                    Id = x.Id,
                    ExaminationSiteName = x.ExaminationSiteName,
                    ExaminationAddress = x.ExaminationAddress,
                    ExaminationSiteCode = x.ExaminationSiteCode,
                    ExaminationNumber = x.ExaminationNumber,
                    SeatingCapacity = x.SeatingCapacity,
                    Contacts = x.Contacts,
                    ContactUs = x.ContactUs,
                    Sort = x.Sort

                }).ToList();

                return new PageResponse<ExaminationSiteResponse> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };

            });
        }

        /// <summary>
        /// 查找 考点详情
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<ExaminationSiteDetailsRequest>> FindExaminationSiteDetails(long Id)
        {
            return await Task.Run(() =>
            {
                //获取 预览
                var sql = DbContext.FreeSql.Select<ExaminationSite>().Where(m => m.Id == Id && m.IsDelete == CommonConstants.IsNotDelete).ToSql();
                var obj = DbContext.FreeSql.Ado.Query<ExaminationSiteDetailsRequest>(sql).FirstOrDefault();
                if (obj == null) 
                    return new ResponseContext<ExaminationSiteDetailsRequest> { Code = CommonConstants.ErrorCode, Msg = "查找失败，找不到原始信息!" };

                var sql1 = DbContext.FreeSql.Select<ExaminationSiteDetails>().Where(m => m.ExaminationSiteId == Id).ToSql();
                var list = DbContext.FreeSql.Ado.Query<ExaminationSiteDetails>(sql1);

                obj.ExaminationSiteDetails = list;

                return new ResponseContext<ExaminationSiteDetailsRequest> { Code = CommonConstants.SuccessCode, Data = obj };
            });
        }

        /// <summary>
        /// 删除考点
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveExaminationSite(long id, long currentUserId)
        {
            ExaminationSite entity = await DbContext.FreeSql.GetRepository<ExaminationSite>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该考点!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<ExaminationSite>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;

            if (state == true)
            {
                //删除考点对应考场记录
                DbContext.FreeSql.Delete<ExaminationSiteDetails>().Where(a => a.ExaminationSiteId == id).ExecuteAffrows();
                //删除考点对应考场证书记录
                DbContext.FreeSql.Delete<CertificateDetails>().Where(a => a.ExaminationSiteId == id).ExecuteAffrows();
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }

        /// <summary>
        /// 删除考场（物理删）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveExaminationRoom(long id)
        {
            return await Task.Run(()=> {

                var i = DbContext.FreeSql.Delete<ExaminationSiteDetails>().Where(a => a.Id == id).ExecuteAffrows();
                if (i == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该考场!" };

                //删除对应考场证书记录
                DbContext.FreeSql.Delete<CertificateDetails>().Where(a => a.ExaminationRoomId == id).ExecuteAffrows();

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = "成功" };

            });


        }
        #endregion

        #region 教材管理（后台）
        /// <summary>
        /// 添加 更新 教材
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> AddUpdateTextBook(TextBookDto model, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID

                if (model.Id != 0)
                {
                    //修改
                    var textBook = DbContext.FreeSql.Select<TextBook>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (textBook == null)
                        return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

                    var entity = new TextBook();
                    if (textBook.Module!=model.Module||textBook.Grade!=model.Grade||textBook.Type!=model.Type)
                    {
                        entity = DbContext.FreeSql.Select<TextBook>().Where(m => m.Module == model.Module && m.Grade == model.Grade && m.Type==model.Type && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (entity != null)
                            return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "已有该证书和等级，修改失败!" };

                        if (model.Grade == 0)
                        {
                            for (int j = 1; j < 4; j++)
                            {
                                entity = DbContext.FreeSql.Select<TextBook>().Where(m => m.Module == model.Module && m.Grade == j && m.Type == model.Type && m.IsDelete == CommonConstants.IsNotDelete).First();
                                if (entity != null)
                                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "已有该证书和等级，保存失败!" };
                            }
                        }

                        if (model.Grade != 0)
                        {
                            entity = DbContext.FreeSql.Select<TextBook>().Where(m => m.Module == model.Module && m.Grade == 0 && m.Type == model.Type && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (entity != null)
                                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "已有该证书和等级，保存失败!" };
                        }
                    }

                    textBook.TextbookName = model.TextbookName;
                    textBook.Module = model.Module;
                    textBook.Grade = model.Grade;
                    textBook.State = model.State;
                    textBook.Type = model.Type;
                    textBook.UpdateBy = opreationId;
                    textBook.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<TextBook>().SetSource(textBook).UpdateColumns(m => new { m.TextbookName, m.Module, m.Grade, m.State,m.Type, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    if (i == 0)
                        return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = model.Id, Msg = "修改失败" };
                }
                else
                {


                    var entity = DbContext.FreeSql.Select<TextBook>().Where(m => m.Module == model.Module && m.Grade == model.Grade && m.Type == model.Type && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (entity != null)
                        return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "已有该证书和等级，保存失败!" };

                    if (model.Grade == 0)
                    {
                        for (int i = 1; i < 4; i++)
                        {
                            entity = DbContext.FreeSql.Select<TextBook>().Where(m => m.Module == model.Module && m.Grade == i && m.Type == model.Type && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (entity != null)
                                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "已有该证书和等级，保存失败!" };
                        }
                    }

                    if (model.Grade != 0)
                    {
                            entity = DbContext.FreeSql.Select<TextBook>().Where(m => m.Module == model.Module && m.Grade == 0 && m.Type == model.Type && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (entity != null)
                                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "已有该证书和等级，保存失败!" };
                    }


                    //保存
                    var textBook = new TextBook
                    {
                        Id = IdWorker.NextId(),
                        TextbookName = model.TextbookName,
                        Module = model.Module,
                        Grade = model.Grade,
                        State = model.State,
                        Type=model.Type,
                        CreateBy = opreationId,
                        CreateTime = DateTime.Now
                    };
                    textBook = DbContext.FreeSql.GetRepository<TextBook>().Insert(textBook);
                    if (textBook == null)
                        return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = model.Id, Msg = "保存失败" };
                    else
                        model.Id = textBook.Id;

                }

                return new ResponseContext<long> { Code = CommonConstants.SuccessCode, Data = model.Id };
            });
        }

        /// <summary>
        /// 添加 更新 章节
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<TextBookDetails>>> AddUpdateChapter(TextBookChapterDto model, long Id)
        {

            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID

                var items =new List<TextBookDetails>();
                if (model.TextBookDetails[0].Id != 0)
                {
                    //修改
                    for (var a = 0; a < model.TextBookDetails.Count; a++) 
                        items.Add(new TextBookDetails { Id = model.TextBookDetails[a].Id, ParentId = model.TextBookDetails[a].ParentId, ChapterName = model.TextBookDetails[a].ChapterName, Sort = model.TextBookDetails[a].Sort, TextbookContent = model.TextBookDetails[a].TextbookContent, UploadFiles = model.TextBookDetails[a].UploadFiles, TextbookId = model.TextBookDetails[a].TextbookId });

                    //前端要创建时间排序（查出在赋值）
                    var obj = DbContext.FreeSql.Select<TextBookDetails>().Where(m => m.Id == model.TextBookDetails[0].Id).First();
                    items[0].CreateTime = obj.CreateTime;

                    var ii = DbContext.FreeSql.GetRepository<TextBookDetails>().UpdateDiy.SetSource(items).ExecuteAffrows();
                    if (ii == 0)
                        return new ResponseContext<List<TextBookDetails>> { Code = CommonConstants.ErrorCode, Data = items, Msg = "修改章失败" };

                    //items = model.TextBookDetails;
                    //var ii = DbContext.FreeSql.GetRepository<TextBookDetails>().UpdateDiy.SetSource(items).ExecuteAffrows();
                    //if (ii == 0)
                    //    return new ResponseContext<List<TextBookDetails>> { Code = CommonConstants.ErrorCode, Data = items, Msg = "修改章失败" };
                }
                else
                {
                    for (var a = 0; a < model.TextBookDetails.Count; a++) 
                        items.Add(new TextBookDetails { Id = IdWorker.NextId(), ParentId = model.TextBookDetails[a].ParentId, ChapterName = model.TextBookDetails[a].ChapterName, Sort = model.TextBookDetails[a].Sort, TextbookContent = model.TextBookDetails[a].TextbookContent, UploadFiles = model.TextBookDetails[a].UploadFiles, TextbookId = model.TextBookDetails[a].TextbookId });
                    var textBookDetails = DbContext.FreeSql.GetRepository<TextBookDetails>().Insert(items);
                    if (textBookDetails == null)
                        return new ResponseContext<List<TextBookDetails>> { Code = CommonConstants.ErrorCode, Data = items, Msg = "保存章失败" };


                    //items = model.TextBookDetails;
                    //items.ForEach(x => { x.Id = IdWorker.NextId(); x.TextbookId = items[0].TextbookId; });
                    //var textBookDetails = DbContext.FreeSql.GetRepository<TextBookDetails>().Insert(items);
                    //if (textBookDetails == null)
                    //    return new ResponseContext<List<TextBookDetails>> { Code = CommonConstants.ErrorCode, Data = items, Msg = "保存章失败" };
                }

                return new ResponseContext<List<TextBookDetails>> { Code = CommonConstants.SuccessCode, Data = items };
            });
        }

        /// <summary>
        /// 查找 教材
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<TextBookResponse>> FindTextBook(TextBookRequest model)
        {
            return await Task.Run(() =>
            {

                var selectData = DbContext.FreeSql.GetRepository<TextBook>().Where(m => m.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.TextbookName), x => x.TextbookName.Contains(model.TextbookName))
                .WhereIf(model.Module > 0, x => x.Module == model.Module)
                .WhereIf(model.Grade != null, x => x.Grade == model.Grade)
                .WhereIf(model.State != -1, x => x.State == model.State)
                .OrderByDescending(m => m.CreateTime);
                var totalCount = selectData.Count();
                var sql = selectData.Page(model.PageIndex, model.PageSize).ToSql("Id,TextbookName,Module,Grade,State,Type");
                var items = DbContext.FreeSql.Ado.Query<TextBookResponse>(sql).Select(x => new TextBookResponse
                {
                    Id = x.Id,
                    TextbookName = x.TextbookName,
                    Module = x.Module,
                    Grade = x.Grade,
                    State = x.State,
                    Type=x.Type
                }).ToList();

                return new PageResponse<TextBookResponse> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };

            });
        }

        /// <summary>
        /// 编辑 预览教材 
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TextBookPreviewResponse>> FindTextBookPreview(long Id)
        {
            return await Task.Run(() =>
            {

                //查出教材
                var obj = new TextBookPreviewResponse();
                var sql = DbContext.FreeSql.Select<TextBook>().Where(m => m.Id == Id && m.IsDelete == CommonConstants.IsNotDelete).ToSql();
                var TextBook = DbContext.FreeSql.Ado.Query<TextBookPreviewResponse>(sql).FirstOrDefault();
                if (TextBook == null)
                    return new ResponseContext<TextBookPreviewResponse> { Code = CommonConstants.ErrorCode, Msg = "查找失败，找不到原始信息!" };
                obj = TextBook;

                //查出章 一级
                var sql1 = DbContext.FreeSql.Select<TextBookDetails>().Where(m => m.TextbookId == Id&&m.ParentId==0).OrderBy(m => m.CreateTime).ToSql();
                var list = DbContext.FreeSql.Ado.Query<TextBookDetailsResponse>(sql1);
                obj.TextBookDetails = list;

                var list1 = new List<TextBookDetailsResponse1>();
                for (int i=0;i< list.Count;i++)
                {
                    //查出节 二级
                    var sql2 = DbContext.FreeSql.Select<TextBookDetails>().Where(m => m.ParentId == obj.TextBookDetails[i].Id).OrderBy(m => m.CreateTime).ToSql();
                    list1 = DbContext.FreeSql.Ado.Query<TextBookDetailsResponse1>(sql2);
                    obj.TextBookDetails[i].TextBookDetails = list1;
                    //查出节 三级
                    for (int j = 0; j < list1.Count; j++)
                    {
                        var sql3 = DbContext.FreeSql.Select<TextBookDetails>().Where(m => m.ParentId == list1[j].Id).OrderBy(m => m.CreateTime).ToSql();
                        var list2 = DbContext.FreeSql.Ado.Query<TextBookDetailsResponse1>(sql3);
                        list1[j].TextBookDetails = list2;
                    }
                }

                return new ResponseContext<TextBookPreviewResponse> { Code = CommonConstants.SuccessCode, Data = obj };

            });
        }

        /// <summary>
        /// 删除 教材
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveTextBook(long id, long currentUserId)
        {
            TextBook entity = await DbContext.FreeSql.GetRepository<TextBook>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该教材!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<TextBook>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }

        /// <summary>
        /// 删除 章节
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveChapter(long id, long currentUserId)
        {
            var i = DbContext.FreeSql.Delete<TextBookDetails>().Where(a => a.Id == id).ExecuteAffrows();
            if (i == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该章节!" };

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = "成功" };
        }
        #endregion

        #region 教材（前台）
        /// <summary>
        /// 根据下拉模块带出数据
        /// </summary>
        /// <param name="userTicket"></param>
        public async Task<ResponseContext<List<TextBooksAllModuleDto>>>  TeachersAllClasses(int Module)
        {
            return await Task.Run(() =>
            {
                string str = string.Empty;
                str = Module != 0 ? $"Module={Module} AND" : "";
                var items = DbContext.FreeSql.Ado.Query<TextBooksAllModuleDto>($"SELECT Module,Grade,Type FROM 1x_textbook WHERE {str} State=0  AND IsDelete=0");
                return new ResponseContext<List<TextBooksAllModuleDto>> { Code = CommonConstants.SuccessCode, Data = items };
            });
        }

        /// <summary>
        /// 查找 预览教材
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TextBooksResponse>> FindTextBooks(TextBooksRequest model)
        {
            return await Task.Run(() =>
            {

                //查出教材
                var obj = new TextBooksResponse();
                var sql = DbContext.FreeSql.Select<TextBook>().Where(m => m.Module == model.Module && m.Grade == model.Grade && m.State == 0 && m.Type==model.Type && m.IsDelete == CommonConstants.IsNotDelete).ToSql();
                var TextBook = DbContext.FreeSql.Ado.Query<TextBooksResponse>(sql).FirstOrDefault();
                if (TextBook == null)
                {
                    var sql3 = DbContext.FreeSql.Select<TextBook>().Where(m => m.Module == model.Module && m.Grade == 0 && m.Type == model.Type && m.IsDelete == CommonConstants.IsNotDelete).ToSql();
                    TextBook = DbContext.FreeSql.Ado.Query<TextBooksResponse>(sql3).FirstOrDefault();

                    if (TextBook == null)
                        return new ResponseContext<TextBooksResponse> { Code = CommonConstants.SuccessCode, Msg = "" };
                }
                obj = TextBook;

                //查出章 一级
                var sql1 = DbContext.FreeSql.Select<TextBookDetails>().Where(m => m.TextbookId == TextBook.Id && m.ParentId == 0).OrderBy(m => m.CreateTime).ToSql();//.OrderByDescending(m => m.Sort)
                var list = DbContext.FreeSql.Ado.Query<TextBookDetailsResponse>(sql1);
                obj.TextBookDetails = list;

                var list1 = new List<TextBookDetailsResponse1>();
                for (int i = 0; i < obj.TextBookDetails.Count; i++)
                {
                    //查出节  二级
                    var sql2 = DbContext.FreeSql.Select<TextBookDetails>().Where(m => m.ParentId == obj.TextBookDetails[i].Id).OrderBy(m => m.CreateTime).ToSql();
                    list1 = DbContext.FreeSql.Ado.Query<TextBookDetailsResponse1>(sql2);
                    obj.TextBookDetails[i].TextBookDetails = list1;
                    //查出节 三级
                    for (int j = 0; j < list1.Count; j++)
                    {
                        var sql3 = DbContext.FreeSql.Select<TextBookDetails>().Where(m => m.ParentId == list1[j].Id).OrderBy(m => m.CreateTime).ToSql();
                        var list2 = DbContext.FreeSql.Ado.Query<TextBookDetailsResponse1>(sql3);
                        list1[j].TextBookDetails = list2;


                        if (list1[j].UploadFiles==""||list1[j].UploadFiles==null)
                        {
                            if (list2.Count != 0)
                            {
                                obj.TextBookDetails[i].TextBookDetails[j].UploadFiles = list2[0].UploadFiles;
                            }
                        }

                    }
                }
                return new ResponseContext<TextBooksResponse> { Code = CommonConstants.SuccessCode, Data = obj };

            });
        }
        #endregion

        #region 新闻管理(后台)
        /// <summary>
        /// 添加 更新 新闻
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateNews(NewsDto model, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID

                if (model.Id > 0)
                {
                    //修改
                    var textBook = DbContext.FreeSql.Select<News>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (textBook == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

                    textBook.Title = model.Title;
                    textBook.NewsContent = model.NewsContent;
                    textBook.Sort = model.Sort;
                    textBook.State = model.State;
                    textBook.UpdateBy = opreationId;
                    textBook.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<News>().SetSource(textBook).UpdateColumns(m => new { m.Title, m.NewsContent, m.Sort, m.State, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    if (i == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                }
                else
                {
                    //保存
                    var newsBook = new News
                    {
                        Id = IdWorker.NextId(),
                        Title = model.Title,
                        NewsContent = model.NewsContent,
                        Sort = model.Sort,
                        State = model.State,
                        CreateBy = opreationId,
                        CreateTime = DateTime.Now
                    };
                    newsBook = DbContext.FreeSql.GetRepository<News>().Insert(newsBook);
                    if (newsBook == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "保存失败" };

                }

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 查找 新闻
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<NewsResponse>> FindNews(NewsRequest model)
        {
            return await Task.Run(() =>
            {

                var selectData = DbContext.FreeSql.GetRepository<News>().Where(m => m.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Title), x => x.Title.Contains(model.Title))
                .WhereIf(model.Id > 0, x => x.Id == model.Id)
                .OrderByDescending(m => m.Sort);
                var totalCount = selectData.Count();
                var sql = selectData.Page(model.PageIndex, model.PageSize).ToSql("Id,Title,NewsContent,Sort,State");
                var items = DbContext.FreeSql.Ado.Query<NewsResponse>(sql).Select(x => new NewsResponse
                {
                    Id = x.Id,
                    Title = x.Title,
                    NewsContent = x.NewsContent,
                    Sort = x.Sort,
                    State = x.State,
                }).ToList();

                return new PageResponse<NewsResponse> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };

            });
        }

        /// <summary>
        /// 删除 新闻
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveNews(long id, long currentUserId)
        {
            News entity = await DbContext.FreeSql.GetRepository<News>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该教材!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<News>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }
        #endregion

        #region 新闻（前台）
        /// <summary>
        /// 查找 新闻
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<NewsHomeResponse>>> FindNewsHome()
        {
            return await Task.Run(() =>
            {
                var sql = DbContext.FreeSql.Select<News>().Where(m => m.State == 1 && m.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(o=>o.Sort).ToSql();
                var news = DbContext.FreeSql.Ado.Query<NewsHomeResponse>(sql);
                    if (news == null)
                        return new ResponseContext<List<NewsHomeResponse>> { Code = CommonConstants.SuccessCode, Msg = "" };

                return new ResponseContext<List<NewsHomeResponse>> { Code = CommonConstants.SuccessCode, Data = news };

            });
        }

        /// <summary>
        /// 预览 新闻详情
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<NewsHomeResponse>>> FindNewsDetails()
        {
            return await Task.Run(() =>
            {
                var sql = DbContext.FreeSql.Select<News>().Where(m => m.State == 1 && m.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(o => o.Sort).ToSql();
                var news = DbContext.FreeSql.Ado.Query<NewsHomeResponse>(sql);
                if (news == null)
                    return new ResponseContext<List<NewsHomeResponse>> { Code = CommonConstants.SuccessCode, Msg = "" };

                return new ResponseContext<List<NewsHomeResponse>> { Code = CommonConstants.SuccessCode, Data = news };

            });
        }
        #endregion
    }
}
