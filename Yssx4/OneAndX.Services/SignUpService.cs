﻿using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Redis;
using Yssx.Framework.Utils;

namespace OneAndX.Services
{
    /// <summary>
    /// 考试报名
    /// </summary>
    public class SignUpService : ISignUpService
    {
        #region 报名（前台）
        /// <summary>
        /// 添加 更新 报名 （考证报名 模块）
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateSignUp(SignUpDto model, long currentUserId)
        {
            #region 参数校验
            var examPlan = await DbContext.FreeSql.Select<ExamPlan>()
                .Where(m => m.Id == model.ExamPlanId && m.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync();

            if (examPlan == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "考试计划不存在。" };
            }
            var datetime = DateTime.Now.Date;// DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            if (examPlan.ApplyBeginTime.Date > datetime || examPlan.ApplyEndTime.Date < datetime)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "无法报名，报名未开始或报名已结束。" };
            }

            #endregion

            if (model.Id != 0) //修改
            {
                if (currentUserId == 0)
                {
                    currentUserId = model.Id;
                }
                //修改
                var signUp = await DbContext.FreeSql.Select<SignUp>()
                    .Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync();

                if (signUp == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

                if (!string.IsNullOrWhiteSpace(signUp.ExamCardNumber))
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "准考证已生成，不能在修改报名信息!" };
                }

                #region 赋值

                signUp.Type = model.Type;
                signUp.Certificate = examPlan.Certificate;
                signUp.CertificateLevel = examPlan.CertificateLevel;
                signUp.ExamTime = model.ExamTime;
                signUp.Name = model.Name;
                signUp.Sex = model.Sex;
                signUp.DocumentType = model.DocumentType;
                signUp.IDNumber = model.IDNumber;
                signUp.Province = model.Province;
                signUp.City = model.City;
                signUp.Area = model.Area;
                signUp.Education = model.Education;
                signUp.School = model.School;
                signUp.Major = model.Major;
                signUp.FrontIDCard = model.FrontIDCard;
                signUp.ReverseSideIDCard = model.ReverseSideIDCard;
                signUp.Photo = model.Photo;
                signUp.Phone = model.Phone;
                signUp.UpdateBy = currentUserId;
                signUp.UpdateTime = DateTime.Now;
                signUp.IndustryName = model.IndustryName;
                signUp.CompanyName = model.CompanyName;
                signUp.PositionName = model.PositionName;

                #endregion

                #region 数据库修改

                var i = await DbContext.FreeSql.Update<SignUp>().SetSource(signUp).UpdateColumns(m =>
                 new
                 {
                     m.Type,
                     m.Certificate,
                     m.CertificateLevel,
                     m.ExamTime,
                     m.Name,
                     m.Sex,
                     m.DocumentType,
                     m.IDNumber,
                     m.Province,
                     m.City,
                     m.Education,
                     m.School,
                     m.Area,
                     m.Major,
                     m.FrontIDCard,
                     m.ReverseSideIDCard,
                     m.Photo,
                     m.Phone,
                     m.UpdateBy,
                     m.UpdateTime,
                     m.IndustryName,
                     m.CompanyName,
                     m.PositionName,
                 }).ExecuteAffrowsAsync();
                if (i == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };

                #endregion

            }
            else//新增
            {
                #region 创建对象

                long tenantId = CommonConstants.OneXTenantId;
                var usertype = UserTypeEnums.OrdinaryUser;
                var password = CommonConstants.NewUserDefaultPwd.Md5();
                var expireDate = examPlan.EndTime.AddDays(30);
                var createTime = DateTime.Now;

                Guid clientId = Guid.NewGuid();

                var signUp = model.MapTo<SignUp>();
                signUp.Id = IdWorker.NextId();

                if (currentUserId == 0)
                {
                    currentUserId = model.Id;
                }

                signUp.CreateTime = createTime;
                signUp.CreateBy = currentUserId;
                signUp.Certificate = examPlan.Certificate;
                signUp.CertificateLevel = examPlan.CertificateLevel;
                signUp.TenantId = tenantId;
                signUp.ClientId = clientId;
                signUp.UserType = usertype;

                //竞赛
                var jsUser = new JsSkillCompetitionUser
                {
                    Id = signUp.Id,
                    BigDataPositionId = 0,
                    BigPositionName = "",
                    ClientId = clientId,
                    CreateTime = createTime,
                    ExpireDate = expireDate,
                    GroupNo = "",
                    IdNumber = signUp.IDNumber,
                    MobilePhone = "",
                    NikeName = signUp.Name,
                    Password = password,
                    Photo = signUp.Photo,
                    RealName = signUp.Name,
                    Sex = model.Sex,
                    TenantId = tenantId,
                    UserType = usertype,
                    Status = Status.Enable,
                };

                //云实训
                SxYssxUser sxUser = new SxYssxUser
                {
                    Id = signUp.Id,
                    MobilePhone = "",
                    UserName = signUp.Name,
                    NikeName = System.Web.HttpUtility.UrlEncode(signUp.Name, Encoding.UTF8),
                    RealName = signUp.Name,
                    Password = password,
                    UserType = signUp.UserType.GetHashCode(),
                    Status = 1,
                    UpdateTime = DateTime.Now,
                    CreateTime = DateTime.Now,
                    ClientId = clientId,
                    RegistrationType = 1,
                    TenantId = CommonConstants.OneXTenantId,
                };

                //var hysUser = new HysSkillCompetitionUser
                //{
                //    Id = signUp.Id,
                //    BigDataPositionId = 0,
                //    BigPositionName = "",
                //    ClientId = clientId,
                //    CreateTime = createTime,
                //    ExpireDate = expireDate,
                //    GroupNo = "",
                //    IdNumber = signUp.IDNumber,
                //    MobilePhone = signUp.Phone,
                //    NikeName = signUp.Name,
                //    Password = "",
                //    Photo = signUp.Photo,
                //    RealName = signUp.Name,
                //    Sex = model.Sex,
                //    TenantId = tenantId,
                //    UserType = usertype,
                //    Status = Status.Enable,
                //};

                #endregion

                #region 插入数据库

                int xCount = await DbContext.FreeSql.Insert(signUp).ExecuteAffrowsAsync();//1+X

                int hysCount = await DbContext.JSFreeSql.Insert(jsUser).ExecuteAffrowsAsync();//竞赛

                int sxCount = await DbContext.SxFreeSql.Insert(sxUser).ExecuteAffrowsAsync();//云实训

                //int jsCount = DbContext.HysFreeSql.Insert(hysUser).ExecuteAffrowsAsync();//行业赛

                if (xCount == 0 || hysCount == 0 || sxCount == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "保存失败" };

                #endregion
            }

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };

        }

        /// <summary>
        /// 根据Id 查询单条表明信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<SignUpDto>> GetInfo(long id)
        {

            var selectData = await DbContext.FreeSql.GetRepository<SignUp>()
                .Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Id == id)
                .FirstAsync<SignUpDto>();

            return new ResponseContext<SignUpDto> { Code = CommonConstants.SuccessCode, Data = selectData };

        }

        /// <summary>
        /// 查找 报名列表（不分页）（个人中心-我的报名）
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<SignUpDto>> FindSignUp(SignUpRequest model, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID

                var selectData = DbContext.FreeSql.GetRepository<SignUp>().Where(m => m.CreateBy == opreationId && m.IsDelete == CommonConstants.IsNotDelete)
                .OrderByDescending(m => m.CreateTime);
                var totalCount = selectData.Count();
                var sql = selectData.ToSql("Id");
                var items = DbContext.FreeSql.Ado.Query<SignUpDto>(sql).Select(x => new SignUpDto
                {
                    Id = x.Id,
                    Type = x.Type,
                    Certificate = x.Certificate,
                    CertificateLevel = x.CertificateLevel,
                    ExamPlanId = x.ExamPlanId,
                    ExamTime = x.ExamTime,
                    Name = x.Name,
                    Sex = x.Sex,
                    DocumentType = x.DocumentType,
                    IDNumber = x.IDNumber,
                    Province = x.Province,
                    City = x.City,
                    Area = x.Area,
                    Education = x.Education,
                    School = x.School,
                    Major = x.Major,
                    FrontIDCard = x.FrontIDCard,
                    ReverseSideIDCard = x.ReverseSideIDCard,
                    Photo = x.Photo
                }).ToList();

                return new PageResponse<SignUpDto> { Code = CommonConstants.SuccessCode, Data = items };

            });
        }

        /// <summary>
        /// 删除 报名（暂时用不到）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveSignUp(long id, long currentUserId)
        {
            SignUp entity = await DbContext.FreeSql.GetRepository<SignUp>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该报名!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<SignUp>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }

        /// <summary>
        /// 获取个人报名信息
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetSignUpListDto>>> GetSignUpList(long currentUserId)
        {
            var selectData = await DbContext.FreeSql.GetRepository<SignUp>()
                .Where(m => m.CreateBy == currentUserId && m.IsDelete == CommonConstants.IsNotDelete)
                .OrderByDescending(m => m.CreateTime)
                .ToListAsync<GetSignUpListDto>();

            return new ResponseContext<List<GetSignUpListDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
        }
        #endregion

        #region 获取报名可选省市区地点

        /// <summary>
        /// 根据考试计划Id获取对应的考点信息
        /// </summary>
        /// <param name="examPlanId"></param>
        public async Task<ResponseContext<List<ExamSiteProvinceDto>>> GetExamSiteByExamPlanId(long examPlanId)
        {
            var entitys = await DbContext.FreeSql.GetRepository<OneXPlanExamRoom>()
                .Where(a => a.ExamPlanId == examPlanId && a.IsDelete == CommonConstants.IsNotDelete)
                .From<ExaminationSite>((a, b) => a.InnerJoin(aa => aa.ExamSiteId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                .Distinct()
                .ToListAsync((a, b) => new ExamSiteBaseInfoDto
                {
                    Province = b.Province,
                    City = b.City,
                    Area = b.Area,
                });

            var data = new List<ExamSiteProvinceDto>();
            //构建省市区数据
            foreach (var item in entitys.GroupBy(e => e.Province))//省
            {
                var cs = item.ToList();
                var citys = new List<ExamSiteCityDto>();
                foreach (var area in cs.GroupBy(b => b.City))//市
                {
                    citys.Add(new ExamSiteCityDto
                    {
                        Province = item.Key,
                        City = area.Key,
                        Area = area.ToList()//区
                    });
                }
                data.Add(new ExamSiteProvinceDto
                {
                    Province = item.Key,
                    City = citys
                });

            }

            return new ResponseContext<List<ExamSiteProvinceDto>> { Code = CommonConstants.SuccessCode, Data = data };
        }

        #endregion

        #region 获取用户准考证信息
        /// <summary>
        /// 获取用户准考证信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserExamCardNumberViewModel>> GetUserExamCardNumberInfo(long userId)
        {
            ResponseContext<UserExamCardNumberViewModel> response = new ResponseContext<UserExamCardNumberViewModel>();

            var signUpData = await DbContext.FreeSql.Select<SignUp>().Where(x => x.Id == userId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (signUpData == null)
            {
                response.Msg = "用户不存在!";
                return response;
            }
            if (string.IsNullOrEmpty(signUpData.ExamCardNumber))
            {
                response.Msg = "准考证未生成!";
                return response;
            }

            //查询报名信息表
            var selectData = await DbContext.FreeSql.Select<SignUp>().From<ExaminationSite, ExaminationSiteDetails, ExamPlan>(
                (a, b, c, d) => a.LeftJoin(x => x.ExamSiteId == b.Id)
                .LeftJoin(x => x.ExamRoomId == c.Id)
                .LeftJoin(x => x.ExamPlanId == d.Id))
                 .Where((a, b, c, d) => a.Id == userId && a.IsDelete == CommonConstants.IsNotDelete)
                 .ToListAsync((a, b, c, d) => new UserExamCardNumberViewModel
                 {
                     Name = a.Name,
                     Sex = a.Sex,
                     IDNumber = a.IDNumber,
                     Photo = a.Photo,
                     Phone = a.Phone,
                     ExamCardNumber = a.ExamCardNumber,
                     ExamPassword = a.ExamPassword,
                     ExamSiteName = b.ExaminationSiteName,
                     ExaminationSiteCode = b.ExaminationSiteCode,
                     SpecificAddress = b.SpecificAddress,
                     ExamRoomName = c.ExaminationRoomName,
                     SeatNumber = a.SeatNumber,
                     ExamPlanId = a.ExamPlanId,
                     Certificate = d.Certificate,
                     CertificateLevel = d.CertificateLevel,
                     ExamTime = d.BeginTime,
                     EndTime = d.EndTime,
                 });

            response.Data = selectData.FirstOrDefault();

            return response;
        }
        #endregion

    }
}
