﻿using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;

namespace OneAndX.Services
{
    /// <summary>
    /// 历届考试
    /// </summary>
    public class PreviouExamService : IPreviouExamService
    {
        #region 历届考试
        /// <summary>
        /// 获取历届考试列表
        /// </summary>
        public async Task<PageResponse<SavePreviouExamListDto>> GetPreviouExamList(GetPreviouExamListRequest model)
        {
            return await Task.Run(() =>
            {
                long totalCount = 0;
                var select = DbContext.FreeSql.GetRepository<OneXPreviouExam>().Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                    .WhereIf(model.PreviouExamType != PreviouExamType.DEFAULT, x => x.PreviouExamType == model.PreviouExamType)
                    .WhereIf(model.BeginTime.HasValue, x => x.BeginTime >= model.BeginTime)
                    .WhereIf(model.EndTime.HasValue, x => x.EndTime <= model.EndTime)
                    .Count(out totalCount)
                    .OrderBy(x => x.Sort)
                    .Page(model.PageIndex, model.PageSize);
                var selectData = select.ToList<SavePreviouExamListDto>();
                foreach (var item in selectData)
                {
                    //报名人数
                    item.ApplyQty = (int)DbContext.FreeSql.GetRepository<OneXPreviouExamUser>().Where(x => x.ExamId == item.Id && x.IsDelete == CommonConstants.IsNotDelete).Count();
                    //花絮
                    var rTidbitDetail = DbContext.FreeSql.GetRepository<OneXPreviouExamTidbit>().Where(x => x.ExamId == item.Id)
                        .OrderBy(x => x.Sort).Select(x => new GetPreviouExamTidbitDto
                        {
                            Id = x.Id,
                            Description = x.Description,
                            Url = x.Url,
                            Sort = x.Sort
                        }).ToList();
                    item.TidbitDetail = rTidbitDetail;
                    //培训资料
                    var rFileDetail = DbContext.FreeSql.GetRepository<OneXPreviouExamFile>().Where(x => x.ExamId == item.Id)
                        .OrderBy(x => x.Sort).Select(x => new GetPreviouExamFileDto
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Url = x.Url,
                            Sort = x.Sort
                        }).ToList();
                    item.FileDetail = rFileDetail;
                }

                return new PageResponse<SavePreviouExamListDto> { PageIndex = model.PageIndex, PageSize = model.PageSize, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 保存历届考试
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SavePreviouExam(SavePreviouExamListDto model)
        {
            DateTime dtNow = DateTime.Now;
            long currentUserId = -1;
            return await Task.Run(() =>
            {
                #region 校验
                //TODO 校验
                if (model.Id > 0)
                {
                    var rAnyEntity = DbContext.FreeSql.GetRepository<OneXPreviouExam>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rAnyEntity.Any())
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到历届考试信息" };
                }
                if (string.IsNullOrEmpty(model.Name))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
                if (model.PreviouExamType == PreviouExamType.DEFAULT)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择证书类型" };
                if (string.IsNullOrEmpty(model.Img))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请上传封面" };
                if (string.IsNullOrEmpty(model.XImg))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请上传证书样式" };
                if (model.Sort <= 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请填写序号" };
                var rAnyNameEntity = DbContext.FreeSql.GetRepository<OneXPreviouExam>().Where(m => m.Id != model.Id && m.Name == model.Name && m.IsDelete == CommonConstants.IsNotDelete);
                if (rAnyNameEntity.Any())
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已存在该历届考试" };
                #endregion

                var rPreviouExamEntity = new OneXPreviouExam
                {
                    Id = model.Id > 0 ? model.Id : IdWorker.NextId(),
                    Name = model.Name,
                    PreviouExamType = model.PreviouExamType,
                    IssueDate = model.IssueDate,
                    ApplyBeginTime = model.ApplyBeginTime,
                    ApplyEndTime = model.ApplyEndTime,
                    BeginTime = model.BeginTime,
                    EndTime = model.EndTime,
                    Img = model.Img,
                    Sort = model.Sort,
                    CreateBy = currentUserId,
                    CreateTime = dtNow,
                    XImg=model.XImg,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };

                //更新数据
                var state = false;
                if (model.Id > 0)
                    state = DbContext.FreeSql.Update<OneXPreviouExam>().SetSource(rPreviouExamEntity).IgnoreColumns(m => new { m.CreateBy, m.CreateTime }).ExecuteAffrows() > 0;
                else
                    state = DbContext.FreeSql.Insert(rPreviouExamEntity).ExecuteAffrows() > 0;

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state };
            });
        }

        /// <summary>
        /// 删除历届考试
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeletePreviouExam(long id)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                long currentUserId = -1;
                //考试
                var entity = DbContext.FreeSql.GetRepository<OneXPreviouExam>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到历届考试数据!" };

                entity.IsDelete = CommonConstants.IsDelete;
                entity.UpdateTime = dtNow;
                entity.UpdateBy = currentUserId;
                //花絮明细
                var listTidbitDetail = DbContext.FreeSql.GetRepository<OneXPreviouExamTidbit>().Where(x => x.ExamId == id && x.IsDelete == CommonConstants.IsNotDelete).ToList(m => new OneXPreviouExamTidbit
                {
                    Id = m.Id,
                    IsDelete = CommonConstants.IsDelete,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                });
                //培训资料明细
                var listFileDetail = DbContext.FreeSql.GetRepository<OneXPreviouExamFile>().Where(x => x.ExamId == id && x.IsDelete == CommonConstants.IsNotDelete).ToList(m => new OneXPreviouExamFile
                {
                    Id = m.Id,
                    IsDelete = CommonConstants.IsDelete,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                });

                bool state = false;
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //考试
                    state = DbContext.FreeSql.Update<OneXPreviouExam>().SetSource(entity).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                    //花絮明细
                    if (state && listTidbitDetail.Count > 0)
                        DbContext.FreeSql.Update<OneXPreviouExamTidbit>().SetSource(listTidbitDetail).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                    //培训资料明细
                    if (state && listFileDetail.Count > 0)
                        DbContext.FreeSql.Update<OneXPreviouExamFile>().SetSource(listFileDetail).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();

                    tran.Commit();
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        #endregion


        #region 花絮

        /// <summary>
        /// 获取历届考试 - 花絮列表
        /// </summary>
        public async Task<PageResponse<GetPreviouExamTidbitDto>> GetPreviouExamTidbitList(long examId)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetRepository<OneXPreviouExamTidbit>().Where(x => x.ExamId == examId && x.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy(x => x.Sort).ToList(x => new GetPreviouExamTidbitDto
                    {
                        Id = x.Id,
                        Description = x.Description,
                        Url = x.Url,
                        Sort = x.Sort
                    });
                return new PageResponse<GetPreviouExamTidbitDto> { PageIndex = 1, PageSize = selectData.Count, RecordCount = selectData.Count, Data = selectData };
            });
        }

        /// <summary>
        /// 保存历届考试 - 花絮
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SavePreviouExamTidbit(SavePreviouExamTidbitDto model)
        {
            DateTime dtNow = DateTime.Now;
            long currentUserId = -1;
            return await Task.Run(() =>
            {
                #region 校验
                //TODO 校验
                if (model.Id > 0)
                {
                    var rAnyTidbitEntity = DbContext.FreeSql.GetRepository<OneXPreviouExamTidbit>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rAnyTidbitEntity.Any())
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到花絮信息" };
                }
                var rAnyEntity = DbContext.FreeSql.GetRepository<OneXPreviouExam>().Where(m => m.Id == model.ExamId && m.IsDelete == CommonConstants.IsNotDelete);
                if (!rAnyEntity.Any())
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到历届考试" };

                if (string.IsNullOrEmpty(model.Description))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，请填写描述" };
                if (string.IsNullOrEmpty(model.Url))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，请上传图片" };
                if (model.Sort <= 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，请填写序号" };
                #endregion

                var rSaveData = new OneXPreviouExamTidbit()
                {
                    Id = model.Id > 0 ? model.Id : IdWorker.NextId(),
                    ExamId = model.ExamId,
                    Description = model.Description,
                    Url = model.Url,
                    Sort = model.Sort,
                    CreateBy = currentUserId,
                    CreateTime = dtNow,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };

                //更新数据
                var state = false;
                if (model.Id > 0)
                    state = DbContext.FreeSql.Update<OneXPreviouExamTidbit>().SetSource(rSaveData).UpdateColumns(m => new { m.Description, m.Url, m.Sort, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                else
                    state = DbContext.FreeSql.Insert(rSaveData).ExecuteAffrows() > 0;

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state };
            });
        }

        /// <summary>
        /// 删除历届考试 - 花絮
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeletePreviouExamTidbit(long id, long examId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                long currentUserId = -1;

                var entity = DbContext.FreeSql.GetRepository<OneXPreviouExamTidbit>().Where(m => m.Id == id && m.ExamId == examId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到历届考试花絮数据!" };

                entity.IsDelete = CommonConstants.IsDelete;
                entity.UpdateTime = dtNow;
                entity.UpdateBy = currentUserId;

                //商品明细
                bool state = DbContext.FreeSql.Update<OneXPreviouExamTidbit>().SetSource(entity).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        #endregion


        #region 培训资料

        /// <summary>
        /// 获取历届考试 - 培训资料列表
        /// </summary>
        public async Task<PageResponse<GetPreviouExamFileDto>> GetPreviouExamFileList(long examId)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetRepository<OneXPreviouExamFile>().Where(x => x.ExamId == examId && x.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy(x => x.Sort).ToList(x => new GetPreviouExamFileDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Url = x.Url,
                        Sort = x.Sort
                    });
                return new PageResponse<GetPreviouExamFileDto> { PageIndex = 1, PageSize = selectData.Count, RecordCount = selectData.Count, Data = selectData };
            });
        }

        /// <summary>
        /// 保存历届考试 - 培训资料
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SavePreviouExamFile(SavePreviouExamFileDto model)
        {
            DateTime dtNow = DateTime.Now;
            long currentUserId = -1;
            return await Task.Run(() =>
            {
                #region 校验
                //TODO 校验
                var rAnyEntity = DbContext.FreeSql.GetRepository<OneXPreviouExam>().Where(m => m.Id == model.ExamId && m.IsDelete == CommonConstants.IsNotDelete);
                if (!rAnyEntity.Any())
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到历届考试" };

                if (string.IsNullOrEmpty(model.Name))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，文件名称为空" };
                if (string.IsNullOrEmpty(model.Url))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，文件地址为空" };
                if (model.Sort <= 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，请填写序号" };
                #endregion

                var rSaveData = new OneXPreviouExamFile()
                {
                    Id = model.Id > 0 ? model.Id : IdWorker.NextId(),
                    ExamId = model.ExamId,
                    Name = model.Name,
                    Url = model.Url,
                    Sort = model.Sort,
                    CreateBy = currentUserId,
                    CreateTime = dtNow,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };
                //更新数据
                var state = false;
                if (model.Id > 0)
                    state = DbContext.FreeSql.Update<OneXPreviouExamFile>().SetSource(rSaveData).UpdateColumns(m => new { m.Sort, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                else
                    state = DbContext.FreeSql.Insert(rSaveData).ExecuteAffrows() > 0;

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state };
            });
        }

        /// <summary>
        /// 删除历届考试 - 培训资料
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeletePreviouExamFile(long id, long examId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                long currentUserId = -1;

                var entity = DbContext.FreeSql.GetRepository<OneXPreviouExamFile>().Where(m => m.Id == id && m.ExamId == examId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到历届考试培训资料数据!" };

                entity.IsDelete = CommonConstants.IsDelete;
                entity.UpdateTime = dtNow;
                entity.UpdateBy = currentUserId;

                //商品明细
                bool state = DbContext.FreeSql.Update<OneXPreviouExamFile>().SetSource(entity).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        #endregion


        #region 报名用户和证书

        /// <summary>
        /// 获取历届考试 - 报名用户列表
        /// </summary>
        public async Task<PageResponse<GetPreviouExamUserListDto>> GetPreviouExamUserList(GetPreviouExamUserListRequest model)
        {
            return await Task.Run(() =>
            {
                long totalCount = 0;
                var select = DbContext.FreeSql.GetRepository<OneXPreviouExamUser>()
                    .Where(x => x.ExamId == model.ExamId && x.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy(x => x.CreateTime)
                    .Count(out totalCount)
                    .Page(model.PageIndex, model.PageSize);
                var selectData = select.ToList<GetPreviouExamUserListDto>();

                return new PageResponse<GetPreviouExamUserListDto> { PageIndex = model.PageIndex, PageSize = model.PageSize, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 获取历届考试 - 证书列表
        /// </summary>
        public async Task<PageResponse<GetPreviouExamCertificateListDto>> GetPreviouExamCertificateList(GetPreviouExamUserListRequest model)
        {
            return await Task.Run(() =>
            {
                long totalCount = 0;
                //考试
                var entity = DbContext.FreeSql.GetRepository<OneXPreviouExam>().Where(m => m.Id == model.ExamId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new PageResponse<GetPreviouExamCertificateListDto> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到历届考试数据!" };

                var select = DbContext.FreeSql.GetRepository<OneXPreviouExamUser>()
                    .Where(x => x.ExamId == model.ExamId && x.IsIssueCertificate && x.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy(x => x.Ranking)
                    .Count(out totalCount)
                    .Page(model.PageIndex, model.PageSize);
                var selectData = select.ToList<GetPreviouExamCertificateListDto>();
                //查询证书类型
                foreach (var item in selectData)
                {
                    item.IssueDate = entity.IssueDate;
                    item.PreviouExamType = entity.PreviouExamType;
                }

                return new PageResponse<GetPreviouExamCertificateListDto> { PageIndex = model.PageIndex, PageSize = model.PageSize, RecordCount = totalCount, Data = selectData };
            });
        }

        #endregion





    }
}
