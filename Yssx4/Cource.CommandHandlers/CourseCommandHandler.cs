﻿using ECommon.Components;
using ENode.Commanding;
using Sx.Course.Commands;
using Sx.Course.Domain.Models;
using Sx.Course.Domain.Services;
using System.Threading.Tasks;

namespace Sx.Course.CommandHandlers
{
    [Component]
    public class CourseCommandHandler :
        ICommandHandler<CreateCourse>,
        ICommandHandler<UpdateCourse>,
        ICommandHandler<EditCourseStatus>,
        ICommandHandler<CourseAuditStatusEdit>
    {
        private readonly RegistCourseSubjectService _courseSubjectService;
        public CourseCommandHandler(RegistCourseSubjectService courseSubjectService)
        {
            _courseSubjectService = courseSubjectService;
        }

        /// <summary>
        /// 新增课程
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public Task HandleAsync(ICommandContext context, CreateCourse command)
        {
            return Task.Factory.StartNew(() =>
            {
                _courseSubjectService.AddCourseSubject(command.CourseId);
                context.Add(new SxCourse(command.AggregateRootId, new CourseInfo(
                command.CourseId,
                command.CourseName,
                command.CourseTitle,
                command.CourseTypeId,
                command.KnowledgePointId,
                command.Education,
                command.Author,
                command.PublishingHouse,
                command.PublishingDate,
                command.Images,
                command.TenantId,
                command.CreateBy)));
            });
        }

        /// <summary>
        /// 修改课程
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public async Task HandleAsync(ICommandContext context, UpdateCourse command)
        {
            var course = await context.GetAsync<SxCourse>(command.AggregateRootId);
            if (course == null)
            {
                await context.AddAsync(new SxCourse(command.CourseId, new CourseEditInfo(
                    command.CourseId,
                    command.CourseName,
                    command.CourseTitle,
                    command.CourseTypeId,
                    command.KnowledgePointId,
                    command.Education,
                    command.Author,
                    command.PublishingHouse,
                    command.PublishingDate,
                    command.Images)));
            }
            else
            {
                course.Update(new CourseEditInfo(
                    command.CourseId,
                    command.CourseName,
                    command.CourseTitle,
                    command.CourseTypeId,
                    command.KnowledgePointId,
                    command.Education,
                    command.Author,
                    command.PublishingHouse,
                    command.PublishingDate,
                    command.Images));
            }
        }

        /// <summary>
        /// 删除课程，修改删除状态
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public async Task HandleAsync(ICommandContext context, EditCourseStatus command)
        {
            var course = await context.GetAsync<SxCourse>(command.AggregateRootId);
            if (course == null)
            {

            }
            else
            {
                course.EditToStatus(command.CourseId);
            }
        }

        /// <summary>
        /// 审核课程
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public async Task HandleAsync(ICommandContext context, CourseAuditStatusEdit command)
        {
            var course = await context.GetAsync<SxCourse>(command.AggregateRootId);
            course.EditAuditStatus(new CourseEditAuditStatus(command.CourseId, command.AuditStatus, command.Description));
        }
    }
}
