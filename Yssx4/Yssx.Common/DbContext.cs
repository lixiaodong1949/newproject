﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Common
{
    public static class DbContext
    {
        /// <summary>
        /// 数据库上下文单例
        /// </summary>
        public static IFreeSql FreeSql { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="freeSql"></param>
        public static void RegisterFreeSql(IFreeSql freeSql)
        {
            FreeSql = freeSql;
        }

    }
}
