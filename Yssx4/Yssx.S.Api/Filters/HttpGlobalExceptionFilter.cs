﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using Yssx.Framework.Logger;

namespace Yssx.S.Api.Filters
{
    public class HttpGlobalExceptionFilter : IExceptionFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public void OnException(ExceptionContext context)
        {
            var ex = context.Exception;
            var msg = $"在执行 action[{context.ActionDescriptor.DisplayName}] 时捕捉到未处理的异常：{ex.GetType()}\nFilter已进行错误处理。";
            context.Result = new ContentResult()
            {
                Content = msg,
                ContentType = "application/json",
                StatusCode = (int)HttpStatusCode.InternalServerError
            };

            CommonLogger.Error(msg, ex);
        }
    }
}
