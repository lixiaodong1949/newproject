using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Text;
using Tas.Common.Configurations;
using Yssx.Framework;
using Yssx.Framework.Logger;
using Yssx.Framework.Web.Filter;
using Yssx.S.Api.Auth;
using Yssx.Swagger;
using NLog.Extensions.Logging;
using Yssx.Redis;
using Yssx.Framework.Dal;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Auth;
using AutoMapper;
using Yssx.Repository.Extensions;
using Yssx.Framework.IM;
using System;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;
using Yssx.S.IServices.Topics;
using Yssx.S.ServiceImpl.Topics;
using Yssx.S.IServices.Examination;
using Yssx.S.ServiceImpl.Examination;
using Yssx.S.IServices.Means;
using Yssx.S.ServiceImpl.Means;
using Yssx.S.IServices.ExamineAndVerify;
using Yssx.S.ServiceImpl.ExamineAndVerify;
using Yssx.M.IServices;
using Yssx.M.ServiceImpl;
using Yssx.S.IServices.SceneTraining;
using Yssx.S.ServiceImpl.SceneTraining;
using Yssx.S.IServices.BigData;
using Yssx.Framework.Payment.Webchat;
using Yssx.Framework.Payment.Alipay;
using Yssx.S.IServices.Order;
using Yssx.S.ServiceImpl.Order;
using Yssx.S.IServices.Mall;
using Yssx.S.ServiceImpl.Mall;
using Yssx.S.Service;
using MediatR;
using Zdap.MQ.Produce;

namespace Yssx.S.Api
{
    public class Startup
    {
        private CustsomSwaggerOptions CURRENT_SWAGGER_OPTIONS = new CustsomSwaggerOptions()
        {
            ProjectName = "云上实训",
            ApiVersions = new string[] { "v1" },//要显示的版本
            UseCustomIndex = false,
            RoutePrefix = "swagger",

            AddSwaggerGenAction = c =>
            {
                var xmlPaths = Directory.GetFiles(System.AppContext.BaseDirectory, "Yssx.*.xml").ToList();
                foreach (var path in xmlPaths)
                {
                    c.IncludeXmlComments(path, true);
                }
            },
            UseSwaggerAction = c =>
            {

            },
            UseSwaggerUIAction = c =>
            {
            }
        };
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        private void AddTokenValidation(IServiceCollection services)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(CommonConstants.IdentServerSecret));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var permissionRequirement = new PermissionRequirement(signingCredentials);
            services.AddSingleton(permissionRequirement);
        }
        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddScoped<CustomExceptionFilter>();
            services.AddMvc(options =>
            {
                options.Filters.Add<ActionModelStateFilter>();
                options.Filters.Add<AuthorizationFilter>();

                //options.Filters.Add<Filters.HttpGlobalExceptionFilter>();


            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddJsonOptions(options =>
            {
                //忽略循环引用
                //options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                //options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                //设置时间格式
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();//json字符串大小写原样输出
            }).ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            //版本控制
            // services.AddMvcCore().AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'VVV");

            services.AddApiVersioning(option =>
            {
                // allow a client to call you without specifying an api version
                // since we haven't configured it otherwise, the assumed api version will be 1.0
                option.AssumeDefaultVersionWhenUnspecified = true;
                option.ReportApiVersions = false;
            })
            .AddCustomSwagger(CURRENT_SWAGGER_OPTIONS)
            .AddCors(options =>
            {
                options.AddPolicy(CommonConstants.AnyOrigin, builder =>
                {
                    builder.AllowAnyOrigin() //允许任何来源的主机访问                             
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();//指定处理cookie                                   
                });
            });

            services.AddMediatR(typeof(Program).Assembly);

            services.AddHttpClient();

            //services.AddMQProducerServices(Configuration);//消息队列，生产者

            //services.AddAutoMapper();
            AddTokenValidation(services);
            InitService(services);

        }

        /// <summary>
        /// 依赖注入业务服务
        /// </summary>
        /// <param name="services"></param>
        private void InitService(IServiceCollection services)
        {
            //yssxDI,服务注入
            services.AddTransient<ISigninService, SigninService>();//签到
            services.AddTransient<IUserAddressService, UserAddressService>();//用户收货地址
            services.AddTransient<IIntegralProductService, IntegralProductService>();//积分商品
            services.AddTransient<IIntegralService, IntegralService>();//用户积分
            services.AddTransient<IIntegralProductExchangeService, IntegralProductExchangeService>();//积分商品兑换
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<ISectionService, SectionService>();
            services.AddTransient<ICaseService, CaseService>();
            services.AddTransient<IExamPaperService, ExamPaperService>();
            services.AddTransient<IExamPaperGroupService, ExamPaperGroupService>();
            services.AddTransient<IImService, ImService>();
            services.AddTransient<IStudentExamService, StudentExamService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAuthenticateService, AuthenticateService>();
            services.AddTransient<IAuthenticateService, AuthenticateService>();
            services.AddTransient<ITemplateService, TemplateService>();
            services.AddTransient<IUploadCaseService, UploadCaseService>();
            services.AddTransient<ITopicService, TopicService>();
            services.AddTransient<ICertificateTopicService, CertificateTopicService>();
            services.AddTransient<IResourceSettingService, ResourceSettingService>();
            services.AddTransient<ITopicPublicService, TopicPublicService>();
            services.AddTransient<ICertificateTopicPublicService, CertificateTopicPublicService>();
            services.AddTransient<IActivateSchoolService, ActivateSchoolService>();
            services.AddTransient<ISummaryService, SummaryService>();
            services.AddTransient<ISubjectService, SubjectService>();
            services.AddTransient<ICourseSubjectService, CourseSubjectService>();
            services.AddTransient<ICourseSummaryService, CourseSummaryService>();
            services.AddTransient<ICourceFilesService, CourceFilesService>();
            services.AddTransient<IExamineAndVerifyService, ExamineAndVerifyService>();
            services.AddTransient<IStampService, StampService>();
            services.AddTransient<IbkUserService, bkUserService>();
            services.AddTransient<IPositionService, PositionService>();
            services.AddTransient<IDepartmentService, DepartmentService>();
            services.AddTransient<IPostPersonService, PostPersonService>();
            services.AddTransient<IHomePageService, HomePageService>();
            services.AddTransient<ISceneTrainingService, SceneTrainingService>();
            services.AddTransient<ISceneTrainingMallService, SceneTrainingMallService>();
            services.AddTransient<IPrepareCourseService, PrepareCourseService>();
            services.AddTransient<IPrepareSectionService, PrepareSectionService>();
            services.AddTransient<ISceneTrainingOrderService, SceneTrainingOrderService>();
            services.AddTransient<ISemesterService, SemesterService>();
            services.AddTransient<IPrepareCourseClassService, PrepareCourseClassService>();
            services.AddTransient<ICourseProductService, CourseProductService>();
            services.AddTransient<IUserExercisesService, UserExercisesService>();
            services.AddTransient<ITaskService, TaskService>();
            services.AddTransient<ICourseExercisesService, CourseExercisesService>();
            services.AddTransient<IAttendClassService, AttendClassService>();
            services.AddTransient<IJobTrainingTaskService, JobTrainingTaskService>();
            services.AddTransient<ICourseTestOnLineTaskService, CourseTestOnLineTaskService>();
            services.AddTransient<ISchoolService, SchoolService>();
            services.AddTransient<IResourceSettingService, ResourceSettingService>();
            services.AddTransient<IBigDataService, BigDataService>();
            services.AddTransient<IStudentService, StudentService>();
            services.AddTransient<IHomeService, HomeService>();
            services.AddTransient<IUserHomeService, UserHomeService>();
            services.AddTransient<ILessonPlanningService, LessonPlanningService>();
            services.AddTransient<IAppManageService, AppManageService>();
            services.AddTransient<ILessonPlanningClassService, LessonPlanningClassService>();
            services.AddTransient<IPlanningAttendClassService, PlanningAttendClassService>();
            services.AddTransient<IInternshipProveService, InternshipProveService>();
            services.AddTransient<IStatisticsStudentAutoTrainingService, StatisticsStudentAutoTrainingService>();
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<IPaymentServiceV2, PaymentServiceV2>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IProductCouponsService, ProductCouponsService>();
            services.AddHostedService<WeiDianHostedService>();
            services.AddTransient<IUserOrderService, UserOrderService>();
            services.AddTransient<IMallOrderService, MallOrderService>();
            services.AddTransient<IMallProductService, MallProductService>();
            services.AddTransient<ITaxCalculatorService, TaxCalculatorService>();
            services.AddTransient<INewsService, NewsService>();
            services.AddTransient<IIntegralService, IntegralService>();
            services.AddTransient<IAdvertisingSpaceService, AdvertisingSpaceService>();
            services.AddTransient<INaturalPersonsService, NaturalPersonsService>();
            services.AddTransient<ICoursePrepareService, CoursePrepareService>();
            services.AddTransient<ITeachCourseService, TeachCourseService>();
            services.AddTransient<IAppService, AppService>();
            services.AddTransient<ICourseTaskService, CourseTaskService>();


            #region 同步实训
            //tbsxDI,服务注入
            services.AddTransient<ICourseServiceNew, CourseServiceNew>();
            services.AddTransient<ISectionServiceNew, SectionServiceNew>();
            services.AddTransient<ITopicPublicServiceNew, TopicPublicServiceNew>();
            services.AddTransient<ICertificateTopicPublicServiceNew, CertificateTopicPublicServiceNew>();
            services.AddTransient<ISummaryServiceNew, SummaryServiceNew>();
            services.AddTransient<ICourseSummaryServiceNew, CourseSummaryServiceNew>();
            services.AddTransient<ISubjectServiceNew, SubjectServiceNew>();
            services.AddTransient<ICourseSubjectServiceNew, CourseSubjectServiceNew>();
            services.AddTransient<IResourceSettingServiceNew, ResourceSettingServiceNew>();
            services.AddTransient<ITemplateServiceNew, TemplateServiceNew>();
            services.AddTransient<IStampServiceNew, StampServiceNew>();
            services.AddTransient<ICourceFilesServiceNew, CourceFilesServiceNew>();
            services.AddTransient<ICaseServiceNew, CaseServiceNew>();
            services.AddTransient<ICaseDepartmentServiceNew, CaseDepartmentServiceNew>();
            services.AddTransient<IInternshipProveNewService, InternshipProveNewService>();
            services.AddTransient<IStudentExamServiceNew, StudentExamNewService>();
            services.AddTransient<IDrillTypeService, DrillTypeService>();
            services.AddTransient<IFlowService, FlowService>();
            services.AddTransient<ITopicServiceNew, TopicServiceNew>();
            services.AddTransient<IExamineAndVerifyServiceNew, ExamineAndVerifyServiceNew>();

            #endregion

            services.AddTransient<IUploadFileService, UploadFileService>();

        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime lifetime)
        {
            app.UseExceptionHandler(builder =>
            {
                builder.Run(async context =>
                {
                    context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                    context.Response.ContentType = "application/json";
                    var ex = context.Features.Get<Microsoft.AspNetCore.Diagnostics.IExceptionHandlerFeature>();
                    if (ex?.Error != null)
                    {
                        CommonLogger.Error("UseExceptionHandler", ex?.Error);
                    }
                    await context.Response.WriteAsync(ex?.Error?.Message ?? "an error occure");
                });
            }).UseHsts();
            //string enableSwagger = Configuration["swaggerEnable"];
            #region Consul
            //app.UseConsul(Configuration);//consul  服务注册
            #endregion
            app.UseMvc().UseCustomSwagger(CURRENT_SWAGGER_OPTIONS);
            //app.UseMvc();
            //if (enableSwagger == "1")
            //{
            //    app.UseCustomSwagger(CURRENT_SWAGGER_OPTIONS);
            //}
            app.UseCors(CommonConstants.AnyOrigin);
            app.UseStaticFiles();


            //string[] servers = Configuration["ImServers"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);//im服务器地址
            //ProjectConfiguration.GetInstance().
            //    RegisterConfiguration(Configuration).
            //    UseCommonLogger(loggerFactory.AddNLog()).
            //    UseRedis(Configuration["RedisConnection"]).
            //    UseFreeSqlRepository(Configuration["DbConnection"], Configuration["ReadDbConnection1"], Configuration["ReadDbConnection2"], Configuration["ReadDbConnection3"], Configuration["ReadDbConnection4"]).
            //    UseFreeSqlBDRepository(Configuration["DbBigDataConnection"], Configuration["BigDbConnection1"], Configuration["BigDbConnection2"], Configuration["BigDbConnection3"], Configuration["BigDbConnection4"]).
            //    //UseFreeSqlRepositoryOax(Configuration["OaxDbConnection"]).
            //    //UseFreeSqlBDRepository(Configuration["DbBigDataConnection"], Configuration["BigDbConnection1"], Configuration["BigDbConnection2"]).
            //    UseAutoMapper(Framework.Utils.RuntimeHelper.GetAllAssemblies())
            //    .UseWebchat(Configuration)
            //    .UseAlipay(Configuration);
            ////UseImClient(Configuration["ImRedisConnection"], servers);

            DataMergeHelper.Start();
        }
    }
}
