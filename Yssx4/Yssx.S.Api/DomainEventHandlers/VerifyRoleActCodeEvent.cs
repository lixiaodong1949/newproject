﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yssx.S.Api.DomainEventHandlers
{
    public class VerifyRoleActCodeEvent : INotification
    {
        public VerifyRoleActCodeEvent(long userid)
        {
            UserId = userid;
        }
        public long UserId { get; set; }
    }
}
