﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 上课相关Api [Obsolete]
    /// </summary>
    public class AttendClassController : BaseController
    {
        IAttendClassService _service;

        public AttendClassController(IAttendClassService service)
        {
            _service = service;
        }

        #region 上课
        /// <summary>
        /// 上课
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> CreateAttendClassRecord(AttendClassDto dto)
        {
            return await _service.CreateAttendClassRecord(dto, this.CurrentUser);
        }
        #endregion

        #region 获取课堂记录
        /// <summary>
        /// 获取课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxClassRecordViewModel>>> GetClassRecordList([FromQuery]YssxClassRecordQuery query)
        {
            return await _service.GetClassRecordList(query, this.CurrentUser);
        }
        #endregion

        #region 删除课堂记录
        /// <summary>
        /// 删除课堂记录
        /// </summary>
        /// <param name="identifyCode"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveClassRecord(long identifyCode)
        {
            return await _service.RemoveClassRecord(identifyCode, this.CurrentUser);
        }
        #endregion

        #region 导出课堂记录
        /// <summary>
        /// 导出课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> ExportClassRecord([FromQuery]YssxClassRecordQuery query)
        {
            //return await _service.ExportClassRecord(query, this.CurrentUser);
            
            //查询课堂记录
            ResponseContext<List<YssxClassRecordViewModel>> sourceData = await _service.GetClassRecordList(query, this.CurrentUser);
            List<ExportYssxClassRecordViewModel> data = sourceData.Data.MapTo<List<ExportYssxClassRecordViewModel>>();

            byte[] ms = null;
            if (data != null && data.Count > 0)
                ms = NPOIHelper<ExportYssxClassRecordViewModel>.OutputExcel(data, new string[] { "课堂记录" });
            else
                return NotFound();

            return File(ms, "application/ms-excel", "课堂记录.xlsx");
        }
        #endregion


    }
}