﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.ServiceImpl;
using Yssx.S.ServiceImpl.Web;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 验证码
    /// </summary>
    public class VerificationCodeController : BaseController
    {
        private IRegisteredService _service;

        public VerificationCodeController()
        {
            _service = new RegisteredService();
        }
        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <param name="phone">电话号码</param>
        /// <param name="type">找回密码传1，更换手机传2，其他时候忽略此参数</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ResponseContext<bool> GetVerifyCode(string phone, int? type)
        {
            //bool b = DysmsService.SendSms(phone);
            //return new ResponseContext<bool> { Code = b ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = b };
            return DysmsService.SendSms(phone, type);
        }

        /// <summary>
        /// 校验验证码
        /// </summary>
        /// <param name="mb">电话号码</param>
        /// <param name="verificationCode">短信发送的验证码</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> CheckVerificationCode(string mb, string verificationCode)
        {
            return await _service.CheckVerificationCode(mb, verificationCode);
        }

        /// <summary>
        /// 验证手机号码
        /// </summary>
        /// <param name="mb">手机号码</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> VerifyPhoneNumber(string mb)
        {
            return await _service.VerifyPhoneNumber(mb);
        }

        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="mb">电话号码</param>
        /// <param name="password">密码</param>
        /// <param name="againPassword">再次输入的密码</param>
        /// <param name="nikename">昵称</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<YssxUser>> RegisteredUsers(string mb, string password, string againPassword, string nikename)
        {
            return await _service.RegisteredUsers(mb, password, againPassword, nikename);
        }

        /// <summary>
        /// 用户注册(学生身份) 通过教师邀请 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<YssxUser>> RegisterStudentByInvite(RegisterUserRequestModel dto)
        {
            return await _service.RegisterStudentByInvite(dto);
        }

    }
}