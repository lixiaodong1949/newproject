﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 场景实训-商城页面
    /// </summary>
    public class SceneTrainingMallController : BaseController
    {
        private ISceneTrainingMallService _service;

        /// <summary>
        /// 
        /// </summary>
        public SceneTrainingMallController(ISceneTrainingMallService service)
        {
            _service = service;
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<PageResponse<SceneTrainingMallResult>> Find(SceneTrainingMallDto model)
        {
            return await _service.Find(model);
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<SceneTrainingMallRequest>> Details(long Id)
        {
            return await _service.Details(Id);
        }

        /// <summary>
        ///  点赞
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> Fabulous(FabulousDao model)
        {
            return await _service.Fabulous(model, CurrentUser.Id);
        }
    }
}