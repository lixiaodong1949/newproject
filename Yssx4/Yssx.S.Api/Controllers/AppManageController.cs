﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.Dto.Order;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class AppManageController : BaseController
    {
        private readonly IAppManageService _service;

        public AppManageController(IAppManageService service)
        {
            _service = service;
        }

        /// <summary>
        /// 获取最新版本信息
        /// </summary>
        /// <param name="appid">appId：1 云上实训</param>
        /// <param name="type">类型：1 ISO，2 Android</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<AppNewVersionInfoDto>> NewVersionInfo(AppIdEnum appid, int type)
        {
            return await _service.NewVersionInfo(appid, type);
        }

    }
}
