﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.IServices.Topics;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 岗位相关服务
    /// </summary>
    public class PositionController : BaseController
    {
        private IPositionService _positionService;
        public PositionController(IPositionService positionService)
        {
            _positionService = positionService;
        }
        /// <summary>
        /// 获取岗位列表（不分页）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<GetPositionListResponse>>> GetPositionList()
        {
            return await _positionService.GetPositionList();
        }

        /// <summary>
        /// 获取岗位列表（分页）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<PageResponse<GetPositionListResponse>>> GetPositionListByPage([FromQuery]GetPositionListRequest model)
        {
            return await _positionService.GetPositionListByPage(model);
        }

        /// <summary>
        /// 保存岗位
        /// </summary>
        /// <returns></returns> 
        [HttpPost]
        public async Task<ResponseContext<bool>> SavePosition(PositionRequest model)
        {
            return await _positionService.SavePosition(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除岗位
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeletePosition(long id)
        {
            return await _positionService.DeletePosition(id, CurrentUser.Id);
        }
    }
}