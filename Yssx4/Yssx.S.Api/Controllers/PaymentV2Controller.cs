﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Logger;
using Yssx.S.Dto.Mall;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 支付
    /// </summary>
    public class PaymentV2Controller : BaseController
    {
        private readonly IPaymentServiceV2 _paymentService;

        public PaymentV2Controller(IPaymentServiceV2 paymentService)
        {
            _paymentService = paymentService;
        }

        /// <summary>
        /// 支付宝支付
        /// </summary>
        /// <returns>支付表单</returns>
        [HttpPost]
        public async Task<ResponseContext<PayResponse>> Alipay(CreateMallOrderDto input)
        {
            return await _paymentService.Alipay(input, CurrentUser);
        }

        /// <summary>
        /// 支付宝手机支付
        /// </summary>
        /// <returns>支付表单</returns>
        [HttpPost]
        public async Task<ResponseContext<PayResponse>> AlipayWap(CreateMallOrderDto input)
        {
            return await _paymentService.AlipayWap(input, CurrentUser);
        }

        /// <summary>
        /// 支付宝云实习APP支付
        /// </summary>
        /// <returns>支付表单</returns>
        [HttpPost]
        public async Task<ResponseContext<PayResponse>> AlipayApp(CreateMallOrderDto input)
        {
            return await _paymentService.AlipayApp(input, 1, CurrentUser);
        }

        /// <summary>
        /// 支付宝云实习3d-APP支付
        /// </summary>
        /// <returns>支付表单</returns>
        [HttpPost]
        public async Task<ResponseContext<PayResponse>> AlipayApp3d(CreateMallOrderDto input)
        {
            return await _paymentService.AlipayApp(input, 2, CurrentUser);
        }

        /// <summary>
        /// 支付宝APP支付 - 待支付订单
        /// </summary>
        /// <returns>支付表单</returns>
        [HttpGet]
        public async Task<ResponseContext<PayResponse>> AlipayAppContinue(long id)
        {
            return await _paymentService.AlipayAppContinue(id, CurrentUser.Id);
        }

        /// <summary>
        /// 支付宝异步回调
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public void NotifyAliapy()
        {
            Dictionary<string, string> input = new Dictionary<string, string>();
            foreach (var key in Request.Form.Keys)
            {
                var value = Request.Form[key];
                input.Add(key, value);
            }
            CommonLogger.Error("支付宝回调：" + JsonConvert.SerializeObject(input));
            _paymentService.AlipayNotify(input, 1);
        }

        /// <summary>
        /// 支付宝App异步回调
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public void NotifyAliapyApp()
        {
            Dictionary<string, string> input = new Dictionary<string, string>();
            foreach (var key in Request.Form.Keys)
            {
                var value = Request.Form[key];
                input.Add(key, value);
            }
            CommonLogger.Error("支付宝App回调：" + JsonConvert.SerializeObject(input));
            _paymentService.AlipayNotify(input, 2);
        }

        /// <summary>
        /// 支付宝3d-App异步回调
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public void NotifyAliapyApp3d()
        {
            //key
            CommonLogger.Error("支付宝3dApp回调1：" + Request.Form.Keys);
            //原始数据
            CommonLogger.Error("支付宝3dApp回调2：" + Request.Body.ToString());
            //alipayClient.pageExecute(Request, "get").getBody();
            Dictionary<string, string> input = new Dictionary<string, string>();
            foreach (var key in Request.Form.Keys)
            {
                var value = Request.Form[key];
                input.Add(key, value);
            }
            CommonLogger.Error("支付宝3dApp回调：" + JsonConvert.SerializeObject(input));
            _paymentService.AlipayNotify(input, 3);
        }

        /// <summary>
        /// 微信支付
        /// </summary>
        /// <param name="input"></param>
        /// <returns>支付二维码地址</returns>
        [HttpPost]
        public async Task<ResponseContext<PayResponse>> Webchat(CreateMallOrderDto input)
        {
            return await _paymentService.Webchat(input, CurrentUser);
        }

        /// <summary>
        /// 微信H5支付
        /// </summary>
        /// <param name="input"></param>
        /// <returns>支付二维码地址</returns>
        [HttpPost]
        public async Task<ResponseContext<PayResponse>> WebchatWap(CreateMallOrderDto input)
        {
            return await _paymentService.WebchatWap(input, CurrentUser);
        }

        /// <summary>
        /// 微信App支付
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PayWechatResponse>> WechatApp(CreateMallOrderDto input)
        {
            return await _paymentService.WechatApp(input, CurrentUser);
        }

        /// <summary>
        /// 微信App支付 - 3d云实习
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PayWechatResponse>> WechatApp3d(CreateMallOrderDto input)
        {
            return await _paymentService.WechatApp3d(input, CurrentUser);
        }

        /// <summary>
        /// 微信小程序支付
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PayWechatResponse>> WechatJsapi(CreateMallOrderDto input)
        {
            return await _paymentService.WechatJsapi(input, CurrentUser);
        }

        /// <summary>
        /// 微信App支付 - 待支付订单
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<PayWechatResponse>> WechatAppContinue(long id)
        {
            return await _paymentService.WechatAppContinue(id, CurrentUser.Id);
        }

        /// <summary>
        /// 微信异步回调
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public void NotifyWebchat()
        {
            using (StreamReader reader = new StreamReader(Request.Body))
            {
                var xmlString = reader.ReadToEnd();
                CommonLogger.Error("微信回调：" + xmlString);
                _paymentService.WebchatNotify(xmlString);
            }
        }
    }
}