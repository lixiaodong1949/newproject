﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.M.IServices;
using Yssx.S.Dto;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 学校
    /// </summary>
    public class SchoolController : BaseController
    {
        ISchoolService _service;
        public SchoolController(ISchoolService service)
        {
            _service = service;
        }

        #region 院系
        /// <summary>
        /// 添加 更新 院系
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateSchoolCollege(CollegeDto model)
        {
            if (model.TenantId <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "当前用户未关联具体学校" };
            return await _service.AddUpdateSchoolCollege(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取 学校-院系 列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<CollegeDto>>> GetSchoolColleges(SchoolCollegeRequest model)
        {
            if (model.TenantId <= 0)
                return new ResponseContext<PageResponse<CollegeDto>> { Code = CommonConstants.SuccessCode, Data = new PageResponse<CollegeDto>() };
            return await _service.GetSchoolColleges(model);
        }
        #endregion

        #region 专业
        /// <summary>
        /// 添加 更新 专业
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateCollegeProfession(ProfessionDto model)
        {
            if (model.TenantId <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "当前用户未关联具体学校" };
            return await _service.AddUpdateCollegeProfession(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取 学校-院系-专业 列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<ProfessionDto>>> GetCollegeProfessions(CollegeProfessionsRequest model)
        {
            if (model.TenantId <= 0)
                return new ResponseContext<PageResponse<ProfessionDto>> { Code = CommonConstants.SuccessCode, Data = new PageResponse<ProfessionDto>() };
            return await _service.GetCollegeProfessions(model);
        }
        #endregion

        #region 班级
        /// <summary>
        /// 添加 更新 班级
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateClass(ClassDto model)
        {
            if (model.TenantId <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "当前用户未关联具体学校" };
            return await _service.AddUpdateClass(model, CurrentUser.Id);
        }
        /// <summary>
        /// 获取 学校-院系-专业-班级 列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<ClassDto>>> GetClasses(ClassRequest model)
        {
            if (model.TenantId <= 0)
                return new ResponseContext<PageResponse<ClassDto>> { Code = CommonConstants.SuccessCode, Data = new PageResponse<ClassDto>() };
            return await _service.GetClasses(model);
        }

        /// <summary>
        /// 添加 更新 班级 - 教师专用
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateClassOnTeacher(ClassDto model)
        {
            if (model.TenantId <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "当前用户未关联具体学校" };
            return await _service.AddUpdateClassOnTeacher(model, CurrentUser.Id);
        }
        
        /// <summary>
        /// 获取学校年级
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SchoolYearDto>>> GetSchoolYearList()
        {
            return await _service.GetSchoolYearList(CurrentUser.TenantId);
        }
        #endregion


    }
}