﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 广告位
    /// </summary>
    public class AdvertisingSpaceController : BaseController
    {
        private IAdvertisingSpaceService _service;

        public AdvertisingSpaceController(IAdvertisingSpaceService service)
        {
            _service = service;
        }

        /// <summary>
        /// 获取 广告位
        /// </summary>
        /// <param name="PlatformType">1专一网PC 11专一网APP 2技能抽查PC 22技能抽查APP  3技能竞赛PC 33技能竞赛APP  44云实习APP</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<AdvertisingSpaceChartResponse>> AdvertisingSpaceChart(int PlatformType)
        {
            return await _service.AdvertisingSpaceChart(PlatformType);
        }
    }
}