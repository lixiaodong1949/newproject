﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.Dto.Order;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// App相关Api
    /// </summary>
    public class AppController : BaseController
    {
        private readonly IAppService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public AppController(IAppService service)
        {
            _service = service;
        }

        #region 用户是否已存在预览课程行为
        /// <summary>
        /// 用户是否已存在预览课程行为
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> IsExistUserPreviewCourseAction()
        {
            return await _service.IsExistUserPreviewCourseAction(CurrentUser);
        }
        #endregion

        #region 获取用户预览课程信息
        /// <summary>
        /// 获取用户预览课程信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<UserPreviewCourseInfoViewModel>> GetUserPreviewCourseInfoForApp()
        {
            return await _service.GetUserPreviewCourseInfoForApp(CurrentUser);
        }
        #endregion

        #region 获取学校课程订单数据(App)
        /// <summary>
        /// 获取学校课程订单数据(App)
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<OrderCourseDto>>> GetCourseListBySchoolPlatForApp([FromQuery] CourseSearchDto dto)
        {
            return await _service.GetCourseListBySchoolPlatForApp(dto, CurrentUser);
        }
        #endregion

        #region 获取学校课程任务下课程数据(App)
        /// <summary>
        /// 获取学校课程任务下课程数据(App)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<OrderCourseDto>>> GetCourseListBySchoolCourseTaskForApp()
        {
            return await _service.GetCourseListBySchoolCourseTaskForApp(CurrentUser);
        }
        #endregion

        #region 获取学校课程订单数据(不分学历层级)
        /// <summary>
        /// 获取学校课程订单数据(不分学历层级)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxCourseListDto>>> GetSchoolBuyCourseListForApp()
        {
            return await _service.GetSchoolBuyCourseListForApp(CurrentUser);
        }
        #endregion

        #region 记录用户预览课程记录
        /// <summary>
        /// 记录用户预览课程记录
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> RecordUserCoursePreviewAction(UserCoursePreviewRequestModel dto)
        {
            return await _service.RecordUserCoursePreviewAction(dto, this.CurrentUser);
        }
        #endregion

        #region 记录用户发布课程任务记录
        /// <summary>
        /// 记录用户发布课程任务记录
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> RecordUserPublishCourseTaskAction(UserPublishCourseTaskRequestModel dto)
        {
            return await _service.RecordUserPublishCourseTaskAction(dto, this.CurrentUser);
        }
        #endregion

    }
}
