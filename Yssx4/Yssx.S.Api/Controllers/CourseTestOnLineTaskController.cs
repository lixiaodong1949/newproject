﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Task;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 课堂任务
    /// </summary>
    public class CourseTestOnLineTaskController : BaseController
    {
        private ICourseTestOnLineTaskService _service;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public CourseTestOnLineTaskController(ICourseTestOnLineTaskService service)
        {
            _service = service;
        }

        /// 新增课题任务
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<AddTestTaskDto>> AddTestTask(TaskDto model)
        {
            return await _service.AddTestTask(model,this.CurrentUser.Id);
        }

        /// <summary>
        /// 收题
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> StopTestTask(long taskId)
        {
            return await _service.StopTestTask(taskId);
        }

        /// <summary>
        /// 任务详情
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="examId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TestTaskDetailsDto>> TestTaskDetails(long taskId, long examId)
        {
            return await _service.TestTaskDetails(taskId, examId);
        }

        /// <summary>
        /// 任务结果分析
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="examId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<AnalyzeTaskResultDto>> AnalyzeTaskResult(long taskId, long examId)
        {
            return await _service.AnalyzeTaskResult(taskId, examId);
        }

        /// <summary>
        /// 显示做题学生列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<AnalyzeQuestionDetailsStudentsDto>>> AnalyzeQuestionStudents(long taskId, long examId)
        {
            return await _service.AnalyzeQuestionStudents(taskId, examId);
        }


        /// <summary>
        /// 我的任务列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="courseName">课程名称</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<MyCourseTaskDto>> GetMyCourseTaskList(int pageIndex, int pageSize, string courseName)
        {
            return await _service.GetMyCourseTaskList(this.CurrentUser, pageIndex, pageSize, courseName);
        }

        /// <summary>
        /// 任务题目列表
        /// </summary>
        /// <param name="taskId">任务Id</param>
        /// <param name="examId">试卷Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourseTaskExamTopicList(long taskId, long examId)
        {
            return await _service.GetCourseTaskExamTopicList(taskId,examId);
        }
    }
}