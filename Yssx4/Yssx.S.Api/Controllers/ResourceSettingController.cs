﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 配置
    /// </summary>
    public class ResourceSettingController : BaseController
    {
        IResourceSettingService _settingService;
        public ResourceSettingController(IResourceSettingService settingService)
        {
            _settingService = settingService;
        }


        #region 案例标签
        /// <summary>
        /// 获取案例标签列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<TagDto>> GetTagList(TagRequest model)
        {
            return await _settingService.GetTagList(model);
        }

        /// <summary>
        /// 新增，编辑案例标签
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> AddOrEditTag(TagDto model)
        {
            if (model.Id == 0)
                model.TagSourceType = TagSourceType.Case;
            return await _settingService.AddOrEditTag(model, CurrentUser.Id);
        }

        #endregion

        #region 知识点

        /// <summary>
        /// 获取知识点下拉列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<KnowledgePointDto>>> GetKnowledgePointList(long ignoreId)
        {
            return await _settingService.GetKnowledgePointList(ignoreId);
        }

        /// <summary>
        /// 新增，编辑知识点
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> AddOrEditKnowledgePoint(KnowledgePointDto model)
        {
            return await _settingService.AddOrEditKnowledgePoint(model, CurrentUser.Id);
        }

        #endregion

        #region 操作日志
        /// <summary>
        ///  获取 操作日志
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<OperationLogDto>> OperationLog(OperationLogRequest model)
        {
            return await _settingService.OperationLog(model, CurrentUser.Id);
        }
        #endregion

        #region Python接口Token
        /// <summary>
        ///  获取 Python接口Token
        /// </summary>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<string>> GetPythonToken()
        {
            return await _settingService.GetPythonToken();
        }
        #endregion

    }
}