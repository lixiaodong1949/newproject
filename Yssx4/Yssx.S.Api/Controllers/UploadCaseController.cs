﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 上传案例
    /// </summary>
    public class UploadCaseController : BaseController
    {
        private IUploadCaseService _service;

        /// <summary>
        /// --预留传Type参数0.PC 1.APP
        /// </summary>
        public UploadCaseController(IUploadCaseService service)
        {
            _service = service;
        }

        #region 上传案例
        /// <summary>
        /// 添加 更新 案例
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateCase(UploadCaseDto model)
        {
            return await _service.AddUpdateCase(model, CurrentUser.Id);
        }

        /// <summary>
        ///  删除 案例
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteUpdateCase(long Id)
        {
            return await _service.DeleteUpdateCase(Id, CurrentUser.Id);
        }

        /// <summary>
        /// 获取 案例列表
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<UploadCaseResult>> FindUpdateCase(UploadCaseQueryDao model)
        {
            return await _service.FindUpdateCase(model, CurrentUser.Id);
        }
        /// <summary>
        /// 获取 商城案例列表--无用户登陆
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<PageResponse<NoUserUploadCaseListResponse>> NoUserUploadCaseList(NoUserUploadCaseQueryDao model)
        {
            return await _service.NoUserUploadCaseList(model);
        }

        /// <summary>
        /// 获取 商城案例预览--无用户登陆
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<NoUserUploadCaseResponse>> NoUserUploadCase(long Id)
        {
            return await _service.NoUserUploadCase(Id);
        }

        /// <summary>
        ///  上架 案例
        /// </summary>
        /// <param name="id">主键Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> CaseRack(CaseRackDao model)
        {
            return await _service.CaseRack(model, CurrentUser.Id);
        }

        /// <summary>
        ///  下架 案例
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<bool>> CaseFrame(long Id)
        {
            return await _service.CaseFrame(Id, CurrentUser.Id);
        }

        /// <summary>
        ///  撤销 案例
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<bool>> Revoke(long Id)
        {
            return await _service.Revoke(Id, CurrentUser.Id);
        }

        /// <summary>
        ///  预览 案例
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<UploadCaseResponse>> Preview(long Id)
        {
            return await _service.Preview(Id,CurrentUser.Id);
        }


        /// <summary>
        ///  获取 标签列表
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<UploadCaseTag>> GetTagLib(UploadCaseTagLibDao model)
        {
            return await _service.GetTagLib(model);
        }

        /// <summary>
        ///  点赞  商城案例
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> Fabulous(CaseFabulousDao model)
        {
            return await _service.Fabulous(model, CurrentUser.Id);
        }

        /// <summary>
        ///  上传 证书
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> Certificate(CertificateDao model)
        {
            return await _service.Certificate(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取 商城好文推荐
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<GoodArticle>> GoodCase()
        {
            return await _service.GoodCase();
        }

        /// <summary>
        ///  获取 交易记录
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<PageResponse<TransactionRecords>> GetTransactionRecords(TransactionRecordsDao model)
        {
            return await _service.GetTransactionRecords(model);
        }

        /// <summary>
        /// 添加 更新 批注(追加批注)
        /// </summary>
        /// <param name="md"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateComments(CommentsDao md)
        {
            return await _service.AddUpdateComments(md, CurrentUser.Id);
        }

        /// <summary>
        ///  获取 批注
        /// </summary>
        /// <param name="Id">案例Id</param>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<List<CommentsResponse>>> GetComments(long Id)
        {
            return await _service.GetComments(Id);
        }


        /// <summary>
        /// 删除 批注
        /// </summary>
        /// <param name="Id">主键Id</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteComments(DeleteCommentsDao md)
        {
            return await _service.DeleteComments(md, CurrentUser.Id);
        }

        ///// <summary>
        ///// 获取 购买案例
        ///// </summary>
        ///// <param name="id">Id</param>
        ///// <returns></returns>    
        //[HttpPost]
        //public async Task<PageResponse<UploadCaseResponse>> FindPurchaseCase(PurchUploadCaseDao model)
        //{
        //    return await _service.FindPurchaseCase(model, CurrentUser.Id);
        //}

        ///// <summary>
        ///// 批量 上传案例
        ///// </summary>
        ///// <param name="id">Id</param>
        ///// <returns></returns>    
        //[HttpPost]
        //public async Task<ResponseContext<bool>> BatchUploadCase(BatchUploadCaseDao model)
        //{
        //    return await _service.BatchUploadCase(model, CurrentUser.Id);
        //}

        ///// <summary>
        ///// 更新 订单交易记录和状态
        ///// </summary>
        ///// <param name="Id">案例ID</param>
        ///// <returns></returns>
        //[HttpGet]
        //public async Task<ResponseContext<bool>> OrderTransactions(long Id)
        //{
        //    return await _service.OrderTransactions(Id, CurrentUser.Id);
        //}
        #endregion
    }
}