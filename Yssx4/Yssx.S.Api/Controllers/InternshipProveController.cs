﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 实习证书
    /// </summary>
    public class InternshipProveController : BaseController
    {
        private IInternshipProveService _internshipProveService;
        /// <summary>
        /// 
        /// </summary>
        public InternshipProveController(IInternshipProveService internshipProveService)
        {
            _internshipProveService = internshipProveService;
        }


        #region 实习证书
        /// <summary>
        /// 获取生成实习证书的资料
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<InternshipProveInfoDto>> GetInternshipProveInfo(long gradeId)
        {
            return await _internshipProveService.GetInternshipProveInfo(gradeId, CurrentUser.Id);
        }

        /// <summary>
        /// 生成实习证书
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddInternshipProve(AddInternshipProveDto model)
        {
            return await _internshipProveService.AddInternshipProve(model, CurrentUser.Id);
        }

        /// <summary>
        /// 查询证书列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<InternshipProveListDto>> GetInternshipProveList(InternshipProveListRequest model)
        {
            return await _internshipProveService.GetInternshipProveList(model, CurrentUser.Id);
        }

        /// <summary>
        /// 实习证书查询
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<InternshipProveListDto>> GetInternshipProveBySN(string sn)
        {
            return await _internshipProveService.GetInternshipProveBySN(sn);
        }
        #endregion

        #region 实训列表
        /// <summary>
        /// 实训列表 - 岗位实训
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<GetPaidJobInternshipListDto>> GetPaidJobInternshipList(GetPaidJobInternshipListRequest model)
        {
            return await _internshipProveService.GetPaidJobInternshipList(model, CurrentUser.Id);
        }

        /// <summary>
        /// 查询通关记录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<JobTrainingEndRecordDto>> GetJobTrainingEndRecord(JobTrainingEndRecordRequest model)
        {
            return await _internshipProveService.GetJobTrainingEndRecord(model, CurrentUser.Id);
        }
        #endregion



    }
}
