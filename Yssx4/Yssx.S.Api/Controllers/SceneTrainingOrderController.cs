﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Api.Auth;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.Dto.SceneTraining;
using Yssx.S.IServices;
using Yssx.S.IServices.SceneTraining;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 场景实训订单
    /// </summary>
    public class SceneTrainingOrderController : BaseController
    {
        ISceneTrainingOrderService _service;
        public SceneTrainingOrderController(ISceneTrainingOrderService service)
        {
            _service = service;
        }

        #region 获取购买场景实训列表
        /// <summary>
        /// 获取购买场景实训列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxSceneTrainingViewModel>> GetSceneTrainingByBuy([FromQuery]YssxSceneTrainingQuery query)
        {
            return await _service.GetSceneTrainingByBuy(query, this.CurrentUser);
        }
        #endregion
    }
}