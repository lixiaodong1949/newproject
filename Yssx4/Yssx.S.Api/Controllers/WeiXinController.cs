﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 微信请求接口
    /// </summary>
    public class WeiXinController : BaseController
    {
        IHttpClientFactory _httpClientFactory;
        public WeiXinController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        /// <summary>
        /// 取微信用户信息(旧)
        /// </summary>
        /// <param name="code">微信授权的code</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<WeiXinDto>> GetWxInfo(string code)
        {
            var client = _httpClientFactory.CreateClient();
            string uri = $"https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx1e4be604f5cc72be&secret=10c7c5bf3e760b77428818b60442e4dd&code={code}&grant_type=authorization_code";
            var result = await client.GetAsync(uri);
            HttpContent response = result.Content;
            var s = await response.ReadAsStringAsync();
            if (s.Contains("errcode"))
            {
                return new ResponseContext<WeiXinDto> { Code = CommonConstants.ErrorCode, Msg = "无效的微信码!" };
            }
            WeiXinDto data = JsonConvert.DeserializeObject<WeiXinDto>(s);
            return new ResponseContext<WeiXinDto> { Data = data };
        }

        /// <summary>
        /// 获取微信用户信息(PC端和APP)
        /// </summary>
        /// <param name="code">微信授权的code</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<WeiXinInfoDto>> GetWxInfoByCode(string code)
        {
            //获取openid
            var client = _httpClientFactory.CreateClient();
            string uri = $"https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx1e4be604f5cc72be&secret=10c7c5bf3e760b77428818b60442e4dd&code={code}&grant_type=authorization_code";
            //string uri = $"https://api.weixin.qq.com/sns/oauth2/access_token?appid=wxd17811861c73ae5c&secret=4d75ad6356cf3fe520770d2cdbccc823&code={code}&grant_type=authorization_code";
            var result = await client.GetAsync(uri);
            HttpContent response = result.Content;
            var s = await response.ReadAsStringAsync();
            if (s.Contains("errcode"))
            {
                return new ResponseContext<WeiXinInfoDto> { Code = CommonConstants.ErrorCode, Msg = "无效的微信码!" };
            }
            WeiXinInfoDto data = JsonConvert.DeserializeObject<WeiXinInfoDto>(s);
            //获取unionid
            string uriU = $"https://api.weixin.qq.com/sns/userinfo?access_token={data.Access_Token}&openid={data.Openid}";
            var resultU = await client.GetAsync(uriU);
            HttpContent responseU = resultU.Content;
            var resU = await responseU.ReadAsStringAsync();
            if (resU.Contains("errcode"))
            {
                return new ResponseContext<WeiXinInfoDto> { Code = CommonConstants.ErrorCode, Msg = "获取微信昵称失败!" };
            }
            WeiXinInfoDto dataU = JsonConvert.DeserializeObject<WeiXinInfoDto>(resU);
            data.NickName = dataU.NickName;
            data.Unionid = dataU.Unionid;
            return new ResponseContext<WeiXinInfoDto> { Data = data };
        }

        /// <summary>
        /// 取微信用户信息(小程序)
        /// </summary>
        /// <param name="code">微信授权的code</param>
        /// <param name="appletsType">哪个小程序</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<WeiXinAppletsDto>> GetWxInfoByAppletsCode(string code, int appletsType = 0)
        {
            List<AppletsType> applets = new List<AppletsType>
            {
                new AppletsType{Index=0,AppId="wxc582ffca0734cab9",Secret="9186e2c0d5f4ba174e2afa53",Desc="专一网老师端" },
                new AppletsType{Index=1,AppId="wxd17811861c73ae5c",Secret="4d75ad6356cf3fe520770d2cdbccc823",Desc="初级会计证"}
            };
            AppletsType applets1 = applets.Find(x => x.Index == appletsType);
            var client = _httpClientFactory.CreateClient();
            //string uri = $"https://api.weixin.qq.com/sns/jscode2session?appid=wxc582ffca0734cab9&secret=9186e2c0d5f4ba174e2afa53bedebcd7&js_code={code}&grant_type=authorization_code";
            string uri = $"https://api.weixin.qq.com/sns/jscode2session?appid={applets1.AppId}&secret={applets1.Secret}&js_code={code}&grant_type=authorization_code";
            var result = await client.GetAsync(uri);
            HttpContent response = result.Content;
            var s = await response.ReadAsStringAsync();
            if (s.Contains("errcode"))
            {
                return new ResponseContext<WeiXinAppletsDto> { Code = CommonConstants.ErrorCode, Msg = "无效的微信码!" };
            }
            WeiXinAppletsDto data = JsonConvert.DeserializeObject<WeiXinAppletsDto>(s);
            return new ResponseContext<WeiXinAppletsDto> { Data = data };
        }

        /// <summary>
        /// 微信小程序解密用户数据
        /// </summary>
        /// <param name="iv">iv</param>
        /// <param name="key">session_key</param>
        /// <param name="inputdata">encryptedData</param>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<WeiXinDecodeUserDto>> GetDecodeUser(string iv, string key, string inputdata)
        {
            return await Task.Run(() =>
            {
                WeiXinDecodeUserDto resData = new WeiXinDecodeUserDto();
                try
                {
                    iv = iv.Replace(" ", "+");
                    key = key.Replace(" ", "+");
                    inputdata = inputdata.Replace(" ", "+");
                    byte[] encryptedData = Convert.FromBase64String(inputdata);

                    RijndaelManaged rijndaelCipher = new RijndaelManaged();
                    rijndaelCipher.Key = Convert.FromBase64String(key); // Encoding.UTF8.GetBytes(AesKey);
                    rijndaelCipher.IV = Convert.FromBase64String(iv);// Encoding.UTF8.GetBytes(AesIV);
                    rijndaelCipher.Mode = CipherMode.CBC;
                    rijndaelCipher.Padding = PaddingMode.PKCS7;
                    ICryptoTransform transform = rijndaelCipher.CreateDecryptor();
                    byte[] plainText = transform.TransformFinalBlock(encryptedData, 0, encryptedData.Length);
                    string result = Encoding.UTF8.GetString(plainText);
                    resData = JsonConvert.DeserializeObject<WeiXinDecodeUserDto>(result);
                    return new ResponseContext<WeiXinDecodeUserDto> { Code = CommonConstants.SuccessCode, Data = resData };
                }
                catch (Exception e)
                {
                    return new ResponseContext<WeiXinDecodeUserDto> { Code = CommonConstants.ErrorCode, Msg = e.Message };
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<string>> GetWxTicket()
        {
            var client = _httpClientFactory.CreateClient();
            WeiXinTicketDto dto2 = new WeiXinTicketDto();
            WeiXinDto dto = new WeiXinDto();

            string cacheData = await RedisHelper.HGetAsync(CacheKeys.WeiXinTokenHashKey, "weixinToken");
            if (string.IsNullOrWhiteSpace(cacheData))
            {
                //string uri = $"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx8020a8cbd9b9341c&secret=b7dc5a5f5f877b89078f974b5982a386";
                //var result = await client.GetAsync(uri);
                //HttpContent response = result.Content;
                var s = await GetWeiXinToken();
                if (s.Contains("errcode"))
                {
                    return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = s };
                }
                dto = JsonConvert.DeserializeObject<WeiXinDto>(s);
                dto.Date = DateTime.Now;
                await RedisHelper.HSetAsync(CacheKeys.WeiXinTokenHashKey, "weixinToken", dto);

            }
            else
            {
                dto = JsonConvert.DeserializeObject<WeiXinDto>(cacheData);
                double x = (DateTime.Now - dto.Date).TotalHours;
                if (x > 1.8d)
                {
                    var token = await GetWeiXinToken();
                    if (token.Contains("errcode"))
                    {
                        return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = token };
                    }
                    dto = JsonConvert.DeserializeObject<WeiXinDto>(token);
                    dto.Date = DateTime.Now;
                    await RedisHelper.HSetAsync(CacheKeys.WeiXinTokenHashKey, "weixinToken", dto);
                }

            }

            string uri2 = $"https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={dto.Access_Token}&type=jsapi";
            var result2 = await client.GetAsync(uri2);
            HttpContent response2 = result2.Content;
            var s2 = await response2.ReadAsStringAsync();
            dto2 = JsonConvert.DeserializeObject<WeiXinTicketDto>(s2);
            return new ResponseContext<string> { Data = dto2.Ticket };
        }

        private async Task<string> GetWeiXinToken()
        {
            var client = _httpClientFactory.CreateClient();
            string uri = $"https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx8020a8cbd9b9341c&secret=b7dc5a5f5f877b89078f974b5982a386";
            var result = await client.GetAsync(uri);
            HttpContent response = result.Content;
            var s = await response.ReadAsStringAsync();
            //if (s.Contains("errcode"))
            //{
            //    return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = s };
            //}
            return s;
        }
    }
}