﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 用户
    /// </summary>
    public class UserController : BaseController
    {
        private IUserService _userService;
        private IbkUserService _bkUserService;
        public UserController(IUserService userService, IbkUserService bkUserService)
        {
            _userService = userService;
            _bkUserService = bkUserService;
        }

        /// <summary>
        /// 获取用户姓名
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public string GetUserName(string id)
        {
            return "1111";
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ResponseContext<bool>> FindUserByName(string mb)
        {
            return await _userService.FindUserByName(mb);
        }

        #region 绑定手机号、微信
        /// <summary>
        ///  绑定手机号
        /// </summary>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<bool>> BindPhoneByUser(string mobilePhone, string verificationCode)
        {
            return await _bkUserService.BindPhoneByUser(mobilePhone, verificationCode, CurrentUser.Id);
        }

        /// <summary>
        ///  绑定微信
        /// </summary>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<bool>> BindWechartByUser(string wechart, string unionid)
        {
            return await _bkUserService.BindWechartByUser(wechart, unionid, CurrentUser.Id);
        }

        #endregion

        /// <summary>
        /// 修改个人信息
        /// </summary>
        /// <param name="can">参数</param>
        /// <param name="type">类型（1.昵称 2.头像 3.真实姓名 4.工学号）</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ModifyNickname(string can, int type)
        {
            return await _userService.ModifyUserInfo(can, type, CurrentUser.Id);
        }

        /// <summary>
        /// 老师修改学生姓名学号信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> ModifyStudentInfoByUserId(StudentInfoRequest model)
        {
            return await _userService.ModifyStudentInfoByUserId(model, CurrentUser.Id);
        }
        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<YssxUser>> UserInformation()
        {
            return await _userService.UserInformation(CurrentUser.Id);
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UpdateUserPassword(UpdateUserPasswordDto model)
        {
            return await _userService.UpdateUserPassword(model, CurrentUser.Id);
        }

        /// <summary>
        /// 修改班级
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UpdateClass(UpdateClassDto dto)
        {
            return await _userService.UpdateClass(this.CurrentUser.Id, dto);
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<UserInfoDto>> GetUserInfo()
        {
            return await _userService.GetUserInfo(this.CurrentUser);
        }

        /// <summary>
        /// 是否付费用户
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> IsVip()
        {
            return await _userService.IsVip(this.CurrentUser.Id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ResponseContext<bool>> AddPartnerInfo(PartnerDto dto)
        {
            if (dto != null)
            {
                return await _userService.AddUserInfo(dto.Name, dto.Mobile);
            }

            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请传入参数!" };
        }
    }
}