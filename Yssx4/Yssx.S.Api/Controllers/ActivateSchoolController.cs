﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Utils;
using Yssx.S.Api.DomainEventHandlers;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.IServices;
using Yssx.S.IServices.Means;
using Yssx.S.Pocos;
using Yssx.S.ServiceImpl;
using Yssx.S.ServiceImpl.Web;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 激活学校
    /// </summary>
    public class ActivateSchoolController : BaseController
    {
        private IActivateSchoolService _service; private readonly IMediator _mediator;

        /// <summary>
        /// 
        /// </summary>
        public ActivateSchoolController(IActivateSchoolService service, IMediator mediator)
        {
            _service = service;
            _mediator = mediator;
        }

        /// <summary>
        /// 验证学下激活码
        /// </summary>
        /// <param name="schoolCode">学校激活码</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<SchoolActivationSuccessfulDto>> VerifySchoolActivationCode(string schoolCode)
        {
            return await _service.VerifySchoolActivationCode(schoolCode);
        }

        /// <summary>
        /// 验证角色激活码
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<VerifyRoleActivationCodeDto>> VerifyRoleActivationCode(string roleCode)
        {
            return await _service.VerifyRoleActivationCode(roleCode, CurrentUser.Id);
        }

        /// <summary>
        /// 填写激活信息
        /// </summary>
        /// <param name="dto">激活信息</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<FillInActivAtionInformationDto>> FillInActivAtionInformation(YssxActivationInformationDto dto)
        {
            return await _service.FillInActivAtionInformation(dto, CurrentUser);
        }

        /// <summary>
        /// 下拉数据
        /// </summary>
        /// <param name="id">id（查学校Id传0，查院系传学校Id，查专业传院系Id，查班级传专业Id）</param>
        /// <param name="typeId">1，学校，2，院系，3，专业，4，班级</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<DropdownListDto>>> DropdownList(long id, int typeId)
        {
            return await _service.DropdownList(id, typeId);
        }

        /// <summary>
        /// 获取用户激活信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<ActivationRecordDto>> ActivationRecord()
        {
            return await _service.ActivationRecord(CurrentUser.Id);
        }



        /// <summary>
        /// 验证角色激活码 - new
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<VerifyRoleActivationCodeDto>> VerifyRoleActCode(string roleCode, int roleType)
        {
            var result = await _service.VerifyRoleActCode(roleCode, roleType, CurrentUser.Id);
            if (result.Code == CommonConstants.SuccessCode)
            {
                await _mediator.Publish(new VerifyRoleActCodeEvent(CurrentUser.Id));
            }
            return result;
        }

        /// <summary>
        /// 补充激活信息 - new
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<FillInActivAtionInformationDto>> FillInActInformation(ActivationInfoRequest model)
        {
            return await _service.FillInActInformation(model, CurrentUser.Id, CurrentUser.LoginType);
        }

    }
}