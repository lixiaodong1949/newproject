﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 课程备课相关Api
    /// </summary>
    public class CoursePrepareControlle : BaseController
    {
        ICoursePrepareService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public CoursePrepareControlle(ICoursePrepareService service)
        {
            _service = service;
        }

        #region 新增/编辑课程备课
        /// <summary>
        /// 新增/编辑课程备课
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> AddOrEditCoursePrepare(CoursePrepareRequestModel dto)
        {
            return await _service.AddOrEditCoursePrepare(dto, this.CurrentUser);
        }
        #endregion

        #region 获取课程备课列表
        /// <summary>
        /// 获取课程备课列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CoursePrepareViewModel>>> GetCoursePrepareList([FromQuery] CoursePrepareQuery query)
        {
            return await _service.GetCoursePrepareList(query, this.CurrentUser);
        }
        #endregion

        #region 删除课程备课
        /// <summary>
        /// 删除课程备课
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCoursePrepareById(long id)
        {
            return await _service.DeleteCoursePrepareById(id, this.CurrentUser);
        }
        #endregion

        #region 新增课程备课文件(引用课程资源)
        /// <summary>
        /// 新增课程备课文件(引用课程资源)
        /// </summary>
        /// <param name="requestList"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddCoursePrepareFiles(List<CoursePrepareFilesRequestModel> requestList)
        {
            return await _service.AddCoursePrepareFiles(requestList, this.CurrentUser);
        }
        #endregion

        #region 获取课程预习/备课文件列表
        /// <summary>
        /// 获取课程预习/备课文件列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CoursePrepareFilesViewModel>>> GetCoursePrepareFilesList([FromQuery] CoursePrepareFilesQuery query)
        {
            return await _service.GetCoursePrepareFilesList(query);
        }
        #endregion

        #region 获取课程备课选中课程题目列表
        /// <summary>
        /// 获取课程备课选中课程题目列表
        /// </summary>
        /// <param name="couPrepareId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CoursePrepareSelectCourseTopicViewModel>>> GetPrepareSelectCourseTopicList(long couPrepareId)
        {
            return await _service.GetPrepareSelectCourseTopicList(couPrepareId);
        }
        #endregion

        #region 获取课程备课选中案例题目列表
        /// <summary>
        /// 获取课程备课选中案例题目列表
        /// </summary>
        /// <param name="couPrepareId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CoursePrepareSelectCaseTopicIdsViewModel>>> GetPrepareSelectCaseTopicIdList(long couPrepareId)
        {
            return await _service.GetPrepareSelectCaseTopicIdList(couPrepareId);
        }
        #endregion

        #region 删除课程备课文件(根据主键)
        /// <summary>
        /// 删除课程备课文件(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCoursePrepareFilesById(long id)
        {
            return await _service.DeleteCoursePrepareFilesById(id, this.CurrentUser);
        }
        #endregion

        #region 删除课程备课文件(根据文件主键)
        /// <summary>
        /// 删除课程备课文件(根据文件主键)
        /// </summary>
        /// <param name="couPreId">课程备课Id</param>
        /// <param name="fileId">课件库Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCoursePrepareFilesByFileId(long couPreId, long fileId)
        {
            return await _service.DeleteCoursePrepareFilesByFileId(couPreId, fileId, this.CurrentUser);
        }
        #endregion

        #region 新增课程备课题目(引用课程题目)
        /// <summary>
        /// 新增课程备课题目(引用课程题目)
        /// </summary>
        /// <param name="requestList"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddCoursePrepareTopic(List<CoursePrepareTopicRequestModel> requestList)
        {
            return await _service.AddCoursePrepareTopic(requestList, this.CurrentUser);
        }
        #endregion

        #region 发布预习
        /// <summary>
        /// 发布预习
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> PublishCoursePreview(CoursePreviewRequestModel dto)
        {
            return await _service.PublishCoursePreview(dto, this.CurrentUser);
        }
        #endregion

        #region 获取课程教师预习列表
        /// <summary>
        /// 获取课程教师预习列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<CoursePreviewToTeacherViewModel>> GetTeacherCoursePreviewList([FromQuery] CoursePreviewToTeacherQuery query)
        {
            return await _service.GetTeacherCoursePreviewList(query, this.CurrentUser);
        }
        #endregion

        #region 删除课程预习(根据主键)
        /// <summary>
        /// 删除课程预习(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCoursePreviewById(long id)
        {
            return await _service.DeleteCoursePreviewById(id, this.CurrentUser);
        }
        #endregion

        #region 获取课程学生预习列表
        /// <summary>
        /// 获取课程学生预习列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<CoursePreviewToStudentViewModel>> GetStudentCoursePreviewList([FromQuery] CoursePreviewToStudentQuery query)
        {
            return await _service.GetStudentCoursePreviewList(query, this.CurrentUser);
        }
        #endregion

        #region 学生首次进入课程预习记录
        /// <summary>
        /// 学生首次进入课程预习记录
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> StudentFirstCheckCoursePreview(StudentViewPreviewRequestModel dto)
        {
            return await _service.StudentFirstCheckCoursePreview(dto, this.CurrentUser);
        }
        #endregion

        #region 获取学生查看预习详情列表
        /// <summary>
        /// 获取学生查看预习详情列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<StudentPreviewCourseDetailViewModel>> GetStudentPreviewCourseDetailList([FromQuery] StudentPreviewCourseDetailQuery query)
        {
            return await _service.GetStudentPreviewCourseDetailList(query);
        }
        #endregion

        #region 获取学生数量及查看预习数量记录
        /// <summary>
        /// 获取学生数量及查看预习数量记录
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<StudentNumberAndHadCheckNumberViewModel>> GetStudentNumberAndHadCheckNumberRecord([FromQuery] StudentNumberAndHadCheckNumberQuery query)
        {
            return await _service.GetStudentNumberAndHadCheckNumberRecord(query);
        }
        #endregion

        #region 添加学生课程资源不懂记录
        /// <summary>
        /// 添加学生课程资源不懂记录
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddStudentDoubtCourseFileRecord(StudentDoubtCourseFileRequestModel dto)
        {
            return await _service.AddStudentDoubtCourseFileRecord(dto, this.CurrentUser);
        }
        #endregion

        #region 获取学生不懂(留言)详情列表
        /// <summary>
        /// 获取学生不懂(留言)详情列表
        /// </summary>
        /// <param name="couPreviewId">课程预习Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<StudentDoubtDetailViewModel>>> GetStudentDoubtDetailList(long couPreviewId)
        {
            return await _service.GetStudentDoubtDetailList(couPreviewId);
        }
        #endregion

        #region 获取预习某一资源 学生不懂详情记录
        /// <summary>
        /// 获取预习某一资源 学生不懂详情记录
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<FileStudentDoubtInfoViewModel>>> GetFileStudentDoubtInfoRecord([FromQuery] FileStudentDoubtInfoQuery query)
        {
            return await _service.GetFileStudentDoubtInfoRecord(query, this.CurrentUser);
        }
        #endregion

        #region 获取课程备课题目列表(课程资源来源)
        /// <summary>
        /// 获取课程备课题目列表(课程资源来源)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<CoursePrepareTopicViewModel>> GetCoursePrepareTopicList([FromQuery] CoursePrepareTopicQuery query)
        {
            return await _service.GetCoursePrepareTopicList(query);
        }
        #endregion

        #region 获取课程备课文件资源列表(课程资源来源)
        /// <summary>
        /// 获取课程备课文件资源列表(课程资源来源)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<CourseFilesResourceViewModel>> GetCoursePrepareFileList([FromQuery] CourseFilesResourceQuery query)
        {
            return await _service.GetCoursePrepareFileList(query);
        }
        #endregion

    }
}