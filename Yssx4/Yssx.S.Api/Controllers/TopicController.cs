﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices.Topics;
using Yssx.S.ServiceImpl.Topics;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 题目
    /// </summary>
    public class TopicController : BaseController
    {
        private ITopicService _topicService;
        private ICertificateTopicService _certificateTopicService;
        /// <summary>
        /// 
        /// </summary>
        public TopicController()
        {
            _topicService = new TopicService();
            _certificateTopicService = new CertificateTopicService();
        }

        #region 添加题目
        /// <summary>
        /// 添加 更新 题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveSimpleQuestion(SimpleQuestionRequest model)
        {
//            List<long> ids = new List<long> {993137788399221,
//993137788399231,
//993137788399658,
//993337788399218,
//993337788399618,
//993337788399626,
//993337788399627,
//993337788399629,
//993337788399648,
//993337788399668,993137788399655,993337788399208 };
            //if (!ids.Any(x => x == this.CurrentUser.Id)) return new ResponseContext<bool> { Data = false, Code = CommonConstants.ErrorCode, Msg = "无权限!" };
            return await _topicService.SaveSimpleQuestion(model, CurrentUser);
        }

        /// <summary>
        /// 添加 更新 复杂题目（分录题、表格题、案例题）
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveComplexQuestion(ComplexQuestionRequest model)
        {
            return await _topicService.SaveComplexQuestion(model, CurrentUser);
        }

        /// <summary>
        /// 添加 更新 综合题
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveMainSubQuestion(MainSubQuestionRequest model)
        {
            return await _topicService.SaveMainSubQuestion(model, CurrentUser);
        }
        #endregion

        #region 获取题目信息
        /// <summary>
        /// 获取题目信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TopicInfoDto>> GetQuestion(long id)
        {
            return await _topicService.GetQuestion(id);
        }

        /// <summary>
        /// 根据CaseId查询题目
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<TopicStatisticsView>> GetQuestionByCase(GetQuestionByCaseRequest model)
        {
            return await _topicService.GetQuestionByCase(model);
        }

        /// <summary>
        /// 获取图表题导入的EXCEL数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<TopicFillGridDto>>> GetQuestionInFillGrid(long caseId)
        {
            return await _topicService.GetQuestionInFillGrid(caseId);
        }

        /// <summary>
        /// 查询缓存题目列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<TopicListView>>> GetQuestionByCache(long caseId)
        {
            return await _topicService.GetQuestionByCache(caseId);
        }
        #endregion

        #region 删除题目、选项、文件
        /// <summary>
        /// 删除题目
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteQuestion(long id)
        {
            return await _topicService.DeleteQuestion(id, CurrentUser);
        }

        /// <summary>
        /// 删除附件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteTopicFile(long id)
        {
            return await _topicService.DeleteTopicFile(id, CurrentUser);
        }

        /// <summary>
        /// 删除选项
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteOption(long topicId, long id)
        {
            return await _topicService.DeleteOption(topicId, id, CurrentUser);
        }

        /// <summary>
        /// 设置题目状态
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="status">0：禁用,1:启用</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetQuestionStatuts(long topicId, int status)
        {
            return await _topicService.SetQuestionStatuts(topicId, status, CurrentUser);
        }
        #endregion

        #region 修改题目信息 - 案例操作

        /// <summary>
        /// 案例修改分值题目获取
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<TopicInfoDto>>> GetQuestionByCaseId(long CaseId)
        {
            return await _topicService.GetQuestionByCaseId(CaseId);
        }

        /// <summary>
        /// 案例修改分值批量修改
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UpdateQuestionByCaseId(List<TopicInfoDto> model)
        {
            return await _topicService.UpdateQuestionByCaseId(model);
        }

        /// <summary>
        /// 案例修改分值题目获取
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetQuetionScore(long caseId, decimal x)
        {
            return await _topicService.SetQuetionScore(caseId, x);
        }
        #endregion

        #region 分录题操作
        /// <summary>
        /// 添加凭证（分录题）
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveCertificateTopic(CertificateTopicRequest model)
        {
            return await _certificateTopicService.SaveCertificateTopic(model);
        }

        /// <summary>
        /// 获取单个分录题
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CertificateTopicRequest>> GetCoreCertificateTopic(long id)
        {
            return await _certificateTopicService.GetCoreCertificateTopic(id);
        }

        /// <summary>
        /// 根据凭证id删除凭证(分录题)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCertificateTopicById(long id)
        {
            return await _certificateTopicService.DeleteCertificateTopicById(id);
        }

        /// <summary>
        /// 根据凭证id删除凭证(分录题)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCertificateTopicByTopicId(long id)
        {
            return await _certificateTopicService.DeleteCertificateTopicByTopicId(id);
        }
        #endregion

    }
}
