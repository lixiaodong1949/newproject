﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 课程备课上课相关Api
    /// </summary>
    public class PlanningAttendClassController : BaseController
    {
        IPlanningAttendClassService _service;

        public PlanningAttendClassController(IPlanningAttendClassService service)
        {
            _service = service;
        }

        #region 课程备课上课        
        /// <summary>
        /// 课程备课上课
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> CreatePlanningAttendClassRecord(PlanningAttendClassRequestModel dto)
        {
            return await _service.CreatePlanningAttendClassRecord(dto, this.CurrentUser);
        }
        #endregion

        #region 获取备课课堂记录
        /// <summary>
        /// 获取备课课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<PlanningClassRecordViewModel>> GetPlanningClassRecordList([FromQuery]PlanningClassRecordQuery query)
        {
            return await _service.GetPlanningClassRecordList(query, this.CurrentUser);
        }
        #endregion

        #region 删除备课课堂记录列表
        /// <summary>
        /// 删除备课课堂记录列表
        /// </summary>
        /// <param name="attendClassRecordIds">上课课堂记录Ids</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> RemovePlanningClassRecordList(List<long> attendClassRecordIds)
        {
            return await _service.RemovePlanningClassRecordList(attendClassRecordIds, this.CurrentUser);
        }
        #endregion

        #region 导出备课课堂记录
        /// <summary>
        /// 导出备课课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> ExportPlanningClassRecord([FromQuery]PlanningClassRecordQuery query)
        {
            ResponseContext<List<ExportYssxClassRecordViewModel>> response = await _service.ExportPlanningClassRecord(query, this.CurrentUser);

            byte[] ms = null;
            if (response.Data != null && response.Data.Count > 0)
                ms = NPOIHelper<ExportYssxClassRecordViewModel>.OutputExcel(response.Data, new string[] { "课堂记录" });
            else
                return NotFound();

            return File(ms, "application/ms-excel", "课堂记录.xlsx");
        }
        #endregion

    }
}