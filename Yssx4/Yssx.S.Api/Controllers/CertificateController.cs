﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 记账凭证数据统计查询接口
    /// </summary>
    public class CertificateController : BaseController
    {
        ICertificateService service;
        public CertificateController()
        {
            service = new CertificateService();
        }

        /// <summary>
        /// 明细账查询
        /// </summary>
        /// <param name="subjectId">科目Id</param>
        /// <param name="id">案例Id</param>
        /// <param name="type">请求类型0:后台，1：学生端</param>
        /// <param name="gradeId">gradeId</param>
        /// <param name="dateStr">会计期间</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CertificateDto>>> GetCertificateDetailDataList(long id, long subjectId, long gradeId, int type,string dateStr)
        {
            return await service.GetCertificateDetailDataList(id,subjectId,gradeId,type, dateStr);
        }

        /// <summary>
        /// 总账查询
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="type">请求类型0:后台，1：学生端</param>
        /// <param name="gradeId">gradeId</param>
        /// <param name="subjectType">科目类型</param>
        /// <param name="keyword">查询关键字</param>
        ///  <param name="dateStr">会计期间</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CertificateDto>>> GetCertificateSummerDataList(long id, long gradeId, int subjectType, string keyword, int type, string dateStr)
        {
            return await service.GetCertificateSummerDataList(id, gradeId, subjectType, keyword,type, dateStr);
        }

        /// <summary>
        /// 余额查询
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="gradeId">gradeId</param>
        /// <param name="type">请求类型0:后台，1：学生端</param>
        /// <param name="subjectType">科目类型</param>
        /// <param name="keyword">查询关键字</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CertificateBalanceDto>>> GetCertificateBalanceDataList(long id, long gradeId, int subjectType, string keyword, int type)
        {
            return await service.GetCertificateBalanceDataList(id, gradeId, subjectType, keyword, type);
        }
    }
}