﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.Dto.Topics;
using Yssx.S.IServices;
using Yssx.S.IServices.Examination;
using Yssx.S.IServices.Topics;
using Yssx.S.ServiceImpl;
using Yssx.S.ServiceImpl.Examination;
using Yssx.S.ServiceImpl.Topics;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 试卷相关服务
    /// </summary>
    public class ExamPaperController : BaseController
    {
        private IExamPaperService _service;
        public ExamPaperController(IExamPaperService service)
        {
            _service = service;
        }

        /// <summary>
        /// 获取考试岗位信息
        /// </summary>
        /// <param name="examId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<ExamPaperPostionView>>> GetExamPaperPostions(long examId)
        {
            return await _service.GetExamPaperPostions(examId);
        }

    }
}