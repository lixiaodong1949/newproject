﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.Formula.Functions;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.IServices.Examination;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// App课程，岗位实训做题统一接口
    /// </summary>
    public class AppExamController : BaseController
    {
        private IStudentExamService _service;
        private ICourseExercisesService _courseExercisesService;
        public AppExamController(IStudentExamService service, ICourseExercisesService courseExercisesService)
        {
            _service = service;
            _courseExercisesService = courseExercisesService;
        }

        /// <summary>
        ///  获取试卷信息（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <param name="examId"></param>
        /// <param name="gradeId"></param>
        /// <param name="courseId"></param>
        /// <param name="sectionId"></param>
        /// <param name="option">0：岗位实训,1：课程实训</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperBasicInfo>> GetExamPaperBasicInfo(long examId, long gradeId = 0, long courseId = 0, long sectionId = 0, int option = 0)
        {
            if (option == 0)
            {
                return await _service.GetExamPaperBasicInfo(examId, CurrentUser, gradeId);
            }
            else
            {
                ResponseContext<ExamPaperCourseBasicInfo> response = await _courseExercisesService.GetCourseExamPaperBasicInfo(courseId, sectionId, CurrentUser, gradeId);
                ExamPaperBasicInfo basicInfo = new ExamPaperBasicInfo();
                if (response.Code == 200)
                {
                    basicInfo = new ExamPaperBasicInfo
                    {
                        ExamId = response.Data.ExamId,
                        ExamScore = response.Data.ExamScore,
                        GradeId = response.Data.GradeId,
                        PassScore = response.Data.PassScore,
                        LeftSeconds = response.Data.LeftSeconds,
                        QuestionInfoList = response.Data.QuestionInfoList.Select(x => new QuestionInfo
                        {
                            QuestionId = x.QuestionId,
                            Title = x.Title,
                            Sort = x.Sort,
                            Score = x.Score,
                            QuestionType = x.QuestionType,
                            ParentQuestionId = x.ParentQuestionId,
                            AnswerScore = x.AnswerScore,
                            QuestionAnswerStatus = x.QuestionAnswerStatus,
                        }).ToList(),
                        StudentExamStatus = response.Data.StudentExamStatus,
                        TotalMinutes = response.Data.TotalMinutes,
                        UsedSeconds = response.Data.UsedSeconds,
                    };
                }
                return new ResponseContext<ExamPaperBasicInfo> { Code = response.Code, Msg = response.Msg, Data = basicInfo };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <param name="examType"></param>
        /// <param name="option">0：岗位实训,1：课程实训</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfo(long gradeId, long questionId, int examType = 0, int option = 0)
        {
            if (option == 0)
            {
                return await _service.GetQuestionInfo(gradeId, questionId, CurrentUser);
            }
            else
            {
                ResponseContext<ExamCourseQuestionInfo> rsp = await _courseExercisesService.GetCourseQuestionInfo(gradeId, GetExamType(examType), questionId, CurrentUser);
                ExamQuestionInfo questionInfo = new ExamQuestionInfo();
                if (rsp.Code == 200)
                {
                    questionInfo = new ExamQuestionInfo
                    {
                        AccountEntryStatus = rsp.Data.AccountEntryStatus,
                        AnswerCompareInfo = rsp.Data.AnswerCompareInfo,
                        AnswerResultStatus = rsp.Data.AnswerResultStatus,
                        AnswerValue = rsp.Data.AnswerValue,
                        CalculationType = rsp.Data.CalculationType,
                        Content = rsp.Data.Content,
                        Hint = rsp.Data.Hint,
                        IsBigDataSubmit = rsp.Data.IsBigDataSubmit,
                        IsCollection = rsp.Data.IsCollection,
                        IsCopy = rsp.Data.IsCopy,
                        IsDisorder = rsp.Data.IsDisorder,
                        IsSettled = rsp.Data.IsSettled,
                        LeftSeconds = rsp.Data.LeftSeconds,
                        QuestionContentType = rsp.Data.QuestionContentType,
                        SubQuestion = rsp.Data.SubQuestion != null ? rsp.Data.SubQuestion.Select(x => new ExamQuestionInfo
                        {
                            AccountEntryStatus = x.AccountEntryStatus,
                            AnswerCompareInfo = x.AnswerCompareInfo,
                            AnswerResultStatus = x.AnswerResultStatus,
                            AnswerValue = x.AnswerValue,
                            CalculationType = x.CalculationType,
                            Content = x.Content,
                            Hint = x.Hint,
                            IsBigDataSubmit = x.IsBigDataSubmit,
                            IsCollection = x.IsCollection,
                            IsCopy = x.IsCopy,
                            IsDisorder = x.IsDisorder,
                            IsSettled = x.IsSettled,
                            LeftSeconds = x.LeftSeconds,
                            QuestionContentType = x.QuestionContentType,
                            QuestionId = x.QuestionId,
                            QuestionType = x.QuestionType,
                            Score = x.Score,
                            Sort = x.Sort,
                            StudentAnswer = x.StudentAnswer,
                            StudentExamStatus = x.StudentExamStatus,
                            Title = x.Title,
                            QuestionFile = x.QuestionFile,
                            TopicContent = x.TopicContent,
                            FullContent = x.FullContent,
                            CertificateTopic =x.CertificateTopic,
                            Options = x.Options,
                        }).ToList() : null,
                        CertificateTopic=rsp.Data.CertificateTopic,
                
                        QuestionId = rsp.Data.QuestionId,
                        QuestionType = rsp.Data.QuestionType,
                        Score = rsp.Data.Score,
                        Sort = rsp.Data.Sort,
                        StudentAnswer = rsp.Data.StudentAnswer,
                        StudentExamStatus = rsp.Data.StudentExamStatus,
                        Title = rsp.Data.Title,
                        QuestionFile = rsp.Data.QuestionFile,
                        TopicContent = rsp.Data.TopicContent,
                        FullContent = rsp.Data.FullContent,
                        Options = rsp.Data.Options,
                    };
                }
                return new ResponseContext<ExamQuestionInfo> { Code = rsp.Code, Msg = rsp.Msg, Data = rsp.Code == 200 ? questionInfo : null };
            }
        }

        private CourseExamType GetExamType(int examType)
        {
            CourseExamType courseExam = CourseExamType.CourceTest;
            switch (examType)
            {
                case 0: courseExam = CourseExamType.CourceTest; break;
                case 1: courseExam = CourseExamType.PrepareCourseTest; break;
                case 2: courseExam = CourseExamType.ClassExam; break;
                case 3: courseExam = CourseExamType.ClassTask; break;
                case 4: courseExam = CourseExamType.ClassHomeWork; break;
                case 5: courseExam = CourseExamType.ClassTest; break;
                default:
                    break;
            }
            return courseExam;
        }

        /// <summary>
        /// 提交答案
        /// </summary>
        /// <param name="model">作答信息</param>
        /// <returns>作答成功信息</returns>
        [HttpPost]
        public async Task<ResponseContext<QuestionResult>> SubmitAnswer(AppSubmitAnswerDto model)
        {
            if (model.Option == 0)
            {
                return await _service.SubmitAnswer(model.Answer, CurrentUser);
            }
            else
            {
                CourseQuestionAnswer course = new CourseQuestionAnswer
                {
                    AccountEntryList = model.Answer.AccountEntryList,
                    AccountEntryStatus = model.Answer.AccountEntryStatus,
                    AnswerValue = model.Answer.AnswerValue,
                    GradeDetailId = model.Answer.GradeDetailId,
                    GradeId = model.Answer.GradeId,
                    IsIgnore = model.Answer.IsIgnore,
                    MultiQuestionAnswers = model.Answer.MultiQuestionAnswers != null ? model.Answer.MultiQuestionAnswers.Select(x => new CourseQuestionAnswer
                    {
                        AccountEntryList = x.AccountEntryList,
                        AccountEntryStatus = x.AccountEntryStatus,
                        AnswerValue = x.AnswerValue,
                        GradeDetailId = x.GradeDetailId,
                        GradeId = x.GradeId,
                        IsIgnore = x.IsIgnore,
                        QuestionId = x.QuestionId,
                        QuestionType = x.QuestionType,
                        Sort = x.Sort,
                    }).ToList() : null,
                    QuestionId = model.Answer.QuestionId,
                    QuestionType = model.Answer.QuestionType,
                    Sort = model.Answer.Sort,
                };
                return await _courseExercisesService.SubmitAnswer(course, model.ExamType, CurrentUser);
            }
        }


    }
}