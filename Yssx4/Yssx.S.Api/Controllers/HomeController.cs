﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NPOI.SS.Formula.Functions;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Entity;
using Yssx.S.Dto.Home;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{

    /// <summary>
    /// 云上实训首页
    /// </summary>
    public class HomeController : BaseController
    {
        private IHomeService _service;
        IHttpContextAccessor _httpContextAccessor;
        //PermissionRequirement _requirement;
        public HomeController(IHomeService service, IHttpContextAccessor httpContextAccessor)
        {
            _service = service;
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 会计实训列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<PageResponse<AccountantTrainingDto>> GetAccountantTrainingList([FromQuery]PageRequest model)
        {
            return await _service.GetAccountantTrainingList(model);
        }
        /// <summary>
        /// 经典案例列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<PageResponse<CaseTrainingDto>> GetCaseTrainingList([FromQuery]PageRequest model)
        {
            return await _service.GetCaseTrainingList(model);
        }

        /// <summary>
        /// 课程实训列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<PageResponse<CourseTrainingDto>> GetCourseTrainingList([FromQuery]PageRequest model)
        {
            return await _service.GetCourseTrainingList(model);
        }

        /// <summary>
        /// 获取课程详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<CourseTrainingDto>> GetCourseInfoById(long id)
        {
            return await _service.GetCourseInfoById(id);
        }
        /// <summary>
        /// 情景实训列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<PageResponse<HomeSceneTrainingDto>> GetSceneTrainingList([FromQuery]PageRequest model)
        {
            return await _service.GetSceneTrainingList(model);
        }

        /// <summary>
        /// 统计阅读数
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ip"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> ReadStatistics(long id,string ip)
        {
           // HttpContextAccessor context = new HttpContextAccessor();
            //var ip = _httpContextAccessor.HttpContext?.Connection.RemoteIpAddress.ToString();
          //  ip = HttpContext.GetClientUserIp();
            return await _service.ReadStatistics(id, ip);
        }

        /// <summary>
        /// 提醒
        /// </summary>
        /// <param name="moblie"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> Remind11(string moblie)
        {
            return await _service.Remind11(moblie);
        }


        /// <summary>
        /// 综合实训列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        //[HttpGet]
        //[AllowAnonymous]
        //public async Task<PageResponse<SynthesizeTrainingDto>> GetSynthesizeTrainingList([FromQuery]PageRequest model)
        //{
        //    return await _service.GetSynthesizeTrainingList(model);
        //}
    }
}