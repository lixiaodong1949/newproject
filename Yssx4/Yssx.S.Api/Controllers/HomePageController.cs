﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 首页服务接口
    /// </summary>
    public class HomePageController : BaseController
    {
        private IHomePageService _service;

        /// <summary>
        /// 
        /// </summary>
        public HomePageController(IHomePageService service)
        {
            _service = service;
        }

        /// <summary>
        /// 移动端首页
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<McHomePageDto>> GetMCHomePageInfo()
        {
            return await _service.GetMcHomePageInfo();
        }

        /// <summary>
        /// 广告位信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<BannerInfo>>> GetBannerInfo()
        {
            return await _service.GetBannerInfo();
        }

        /// <summary>
        /// 获取IOS发布信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<ReleaseInfoIOSViewModel>> GetReleaseInfoIOS()
        {
            return await _service.GetReleaseInfoIOS();
        }

    }
}