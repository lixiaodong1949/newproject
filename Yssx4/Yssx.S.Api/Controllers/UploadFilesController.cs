﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.AliyunOss;
using Yssx.S.Dto;
using Yssx.S.Dto.Order;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 上传文件
    /// </summary>
    public class UploadFilesController : BaseController
    {
        private readonly IUploadFileService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public UploadFilesController(IUploadFileService service)
        {
            _service = service;
        }

        /// <summary>
        /// 文件上传
        /// </summary>
        /// <param name="form"></param>
        /// <param name="requestBucketName"></param>
        /// <param name="watermark">是否添加水印(默认不添加)</param>
        /// <param name="iscompressed">是否压缩图片(默认压缩)</param>
        /// <param name="istestSpace"></param> 
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ResponseContext<string> PostFiles(IFormCollection form, string requestBucketName = "freessoul", [FromForm] bool watermark = false, [FromForm] bool iscompressed = true, [FromForm] bool istestSpace = false)
        {
            var file = form.Files.FirstOrDefault();
            var file1 = Request.Form.Files.FirstOrDefault();
            if (file == null)
            {
                if (file1 == null)
                    return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = "没有接收到文件，上传失败" };
                else
                    file = file1;
            }
            return UploadIFormFile(file, requestBucketName, watermark, iscompressed, istestSpace);
        }

        /// <summary>
        /// 文件上传
        /// </summary>
        /// <param name="file"></param>
        /// <param name="requestBucketName"></param>
        /// <param name="watermark">是否添加水印</param>
        /// <param name="iscompressed">是否压缩图片</param>
        /// <param name="istestSpace"></param> 
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ResponseContext<string> UploadIFormFile(IFormFile file, string requestBucketName = "freessoul", [FromForm] bool watermark = false, [FromForm] bool iscompressed = true, [FromForm] bool istestSpace = false)
        {
            var file1 = Request.Form.Files.FirstOrDefault();
            if (file == null)
            {
                if (file1 == null)
                    return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = "没有接收到文件，上传失败" };
                else
                    file = file1;
            }
            Tuple<string, string> tuple = null;
            string msg = "";
            try
            {
                tuple = UploadFormFile(file, requestBucketName, watermark, iscompressed, istestSpace);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            if (tuple == null)
                return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Msg = $"tuple为空,上传失败：{msg}" };

            return new ResponseContext<string> { Code = CommonConstants.SuccessCode, Data = tuple.Item2 };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="requestBucketName"></param>
        /// <param name="watermark"></param>
        /// <param name="iscompressed"></param>
        /// <param name="istestSpace"></param>
        /// <returns></returns>
        private Tuple<string, string> UploadFormFile(IFormFile file, string requestBucketName = "freessoul", bool watermark = false, bool iscompressed = true, bool istestSpace = false)
        {
            using (var ms = file.OpenReadStream())
            {
                return AliyunOssHelper.OssUpload(file.FileName, ms, requestBucketName, watermark, iscompressed, istestSpace);
            }
        }




    }
}
