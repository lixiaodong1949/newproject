﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 产品
    /// </summary>
    public class ProductController : BaseController
    {

        private IProductService service;
        private IProductCouponsService couponsService;
        public ProductController(IProductService _service, IProductCouponsService _couponsService)
        {
            service = _service;
            couponsService = _couponsService;
        }

        #region 抢购商品
        /// <summary>
        /// 查询抢购商品
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<ProductDto>>> GetCasProducts()
        {
            return await service.GetCasProducts();
        }
        #endregion

        #region 产品卡券
        /// <summary>
        /// 验证产品卡券
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ExchangeCouponsCode(string code, string mobilePhone)
        {
            return await couponsService.ExchangeCouponsCode(code, mobilePhone, CurrentUser.Id);
        }
        #endregion

        #region 产品数据
        /// <summary>
        /// 获取指定产品数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<SaveProductDto>> GetProductInfoById(long id)
        {
            return await service.GetProductInfoById(id);
        }
        #endregion




    }
}
