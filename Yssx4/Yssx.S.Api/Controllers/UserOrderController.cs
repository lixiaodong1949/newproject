﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto.Order;
using Yssx.S.IServices.Order;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 用户订单
    /// </summary>
    public class UserOrderController : BaseController
    {
        private readonly IUserOrderService _userOrderService;

        public UserOrderController(IUserOrderService userOrderService)
        {
            _userOrderService = userOrderService;
        }

        ///// <summary>
        ///// 查询订单
        ///// </summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<PageResponse<UserOrderDto>> GetOrderList(GetUserOrdersDto input)
        //{
        //    input.UserId = CurrentUser.Id;
        //    return await _userOrderService.GetOrderList(input);
        //}
    }
}
