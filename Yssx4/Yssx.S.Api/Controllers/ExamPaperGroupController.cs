﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.Dto.Topics;
using Yssx.S.IServices;
using Yssx.S.IServices.Examination;
using Yssx.S.IServices.Topics;
using Yssx.S.ServiceImpl;
using Yssx.S.ServiceImpl.Examination;
using Yssx.S.ServiceImpl.Topics;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 考试分组相关服务
    /// </summary>
    public class ExamPaperGroupController : BaseController
    {
        private IExamPaperGroupService _service;
        public ExamPaperGroupController(IExamPaperGroupService service)
        {
            _service = service;
        }
        /// <summary>
        /// 获取考试分组列表 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<ExamPaperGroupListView>> GetExamPaperGroupListByPage(PageRequest<ExamPaperGroupQueryDto> model)
        {
            return await _service.GetExamPaperGroupListByPage(model,CurrentUser.Id);
        }
        /// <summary>
        /// 获取考试分组信息
        /// </summary>
        /// <param name="model">groupId</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<ExamPaperGroupListView>> GetExamPaperGroupInfo(PageRequest<long> model)
        {
            return await _service.GetExamPaperGroupInfo(model, CurrentUser.Id);
        }
        /// <summary>
        /// 创建分组 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<ExamPaperGroupListView>> CreateExamGroupAsync(ExamPaperGroupUpdateDto model)
        {
            return await _service.CreateExamGroup(model, CurrentUser);
        }
        /// <summary>
        /// 加入分组 
        /// </summary>
        /// <param name="groupId">分组id</param>
        /// <param name="postionId">岗位id</param>
        /// <param name="password">分组密码</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> JoinExamGroup(long groupId, long postionId, string password = "")
        {
            return await _service.JoinExamGroup(groupId, postionId, CurrentUser, password);
        }

        /// <summary>
        /// 切换岗位 
        /// </summary>
        /// <param name="groupId">分组id</param>
        /// <param name="postionId">岗位id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ChangeExamGroupPosition(long groupId, long postionId)
        {
            return await _service.ChangeExamGroupPosition(groupId, postionId, CurrentUser);
        }
        /// <summary>
        /// 退出分组(如果是创建人退出，则解散分组)
        /// </summary>
        /// <param name="groupId">分组Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ExitExamGroup(long groupId)
        {
            return await _service.ExitExamGroup(groupId, CurrentUser.Id);
        }
        /// <summary>
        /// 更改准备状态
        /// </summary>
        /// <param name="groupId">groupId</param>
        /// <param name="IsReady">true:已准备，false:未准备</param>
        /// <returns>bool,true表示分组成员全部准备完毕，否则未全部准备</returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> UpdateReadyStatus(long groupId,bool IsReady = false)
        {
            return await _service.UpdateReadyStatus(groupId, CurrentUser,IsReady);
        }
    }
}