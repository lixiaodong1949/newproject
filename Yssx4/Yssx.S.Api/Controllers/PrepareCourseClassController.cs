﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 课程班级相关Api [Obsolete]
    /// </summary>
    public class PrepareCourseClassController : BaseController
    {
        IPrepareCourseClassService _service;

        public PrepareCourseClassController(IPrepareCourseClassService service)
        {
            _service = service;
        }

        #region 获取教师班级
        /// <summary>
        /// 获取教师班级
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxTeacherClassViewModel>>> GetTeacherClassList([FromQuery]YssxTeacherClassQuery query)
        {
            return await _service.GetTeacherClassList(query, this.CurrentUser);
        }
        #endregion

        #region 绑定课程班级(PC)
        /// <summary>
        /// 绑定课程班级(PC)
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> BindingCourseClassForPC(PrepareCourseClassDto dto)
        {
            return await _service.BindingCourseClassForPC(dto, this.CurrentUser);
        }
        #endregion

        #region 绑定课程班级(App)
        /// <summary>
        /// 绑定课程班级(App)
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> BindingCourseClassForApp(PrepareCourseClassForAppDto dto)
        {
            return await _service.BindingCourseClassForApp(dto, this.CurrentUser);
        }
        #endregion

        #region 解绑单一课程班级
        /// <summary>
        /// 解绑单一课程班级
        /// </summary>
        /// <param name="CourseClassId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> UnlinkCourseClass(long CourseClassId)
        {
            return await _service.UnlinkCourseClass(CourseClassId, this.CurrentUser);
        }
        #endregion

        #region 获取学期班级列表(班级管理)
        /// <summary>
        /// 获取学期班级列表(班级管理)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxSemesterClassViewModel>>> GetSemesterClassList()
        {
            return await _service.GetSemesterClassList(this.CurrentUser);
        }
        #endregion

        #region 根据班级获取学生信息
        /// <summary>
        /// 根据班级获取学生信息
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxCourseClassStudentViewModel>>> GetStudentListByClass([FromQuery]YssxClassStudentQuery query)
        {
            return await _service.GetStudentListByClass(query);
        }
        #endregion

        #region 根据教师课程绑定班级获取学生信息(学生资料)
        /// <summary>
        /// 根据教师课程绑定班级获取学生信息(学生资料)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxCourseClassStudentViewModel>>> GetStudentListByTeacherCourseClass([FromQuery]YssxCourseClassStudentQuery query)
        {
            return await _service.GetStudentListByTeacherCourseClass(query, this.CurrentUser);
        }
        #endregion

        #region 根据某一个课程获取绑定班级的学生实体
        /// <summary>
        /// 根据某一个课程获取绑定班级的学生实体
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxClassStudentByCourseViewModel>>> GetStudentListByCourse([FromQuery]YssxCourseClassStudentByCourseQuery query)
        {
            return await _service.GetStudentListByCourse(query, this.CurrentUser);
        }
        #endregion

        #region 获取教师绑定课程班级列表
        /// <summary>
        /// 获取教师绑定课程班级列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxTeacherClassViewModel>>> GetTeacherBindingCourseClassList()
        {
            return await _service.GetTeacherBindingCourseClassList(this.CurrentUser);
        }
        #endregion

    }
}