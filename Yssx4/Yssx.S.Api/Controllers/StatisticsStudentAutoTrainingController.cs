﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 统计学生自主实训相关Api
    /// </summary>
    public class StatisticsStudentAutoTrainingController : BaseController
    {
        IStatisticsStudentAutoTrainingService _service;

        public StatisticsStudentAutoTrainingController(IStatisticsStudentAutoTrainingService service)
        {
            _service = service;
        }

        #region 获取学校企业(案例)列表
        /// <summary>
        /// 获取学校企业(案例)列表
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SchoolCaseViewModel>>> GetSchoolCaseList()
        {
            return await _service.GetSchoolCaseList(this.CurrentUser);
        }
        #endregion

        #region 获取学生自主实训已完成列表
        /// <summary>
        /// 获取学生自主实训已完成列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<StudentAutoTrainingFinishedViewModel>> GetStudentAutoTrainingFinishedList([FromQuery]StudentAutoTrainingQuery query)
        {
            return await _service.GetStudentAutoTrainingFinishedList(query, this.CurrentUser);
        }
        #endregion

        #region 导出学生自主实训已完成列表
        /// <summary>
        /// 导出学生自主实训已完成列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExportStudentAutoTrainingFinishedList([FromQuery]StudentAutoTrainingQuery query)
        {
            Framework.Authorizations.UserTicket userInfo = new Framework.Authorizations.UserTicket
            {
                Id = query.UserId
            };

            //查询学生自主实训已完成列表
            ResponseContext<List<StudentAutoTrainingFinishedViewModel>> sourceData = await _service.ExportStudentAutoTrainingFinishedList(query, userInfo);

            List<ExportStudentAutoTrainingFinishedViewModel> list = new List<ExportStudentAutoTrainingFinishedViewModel>();
            if (sourceData.Data != null && sourceData.Data.Count > 0)
            {
                sourceData.Data.ForEach(x =>
                {
                    ExportStudentAutoTrainingFinishedViewModel model = new ExportStudentAutoTrainingFinishedViewModel
                    {
                        StudentName = x.StudentName,
                        ClassName = x.ClassName,
                        CaseName = x.CaseName,
                        Score = x.Score,
                        UsedSecondsDesc = x.UsedSecondsDesc,
                        SubmitTime = x.SubmitTime ?? DateTime.Now
                    };

                    list.Add(model);
                });
            }

            byte[] ms = null;
            if (list.Count > 0)
                ms = NPOIHelper<ExportStudentAutoTrainingFinishedViewModel>.OutputExcel(list, new string[] { "成绩记录" });
            else
                return NotFound();

            return File(ms, "application/ms-excel", "学生已完成自主实训成绩记录.xlsx");
        }
        #endregion

    }
}