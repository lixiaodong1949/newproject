﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 常见问题
    /// </summary>
    public class FAQController : BaseController
    {
        private IFAQService _service;
        public FAQController()
        {
            _service = new FAQService();
        }

        /// <summary>
        /// 获取问题分类列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<PageResponse<FAQClassDto>> GetFAQClassByPage(GetFAQClassByPageRequest model)
        {
            return await _service.GetFAQClassByPage(model);
        }

        /// <summary>
        /// 获取常见问题列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<PageResponse<FAQInfoDto>> GetFAQInfoByPage(GetFAQInfoByPageRequest model)
        {
            return await _service.GetFAQInfoByPage(model);
        }

        /// <summary>
        /// 获取指定常见问题
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<FAQInfoOneDto>> GetFAQInfoById(long id)
        {
            return await _service.GetFAQInfoById(id);
        }

        /// <summary>
        /// 获取角色分类问题列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<FAQRoleInfoDto>> GetFAQRoleList()
        {
            return await _service.GetFAQRoleList();
        }


    }
}