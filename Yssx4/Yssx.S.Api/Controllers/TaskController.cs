﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.Dto.Task;
using Yssx.S.IServices.Topics;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 课业任务
    /// </summary>
    public class TaskController : BaseController
    {
        private ITaskService _taskService;
        /// <summary>
        /// 
        /// </summary>
        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }


        #region 课业任务

        #region old
        /// <summary>
        /// 查询课业任务列表 - 教师端
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<TaskView>> GetTaskByPage(GetTaskByPageRequest model)
        {
            return await _taskService.GetTaskByPage(model, CurrentUser.Id);
        }

        /// <summary>
        /// 查询课业任务列表 - 学生端
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<StudentTaskView>> GetStudentTaskByPage(GetTaskByPageRequest model)
        {
            return await _taskService.GetStudentTaskByPage(model, CurrentUser.Id);
        }

        /// <summary>
        /// 查询指定课业任务信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TaskInfoDto>> GetTaskById(long taskId)
        {
            return await _taskService.GetTaskById(taskId, CurrentUser.Id);
        }

        /// <summary>
        /// 修改课业任务
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EditTask(TaskDto model)
        {
            return await _taskService.EditTask(model, CurrentUser.Id);
        }
        #endregion

        #region new
        /// <summary>
        /// 查询课业任务列表 - 教师端 - new
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<TaskView>> GetTaskByPageAll(GetTaskByPageAllRequest model)
        {
            return await _taskService.GetTaskByPageAll(model, CurrentUser.Id);
        }

        /// <summary>
        /// 查询课业任务列表 - 学生端 - new
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<StudentTaskView>> GetStudentTaskByPageAll(GetTaskByPageAllRequest model)
        {
            return await _taskService.GetStudentTaskByPageAll(model, CurrentUser.Id);
        }

        /// <summary>
        /// 查询指定课业任务信息 - new
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TaskInfoDto>> GetTaskInfoById(long taskId)
        {
            return await _taskService.GetTaskInfoById(taskId, CurrentUser.Id);
        }

        /// <summary>
        /// 修改课业任务 - new
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EditTaskInfo(TaskDto model)
        {
            return await _taskService.EditTaskInfo(model, CurrentUser.Id);
        }
        #endregion

        /// <summary>
        /// 查询指定课业任务状态
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<LessonsTaskStatus>> GetTaskStatusById(long taskId, long GradeId)
        {
            return await _taskService.GetTaskStatusById(taskId, GradeId);
        }

        /// <summary>
        /// 删除课业任务
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveTask(long id)
        {
            return await _taskService.RemoveTask(id, CurrentUser.Id);
        }

        /// <summary>
        /// 结束课业任务
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ClosedTask(long id)
        {
            return await _taskService.ClosedTask(id, CurrentUser.Id);
        }

        #endregion

        #region 组卷 - old
        /// <summary>
        /// 手动组卷
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> ManualOrgPaper(TaskManualOrgPaperDto model)
        {
            return await _taskService.ManualOrgPaper(model, CurrentUser.Id, CurrentUser.TenantId);
        }

        /// <summary>
        /// 查询题目类型数量统计（1.知识点 2.课程章节）
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<TaskTopicGroupDto>> GetTaskTopicById(GetTaskTopicByIdRequest model)
        {
            return await _taskService.GetTaskTopicById(model, CurrentUser.Id);
        }

        /// <summary>
        /// 智能组卷
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> RandomOrgPaper(TaskRandomOrgPaperDto model)
        {
            return await _taskService.RandomOrgPaper(model, CurrentUser.Id, CurrentUser.TenantId);
        }
        #endregion

        #region 组卷 - new
        /// <summary>
        /// 手动组卷 - new
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> ManualCreatePaper(TaskManualCreatePaperDto model)
        {
            return await _taskService.ManualCreatePaper(model, CurrentUser.Id, CurrentUser.TenantId);
        }

        /// <summary>
        /// 查询题目类型数量统计（1.知识点 2.课程章节） - new
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<TaskTopicGroupDto>> GetTaskTopicGroup(GetTaskTopicGroupRequest model)
        {
            return await _taskService.GetTaskTopicGroup(model, CurrentUser.Id);
        }

        /// <summary>
        /// 智能组卷 - new
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> RandomCreatePaper(TaskRandomCreatePaperDto model)
        {
            return await _taskService.RandomCreatePaper(model, CurrentUser.Id, CurrentUser.TenantId);
        }
        #endregion

        #region 课业统计

        /// <summary>
        /// 课业任务统计信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TaskGradeReportDto>> GetTaskGradeReport(long taskId)
        {
            return await _taskService.GetTaskGradeReport(taskId, CurrentUser.Id);
        }
        /// <summary>
        /// 课业任务统计信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TaskGradeReportDto>> GetCourseGradeDetailByTask(long taskId, string classIds)
        {
            return await _taskService.GetCourseGradeDetailByTask(taskId, classIds, CurrentUser.Id);
        }

        /// <summary>
        /// 课业任务统计信息 - 移动端
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<TaskGradeReportGradeDto>> GetTaskGradeReportByApp(TaskGradeReportByAppDto model)
        {
            return await _taskService.GetTaskGradeReportByApp(model);
        }

        /// <summary>
        /// 导出课业任务统计数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExportTaskGrade(long taskId)
        {
            ResponseContext<TaskGradeReportDto> sourceData = await _taskService.ExportTaskGrade(taskId);
            var fileName = sourceData.Data.Name;
            var rGradeData = sourceData.Data.GradeDetail;
            var selectData = new List<ExportTaskGradeDto>();
            foreach (var item in rGradeData)
            {
                ExportTaskGradeDto rGradeDetail = new ExportTaskGradeDto
                {
                    StudentNo = item.StudentNo,
                    UserName = item.UserName,
                    ClassName = item.ClassName,
                    Score = item.Score,
                    CorrectCount = item.CorrectCount,
                    ErrorCount = item.ErrorCount,
                    BlankCount = item.BlankCount,
                    UsedSeconds = item.IsAbsent ? "缺考" : item.UsedSecondsDesc,
                    Ranking = item.Ranking
                };
                selectData.Add(rGradeDetail);
            }

            byte[] ms = null;
            if (selectData != null && selectData.Count > 0)
                ms = NPOIHelper<ExportTaskGradeDto>.OutputExcel(selectData, new string[] { "课业任务统计" });
            else
                return NotFound();

            return File(ms, "application/ms-excel", fileName + ".xlsx");
        }

        #endregion

        #region 课堂任务

        /// <summary>
        /// 查询课堂测验列表 - 教师端 - new
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<TestingTaskView>> GetTestingTaskByPage(GetTestingTaskByPageDto model)
        {
            return await _taskService.GetTestingTaskByPage(model, CurrentUser.Id);
        }

        /// <summary>
        /// 查询课堂测验列表 - 学生端 - new
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<StudentTestingTaskView>> GetStudentTestingTaskByPage(GetTestingTaskByPageDto model)
        {
            return await _taskService.GetStudentTestingTaskByPage(model, CurrentUser.Id);
        }


        #region old
        /// <summary>
        /// 开始做题
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<string>> StartCourseTestingTask(TaskDto model)
        {
            return await _taskService.StartCourseTestingTask(model, this.CurrentUser.Id);
        }

        /// <summary>
        /// 收题
        /// </summary>
        /// <param name="TaskID"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<CourseTestingTaskAnalysis>> StopCourseTestingTask(List<long> TaskID)
        {
            return await _taskService.StopCourseTestingTask(TaskID, this.CurrentUser.Id);
        }

        /// <summary>
        /// 刷新查看
        /// </summary>
        /// <param name="TaskID"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<CourseTestingTaskAnalysis>> RefreshCourseTestingTask(List<long> TaskID)
        {
            return await _taskService.RefreshCourseTestingTask(TaskID);
        }

        ///// <summary>
        ///// 讲解习题
        ///// </summary>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<ResponseContext<object>> ExplainCourseTestingTask(long TopicID)
        //{
        //    return await _taskService.ExplainCourseTestingTask(TopicID);
        //}

        /// <summary>
        /// 获取课堂任务题目列表
        /// </summary>
        /// <param name="TaskID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<object>> GetCourseTestingTaskTopicList(long TaskID)
        {
            return await _taskService.GetCourseTestingTaskTopicList(TaskID);
        }

        /// <summary>
        /// 获取我的任务课程
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<MyCourseTaskDto>> GetMyCourseTaskByPage(int pageIndex, int pageSize)
        {
            return await _taskService.GetMyCourseTaskByPage(this.CurrentUser.Id, pageIndex, pageSize);
        }

        /// <summary>
        /// 获取课程任务试卷题目列表
        /// </summary>
        /// <param name="TaskID"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourseTaskExamTopicList(long TaskID)
        {
            return await _taskService.GetCourseTaskExamTopicList(TaskID);
        }

        #endregion

        #endregion

        /// <summary>
        /// 重启任务
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="endDate"></param>
        /// <param name="isCanCheckExam"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetTaskStatus(long taskId, DateTime endDate, bool isCanCheckExam)
        {
            return await _taskService.SetTaskStatus(taskId, endDate, isCanCheckExam);
        }

        /// <summary>
        /// 重启任务 APP
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SetTaskStatusApp(SetTaskStatusDto dto)
        {
            return await _taskService.SetTaskStatus(dto.TaskId, dto.EndDate, dto.IsCanCheckExam);
        }

        /// <summary>
        /// 重启学生做题
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetStudentStatus(long gradeId)
        {
            return await _taskService.SetStudentStatus(gradeId);
        }

        /// <summary>
        /// 一键交卷
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> BatchSubmitExam(long taskId)
        {
            return await _taskService.BatchSubmitExam(taskId, CurrentUser.Id);
        }

        #region 打开或关闭课程任务学生查看试卷入口
        /// <summary>
        /// 打开或关闭课程任务学生查看试卷入口
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> OpenOrCloseCourseTaskCheckExam(CloseCheckExamRequestModel request)
        {
            return await _taskService.OpenOrCloseCourseTaskCheckExam(request, CurrentUser.Id);
        }
        #endregion

    }
}