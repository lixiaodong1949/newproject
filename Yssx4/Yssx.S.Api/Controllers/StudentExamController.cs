﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.Dto.SceneTraining;
using Yssx.S.Dto.Topics;
using Yssx.S.IServices;
using Yssx.S.IServices.Examination;
using Yssx.S.IServices.Topics;
using Yssx.S.ServiceImpl;
using Yssx.S.ServiceImpl.Examination;
using Yssx.S.ServiceImpl.Topics;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 考试相关服务
    /// </summary>
    public class StudentExamController : BaseController
    {
        private IStudentExamService _service;
        public StudentExamController(IStudentExamService service)
        {
            _service = service;
        }
        /// <summary>
        /// 获取自主练习试卷列表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="CypName">公司名称</param>
        /// <param name="IdtName">行业名称</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<PracticeExamListView>> GetPracticeExamListByPage(Practice model)
        {
            return await _service.GetPracticeExamListByPage(model, CurrentUser);
        }
        /// <summary>
        /// 获取真题模拟试卷列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<EmulationExamListView>> GetEmulationExamListByPage(PageRequest model)
        {
            return await _service.GetEmulationExamListByPage(model, CurrentUser);
        }
        /// <summary>
        /// 预览试卷基本信息--接口方法
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperBasicInfo>> GetExamPaperInfoView(long examId)
        {
            return await _service.GetExamPaperInfoView(examId, CurrentUser);
        }
        /// <summary>
        /// 预览试卷作答信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperBasicInfo>> GetExamPaperBasicInfoView(long gradeId)
        {
            return await _service.GetExamPaperBasicInfoView(gradeId, CurrentUser);
        }
        /// <summary>
        /// 预览试卷题目 信息--接口方法
        /// </summary>
        /// <param name="examId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamQuestionInfo>> GetExamQuestionInfoView(long examId, long questionId)
        {
            return await _service.GetExamQuestionInfoView(examId, questionId, CurrentUser);
        }
        /// <summary>
        /// 预览题目作答信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfoView(long gradeId, long questionId)
        {
            return await _service.GetQuestionInfoView(gradeId, questionId, CurrentUser);
        }
        /// <summary>
        /// 预览题目信息
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <param name="userTicket"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamQuestionInfo>> GetQuestionBasicInfoView(long questionId)
        {
            return await _service.GetQuestionBasicInfoView(questionId);
        }
        /// <summary>
        /// 自主练习清空作答记录
        /// </summary>
        /// <param name="examId">examId>0，表示只清空当前测试的记录，否则清空所有测试记录</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ClearExamGradeInfo(long examId)
        {
            return await _service.ClearExamGradeInfo(CurrentUser,examId);
        }
        /// <summary>
        /// 自主练习清空单条作答记录
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ClearGradeInfo(long gradeId)
        {
            return await _service.ClearGradeInfo(gradeId, CurrentUser);
        }

        /// <summary>
        /// 获取试卷信息（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <param name="examId"></param>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperBasicInfo>> GetExamPaperBasicInfo(long examId, long gradeId = 0)
        {
            return await _service.GetExamPaperBasicInfo(examId, CurrentUser,gradeId);
        }

        /// <summary>
        /// 获取题目信息
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfo(long gradeId, long questionId)
        {
            return await _service.GetQuestionInfo(gradeId, questionId, CurrentUser);
        }
        /// <summary>
        /// 提交答案
        /// </summary>
        /// <param name="model">作答信息</param>
        /// <returns>作答成功信息</returns>
        [HttpPost]
        public async Task<ResponseContext<QuestionResult>> SubmitAnswer(QuestionAnswer model)
        {
            return await _service.SubmitAnswer(model, CurrentUser);
        }
        /// <summary>
        /// 提交考卷 
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="isAutoSubmit">是否自动提交，默认false</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GradeInfo>> SubmitExam(long gradeId, bool isAutoSubmit = false)
        {
            return await _service.SubmitExam(gradeId, CurrentUser,isAutoSubmit);
        }
        /// <summary>
        /// 一键交卷
        /// </summary>
        /// <param name="examId"></param>
        [HttpGet]
        public async Task<ResponseContext<long>> SubmitExamBatch(long examId)
        {
            return await _service.SubmitExamBatch(examId);
        }
        /// <summary>
        /// 自动交卷-定时任务
        /// </summary>
        [HttpGet]
        public async Task<ResponseContext<long>> SubmitExamTask()
        {
            return await _service.SubmitExamTask(CurrentUser);
        }
        /// <summary>
        /// 行业下拉列表
        /// </summary>
        [HttpGet]
        public async Task<ResponseContext<List<IndustryDetails>>> Industry()
        {
            return await _service.Industry();
        }
        /// <summary>
        /// 4个实训 开关
        /// </summary>
        /// <param name="Id">实训Id</param>
        /// <param name="OnOff">true学生可见 false学生不可见</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> Switch(long Id, bool OnOff)
        {
            return await _service.Switch(Id,OnOff, CurrentUser.Id);
        }
        /// <summary>
        /// 业务场景实训列表
        /// </summary>
        /// <param name="dto">搜索</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<BusinessTraining>> TrainingServiceList(PracticeScene dto)
        {
            return await _service.TrainingServiceList(dto, CurrentUser);
        }
        /// <summary>
        /// 获取当前用户课程（自己的课程和购买的课程）
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<CourseInfoList>> GetCourseInfoByUser(PracticeCourse dto)
        {
            return await _service.GetCourseInfoByUser(dto, CurrentUser);
        }

        /// <summary>
        /// 获取考证课程
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<CourseInfoList>> GetZhenShuCourseList()
        {
            return await _service.GetZhenShuCourseList(CurrentUser);
        }

        /// <summary>
        /// 获取 岗位实训列表
        /// </summary>
        [HttpPost]
        public async Task<ResponseContext<List<PositionPracticeInfo>>> GetPositionPracticeList(PositionPracticeDto model)
        {
            return await _service.GetPositionPracticeList(model, CurrentUser);
        }

        /// <summary>
        ///获取 岗位/课程绑定的班级列表
        /// </summary>
        [HttpGet]
        public async Task<ResponseContext<List<BindClassInfo>>> GetBindClassList()
        {
            return await _service.GetBindClassList(CurrentUser);
        }

        /// <summary>
        /// 添加 更新 岗位/课程实训
        /// </summary>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdatePosition(PositionBindClassDto model)
        {
            return await _service.AddUpdatePosition(model, CurrentUser);
        }

        /// <summary>
        /// 获取 课程实训列表
        /// </summary>
        /// <param name="type">0老平台 1新平台</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<CoursePracticeInfo>>> GetCoursePracticeList(int type)
        {
            return await _service.GetCoursePracticeList(type,CurrentUser);
        }

        /// <summary>
        /// 章 根据课程id获取章列表
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <param name="classId">班级Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CourseChapterInfo>>> GetChapterList(long courseId, long classId)
        {
            return await _service.GetChapterList(courseId, classId, CurrentUser);
        }

        /// <summary>
        /// 获取 自主实训操作记录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<OperationRecordInfo>> OperationRecord(OperationRecordDto model)
        {
            return await _service.OperationRecord(model, CurrentUser);
        }

        /// <summary>
        /// 获取 未绑定列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<UnboundClassInfo>>> GetUnboundClass()
        {
            return await _service.GetUnboundClass(CurrentUser);
        }
    }
}