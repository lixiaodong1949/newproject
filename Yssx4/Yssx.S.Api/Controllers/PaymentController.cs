﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using Yssx.S.Dto.Order;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 支付
    /// </summary>
    public class PaymentController : BaseController
    {
        private readonly IPaymentService _paymentService;

        public PaymentController(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        /// <summary>
        /// 支付宝支付
        /// </summary>
        /// <returns>支付表单</returns>
        [HttpPost]
        public async Task<ResponseContext<AlipayResponse>> Alipay(CreateRtpOrderDto input)
        {
            return await _paymentService.Alipay(input, CurrentUser);
        }

        /// <summary>
        /// 支付宝手机支付
        /// </summary>
        /// <returns>支付表单</returns>
        [HttpPost]
        public async Task<ResponseContext<AlipayResponse>> AlipayWap(CreateRtpOrderDto input)
        {
            return await _paymentService.AlipayWap(input, CurrentUser);
        }

        /// <summary>
        /// 支付宝异步回调
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public void NotifyAliapy()
        {
            Dictionary<string, string> input = new Dictionary<string, string>();
            foreach (var key in Request.Form.Keys)
            {
                var value = Request.Form[key];
                input.Add(key, value);
            }
            CommonLogger.Error("支付宝回调：" + JsonConvert.SerializeObject(input));
            _paymentService.AlipayNotify(input);
        }

        /// <summary>
        /// 微信支付
        /// </summary>
        /// <param name="input"></param>
        /// <returns>支付二维码地址</returns>
        [HttpPost]
        public async Task<ResponseContext<WebchatResponse>> Webchat(CreateRtpOrderDto input)
        {
            return await _paymentService.Webchat(input, CurrentUser);
        }

        /// <summary>
        /// 微信H5支付
        /// </summary>
        /// <param name="input"></param>
        /// <returns>支付二维码地址</returns>
        [HttpPost]
        public async Task<ResponseContext<WebchatResponse>> WebchatWap(CreateRtpOrderDto input)
        {
            return await _paymentService.WebchatWap(input, CurrentUser);
        }

        /// <summary>
        /// 微信异步回调
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public void NotifyWebchat()
        {
            using (StreamReader reader = new StreamReader(Request.Body))
            {
                var xmlString = reader.ReadToEnd();
                CommonLogger.Error("微信回调：" + xmlString);
                _paymentService.WebchatNotify(xmlString);
            }
        }

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<RtpOrderDto>> GetOrderList(GetRtpOrderDto input)
        {
            input.UserId = CurrentUser.Id;
            return await _paymentService.GetOrderList(input);
        }

        /// <summary>
        /// 获取是否已下过订单
        /// </summary>
        /// <param name="caseId">案例id</param>
        /// <param name="currentUser"></param>
        /// <returns>True 已支付过；False 未支付过</returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> GetCaseOrderStatus(long caseId)
        {
            return await _paymentService.GetCaseOrderStatus(caseId,CurrentUser);
        }
    }
}