﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 印章相关Api
    /// </summary>
    public class StampController : BaseController
    {
        IStampService _service;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public StampController(IStampService service)
        {
            _service = service;
        }



        /// <summary>
        /// 保存印章
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveStamp(StampRequest model)
        {
            return await _service.SaveStamp(model,CurrentUser.Id);
        }

        /// <summary>
        /// 删除印章
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteStamp(long id)
        {
            return await _service.DeleteStamp(id);
        }

        /// <summary>
        /// 获取印章列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<StampDto>> GetStampList([FromQuery]StampListRequest model)
        {
            return await _service.GetStampList(model,CurrentUser.Id);
        }

    }
}