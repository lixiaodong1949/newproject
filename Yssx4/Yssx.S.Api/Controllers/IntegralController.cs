using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Microsoft.AspNetCore.Authorization;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 用户积分
    /// </summary>
    public class IntegralController : BaseController
    {
        private IIntegralService _service;

        public IntegralController(IIntegralService service)
        {
            _service = service;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<IntegralDetailsDto>> Get(long id)
        {
            var response = new ResponseContext<IntegralDetailsDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Get(id);

            return response;
        }

        /// <summary>
        /// 获取用户当前可用总积分
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<long>> GetSum()
        {
            var response = await _service.GetSum(CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 获取用户积分排行
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ListResponse<IntegralGetTopResponseDto>> GetTop([FromQuery] PageRequest page)
        {
            var response = await _service.GetTop(page);

            return response;
        }
        /// <summary>
        /// 获取用户的积分详情列表信息 分页
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<IntegralDetailsUserPageResponseDto>> DetailsListByUserId([FromQuery] IntegralDetailsUserRequestDto query)
        {
            var response = new PageResponse<IntegralDetailsUserPageResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }
            query.UserId = CurrentUser.Id;

            response = await _service.DetailsListByUserId(query);

            return response;
        }

        /// <summary>
        /// 获取当前用户积分总分和商品兑换总数
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<IntegralStatistics>> GetStatistics()
        {
            var response = await _service.GetStatistics(CurrentUser.Id);

            return response;
        }
    }
}