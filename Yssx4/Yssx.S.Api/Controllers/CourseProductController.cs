﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 商品课程
    /// </summary>
    public class CourseProductController : BaseController
    {
        private ICourseProductService _service;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public CourseProductController(ICourseProductService service)
        {
            _service = service;
        }

        /// <summary>
        /// 获取课程章节
        /// </summary>
        /// <param name="cid">课程Id</param>
        /// <param name="uid">用户Id</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<YssxSectionDto>>> GetSectionListByUid(long cid, long uid)
        {
            return await _service.GetSectionListByUid(cid, uid);
        }

        /// <summary>
        /// 课程课件
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="uid"></param>
        /// <param name="fileType">1:课件，2：视频,3:教案</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesListByUid(long cid, long uid, int fileType)
        {
            return await _service.GetSectionFilesListByUid(cid, uid, fileType);
        }

        /// <summary>
        /// APP获取课程课件
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="fileType">1:课件，2：视频,3:教案</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesListById(long cid, int fileType)
        {
            return await _service.GetSectionFilesListById(cid, fileType);
        }

        /// <summary>
        ///  APP获取课程案例场景
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CourseCaseDto>>> GetCourseCaseListById(long cid)
        {
            return await _service.GetCourseCaseListById(cid);
        }

        /// <summary>
        /// 课程习题
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<YssxSectionTopicDto>>> GetSectionTopicListByUid(long sid, long uid)
        {
            return await _service.GetYssxSectionTopicListByUid(sid, uid);
        }

        /// <summary>
        /// 课程详情首页
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<int>> CourseDetailsPage(long cid, long uid) 
        {
            return await _service.CourseDetailsPage(cid, uid);
        }
    }
}