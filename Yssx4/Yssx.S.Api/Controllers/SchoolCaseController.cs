﻿//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Mvc;
//using System.Threading.Tasks;
//using Yssx.Framework;
//using Yssx.M.IServices;
//using Yssx.S.Dto;
//using Yssx.S.Dto.ExamPaper;
//using Yssx.S.Dto.Means;
//using Yssx.S.IServices;
//using Yssx.S.IServices.Examination;
//using Yssx.S.Pocos;
//using Yssx.S.ServiceImpl;

//namespace Yssx.S.Api.Controllers
//{
//    /// <summary>
//    /// 场景实训做题相关服务
//    /// </summary>
//    public class SchoolCaseController : BaseController
//    {
//        private ISchoolCaseService _schoolCaseService;
//        public SchoolCaseController(ISchoolCaseService schoolCaseService)
//        {
//            _schoolCaseService = schoolCaseService;
//        }


//        /// <summary>
//        /// 创建订单
//        /// </summary>
//        /// <param name="model"></param>
//        /// <returns></returns>
//        [HttpPost]
//        public async Task<ResponseContext<bool>> AddCaseOrder(CaseOrderRequest model)
//        {
//            return await _schoolCaseService.AddCaseOrder(model);
//        }
//        /// <summary>
//        /// 给学校添加/修改案例（添加是批量，修改时是单条记录，对应的caseids传一个就好了）
//        /// </summary>
//        /// <param name="model"></param>
//        /// <returns></returns>
//        [HttpPost]
//        public async Task<ResponseContext<bool>> SchoolCaseSet(SchoolCaseRequest model)
//        {
//            return await _schoolCaseService.SchoolCaseSet(model,CurrentUser.Id);
//        }
//        /// <summary>
//        /// 开启案例（更改状态为开启，并生成试卷）
//        /// </summary>
//        /// <param name="id"></param>
//        /// <param name="status"></param>
//        /// <returns></returns>
//        [HttpPost]
//        public async Task<ResponseContext<bool>> OpenSchoolCase(ShoolCaseStatusSetRequest model)
//        {
//            return await _schoolCaseService.OpenSchoolCase(model, CurrentUser.Id);
//        }
//    }
//}