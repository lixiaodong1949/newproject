﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 课程备课相关Api [Obsolete]
    /// </summary>
    public class PrepareCourseController : BaseController
    {
        IPrepareCourseService _service;

        public PrepareCourseController(IPrepareCourseService service)
        {
            _service = service;
        }

        #region 选中备课
        /// <summary>
        /// 选中备课
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> SelectedLessonPrepare(SelectedLessonPrepareDto dto)
        {
            return await _service.SelectedLessonPrepare(dto, this.CurrentUser);
        }
        #endregion

        #region 取消(删除)选中备课
        /// <summary>
        /// 取消(删除)选中备课
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UncheckLessonPrepare(List<long> ids)
        {
            return await _service.UncheckLessonPrepare(ids, this.CurrentUser);
        }
        #endregion

        #region 获取备课课程列表
        /// <summary>
        /// 获取备课课程列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxPrepareCourseViewModel>> GetPrepareCourseList([FromQuery]YssxPrepareCourseQuery query)
        {
            return await _service.GetPrepareCourseList(query, this.CurrentUser);
        }
        #endregion

        #region 编辑备课课程
        /// <summary>
        /// 编辑备课课程
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EditPrepareCourse(PrepareCourseDto dto)
        {
            return await _service.EditPrepareCourse(dto, this.CurrentUser);
        }
        #endregion

        #region 编辑备课课程学期
        /// <summary>
        /// 编辑备课课程学期
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EditPrepareCourseSemester(PrepareCourseSemesterDto dto)
        {
            return await _service.EditPrepareCourseSemester(dto, this.CurrentUser);
        }
        #endregion

    }
}