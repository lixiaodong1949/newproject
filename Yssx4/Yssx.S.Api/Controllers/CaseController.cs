﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Topics;
using Yssx.S.IServices;
using Yssx.S.IServices.Topics;
using Yssx.S.ServiceImpl;
using Yssx.S.ServiceImpl.Topics;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 案例相关服务
    /// </summary>
    public class CaseController : BaseController
    {
        private ICaseService _service;
        public CaseController(ICaseService service)
        {
            _service = service;
        }

        /// <summary>
        /// 新增案例
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveCase(CaseRequest model)
        {
            return await _service.SaveCase(model);
        }
        /// <summary>
        /// 根据id删除
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCase(long caseId)
        {
            return await _service.DeleteCase(caseId);
        }
        /// <summary>
        /// 查询单个对象
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CaseDto>> GetCase(long caseId)
        {
            return await _service.GetCase(caseId);
        }
        /// <summary>
        /// 查询案例列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<CaseDto>> GetCaseList(CaseListRequest model)
        {
            return await _service.GetCaseList(model);
        }
        /// <summary>
        /// 查询案例列表+区域
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<CaseDto>> GetCaseAreaList(CaseListRequest model)
        {
            return await _service.GetCaseAreaList(model);
        }
        /// <summary>
        /// 查找所有，返回案例id和名称
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CaseNameDto>>> GetCaseNameList()
        {
            return await _service.GetCaseNameList();
        }

        /// <summary>
        /// 复制案例
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> CloneCase(CloneCaseRequest model)
        {
            return await _service.CloneCase(model,CurrentUser.Id);
        }
        /// <summary>
        /// 一键更新案例下分录题的AnswerValue
        /// </summary>
        /// <param name="caseId">id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ReplaceAnswerValue(long caseId)
        {
            return await _service.ReplaceAnswerValue(caseId, CurrentUser.Id);
        }
        /// <summary>
        /// 设置案例的区域
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetCaseRegion(long id, string region)
        {
            return await _service.SetCaseRegion(id,region,CurrentUser.Id);
        }
        /// <summary>
        /// 修改案例场景实训封面图
        /// </summary>
        /// <param name="id">案例id</param>
        /// <param name="fileUrl">封面图地址</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetCaseSceneImg(long id,string fileUrl)
        {
            return await _service.SetCaseSceneImg(id, fileUrl);
        }

        #region 获取教师购买/赠送的实训套题列表(只获取有行业的套题)
        /// <summary>
        /// 获取教师购买/赠送的实训套题列表(只获取有行业的套题)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxJobTrainingViewModel>>> GetTeacherJobTrainingList()
        {
            return await _service.GetTeacherJobTrainingList(this.CurrentUser);
        }
        #endregion

        #region 获取教师购买/赠送的实训套题列表(App使用)(只获取有行业的套题)
        /// <summary>
        /// 获取教师购买/赠送的实训套题列表(App使用)(只获取有行业的套题)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxJobTrainingViewModelForApp>>> GetTeacherJobTrainingListForApp(int year)
        {
            return await _service.GetTeacherJobTrainingListForApp(this.CurrentUser, year);
        }
        #endregion

        //[HttpGet]
        //[AllowAnonymous]
        //public async Task<string> GetCurrentThreadIdAsync()
        //{

        //    return await Task.Run(()=>
        //    {
        //        Thread.Sleep(4000);
        //       return Thread.CurrentThread.ManagedThreadId.ToString();
        //    });
        //}
        //[HttpGet]
        //[AllowAnonymous]
        //public string GetCurrentThreadId()
        //{
        //    Thread.Sleep(4000);
        //    return  Thread.CurrentThread.ManagedThreadId.ToString();
        //}
        //[HttpGet]
        //[AllowAnonymous]
        //public string GetThreadIdTest(int sleepTime)
        //{
        //    var info = string.Format("api执行线程:{0}", Thread.CurrentThread.ManagedThreadId);
        //    //System.Threading.Thread.Sleep(sleepTime);
        //    var infoTask = TaskCaller(sleepTime).Result;
        //    var infoFinished = string.Format("api执行线程（task调用完成后）:{0}", Thread.CurrentThread.ManagedThreadId);
        //    return string.Format("{0},{1},{2}", info,infoTask, infoFinished);
        //    //return "睡眠时间（秒）：" + sleepTime;
        //}

        //[HttpGet]
        //[AllowAnonymous]
        //public async Task<string> GetThreadIdTestAsync(int sleepTime)
        //{
        //    var info = string.Format("api执行线程:{0}", Thread.CurrentThread.ManagedThreadId);
        //    //await TaskCaller(sleepTime);
        //    var infoTask=await TaskCaller(sleepTime);
        //    var infoTaskFinished = string.Format("api执行线程（task调用完成后）:{0}", Thread.CurrentThread.ManagedThreadId);
        //    return string.Format("{0},{1},{2}", info, infoTask, infoTaskFinished);
        //    //return "Async睡眠时间（秒）：" + sleepTime;
        //}
        //private async Task<string> TaskCaller(int sleepTime)
        //{
        //    await Task.Delay(sleepTime);
        //    return string.Format("task 执行线程:{0}", Thread.CurrentThread.ManagedThreadId);
        //}
    }
}