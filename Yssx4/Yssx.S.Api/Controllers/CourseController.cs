﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.OptimizeContent;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 课程
    /// </summary>
    public class CourseController : BaseController
    {
        private ICourseService _service;

        /// <summary>
        /// 
        /// </summary>
        public CourseController(ICourseService service)
        {
            _service = service;
        }

        /// <summary>
        /// 新增,修改课程
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> AddOrEditCourse(YssxCourseDto dto)
        {
            return await _service.AddOrEditCourse(dto, this.CurrentUser);
        }

        /// <summary>
        /// 克隆课程
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> CloneCourse(YssxCourseDto dto)
        {
            return await _service.CloneCourse(dto, this.CurrentUser);
        }

        /// <summary>
        /// 删除课程
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveCourse(long id)
        {
            return await _service.RemoveCourse(id);
        }

        /// <summary>
        /// 课程列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxCourseDto>> GetCourseListByPage([FromQuery]CourseSearchDto dto)
        {
            if (CurrentUser.Type != (int)UserTypeEnums.Professional)
            {
                dto.Education = CurrentUser.Education;
            }
            return await _service.GetCourseListByPage(dto, this.CurrentUser);
        }

        /// <summary>
        /// 获取课程列表(NEW)
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxCourseListDto>> GetCourseList([FromQuery]CourseSearchDto dto)
        {
            return await _service.GetCourseList(dto, CurrentUser);
        }

        /// <summary>
        /// 学校购买的的课程
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxCourseListDto>> GetCourseListBySchool([FromQuery]CourseSearchDto dto)
        {
            return await _service.GetCourseListBySchool(dto, CurrentUser);
        }

        /// <summary>
        /// 获取课程详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<YssxCourseDto>> GetCourseById(long id)
        {
            return await _service.GetCourseById(id);
        }

        /// <summary>
        /// 获取快捷显示列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxCourseListDto>> GetShowList(int pageIndex, int pageSize)
        {
            return await _service.GetShowList(new PageRequest { PageIndex = pageIndex, PageSize = pageSize },CurrentUser);
        }
        /// <summary>
        /// 获取快捷显示列表（查全部）
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxCourseListDto>>> GetShowAllList()
        {
            return await _service.GetShowAllList(CurrentUser);
        }
        /// <summary>
        /// 设置快捷显示
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SetIsShow(SetShowDto dto)
        {
            return await _service.SetIsShow(dto.Id, dto.IsShow);
        }


        /// <summary>
        /// 商城首页课程列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortType"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<PageResponse<YssxCourseDto>> GetCourseListPage(int pageIndex, int pageSize, int sortType)
        {
            return await _service.GetCourseListPage(pageIndex, pageSize, sortType);
        }

        /// <summary>
        /// 购买课程(测试接口)
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> BuyClassTest(long courseId)
        {
            return await _service.BuyClassTest(courseId, this.CurrentUser);
        }

        /// <summary>
        /// 获取购买课程列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxCourseOrderViewModel>> GetCourseOrderList([FromQuery]YssxCourseOrderQuery query)
        {
            return await _service.GetCourseOrderList(query, this.CurrentUser);
        }

        /// <summary>
        /// 获取课程列表(App)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxCourseForAppViewModel>> GetCourseForAppList([FromQuery]YssxCourseForAppQuery query)
        {
            return await _service.GetCourseForAppList(query, this.CurrentUser);
        }


        #region 引用平台习题(深度拷贝购买课程下的某一道习题)
        /// <summary>
        /// 引用平台习题(深度拷贝购买课程下的某一道习题)
        /// </summary>
        /// <param name="courseId">习题即将存放的课程Id</param>
        /// <param name="topicId">需要引用的习题Id</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> QuotationPlatformsTopic(long courseId, long topicId)
        {
            return await _service.QuotationPlatformsTopic(courseId, topicId, this.CurrentUser);
        }
        #endregion

        #region 考证资源列表 - 初级会计考证
        /// <summary>
        /// 考证资源列表 - 初级会计考证
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<GetCourseJuniorOrderListDto>> GetCourseJuniorOrderList(GetCourseJuniorOrderListRequest model)
        {
            return await _service.GetCourseJuniorOrderList(model, CurrentUser.Id);
        }
        #endregion
        
        #region 获取购买课程列表
        /// <summary>
        /// 获取购买课程列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<BuyCourseInfoViewModel>> GetBuyCourseList([FromQuery]BuyCourseInfoQuery query)
        {
            return await _service.GetBuyCourseList(query, CurrentUser.Id);
        }
        #endregion

        #region 绑定课程知识点
        /// <summary>
        /// 绑定课程知识点
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> BindingCourseKnowledgePoint(CourseKnowledgePointDto dto)
        {
            return await _service.BindingCourseKnowledgePoint(dto, this.CurrentUser);
        }
        #endregion

        #region 获取课程知识点列表
        /// <summary>
        /// 获取课程知识点列表
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CourseKnowledgePointViewModel>>> GetCourseKnowledgePointList(long courseId)
        {
            return await _service.GetCourseKnowledgePointList(courseId);
        }
        #endregion

        #region 删除课程知识点(根据主键)
        /// <summary>
        /// 删除课程知识点(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCourseKnowledgePointById(long id)
        {
            return await _service.DeleteCourseKnowledgePointById(id, this.CurrentUser);
        }
        #endregion

        #region 删除课程知识点(根据课程、知识点主键)
        /// <summary>
        /// 删除课程知识点(根据课程、知识点主键)
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <param name="knowId">知识点Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCourseKnowledgePointByCourseAndKnowId(long courseId, long knowId)
        {
            return await _service.DeleteCourseKnowledgePointByCourseAndKnowId(courseId, knowId, this.CurrentUser);
        }
        #endregion

        /// <summary>
        /// 获取学校课程引用数据
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxCourseListDto>> GetCourseListBySchoolOrder([FromQuery]CourseSearchDto dto)
        {
            return await _service.GetCourseListBySchoolOrder(dto, CurrentUser);
        }
        /// <summary>
        /// 获取学校课程订单数据
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<OrderCourseDto>>> GetCourseListBySchoolPlat([FromQuery]CourseSearchDto dto)
        {
            return await _service.GetCourseListBySchoolPlat(dto, CurrentUser);
        }
        /// <summary>
        /// 移除/引用课程  
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> RemoveCourseByOrder(RemoveCourseDto model)
        {
            return await _service.RemoveCourseByOrder(model, CurrentUser);
        }

        /// <summary>
        /// 根据课程id获取知识点parentids
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<string>> GetKnowledgePointIdByCourse(long courseId)
        {
            return await _service.GetKnowledgePointIdByCourse(courseId);
        }
    }
}