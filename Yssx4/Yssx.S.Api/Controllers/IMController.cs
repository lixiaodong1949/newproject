﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.Dto.Topics;
using Yssx.S.IServices;
using Yssx.S.IServices.Examination;
using Yssx.S.IServices.Topics;
using Yssx.S.ServiceImpl;
using Yssx.S.ServiceImpl.Examination;
using Yssx.S.ServiceImpl.Topics;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 实时消息相关服务
    /// </summary>
    public class ImController : BaseController
    {
        private IImService _service;
        public ImController(IImService service)
        {
            _service = service;
        }
        /// <summary>
        /// 获取服务器地址
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<string>> GetConnectServerUrl()
        {
            return await _service.GetConnectServerUrl(CurrentUser.ClientId, HttpContext.GetClientUserIp());
        }
        /// <summary>
        /// 群聊,加入群聊
        /// </summary>
        /// <param name="groupId">群聊id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> JoinGroup(string groupId)
        {
            return await _service.JoinGroup(groupId,CurrentUser.ClientId);
        }
        /// <summary>
        /// 群聊,离开群聊
        /// </summary>
        /// <param name="groupId">群聊id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> LeaveGroup(string groupId)
        {
            return await _service.LeaveGroup(groupId,CurrentUser.ClientId);
        }
        /// <summary>
        /// 群聊，获取群聊所有客户端id
        /// </summary>
        /// <param name="groupId">群聊id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<Guid[]>> GetGroupClientList(string groupId)
        {
            return await _service.GetGroupClientList(groupId);
        }
        /// <summary>
        /// 群聊，发送群聊消息(群聊id,消息内容,groupid,message)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SendGroupMsg(ImDto model)
        {
            return await _service.SendGroupMsg(model,CurrentUser.ClientId);
        }
        /// <summary>
        /// 单聊(接收者,发送内容,是否需要回执-receiveUserId,message,isReceipt)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SendMsg(ImDto model)
        {
            return await _service.SendMsg(model,CurrentUser.ClientId);
        }
        /// <summary>
        /// 获取群聊聊天记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<string[]>> GetGroupMsgHistory(ImMsgRequest model)
        {
            return await _service.GetGroupMsgHistory(model);
        }
    }
}