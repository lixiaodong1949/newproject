﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Yssx.Framework;
using Yssx.Framework.Utils;
using Yssx.Repository.Extensions;
using Yssx.S.Dto.Tax;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 个税工具
    /// </summary>
    public class TaxCalculatorController : BaseController
    {
        private ITaxCalculatorService calculatorService;
        public TaxCalculatorController(ITaxCalculatorService _service)
        {
            calculatorService = _service;
        }

        /// <summary>
        /// 税率计算
        /// </summary>
        /// <param name="taxType"></param>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<TaxCalculatorDto>> TaxCalculator(int taxType, int monthType, decimal x1 = 0, decimal x2 = 0)
        {
            return await calculatorService.TaxCalculator(taxType, x1, x2, monthType);
        }

        /// <summary>
        /// 单个税率计算
        /// </summary>
        /// <param name="taxType"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<TaxCalculatorSignDto>> TaxCalculatorSign(int taxType, decimal amount)
        {
            return await calculatorService.TaxCalculatorSign(taxType, amount);
        }


        /// <summary>
        /// 特许权数据导入
        /// </summary>
        /// <param name="file"></param>
        /// <param name="taxType">类型</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ResponseContext<List<TaxToolDto>>> ImportDisclosureData([FromForm] IFormFile file, [FromForm] int taxType)
        {
            return await Task.Factory.StartNew<ResponseContext<List<TaxToolDto>>>(() =>
            {
                List<TaxToolDto> taxTools = new List<TaxToolDto>();
                using (var ms = file.OpenReadStream())
                {
                    taxTools = NPOIHelper<TaxToolDto>.ImportExcelData(ms, "disclosure_data");
                }
                taxTools.ForEach(x =>
                {
                    if (x.Salary1 > 0)
                    {
                        x.Salary = x.Salary1;
                    }
                    else if (x.Salary2 > 0)
                    {
                        x.Salary = x.Salary2;
                    }
                    else if (x.Salary3 > 0)
                    {
                        x.Salary = x.Salary3;
                    }
                });
                RedisHelper.Set($"Cache:Tax:disclosure_data:{taxType}", taxTools, 10 * 60);
                return new ResponseContext<List<TaxToolDto>> { Data = taxTools };
            });

        }

        /// <summary>
        ///  租赁数据导入
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ResponseContext<List<TaxLeaseDto>>> ImportRepairData([FromForm] IFormFile file)
        {
            return await Task.Factory.StartNew<ResponseContext<List<TaxLeaseDto>>>(() =>
            {
                List<TaxLeaseDto> taxTools = new List<TaxLeaseDto>();
                using (var ms = file.OpenReadStream())
                {
                    taxTools = NPOIHelper<TaxLeaseDto>.ImportExcelData(ms, "disclosure_data");
                }
                taxTools.ForEach(x =>
                {
                    if (x.Salary4 > 0)
                    {
                        x.Salary = x.Salary4;
                    }
                });
                RedisHelper.Set($"Cache:Repair:repair_data", taxTools, 10 * 60);
                return new ResponseContext<List<TaxLeaseDto>> { Data = taxTools };
            });
        }

        /// <summary>
        /// 工资，薪金导入
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ResponseContext<List<TaxSalaryDto>>> ImportSalaryData([FromForm] IFormFile file)
        {
            return await Task.Factory.StartNew<ResponseContext<List<TaxSalaryDto>>>(() =>
            {
                if (file == null) return new ResponseContext<List<TaxSalaryDto>>
                {
                    Code = CommonConstants.ErrorCode,
                    Msg = "上传失败"
                };
                List<TaxSalaryDto> dtos = new List<TaxSalaryDto>();
                using (var ms = file.OpenReadStream())
                {
                    dtos = NPOIHelper<TaxSalaryDto>.ImportExcelData(ms, "salary_data");

                }
                dtos.ForEach(x =>
                {
                    x.TotalAmount = x.BaseSalary + x.Subsidy + x.Bonus;
                    // x.SpecialAmount = x.TotalAmount * 0.19m;
                    x.TaxableIncome = x.TotalAmount - x.SpecialAmount - x.Sed - 5000;
                });
                RedisHelper.Set($"Cache:Salary:salary_data", dtos, 10 * 60);
                return new ResponseContext<List<TaxSalaryDto>> { Data = dtos };
            });
        }

        /// <summary>
        /// 下载模板
        /// </summary>
        /// <param name="taxType"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public FileResult DownloadToExcel(int taxType)
        {
            if (taxType == 3)
            {
                return SalaryTemp();
            }
            else if (taxType == 4)
            {
                return LeaseTemp();
            }
            else
            {
                return UniversalTemp(taxType);
            }



            //return File(ms, "application/ms-excel", "StudentList.xlsx");
        }

        private FileResult UniversalTemp(int taxType)
        {
            List<TaxToolDto> dtos = new List<TaxToolDto>();
            string json = RedisHelper.Get($"Cache:Tax:disclosure_data:{taxType}");
            if (!string.IsNullOrEmpty(json)) { dtos = JsonConvert.DeserializeObject<List<TaxToolDto>>(json); }
            else
            {
                dtos.Add(new TaxToolDto { UserName = "模板" });
            }

            byte[] ms = NPOIHelper<TaxToolDto>.OutputExcel(dtos, new string[] { "111" });
            return File(ms, "application/ms-excel", "tax_temp.xlsx");
        }

        private FileResult LeaseTemp()
        {
            List<TaxLeaseDto> leaseDtos = new List<TaxLeaseDto>();
            string json = RedisHelper.Get($"Cache:Repair:repair_data");
            if (!string.IsNullOrEmpty(json)) { leaseDtos = JsonConvert.DeserializeObject<List<TaxLeaseDto>>(json); }
            else
            {
                leaseDtos.Add(new TaxLeaseDto { UserName = "模板" });
            }

            byte[] ms = NPOIHelper<TaxLeaseDto>.OutputExcel(leaseDtos, new string[] { "111" });
            return File(ms, "application/ms-excel", "lease_temp.xlsx");
        }

        private FileResult SalaryTemp()
        {
            List<TaxSalaryDto> salaryDtos = new List<TaxSalaryDto>();
            string json = RedisHelper.Get($"Cache:Salary:salary_data");
            if (!string.IsNullOrEmpty(json)) { salaryDtos = JsonConvert.DeserializeObject<List<TaxSalaryDto>>(json); }
            else
            {
                salaryDtos.Add(new TaxSalaryDto { UserName = "模板" });
            }

            byte[] ms = NPOIHelper<TaxSalaryDto>.OutputExcel(salaryDtos, new string[] { "111" });
            return File(ms, "application/ms-excel", "salary_temp.xlsx");
        }
    }
}
