﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 备课课程章节相关Api
    /// </summary>
    public class SemesterController : BaseController
    {
        ISemesterService _service;

        public SemesterController(ISemesterService service)
        {
            _service = service;
        }

        #region 新增/编辑学期
        ///// <summary>
        ///// 新增/编辑学期
        ///// </summary>
        ///// <param name="dto"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<ResponseContext<bool>> AddOrEditSemester(SemesterDto dto)
        //{
        //    return await _service.AddOrEditSemester(dto, CurrentUser.Id);
        //}
        #endregion

        #region 获取学期列表
        /// <summary>
        /// 获取学期列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxSemesterViewModel>>> GetSemesterList([FromQuery]YssxSemesterQuery query)
        {
            return await _service.GetSemesterList(query);
        }
        #endregion

        #region 删除学期
        ///// <summary>
        ///// 删除学期
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public async Task<ResponseContext<bool>> RemoveSemester(long id)
        //{
        //    return await _service.RemoveSemester(id, CurrentUser.Id);
        //}
        #endregion

    }
}