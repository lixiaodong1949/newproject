﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.Dto.Means;
using Yssx.S.IServices;
using Yssx.S.IServices.Examination;
using Yssx.S.Pocos;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 场景实训做题相关服务
    /// </summary>
    public class UserExercisesController : BaseController
    {
        private IUserExercisesService _userExercisesService;
        public UserExercisesController(IUserExercisesService userExercisesService)
        {
            _userExercisesService = userExercisesService;
        }

        /// <summary>
        /// 获取题目信息
        /// </summary>
        /// <param name="progressId">场景实训记录id（类似竞赛中的gradeid）</param>
        /// <param name="exercisesType">0 场景实训 1课程练习</param>
        /// <param name="questionId">题目id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfo(long progressId, ExercisesType exercisesType, long questionId)
        {
            return await _userExercisesService.GetQuestionInfo(progressId,exercisesType,questionId,CurrentUser);
        }
        /// <summary>
        /// 提交答案
        /// </summary>
        /// <param name="model"></param>
        /// <param name="caseId">对应场景实训公司id</param>
        /// <param name="exercisesType">0 场景实训 1课程练习</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<QuestionResult>> SubmitAnswer(QuestionAnswer model, long caseId, ExercisesType exercisesType)
        {
            return await _userExercisesService.SubmitAnswer(model, caseId, exercisesType, CurrentUser);
        }

        /// <summary>
        /// 通关记录新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<ResponseContext<bool>> AddGradeRecord(OaxGradeRecordRequestDto model)
        {
            return await _userExercisesService.AddGradeRecord(model);
        }
    }
}