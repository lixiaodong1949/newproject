﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.IServices.ExamineAndVerify;
using Yssx.S.Pocos;
using Yssx.S.ServiceImpl;
using Yssx.S.ServiceImpl.Web;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 审核
    /// </summary>
    public class ExamineAndVerifyController : BaseController
    {
        private IExamineAndVerifyService _service;

        /// <summary>
        /// 
        /// </summary>
        public ExamineAndVerifyController(IExamineAndVerifyService service)
        {
            _service = service;
        }

        /// <summary>
        /// 审核
        /// </summary>
        /// <param name="dto">审核dto</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> ExamineAndVerify(ExamineAndVerifyDto dto) {
            return await _service.ExamineAndVerify(dto, CurrentUser.Id);
        }

        /// <summary>
        /// 上下架
        /// </summary>
        /// <param name="dto">上下架dto</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UpperAndLowerFrames(UpperAndLowerFramesDto dto) {
            return await _service.UpperAndLowerFrames(dto, CurrentUser.Id);
        }
    }
}