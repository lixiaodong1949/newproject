using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Microsoft.AspNetCore.Authorization;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 积分商品
    /// </summary>
    public class IntegralProductController : BaseController
    {
        private IIntegralProductService _service;

        public IntegralProductController(IIntegralProductService service)
        {
            _service = service;
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<IntegralProductInfoDto>> Info(long id)
        {
            var response = new ResponseContext<IntegralProductInfoDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Info(id);

            return response;
        }

        /// <summary>
        /// 查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ListResponse<IntegralProductBaseInfoDto>> BaseListPage()
        {
            //var response = new PageResponse<IntegralProductBaseInfoDto> { Code = CommonConstants.BadRequest };
            //if (query == null)
            //{
            //    response.Msg = "参数不能为空";
            //    return response;
            //}

            var response = await _service.BaseListPage();

            return response;
        }
    }
}