﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 教师班级相关Api
    /// </summary>
    public class LessonPlanningClassController : BaseController
    {
        ILessonPlanningClassService _service;

        public LessonPlanningClassController(ILessonPlanningClassService service)
        {
            _service = service;
        }

        #region 教师绑定班级
        /// <summary>
        /// 教师绑定班级
        /// </summary>
        /// <param name="requestList">班级列表</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> BindingTeacherClassRelation(List<TeacherClassRequestModel> requestList)
        {
            return await _service.BindingTeacherClassRelation(requestList, this.CurrentUser);
        }
        #endregion

        #region 教师绑定班级(App)
        /// <summary>
        /// 教师绑定班级(App)
        /// </summary>
        /// <param name="requestList">班级列表</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> BindingTeacherClassRelationForApp(List<TeacherClassRequestModel> requestList)
        {
            return await _service.BindingTeacherClassRelationForApp(requestList, this.CurrentUser);
        }
        #endregion

        #region 解绑单一教师班级
        /// <summary>
        /// 解绑单一教师班级
        /// </summary>
        /// <param name="teacherClassId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UnlinkTeacherClass(long teacherClassId)
        {
            return await _service.UnlinkTeacherClass(teacherClassId, this.CurrentUser);
        }
        #endregion

        #region 获取教师班级列表
        /// <summary>
        /// 获取教师班级列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="year">入学年份</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxTeacherClassRelationViewModel>>> GetTeacherClassRelationList([FromQuery]YssxTeacherClassRequestModel query)
        {
            return await _service.GetTeacherClassRelationList(query, this.CurrentUser);
        }
        #endregion

        #region 根据班级获取班级下学生信息
        /// <summary>
        /// 根据班级获取班级下学生信息
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxTeacherClassStudentViewModel>>> GetStudentListByClassId([FromQuery]YssxTeacherClassStudentRequestModel query)
        {
            return await _service.GetStudentListByClassId(query);
        }
        #endregion

        #region 获取教师下所有班级学生
        /// <summary>
        /// 获取教师下所有班级学生
        /// </summary>
        /// <param name="year">入学年份</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<AllClassStudentByTeacherViewModel>>> GetTeacherAllStudentList(int year)
        {
            return await _service.GetTeacherAllStudentList(year, this.CurrentUser);
        }
        #endregion

        #region 根据班级Ids获取学生Id、Name
        /// <summary>
        /// 根据班级Ids获取学生Id、Name
        /// </summary>
        /// <param name="classIds"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<StudentInfoByClassIdsViewModel>>> GetStudentNameByClassIds([FromQuery]List<long> classIds)
        {
            return await _service.GetStudentNameByClassIds(classIds);
        }
        #endregion

        #region 学生退班(通过教师操作)
        /// <summary>
        /// 学生退班(通过教师操作)
        /// </summary>
        /// <param name="studentUserId">学生UserId</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> StudentQuitClassByTeacher(long studentUserId)
        {
            return await _service.StudentQuitClassByTeacher(studentUserId, this.CurrentUser);
        }
        #endregion

        #region 获取教师是否有课程、班级(是否可以发布任务)
        /// <summary>
        /// 获取教师是否有课程、班级(是否可以发布任务)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TeacherIsCanPublishTask>> GetTeacherIsCanPublishTask()
        {
            return await _service.GetTeacherIsCanPublishTask(this.CurrentUser);
        }
        #endregion

        #region 编辑学生信息
        /// <summary>
        /// 编辑学生信息
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EditStudentByTeacher(EditStudentByTeacherRequestModel request)
        {
            return await _service.EditStudentByTeacher(request, this.CurrentUser);
        }
        #endregion

        #region 教师导入学生名单
        /// <summary>
        /// 教师导入学生名单
        /// </summary>
        /// <param name="file"></param>
        /// <param name="classId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<ImportStudentResultModel>> ImportStudentByTeacher([FromForm] IFormFile file, [FromForm] long classId)
        {
            return await _service.ImportStudentByTeacher(file, classId, this.CurrentUser);
        }
        #endregion

        #region 教师手动添加学生
        /// <summary>
        /// 教师手动添加学生
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> ManualAddStudentByTeacher(TeacherAddStudentRequestModel request)
        {
            return await _service.ManualAddStudentByTeacher(request, this.CurrentUser);
        }
        #endregion

    }
}