﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.M.IServices;
using Yssx.S.Dto;

namespace Yssx.S.Api.Controllers
{

    public class StudentController : BaseController
    {
        private IStudentService _service;
        public StudentController(IStudentService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<ResponseContext<List<StudentClassListDto>>> GetStudentsByClassIds(StudentByClassDto dto)
        {
            return await _service.GetStudentsByClassIds(dto.Ids);
        }
    }
}