﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.Dto.CourseExam;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.Dto.Means;
using Yssx.S.IServices;
using Yssx.S.IServices.Examination;
using Yssx.S.Pocos;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 课程练习做题相关服务
    /// </summary>
    public class CourseExercisesController : BaseController
    {
        private ICourseExercisesService _courseExercisesService;
        public CourseExercisesController(ICourseExercisesService courseExercisesService)
        {
            _courseExercisesService = courseExercisesService;
        }

        /// <summary>
        /// 获取当前用户课程（自己的课程和购买的课程）
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CourseInfoDto>>> GetCourseInfoByUser()
        {
            return await _courseExercisesService.GetCourseInfoByUser(CurrentUser);
        }
        /// <summary>
        /// 根据课程id获取章节信息
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<SectionInfoDto>> GetSectionListByCourse(long courseId, int pageSize, int pageIndex)
        {
            return await _courseExercisesService.GetSectionListByCourse(courseId, pageSize, pageIndex, CurrentUser);
        }
        /// <summary>
        /// 获取课程练习试卷信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourseExamPaperBasicInfo(long courseId, long sectionId, long gradeId = 0)
        {
            return await _courseExercisesService.GetCourseExamPaperBasicInfo(courseId, sectionId, CurrentUser, gradeId);
        }
        /// <summary>
        /// 获取当前用户所在备课的课程
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<PrepareCourseInfoDto>>> GetPrepareCourseInfoByUser()
        {
            return await _courseExercisesService.GetPrepareCourseInfoByUser(CurrentUser);
        }
        /// <summary>
        /// 根据备课课程id获取章节信息
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<SectionInfoDto>> GetPrepareSectionListByCourse(long courseId, int pageSize, int pageIndex)
        {
            return await _courseExercisesService.GetPrepareSectionListByCourse(courseId, pageSize, pageIndex, CurrentUser);
        }
        /// <summary>
        /// 根据章Id获取节信息
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubSectionInfoDto>>> GetSubSectionList(long sectionId)
        {
            return await _courseExercisesService.GetSubSectionList(sectionId, CurrentUser.Id);
        }

        /// <summary>
        /// 课程实训历史记录
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<ExamCourseDetailsDto>>> GetExamCourseDetailsList(long sectionId)
        {
            return await _courseExercisesService.GetExamCourseDetailsList(sectionId, CurrentUser);
        }
        /// <summary>
        /// 获取试卷信息--接口方法（包括试卷基本信息和试卷包含的题目信息）备课课程练习
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetPrepareCourseExamPaperBasicInfo(long courseId, long sectionId, UserTicket user, long gradeId = 0)
        {
            return await _courseExercisesService.GetPrepareCourseExamPaperBasicInfo(courseId, sectionId, CurrentUser, gradeId);
        }
        /// <summary>
        /// 获取任务试卷信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetTaskExamPaperBasicInfo(long taskId, long gradeId = 0)
        {
            return await _courseExercisesService.GetTaskExamPaperBasicInfo(taskId, CurrentUser, gradeId);
        }
        /// <summary>
        /// 获取题目信息--接口方法（课程练习，作业，任务，考试等）
        /// </summary>
        /// <param name=""></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamCourseQuestionInfo>> GetCourseQuestionInfo(long gradeId, CourseExamType examType, long questionId)
        {
            return await _courseExercisesService.GetCourseQuestionInfo(gradeId, examType, questionId, CurrentUser);
        }

        /// <summary>
        /// 提交答案--接口方法
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <returns>作答成功信息</returns>
        [HttpPost]
        public async Task<ResponseContext<QuestionResult>> SubmitAnswer(CourseQuestionAnswer model, CourseExamType examType)
        {
            return await _courseExercisesService.SubmitAnswer(model, examType, CurrentUser);
        }
        /// <summary>
        /// 预览题目作答信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamCourseQuestionInfo>> GetCourseQuestionInfoView(long gradeId, long questionId)
        {
            return await _courseExercisesService.GetCourseQuestionInfoView(gradeId, questionId, CurrentUser);
        }
        /// <summary>
        /// 提交考卷---接口方法 
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GradeInfo>> SubmitExam(long gradeId)
        {
            return await _courseExercisesService.SubmitExam(gradeId, CurrentUser);
        }

        /// <summary>
        /// 预览作业任务试卷基本信息--接口方法
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetTaskExamPaperInfoView(long examId)
        {
            return await _courseExercisesService.GetTaskExamPaperInfoView(examId, CurrentUser);
        }
        /// <summary>
        /// 预览试卷作答信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetExamPaperGradeInfoView(CourseExamType examType, long gradeId)
        {
            return await _courseExercisesService.GetExamPaperGradeInfoView(examType, gradeId, CurrentUser);
        }

        /// <summary>
        /// 课堂任务获取试卷信息
        /// </summary>
        /// <param name="examId"></param>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourceOnlineExamPaperBasicInfo(long examId, long gradeId)
        {
            return await _courseExercisesService.GetCourceOnlineExamPaperBasicInfo(examId, gradeId, this.CurrentUser);
        }

        /// <summary>
        /// 课程练习/预览
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CourseSectionInfoDto>> GetCourseInfoByCourseId(long courseId)
        {
            return await _courseExercisesService.GetCourseInfoByCourseId(courseId,CurrentUser.Id);
        }

        /// <summary>
        /// 根据章节Id获取节作答信息
        /// </summary>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<SubSectionInfoDto>> GetSectionExamInfo(long sectionId)
        {
            return await _courseExercisesService.GetSectionExamInfo(sectionId,CurrentUser.Id);
        }

        /// <summary>
        /// 案例分析题打分
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SubmitAnswerByTeacher(ShortQuestionAnswer answer)
        {
            return await _courseExercisesService.SubmitAnswerByTeacher(answer,CurrentUser.Id);
        }

        /// <summary>
        /// 根据任务查询简答题列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TaskShortQuestionDto>> GetShortQuestionListByTask(long taskId)
        {
            return await _courseExercisesService.GetShortQuestionListByTask(taskId);
        }
        /// <summary>
        /// 根据案例分析题ID获取学生作答记录列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="questionId"></param>
        /// <param name="classId"></param>
        /// <param name="answerStatus">0全部  1 已作答 2未作答</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<ShortQuestionAnswerDto>>> GetStudentAnswerList(long taskId, long questionId, long classId, int answerStatus,string keywords)
        {
            return await _courseExercisesService.GetStudentAnswerList(taskId, questionId,classId,answerStatus, keywords);
        }
    }
}