﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 自然人
    /// </summary>
    public class NaturalPersonsController : BaseController
    {
        private INaturalPersonsService _service;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public NaturalPersonsController(INaturalPersonsService service)
        {
            _service = service;
        }

        #region 人员信息采集
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<GetListResponse>> GetPersonnelList(PersonnelListDao model)
        {
            return await _service.GetPersonnelList(model, CurrentUser.Id);
        }

        /// <summary>
        ///查看资料
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<ViewInfoResponse>> GetPersonnelDataList(PersonnelListDao model)
        {
            return await _service.GetPersonnelDataList(model);
        }

        /// <summary>
        /// 添加 更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdate(NotesDao model)
        {
            return await _service.AddUpdate(model, CurrentUser.Id);
        }
        
        /// <summary>
        /// 报送
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Submitted(IdsDao model)
        {
            return await _service.Submitted(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取反馈
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Feedback(IdsDao model)
        {
            return await _service.Feedback(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Delete(IdsDao model)
        {
            return await _service.Delete(model, CurrentUser.Id);
        }

        /// <summary>
        /// 清空重做
        /// </summary>
        /// <param name="CaseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> Redo(long CaseId)
        {
            return await _service.Redo(CaseId, CurrentUser.Id);
        }
        #endregion

        #region 专项附加扣除信息采集
        #endregion

        #region 综合所得申报
        #endregion
    }
}
