﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 备课课程章节相关Api [Obsolete]
    /// </summary>
    public class PrepareSectionController : BaseController
    {
        IPrepareSectionService _service;

        public PrepareSectionController(IPrepareSectionService service)
        {
            _service = service;
        }

        #region 新增/编辑备课课程章节
        /// <summary>
        /// 新增/编辑备课课程章节
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditPrepareSection(PrepareSectionDto dto)
        {
            return await _service.AddOrEditPrepareSection(dto, this.CurrentUser);
        }
        #endregion

        #region 获取备课课程章节列表
        /// <summary>
        /// 获取备课课程章节列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxPrepareSectionViewModel>>> GetPrepareSectionList([FromQuery]YssxPrepareSectionQuery query)
        {
            return await _service.GetPrepareSectionList(query);
        }
        #endregion

        #region 删除备课课程章节
        /// <summary>
        /// 删除备课课程章节
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemovePrepareSection(long id)
        {
            return await _service.RemovePrepareSection(id, this.CurrentUser);
        }
        #endregion

        #region 删除备课课程本章所有小节
        /// <summary>
        /// 删除备课课程本章所有小节
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemovePrepareAllSectionOfChapter(long id)
        {
            return await _service.RemovePrepareAllSectionOfChapter(id, this.CurrentUser);
        }
        #endregion

        #region 新增备课课程节习题
        /// <summary>
        /// 新增备课课程节习题
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddPrepareSectionTopic(PrepareSectionTopicDto dto)
        {
            return await _service.AddPrepareSectionTopic(dto, this.CurrentUser);
        }
        #endregion

        #region 获取备课课程节习题列表
        /// <summary>
        /// 获取备课课程节习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxPrepareSectionTopicViewModel>>> GetPrepareSectionTopicList([FromQuery]YssxPrepareInfoQuery query)
        {
            return await _service.GetPrepareSectionTopicList(query);
        }
        #endregion

        #region 删除备课课程节习题
        /// <summary>
        /// 删除备课课程节习题
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemovePrepareSectionTopic(long id)
        {
            return await _service.RemovePrepareSectionTopic(id, this.CurrentUser);
        }
        #endregion

        #region 新增备课课程节场景实训
        /// <summary>
        /// 新增备课课程节场景实训
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddPrepareSectionSceneTraining(PrepareSectionSceneTrainingDto dto)
        {
            return await _service.AddPrepareSectionSceneTraining(dto, this.CurrentUser);
        }
        #endregion

        #region 获取备课课程节场景实训列表
        /// <summary>
        /// 获取备课课程节场景实训列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxPrepareSectionSceneTrainingViewModel>>> GetPrepareSectionSceneTrainingList([FromQuery]YssxPrepareInfoQuery query)
        {
            return await _service.GetPrepareSectionSceneTrainingList(query);
        }
        #endregion

        #region 删除备课课程节场景实训
        /// <summary>
        /// 删除备课课程节场景实训
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemovePrepareSectionSceneTraining(long id)
        {
            return await _service.RemovePrepareSectionSceneTraining(id, this.CurrentUser);
        }
        #endregion

        #region 新增/编辑备课课程节教材
        /// <summary>
        /// 新增/编辑备课课程节教材
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditPrepareSectionTextBook(PrepareSectionTextBookDto dto)
        {
            return await _service.AddOrEditPrepareSectionTextBook(dto, this.CurrentUser);
        }
        #endregion

        #region 获取备课课程某一节教材详情
        /// <summary>
        /// 获取备课课程某一节教材详情
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxPrepareSectionTextBookViewModel>>> GetPrepareSectionTextBookDetail([FromQuery]YssxPrepareInfoQuery query)
        {
            return await _service.GetPrepareSectionTextBookDetail(query);
        }
        #endregion

        #region 删除备课课程节教材
        /// <summary>
        /// 删除备课课程节教材
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemovePrepareSectionTextBook(long id)
        {
            return await _service.RemovePrepareSectionTextBook(id, this.CurrentUser);
        }
        #endregion

        #region 新增备课课程课件、视频、教案、授课计划
        /// <summary>
        /// 新增备课课程课件、视频、教案、授课计划
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddPrepareSectionFiles(PrepareSectionFilesDto dto)
        {
            return await _service.AddPrepareSectionFiles(dto, this.CurrentUser);
        }
        #endregion

        #region 获取备课课程课件、视频、教案、授课计划列表(App)
        /// <summary>
        /// 获取备课课程课件、视频、教案、授课计划列表(App)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxPrepareSectionFilesViewModel>>> GetPrepareSectionFilesListForApp([FromQuery]YssxPrepareSectionFilesQuery query)
        {
            return await _service.GetPrepareSectionFilesListForApp(query);
        }
        #endregion

        #region 获取备课课程课件、视频、教案、授课计划列表(PC)
        /// <summary>
        /// 获取备课课程课件、视频、教案、授课计划列表(PC)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<YssxPrepareSectionFilesForPCViewModel>> GetPrepareSectionFilesListForPC([FromQuery]YssxPrepareSectionFilesQuery query)
        {
            return await _service.GetPrepareSectionFilesListForPC(query);
        }
        #endregion

        #region 删除备课课程课件、视频、教案、授课计划
        /// <summary>
        /// 删除备课课程课件、视频、教案、授课计划
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemovePrepareSectionFiles(long id)
        {
            return await _service.RemovePrepareSectionFiles(id, this.CurrentUser);
        }
        #endregion


    }
}