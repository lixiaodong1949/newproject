﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Api.Auth;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.Dto.SceneTraining;
using Yssx.S.IServices;
using Yssx.S.IServices.SceneTraining;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 场景实训
    /// </summary>
    public class SceneTrainingController : BaseController
    {
        ISceneTrainingService _service;
        public SceneTrainingController(ISceneTrainingService service)
        {
            _service = service;
        }

        /// <summary>
        /// 上传场景实训dto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddTrainingService(AddTrainingServiceDto dto)
        {
            return await _service.AddTrainingService(dto, CurrentUser.Id);
        }

        /// <summary>
        /// 下拉数据
        /// </summary>
        /// <param name="id">id(公司下拉传0，部门下拉传公司id,人员下来传部门id)</param>
        /// <param name="typeId">1,公司,2,部门,3,人员,4,场景业务</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<DropDownDataDto>>> DropDownData(long id, int typeId)
        {
            return await _service.DropDownData(id, typeId);
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="dto">搜索</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<YssxSceneTrainingDto>> TrainingServiceList(TrainingServicePageRequest dto)
        {
            return await _service.TrainingServiceList(dto,CurrentUser);
        }

        /// <summary>
        /// 描述
        /// </summary>
        /// <param name="describe">描述</param>
        /// <param name="id">场景实训id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> Describe(string describe, long id)
        {
            return await _service.Describe(describe, id);
        }

        /// <summary>
        /// 实训详情
        /// </summary>
        /// <param name="id">场景实训id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TrainingServiceDetailsDto>> TrainingServiceDetails(long id)
        {
            return await _service.TrainingServiceDetails(id);
        }

        /// <summary>
        /// 预览-场景实训详情
        /// </summary>
        /// <param name="id">场景实训id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TrainingServicePreviewDto>> TrainingServicePreview(long id)
        {
            return await _service.TrainingServicePreview(id);
        }

        /// <summary>
        /// 做题开始详情
        /// </summary>
        /// <param name="id">场景实训id</param>
        /// <param name="progressId">实训记录Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ProblemDetailsDto>> ProblemDetails(long id,long progressId)
        {
            return await _service.ProblemDetails(id, progressId,CurrentUser.Id);
        }

        /// <summary>
        /// 场景实训流程详情(步骤详情)
        /// </summary>
        /// <param name="id">场景实训流程id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ProcessDetails>> SceneTrainingDetails(long id)
        {
            return await _service.SceneTrainingDetails(id);
        }

        /// <summary>
        /// 预览-公司下面的场景实训列表
        /// </summary>
        /// <param name="id">公司id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TrainingServicePreviewListDto>> TrainingServicePreviewList(long id)
        {
            return await _service.TrainingServicePreviewList(id);
        }

        /// <summary>
        /// 添加实训进度
        /// </summary>
        /// <param name="dto">添加实训进度dto</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddProgress(AddProgressDto dto)
        {
            return await _service.AddProgress(dto, CurrentUser.Id);
        }

        /// <summary>
        /// 复盘总结-业务列表
        /// </summary>
        /// <param name="id">场景实训id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CheckingSummaryListDto>> CheckingSummaryList(long id) {
            return await _service.CheckingSummaryList(id);
        }

        /// <summary>
        ///修改进度状态
        /// </summary>
        /// <param name="id">实训进度id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> UpdateProgress(long id) {
            return await _service.UpdateProgress(id);
        }

        /// <summary>
        /// 实训进度详情
        /// </summary>
        /// <param name="id">实训记录id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ProgressDetailsDto>> ProgressDetails(long id) {
            return await _service.ProgressDetails(id, CurrentUser.Id);
        }
    }
}