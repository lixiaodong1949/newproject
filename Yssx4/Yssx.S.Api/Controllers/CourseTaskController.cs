﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers
{
    /// <summary>
    /// 课程任务
    /// </summary>
    public class CourseTaskController : BaseController
    {
        ICourseTaskService _service;
        public CourseTaskController(ICourseTaskService service)
        {
            _service = service;
        }

        /// <summary>
        /// 根据查询结果获取题型列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<QuestionTypeData>>> GetQuestionTypes([FromQuery]CourseTopicQuery query)
        {
            return await _service.GetQuestionTypesByQuery(query);
        }

        /// <summary>
        /// 获取习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<CourseTaskTopicDto>> GetCourseTopicList([FromQuery]CourseTopicQuery query)
        {
            return await _service.GetCourseTopicList(query, CurrentUser.Id);
        }

        /// <summary>
        /// 加入任务篮
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddTaskCart(TaskCartRequest model)
        {
            return await _service.AddTaskCart(model, CurrentUser.Id);
        }
        /// <summary>
        /// 批量加入任务篮
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddTaskCartBatch(BatchTaskCartRequest model)
        {
            return await _service.AddTaskCartBatch(model, CurrentUser.Id);
        }
        /// <summary>
        /// 根据Id移除任务篮
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteTaskCart(DeleteTaskCartRequest model)
        {
            return await _service.DeleteTaskCart(model, CurrentUser.Id);
        }
        /// <summary>
        /// 根据课程Id和题型移除任务篮
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteTaskCartByQuestionType(long courseId, QuestionType questionType)
        {
            return await _service.DeleteTaskCartByQuestionType(courseId, questionType, CurrentUser.Id);
        }
        /// <summary>
        /// 清空任务篮
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> ClearTaskCart(long courseId)
        {
            return await _service.ClearTaskCart(courseId, CurrentUser.Id);
        }
        /// <summary>
        /// 获取任务篮列表
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<TaskCartTopicDto>>> GetTaskCartList(long courseId)
        {
            return await _service.GetTaskCartList(CurrentUser.Id, courseId);
        }
        /// <summary>
        /// 根据课程查找任务篮题目数量
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<long>> GetTaskCartCountByCourseId(long courseId)
        {
            return await _service.GetTaskCartCountByCourseId(CurrentUser.Id, courseId);
        }
        /// <summary>
        /// 任务篮弹窗
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<TaskCartQuestionCountDto>>> GetTaskCarts(long courseId)
        {
            return await _service.GetTaskCarts(CurrentUser.Id, courseId);
        }
        /// <summary>
        /// 智能组卷时根据查询条件返回题型及题目数量
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<RandomTaskCartQuestionTypeDto>>> GetRandomQuestionType(RandomTaskCartRequest model)
        {
            return await _service.GetRandomQuestionType(model, CurrentUser.Id);
        }
        /// <summary>
        /// 智能组卷生成题目
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> RandomAddTaskCarts(RandomTaskCartRequest model)
        {
            return await _service.RandomAddTaskCarts(model, CurrentUser.Id);
        }
        /// <summary>
        /// 发布任务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> PublishCourseTask(PublicCourseTaskDto model)
        {
            return await _service.PublishCourseTask(model, CurrentUser.Id, CurrentUser.TenantId);
        }

        /// <summary>
        /// 查询课业任务列表 - 教师端
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<CourseTaskListDto>> GetTeacherCourseTaskList([FromQuery]CourseTaskQueryRequest model)
        {
            return await _service.GetTeacherCourseTaskList(model, CurrentUser.Id);
        }

        /// <summary>
        /// 课程任务基础信息修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EditTaskBasicInfo(EditTaskBasicInfoDto model)
        {
            return await _service.EditTaskBasicInfo(model, CurrentUser.Id);
        }

        /// <summary>
        /// 查询课业任务列表 - 学生端
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<StudentCourseListDto>> GetStudentCourseTaskList([FromQuery]StudentCourseTaskRequest model)
        {
            return await _service.GetStudentCourseTaskList(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除课业任务
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> RemoveTask(long id)
        {
            return await _service.RemoveTask(id, CurrentUser.Id);
        }
        /// <summary>
        /// 结束课业任务
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> ClosedTask(long id)
        {
            return await _service.ClosedTask(id, CurrentUser.Id);
        }
        /// <summary>
        /// 一键交卷
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> BatchSubmitExam(long id)
        {
            return await _service.BatchSubmitExam(id, CurrentUser.Id);
        }
        /// <summary>
        /// 预览课程任务信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CourseTaskInfoDto>> GetCourseTaskInfoById(long taskId)
        {
            return await _service.GetCourseTaskInfoById(taskId,CurrentUser.Id);
        }

        /// <summary>
        /// 课业任务统计信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CourseTaskGradeReportDto>> GetCourseGradeDetailByTask(long taskId, string classIds)
        {
            return await _service.GetCourseGradeDetailByTask(taskId, classIds,CurrentUser.Id);
        }

        /// <summary>
        /// 获取学生答题对错详情
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<StudentGradeInfoDto>> GetTaskTopicGradeDetailList(long gradeId, long taskId)
        {
            return await _service.GetTaskTopicGradeDetailList(gradeId,taskId);
        }
        /// <summary>
        /// 章节习题分析
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<TaskSectionGradeInfoDto>>> GetTaskSectionGradeDetailList(long taskId)
        {
            return await _service.GetTaskSectionGradeDetailList(taskId);
        }
    }
}