﻿using Consul;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;

namespace Yssx.S.Api
{
    public static class ConsulServiceRregister
    {
        /// <summary>
        /// 服务注册
        /// </summary>
        /// <param name="app"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseConsul(this IApplicationBuilder app, IConfiguration configuration)
        {
            
               //var lifetime = app.ApplicationServices.GetRequiredService<IHostApplicationLifetime>();
           
            var consulConfig = InitConsulConfig(configuration);//初始化配置
            var consulService = consulConfig.ConsulService;

            // 添加健康检查路由地址
            app.Map(consulConfig.HealthUrl, HealthMap);//consul 手动创建健康检查 api

            Uri address;
            if (string.IsNullOrWhiteSpace(consulService.Url))
            {
                address = new Uri($"{ consulService.Scheme}://{consulService.IP}:{consulService.Port}");
            }
            else
            {
                address = new Uri(consulService.Url);
            }

            var consulClient = new ConsulClient(x => x.Address = address);//请求注册的 Consul 地址
            var httpCheck = new AgentServiceCheck()
            {
                DeregisterCriticalServiceAfter = TimeSpan.FromSeconds(60),//健康检查失败后多久从consul中移除该服务(单位：s)，0表示一直不移除
                Interval = TimeSpan.FromSeconds(10),//健康检查时间间隔，或者称为心跳间隔
                HTTP = $"{consulConfig.Scheme}://{consulConfig.IP}:{consulConfig.Port}{consulConfig.HealthUrl}",//健康检查地址
                Timeout = TimeSpan.FromSeconds(5),
            };

            // Register service with consul
            var registration = new AgentServiceRegistration()
            {
                Checks = new[] { httpCheck },
                ID = consulConfig.APIName + consulConfig.IP + "_" + consulConfig.Port,
                Name = consulConfig.APIName,
                Address = consulConfig.IP,
                Port = consulConfig.Port,
                Tags = new[] { $"{consulConfig.APIName}/{consulConfig.IP}:{consulConfig.Port}" }//添加 urlprefix-/servicename 格式的 tag 标签，以便 Fabio 识别
            };

            consulClient.Agent.ServiceRegister(registration).Wait();//服务启动时注册，内部实现其实就是使用 Consul API 进行注册（HttpClient发起）socket???

            //lifetime.ApplicationStopping.Register(() =>
            //{
            //    consulClient.Agent.ServiceDeregister(registration.ID).Wait();//服务停止时取消注册，当前系统停止从时Consul注销本服务的注册
            //});

            return app;
        }

        private static ConsulConfig InitConsulConfig(IConfiguration configuration)
        {
            //var a = configuration["ConsulConfig:apiname"];
            //Console.WriteLine(a);

            if (configuration == null)
            {
                throw new NullReferenceException("服务发现配置无法获取到，必须配置ConsulConfig节点");
            }

            var appsetting = configuration.Get<BaseAppsettings>();

            if (appsetting == null || appsetting.ConsulConfig == null)
            {
                throw new NullReferenceException("服务发现配置无法获取到，必须配置ConsulConfig节点");
            }

            var consulConfig = appsetting.ConsulConfig;

            var consulService = consulConfig.ConsulService;
            if (consulService == null || (string.IsNullOrWhiteSpace(consulService.Url) && string.IsNullOrWhiteSpace(consulService.Scheme) && string.IsNullOrWhiteSpace(consulService.IP) && consulService.Port == 0))
            {
                throw new NullReferenceException("服务发现配置不正确，必须配置ConsulConfig:ConsulService节点");
            }
            else if (string.IsNullOrWhiteSpace(consulService.Url))
            {
                if (string.IsNullOrWhiteSpace(consulService.Scheme))
                {
                    consulService.Scheme = "http";
                }
                if (string.IsNullOrWhiteSpace(consulService.IP))
                {
                    throw new NullReferenceException("服务发现配置不正确，必须配置ConsulConfig:ConsulService:IP节点");
                }
                if (consulService.Port == 0)
                {
                    throw new NullReferenceException("服务发现配置不正确，必须配置ConsulConfig:ConsulService:Port节点");
                }
            }

            //获取本机的IP地址、端口号、Scheme
            if (string.IsNullOrWhiteSpace(consulConfig.APIName))
            {
                throw new NullReferenceException("API名称不能为空，必须配置ConsulConfig:ApiName节点");
            }
            //获取本机的IP地址、端口号、Scheme
            if (string.IsNullOrWhiteSpace(consulConfig.Scheme))
            {
                consulConfig.Scheme = "http";
            }
            if (string.IsNullOrWhiteSpace(consulConfig.IP))
            {
                consulConfig.IP = GetLocalIPV4();

                //Console.WriteLine(consulConfig.IP);
            }
            if (consulConfig.Port == 0)
            {
                var urls = configuration["urls"];
                if (!string.IsNullOrWhiteSpace(urls))
                {
                    consulConfig.Port = int.Parse(urls.Split(";").Where(e => e.Contains(consulConfig.Scheme + "://")).Select(e => e.Substring(e.LastIndexOf(":") + 1)).FirstOrDefault());
                }
                //Console.WriteLine(consulConfig.Port);
            }

            if (consulConfig.Port == 0)
            {
                throw new NullReferenceException("服务发现配置不正确，必须配置ConsulConfig:Port节点");
            }

            return consulConfig;
        }

        /// <summary>
        /// 健康检查 （可以使用  app.UseHealthChecks(healthConfig.Url);//consul 使用 .net core 自带健康检查 api）
        /// </summary>
        /// <param name="app"></param>
        private static void HealthMap(IApplicationBuilder app) //consul 
        {
            //app.UseHealthChecks(healthConfig.Url);//consul 使用 .net core 自带健康检查 api

            app.Run(async context =>
            {
                await context.Response.WriteAsync("OK");
            });
        }

        private static string GetLocalIPV4()
        {
            var ip = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()
                    .Select(p => p.GetIPProperties())
                    .SelectMany(p => p.UnicastAddresses)
                    .Where(p => p.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork && !System.Net.IPAddress.IsLoopback(p.Address))
                    .FirstOrDefault()?.Address.ToString();

            return ip;
        }

    }
    public class ConsulConfig
    {
        /// <summary>
        /// API唯一名称
        /// </summary>
        public string APIName { get; set; }
        public string IP { get; set; }
        public int Port { get; set; }
        public string Scheme { get; set; }
        /// <summary>
        /// 健康检查 Action 名称（非必填）
        /// </summary>
        public string HealthUrl { get; set; } = "/health";
        /// <summary>
        /// 健康检查失败后多久从consul中移除该服务(单位：s)，0表示一直不移除
        /// </summary>
        public int DeregisterCriticalServiceAfter { get; set; } = 60;//健康检查失败后多久从consul中移除该服务(单位：s)，0表示一直不移除
        /// <summary>
        /// //健康检查时间间隔，或者称为心跳间隔
        /// </summary>
        public int Interval { get; set; } = 10;//健康检查时间间隔，或者称为心跳间隔
        /// <summary>
        /// 
        /// </summary>
        public int Timeout { get; set; } = 5;
        public ConsulService ConsulService { get; set; }
    }
    public class ConsulService
    {
        public string Url { get; set; }
        public string IP { get; set; }
        public int Port { get; set; }
        public string Scheme { get; set; }
    }
}
