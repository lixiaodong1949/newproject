﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yssx.Users.WebHost.Auth
{
    public class AuthorizationFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context.Filters.Any(item => item is IAllowAnonymousFilter))
            {
                return;
            }

            if (!(context.ActionDescriptor is ControllerActionDescriptor))
            {
                return;
            }
            //判断token有效性
            var claims = context.HttpContext.User.Claims;
            
            //var dicClaim = claims.ToDictionary(m => m.Type, m => m.Value);
            //dicClaim.TryGetValue("name", out var name);
            //dicClaim.TryGetValue("UserId", out var id);
            return;
        }
    }
}
