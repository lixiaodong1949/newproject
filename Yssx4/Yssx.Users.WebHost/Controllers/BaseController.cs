﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Yssx.Users.WebHost.Controllers
{
    public class BaseController : Controller
    {

        /// <summary>
        ///  Called before the action method is invoked.
        /// </summary>
        /// <param name="context"></param>
        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            foreach (var c in context.HttpContext.User.Claims)
            {
                string typeStr = c.Type.ToString();
                string value = c.Value;
            }
        }
    }
}