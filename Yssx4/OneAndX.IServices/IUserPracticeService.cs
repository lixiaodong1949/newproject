﻿using OneAndX.Dto;
using OneAndX.Dto.UserPratice;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.IServices
{
    /// <summary>
    /// 信息
    /// </summary>
    public interface IUserPracticeService
    {

        /// <summary>
        /// 查询案例列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<List<PraticeCaseListResponseDto>>> GetCaseListPageByCertificate(PraticeCaseQueryRequestDto queryDto);
        /// <summary>
        /// 新增通关记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddGradeRecord(GradeRecordRequestDto model);
        /// <summary>
        /// 通关记录查询
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="caseId">自主训练是根据userid+caseid查询</param>
        /// <param name="certificate">考前自测是根据userid+certificate查询</param>
        /// <returns></returns>
        Task<ResponseContext<List<GradeRecordRequestDto>>> GetGradeRecordList(long userId, long caseId, Certificate? certificate,int examType);
        /// <summary>
        /// 获取题目信息
        /// </summary>
        /// <param name="caseSourceSystem"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<QuestionInfoDto>>> GetQuestionInfo(CaseSourceSystem caseSourceSystem, long caseId);

        Task<ResponseContext<List<AllCaseDto>>> GetAllCaseList(long userId, PraticeCaseQueryDto dto);

        Task<ResponseContext<List<RandomCaseDto>>> GetRandomCaseList(Certificate cf, CertificateLevel cfl);
    }  
}
