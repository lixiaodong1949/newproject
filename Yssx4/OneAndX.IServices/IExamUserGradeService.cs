﻿using OneAndX.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.IServices
{
    /// <summary>
    /// 考试成绩
    /// </summary>
    public interface IExamUserGradeService
    {
        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<List<ExamUserGradeListResponseDto>>> GetList(ExamUserGradeQueryRequestDto queryDto);

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Add(ExamUserGradeAddDto dto, long currentUserId);

        Task<ResponseContext<RandomUserGradeDto>> GetRandomUser();
    }
}
