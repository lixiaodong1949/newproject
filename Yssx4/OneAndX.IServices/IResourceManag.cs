﻿using OneAndX.Dto;
using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.IServices
{
    /// <summary>
    /// 资源管理
    /// </summary>
    public interface IResourceManag
    {
        /// <summary>
        /// 添加 更新 考点
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateExaminationSite(ExaminationSiteDto model, long Id);

        /// <summary>
        /// 查找 考点列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<ExaminationSiteResponse>> FindExaminationSite(ExaminationSiteRequest model);

        /// <summary>
        /// 查找 考点详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<ExaminationSiteDetailsRequest>> FindExaminationSiteDetails(long Id);

        /// <summary>
        /// 删除 考点
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveExaminationSite(long id, long currentUserId);

        /// <summary>
        /// 删除考场
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveExaminationRoom(long id);



        /// <summary>
        /// 添加 更新 教材 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> AddUpdateTextBook(TextBookDto model, long Id);


        /// <summary>
        /// 添加 更新 章节
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<List<TextBookDetails>>> AddUpdateChapter(TextBookChapterDto model, long Id);

        /// <summary>
        /// 查找 教材
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<TextBookResponse>> FindTextBook(TextBookRequest model);

        /// <summary>
        /// 编辑 预览教材 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<TextBookPreviewResponse>> FindTextBookPreview(long Id);

        /// <summary>
        /// 删除 教材
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveTextBook(long id, long currentUserId);


        /// <summary>
        /// 删除 章节
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveChapter(long id, long currentUserId);



        /// <summary>
        /// 根据下拉模块带出数据
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<List<TextBooksAllModuleDto>>> TeachersAllClasses(int Module);

        /// <summary>
        /// 查找 预览教材（前台）
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<TextBooksResponse>> FindTextBooks(TextBooksRequest model);




        /// <summary>
        /// 添加 更新 新闻
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateNews(NewsDto model, long Id);

        /// <summary>
        /// 查找 新闻
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<NewsResponse>> FindNews(NewsRequest model);

        /// <summary>
        /// 删除 新闻
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveNews(long id, long currentUserId);


        /// <summary>
        /// 查询 新闻（前台）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<NewsHomeResponse>>> FindNewsHome();

    }
}