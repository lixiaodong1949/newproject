﻿using OneAndX.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;

namespace OneAndX.IServices
{
    /// <summary>
    /// 登录接口
    /// </summary>
    public interface ILoginService
    {
        /// <summary>
        /// 账号密码登陆
        /// </summary>
        /// <param name="dto">用户信息</param>
        /// <returns></returns>
        Task<ResponseContext<UserTicket>> UserPasswordLogin(UserLoginInDto dto, string loginIp, PermissionRequirement _requirement);

        /// <summary>
        /// 登录考试
        /// </summary>
        /// <returns></returns>    
        Task<ResponseContext<ExamCurrentBaseInfo>> LoginExam(string examCardNumber, string examPassword, string loginIp, PermissionRequirement _requirement);

        /// <summary>
        /// 获取一条考生准考证信息
        /// </summary>
        /// <returns></returns> 
        Task<ResponseContext<ExamStudentBaseInfoDto>> GetLoginExam(string examCardNumber = "");

    }
}
