﻿using OneAndX.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace OneAndX.IServices
{
    /// <summary>
    /// 考试管理接口(前台)
    /// </summary>
    public interface IExamManagerFrontService
    {
        /// <summary>
        /// 获取考试计划列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<ExamPlanViewModel>>> GetExamPlanList(ExamPlanQuery query);

        /// <summary>
        /// 获取已报名考试计划考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SignUpExamRoomViewModel>>> GetSignUpExamRoomList(long planId);

        /// <summary>
        /// 随机分配考生考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        Task<ResponseContext<int>> RandomAllotExamRoom(long planId, long userId);

        /// <summary>
        /// 获取已随机分配考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SignUpExamRoomViewModel>>> GetAllotedExamRoomList(long planId);

        /// <summary>
        /// 随机生成准考证和密码
        /// </summary>
        /// <param name="planId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<int>> RandomCreateExamCardNumber(long planId, long userId);

        /// <summary>
        /// 获取考生信息列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<ExamineeInfoViewModel>> GetExamineeInfoList(ExamineeInfoQuery query);

        /// <summary>
        /// 随机生成试卷
        /// </summary>
        /// <param name="planId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RandomCreateExamPaper(long planId, long userId);

        /// <summary>
        /// 获取考生成绩列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<ExamineeGradeViewModel>> GetExamineeGradeList(ExamineeGradeQuery query);

        /// <summary>
        /// 获取考试计划试卷信息列表
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<PlanExamPaperInfoViewModel>>> GetPlanExamPaperInfoList(long planId);

        /// <summary>
        /// 获取正式考试试卷信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<OfficialExamPaperInfoViewModel>> GetOfficialExamPaperInfoList(long userId);
    }
}
