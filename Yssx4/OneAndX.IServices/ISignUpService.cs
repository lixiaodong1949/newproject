﻿using OneAndX.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Entity;

namespace OneAndX.IServices
{
    public interface ISignUpService
    {
        /// <summary>
        /// 添加 更新 报名 （考证报名 模块）
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateSignUp(SignUpDto model, long Id);

        /// <summary>
        /// 根据Id 查询单条表明信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<SignUpDto>> GetInfo(long id);

        /// <summary>
        /// 获取个人报名信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<GetSignUpListDto>>> GetSignUpList(long Id);

        /// <summary>
        /// 根据考试计划Id获取对应的考点信息
        /// </summary>
        /// <param name="examPlanId"></param>
        Task<ResponseContext<List<ExamSiteProvinceDto>>> GetExamSiteByExamPlanId(long examPlanId);

        /// <summary>
        /// 获取用户准考证信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<UserExamCardNumberViewModel>> GetUserExamCardNumberInfo(long userId);

    }
}
