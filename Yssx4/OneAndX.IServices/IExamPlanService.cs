﻿using OneAndX.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.IServices
{
    /// <summary>
    /// 考试计划
    /// </summary>
    public interface IExamPlanService
    {
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Update(ExamPlanUpdateDto queryDto, long currentUserId);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Delete(long id, long currentUserId);

        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<PageResponse<ExamPlanListPageResponseDto>> GetListPage(ExamPlanQueryRequestDto queryDto);

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Add(ExamPlanAddDto dto, long currentUserId);

        /// <summary>
        /// 查询考试计划基本信息列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CertificateExamPlan>>> GetExamPlanBaseInfoList();
    }
}
