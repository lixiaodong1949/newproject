﻿using FreeSql.DataAnnotations;
using System;

namespace Sx.Course.Entitys
{
    /// <summary>
    /// 课程章节汇总概要
    /// </summary>
    [Table(Name = "yssx_section_summary")]
    public class YssxSectionSummary
    {
        public long Id { get; set; }

        public long CourseId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SummaryType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //public string SummaryName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 操作更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
        //public string JsonContent { get; set; }
    }
}
