﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Sx.Course.Entitys
{
    /// <summary>
    /// 课程科目表
    /// </summary>
    [Table(Name = "yssx_course_subject")]
    public class YssxCourseSubject : BizBaseEntity<long>
    {
        /// <summary>
        /// 科目代码
        /// </summary>
        [Column(Name = "SubjectCode", DbType = "varchar(20)", IsNullable = false)]
        public long SubjectCode { get; set; }

        /// <summary>
        /// 父级ID 最多四级
        /// </summary>
        [Column(Name = "ParentId", DbType = "varchar(20)", IsNullable = true)]
        public long ParentId { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 创建者
        /// </summary>
        public long CreateBy { get; set; }

        /// <summary>
        /// 一级代码
        /// </summary>
        [Column(Name = "Code1", DbType = "varchar(20)", IsNullable = true)]
        public string Code1 { get; set; }

        /// <summary>
        /// 二级代码
        /// </summary>
        [Column(Name = "Code2", DbType = "varchar(20)", IsNullable = true)]
        public string Code2 { get; set; }

        /// <summary>
        /// 三级代码
        /// </summary>
        [Column(Name = "Code3", DbType = "varchar(20)", IsNullable = true)]
        public string Code3 { get; set; }

        /// <summary>
        /// 四级代码
        /// </summary>
        [Column(Name = "Code4", DbType = "varchar(20)", IsNullable = true)]
        public string Code4 { get; set; }

        /// <summary>
        /// 科目名称
        /// </summary>
        [Column(Name = "SubjectName", DbType = "varchar(100)", IsNullable = true)]
        public string SubjectName { get; set; }

        /// <summary>
        /// 余额方向
        /// </summary>
        [Column(Name = "Direction", DbType = "varchar(20)", IsNullable = true)]
        public string Direction { get; set; }

        /// <summary>
        /// 科目类型(1:资产,2:负债)
        /// </summary>
        public int SubjectType { get; set; }

        /// <summary>
        /// 助记码
        /// </summary>
        [Column(Name = "AssistCode", DbType = "varchar(50)", IsNullable = true)]
        public string AssistCode { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        public decimal CreditorAmount { get; set; }
    }
}
