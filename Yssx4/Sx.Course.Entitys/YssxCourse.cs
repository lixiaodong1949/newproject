﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework.Entity;

namespace Sx.Course.Entitys
{
    /// <summary>
    /// 课程
    /// </summary>
    [Table(Name = "yssx_course")]
    public class YssxCourse : TenantBaseEntity<long>
    {
        /// <summary>
        /// 课程名称
        /// </summary>
        [Column(Name = "CourseName", DbType = "varchar(255)", IsNullable = true)]
        public string CourseName { get; set; }

        /// <summary>
        /// 课程类型0:常规课
        /// </summary>
        public int CourseType { get; set; }

        /// <summary>
        /// 特点副标
        /// </summary>
        [Column(Name = "CourseTitle", DbType = "varchar(255)", IsNullable = true)]
        public string CourseTitle { get; set; }

        /// <summary>
        /// 课程类型
        /// </summary>
        public long CourseTypeId { get; set; }

        /// <summary>
        /// 知识点
        /// </summary>
        public long KnowledgePointId { get; set; }

        /// <summary>
        /// 学历标识
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        [Column(Name = "Author", DbType = "varchar(100)", IsNullable = true)]
        public string Author { get; set; }

        /// <summary>
        /// 出版社
        /// </summary>
        [Column(Name = "PublishingHouse", DbType = "varchar(100)", IsNullable = true)]
        public string PublishingHouse { get; set; }

        /// <summary>
        /// 出版时间
        /// </summary>
        public string PublishingDate { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// 审核状态(0,待审核，1，审核成功，2，审核拒绝)
        /// </summary>
        public int AuditStatus { get; set; }

        /// <summary>
        /// 审核说明
        /// </summary>
        public string AuditStatusExplain { get; set; }

        /// <summary>
        /// 是否上架(0,待上架，1，上架待审，2，上架)
        /// </summary>
        public int Uppershelf { get; set; }

        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; }


        /// <summary>
        /// 简介
        /// </summary>
        [Column(Name = "Intro", DbType = "varchar(2000)", IsNullable = true)]
        public string Intro { get; set; }

        /// <summary>
        /// 描述备注
        /// </summary>
        [Column(Name = "Description", DbType = "text", IsNullable = true)]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreateByName { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadCount { get; set; }
    }

    /// <summary>
    /// 课件库
    /// </summary>
    [Table(Name = "yssx_course_files")]
    public class YssxCourceFiles : TenantAndUserEntity<long>
    {
        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string File { get; set; }

        public int Sort { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 知识点组Id
        /// </summary>
        public long KlgpGroupId { get; set; }

        /// <summary>
        /// 创建人姓名
        /// </summary>
        public string CreateByName { get; set; }


        /// <summary>
        /// 文件类型0:Excel,1:Word,2:PDF,3:PPT,4:Img,5:ZIP
        /// </summary>
        public int FilesType { get; set; }

        /// <summary>
        /// 0:课件,1:视频,2:教案
        /// </summary>
        public int CategoryType { get; set; }

        /// <summary>
        /// 审核状态(0,待审核，1，审核成功，2，审核拒绝)
        /// </summary>
        public int AuditStatus { get; set; }

        /// <summary>
        /// 审核说明
        /// </summary>
        public string AuditStatusExplain { get; set; }

        /// <summary>
        /// 是否上架(0,待上架，1，上架待审，2，上架)
        /// </summary>
        public int Uppershelf { get; set; }

        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; }


        /// <summary>
        /// 文件大小
        /// </summary>
        public int FileSize { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadCount { get; set; }
    }
}
