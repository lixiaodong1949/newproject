﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using NLog.Extensions.Logging;
using OneAndX.Api.Auth;
using OneAndX.IServices;
using OneAndX.Services;
using System;
using System.IO;
using System.Linq;
using System.Text;
using Tas.Common.Configurations;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Logger;
using Yssx.Framework.Payment.Alipay;
using Yssx.Framework.Payment.Webchat;
using Yssx.Framework.Redis;
using Yssx.Framework.Utils;
using Yssx.Framework.Web.Filter;
using Yssx.Redis;
using Yssx.Repository.Extensions;
using Yssx.Swagger;

namespace OneAndX.Api
{
    public class Startup
    {
        private CustsomSwaggerOptions CURRENT_SWAGGER_OPTIONS = new CustsomSwaggerOptions()
        {
            ProjectName = "1+X证书",
            ApiVersions = new string[] { "v1" },//要显示的版本
            UseCustomIndex = false,
            RoutePrefix = "swagger",

            AddSwaggerGenAction = c =>
            {
                var xmlPaths = Directory.GetFiles(AppContext.BaseDirectory, "OneAndX.*.xml").ToList();
                var xmlPaths2 = Directory.GetFiles(AppContext.BaseDirectory, "Yssx.*.xml").ToList();
                xmlPaths.AddRange(xmlPaths2);
                foreach (var path in xmlPaths)
                {
                    c.IncludeXmlComments(path, true);
                }
            },
            UseSwaggerAction = c =>
            {

            },
            UseSwaggerUIAction = c =>
            {
            }
        };
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        private void AddTokenValidation(IServiceCollection services)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(CommonConstants.IdentServerSecret));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var permissionRequirement = new PermissionRequirement(signingCredentials);
            services.AddSingleton(permissionRequirement);
        }
        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddScoped<CustomExceptionFilter>();
            services.AddMvc(options =>
            {
                options.Filters.Add<ActionModelStateFilter>();
                options.Filters.Add<AuthorizationFilter>();

                //options.Filters.Add<Filters.HttpGlobalExceptionFilter>();


            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddJsonOptions(options =>
            {
                //忽略循环引用
                //options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                //options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                //设置时间格式
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();//json字符串大小写原样输出
            }).ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            //版本控制
            // services.AddMvcCore().AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'VVV");

            services.AddApiVersioning(option =>
            {
                // allow a client to call you without specifying an api version
                // since we haven't configured it otherwise, the assumed api version will be 1.0
                option.AssumeDefaultVersionWhenUnspecified = true;
                option.ReportApiVersions = false;
            })
            .AddCustomSwagger(CURRENT_SWAGGER_OPTIONS)
            .AddCors(options =>
            {
                options.AddPolicy(CommonConstants.AnyOrigin, builder =>
                {
                    builder.AllowAnyOrigin() //允许任何来源的主机访问                             
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();//指定处理cookie                                   
                });
            });
            services.AddHttpClient();
            //services.AddAutoMapper();
            AddTokenValidation(services);
            InitService(services);
        }

        /// <summary>
        /// 依赖注入业务服务
        /// </summary>
        /// <param name="services"></param>
        private void InitService(IServiceCollection services)
        {
            services.AddTransient<ILoginService, LoginService>();
            services.AddTransient<IRegisterService, RegisterService>();
            services.AddTransient<IExamCaseService, ExamCaseService>();
            services.AddTransient<IExamPlanService, ExamPlanService>();
            services.AddTransient<IExamUserGradeService, ExamUserGradeService>();
            services.AddTransient<ISignUpService, SignUpService>();
            services.AddTransient<IResourceManag, ResourceManagService>(); 
            services.AddTransient<IUserPracticeService, UserPracticeService>();
            services.AddTransient<IExamManagerBackService, ExamManagerBackService>();
            services.AddTransient<IExamManagerFrontService, ExamManagerFrontService>();
            services.AddTransient<IPreviouExamService, PreviouExamService>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime lifetime)
        {
            app.UseExceptionHandler(builder =>
            {
                builder.Run(async context =>
                {
                    context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                    context.Response.ContentType = "application/json";
                    var ex = context.Features.Get<Microsoft.AspNetCore.Diagnostics.IExceptionHandlerFeature>();
                    if (ex?.Error != null)
                    {
                        CommonLogger.Error("UseExceptionHandler", ex?.Error);
                    }
                    await context.Response.WriteAsync(ex?.Error?.Message ?? "an error occure");
                });
            }).UseHsts();
            //string enableSwagger = Configuration["swaggerEnable"];
            app.UseMvc().UseCustomSwagger(CURRENT_SWAGGER_OPTIONS);
            //app.UseMvc();
            //if (enableSwagger == "1")
            //{
            //    app.UseCustomSwagger(CURRENT_SWAGGER_OPTIONS);
            //}
            app.UseCors(CommonConstants.AnyOrigin);
            app.UseStaticFiles();
            ////加载Nlog的nlog.config配置文件
            //NLog.LogManager.LoadConfiguration("nlog.config");

            //string[] servers = Configuration["ImServers"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);//im服务器地址
            ProjectConfiguration.GetInstance().
                RegisterConfiguration(Configuration).
                UseCommonLogger(loggerFactory.AddNLog()).
                UseRedis(Configuration["RedisConnection"]).
                UseRedisJs(Configuration["JsRedisConnection"]).
                UseRedisHys(Configuration["HysRedisConnection"]).
                UseRedisSx(Configuration["SxRedisConnection"]).
                UseFreeSqlRepository(Configuration["DbConnection"]).
                UseFreeSqlRepositoryJs(Configuration["JsDbConnection"]).
                UseFreeSqlRepositoryHys(Configuration["HysDbConnection"]).
                UseFreeSqlRepositorySx(Configuration["SxDbConnection"]).
                //UseFreeSqlBDRepository(Configuration["DbBigDataConnection"], Configuration["BigDbConnection1"], Configuration["BigDbConnection2"], Configuration["BigDbConnection3"], Configuration["BigDbConnection4"]).
                //UseFreeSqlBDRepository(Configuration["DbBigDataConnection"], Configuration["BigDbConnection1"], Configuration["BigDbConnection2"]).
                UseAutoMapper(RuntimeHelper.GetAllAssemblies());
            //UseImClient(Configuration["ImRedisConnection"], servers);

            DataMergeHelper.Start();
        }
    }
}
