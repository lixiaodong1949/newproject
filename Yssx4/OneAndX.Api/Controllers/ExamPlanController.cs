﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{

    /// <summary>
    /// 考试管理(后台)
    /// </summary>
    [AllowAnonymous]
    public class ExamPlanController : BaseController
    {
        IExamPlanService service = null;
        public ExamPlanController(IExamPlanService ExamPlanService)
        {
            service = ExamPlanService;
        }

        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<ExamPlanListPageResponseDto>> GetListPage(ExamPlanQueryRequestDto queryDto)
        {
            var result = new PageResponse<ExamPlanListPageResponseDto>();
            //if (queryDto.Certificate <= 0)
            //{
            //    result.Msg = "Certificate无效。";
            //    result.Code = CommonConstants.ErrorCode;
            //    return result;
            //}
            return await service.GetListPage(queryDto);
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Add(ExamPlanAddDto dto)
        {
            var result = new ResponseContext<bool>();
            //if (dto.Id <= 0)
            //{
            //    result.Msg = "Id无效。";
            //    result.Code = CommonConstants.ErrorCode;
            //    return result;
            //}

            if (dto.ApplyEndTime <= dto.ApplyBeginTime)
            {
                result.Msg = "报名时间设置不正确。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }

            if (dto.EndTime < dto.BeginTime)
            {
                result.Msg = "考试时间设置不正确,开始时间不能小于结束时间。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }

            if (dto.BeginTime <= dto.ApplyEndTime)
            {
                result.Msg = "报名结束时间必须要早于考试时间开始时间。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }

            dto.TotalMinutes = int.Parse((dto.EndTime - dto.BeginTime).TotalMinutes.ToString());

            return await service.Add(dto, 0);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Update(ExamPlanUpdateDto dto)
        {
            var result = new ResponseContext<bool>();
            if (dto.Id <= 0)
            {
                result.Msg = "Id无效。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }

            if (dto.ApplyEndTime <= dto.ApplyBeginTime)
            {
                result.Msg = "报名时间设置不正确。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }

            if (dto.EndTime <= dto.BeginTime)
            {
                result.Msg = "考试时间设置不正确。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }

            if (dto.BeginTime <= dto.ApplyEndTime)
            {
                result.Msg = "报名结束时间必须要早于考试时间开始时间。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }

            dto.TotalMinutes = int.Parse((dto.EndTime - dto.BeginTime).TotalMinutes.ToString());

            return await service.Update(dto, 0);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> Delete(long id)
        {
            var result = new ResponseContext<bool>();
            if (id <= 0)
            {
                result.Msg = "Id无效。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }
            return await service.Delete(id, 0); 
        }
        /// <summary>
        /// 查询考试计划基本信息列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CertificateExamPlan>>> GetExamPlanBaseInfoList()
        {
            //var result = new ResponseContext<bool>();
            //if (id <= 0)
            //{
            //    result.Msg = "Id无效。";
            //    result.Code = CommonConstants.ErrorCode;
            //    return result;
            //}
            return await service.GetExamPlanBaseInfoList();
        }
    }
}
