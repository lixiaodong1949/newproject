﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 历届考试
    /// </summary>
    public class PreviouExamController : BaseController
    {
        IPreviouExamService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public PreviouExamController(IPreviouExamService service)
        {
            _service = service;
        }

        #region 历届考试
        /// <summary>
        /// 获取历届考试列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<PageResponse<SavePreviouExamListDto>> GetPreviouExamList(GetPreviouExamListRequest model)
        {
            return await _service.GetPreviouExamList(model);
        }

        /// <summary>
        /// 保存历届考试
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> SavePreviouExam(SavePreviouExamListDto model)
        {
            return await _service.SavePreviouExam(model);
        }

        /// <summary>
        /// 删除历届考试
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> DeletePreviouExam(long id)
        {
            return await _service.DeletePreviouExam(id);
        }

        #endregion


        #region 花絮
        /// <summary>
        /// 获取历届考试 - 花絮列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<PageResponse<GetPreviouExamTidbitDto>> GetPreviouExamTidbitList(long examId)
        {
            return await _service.GetPreviouExamTidbitList(examId);
        }

        /// <summary>
        /// 保存历届考试 - 花絮
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> SavePreviouExamTidbit(SavePreviouExamTidbitDto model)
        {
            return await _service.SavePreviouExamTidbit(model);
        }

        /// <summary>
        /// 删除历届考试 - 花絮
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> DeletePreviouExamTidbit(long id, long examId)
        {
            return await _service.DeletePreviouExamTidbit(id, examId);
        }

        #endregion


        #region 培训资料
        /// <summary>
        /// 获取历届考试 - 培训资料列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<PageResponse<GetPreviouExamFileDto>> GetPreviouExamFileList(long examId)
        {
            return await _service.GetPreviouExamFileList(examId);
        }

        /// <summary>
        /// 保存历届考试 - 培训资料
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> SavePreviouExamFile(SavePreviouExamFileDto model)
        {
            return await _service.SavePreviouExamFile(model);
        }

        /// <summary>
        /// 删除历届考试 - 培训资料
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> DeletePreviouExamFile(long id, long examId)
        {
            return await _service.DeletePreviouExamFile(id, examId);
        }

        #endregion


        #region 报名用户和证书
        /// <summary>
        /// 获取历届考试 - 报名用户列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<PageResponse<GetPreviouExamUserListDto>> GetPreviouExamUserList(GetPreviouExamUserListRequest model)
        {
            return await _service.GetPreviouExamUserList(model);
        }

        /// <summary>
        /// 获取历届考试 - 证书列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<PageResponse<GetPreviouExamCertificateListDto>> GetPreviouExamCertificateList(GetPreviouExamUserListRequest model)
        {
            return await _service.GetPreviouExamCertificateList(model);
        }

        #endregion


    }
}