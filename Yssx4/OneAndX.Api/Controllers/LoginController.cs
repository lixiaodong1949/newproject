﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 登录服务
    /// </summary>
    public class LoginController : BaseController
    {
        /// <summary>
        /// 自定义策略参数
        /// </summary>
        PermissionRequirement _requirement;
        ILoginService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="requirement"></param>
        /// <param name="service"></param>
        public LoginController(PermissionRequirement requirement, ILoginService service)
        {
            _requirement = requirement;
            _service = service;
        }

        #region 账号密码登陆
        /// <summary>
        /// 账号密码登陆
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>      
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<UserTicket>> UserPasswordLogin(UserLoginInDto dto)
        {
            return await _service.UserPasswordLogin(dto, HttpContext.GetClientUserIp(), _requirement);
        }
        #endregion

        /// <summary>
        /// 登录考试
        /// </summary>
        /// <returns></returns>    
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<ExamCurrentBaseInfo>> LoginExam(string examCardNumber, string examPassword)
        {
            return await _service.LoginExam(examCardNumber, examPassword, HttpContext.GetClientUserIp(), _requirement);
        }

        /// <summary>
        /// 获取考生准考证信息
        /// </summary>
        /// <returns></returns>    
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<ExamStudentBaseInfoDto>> GetLoginExam(string examCardNumber="")
        {
            return await _service.GetLoginExam(examCardNumber);
        }
    }
}
