﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 试卷案例信息
    /// </summary>
    [AllowAnonymous]
    public class ExamCaseController : BaseController
    {
        IExamCaseService service = null;
        public ExamCaseController(IExamCaseService examCaseService)
        {
            service = examCaseService;
        }

        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<ExamCaseListPageResponseDto>> GetListPage(ExamCaseQueryRequestDto queryDto)
        {
            var result = new PageResponse<ExamCaseListPageResponseDto>();
            if (queryDto.Certificate <= 0)
            {
                result.Msg = "Certificate无效。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }
            return await service.GetListPage(queryDto);
        }

        /// <summary>
        /// 修改(后台)
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Update(ExamCaseUpdateDto dto)
        {
            var result = new ResponseContext<bool>();
            if (dto.Id <= 0)
            {
                result.Msg = "Id无效。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }
            return await service.Update(dto, 0);
        }
        /// <summary>
        /// 删除(后台)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> Delete(long id)
        {
            var result = new ResponseContext<bool>();
            if (id <= 0)
            {
                result.Msg = "Id无效。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }
            return await service.Delete(id, 0);
        }
        /// <summary>
        /// 查询案例的题目列表
        /// </summary>
        /// <param name="caseId">案例Id</param>
        /// <param name="sourceSystem">案例所属源系统</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<TopicBaseInfoResponseDto>>> GetCaseTopics(long caseId, CaseSourceSystem sourceSystem)
        {
            var result = new ResponseContext<List<TopicBaseInfoResponseDto>>();
            if (caseId <= 0)
            {
                result.Msg = "caseId无效。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }
            return await service.GetCaseTopics(caseId, sourceSystem);
        }
    }
}
