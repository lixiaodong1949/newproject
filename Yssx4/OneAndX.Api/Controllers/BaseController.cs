﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using OneAndX.Api.Auth;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Produces("application/json")]
    [ApiVersion("1.0", Deprecated = false)]
    [Route("oax/[controller]/[action]")]
    [ApiController]
    [EnableCors(CommonConstants.AnyOrigin)]//跨域
    [ServiceFilter(typeof(CustomExceptionFilter))]
    public class BaseController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        protected UserTicket CurrentUser { get; private set; }

        /// <summary>
        ///  Called before the action method is invoked.
        /// </summary>
        /// <param name="context"></param>
        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            HttpContext.Items.TryGetValue(CommonConstants.ContextUserPropertyKey, out var requestUser);
            CurrentUser = requestUser as UserTicket;
        }
    }
}