﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 新闻管理（后台）
    /// </summary>
    public class NewsManagController : BaseController
    {
        private IResourceManag _service;

        public NewsManagController(IResourceManag service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加 更新 新闻
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> AddUpdateNews(NewsDto model)
        {
            return await _service.AddUpdateNews(model, 1);//CurrentUser.Id
        }

        /// <summary>
        ///  查找 新闻
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<PageResponse<NewsResponse>> FindNews(NewsRequest model)
        {
            return await _service.FindNews(model);
        }

        /// <summary>
        ///  删除 新闻
        /// </summary>
        /// <param name="Id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> RemoveNews(long Id)
        {
            return await _service.RemoveNews(Id, 2);// CurrentUser.Id
        }
    }
}