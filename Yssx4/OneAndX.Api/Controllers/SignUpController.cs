﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 考证报名
    /// </summary>  
    public class SignUpController : BaseController
    {
        private ISignUpService _service;

        public SignUpController(ISignUpService service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加 更新 报名 （考证报名 模块）
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateSignUp(SignUpDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Data = false };

            if (string.IsNullOrWhiteSpace(model.IDNumber) || model.IDNumber.Length != 18)
            {
                result.Msg = "IDNumber无效。";
                return result;
            }
            if (string.IsNullOrWhiteSpace(model.Name))
            {
                result.Msg = "Name无效。";
                return result;
            }
            long currentUserId = 0;
            if (CurrentUser != null)
                currentUserId = CurrentUser.Id;

            return await _service.AddUpdateSignUp(model, currentUserId);
        }

        /// <summary>
        /// 根据Id 查询单条表明信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<SignUpDto>> GetInfo(long id)
        {
            return await _service.GetInfo(id);
        }

        /// <summary>
        /// 根据考试计划Id获取对应的考点信息
        /// </summary>
        /// <param name="examPlanId">考试计划Id</param>
        [HttpGet]
        public async Task<ResponseContext<List<ExamSiteProvinceDto>>> GetExamSiteByExamPlanId(long examPlanId)
        {
            return await _service.GetExamSiteByExamPlanId(examPlanId);
        }
        ///// <summary>
        ///// 查找 报名列表（不分页）（个人中心-我的报名 模块）
        ///// </summary>
        ///// <param name="id">Id</param>
        ///// <returns></returns>    
        //[HttpPost]
        //public async Task<ResponseContext<List<SignUpDto>>> FindSignUp(SignUpRequest model)
        //{
        //    return await _service.FindSignUp(model, CurrentUser.Id);
        //}

        /// <summary>
        /// 获取个人报名信息
        /// </summary>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<List<GetSignUpListDto>>> GetSignUpList(long userId)
        {
            var result = new ResponseContext<List<GetSignUpListDto>> { Code = CommonConstants.BadRequest, Data = null };
            if (userId <= 0)
            {
                result.Msg = "userId无效。";
                return result;
            }

            long currentUserId = 0;
            if (CurrentUser != null)
                currentUserId = CurrentUser.Id;

            //if (userId != currentUserId)
            //{
            //    result.Msg = "userId无效。";
            //    return result;
            //}

            return await _service.GetSignUpList(userId);
        }

        #region 获取用户准考证信息
        /// <summary>
        /// 获取用户准考证信息
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ResponseContext<UserExamCardNumberViewModel>> GetUserExamCardNumberInfo(long userId)
        {
            return await _service.GetUserExamCardNumberInfo(userId);
        }
        #endregion

    }
}