﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 资源管理（后台）
    /// </summary>
    public class ResourceManagController : BaseController
    {
        private IResourceManag _service;

        public ResourceManagController(IResourceManag service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加 更新 考点
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> AddUpdateExaminationSite(ExaminationSiteDto model)
        {
            return await _service.AddUpdateExaminationSite(model, 1);//CurrentUser.Id
        }

        /// <summary>
        /// 查找 考点列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<PageResponse<ExaminationSiteResponse>> FindExaminationSite(ExaminationSiteRequest model)
        {
            return await _service.FindExaminationSite(model);
        }

        /// <summary>
        ///  查找 考点详情
        /// </summary>
        /// <param name="Id">考点Id</param>
        /// <returns></returns>    
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<ExaminationSiteDetailsRequest>> FindExaminationSiteDetails(long Id)
        {
            return await _service.FindExaminationSiteDetails(Id);
        }

        /// <summary>
        ///  删除 考点
        /// </summary>
        /// <param name="Id">考点Id</param>
        /// <returns></returns>    
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> RemoveExaminationSite(long Id)
        {
            return await _service.RemoveExaminationSite(Id,2);// CurrentUser.Id
        }

        /// <summary>
        ///  删除 考场
        /// </summary>
        /// <param name="Id">考场Id</param>
        /// <returns></returns>    
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> RemoveExaminationRoom(long Id)
        {
            return await _service.RemoveExaminationRoom(Id);
        }
    }
}