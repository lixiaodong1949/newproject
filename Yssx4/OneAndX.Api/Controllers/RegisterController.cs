﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using OneAndX.Pocos;
using OneAndX.Services;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 注册服务
    /// </summary>
    public class RegisterController : BaseController
    {
        IRegisterService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public RegisterController(IRegisterService service)
        {
            _service = service;
        }

        #region 验证手机号码
        /// <summary>
        /// 验证手机号码
        /// </summary>
        /// <param name="mobilePhone">手机号码</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> VerifyPhoneNumber(string mobilePhone)
        {
            return await _service.VerifyPhoneNumber(mobilePhone);
        }
        #endregion

        #region 获取验证码
        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <param name="mobilePhone">手机号码</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ResponseContext<bool> GetVerifyCode(string mobilePhone)
        {
            return RegisterService.SendSms(mobilePhone, null);
        }
        #endregion

        #region 校验注册验证码
        /// <summary>
        /// 校验注册验证码
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> CheckVerificationCode(MobilePhoneVerificationCodeDto dto)
        {
            return await _service.CheckVerificationCode(dto);
        }
        #endregion

        #region 注册用户
        /// <summary>
        /// 注册用户
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<SxYssxUser>> RegisteredUsers(UserRegisterDto dto)
        {
            return await _service.RegisteredUsers(dto);
        }
        #endregion

    }
}
