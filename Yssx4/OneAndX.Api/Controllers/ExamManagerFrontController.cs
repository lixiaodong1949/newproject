﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 考试管理服务(前台)
    /// </summary>
    public class ExamManagerFrontController : BaseController
    {
        IExamManagerFrontService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public ExamManagerFrontController(IExamManagerFrontService service)
        {
            _service = service;
        }

        #region 获取考试计划列表
        /// <summary>
        /// 获取考试计划列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<ExamPlanViewModel>>> GetExamPlanList([FromQuery]ExamPlanQuery query)
        {
            return await _service.GetExamPlanList(query);
        }
        #endregion

        #region 获取已报名考试计划考场
        /// <summary>
        /// 获取已报名考试计划考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SignUpExamRoomViewModel>>> GetSignUpExamRoomList(long planId)
        {
            return await _service.GetSignUpExamRoomList(planId);
        }
        #endregion

        #region 随机分配考生考场
        /// <summary>
        /// 随机分配考生考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<int>> RandomAllotExamRoom(long planId)
        {
            return await _service.RandomAllotExamRoom(planId, this.CurrentUser.Id);
        }
        #endregion

        #region 获取已随机分配考场
        /// <summary>
        /// 获取已随机分配考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SignUpExamRoomViewModel>>> GetAllotedExamRoomList(long planId)
        {
            return await _service.GetAllotedExamRoomList(planId);
        }
        #endregion

        #region 随机生成准考证和密码
        /// <summary>
        /// 随机生成准考证和密码
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<int>> RandomCreateExamCardNumber(long planId)
        {
            return await _service.RandomCreateExamCardNumber(planId, this.CurrentUser.Id);
        }
        #endregion

        #region 获取考生信息列表
        /// <summary>
        /// 获取考生信息列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<ExamineeInfoViewModel>> GetExamineeInfoList([FromQuery]ExamineeInfoQuery query)
        {
            return await _service.GetExamineeInfoList(query);
        }
        #endregion

        #region 随机生成试卷
        /// <summary>
        /// 随机生成试卷
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RandomCreateExamPaper(long planId)
        {
            return await _service.RandomCreateExamPaper(planId, this.CurrentUser.Id);
        }
        #endregion

        #region 获取考生成绩列表
        /// <summary>
        /// 获取考生成绩列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<ExamineeGradeViewModel>> GetExamineeGradeList([FromQuery]ExamineeGradeQuery query)
        {
            return await _service.GetExamineeGradeList(query);
        }
        #endregion

        #region 获取考试计划试卷信息列表
        /// <summary>
        /// 获取考试计划试卷信息列表
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<PlanExamPaperInfoViewModel>>> GetPlanExamPaperInfoList(long planId)
        {
            return await _service.GetPlanExamPaperInfoList(planId);
        }
        #endregion

        #region 获取正式考试试卷信息
        /// <summary>
        /// 获取正式考试试卷信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<OfficialExamPaperInfoViewModel>> GetOfficialExamPaperInfoList()
        {
            return await _service.GetOfficialExamPaperInfoList(this.CurrentUser.Id);
        }
        #endregion

    }
}
