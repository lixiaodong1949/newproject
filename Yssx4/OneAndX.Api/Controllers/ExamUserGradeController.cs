﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 用户成绩管理（前台）
    /// </summary>
    public class ExamUserGradeController : BaseController
    {
        IExamUserGradeService service = null;
        public ExamUserGradeController(IExamUserGradeService _service)
        {
            service = _service;
        }
        /// <summary>
        /// 查询考试成绩列表
        /// </summary>
        /// <param name="userIDNumber">考生身份证号</param>
        /// <param name="userName">考生姓名</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ResponseContext<List<ExamUserGradeListResponseDto>>> GetList(string userIDNumber, string userName)
        {
            var result = new ResponseContext<List<ExamPlanListPageResponseDto>>();

            ExamUserGradeQueryRequestDto queryDto = new ExamUserGradeQueryRequestDto
            {
                UserName = userName,
                UserIDNumber = userIDNumber,
            };
            //if (queryDto.Certificate <= 0)
            //{
            //    result.Msg = "Certificate无效。";
            //    result.Code = CommonConstants.ErrorCode;
            //    return result;
            //}
            return await service.GetList(queryDto);
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]        
        public async Task<ResponseContext<bool>> Add(ExamUserGradeAddDto dto)
        {
            var result = new ResponseContext<bool>();

            return await service.Add(dto, CurrentUser.Id);
        }

        [HttpGet]
        public async Task<ResponseContext<RandomUserGradeDto>> GetRandomUser()
        {
            return await service.GetRandomUser();
        }
    }
}
