﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.IServices;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 考试管理服务(后台)
    /// </summary>
    public class ExamManagerBackController : BaseController
    {
        IExamManagerBackService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public ExamManagerBackController(IExamManagerBackService service)
        {
            _service = service;
        }

        #region 获取某等级证书考场列表
        /// <summary>
        /// 获取某等级证书考场列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<CertificateExamRoomViewModel>>> GetCertificateExamRoomList([FromQuery]CertificateExamRoomQuery query)
        {
            return await _service.GetCertificateExamRoomList(query);
        }
        #endregion

        #region 分配考试计划考场
        /// <summary>
        /// 分配考试计划考场
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> AddPlanExamRoom(PlanExamRoomDto dto)
        {
            return await _service.AddPlanExamRoom(dto);
        }
        #endregion

        #region 获取已分配考试计划考场
        /// <summary>
        /// 获取已分配考试计划考场
        /// </summary>
        /// <param name="planId"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<DistributeExamRoomViewModel>>> GetDistributeExamRoomList(long planId)
        {
            return await _service.GetDistributeExamRoomList(planId);
        }
        #endregion

        #region 删除已分配考试计划考场或考点
        /// <summary>
        /// 删除已分配考试计划考场或考点
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> DeleteDistributeExamSiteOrRoom(DeleteDistributeExamSiteOrRoomDto dto)
        {
            return await _service.DeleteDistributeExamSiteOrRoom(dto);
        }
        #endregion

    }
}
