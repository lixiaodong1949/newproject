﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OneAndX.Dto;
using OneAndX.Dto.UserPratice;
using OneAndX.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Api.Controllers
{
    /// <summary>
    /// 用户练习(后台)
    /// </summary>
    [AllowAnonymous]
    public class UserPracticeController : BaseController
    {
        IUserPracticeService service = null;
        public UserPracticeController(IUserPracticeService userPracticeService)
        {
            service = userPracticeService;
        }

        /// <summary>
        /// 获取所有案例资源
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
       [HttpGet]
        public async Task<ResponseContext<List<AllCaseDto>>> GetAllCaseList([FromQuery] PraticeCaseQueryDto queryDto)
        {
            return await service.GetAllCaseList(queryDto.Userid, queryDto);
        }

        /// <summary>
        /// 自测随机案例数据
        /// </summary>
        /// <param name="cf"></param>
        /// <param name="cfl"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<RandomCaseDto>>> GetRandomCaseList(Certificate cf, CertificateLevel cfl)
        {
            return await service.GetRandomCaseList(cf, cfl);
        }

        /// <summary>
        /// 查询案例列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<PraticeCaseListResponseDto>>> GetCaseListPageByCertificate([FromQuery] PraticeCaseQueryRequestDto queryDto)
        {
            var result = new ResponseContext<List<PraticeCaseListResponseDto>>();
            if (queryDto.Certificate <= 0)
            {
                result.Msg = "Certificate无效。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }
            return await service.GetCaseListPageByCertificate(queryDto);
        }
        /// <summary>
        /// 新增通关记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddGradeRecord(GradeRecordRequestDto model)
        {
            return await service.AddGradeRecord(model);
        }
        /// <summary>
        /// 通关记录查询
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="caseId">自主训练是根据userid+caseid查询</param>
        /// <param name="certificate">考前自测是根据userid+certificate查询</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<GradeRecordRequestDto>>> GetGradeRecordList(long userId, long caseId, Certificate certificate, int examType)
        {
            return await service.GetGradeRecordList(userId, caseId, certificate, examType);
        }

        /// <summary>
        /// 获取题目信息
        /// </summary>
        /// <param name="caseSourceSystem"></param>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<QuestionInfoDto>>> GetQuestionInfo(CaseSourceSystem caseSourceSystem, long caseId)
        {
            return await service.GetQuestionInfo(caseSourceSystem, caseId);
        }
    }
}


