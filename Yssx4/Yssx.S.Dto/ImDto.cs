﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Yssx.S.Dto
{
    /// <summary>
    /// Im接口参数
    /// </summary>
    public class ImDto
    {
        /// <summary>
        /// 分组id
        /// </summary>
        public string GroupId { get; set; }

        /// <summary>
        /// 消息内容
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 消息接收人clientId
        /// </summary>
        public Guid ReceiveClientId { get; set; }

        /// <summary>
        /// 发送的消息是否需要回执
        /// </summary>
        public bool IsReceipt { get; set; } = false;

        /// <summary>
        /// 是否保存聊天记录
        /// </summary>
        public bool IsSave { get; set; } = false;

    }

    /// <summary>
    /// 消息查询
    /// </summary>
    public class ImMsgRequest
    {
        /// <summary>
        /// 分组id
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// 开始索引
        /// </summary>
        public long Start { get; set; } = 0;

        /// <summary>
        /// 消息数量
        /// </summary>
        public long Lenth { get; set; } = 20;
    }
}
