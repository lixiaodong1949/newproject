﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 移动端首页dto
    /// </summary>
    public class McHomePageDto
    {
        /// <summary>
        /// banner图信息
        /// </summary>
        public List<BannerInfo> BannerInfos { get; set; }
        /// <summary>
        /// 课程信息
        /// </summary>
        public List<CourseInfo> CourseInfos { get; set; }
        /// <summary>
        /// 课件信息
        /// </summary>
        public List<CourseFilesInfo> CourseFilesInfos { get; set; }
        /// <summary>
        /// 业务场景信息
        /// </summary>
        public List<BusinessSceneInfo> BusinessSceneInfos { get; set; }
    }
    public class BannerInfo
    {
        /// <summary>
        /// 主键
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 跳转url
        /// </summary>
        public string BannerUrl { get; set; }
        /// <summary>
        /// 图片地址
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        public long ProductId { get; set; }
    }
    public class CourseInfo
    {
        public long Id { get; set; }
        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; set; }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string Intro { get; set; }
    }
    /// <summary>
    /// 课件信息
    /// </summary>
    public class CourseFilesInfo
    {
        public long Id { get; set; }
        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 文件类型0:Excel,1:Word,2:PDF,3:PPT,4:Img,5:ZIP
        /// </summary>
        public int FilesType { get; set; }
        ///// <summary>
        ///// 所属课程Id
        ///// </summary>
        //public string CourseId { get; set; }

        ///// <summary>
        ///// 所属课程名称
        ///// </summary>
        //public string CourseName { get; set; }

        /// <summary>
        /// 知识点id集合
        /// </summary>
        public string KnowledgePointId { get; set; }
        /// <summary>
        /// 知识点
        /// </summary>
        public string KnowledgePoints { get; set; }
    }
    /// <summary>
    /// 业务场景实训信息
    /// </summary>
    public class BusinessSceneInfo
    {
        /// <summary>
        /// 图片路径
        /// </summary>
        public string FileUrl { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
