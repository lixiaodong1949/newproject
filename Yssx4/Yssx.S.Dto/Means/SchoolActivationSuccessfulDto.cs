﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Means
{
    /// <summary>
    /// 学校激活成功dto
    /// </summary>
    public class SchoolActivationSuccessfulDto
    {
        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }
        /// <summary>
        /// 截至日期
        /// </summary>
        public DateTime Deadline { get; set; }
        /// <summary>
        /// 剩余天数
        /// </summary>
        public int RemainingDays { get; set; }
        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }
    }
}
