﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Means
{
    /// <summary>
    /// 修改密码dto
    /// </summary>
    public class UpdateUserPasswordDto
    {
        /// <summary>
        /// 原密码
        /// </summary>
        public string OriginalPassword { get; set; }
        /// <summary>
        /// 新密码
        /// </summary>
        public string NewPassword { get; set; }
        /// <summary>
        /// 再次密码
        /// </summary>
        public string AgainNewPassword { get; set; }
    }
}
