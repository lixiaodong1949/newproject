﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Means
{
    /// <summary>
    /// 验证角色激活码dto
    /// </summary>
    public class VerifyRoleActivationCodeDto
    {
        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }
        /// <summary>
        /// 角色名称
        /// </summary>
        public string RoleName { get; set; }
        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }
        /// <summary>
        /// 类型Id(0管理员,1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户)
        /// </summary>
        public int TypeId { get; set; }
        /// <summary>
        /// 激活码Id
        /// </summary>
        public long ActivationCodeId { get; set; }
    }
}
