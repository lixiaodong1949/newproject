﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 激活信息
    /// </summary>
    public class ActivationInfoRequest
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 工号或者学号
        /// </summary>
        public string WorkNumber { get; set; }
        /// <summary>
        /// 院系
        /// </summary>
        public long CollegeId { get; set; }
        /// <summary>
        /// 专业
        /// </summary>
        public long ProfessionId { get; set; }
        /// <summary>
        /// 学生班级
        /// </summary>
        public long? ClassId { get; set; }
        /// <summary>
        /// 教师绑定班级列表
        /// </summary>
        public List<long> TeacherClassId { get; set; }
        /// <summary>
        /// 类型(用户类型0管理员,1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户)
        /// </summary>
        public int TypeId { get; set; }
        /// <summary>
        /// 类型名称
        /// </summary>
        public string TypeName { get; set; }


    }
}
