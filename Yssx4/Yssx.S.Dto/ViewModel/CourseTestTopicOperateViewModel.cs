﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课堂测验题目操作状态返回实体
    /// </summary>
    public class CourseTestTopicOperateViewModel
    {
        /// <summary>
        /// 题目操作状态 0:未测验 1:测验进行中 2:测验完成
        /// </summary>
        public CourseTestTopicOperateStatus TopicStatus { get; set; }

        /// <summary>
        /// 课堂测验任务Id
        /// </summary>
        public long TestTaskId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务来源 0:课程 1:案例
        /// </summary>
        public CourseTestTaskSource TaskSource { get; set; }

        /// <summary>
        /// 课堂测验试卷Id
        /// </summary>
        public long ExamId { get; set; }
    }
}
