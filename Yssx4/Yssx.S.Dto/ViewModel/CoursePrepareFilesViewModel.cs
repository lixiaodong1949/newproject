﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课文件返回实体
    /// </summary>
    public class CoursePrepareFilesViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 课件库Id
        /// </summary>
        public long CourseFileId { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件地址
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public FilesType FilesType { get; set; }

        /// <summary>
        /// 资源类型 0:课件 1:教案 2:视频 3:思政案例 4:个人素材
        /// </summary>
        public CoursePrepareResourceType ResourceType { get; set; }

        /// <summary>
        /// 资源归属 0:课前预习 1:课中上课
        /// </summary>
        public CoursePrepareResourcesBelong ResourceBelong { get; set; }

        /// <summary>
        /// 是否是附件(区分思政案例超链接)
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointIds { get; set; }

        /// <summary>
        /// 知识点名称
        /// </summary>
        public string KnowledgePointNames { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
