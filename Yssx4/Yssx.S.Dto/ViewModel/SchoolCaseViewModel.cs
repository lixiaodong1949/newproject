﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取学校企业(案例)实体
    /// </summary>
    public class SchoolCaseViewModel
    {
        /// <summary>
        /// 企业Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary> 
        public string CaseName { get; set; }
    }
}
