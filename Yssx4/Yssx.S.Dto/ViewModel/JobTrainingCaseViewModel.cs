﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 岗位实训任务套题返回实体
    /// </summary>
    public class JobTrainingCaseViewModel
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long JobTrainingTaskId { get; set; }

        /// <summary>
        /// 岗位实训Id
        /// </summary>
        public long JobTrainingId { get; set; }

        /// <summary>
        /// 岗位实训名称
        /// </summary>
        public string JobTrainingName { get; set; }

        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public Nullable<long> ExamId { get; set; }

        /// <summary>
        /// 学生数量
        /// </summary>
        public int StudentCount { get; set; }

        /// <summary>
        /// 已交学生数量
        /// </summary>
        public int SubmittedCount { get; set; }

        /// <summary>
        /// 试卷状态 0:未交卷 1:已交卷
        /// </summary>
        public int ExamStatus { get; set; }
    }
}
