﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取备课课程章节教材实体
    /// </summary>
    public class YssxPrepareSectionTextBookViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 教材描述
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 知识点
        /// </summary>
        public string KnowledgePointId { get; set; }
    }
}
