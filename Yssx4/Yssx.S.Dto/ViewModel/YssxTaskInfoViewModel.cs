﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 任务信息(TempViewModel)
    /// </summary>
    public class YssxTaskInfoViewModel
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        public int TaskType { get; set; }

        /// <summary>
        /// 任务开始时间
        /// </summary>
        public Nullable<DateTime> TaskStartTime { get; set; }

        /// <summary>
        /// 任务结束时间
        /// </summary>
        public Nullable<DateTime> TaskEndTime { get; set; }

        /// <summary>
        /// 任务状态 0:未开始 1:已开始 2:已结束
        /// </summary>
        public int TaskStatus { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 总题数
        /// </summary>
        public int TotalQuestionCount { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public Nullable<DateTime> UpdateTime { get; set; }

        /// <summary>
        /// 课程ID(课堂测验、课程任务)
        /// </summary>
        public long CourseId { get; set; }

    }
}
