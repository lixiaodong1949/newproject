﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取教师班级关系实体
    /// </summary>
    public class YssxTeacherClassRelationViewModel
    {
        /// <summary>
        /// 教师班级关系表主键Id
        /// </summary>
        public Nullable<long> TeacherClassId { get; set; }

        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 班级下学生个数
        /// </summary>
        public int StudentCount { get; set; }

        /// <summary>
        /// 入学年份
        /// </summary>
        public int SchoolYear { get; set; }

    }
}
