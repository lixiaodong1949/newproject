﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取场景实训实体(单独购买)
    /// </summary>
    public class YssxSceneTrainingViewModel
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long SceneTrainingId { get; set; }

        /// <summary>
        /// 场景实训名称
        /// </summary>
        public string SceneTrainingName { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 业务场景名称
        /// </summary>
        public string BusinessSceneName { get; set; }

        /// <summary>
        /// 流程步骤
        /// </summary>
        public int ProcessStepsCount { get; set; }

        /// <summary>
        /// 题目数量
        /// </summary>
        public int TopicCount { get; set; }
            
        /// <summary>
        /// 使用次数
        /// </summary>
        public int UseNumber { get; set; }
        
        /// <summary>
        /// 更新时间
        /// </summary>
        public Nullable<DateTime> UpdateTime { get; set; }
    }
}
