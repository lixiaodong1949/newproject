﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学生不懂详情返回实体
    /// </summary>
    public class StudentDoubtDetailViewModel
    {
        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public FilesType FilesType { get; set; }

        /// <summary>
        /// 资源类型 0:课件 1:教案 2:视频 3:思政案例 4:个人素材
        /// </summary>
        public CoursePrepareResourceType ResourceType { get; set; }

        /// <summary>
        /// 学生数量
        /// </summary>
        public int StudentNumber { get; set; }

        /// <summary>
        /// 学生留言列表
        /// </summary>
        public List<StudentLeaveWordDetailViewModel> LeaveWordList = new List<StudentLeaveWordDetailViewModel>();
    }

    /// <summary>
    /// 学生留言详情实体
    /// </summary>
    public class StudentLeaveWordDetailViewModel
    {
        /// <summary>
        /// 学生名称
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// 疑问(不懂)描述
        /// </summary>
        public string QuestionDescribe { get; set; }

        /// <summary>
        /// 留言展示 (名称+描述)
        /// </summary>        
        public string QuestionDisplay
        {
            get
            {
                if (StudentName != null && QuestionDescribe != null)
                    return string.Format("{0}:{1}", StudentName, QuestionDescribe);
                return null;
            }
        }

        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime PublishDate { get; set; }
    }
}
