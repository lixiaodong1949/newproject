﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课题目返回实体
    /// </summary>
    public class CoursePrepareTopicViewModel
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目名称
        /// </summary>
        public string QuestionName { get; set; }

        /// <summary>
        /// 题目内容
        /// </summary>
        public string QuestionContent { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 题目类型名称
        /// </summary>
        public string QuestionTypeName
        {
            get { return QuestionType.GetDescription(); }
            set { }
        }

        /// <summary>
        /// 题目知识点id（多个id用,隔开）
        /// </summary>
        public string CenterKnowledgePointIds { get; set; }

        /// <summary>
        /// 知识点名称集合
        /// </summary>
        public string CenterKnowledgePointNames { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>
        public int DifficultyLevel { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }
    }
}
