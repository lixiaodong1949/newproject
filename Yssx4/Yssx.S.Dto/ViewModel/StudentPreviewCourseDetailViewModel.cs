﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取学生查看预习详情返回实体
    /// </summary>
    public class StudentPreviewCourseDetailViewModel
    {
        /// <summary>
        /// 学生名称
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 查看预习记录Id
        /// </summary>
        public long ViewRecordId { get; set; }

        /// <summary>
        /// 是否已查看预习 false:未查看 true:已查看
        /// </summary>        
        public bool IsViewCouPreview
        {
            get
            {
                if (ViewRecordId > 0)
                    return true;
                return false;
            }
        }
    }
}
