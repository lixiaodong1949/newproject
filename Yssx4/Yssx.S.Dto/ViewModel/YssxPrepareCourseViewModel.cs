﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取备课课程实体
    /// </summary>
    public class YssxPrepareCourseViewModel
    {
        /// <summary>
        /// 备课课程Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 课程类型0:常规课
        /// </summary>
        public int CourseType { get; set; }

        /// <summary>
        /// 特点副标
        /// </summary>
        public string CourseTitle { get; set; }

        /// <summary>
        /// 课程类型
        /// </summary>
        public long CourseTypeId { get; set; }

        /// <summary>
        /// 知识点
        /// </summary>
        public long KnowledgePointId { get; set; }

        /// <summary>
        /// 学历标识
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 出版社
        /// </summary>
        public string PublishingHouse { get; set; }

        /// <summary>
        /// 出版时间
        /// </summary>
        public string PublishingDate { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string Intro { get; set; }

        /// <summary>
        /// 描述备注
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadCount { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateByName { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public string UpdateTime { get; set; }

        /// <summary>
        /// 课程有效期(赠送课程有有效期,选中备课后同有有效期)
        /// </summary>
        public Nullable<DateTime> EffectiveDate { get; set; }

        /// <summary>
        /// 学期主键Id
        /// </summary>
        public Nullable<long> SemesterId { get; set; }

        /// <summary>
        /// 学期名称
        /// </summary>
        public string SemesterName { get; set; }

        /// <summary>
        /// 课程汇总信息
        /// </summary>
        public List<YssxSectionSummaryDto> SummaryListViewModel { get; set; } = new List<YssxSectionSummaryDto>();

        /// <summary>
        /// 课程绑定班级列表
        /// </summary>
        public List<YssxTeacherClassViewModel> CourseClassList = new List<YssxTeacherClassViewModel>();

        /// <summary>
        /// 任务Id
        /// </summary>
        public Nullable<long> TaskId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public Nullable<long> ExamId { get; set; }

        /// <summary>
        /// 原始课程Id
        /// </summary>
        public Nullable<long> OriginalCourseId { get; set; }
    }
}
