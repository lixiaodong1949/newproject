﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 用户自主训练实体
    /// </summary>
    public class YssxUserAutoTrainingViewModel
    {
        /// <summary>
        /// 实操个数
        /// </summary>
        public int OperateCount { get; set; }

        /// <summary>
        /// 实训列表
        /// </summary>
        public List<YssxUserTrainingViewModel> TrainingList { get; set; } = new List<YssxUserTrainingViewModel>();

    }
}
