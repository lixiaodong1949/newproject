﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师是否可以发布任务实体
    /// </summary>
    public class TeacherIsCanPublishTask
    {
        /// <summary>
        /// 是否有课程 false:无 true:有
        /// </summary>
        public bool IsHasCourse { get; set; }

        /// <summary>
        /// 是否有绑定班级 false:无 true:有
        /// </summary>
        public bool IsHasClass { get; set; }
    }
}
