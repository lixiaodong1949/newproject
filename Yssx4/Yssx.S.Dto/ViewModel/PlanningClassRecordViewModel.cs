﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课课堂记录实体
    /// </summary>
    public class PlanningClassRecordViewModel
    {
        /// <summary>
        /// 上课记录Id
        /// </summary>
        public long AttendClassRecordId { get; set; }

        /// <summary>
        /// 上课课程名称
        /// </summary> 
        public string CourseName { get; set; }

        /// <summary>
        /// 授课教师名称
        /// </summary> 
        public string TeacherName { get; set; }

        /// <summary>
        /// 上课时间
        /// </summary>
        public DateTime AttendClassTime { get; set; }

        /// <summary>
        /// 学期名称
        /// </summary>
        public string SemesterName { get; set; }

        /// <summary>
        /// 上课课堂班级列表
        /// </summary>
        public List<PlanningClassRecordClassViewModel> RecordClassList { get; set; } = new List<PlanningClassRecordClassViewModel>();
    }

    /// <summary>
    /// 备课课堂记录班级实体
    /// </summary>
    public class PlanningClassRecordClassViewModel
    {
        /// <summary>
        /// 上课记录Id
        /// </summary>
        public long AttendClassRecordId { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }

}
