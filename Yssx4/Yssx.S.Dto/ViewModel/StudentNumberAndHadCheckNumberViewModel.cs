﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取学生总数及查看预习数量返回实体
    /// </summary>
    public class StudentNumberAndHadCheckNumberViewModel
    {
        /// <summary>
        /// 学生总数
        /// </summary>
        public int StudentNumber { get; set; }

        /// <summary>
        /// 已查看数量
        /// </summary>
        public int HadCheckNumber { get; set; }

        /// <summary>
        /// 未查看数量
        /// </summary>
        public int NotCheckNumber 
        {
            get
            {
                return StudentNumber - HadCheckNumber;
            }
        }
    }
}
