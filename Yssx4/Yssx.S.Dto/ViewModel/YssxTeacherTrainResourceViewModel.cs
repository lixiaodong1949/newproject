﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师实训资源详情
    /// </summary>
    public class YssxTeacherTrainResourceViewModel
    {
        /// <summary>
        /// 实训类型 0:财务大数据实训 1:业税财一体实训 2:会计岗位实训 3:出纳岗位实训 4:业务情景实训
        /// </summary>
        public int TrainType { get; set; }

        /// <summary>
        /// 资源总计数量
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 已获资源数量
        /// </summary>
        public int HasWonCount { get; set; }

    }
}
