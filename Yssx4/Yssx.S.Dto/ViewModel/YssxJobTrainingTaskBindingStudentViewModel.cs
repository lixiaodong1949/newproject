﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取岗位实训任务绑定班级的学生列表
    /// </summary>
    public class YssxJobTrainingTaskBindingStudentViewModel
    {
        /// <summary>
        /// 任务状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        public Nullable<int> TaskStatus { get; set; }

        /// <summary>
        /// 学生列表
        /// </summary>
        public List<YssxJobTrainingTaskBindingStudentList> StudentList = new List<YssxJobTrainingTaskBindingStudentList>();
    }

    /// <summary>
    /// 学生列表
    /// </summary>
    public class YssxJobTrainingTaskBindingStudentList
    {
        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary> 
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary> 
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary> 
        public string ClassName { get; set; }

        /// <summary>
        /// 学生姓名
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 学生交卷状态 0:未交卷 1:已交卷
        /// </summary>
        public int StudentTaskStatus { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long StudentId { get; set; }

    }

}
