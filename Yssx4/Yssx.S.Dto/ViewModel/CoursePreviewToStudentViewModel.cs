﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学生预习返回实体
    /// </summary>
    public class CoursePreviewToStudentViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 课程预习名称
        /// </summary>
        public string CoursePreviewName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 查看预习记录Id
        /// </summary>
        public long ViewRecordId { get; set; }

        /// <summary>
        /// 是否已查看预习 false:未查看 true:已查看
        /// </summary>        
        public bool IsViewCouPreview
        {
            get
            {
                if (ViewRecordId > 0)
                    return true;
                return false;
            }
        }

        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime PublishDate { get; set; }
    }
}
