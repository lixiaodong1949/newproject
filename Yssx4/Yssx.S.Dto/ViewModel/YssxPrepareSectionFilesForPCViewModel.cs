﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取备课课程课件、视频、教案、授课计划实体
    /// </summary>
    public class YssxPrepareSectionFilesForPCViewModel
    {
        /// <summary>
        /// 课程资源
        /// </summary>
        public List<YssxPrepareSectionFilesViewModel> CourseFilesList { get; set; } = new List<YssxPrepareSectionFilesViewModel>();

        /// <summary>
        /// 章节资源
        /// </summary>
        public List<YssxPrepareSectionFilesViewModel> SectionFilesList { get; set; } = new List<YssxPrepareSectionFilesViewModel>();
    }
}
