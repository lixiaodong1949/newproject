﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取岗位实体
    /// </summary>
    public class YssxPositionViewModel
    {
        /// <summary>
        /// 岗位Id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PositionName { get; set; }
    }
}
