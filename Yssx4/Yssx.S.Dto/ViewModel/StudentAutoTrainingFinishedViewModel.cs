﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取学生自主实训已完成实体
    /// </summary>
    public class StudentAutoTrainingFinishedViewModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 学生名称
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary> 
        public string ClassName { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary> 
        public string CaseName { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 作答总用时
        /// </summary>
        public double UsedSeconds { get; set; }

        /// <summary>
        /// 作答总用时-描述
        /// </summary>
        public string UsedSecondsDesc
        {
            get
            {
                long rHour = (long)UsedSeconds / 3600;
                long rMin = (long)UsedSeconds % 3600 / 60;
                long rSec = (long)UsedSeconds % 3600 % 60;
                return string.Format("{0}小时{1}分钟{2}秒", rHour, rMin, rSec);
            }
        }

        /// <summary>
        /// 交卷时间
        /// </summary>
        public Nullable<DateTime> SubmitTime { get; set; }

        /// <summary>
        /// 企业Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public Nullable<int> RollUpType { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public Nullable<long> ExamId { get; set; }

        /// <summary>
        /// 作答记录Id
        /// </summary>
        public Nullable<long> GradeId { get; set; }
    }
}
