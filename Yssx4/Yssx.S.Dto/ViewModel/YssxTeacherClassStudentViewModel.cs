﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取教师绑定班级下学生实体
    /// </summary>
    public class YssxTeacherClassStudentViewModel
    {
        /// <summary>
        /// 学生Id
        /// </summary>
        public long StudentUserId { get; set; }

        /// <summary>
        /// 学生姓名
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string WorkNumber { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 进班时间
        /// </summary>
        public DateTime InClassTime { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary> 
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary> 
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary> 
        public string ClassName { get; set; }
    }
}
