﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课选中案例题目Ids返回实体
    /// </summary>
    public class CoursePrepareSelectCaseTopicIdsViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }
    }
}
