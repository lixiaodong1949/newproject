﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取学生岗位实训任务实体
    /// </summary>
    public class YssxJobTrainingTaskStudentViewModel
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务类型 0:作业 1:考试
        /// </summary>
        public Nullable<int> ExamType { get; set; }

        /// <summary>
        /// 训练开始时间
        /// </summary>
        public Nullable<DateTime> TrainStartTime { get; set; }

        /// <summary>
        /// 训练结束时间
        /// </summary>
        public Nullable<DateTime> TrainEndTime { get; set; }

        /// <summary>
        /// 任务密码
        /// </summary>
        public string TaskPassword { get; set; }

        /// <summary>
        /// 实训类型 0:综合岗位实训 1:分岗位实训
        /// </summary>
        public Nullable<int> TrainType { get; set; }

        /// <summary>
        /// 分岗位实训组数
        /// </summary>
        public Nullable<int> GroupCount { get; set; }

        /// <summary>
        /// 答案与解析显示类型 0:完成后显示 1:答题中显示
        /// </summary>
        public Nullable<int> AnswerShowType { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// (废弃)
        /// </summary>
        [Obsolete]
        public Nullable<int> TaskStatus { get; set; }

        /// <summary>
        /// 任务状态 0:未开始 1:进行中 2:已结束
        /// </summary>        
        public int TaskFinalStatus
        {
            get
            {
                if (TaskStatus.HasValue && TaskStatus == (int)ExamStatus.End)
                    return (int)ExamStatus.End;
                if (DateTime.Now < TrainStartTime)
                    return (int)ExamStatus.Wait;
                if (TrainStartTime <= DateTime.Now && (!TrainEndTime.HasValue || DateTime.Now < TrainEndTime))
                    return (int)ExamStatus.Started;
                if (TrainStartTime <= DateTime.Now && (TrainEndTime.HasValue && TrainEndTime <= DateTime.Now))
                    return (int)ExamStatus.End;
                return (int)ExamStatus.Wait;
            }
        }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamPaperScore { get; set; }

        /// <summary>
        /// 实训套题列表(岗位实训)
        /// </summary>
        public List<YssxJobTrainingViewModel> TopicList { get; set; } = new List<YssxJobTrainingViewModel>();

        /// <summary>
        /// 所有案例都已交卷 false:否 true:是
        /// </summary>
        public bool IsAllSubmit { get; set; }

        /// <summary>
        /// 任务时长
        /// </summary>
        public string  TaskBurningTime { get; set; }

        /// <summary>
        /// 结束后是否可以查看试卷 false:不可以查看  true:可以查看 【默认为true】
        /// </summary>
        public bool IsCanCheckExam { get; set; }
    }
}
