﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导出学生自主实训已完成实体
    /// </summary>
    public class ExportStudentAutoTrainingFinishedViewModel
    {
        /// <summary>
        /// 学生名称
        /// </summary>
        [Description("姓名")]
        public string StudentName { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary> 
        [Description("班级")]
        public string ClassName { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary> 
        [Description("企业")]
        public string CaseName { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        [Description("分数")]
        public decimal Score { get; set; }

        /// <summary>
        /// 作答总用时-描述
        /// </summary>
        [Description("用时")]
        public string UsedSecondsDesc { get; set; }

        /// <summary>
        /// 交卷时间
        /// </summary>
        [Description("交卷时间")]
        public DateTime SubmitTime { get; set; }
    }
}
