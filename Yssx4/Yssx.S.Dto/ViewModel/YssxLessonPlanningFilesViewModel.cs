﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取课程备课素材、教案
    /// </summary>
    public class YssxLessonPlanningFilesViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long LessonPlanId { get; set; }

        /// <summary>
        /// 文件库Id
        /// </summary>
        public long CourseFileId { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public int FilesType { get; set; }

        /// <summary>
        /// 2:教案 5:素材(课程备课)
        /// </summary>
        public int SectionType { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }

    }
}
