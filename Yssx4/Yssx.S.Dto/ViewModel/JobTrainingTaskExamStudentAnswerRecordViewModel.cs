﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取岗位实训任务试卷学生作答记录
    /// </summary>
    public class JobTrainingTaskExamStudentAnswerRecordViewModel
    {
        /// <summary>
        /// 任务开始时间
        /// </summary>
        public Nullable<DateTime> TrainStartTime { get; set; }

        /// <summary>
        /// 任务结束时间
        /// </summary>
        public Nullable<DateTime> TrainEndTime { get; set; }

        /// <summary>
        /// (废弃)
        /// </summary>
        [Obsolete]
        public Nullable<int> TaskStatus { get; set; }

        /// <summary>
        /// 任务状态 0:未开始 1:进行中 2:已结束
        /// </summary>        
        public int TaskFinalStatus
        {
            get
            {
                if (TaskStatus.HasValue && TaskStatus == (int)ExamStatus.End)
                    return (int)ExamStatus.End;
                if (DateTime.Now < TrainStartTime)
                    return (int)ExamStatus.Wait;
                if (TrainStartTime <= DateTime.Now && (!TrainEndTime.HasValue || DateTime.Now < TrainEndTime))
                    return (int)ExamStatus.Started;
                if (TrainStartTime <= DateTime.Now && (TrainEndTime.HasValue && TrainEndTime <= DateTime.Now))
                    return (int)ExamStatus.End;
                return (int)ExamStatus.Wait;
            }
        }
        
        /// <summary>
        /// 学生数量
        /// </summary>
        public int StudentCount { get; set; }

        /// <summary>
        /// 已交学生数量
        /// </summary>
        public int SubmittedCount { get; set; }

        /// <summary>
        /// 未交学生数量(学生数量-已交学生数量)
        /// </summary>
        public int UnSubmitCount { get { return (StudentCount - SubmittedCount); } }

        /// <summary>
        /// 总题数
        /// </summary>
        public int TotalQuestionCount { get; set; }

        /// <summary>
        /// 学生作答记录
        /// </summary>
        public List<JobTrainingTaskStudentAnswerRecordViewModel> AnswerRecordList = new List<JobTrainingTaskStudentAnswerRecordViewModel>();
    }
}
