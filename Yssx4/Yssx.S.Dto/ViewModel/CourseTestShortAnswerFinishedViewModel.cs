﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 已结束课程测验简答题详情返回实体
    /// </summary>
    public class CourseTestShortAnswerFinishedViewModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 学生名称
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 简答题(案例分析题)作答详情
        /// </summary>
        public string ShortAnswerValue { get; set; }

        /// <summary>
        /// 是否有作答 false:未作答 true:已作答
        /// </summary>
        public bool IsHasAnswer { get; set; }

        /// <summary>
        /// 默认false 前端逻辑
        /// </summary>
        public bool IsShow{ get; set; }
}
}
