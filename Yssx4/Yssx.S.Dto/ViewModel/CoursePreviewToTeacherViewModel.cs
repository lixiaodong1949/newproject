﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师预习返回实体
    /// </summary>
    public class CoursePreviewToTeacherViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 课程预习名称
        /// </summary>
        public string CoursePreviewName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime PublishDate { get; set; }

        /// <summary>
        /// 预习班级列表
        /// </summary>
        public List<CoursePreviewClassViewModel> ClassList = new List<CoursePreviewClassViewModel>();
    }

    /// <summary>
    /// 预习班级列表
    /// </summary>
    public class CoursePreviewClassViewModel
    {
        /// <summary>
        /// 课程预习主键Id
        /// </summary>
        public long CoursePreviewId { get; set; }

        /// <summary>
        /// 班级主键
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }

}
