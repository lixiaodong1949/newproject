﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 用户实训列表
    /// </summary>
    public class YssxUserTrainingViewModel
    {
        /// <summary>
        /// 实训Id
        /// </summary>
        public long TrainingId { get; set; }

        /// <summary>
        /// 实训名称
        /// </summary>
        public string TrainingName { get; set; }

        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 实训分类 0:会计实训 1:出纳实训 2:大数据 3:业财税 4:业务情景实训 5:课程实训
        /// </summary>
        public int SxType { get; set; }

        /// <summary>
        /// 公司Id
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// 行业/公司/学历
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public Nullable<long> GradeId { get; set; }

    }
}
