﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取岗位实训实体
    /// </summary>
    public class YssxJobTrainingViewModel
    {
        /// <summary>
        /// 岗位实训Id
        /// </summary>
        public long JobTrainingId { get; set; }

        /// <summary>
        /// 岗位实训名称
        /// </summary>
        public string JobTrainingName { get; set; }

        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 分数(需统计)得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamScore { get; set; }

        /// <summary>
        /// 所属行业Id
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// 所属行业名称
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 岗位列表
        /// </summary>
        public List<YssxPositionViewModel> PositionList { get; set; } = new List<YssxPositionViewModel>();

        /// <summary>
        /// 试卷Id
        /// </summary>
        public Nullable<long> ExamId { get; set; }

        /// <summary>
        /// 试卷状态 0:未交卷 1:已交卷
        /// </summary>
        public int ExamStatus { get; set; }

        /// <summary>
        /// 作答记录Id
        /// </summary>
        public Nullable<long> GradeId { get; set; }

        /// <summary>
        /// 学生作答状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        public Nullable<int> StudentExamStatus { get; set; }

    }
}
