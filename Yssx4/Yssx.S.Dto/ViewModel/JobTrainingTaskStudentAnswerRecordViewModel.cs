﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取岗位实训任务学生作答记录
    /// </summary>
    public class JobTrainingTaskStudentAnswerRecordViewModel
    {
        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 名称
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 作答题数
        /// </summary>
        public int AnswerCount { get; set; }

        /// <summary>
        /// 正确题数
        /// </summary>
        public int CorrectCount { get; set; }

        /// <summary>
        /// 错误题数
        /// </summary>
        public int ErrorCount { get; set; }

        /// <summary>
        /// 部分对题数
        /// </summary>
        public int PartRightCount { get; set; }

        /// <summary>
        /// 未作答题数
        /// </summary>
        public int BlankCount { get; set; }

        /// <summary>
        /// 答对率
        /// </summary>
        public decimal AnswerRate { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 排名
        /// </summary>
        public string Ranking { get; set; }

        /// <summary>
        /// 作答总用时
        /// </summary>
        public double TotalSeconds { get; set; }

        /// <summary>
        /// 交卷时间
        /// </summary>
        public Nullable<DateTime> SubmitTime { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 考试状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        public StudentExamStatus StudentExamStatus { get; set; }
    }
}
