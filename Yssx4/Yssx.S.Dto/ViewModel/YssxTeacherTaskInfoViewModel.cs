﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师任务信息
    /// </summary>
    public class YssxTeacherTaskInfoViewModel
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务类型 0:课堂测验 1:课程任务 2:岗位实训任务
        /// </summary>
        public Nullable<int> TaskType { get; set; }

        /// <summary>
        /// 任务开始时间
        /// </summary>
        public Nullable<DateTime> TaskStartTime { get; set; }

        /// <summary>
        /// 任务结束时间
        /// </summary>
        public Nullable<DateTime> TaskEndTime { get; set; }

        /// <summary>
        /// (废弃)
        /// </summary>
        [Obsolete]
        public Nullable<int> TaskStatus { get; set; }

        /// <summary>
        /// 任务状态 0:未开始 1:进行中 2:已结束
        /// </summary>        
        public int TaskFinalStatus
        {
            get
            {
                if (TaskStatus.HasValue && TaskStatus == (int)ExamStatus.End)
                    return (int)ExamStatus.End;
                if (DateTime.Now < TaskStartTime)
                    return (int)ExamStatus.Wait;
                if (TaskStartTime <= DateTime.Now && (!TaskEndTime.HasValue || DateTime.Now < TaskEndTime))
                    return (int)ExamStatus.Started;
                if (TaskStartTime <= DateTime.Now && (TaskEndTime.HasValue && TaskEndTime <= DateTime.Now))
                    return (int)ExamStatus.End;
                return (int)ExamStatus.Wait;
            }
        }

        /// <summary>
        /// 学生数量
        /// </summary>
        public int StudentCount { get; set; }

        /// <summary>
        /// 任务班级列表
        /// </summary>
        public List<TaskClassViewModel> ClassList { get; set; } = new List<TaskClassViewModel>();
    }

    /// <summary>
    /// 班级实体
    /// </summary>
    public class TaskClassViewModel
    {
        /// <summary>
        /// 班级Id
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }
}
