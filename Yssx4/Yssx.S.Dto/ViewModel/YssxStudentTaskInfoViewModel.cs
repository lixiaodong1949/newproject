﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学生任务信息
    /// </summary>
    public class YssxStudentTaskInfoViewModel
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务类型 0:课堂测验 1:课程任务 2:岗位实训任务
        /// </summary>
        public Nullable<int> TaskType { get; set; }

        /// <summary>
        /// 任务开始时间
        /// </summary>
        public Nullable<DateTime> TaskStartTime { get; set; }

        /// <summary>
        /// 任务结束时间
        /// </summary>
        public Nullable<DateTime> TaskEndTime { get; set; }

        /// <summary>
        /// (废弃)
        /// </summary>
        [Obsolete]
        public Nullable<int> TaskStatus { get; set; }

        /// <summary>
        /// 任务状态 0:未开始 1:进行中 2:已结束
        /// </summary>        
        public int TaskFinalStatus
        {
            get
            {
                if (TaskStatus.HasValue && TaskStatus == (int)ExamStatus.End)
                    return (int)ExamStatus.End;
                if (DateTime.Now < TaskStartTime)
                    return (int)ExamStatus.Wait;
                if (TaskStartTime <= DateTime.Now && (!TaskEndTime.HasValue || DateTime.Now < TaskEndTime))
                    return (int)ExamStatus.Started;
                if (TaskStartTime <= DateTime.Now && (TaskEndTime.HasValue && TaskEndTime <= DateTime.Now))
                    return (int)ExamStatus.End;
                return (int)ExamStatus.Wait;
            }
        }

        /// <summary>
        /// 总题数
        /// </summary>
        public int TotalQuestionCount { get; set; }

        /// <summary>
        /// 已作答题数
        /// </summary>
        public int HasAnsweredCount { get; set; }

        /// <summary>
        /// 课程ID(课堂测验、课程任务)
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 试卷Id(课堂测验、课程任务)
        /// </summary>
        public Nullable<long> ExamId { get; set; }

        /// <summary>
        /// 作答记录Id(课堂测验、课程任务)
        /// </summary>
        public Nullable<long> GradeId { get; set; }

        /// <summary>
        /// 学生作答状态 0或Null:未开始 1:进行中 2:已结束(课堂测验、课程任务)
        /// </summary>
        public Nullable<int> StudentExamStatus { get; set; }

        /// <summary>
        /// 答案与解析显示类型 0:完成后显示 1:答题中显示(岗位实训任务)
        /// </summary>
        public Nullable<int> AnswerShowType { get; set; }

        /// <summary>
        /// 实训套题列表(岗位实训任务)
        /// </summary>
        public List<JobTrainingTaskViewModel> TopicList { get; set; } = new List<JobTrainingTaskViewModel>();

    }

    /// <summary>
    /// 获取岗位实训实体
    /// </summary>
    public class JobTrainingTaskViewModel
    {
        /// <summary>
        /// 岗位实训Id
        /// </summary>
        public long JobTrainingId { get; set; }

        /// <summary>
        /// 岗位实训名称
        /// </summary>
        public string JobTrainingName { get; set; }

        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 试卷Id(岗位实训任务)
        /// </summary>
        public Nullable<long> ExamId { get; set; }

        /// <summary>
        /// 作答记录Id(岗位实训任务)
        /// </summary>
        public Nullable<long> GradeId { get; set; }

        /// <summary>
        /// 学生作答状态 0:未开始 1:进行中 2:已结束(岗位实训任务)
        /// </summary>
        public Nullable<int> StudentExamStatus { get; set; }
    }

}
