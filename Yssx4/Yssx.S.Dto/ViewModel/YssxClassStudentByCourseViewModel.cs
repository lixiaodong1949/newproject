﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取某一个课程绑定班级的学生实体
    /// </summary>
    public class YssxClassStudentByCourseViewModel
    {        
        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary> 
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary> 
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary> 
        public string ClassName { get; set; }

        /// <summary>
        /// 学生列表
        /// </summary>
        public List<StudentData> StudentList { get; set; }
    }

    /// <summary>
    /// 学生信息
    /// </summary>
    public class StudentData
    {
        /// <summary>
        /// 学生Id
        /// </summary>
        public long StudentUserId { get; set; }

        /// <summary>
        /// 学生姓名
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string WorkNumber { get; set; }
        
        /// <summary>
        /// 班级Id(冗余字段,IOS需要)
        /// </summary> 
        public long ClassId { get; set; }

    }
}
