﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取岗位实训实体
    /// </summary>
    public class YssxJobTrainingViewModelForApp
    {
        /// <summary>
        /// 所属行业Id
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// 所属行业名称
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 岗位实训列表
        /// </summary>
        public List<YssxJobTrainingDetaillViewModel> JobTrainingList { get; set; } = new List<YssxJobTrainingDetaillViewModel>();
    }

    /// <summary>
    /// 岗位实训实体
    /// </summary>
    public class YssxJobTrainingDetaillViewModel
    {
        /// <summary>
        /// 岗位实训Id
        /// </summary>
        public long JobTrainingId { get; set; }

        /// <summary>
        /// 岗位实训名称
        /// </summary>
        public string JobTrainingName { get; set; }

        /// <summary>
        /// 分数(需统计)
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 岗位列表
        /// </summary>
        public List<YssxPositionViewModel> PositionList { get; set; } = new List<YssxPositionViewModel>();

        /// <summary>
        /// 试卷Id
        /// </summary>
        public Nullable<long> ExamId { get; set; }

        /// <summary>
        /// 作答记录Id
        /// </summary>
        public Nullable<long> GradeId { get; set; }

    }
}
