﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取学期实体
    /// </summary>
    public class YssxSemesterViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 学期名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
       
        /// <summary>
        /// 是否禁用 0:启用 1:禁用
        /// </summary>
        public int IsDisabled { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<DateTime> CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public Nullable<DateTime> UpdateTime { get; set; }
    }
}
