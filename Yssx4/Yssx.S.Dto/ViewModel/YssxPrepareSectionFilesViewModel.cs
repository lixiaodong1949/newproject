﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取备课课程课件、视频、教案、授课计划实体
    /// </summary>
    public class YssxPrepareSectionFilesViewModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章Id
        /// </summary>
        public Nullable<long> SectionId { get; set; }

        /// <summary>
        /// 课程章名称
        /// </summary>
        public string SectionName { get; set; }

        /// <summary>
        /// 章节标题
        /// </summary>
        public string SectionTitle { get; set; }

        /// <summary>
        /// 文件库Id
        /// </summary>
        public long CourseFileId { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public int FilesType { get; set; }

        /// <summary>
        /// 1:课件 2:教案 3:视频 4:授课计划
        /// </summary>
        public int SectionType { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 课程资源来源 0:购买课程下的资源 1:客户单独购买 2:上传私有
        /// </summary>
        public Nullable<int> DataSourceType { get; set; }

        /// <summary>
        /// 资源有效期
        /// </summary>
        public Nullable<DateTime> EffectiveDate { get; set; }
    }
}
