﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程知识点返回实体
    /// </summary>
    public class CourseKnowledgePointViewModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public Nullable<long> Id { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public long KnowledgePointId { get; set; }

        /// <summary>
        /// 知识点名称
        /// </summary>
        public string KnowledgePointName { get; set; }
    }
}
