using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 修改发货信息 Dto
    /// </summary>
    public class UpdateDeliveryInfoRequestDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Required]
        public long Id { get; set; }
        /// <summary>
        /// 发货状态：0未发货，1已发货 2已收货
        /// </summary>
        public int DeliveryStatus { get; set; }
        /// <summary>
        /// 发货时间
        /// </summary>
        public DateTime DeliveryDate { get; set; }

        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 积分商品来源
        /// </summary>
        public string ProductSource { get; set; }

        /// <summary>
        /// 积分商品链接
        /// </summary>
        public string ProductLink { get; set; }

    }
}