using System;
using System.ComponentModel.DataAnnotations;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 积分商品兑换 Dto
    /// </summary>
    public class IntegralProductExchangeDto
    {
        ///// <summary>
        ///// 唯一Id
        ///// </summary>
        //public long Id { get; set; }
        /// <summary>
        /// 积分商品Id
        /// </summary>
        public long IntegralProductId { get; set; }
        /// <summary>
        /// 用户收货地址Id
        /// </summary>
        public long UserAddressId { get; set; }

        /// <summary>
        /// 用户收货人联系方式
        /// </summary>
        public string UserContact { get; set; }

        /// <summary>
        /// 商品数量
        /// </summary>
        public int ProductNumber { get; set; }

    }
}