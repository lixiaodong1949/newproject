using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 积分商品兑换 返回值Dto
    /// </summary>
    public class IntegralProductExchangePageResponseDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户手机号
        /// </summary>
        public string UserMobile { get; set; }

        /// <summary>
        /// 积分商品Id
        /// </summary>
        public long IntegralProductId { get; set; }

        /// <summary>
        /// 积分商品名称
        /// </summary>
        public string IntegralProductName { get; set; }

        /// <summary>
        /// 兑换积分
        /// </summary>
        public int IntegralNumber { get; set; }

        /// <summary>
        /// 用户收货地址
        /// </summary>
        public string ShippingAddress { get; set; }

        /// <summary>
        /// 用户收货人姓名
        /// </summary>
        public string ShippingName { get; set; }

        /// <summary>
        /// 用户收货人联系方式
        /// </summary>
        public string UserContact { get; set; }

        /// <summary>
        /// 发货状态：0未发货，1已发货 2已收货
        /// </summary>
        public int DeliveryStatus { get; set; } = 0;

        /// <summary>
        /// 发货时间
        /// </summary>
        public DateTime DeliveryDate { get; set; }

        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderCode { get; set; }

        /// <summary>
        /// 积分商品来源
        /// </summary>
        public string ProductSource { get; set; }

        /// <summary>
        /// 积分商品链接
        /// </summary>
        public string ProductLink { get; set; }
        /// <summary>
        /// 兑换时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}