﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.SceneTraining
{
    /// <summary>
    /// 复盘dto
    /// </summary>
    public class CheckingSummaryListDto
    {
        /// <summary>
        /// 部门集合
        /// </summary>
        public List<Xzbm> Xzbm { get; set; }
        /// <summary>
        /// 添加题目集合
        /// </summary>
        public List<Tjtm> Tjtm { get; set; }
        /// <summary>
        /// 作者寄语
        /// </summary>
        public string Zzjy { get; set; }
        /// <summary>
        /// 流程详情集合
        /// </summary>
        public List<ProcessDetails> ProcessDetailsList { get; set; }
    }

    /// <summary>
    /// 选择部门
    /// </summary>
    public class Xzbm {
        /// <summary>
        /// 部门名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 部门Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 详情
        /// </summary>
        public List<Jnzysx> Jnzysx { get; set; }
        /// <summary>
        /// 部门要做的事项集合
        /// </summary>
        //public List<Sx> Sx { get; set; }
        /// <summary>
        /// 部门技能和注意事项集合
        /// </summary>
        //public List<Jnzysx> Jnzysx { get; set; }
        public Xzbm(){
            this.Jnzysx = new List<Jnzysx>();
        }

    }

    /// <summary>
    /// 部门要做得事项
    /// </summary>
    //public class Sx {
    //    /// <summary>
    //    /// 岗位名称
    //    /// </summary>
    //    public string PostName { get; set; }
    //    /// <summary>
    //    /// 选择人员
    //    /// </summary>
    //    public long Xzry { get; set; }
    //    /// <summary>
    //    /// 人员名称
    //    /// </summary>
    //    public string XzryName { get; set; }
    //    /// <summary>
    //    /// 流程名称
    //    /// </summary>
    //    public string Name { get; set; }
    //    /// <summary>
    //    /// 上传资料集合
    //    /// </summary>
    //    public List<Sczl> Sczl { get; set; }
    //}

    /// <summary>
    /// 部门技能和注意事项
    /// </summary>
    public class Jnzysx {


        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostName { get; set; }
        /// <summary>
        /// 选择人员
        /// </summary>
        public long Xzry { get; set; }
        /// <summary>
        /// 人员名称
        /// </summary>
        public string XzryName { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>
        public string Lcname { get; set; }
        /// <summary>
        /// 上传资料集合
        /// </summary>
        public List<Sczl> Sczl { get; set; }

        /// <summary>
        /// 掌握技能和注意事项
        /// </summary>
        public string Jnsx { get; set; }

        /// <summary>
        /// 掌握技能和注意事项
        /// </summary>
        //public string Jnsx { get; set; }
    }
}
