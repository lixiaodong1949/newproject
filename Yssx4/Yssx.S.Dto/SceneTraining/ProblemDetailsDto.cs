﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.SceneTraining
{
    public class ProblemDetailsDto
    {
        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选择公司Id
        /// </summary>
        public long SelectCompanyId { get; set; }
        /// <summary>
        /// 业务场景
        /// </summary>
        public long YwcjId { get; set; }
        /// <summary>
        /// 选择公司名称
        /// </summary>
        public string SelectCompanyName { get; set; }
        /// <summary>
        /// 业务场景
        /// </summary>
        public string YwcjName { get; set; }
        /// <summary>
        /// 流程详情
        /// </summary>
        public List<IdAndNameDetails> ProcessDetailsList { get; set; }
        /// <summary>
        /// 公司背景图
        /// </summary>
        public string BGFileUrl { get; set; }
        /// <summary>
        /// 实训记录表Id
        /// </summary>
        public long ProgressId { get; set; }
    }



    /// <summary>
    /// 流程详情dto
    /// </summary>
    public class IdAndNameDetails
    {
        /// <summary>
        /// 流程名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 流程序号
        /// </summary>
        public int? Lcxh { get; set; }
    }
}
