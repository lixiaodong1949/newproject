﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.SceneTraining
{
    /// <summary>
    /// 下拉数据dto
    /// </summary>
    public class DropDownDataDto
    {
        /// <summary>
        /// id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 图标路径
        /// </summary>
        public string FileUrl { get; set; }
    }
}
