﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.SceneTraining
{
    /// <summary>
    /// 公司下面的场景实训列表dto
    /// </summary>
    public class TrainingServicePreviewListDto
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string SelectCompanyName { get; set; }
        /// <summary>
        /// 场景实训集合
        /// </summary>
        public List<SceneTrainingDto> SceneTrainingListDto { get; set; }
    }

    /// <summary>
    /// 筛选场景实训列dto
    /// </summary>
    public class SceneTrainingDto {
        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 场景实训名称
        /// </summary>
        public string Name { get; set; }
    }
}
