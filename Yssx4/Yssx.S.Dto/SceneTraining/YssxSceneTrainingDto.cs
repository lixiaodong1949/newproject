﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.SceneTraining
{
    public class YssxSceneTrainingDto
    {
        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选择公司Id
        /// </summary>
        public long SelectCompanyId { get; set; }
        /// <summary>
        /// 业务场景
        /// </summary>
        public long YwcjId { get; set; }
        /// <summary>
        /// 排序序号
        /// </summary>
        public long Pxxh { get; set; }
        /// <summary>
        /// 业务往来
        /// </summary>
        public string Ywwl { get; set; }
        /// <summary>
        /// 流程信息
        /// </summary>
        //public long LcxxId { get; set; }
        /// <summary>
        /// 作者寄语
        /// </summary>
        public string Zzjy { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Describe { get; set; }
        /// <summary>
        /// 审核状态(0,待审核，1，审核成功，2，审核拒绝)
        /// </summary>
        public int AuditStatus { get; set; } = 0;

        /// <summary>
        /// 审核说明
        /// </summary>
        public string AuditStatusExplain { get; set; }

        /// <summary>
        /// 是否上架(0,待上架，1，上架待审，2，上架)
        /// </summary>
        public int Uppershelf { get; set; } = 0;

        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string SelectCompanyName { get; set; }

        /// <summary>
        /// 状态(0,待上架,1,上架待审,2,已上架,3,已拒绝)
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 业务场景名称
        /// </summary>
        public string YwcjName { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 使用次数
        /// </summary>
        public int UseNumber { get; set; }
        /// <summary>
        /// 上架人
        /// </summary>
        public string NikeName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
        /// <summary>
        /// 通关次数
        /// </summary>
        public long Tgcs { get; set; }
        /// <summary>
        /// 购买时间
        /// </summary>
        public Nullable<DateTime> PaymentTime { get; set; }
        /// <summary>
        /// 当前进度(0,未开始,1,进行中)
        /// </summary>
        public int Dqjd { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<DateTime> CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 实训记录表Id
        /// </summary>
        public long YssxProgressId { get; set; }
        /// <summary>
        /// 场景实训购买Id
        /// </summary>
        public long SceneTrainingOrderId { get; set; }
        /// <summary>
        /// 当前步数
        /// </summary>
        public int Dqbs { get; set; }
        /// <summary>
        /// 总共步数
        /// </summary>
        public int Zgbs { get; set; }
    }
}
