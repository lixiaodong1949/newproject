﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.SceneTraining
{
    /// <summary>
    /// 添加场景实训
    /// </summary>
    public class AddTrainingServiceDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选择公司Id
        /// </summary>
        public long SelectCompanyId { get; set; }
        /// <summary>
        /// 业务场景
        /// </summary>
        public long Ywcj { get; set; }
        /// <summary>
        /// 排序序号
        /// </summary>
        public long Pxxh { get; set; }
        /// <summary>
        /// 业务往来
        /// </summary>
        public string Ywwl { get; set; }
        /// <summary>
        /// 流程信息
        /// </summary>
        //public long LcxxId { get; set; }
        /// <summary>
        /// 作者寄语
        /// </summary>
        public string Zzjy { get; set; }
        /// <summary>
        /// 流程详情数据集合
        /// </summary>
        public List<ProcessDetailsDto> ProcessDetailsList { get; set; }
        /// <summary>
        /// 0,改状态，1，不改状态
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 删除Id集合
        /// </summary>
        public List<long> DeleteIdList { get; set; }
    }

    /// <summary>
    /// 流程详情
    /// </summary>
    public class ProcessDetailsDto {
        /// <summary>
        /// 流程名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选择部门
        /// </summary>
        public long? Xzbm { get; set; }
        /// <summary>
        /// 选择人员
        /// </summary>
        public long? Xzry { get; set; }
        /// <summary>
        /// 上传资料
        /// </summary>
        public List<Sczl> Sczl { get; set; }
        /// <summary>
        /// 资料图片
        /// </summary>
        public string Zltp { get; set; }
        /// <summary>
        /// 添加题目
        /// </summary>
        public List<Tjtm> TjtmId { get; set; }
        /// <summary>
        /// 掌握技能和注意事项
        /// </summary>
        public string Jnsx { get; set; }
        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long CjsxId { get; set; }
        /// <summary>
        /// 流程序号
        /// </summary>
        public int? Lcxh { get; set; }
        /// <summary>
        /// 是否删除(0，未删除，1，已删除)
        /// </summary>
        public int IsDelete { get; set; }
        /// <summary>
        /// 流程详情Id
        /// </summary>
        public long? Id { get; set; }

    }

    /// <summary>
    /// 上传资料
    /// </summary>
    public class Sczl{
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Url { get; set; }
    }

    /// <summary>
    /// 添加题目
    /// </summary>
    public class Tjtm
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// id
        /// </summary>
        public long Id { get; set; }

        public string questionTypeName { get; set; }
        public int questionType { get; set; }
    }
}
