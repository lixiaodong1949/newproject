﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.S.Pocos.SceneTraining;

namespace Yssx.S.Dto.SceneTraining
{
    /// <summary>
    /// 场景实训-预览
    /// </summary>
    public class TrainingServicePreviewDto
    {
        /// <summary>
        /// 业务场景
        /// </summary>
        public string Ywcj { get; set; }
        /// <summary>
        /// 业务往来
        /// </summary>
        public string Ywwl { get; set; }
        /// <summary>
        /// 业务流程
        /// </summary>
        public List<YssxProcessDetails> ProcessDetailsList { get; set; }
        /// <summary>
        /// 企业简介
        /// </summary>
        public string EnterpriseInfo { get; set; }
        /// <summary>
        /// 公司下部门人员
        /// </summary>
        public List<Bmry> BmryList { get; set; }
        /// <summary>
        /// 公司Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 阅读数量
        /// </summary>
        public int Read { get; set; }
        /// <summary>
        /// 点赞
        /// </summary>
        public int Fabulous { get; set; }
        /// <summary>
        /// 使用次数
        /// </summary>
        public int UseNumber { get; set; }
    }

    /// <summary>
    /// 部门人员
    /// </summary>
    public class Bmry {
        /// <summary>
        /// 部门
        /// </summary>
        public string Bmmc { get; set; }
        /// <summary>
        /// 部门Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 部门下面的人员集合
        /// </summary>
        public List<Ry> Ry { get; set; }
        public Bmry(){
            this.Ry = new List<Ry>();
        }
    }

    /// <summary>
    /// 部门下面的人员
    /// </summary>
    public class Ry {
        /// <summary>       
        /// 岗位名称
        /// </summary>
        public string PostName { get; set; }
        /// <summary>
        /// 人员名称
        /// </summary>
        public string Rymc { get; set; }
    }
}
