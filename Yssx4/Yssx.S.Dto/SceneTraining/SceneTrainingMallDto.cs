﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 场景实训-商城页面--传入参数
    /// </summary>
    public class SceneTrainingMallDto : PageRequest
    {
        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 行业Id
        /// </summary>
        public long? Industry { get; set; }

        /// <summary>
        /// 场景Id
        /// </summary>
        public long? Scene { get; set; }

        /// <summary>
        /// 排序(先用降序) 首页商城专用
        /// null/0(降序/升序)默认创建时间   
        /// 1/11(降序/升序)点赞数   
        /// 2/22(降序/升序)使用次数   
        /// 3/33(降序/升序)更新时间
        /// </summary>
        public int? Sort { get; set; }
    }

    /// <summary>
    /// 点赞--传入参数
    /// </summary>
    public class FabulousDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 点赞(产品说只用1点赞) 0取消点赞 1点赞
        /// </summary>
        public int? Fabulous { get; set; }
    }
}
