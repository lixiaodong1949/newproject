﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.SceneTraining
{
    /// <summary>
    /// 添加实训进度dto
    /// </summary>
    public class AddProgressDto
    {
        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long CjsxId { get; set; }
        /// <summary>
        /// 流程详情Id
        /// </summary>
        public long LcxqId { get; set; }
        /// <summary>
        /// 步数
        /// </summary>
        public int Bs { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>
        public long? TmId { get; set; }
        /// <summary>
        /// 实训进度Id
        /// </summary>
        public long ProgressId { get; set; }
    }
}
