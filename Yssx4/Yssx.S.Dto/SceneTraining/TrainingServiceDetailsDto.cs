﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.SceneTraining
{
    /// <summary>
    /// 场景实训dto
    /// </summary>
    public class TrainingServiceDetailsDto
    {
        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选择公司Id
        /// </summary>
        public long SelectCompanyId { get; set; }
        /// <summary>
        /// 业务场景
        /// </summary>
        public long YwcjId { get; set; }
        /// <summary>
        /// 选择公司名称
        /// </summary>
        public string SelectCompanyName { get; set; }
        /// <summary>
        /// 业务场景
        /// </summary>
        public string YwcjName { get; set; }
        /// <summary>
        /// 排序序号
        /// </summary>
        public long Pxxh { get; set; }
        /// <summary>
        /// 业务往来
        /// </summary>
        public string Ywwl { get; set; }
        /// <summary>
        /// 作者寄语
        /// </summary>
        public string Zzjy { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Describe { get; set; }
        /// <summary>
        /// 流程详情
        /// </summary>
        public List<ProcessDetails> ProcessDetailsList { get; set; }

        public TrainingServiceDetailsDto()
        {
            this.ProcessDetailsList = new List<ProcessDetails>();
        }
    }

    

    /// <summary>
    /// 流程详情dto
    /// </summary>
    public class ProcessDetails {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选择部门
        /// </summary>
        public long Xzbm { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string XzbmName { get; set; }
        /// <summary>
        /// 选择人员
        /// </summary>
        public long Xzry { get; set; }
        /// <summary>
        /// 人员名称
        /// </summary>
        public string XzryName { get; set; }
        /// <summary>
        /// 资料图片
        /// </summary>
        public string Zltp { get; set; }
        /// <summary>
        /// 掌握技能和注意事项
        /// </summary>
        public string Jnsx { get; set; }
        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long CjsxId { get; set; }
        /// <summary>
        /// 上传资料集合
        /// </summary>
        public List<Sczl> Sczl { get; set; }
        /// <summary>
        /// 上传资料
        /// </summary>
        public string Sczls { get; set; }
        /// <summary>
        /// 添加题目集合
        /// </summary>
        public List<Tjtm> TjtmId { get; set; }
        /// <summary>
        /// 添加题目
        /// </summary>
        public string TjtmIds { get; set; }
        /// <summary>
        /// 流程序号
        /// </summary>
        public int Lcxh { get; set; }
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostName { get; set; }
    }
}
