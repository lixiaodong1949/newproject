﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto.SceneTraining
{
    /// <summary>
    /// 场景实训列表
    /// </summary>
    public class TrainingServicePageRequest : PageRequest
    {
        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public long? SelectCompanyId { get; set; }
        /// <summary>
        /// 业务场景
        /// </summary>
        public long? YwcjId { get; set; }
        /// <summary>
        /// 状态(0,待上架,1,上架待审,2,已上架,3,已拒绝,4,全部)
        /// </summary>
        public long? Status { get; set; }
        /// <summary>
        /// 来源(1,云自营,2,其他,3,全部)
        /// </summary>
        public int? LyId { get; set; }
        /// <summary>
        /// 1,资源管理—场景实训列表，2，专业组上传—场景实训列表，3，我的实训-用户实训列表
        /// </summary>
        public int? Type { get; set; }
        /// <summary>
        /// 通过选择(1,已通关,2,未通关,3,全部)
        /// </summary>
        public int Tgxz { get; set; }
    }
}
