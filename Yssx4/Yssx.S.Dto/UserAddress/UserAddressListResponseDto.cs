using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 用户收货地址 返回值Dto
    /// </summary>
    public class UserAddressListResponseDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 收货人姓名
        /// </summary>
        public string ShippingName { get; set; }

        /// <summary>
        /// 是否默认
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        [Required]
        public string ProvinceName { get; set; }
        /// <summary>
        /// 区
        /// </summary>
        [Required]
        public string CityName { get; set; }
        /// <summary>
        /// 区
        /// </summary>
        [Required]
        public string AreaName { get; set; }
        /// <summary>
        /// 省唯一编码
        /// </summary>
        public string ProvinceCode { get; set; }
        /// <summary>
        /// 区唯一编码
        /// </summary>
        public string CityCode { get; set; }
        /// <summary>
        /// 区唯一编码
        /// </summary>
        public string AreaCode { get; set; }

        /// <summary>
        /// 详细地址
        /// </summary>
        [Required]
        public string Details { get; set; }

        /// <summary>
        /// 邮政编码
        /// </summary>
        public string Postcode { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [Required]
        public string Mobile { get; set; }
    }
}