﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取课程课件实体(单独购买、私有上传)
    /// </summary>
    public class YssxCourseFilesViewModelNew
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public long OrderId { get; set; }
        
        /// <summary>
        /// 课件Id
        /// </summary>
        public long CourseFilesId { get; set; }

        /// <summary>
        /// 课件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 知识点组Id
        /// </summary>
        public long KlgpGroupId { get; set; }

        /// <summary>
        /// 创建人姓名
        /// </summary>
        public string CreateByName { get; set; }

        /// <summary>        
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public int FilesType { get; set; }

        /// <summary>
        /// 文件大小
        /// </summary>
        public int FileSize { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadCount { get; set; }

        /// <summary>
        /// 课件创建时间(上传时间)
        /// </summary>
        public Nullable<DateTime> FilesCreateTime { get; set; }

        /// <summary>
        /// 课件更新时间
        /// </summary>
        public Nullable<DateTime> FilesUpdateTime { get; set; }

        /// <summary>
        /// 购买时间
        /// </summary>
        public Nullable<DateTime> PaymentTime { get; set; }

        /// <summary>
        /// 课件来源 0:系统赠送 1:客户创建(购买) 2:上传私有
        /// </summary>
        public Nullable<int> CourseFilesSource { get; set; }

        /// <summary>
        /// 有效期(赠送课件有有效期)
        /// </summary>
        public Nullable<DateTime> EffectiveDate { get; set; }

    }
}
