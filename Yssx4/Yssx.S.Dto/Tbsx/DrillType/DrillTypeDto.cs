﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 实训类型Dto
    /// </summary>
    public class DrillTypeDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; } = Status.Enable;
    }

    /// <summary>
    /// 指导编辑Dto
    /// </summary>
    public class SetGuideInfoDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 流程指导
        /// </summary>
        public string FlowDescription { get; set; }
        /// <summary>
        /// 任务指导
        /// </summary>
        public string TaskDescription { get; set; }
    }
}
