﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取实训类型名称列表 请求dto
    /// </summary>
    public class DrillTypeNameListRequestDto
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>

        public string Name { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [JsonIgnore]
        public Status? Status { get; set; }
    }
}
