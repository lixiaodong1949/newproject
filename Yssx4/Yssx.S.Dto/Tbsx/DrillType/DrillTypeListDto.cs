﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class DrillTypeQueryDto : PageRequest
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>

        public string Name { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [JsonIgnore]
        public Status? Status { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class DrillTypeListDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 题目总分
        /// </summary>
        public decimal TopicTotalSore { get; set; }
    }

}
