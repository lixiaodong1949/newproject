﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询行业入参
    /// </summary>
    public class IndustryRequestNew : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

    }
}
