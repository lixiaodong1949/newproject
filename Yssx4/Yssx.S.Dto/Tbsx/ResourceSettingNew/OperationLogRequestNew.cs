﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 操作日志
    /// </summary>
    public class OperationLogRequestNew : PageRequest
    {
        /// <summary>
        /// 当前模块Id 如案例Id，如场景实训Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 类型 0用户，1专业组
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 模块 1课程，2课件，3题目，4场景实训，5案例，6视频，7岗位实训
        /// </summary>
        public int Module { get; set; }
    }
}
