﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 保存修改岗位管理入参类（查询）输出类
    /// </summary>
    public class PositionDtoNew
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 岗位名称    
        /// </summary>
        public string PositionName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Describe { get; set; }
    }

    /// <summary>
    /// 岗位管理入参类
    /// </summary>
    public class PositionRequestNew : PageRequest
    {
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PositionName { get; set; }
    }
}
