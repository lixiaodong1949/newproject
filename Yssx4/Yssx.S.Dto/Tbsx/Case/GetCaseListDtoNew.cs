﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 公司列表 分页
    /// </summary>
    public class GetCaseListDtoNew
    {
        /// <summary>
        /// 主键，公司Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }
        ///// <summary>
        ///// 公司简称
        ///// </summary>
        //public string BriefName { get; set; }
        /// <summary>
        /// 序号（标号）
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// 公司logo
        /// </summary>
        public string LogoUrl { get; set; }
    }
}
