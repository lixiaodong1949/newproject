﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询已作答题目列表 - 同步实训
    /// </summary>
    public class GetQuestionByGradeDtoNew
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }
    }
}
