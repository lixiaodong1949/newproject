﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 
    /// </summary>
    public class GradeInfoNew
    {
        /// <summary>
        /// 成绩id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 考试总分
        /// </summary>
        public decimal ExamScore { get; set; }

        /// <summary>
        /// 考试分数
        /// </summary>
        public decimal GradeScore { get; set; }

        /// <summary>
        /// 考试用时
        /// </summary>
        public long UsedSeconds { get; set; }

        /// <summary>
        /// 答对题数
        /// </summary>
        public int CorrectCount { get; set; }

        /// <summary>
        /// 答错题数
        /// </summary>
        public int ErrorCount { get; set; }

        /// <summary>
        /// 部分对数量
        /// </summary>
        public int PartRightCount { get; set; }

        /// <summary>
        /// 未答题数
        /// </summary>
        public int BlankCount { get; set; }

        /// <summary>
        /// 正确率
        /// </summary>
        public string CorrectRate
        {
            get
            {
                var total = CorrectCount + ErrorCount;
                if (total == 0)
                    return "--";
                else
                {
                    return CorrectCount * 100 / total + "%";
                }
            }

        }
        /// <summary>
        /// 是否及格
        /// </summary>
        public bool IsPass { get; set; }

        /// <summary>
        /// 考试状态
        /// </summary>
        public GroupStatus GroupStatus { get; set; }

        /// <summary>
        /// 考试状态
        /// </summary>
        public StudentExamStatus StudentExamStatus { get; set; }

        /// <summary>
        /// 等级
        /// </summary>
        public GradeLevel Level
        {
            get
            {
                var rQualifiedScore = ExamScore * (decimal)0.6;
                var rGoodScore = ExamScore * (decimal)0.7;
                var rExcellentScore = ExamScore * (decimal)0.8;
                if (GradeScore >= rExcellentScore)
                    return GradeLevel.Excellent;
                else if (GradeScore >= rGoodScore && GradeScore < rExcellentScore)
                    return GradeLevel.Good;
                else if (GradeScore >= rQualifiedScore && GradeScore < rGoodScore)
                    return GradeLevel.Qualified;
                else
                    return GradeLevel.Unqualified;
            }
        }
    }
}
