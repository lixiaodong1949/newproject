﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 
    /// </summary>
    public class ExamPaperGroupStudentDtoNew
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 分组Id
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// 岗位Id
        /// </summary>
        public long PostionId { get; set; }

        /// <summary>
        /// 成绩主表id，如果为0，表示组团还未成功 
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 组员ID
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// tenantID
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 试卷总时长
        /// </summary>
        public int TotalTime { get; set; }

        /// <summary>
        /// 岗位考试时长
        /// </summary>
        public int TimeMinutes { get; set; }

        /// <summary>
        /// 分组状态(0.已开始，等待成员加入；1.成员已满，等待房间创建人启动开始考试按钮（此时成员人有可能退出房间，创建人也能解散）；2.已完成，开始考试（成员不能再退出）；3.已结束 )
        /// </summary>
        public GroupStatus Status { get; set; }
    }
}
