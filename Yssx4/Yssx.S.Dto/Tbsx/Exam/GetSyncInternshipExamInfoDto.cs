﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取游客模式试卷信息
    /// </summary>
    public class GetSyncInternshipExamInfoDto
    {
        /// <summary>
        /// 是否购买
        /// </summary>
        public bool IsPaid { get; set; } = false;

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 产品Id
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }

    }
}
