﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 印章入参
    /// </summary>
    public class StampListRequestNew : PageRequest
    {
        /// <summary>
        /// 案例ID
        /// </summary>
        public long CaseId { get; set; }
    }
}
