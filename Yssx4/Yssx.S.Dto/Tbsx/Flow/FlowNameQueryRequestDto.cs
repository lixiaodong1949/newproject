﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 流程查询请求Dto
    /// </summary>
    public class FlowNameQueryRequestDto
    {
        /// <summary>
        /// 实训题型Id
        /// </summary>
        public long DrillTypeId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        [JsonIgnore]
        public Status? Status { get; set; }
    }
    /// <summary>
    /// 流程查询返回值Dto
    /// </summary>
    public class FlowNameListDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
