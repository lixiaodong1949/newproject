﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 题目信息
    /// </summary>
    public class TopicInfoDtoNew : BaseQuestionNew
    {
        /// <summary>
        /// 选项
        /// </summary>
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ChoiceOptionNew> Options { get; set; }
        /// <summary>
        /// 多个答案，都好隔开
        /// </summary>
        public override string AnswerValue { get; set; }
        /// <summary>
        /// 题目附件
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<QuestionFileNew> QuestionFile { get; set; }
        /// <summary>
        /// 模板ID
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TemplateId { get; set; }
        /// <summary>
        /// 子题目
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<SubQuestionNew> SubQuestion { get; set; }
        /// <summary>
        /// 是否压缩（0 否 1是）
        /// </summary>
        public int IsGzip { get; set; }
        /// <summary>
        /// 表格题-是否开启复制Excel数据（0 否 1是）
        /// </summary>
        public int IsCopy { get; set; }
        /// <summary>
        /// 表格题-是否开启复制大数据（0 否 1是）
        /// </summary>
        public int IsCopyBigData { get; set; }
        /// <summary>
        /// 表格题-是否无序（0 否 1是）
        /// </summary>
        public int IsDisorder { get; set; }

        /// <summary>
        /// 非完整性得分
        /// </summary>
        public decimal PartialScore { get; set; }
        /// <summary>
        /// 表格题-计算方式（按行计算、按列计算、按单元格平均计算、自由计算）
        /// </summary>
        public CalculationType CalculationType { get; set; }

        /// <summary>
        /// 凭证题目相关设置
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CertificateTopicView CertificateTopic { get; set; }

    }
}
