﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 题目基类
    /// </summary>
    public class BaseQuestionNew
    {
        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 父节点ID-用于多题型(一个题目 多个作答区)
        /// </summary>
        public long ParentId { get; set; } = 0;
        /// <summary>
        /// 是否结账题（0 否 1是）
        /// </summary>
        public int IsSettlement { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }


        /// <summary>
        /// 题型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 题目内容类型(1:纯文字,2:表格,3:图片)
        /// </summary>
        public QuestionContentType QuestionContentType { get; set; }
        /// <summary>
        /// 题干内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 题目数据源内容(不含答案)
        /// </summary> 
        public string TopicContent { get; set; }

        /// <summary>
        /// 题目数据源内容(含答案)
        /// </summary>
        public string FullContent { get; set; }

        /// <summary>
        /// 题目难度
        /// </summary>
        public int? DifficultyLevel { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public virtual string AnswerValue { get; set; }
        /// <summary>
        /// 答案解析
        /// </summary>
        public virtual string Hint { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科） 此处不可编辑，来自案例主表学历标记
        /// </summary>
        public int Education { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 会计期间
        /// </summary>
        public string AccountingPeriodDate { get; set; }
        /// <summary>
        /// 建议时长
        /// </summary>
        public string AdviceTime { get; set; }
        /// <summary>
        /// 流程Id
        /// </summary>
        public long FlowId { get; set; }
        /// <summary>
        /// 部门Id
        /// </summary>
        public long DepartmentId { get; set; }

        /// <summary>
        /// 答案提示文件，多个文件已“,”分隔
        /// </summary>
        public string HintFlies { get; set; }
    }
    /// <summary>
    /// 主题干
    /// </summary>
    public class MainQuestionNew
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 所属流程Id
        /// </summary>
        public long FlowId { get; set; }
        /// <summary>
        /// 部门Id
        /// </summary>
        public long DepartmentId { get; set; }
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 是否结账题（0 否 1是）
        /// </summary>
        public int IsSettlement { get; set; }
        /// <summary>
        /// 具体分类ID：基本技能类别ID，课程ID等
        /// </summary>
        public int QuestionTagTypeId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 解析
        /// </summary>
        public string Hint { get; set; }
        /// <summary>
        /// 题干内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 题目附件
        /// </summary>
        public List<QuestionFileNew> QuestionFile { get; set; }
        /// <summary>
        /// 题型
        /// </summary>
        [JsonIgnore]
        public QuestionType QuestionType => QuestionType.MainSubQuestion;

        /// <summary>
        /// 题目难度
        /// </summary>
        public int? DifficultyLevel { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科） 此处不可编辑，来自案例主表学历标记
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string Keywords { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 会计期间
        /// </summary>
        public string AccountingPeriodDate { get; set; }
        /// <summary>
        /// 建议时长
        /// </summary>
        public string AdviceTime { get; set; }

        /// <summary>
        /// 答案提示文件，多个文件已“,”分隔
        /// </summary>
        public string HintFlies { get; set; }

    }
    /// <summary>
    /// 子题目
    /// </summary>
    public class SubQuestionNew
    {
        public SubQuestionNew(){}

        public SubQuestionNew(long id, QuestionType questionType, QuestionContentType questionContentType, string templateId, string hint,
            string content, string topicContent, string fullContent, string answerValue, int sort, decimal score, decimal partialScore, CalculationType calculationType)
        {
            Id = id;
            QuestionType = questionType;
            QuestionContentType = questionContentType;
            TemplateId = templateId;
            Hint = hint;
            Content = content;
            TopicContent = topicContent;
            FullContent = fullContent;
            AnswerValue = answerValue;
            Sort = sort;
            Score = score;
            PartialScore = partialScore;
            CalculationType = calculationType;
        }


        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 题型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 题目内容类型(1:纯文字,2:表格,3:图片)
        /// </summary>
        public QuestionContentType QuestionContentType { get; set; }

        /// <summary>
        /// 选项--多题型要支持选择题
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ChoiceOptionNew> Options { get; set; }
        /// <summary>
        /// 模板ID
        /// </summary>
        public string TemplateId { get; set; }

        /// <summary>
        /// 答案解析
        /// </summary>
        public virtual string Hint { get; set; }
        /// <summary>
        /// 标题
        /// </summary> 
        public string Content { get; set; }
        /// <summary>
        /// 题目数据源内容(不含答案)
        /// </summary> 
        public string TopicContent { get; set; }

        /// <summary>
        /// 题目数据源内容(含答案)
        /// </summary>
        public string FullContent { get; set; }

        /// <summary>
        /// 答案
        /// </summary>
        public virtual string AnswerValue { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 非完整性得分
        /// </summary>
        public decimal PartialScore { get; set; } = 0;

        #region 表格题
        /// <summary>
        /// 表格题-计算方式（按行计算、按列计算、按单元格平均计算、自由计算）
        /// </summary>
        public CalculationType CalculationType { get; set; }
        /// <summary>
        /// 表格题-是否开启复制Excel数据（0 否 1是）
        /// </summary>
        public int IsCopy { get; set; }
        /// <summary>
        /// 表格题-是否开启复制大数据（0 否 1是）
        /// </summary>
        public int IsCopyBigData { get; set; }
        /// <summary>
        /// 表格题-是否无序（0 否 1是）
        /// </summary>
        public int IsDisorder { get; set; }
        #endregion

        #region 分录题专用
        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }
        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertificateDate { get; set; }
        /// <summary>
        /// 单据数据记录
        /// </summary>
        public List<CertificateDataRecordNew> DataRecords { get; set; }
        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountingManager { get; set; }
        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }
        /// <summary>
        /// 制单人
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 会计主管是否禁用
        /// </summary>
        public Status IsDisableAM { get; set; }
        /// <summary>
        /// 出纳是否禁用
        /// </summary>
        public Status IsDisableCashier { get; set; }
        /// <summary>
        /// 审核人是否禁用
        /// </summary>
        public Status IsDisableAuditor { get; set; }
        /// <summary>
        /// 制单人是否禁用
        /// </summary>
        public Status IsDisableCreator { get; set; }
        /// <summary>
        /// 计分方式 （0 统一设置 1 单独设置 2 单元格计分）
        /// </summary>
        public AccountEntryCalculationType CalculationScoreType { get; set; }
        /// <summary>
        /// 外围
        /// </summary>
        public int OutVal { get; set; }
        /// <summary>
        /// 里围
        /// </summary>
        public int InVal { get; set; }

        /// <summary>
        /// 凭证题目相关设置
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CertificateTopicView CertificateTopic { get; set; }

        #endregion
    }
}
