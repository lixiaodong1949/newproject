﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 根据条件查询案例题目列表 请求dto
    /// </summary>
    public class QueryTopicRequestDto
    {
        /// <summary>
        /// 实训类型
        /// </summary>
        public long DrillTypeId { get; set; }
        /// <summary>
        /// 流程Id
        /// </summary>
        public long FlowId { get; set; }
        /// <summary>
        /// 部门Id
        /// </summary>
        public long DepartmentId { get; set; }
        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType? QuestionType { get; set; }
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
    }
    /// <summary>
    ///  根据条件查询案例题目列表 返回dto
    /// </summary>
    public class QueryCaseTopicDto
    {
        /// <summary>
        /// 流程Id
        /// </summary>
        public long FlowId { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>
        public string FlowName { get; set; }
        [JsonIgnore]
        public int Sort { get; set; }
        /// <summary>
        /// 题目
        /// </summary>
        public List<CaseTopicDto> Topics { get; set; }

    }
    /// <summary>
    ///  根据条件查询案例题目列表 返回dto
    /// </summary>
    public class CaseTopicDto
    {
        /// <summary>
        /// 流程Id
        /// </summary>
        [JsonIgnore]
        public long FlowId { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>
        [JsonIgnore]
        public string FlowName { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>
        public long TopicId { get; set; }
        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 部门Id
        /// </summary>
        public long DepartmentId { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string DepartmentName { get; set; }
        ///// <summary>
        ///// 所属岗位（此处数据来源于案例表所选的岗位集合）
        ///// </summary>
        //public long PositionId { get; set; }
        ///// <summary>
        ///// 题目类型
        ///// </summary>
        //public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
        ///// <summary>
        ///// 学历标记（0 中职 1高职 2 本科）
        ///// </summary>
        //public int Education { get; set; }
        /// <summary>
        /// 建议时长
        /// </summary>
        public string AdviceTime { get; set; }
        [JsonIgnore]
        public int TopicSort { get; set; }
        [JsonIgnore]
        public int FlowSort { get; set; }
    }
}
