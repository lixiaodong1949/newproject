﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.S.Dto.SceneTraining;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程场景实训
    /// </summary>
    public class YssxSectionSceneTrainingDtoNew : BaseDto
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long SceneTrainingId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
