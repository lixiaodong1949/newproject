﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程习题
    /// </summary>
    public class YssxSectionTopicDtoNew:BaseDto
    {

        /// <summary>
        /// 题目名
        /// </summary>
        public string QuetionName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long SectionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long KnowledgePointId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public int QuestionType { get; set; }
        
        /// <summary>
        /// 题干内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
    }
}
