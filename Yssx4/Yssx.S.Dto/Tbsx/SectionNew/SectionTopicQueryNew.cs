﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 章节习题查询实体
    /// </summary>
    public  class SectionTopicQueryNew
    {
        /// <summary>
        /// 节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 题目类型(PC冗余查询字段)
        /// </summary>
        public Nullable<QuestionType> QuestionType { get; set; }

        /// <summary>
        /// 题目搜索(PC冗余查询字段)
        /// </summary>
        public string Keyword { get; set; }

    }
}
