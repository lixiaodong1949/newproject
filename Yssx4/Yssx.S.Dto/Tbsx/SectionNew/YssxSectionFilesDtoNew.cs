﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    ///  课程课件
    /// </summary>
    public class YssxSectionFilesDtoNew:BaseDto
    {
        /// <summary>
        /// 
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string SectionName { get; set; }

        /// <summary>
        /// title
        /// </summary>
        public string SectionTitle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件类型0:Excel,1:Word,2:PDF,3:PPT,4:Img,5:ZIP,6:视频
        /// </summary>
        public int FilesType { get; set; }

        /// <summary>
        /// 文件大小
        /// </summary>
        public int FileSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 1：课件，2：教案
        /// </summary>
        public int SectionType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long CourseFileId { get; set; }
        /// <summary>
        /// 来源0：课件库.1:新增课件
        /// </summary>
        public int FromType { get; set; }
    }
}
