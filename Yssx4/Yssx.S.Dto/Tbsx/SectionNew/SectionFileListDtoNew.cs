﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 章节下面的文件
    /// </summary>
    public class SectionFileListDtoNew
    {
        /// <summary>
        /// 节下面的文件
        /// </summary>
        public List<YssxSectionFilesDtoNew> SectionFileList { get; set; }


        /// <summary>
        /// 课程下面的文件
        /// </summary>
        public List<YssxSectionFilesDtoNew> CourseFileList { get; set; }
    }
}
