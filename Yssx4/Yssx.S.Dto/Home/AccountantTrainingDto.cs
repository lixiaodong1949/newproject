﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Home
{
    /// <summary>
    /// 会计实训
    /// </summary>
    public class AccountantTrainingDto
    {
        /// <summary>
        /// 考试id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 试卷命名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 案例简短描述
        /// </summary>
        public string CaseDesc { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 成绩id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 实训类型
        /// </summary>
        public int SxType { get; set; }

        /// <summary>
        /// 创建年份
        /// </summary>
        public string CaseYear { get; set; }


        /// <summary>
        /// 所属行业名称
        /// </summary>
        public string IndustryName { get; set; }


        /// <summary>
        /// 学历
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 行业类型
        /// </summary>
        public long IndustryId { get; set; }
    }
}
