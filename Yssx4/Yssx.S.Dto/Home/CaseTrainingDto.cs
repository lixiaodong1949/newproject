﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Home
{
    public class CaseTrainingDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 案例简短描述
        /// </summary>
        public string CaseDesc { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author{ get; set; }
    }
}
