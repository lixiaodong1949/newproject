﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Home
{
    /// <summary>
    /// 课程实训
    /// </summary>
    public class CourseTrainingDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 课程符标题
        /// </summary>
        public string CourseTitle { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 学历
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 课程封面
        /// </summary>
        public string Images { get; set; }

        public int UseType { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        public string Intro { get; set; }

        public string PublishingDate { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }
    }
}
