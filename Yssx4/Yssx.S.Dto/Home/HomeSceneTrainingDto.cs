﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Home
{
    /// <summary>
    /// 情景实训
    /// </summary>
    public class HomeSceneTrainingDto
    {
        /// <summary>
        /// 情景实训Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 公司ID
        /// </summary>
        public long SelectCompanyId { get; set; }

        public string SelectCompanyName { get; set; }

        /// <summary>
        /// 业务情景ID
        /// </summary>
        public long YwcjId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 业务情景名称
        /// </summary>
        public string YwcjName { get; set; }


        public string Ywwl { get; set; }

        /// <summary>
        /// 情景简短描述
        /// </summary>
        public string SceneDesc { get; set; }
    }
}
