﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取用户列表 - 入参
    /// </summary>
    public class GetUserListRequest : PageRequest
    {
        /// <summary>
        /// 学校ID
        /// </summary>
        public long? TenantId { get; set; }
        /// <summary>
        /// 用户类型(0.管理员 1.学生 2.教师 3.教务 4.专家 5.开发 6.专业组 7.普通用户)
        /// </summary> 
        public int? UserType { get; set; }
        /// <summary>
        /// 用户状态(0.禁用 1.启用 2.激活码禁用)
        /// </summary> 
        public int? Status { get; set; }

        /// <summary>
        /// 用户真实姓名或手机号
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 是否测试学校(-1.所有 0.正常 1.测试)
        /// </summary>
        public int IsDisplay { get; set; } = -1;

    }
}
