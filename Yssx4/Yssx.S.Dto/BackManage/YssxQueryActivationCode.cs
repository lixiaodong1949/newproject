﻿using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询学校激活码--传入参数
    /// </summary>
    public class YssxQueryActivationCode : PageRequest
    {
        /// <summary>
        /// 学校Id
        /// </summary>
        public long? schoolId { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string schoolName { get; set; }

        /// <summary>
        /// 激活码
        /// </summary>
        public string Code { get; set; }
    }
}
