﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 后端登陆信息
    /// </summary>
    public class YssxbkbkUserDao
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 0普通用户1管理员
        /// </summary>
        public int UserType { get; set; }
    }
}
