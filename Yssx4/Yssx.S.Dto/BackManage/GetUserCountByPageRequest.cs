﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询用户统计列表
    /// </summary>
    public class GetUserCountByPageRequest : PageRequest
    {
        /// <summary>
        /// 学校名称
        /// </summary>
        public string Name { get; set; }

    }

    /// <summary>
    /// 查询用户统计列表
    /// </summary>
    public class GetUserCountByPageDto
    {
        /// <summary>
        /// 用户总数量
        /// </summary>
        public long TotalCount { get; set; }

        /// <summary>
        /// 学校用户统计明细
        /// </summary>
        public List<GetUserCountByPageDetail> SchoolDetail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; } = 20;
        /// <summary>
        /// 
        /// </summary>
        public long RecordCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PageCount => (int)Math.Ceiling((decimal)RecordCount / PageSize);
    }

    /// <summary>
    /// 学校用户统计
    /// </summary>
    public class GetUserCountByPageDetail
    {
        /// <summary>
        /// 学校ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 用户数量
        /// </summary>
        public long UserCount { get; set; }

        /// <summary>
        /// 学校名字
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 学生总数量
        /// </summary>
        public long StudentCount { get; set; }

        /// <summary>
        /// 教师数量
        /// </summary>
        public long TeacherCount { get; set; }

    }

}
