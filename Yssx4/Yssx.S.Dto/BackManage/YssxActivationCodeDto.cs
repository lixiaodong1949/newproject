﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Yssx.S.Pocos.BackManage;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学校激活码传参
    /// </summary>
    public class YssxActivationCodeListDto
    {
        /// <summary>
        /// 激活码集合
        /// </summary>
        public List<YssxActivationCodeDto> CodeList { get; set; }

    }

    /// <summary>
    /// 激活码
    /// </summary>
    public class YssxActivationCodeDto
    {
        /// <summary>
        /// ID
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        ///  备注
        /// </summary>
        public string Remarks { get; set; }
        /// <summary>
        /// 激活码类型（0管理员,1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户）
        /// </summary>
        public int TypeId { get; set; }
        /// <summary>
        /// 类型名称
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// 激活码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// 学校ID
        /// </summary>
        public long SchoolId { get; set; }
        /// <summary>
        /// 可以注册数量
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// 激活截至日期
        /// </summary>
        public DateTime ActivationDeadline { get; set; }
        /// <summary>
        /// 已激活数量
        /// </summary>
        public int ActivatedNumber { get; set; }
        /// <summary>
        /// 状态 0禁用1启用
        /// </summary>
        public int State { get; set; }
        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 入学年级
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// 套餐Id
        /// </summary>
        public List<long> ResourceIds { get; set; }
    }
}
