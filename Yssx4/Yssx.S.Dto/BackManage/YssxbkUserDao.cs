﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    #region 后端
    /// <summary>
    /// 后端用户 密码重置
    /// </summary>
    public class BkResetPassword
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 原密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 新密码
        /// </summary>
        public string NewPassword { get; set; }
        /// <summary>
        /// 确定密码
        /// </summary>
        public string ConfirmPassword { get; set; }
    }
    #endregion

    #region 前端
    /// <summary>
    /// 前端用户 传入参数
    /// </summary>
    public class YssxbkUserDao
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NikeName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// QQ
        /// </summary>
        public string QQ { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 用户状态 0禁用1启用2激活码禁用
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 用户账号
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户类型0管理员 1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户
        /// </summary> 
        public int UserType { get; set; }

        /// <summary>
        /// 微信
        /// </summary>
        public string WeChat { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IdNumber { get; set; }


        /// <summary>
        /// 激活码编号
        /// </summary>
        public string ActivationCodeNo { get; set; }

        /// <summary>
        /// 学校 传Id
        /// </summary>
        public long School { get; set; }

    }

    /// <summary>
    /// 前端用户 密码重置
    /// </summary>
    public class ResetPassword
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

    }

    /// <summary>
    /// 前端用户状态 启用静用
    /// </summary>
    public class StateControl
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 用户状态 0禁用 1启用 2激活码禁用
        /// </summary>
        public int Status { get; set; }

    }

    /// <summary>
    /// 前端用户 返回参数
    /// </summary>
    public class YssxUserResponse : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 邮箱
        /// </summary>
        [Column(Name = "Email", DbType = "varchar(255)", IsNullable = true)]
        public string Email { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [Column(Name = "MobilePhone", DbType = "varchar(255)", IsNullable = true)]
        public string MobilePhone { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [Column(Name = "NikeName", DbType = "varchar(255)", IsNullable = true)]
        public string NikeName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Column(Name = "Password", DbType = "varchar(255)", IsNullable = true)]
        public string Password { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        [Column(Name = "Photo", DbType = "varchar(255)", IsNullable = true)]
        public string Photo { get; set; }

        /// <summary>
        /// QQ
        /// </summary>
        [Column(Name = "QQ", DbType = "varchar(255)", IsNullable = true)]
        public string QQ { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        [Column(Name = "RealName", DbType = "varchar(255)", IsNullable = true)]
        public string RealName { get; set; }

        /// <summary>
        /// 用户状态 0禁用1启用2激活码禁用
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 用户账号
        /// </summary>
        [Column(Name = "UserName", DbType = "varchar(255)", IsNullable = true)]
        public string UserName { get; set; }

        /// <summary>
        /// 用户类型0管理员,1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户
        /// </summary> 
        public int UserType { get; set; }

        /// <summary>
        /// 微信
        /// </summary>
        [Column(Name = "WeChat", DbType = "varchar(255)", IsNullable = true)]
        public string WeChat { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        [Column(Name = "IdNumber", DbType = "varchar(20)", IsNullable = true)]
        public string IdNumber { get; set; }

        /// <summary>
        /// 客户端id
        /// </summary>
        public Guid ClientId { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string PresidentName { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string CollegeIdName { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 学生工号学号
        /// </summary>
        public long StudentNo { get; set; }

        /// <summary>
        /// 学生入学日期
        /// </summary>
        public DateTime EntranceDateTime { get; set; }
    }

    /// <summary>
    /// 前端用户 查询传入参数
    /// </summary>
    public class YssxQuerybkUserDao : PageRequest
    {
        /// <summary>
        /// 学校 Id
        /// </summary>
        public long? SchoolId { get; set; }
        /// <summary>
        /// 用户类型 0管理员,1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户
        /// </summary> 
        public int? UserType { get; set; }
        /// <summary>
        /// 用户状态 0禁用,1启用,2激活码禁用
        /// </summary>
        public int? Status { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
    }

    public class UserListInfoDto
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public int UserType { get; set; }

        /// <summary>
        /// 学校 传Id
        /// </summary>
        public long SchoolId { get; set; }

        public int Number { get; set; }
    }
    #endregion


}
