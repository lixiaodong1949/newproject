﻿

using Yssx.Framework.Entity;

namespace Yssx.S.Dto.BigDatas.Dto
{
    public class PaperCenterRequest : PageRequest
    {
        /// <summary>
        /// 是否获取所有 0 获取未购买的案例  1获取所有案例
        /// </summary>
        public bool IsGetAll { get; set; } = true;
    }
}
