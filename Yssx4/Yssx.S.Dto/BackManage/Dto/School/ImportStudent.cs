﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导入学生
    /// </summary>
    public class ImportStudent
    {
        /// <summary>
        /// 学生姓名
        /// </summary>
        [Description("学生姓名")]
        public string Name { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        [Description("性别")]
        public string Sex { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [Description("手机号")]
        public string MobilePhone { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary> 
        [Description("身份证号")]
        public string IdNumber { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [Description("邮箱")]
        public string Email { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        [Description("学号")]
        public string StudentNo { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        [Description("班级名称")]
        public string ClassName { get; set; }
        /// <summary>
        /// 专业名称
        /// </summary>
        [Description("专业名称")]
        public string ProfessionName { get; set; }
        /// <summary>
        /// 院系名称
        /// </summary>
        [Description("院系名称")]
        public string CollegeName { get; set; }
        /// <summary>
        /// 学校名称
        /// </summary>
        [Description("学校名称")]
        public string SchoolName { get; set; }
        /// <summary>
        /// 入学时间
        /// </summary>
        [Description("入学时间")]
        public DateTime? EntranceDateTime { get; set; }
    }

    /// <summary>
    /// 导入学生用户
    /// </summary>
    public class ImportStudentUser {
        /// <summary>
        /// 激活码
        /// </summary>
        [Description("激活码")]
        public string Code { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [Description("姓名")]
        public string Name { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        [Description("角色")]
        public string Role { get; set; }

        /// <summary>
        /// 工学号
        /// </summary>
        [Description("工学号")]
        public string EngineeringNumber { get; set; }

        /// <summary>
        /// 学校
        /// </summary>
        [Description("学校")]
        public string School { get; set; }

        /// <summary>
        /// 院系
        /// </summary>
        [Description("院系")]
        public string FacultyDepartment { get; set; }

        /// <summary>
        /// 专业
        /// </summary>
        [Description("专业")]
        public string Major { get; set; }

        /// <summary>
        /// 班级
        /// </summary>
        [Description("班级")]
        public string Class { get; set; }
    }

    /// <summary>
    /// 导入学生 - 输出
    /// </summary>
    public class ImportStudentResponse : ImportStudentUser
    {
        public ImportStudentResponse()
        {

        }
        /// <summary>
        /// 行号
        /// </summary>
        public int RowIndex { get; set; }
        /// <summary>
        /// 匹配失败消息
        /// </summary>
        public string ErrorMsg { get; set; }
    }
}
