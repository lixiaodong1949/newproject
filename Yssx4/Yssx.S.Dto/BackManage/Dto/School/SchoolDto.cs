﻿using System;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学校信息入参
    /// </summary>
    public class SchoolDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 学校名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 院校代码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 院校类型(0-中专，1-大专，2=本科)
        /// </summary>
        public SchoolType Type { get; set; }

        /// <summary>
        /// 院校简称
        /// </summary> 
        public string ShortName { get; set; }

        /// <summary>
        /// 院长姓名
        /// </summary>
        public string DeanName { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 学校地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 校徽
        /// </summary>
        public string Logo { get; set; }

        /// <summary>
        /// 账号开始时间
        /// </summary>
        public DateTime AccountStartTime { get; set; }
        /// <summary>
        /// 账号到期时间
        /// </summary>
        public DateTime AccountEndTime { get; set; }


        /// <summary>
        /// 状态（0=禁用（未审核），1-启用（审核））
        /// </summary>
        public Status Status { get; set; }
    }
}
