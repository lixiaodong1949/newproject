﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class ImportTeacherResponse : ImportTeacher
    {
        public ImportTeacherResponse()
        {

        }
        /// <summary>
        /// 行号
        /// </summary>
        public int RowIndex { get; set; }
        /// <summary>
        /// 匹配失败消息
        /// </summary>

        public string ErrorMsg { get; set; }
    }
}
