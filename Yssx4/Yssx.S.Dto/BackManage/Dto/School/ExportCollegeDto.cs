﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导出院系
    /// </summary>
    public class ExportCollegeDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Description("名称")]
        public string Name { get; set; }

        /// <summary>
        /// 院系长
        /// </summary>
        [Description("院系长")]
        public string President { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Description("状态")]
        public string Status { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        [Description("学校名称")]
        public string SchoolName { get; set; }

    }
}
