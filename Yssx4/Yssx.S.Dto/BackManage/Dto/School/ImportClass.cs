﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导入班级
    /// </summary>
    public class ImportClass
    {
        /// <summary>
        /// 班级名称
        /// </summary>
        [Description("班级")]
        public string ClassName { get; set; }
        /// <summary>
        /// 专业名称
        /// </summary>
        [Description("专业")]
        public string ProfessionName { get; set; }
        /// <summary>
        /// 院系名称
        /// </summary>
        [Description("院系")]
        public string CollegeName { get; set; }
    }
}
