﻿

using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询院系入参
    /// </summary>
    public class SchoolCollegeRequest : PageRequest
    {
        /// <summary>
        /// 租户Id(学校Id)
        /// </summary>
        public long? TenantId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string Name { get; set; }
    }
}
