﻿using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 老师
    /// </summary>
    public class TeacherRequest : PageRequest
    {
        /// <summary>
        /// 学校ID
        /// </summary>
        public long? TenantId { get; set; }

        /// <summary>
        /// 学院ID
        /// </summary>
        public long? CollegeId { get; set; }

        /// <summary>
        /// 教师类别
        /// </summary>
        public TeacherType? TeacherType { get; set; }

        /// <summary>
        /// 模糊查询关键字
        /// </summary>
        public string KeyWord { get; set; }

    }
}
