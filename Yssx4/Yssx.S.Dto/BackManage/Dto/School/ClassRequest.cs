﻿

using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 班级入参
    /// </summary>
    public class ClassRequest : PageRequest
    {
        /// <summary>
        /// 租户Id(学校Id)
        /// </summary>
        public long? TenantId { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary> 
        public long? CollegeId { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long? ProfessionId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string Name { get; set; }
    }
    /// <summary>
    /// 考试班级入参
    /// </summary>
    public class MakeExamClassRequest
    {
        /// <summary>
        /// 院系Id
        /// </summary> 
        public long[] CollegeId { get; set; }

        /// <summary>
        /// 学年
        /// </summary>
        public string[] SchoolYear { get; set; }

    }
}
