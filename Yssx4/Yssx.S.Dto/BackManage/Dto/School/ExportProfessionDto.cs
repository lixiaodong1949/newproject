﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导出专业
    /// </summary>
    public class ExportProfessionDto
    {
        /// <summary>
        /// 专业名称
        /// </summary>
        [Description("专业名称")]
        public string Name { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        [Description("院系名称")]
        public string CollegeName { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        [Description("学校名称")]
        public string SchoolName { get; set; }
    }
}
