﻿

using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class GetStudentRequest : PageRequest
    {
        /// <summary>
        /// 学校ID
        /// </summary>
        public long? TenantId { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary>
        public long? CollegeId { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary>
        public long? ProfessionId { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary>
        public long? ClassId { get; set; }

        /// <summary>
        /// 模糊查询关键字
        /// </summary>
        public string KeyWord { get; set; }


        /// <summary>
        /// 老师Id
        /// </summary>
        public long TeacherId { get; set; }
        /// <summary>
        /// 入学年份
        /// </summary>
        public long? EntranceYear { get; set; }
    }
}
