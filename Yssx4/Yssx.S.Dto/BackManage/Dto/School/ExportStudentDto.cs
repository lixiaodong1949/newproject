﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导出学生
    /// </summary>
    public class ExportStudentDto
    {
        /// <summary>
        /// 真实姓名
        /// </summary>
        [Description("真实姓名")]
        public string RealName { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [Description("邮箱")]
        public string Email { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [Description("性别")]
        public string Sex { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [Description("用户名")]
        public string UserName { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        [Description("手机号码")]
        public string MobilePhone { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary> 
        [Description("身份证号码")]
        public string IdNumber { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        [Description("学号")]
        public string StudentNo { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary> 
        [Description("专业名称")]
        public string ProfessionName { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        [Description("院系名称")]
        public string CollegeName { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        [Description("班级名称")]
        public string ClassName { get; set; }
    }
}
