﻿using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学校列表 - 请求信息
    /// </summary>
    public class SchoolPageRequest : PageRequest
    {
        /// <summary>
        /// 学校Id
        /// </summary>
        public long? SchoolId { get; set; }
        /// <summary>
        /// 学校
        /// </summary>
        public string Name { get; set; }

    }
}
