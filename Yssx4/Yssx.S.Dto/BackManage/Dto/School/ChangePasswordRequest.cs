﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 更改密码
    /// </summary>
    public class ChangePasswordRequest
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 旧密码
        /// </summary>
        public string OldPassword { get; set; }
        /// <summary>
        /// 新密码
        /// </summary>
        public string NewPassword { get; set; }
    }
}
