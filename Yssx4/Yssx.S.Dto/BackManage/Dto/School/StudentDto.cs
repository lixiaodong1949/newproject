﻿using System;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学生
    /// </summary>
    public class StudentDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary> 
        public string IdNumber { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }
        /// <summary>
        ///院系Id
        /// </summary> 
        public long CollegeId { get; set; }
        /// <summary>
        /// 学校Id
        /// </summary> 
        public long TenantId { get; set; }
        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }
        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary> 
        public string ProfessionName { get; set; }
        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary> 
        public string SchoolName { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary> 
        public Status Status { get; set; }
        /// <summary>
        /// 入学时间
        /// </summary>
        public DateTime? EntranceDateTime { get; set; }
        /// <summary>
        /// 学年
        /// </summary>
        public string SchoolYear => (EntranceDateTime?.Year)?.ToString();
        /// <summary>
        /// 其他联系方式
        /// </summary>
        public string OtherContacts { get; set; }
        /// <summary>
        /// 是否已抽查
        /// </summary>
        public bool IsSpotExamed { get; set; }

    }

    /// <summary>
    /// 学生列表
    /// </summary>
    public class StudentDtoSummary
    {
        /// <summary>
        /// 班级数量
        /// </summary>
        public int ClassCount { get; set; }
        /// <summary>
        /// 分页数据
        /// </summary>
        public PageResponse<StudentDto> PageData { get; set; }
    }
}
