﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 老师关联班级
    /// </summary>
    public class TeacherClassRequest
    {
        /// <summary>
        /// 学校ID
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 老师ID
        /// </summary>
        public long TeacherID { get; set; }

        /// <summary>
        /// 班级ID列表
        /// </summary>
        public List<long> ClassIds { get; set; }
    }
}
