﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导出学校
    /// </summary>
    public class ExportSchoolDto
    {
        /// <summary>
        /// 学校名称
        /// </summary>
        [Description("学校名称")]
        public string Name { get; set; }
        /// <summary>
        /// 院校代码
        /// </summary>
        [Description("院校代码")]
        public string Code { get; set; }
        /// <summary>
        /// 院校类型(0-中专，1-大专，2=本科)
        /// </summary>
        [Description("院校类型")]
        public string SchoolType { get; set; }

        /// <summary>
        /// 院校简称
        /// </summary> 
        [Description("院校简称")]
        public string ShortName { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        [Description("地址")]
        public string Address { get; set; }

        /// <summary>
        /// 院长姓名
        /// </summary>
        [Description("院长姓名")]
        public string DeanName { get; set; }

        /// <summary>
        /// 账号开始时间
        /// </summary>
        [Description("账号开始时间")]
        public DateTime AccountStartTime { get; set; }
        /// <summary>
        /// 账号到期时间
        /// </summary>
        [Description("账号到期时间")]
        public DateTime AccountEndTime { get; set; }

        /// <summary>
        /// 状态（0-启用（审核），1=禁用（未审核））
        /// </summary>
        [Description("状态")]
        public string Status { get; set; }
    }
}
