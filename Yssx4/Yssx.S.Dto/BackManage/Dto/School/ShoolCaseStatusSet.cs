﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class ShoolCaseStatusSet
    {
        /// <summary>
        /// 要设置的案例id数组
        /// </summary>
        public long[] CaseIds { get; set; }

        /// <summary>
        /// 0：不开启  1：开启
        /// </summary>
        public int Status { get; set; }
    }
}
