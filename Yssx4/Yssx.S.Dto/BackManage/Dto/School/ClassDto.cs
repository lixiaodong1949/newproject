﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class ClassDto : BizBaseDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary>
        public long CollegeId { get; set; }
        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }
        /// <summary>
        /// 专业名称
        /// </summary> 
        public string ProfessionName { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary> 
        public string SchoolName { get; set; }

        /// <summary>
        /// 学生数量
        /// </summary>
        public long StudentCount { get; set; }
        /// <summary>
        /// 任教老师
        /// </summary>
        public string TeachersName { get; set; }
        /// <summary>
        /// 入学年份
        /// </summary>
        public string SchoolYear { get; set; }

        [JsonIgnore]
        public DateTime EntranceDateTime { get; set; }
    }

    public class ClassMiniDto
    {
        /// <summary>
        /// 班级ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 班级名称
        /// </summary>
        public string Name { get; set; }
    }

    public class SchoolYearDto
    {
        public int Year { get; set; }
    }
}
