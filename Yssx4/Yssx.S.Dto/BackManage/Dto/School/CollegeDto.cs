﻿using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 院系
    /// </summary>
    public class CollegeDto : BizBaseDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 院系长
        /// </summary>
        public string President { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

    }
}
