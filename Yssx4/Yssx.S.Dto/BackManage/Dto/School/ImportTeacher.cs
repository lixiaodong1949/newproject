﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导入教师
    /// </summary>
    public class ImportTeacher
    {
        /// <summary>
        /// 学校名称
        /// </summary>
        [Description("学校名称")]
        public string SchoolName { get; set; }
        /// <summary>
        /// 院系名称
        /// </summary>
        [Description("院系名称")]
        public string CollegeName { get; set; }
        /// <summary>
        /// 工号
        /// </summary>
        [Description("工号")]
        public string TeacherNo { get; set; }
        /// <summary>
        /// 老师姓名
        /// </summary>
        [Description("老师姓名")]
        public string Name { get; set; }
        /// <summary>
        /// 职称
        /// </summary>
        [Description("职称")]
        public string Titles { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        [Description("性别")]
        public string Sex { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [Description("手机号")]
        public string MobilePhone { get; set; }
        /// <summary>
        /// 身份证号
        /// </summary>
        [Description("身份证号")]
        public string IdNumber { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [Description("邮箱")]
        public string Email { get; set; }
    }
}
