﻿using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师信息
    /// </summary>
    public class TeacherDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 学校ID-后台操作需要传入此值
        /// </summary>
        public long TenantId { get; set; }
        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }
        /// <summary>
        /// 院系ID
        /// </summary>
        public long? CollegeId { get; set; }
        /// <summary>
        /// 院系
        /// </summary>
        public string CollegeName { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 职称
        /// </summary>        
        public string Titles { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary> 
        public string IdNumber { get; set; }

        /// <summary>
        /// 老师类型
        /// </summary>
        public TeacherType Type { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 性别 -1未知 0男 1女
        /// </summary>
        public Sex Sex { get; set; }
        /// <summary>
        /// 邮箱 
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 工号
        /// </summary>
        public string TeacherNo { get; set; }
    }
}
