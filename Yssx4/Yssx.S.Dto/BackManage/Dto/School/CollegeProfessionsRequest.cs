﻿

using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询学院-专业入参
    /// </summary>
    public class CollegeProfessionsRequest : PageRequest
    {
        /// <summary>
        /// 院系Id(前端传值用)
        /// </summary>
        public long? CollegesId { get; set; }

        /// <summary>
        /// 租户Id(学校Id)
        /// </summary>
        public long? TenantId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string Name { get; set; }
    }
}
