﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学校案例设置实体
    /// </summary>
    public class ShoolCaseSet
    {
        /// <summary>
        /// 要设置的案例id数组
        /// </summary>
        public long[] CaseIds { get; set; }

        /// <summary>
        /// 要设置的学校id数组
        /// </summary>
        public long[] SchoolIds { get; set; }
    }
}
