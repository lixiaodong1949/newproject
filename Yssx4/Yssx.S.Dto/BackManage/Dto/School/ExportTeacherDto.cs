﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导出教师
    /// </summary>
    public class ExportTeacherDto
    {
        /// <summary>
        /// 真实姓名
        /// </summary>
        [Description("教师名称")]
        public string RealName { get; set; }

        /// <summary>
        /// 职称
        /// </summary>        
        [Description("职称")]
        public string Titles { get; set; }

        /// <summary>
        /// 院系
        /// </summary>
        [Description("院系")]
        public string CollegeName { get; set; }

        /// <summary>
        /// 邮箱 
        /// </summary>
        [Description("邮箱")]
        public string Email { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary> 
        [Description("身份证号码")]
        public string IdNumber { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        [Description("手机号码")]
        public string MobilePhone { get; set; }
    }
}
