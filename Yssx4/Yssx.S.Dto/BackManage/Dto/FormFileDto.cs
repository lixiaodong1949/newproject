﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Yssx.S.Dto
{
    public class FormFileDto
    {
        public string FileName { get; set; }

        public Stream FileStream { get; set; }
    }
}
