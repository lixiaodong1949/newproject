﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class BizBaseDto
    {
        /// <summary>
        /// 租户id(学校id)
        /// </summary>
        public long TenantId { get; set; }
    }
}
