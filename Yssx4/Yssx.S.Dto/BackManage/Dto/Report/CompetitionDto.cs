﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Report
{
    /// <summary>
    /// 
    /// </summary>
    public class CompetitionDto
    {
        /// <summary>
        /// 房间号(团队)
        /// </summary>
        public string GroupNo { get; set; }
        
        /// <summary>
        /// 大数据总分
        /// </summary>
        public decimal BigTotalScore { get; set; }
        /// <summary>
        /// 业财税总分
        /// </summary>
        public decimal YcsTotalScore { get; set; }
        /// <summary>
        /// 合计总分
        /// </summary>
        public decimal TotalScore { get; set; }
        /// <summary>
        /// 团队排名
        /// </summary>
        public int Rank { get; set; }
        /// <summary>
        /// 最长用时
        /// </summary>
        public double MaxMinutes { get; set; }
        /// <summary>
        /// 总用时
        /// </summary>
        public decimal TotalSeconds { get; set; }
    }
}
