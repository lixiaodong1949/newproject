﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Report
{
    /// <summary>
    /// 学生
    /// </summary>
    public class StudentGradeInfoDto
    {
        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 案例ID
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 考试Id
        /// </summary>
        public long ExamId { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// 对应作答岗位集合，以逗号隔开
        /// </summary>
        public string[] PostionInfo { get; set; }
        /// <summary>
        /// 总用时
        /// </summary>
        public decimal UsedSeconds { get; set; }
        /// <summary>
        /// 总成绩
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 排名
        /// </summary>
        public int Rank { get; set; }
    }
}
