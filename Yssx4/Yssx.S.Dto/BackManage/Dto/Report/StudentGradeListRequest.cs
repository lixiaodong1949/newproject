﻿using Yssx.Framework.Entity;

namespace Yssx.S.Dto.Report
{
    public class StudentGradeListRequest:PageRequest
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }
    }
}
