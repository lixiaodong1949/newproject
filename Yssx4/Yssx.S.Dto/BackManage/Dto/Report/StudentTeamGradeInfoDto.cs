﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Report
{
    /// <summary>
    /// 学生
    /// </summary>
    public class StudentTeamGradeInfoDto
    {
        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 案例ID
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 考试Id
        /// </summary>
        public long ExamId { get; set; }
        /// <summary>
        /// 房主
        /// </summary>
        public string HostUser { get; set; }
        /// <summary>
        /// 组Id
        /// </summary>
        public string GroupId { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// 对应作答岗位信息，包括岗位、团队成员、个人用时、个人成绩
        /// </summary>
        public List<TeamInfo> TeamInfos { get; set; }
        /// <summary>
        /// 最长用时
        /// </summary>
        public double MaxMinutes { get; set; }
        /// <summary>
        /// 总用时
        /// </summary>
        public double TotalMinutes { get; set; }
        /// <summary>
        /// 总成绩
        /// </summary>
        public decimal TotalScore { get; set; }
        /// <summary>
        /// 团队排名
        /// </summary>
        public int Rank { get; set; }
    }
    /// <summary>
    /// 对应作答岗位信息，包括岗位、团队成员、个人用时、个人成绩
    /// </summary>
    public class TeamInfo
    {
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostionName { get; set; }
        /// <summary>
        /// 该岗位对应作答人
        /// </summary>
        public string StudentName { get; set; }
        /// <summary>
        /// 个人用时
        /// </summary>
        public double Minutes { get; set; }
        /// <summary>
        /// 个人得分
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 是否房主
        /// </summary>
        public bool IsGroupManager { get; set; }
        /// <summary>
        /// 是否当前用户
        /// </summary>
        public bool IsCurrentUser { get; set; }
    }
}
