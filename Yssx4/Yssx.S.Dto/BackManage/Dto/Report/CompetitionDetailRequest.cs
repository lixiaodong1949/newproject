﻿using Yssx.Framework.Entity;

namespace Yssx.S.Dto.Report
{
    public class CompetitionDetailRequest : PageRequest
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
    }
}
