﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 省赛 - 实时监控 输出
    /// </summary>
    public class CompetitionMonitorDto
    {
        /// <summary>
        /// 赛事名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 该赛事任务总题数
        /// </summary>
        public double TotalQuestionCount { get; set; }
        /// <summary>
        /// 该赛事任务总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 明细数据
        /// </summary>
        public List<CompetitionMonitorDetailDto> Detail { get; set; }
    }

    /// <summary>
    /// 省赛 - 实时监控 - 明细
    /// </summary>
    public class CompetitionMonitorDetailDto
    {
        /// <summary>
        /// 团队编号
        /// </summary>
        public string GroupNo { get; set; }
        /// <summary>
        /// 总成绩
        /// </summary>
        public decimal TotalScore { get; set; }
        /// <summary>
        /// 总做题数
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// 排名
        /// </summary>
        public int Rank { get; set; }
    }

}
