﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class LoginRequest
    {
        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; } 
        /// <summary>
        /// 手机号码/学号/工号/QQ/WeChat
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }  
    }
}
