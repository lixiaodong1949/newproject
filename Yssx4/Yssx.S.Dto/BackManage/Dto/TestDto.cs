﻿using Yssx.Redis.Interceptor;
using Yssx.Redis.Interceptor.Attributes;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class TestCacheInterceptorDto: ICacheInterceptorParameter
    {
        /// <summary>
        /// 
        /// </summary>
        [CacheKey(Order =1)]
        public long TopicId { get; set; }
        /// <summary>
        /// 是否强制更新缓存
        /// </summary>
        public bool RefreshCache { get; set; }
    }
}
