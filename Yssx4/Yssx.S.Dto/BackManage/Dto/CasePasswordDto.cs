﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    public class CasePasswordDto
    {
        public string OldPassword;

        public string Password { get; set; }

        public int CasePasswordType { get; set; }
    }
}
