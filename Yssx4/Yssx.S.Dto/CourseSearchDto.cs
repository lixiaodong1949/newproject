﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    public class CourseSearchDto: PageRequest
    {
        /// <summary>
        /// 关键字搜索
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// 0:查自己的，1：查所有的 2:购买的
        /// </summary>
        public int UseType { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 学历 0:中职,1:高职,2:本科
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 课程类型0:常规课
        /// </summary>
        public int CourseType { get; set; }

    }

    public class SetShowDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 0:隐藏,1:显示
        /// </summary>
        public int IsShow { get; set; }
    }
}
