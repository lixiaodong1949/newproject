﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto.Mall
{
    public class CreateMallOrderDto
    {
        /// <summary>
        /// 商品
        /// </summary>
        public List<CreateMallOrderItem> Items { get; set; }
        /// <summary>
        /// 小程序支付需要传入
        /// </summary>
        public string OpenId { get; set; }
        /// <summary>
        /// 平台 - 登录方式（0.PC 1.App 2.小程序 3.H5 4.微店）
        /// </summary>
        public int LoginType { get; set; }
    }

    public class CreateMallOrderItem
    {
        /// <summary>
        /// 标的id
        /// </summary>
        public long TargetId { get; set; }

        /// <summary>
        /// 分类 1.案例 2.月度任务 3.商品
        /// </summary>
        public int Category { get; set; }

        /// <summary>
        /// 来源 （0.商品中心 1.云实习真账实操 2.课程同步实训 3.业税财实训 4.云上实训之岗位实训 5.课程实训 6.经验案例 7.初级会计考证）
        /// </summary>
        public int Source { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity { get; set; }
    }

    public class PayResponse
    {
        /// <summary>
        /// 支付url
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }
    }

    public class PayWechatResponse
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 应用ID
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// 商户号
        /// </summary>
        public string MchId { get; set; }

        /// <summary>
        /// 随机字符串
        /// </summary>
        public string NonceStr { get; set; }

        /// <summary>
        /// 签名
        /// </summary>
        public string Sign { get; set; }

        /// <summary>
        /// 预支付交易会话标识
        /// </summary>
        public string PrepayId { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public string Timestamp { get; set; }

    }

    /// <summary>
    /// 通知短信参数
    /// </summary>
    public class OrderSms
    {
        /// <summary>
        /// 名称
        /// </summary>
        [JsonProperty(PropertyName = "cname")]
        public string Cname { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        [JsonProperty(PropertyName = "cdate")]
        public string Cdate { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        [JsonProperty(PropertyName = "copen")]
        public string Copen { get; set; }
    }

    public class GetUserMallOrder : PageRequest
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public long UserId { get; set; }
    }

    /// <summary>
    /// 单个用户的订单信息
    /// </summary>
    public class UserMallOrder
    {
        /// <summary>
        /// 账号
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 用户类型0管理员,1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户
        /// </summary> 
        public int UserType { get; set; }

        /// <summary>
        /// 微信
        /// </summary>
        public string Wechat { get; set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime RegisterTime { get; set; }

        /// <summary>
        /// 学校学名
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 该用户的订单
        /// </summary>
        public PageResponse<MallOrderDto> Orders { get; set; }
    }

    public class MallOrderDto
    {
        /// <summary>
        /// 订单编号/订单id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 用户id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 支付渠道
        /// * 1 微信 
        /// * 2 支付宝
        /// </summary>
        public int PaymentType { get; set; }

        /// <summary>
        /// 支付时间
        /// </summary>
        public DateTime? PaymentTime { get; set; }

        /// <summary>
        /// 订单来源
        /// * 1 抢购活动
        /// * 2 微店
        /// * 3 前台购买
        /// </summary>
        public int Source { get; set; }

        /// <summary>
        /// 订单实付金额
        /// </summary>
        public decimal OrderTotal { get; set; }

        /// <summary>
        /// 优惠总金额
        /// </summary>
        public decimal OrderDiscount { get; set; }

        /// <summary>
        /// 状态（0.未支付 1.已取消 2.已付款）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 订单状态（0.未支付 1.已取消 2.已付款 3.已关闭）
        /// </summary>
        public int OrderStatus
        {
            get
            {
                var dtNow = DateTime.Now;
                if (Status == 0 && dtNow > CreateTime.AddMinutes(30))
                    return 3;
                else
                    return Status;
            }
        }

        /// <summary>
        /// 第三方支付交易号
        /// </summary>
        public string TransactionNo { get; set; }

        /// <summary>
        /// 推广编码
        /// </summary>
        public long AffiliateId { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 微信
        /// </summary>
        public string WeChat { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        public string ProductName { get; set; }

        public string ProductImg { get; set; }

        /// <summary>
        /// 订单明细
        /// </summary>
        public List<MallOrderItemDto> ItemDetail { get; set; }
    }


    public class MallOrderItemDto
    {
        /// <summary>
        /// 产品Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 产品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// 优惠金额
        /// </summary>
        public decimal DiscountAmount { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }

    }

    public class GetMallOrderDto : PageRequest
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public long? UserId { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 交易状态（-1.全部 0.未支付 1.已取消 2.已付款 3.已关闭）
        /// </summary>
        public int Status { get; set; } = -1;

        /// <summary>
        /// 支付时间 - 起
        /// </summary>
        public DateTime? PaymentTimeBegin { get; set; }

        /// <summary>
        /// 支付时间 - 止
        /// </summary>
        public DateTime? PaymentTimeEnd { get; set; }
    }

    public class MallOrderStatsDto
    {
        /// <summary>
        /// 今日成交金额
        /// </summary>
        public decimal TodayAmount { get; set; }

        /// <summary>
        /// 今日成交
        /// </summary>
        public decimal TodayCount { get; set; }

        /// <summary>
        /// 成交总金额
        /// </summary>
        public decimal TotalAmount { get; set; }
    }

    public class GetMallProductDto : PageRequest
    {
        /// <summary>
        /// 产品Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 产品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 状态 true: 上架   false: 未上架
        /// </summary>
        public bool? IsActive { get; set; }
    }

    public class MallProductDto
    {
        /// <summary>
        /// 产品id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string ShortDesc { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string FullDesc { get; set; }

        /// <summary>
        /// 现价
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal OldPrice { get; set; }

        /// <summary>
        /// 状态：
        /// * true 上架
        /// * false 下架
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }

    public class CreateMallProductDto
    {
        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string ShortDesc { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string FullDesc { get; set; }

        /// <summary>
        /// 现价
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal OldPrice { get; set; }

        /// <summary>
        /// 状态：
        /// * true 上架
        /// * false 下架
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 商品组成项
        /// </summary>
        public List<CreateMallProductItemDto> MallProductItemDtos { get; set; }
    }

    public class CreateMallProductItemDto
    {
        /// <summary>
        /// 分类 1 案例 2 月度任务
        /// </summary>
        public int Category { get; set; }

        /// <summary>
        /// 标的物id
        /// </summary>
        public long TargetId { get; set; }

        /// <summary>
        /// 来源 （1.云实习真账实操 2.课程同步实训圆梦造纸 3.业税财实训）
        /// </summary>
        public int Source { get; set; }
    }

    public class DeleteMallProductDto
    {
        /// <summary>
        /// 商品id列表
        /// </summary>
        public long[] Ids { get; set; }
    }
}
