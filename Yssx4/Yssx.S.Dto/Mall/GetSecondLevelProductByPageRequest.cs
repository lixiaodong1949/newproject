﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取二级分类下商品列表 - 入参
    /// </summary>
    public class GetSecondLevelProductByPageRequest : PageRequest
    {
        /// <summary>
        /// 二级分类ID
        /// </summary>
        public long Id { get; set; }

    }


    /// <summary>
    /// 获取二级分类下商品列表 - 输出
    /// </summary>
    public class GetSecondLevelProductByPageDto
    {
        /// <summary>
        /// 商品ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 现价
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal OldPrice { get; set; }

        /// <summary>
        /// 销量
        /// </summary>
        public int SalesVolume { get; set; }

        /// <summary>
        /// 状态：
        /// * true 上架
        /// * false 下架
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 商品分类ID（多个分类以,分隔）
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// 商品分类名称（多个分类以,分隔）
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public string UpdateByName { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

    }

}
