﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取公司案例列表实体
    /// </summary>
    public class GetCompanyCaseListDto
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string ShortDesc { get; set; }

        /// <summary>
        /// 公司logo
        /// </summary>
        public string LogoUrl { get; set; }

        /// <summary>
        /// 阅读数
        /// </summary>
        public int ReadNumber { get; set; }
    }
}
