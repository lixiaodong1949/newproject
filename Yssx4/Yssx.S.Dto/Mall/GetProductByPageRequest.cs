﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取商品列表 - 入参
    /// </summary>
    public class GetProductByPageRequest : PageRequest
    {
        /// <summary>
        /// 状态（-1.全部 0.下架 1.上架）
        /// </summary>
        public int IsActive { get; set; } = -1;

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 商品ID
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 商品分类ID
        /// </summary>
        public long Category { get; set; }

        /// <summary>
        /// 创建时间 - 起
        /// </summary>
        public DateTime? CreateTimeBegin { get; set; }

        /// <summary>
        /// 创建时间 - 止
        /// </summary>
        public DateTime? CreateTimeEnd { get; set; }

        /// <summary>
        /// 销量区间 - 最低
        /// </summary>
        public int SalesVolumeLow { get; set; }

        /// <summary>
        /// 销量区间 - 最高
        /// </summary>
        public int SalesVolumeHigh { get; set; }

        /// <summary>
        /// 价格区间 - 最低
        /// </summary>
        public decimal PriceLow { get; set; }

        /// <summary>
        /// 价格区间 - 最高
        /// </summary>
        public decimal PriceHigh { get; set; }


    }

    /// <summary>
    /// 获取商品列表 - 输出
    /// </summary>
    public class GetProductByPageDto
    {
        /// <summary>
        /// 商品ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string ShortDesc { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string FullDesc { get; set; }

        /// <summary>
        /// 现价
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal OldPrice { get; set; }

        /// <summary>
        /// 销量
        /// </summary>
        public int SalesVolume { get; set; }

        /// <summary>
        /// 状态：
        /// * true 上架
        /// * false 下架
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 商品分类ID（多个分类以,分隔）
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// 商品分类名称（多个分类以,分隔）
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public string UpdateByName { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

    }



}
