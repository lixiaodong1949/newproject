﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto.Mall
{
    /// <summary>
    /// 商品查询
    /// </summary>
    public class QueryProductsRequestDto : PageRequest
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keyword { get; set; }
        /// <summary>
        /// 一级分类Id
        /// </summary>
        public long FirstCategoryId { get; set; }
        /// <summary>
        /// 二级分类Id
        /// </summary>
        public long SecondCategoryId { get; set; }
    }
}
