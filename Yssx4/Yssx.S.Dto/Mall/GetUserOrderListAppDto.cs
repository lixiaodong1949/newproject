﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询用户订单列表
    /// </summary>
    public class GetUserOrderListAppDto
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 产品Id
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// 产品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 订单实付金额
        /// </summary>
        public decimal OrderTotal { get; set; }

        /// <summary>
        /// 优惠总金额
        /// </summary>
        public decimal OrderDiscount { get; set; }

        /// <summary>
        /// 状态（0.未支付 1.已取消 2.已付款）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 订单状态（0.未支付 1.已取消 2.已付款 3.已关闭）
        /// </summary>
        public int OrderStatus
        {
            get
            {
                var dtNow = DateTime.Now;
                if (Status == 0 && dtNow > CreateTime.AddMinutes(30))
                    return 3;
                else
                    return Status;
            }
        }

        /// <summary>
        /// 下单时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }

    /// <summary>
    /// 查询用户订单列表 - 入参
    /// </summary>
    public class GetUserOrderListAppRequest : PageRequest
    {
        /// <summary>
        /// 搜索内容
        /// </summary>
        public string SearchName { get; set; }

        /// <summary>
        /// 交易状态（-1.全部 0.未支付 1.已取消 2.已付款 3.已关闭）
        /// </summary>
        public int Status { get; set; } = -1;

    }


}
