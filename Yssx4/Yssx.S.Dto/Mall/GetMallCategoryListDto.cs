﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取商品分类列表 - 输出
    /// </summary>
    public class GetMallCategoryListDto
    {
        /// <summary>
        /// 分类ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 副标题
        /// </summary>
        public string Subtitle { get; set; }

        /// <summary>
        /// 排序
        /// </summary> 
        public int Sort { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 布局类型
        /// * 0 九宫格带图（通用）
        /// * 1 横版带图
        /// * 2 横版不带图
        /// </summary>
        public int LayoutType { get; set; }

        /// <summary>
        /// 二级分类明细
        /// </summary>
        public List<GetMallCategoryListDetailDto> ItemDetail { get; set; }

    }

    /// <summary>
    /// 获取商品分类列表 - 二级分类
    /// </summary>
    public class GetMallCategoryListDetailDto
    {
        /// <summary>
        /// 商品分类ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary> 
        public int Sort { get; set; }

        /// <summary>
        /// 商品数量
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
