﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Mall
{
    /// <summary>
    /// 购买后自动生成试卷 - 入参
    /// </summary>
    public class AutoGenerateTestPaperDto
    {
        /// <summary>
        /// 分类(1.案例 2.月度任务)
        /// </summary>
        public int Category { get; set; }

        /// <summary>
        /// 标的物ID
        /// </summary>
        public long TargetId { get; set; }

        /// <summary>
        /// 目标用户ID
        /// </summary>
        public long UserId { get; set; }
    }
}
