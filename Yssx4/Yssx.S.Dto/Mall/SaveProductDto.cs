﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 保存产品
    /// </summary>
    public class SaveProductDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string ShortDesc { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string FullDesc { get; set; }

        /// <summary>
        /// 现价
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal OldPrice { get; set; }

        /// <summary>
        /// 状态：
        /// * true 上架
        /// * false 下架
        /// </summary>
        public bool IsActive { get; set; } = false;

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 商品分类ID（多个分类以,分隔）
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// 开售状态（0.立即开售 1.定时开售）
        /// </summary> 
        public int SaleStatus { get; set; }

        /// <summary>
        /// 定时开售时间
        /// </summary>
        public DateTime? RegularSaleTime { get; set; }

        /// <summary>
        /// 定时开售价格
        /// </summary>
        public decimal RegularSalePrice { get; set; }

        /// <summary>
        /// 保存产品 - 明细
        /// </summary>
        public List<SaveProductDetailDto> ItemDetail { get; set; }

    }

    /// <summary>
    /// 保存产品 - 明细
    /// </summary>
    public class SaveProductDetailDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }


        /// <summary>
        /// 产品id
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// 分类 1 案例 2 月度任务
        /// </summary>
        public int Category { get; set; }

        /// <summary>
        /// 标的物id
        /// </summary>
        public long TargetId { get; set; }

        /// <summary>
        /// 标的物名称
        /// </summary>
        public string TargetName { get; set; }

        /// <summary>
        /// 来源 （1.云实习真账实操 2.课程同步实训 3.业税财实训 4.云上实训之岗位实训 5.课程实训 6.经验案例 7.初级会计考证）
        /// </summary>
        public int Source { get; set; }
    }


}
