﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    public class StudentByClassDto
    {
        public List<long> Ids { get; set; }

        public string Name { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class StudentClassListDto
    {
        /// <summary>
        /// 
        /// </summary>
        public long UserId { get; set; }

        public string Name { get; set; }

        public string CollegeName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ClassName { get; set; }
    }
}
