﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 常见问题
    /// </summary>
    public class FAQInfoDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 问题分类ID
        /// </summary>
        public long FaqClassId { get; set; }

        /// <summary>
        /// 问题分类名称
        /// </summary>
        public string FaqClassName { get; set; }

        /// <summary>
        /// 角色分类（1.普通用户 2.教师 3.学生）
        /// </summary>
        public int RoleType { get; set; } = 0;

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; } = 0;

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadNumber { get; set; } = 0;

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

    }


    /// <summary>
    /// 常见问题 - 单个
    /// </summary>
    public class FAQInfoOneDto : FAQInfoDto
    {
        /// <summary>
        /// 上一篇ID
        /// </summary>
        public long Previous { get; set; }

        /// <summary>
        /// 上一篇标题
        /// </summary>
        public string PreviousTitle { get; set; }

        /// <summary>
        /// 下一篇ID
        /// </summary>
        public long Next { get; set; }

        /// <summary>
        /// 下一篇标题
        /// </summary>
        public string NextTitle { get; set; }

    }

}
