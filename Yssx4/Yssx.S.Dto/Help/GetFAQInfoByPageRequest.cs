﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询常见问题列表 - 入参
    /// </summary>
    public class GetFAQInfoByPageRequest : PageRequest
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 排序方式（0.默认 1.最新 2.热门）
        /// </summary>
        public int SortType { get; set; } = 0;

        /// <summary>
        /// 问题分类ID
        /// </summary>
        public long FaqClassId { get; set; }

    }
}
