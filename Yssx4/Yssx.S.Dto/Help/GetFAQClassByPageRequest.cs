﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询问题分类列表 - 入参
    /// </summary>
    public class GetFAQClassByPageRequest : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

    }
}
