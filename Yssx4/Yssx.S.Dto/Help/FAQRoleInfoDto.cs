﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 角色分类问题列表
    /// </summary>
    public class FAQRoleInfoDto
    {
        /// <summary>
        /// 普通用户问题列表
        /// </summary>
        public List<FAQClassDto> UserDetail { get; set; }

        /// <summary>
        /// 教师问题列表
        /// </summary>
        public List<FAQClassDto> TeacherDetail { get; set; }

        /// <summary>
        /// 学生问题列表
        /// </summary>
        public List<FAQClassDto> StudentDetail { get; set; }

    }
}
