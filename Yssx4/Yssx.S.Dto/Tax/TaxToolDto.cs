﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.Tax
{
    /// <summary>
    /// 个税工具
    /// </summary>
    public class TaxToolDto
    {
        /// <summary>
        /// 工号
        /// </summary>
        [Description("工号")]
        [JsonProperty("jobNum")]
        public string JobNum { get; set; }

        [Description("姓名")]
        [JsonProperty("userName")]
        public string UserName { get; set; }

        /// <summary>
        /// 证件类型
        /// </summary>
        [Description("证件类型")]
        [JsonProperty("idType")]
        public string IdType { get; set; }


        /// <summary>
        /// 证件号码
        /// </summary>
        [Description("证件号码")]
        [JsonProperty("idNum")]
        public string IdNum { get; set; }

        /// <summary>
        /// 特许权使用费所得
        /// </summary>
        [Description("特许权使用费所得")]
        [JsonProperty("salary")]

        public decimal Salary { get; set; }

        [Description("劳务报酬所得")]
        public decimal Salary1 { get; set; }

        [Description("稿酬所得")]
        public decimal Salary2 { get; set; }

        [Description("偶然所得")]
        public decimal Salary3 { get; set; }
        /// <summary>
        /// 应纳税额
        /// </summary>
        [Description("应纳税额")]
        [JsonProperty("taxAmountPayable")]
        public decimal TaxAmountPayable { get; set; }
    }

    public class TaxLeaseDto: TaxToolDto
    {
        /// <summary>
        /// 修缮费
        /// </summary>
        [Description("修缮费")]
        [JsonProperty("repairAmount")]

        public decimal RepairAmount { get; set; }

        [Description("财产租赁所得")]
        public decimal Salary4 { get; set; }
    }

    /// <summary>
    /// 工资，薪金模板
    /// </summary>
    public class TaxSalaryDto
    {
        /// <summary>
        /// 工号
        /// </summary>
        [Description("工号")]
        [JsonProperty("jobNum")]
        public string JobNum { get; set; }

        [Description("姓名")]
        [JsonProperty("userName")]
        public string UserName { get; set; }

        /// <summary>
        /// 证件类型
        /// </summary>
        [Description("证件类型")]
        [JsonProperty("idType")]
        public string IdType { get; set; }


        /// <summary>
        /// 证件号码
        /// </summary>
        [Description("证件号码")]
        [JsonProperty("idNum")]
        public string IdNum { get; set; }

        /// <summary>
        /// 基本工资
        /// </summary>
        [Description("基本工资")]
        [JsonProperty("baseSalary")]
        public decimal BaseSalary { get; set; }

        /// <summary>
        /// 岗位津贴
        /// </summary>
        [Description("岗位津贴")]
        [JsonProperty("subsidy")]
        public decimal Subsidy { get; set; }

        /// <summary>
        /// 绩效奖金
        /// </summary>
        [Description("绩效奖金")]
        [JsonProperty("bonus")]
        public decimal Bonus { get; set; }

        /// <summary>
        /// 收入总额
        /// </summary>
        [Description("收入总额")]
        [JsonProperty("totalAmount")]
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// 专项扣除
        /// </summary>
        [Description("专项扣除")]
        [JsonProperty("specialAmount")]
        public decimal SpecialAmount { get; set; }

        /// <summary>
        /// 专项附加扣除
        /// </summary>
        [Description("专项附加扣除")]
        [JsonProperty("sed")]
        public decimal Sed { get; set; }

        /// <summary>
        /// 应税收入
        /// </summary>
        [Description("应税收入")]
        [JsonProperty("taxableIncome")]
        public decimal TaxableIncome { get; set; }

        /// <summary>
        /// 应纳税额
        /// </summary>
        [Description("应纳税额")]
        [JsonProperty("taxAmountPayable")]
        public decimal TaxAmountPayable { get; set; }
    }

    public class TaxCalculatorDto
    {
        [JsonProperty("taxList")]
        public List<TaxToolDto> TaxList { get; set; }


        /// <summary>
        /// 工资，薪金
        /// </summary>
        [JsonProperty("taxSalarieList")]
        public List<TaxSalaryDto> TaxSalarieList { get; set; }

        /// <summary>
        /// 租赁
        /// </summary>
        [JsonProperty("leaseList")]
        public List<TaxLeaseDto> LeaseList { get; set; }
    }

    public class TaxCalculatorSignDto
    {
        //[JsonProperty("taxDto")]
        //public TaxToolDto TaxDto { get; set; }


        ///// <summary>
        ///// 工资，薪金
        ///// </summary>
        //[JsonProperty("taxSalarieDto")]
        //public TaxSalaryDto TaxSalarieDto { get; set; }

        ///// <summary>
        ///// 租赁
        ///// </summary>
        //[JsonProperty("leaseDto")]
        //public TaxLeaseDto LeaseDto { get; set; }

        /// <summary>
        /// 应纳税额
        /// </summary>
        [JsonProperty("taxAmountPayable")]
        public decimal TaxAmountPayable { get; set; }
    }
}
