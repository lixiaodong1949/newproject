﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取公共 - 题目清单
    /// </summary>
    public class GetPubQuestionByPageRequest : PageRequest
    {
        /// <summary>
        /// 搜索标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 题目来源（0.所有 1.云自营 2.其他）
        /// </summary>
        public int? DataSources { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public int? QuestionType { get; set; }
        /// <summary>
        /// 题目状态
        /// </summary>
        public int? Status { get; set; }
    }
}
