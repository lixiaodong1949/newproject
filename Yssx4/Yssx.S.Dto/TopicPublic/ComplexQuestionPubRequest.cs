﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 公共 - 复杂题型
    /// </summary>
    public class ComplexQuestionPubRequest : BaseQuestionPub
    {
        /// <summary>
        /// 题目附件
        /// </summary>
        public List<QuestionPubFile> QuestionFile { get; set; }
        /// <summary>
        /// 题目知识点
        /// </summary>
        public List<TopicPubKnowledgePointDto> KnowledgePointDetail { get; set; }
        /// <summary>
        /// 模板ID
        /// </summary>
        public string TemplateId { get; set; }
        /// <summary>
        /// 非完整性得分
        /// </summary>
        public decimal PartialScore { get; set; } = 0;
        /// <summary>
        /// 表格题-计算方式（按行计算、按列计算、按单元格平均计算、自由计算）
        /// </summary>
        public CalculationType CalculationType { get; set; } = CalculationType.None;
        /// <summary>
        /// 表格题-是否开启复制Excel数据（0 否 1是）
        /// </summary>
        public int IsCopy { get; set; } = CommonConstants.IsNoCopy;
        /// <summary>
        /// 表格题-是否开启复制大数据（0 否 1是）
        /// </summary>
        public int IsCopyBigData { get; set; } = CommonConstants.IsNoCopy;
        /// <summary>
        /// 表格题-是否无序（0 否 1是）
        /// </summary>
        public int IsDisorder { get; set; } = CommonConstants.IsNoDisorder;

    }
    /// <summary>
    /// 公共 - 多题型（单个题干-多个子题目）
    /// </summary>
    public class MainSubQuestionPubRequest : MainQuestionPub
    {
        /// <summary>
        /// 子题目
        /// </summary>
        public List<SubQuestionPub> SubQuestion { get; set; }
    }
    /// <summary>
    /// 公共 - 题目附件
    /// </summary>
    public class QuestionPubFile
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 链接地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
