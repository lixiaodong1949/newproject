﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取题目清单
    /// </summary>
    public class GetQuestionByPageRequest : PageRequest
    {
        /// <summary>
        /// 搜索标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 知识点ID
        /// </summary>
        public long? KnowledgePointId { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public int? QuestionType { get; set; }
        /// <summary>
        /// 题目状态
        /// </summary>
        public int? Status { get; set; }
        /// <summary>
        /// 开始日期
        /// </summary>
        public DateTime? BeginDate { get; set; }
        /// <summary>
        /// 结束日期
        /// </summary>
        public DateTime? EndDate { get; set; }
    }
}
