﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 
    /// </summary>
    public class ExamPaperGroupListView
    {
        /// <summary>
        /// 分组Id
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// 房间号
        /// </summary>
        public long GroupNo { get; set; }

        /// <summary>
        /// 作答信息id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public long CreateBy { get; set; }

        /// <summary>
        /// 创建人姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }



        /// <summary>
        /// 分组状态(0.已开始，等待成员加入；1.成员已满，等待房间创建人启动开始考试按钮（此时成员人有可能退出房间，创建人也能解散）；2.已完成，开始考试（成员不能再退出）；3.已结束 )
        /// </summary>
        public GroupStatus Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<ExamPaperGroupStudentView> ExamPaperGroupStudentList { get; set; }
    }
}
