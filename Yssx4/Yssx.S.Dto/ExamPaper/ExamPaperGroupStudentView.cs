﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Yssx.Framework;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 
    /// </summary>
    public class ExamPaperGroupStudentView
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 分组Id
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// 组员ID
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 组员名称
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 岗位Id
        /// </summary>
        public long PostionId { get; set; }

        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostionName { get; set; }

        /// <summary>
        /// 是否已准备完毕 
        /// </summary>
        public bool IsReady { get; set; }

        /// <summary>
        /// 剩余时间：秒
        /// </summary>
        public double LeftSeconds { get; set; }

        /// <summary>
        /// 交卷状态
        /// </summary>
        public StudentExamStatus StudentExamStatus { get; set; }

        /// <summary>
        /// 是否当前登陆人
        /// </summary>
        public bool IsSelf { get; set; } = false;

        /// <summary>
        /// 题目总数
        /// </summary>
        public int QuestionCount { get; set; }

        /// <summary>
        /// 已答题目总数
        /// </summary>
        public int QuestionAnswerCount { get; set; }

        /// <summary>
        /// 未答题目总数
        /// </summary>
        public int QuestionNoAnswerCount { get; set; }

        /// <summary>
        /// 考试开始及暂停时保存时间点 
        /// </summary>
        [JsonIgnore]
        public DateTime RecordTime { get; set; }

        /// <summary>
        /// 作答总用时
        /// </summary>
        [JsonIgnore]
        public double UsedSeconds { get; set; }

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }



    }
}
