﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 分组列表查询实体
    /// </summary>
    public class ExamPaperGroupQueryDto
    {
        /// <summary>
        /// 考试id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 房间号
        /// </summary>
        public long GroupNo { get; set; }

    }
}
