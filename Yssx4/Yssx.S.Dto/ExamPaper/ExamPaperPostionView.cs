﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 
    /// </summary>
    public class ExamPaperPostionView
    {
        /// <summary>
        /// 考试id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 岗位Id
        /// </summary>
        public long PostionId { get; set; }

        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostionName { get; set; }
    }
}
