﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Yssx.Framework;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 试卷基本信息 
    /// </summary>
    public class ExamPaperBasicInfo
    {
        #region 考试基本信息

        /// <summary>
        /// 考试ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 作答记录ID
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 企业信息 
        /// </summary>
        public string EnterpriseInfo { get; set; }

        /// <summary>
        /// 学生作答状态
        /// </summary>
        public StudentExamStatus StudentExamStatus { get; set; }

        /// <summary>
        /// 考试总分
        /// </summary>
        public decimal ExamScore { get; set; }

        /// <summary>
        /// 合格分数
        /// </summary>
        public decimal PassScore { get; set; } = 60;

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 考试剩余时长
        /// </summary>
        public long LeftSeconds { get; set; }

        /// <summary>
        /// 作答总用时(秒)
        /// </summary>
        public long UsedSeconds { get; set; }

        #endregion

        /// <summary>
        /// 学生信息
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public StudentInfo StudentInfo { get; set; }

        /// <summary>
        /// 当前用户的组员信息
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ExamPaperGroupStudentView> ExamPaperGroupStudents { get; set; }

        /// <summary>
        /// 学生成绩信息
        /// </summary>
        public StudentGradeInfo StudentGradeInfo { get; set; }

        /// <summary>
        /// 试卷题目信息
        /// </summary>
        public List<QuestionInfo> QuestionInfoList { get; set; }
    }
}
