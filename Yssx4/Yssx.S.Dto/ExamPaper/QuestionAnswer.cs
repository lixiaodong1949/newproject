﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Yssx.Framework;

namespace Yssx.S.Dto.ExamPaper
{
    /// <summary>
    /// 答题信息
    /// </summary>
    public class QuestionAnswer
    {
        /// <summary>
        /// 作答记录主键
        /// </summary>
        public long GradeDetailId { get; set; }

        /// <summary>
        /// 作答ID
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 题目编号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目类型(后端标记多题型中的分录题，用于提交答案后返回多题型中的分录题状态)
        /// </summary>
        [JsonIgnore]
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 答题内容
        /// </summary>
        public string AnswerValue { get; set; }

        /// <summary>
        /// 分录题，制单人是否提交
        /// </summary>
        public bool IsSubmit { get; set; } = false;

        /// <summary>
        /// 是否撤销制单 
        /// </summary>
        public bool IsCancel { get; set; } = false;

        /// <summary>
        /// 是否退回到出纳（主管退回制单时使用 ） 
        /// </summary>
        public bool IsBackToCashier { get; set; } = false;

        /// <summary>
        /// 是否忽略（分录题盖章时供后端使用）
        /// </summary>
        [JsonIgnore]
        public bool IsIgnore { get; set; } = false;

        /// <summary>
        /// 多题型答案
        /// </summary>
        public List<QuestionAnswer> MultiQuestionAnswers { get; set; }

        /// <summary>
        /// 分录题，录入凭证数据。（只需要在第一级题目传入一次所有凭证数据即可，子题目不需要使用此值。）
        /// 非分录题不需要传此值
        /// </summary>
        public List<AccountEntryDTO> AccountEntryList { get; set; }

        /// <summary>
        /// 分录题状态
        /// </summary>
        [JsonIgnore]
        public AccountEntryStatus AccountEntryStatus { get; set; }

        /// <summary>
        /// 分录题审核状态
        /// </summary>
        [JsonIgnore]
        public AccountEntryAuditStatus AccountEntryAuditStatus { get; set; }
    }
}
