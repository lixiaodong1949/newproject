﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Statistics
{
    /// <summary>
    /// 平台资源统计
    /// </summary>
    public class ResourceStatisticsDto
    {
        /// <summary>
        /// 0:合作院校,1用户数,2:课程数,3:实训数,4:做题总数,5:任务发布数
        /// </summary>
        public int ResourseType { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public long Number { get; set; }
    }

    /// <summary>
    /// 用户统计数 每天活跃人数
    /// </summary>
    public class UserStatisticsDto
    {
        public string Date { get; set; }

        public long Number { get; set; }
    }

    public class SchoolUseStatis
    {
        public long Id { get; set; }

        public string Date { get; set; }

        public string SchoolName { get; set; }
        public long Number { get; set; }
    }

    /// <summary>
    /// 用户做题数 每天做题量
    /// </summary>
    public class StudentTopicDetailDto
    {
        public string Date { get; set; }

        public long Number { get; set; }
    }

    /// <summary>
    /// 每周任务发布数
    /// </summary>
    public class TaskStatisticsDto
    {
        public string Date { get; set; }

        public long Number { get; set; }
    }

    public class RegionDataDto
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public long Count { get; set; }

        public string Date { get; set; }
    }
}
