﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教材
    /// </summary>
    public class YssxSectionTextBookDto
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public long KnowledgePointId { get; set; }

        /// <summary>
        /// 节点Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
    }
}
