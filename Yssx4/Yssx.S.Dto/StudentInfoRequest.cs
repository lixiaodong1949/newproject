﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 修改学生姓名、学号入参
    /// </summary>
    public class StudentInfoRequest
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 真实姓名 （未修改传空值）
        /// </summary>
        public string RealName { get; set; }
        /// <summary>
        /// 学号（未修改传空值）
        /// </summary>
        public string StudentNo { get; set; }
    }

}
