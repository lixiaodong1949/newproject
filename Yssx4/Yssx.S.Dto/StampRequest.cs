﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 印章入参
    /// </summary>
    public class StampRequest : BaseDto
    {
        /// <summary>
        /// 印章名称
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 文件路径
        /// </summary>
        public string FileUrl { get; set; }

        /// <summary>
        /// 案例ID
        /// </summary>
        public long CaseId { get; set; }
    }
}
