using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 用户积分 返回值Dto
    /// </summary>
    public class IntegralPageResponseDto
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public string RealName { get; set; }
        public string UserName { get; set; }
        public string UserMobile { get; set; }
        public string Sex { get; set; }

        /// <summary>
        /// 总分
        /// </summary>  
        public long Number { get; set; }
        /// <summary>
        /// 已使用积分
        /// </summary>
        public long UsedNumber { get; set; }

        public DateTime UpdateTime { get; set; }
    }
}