using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取列表分页查询 请求Dto
    /// </summary>
    public class IntegralDetailsListPageRequestDto : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 用户Id 必填
        /// </summary>
        [Required]
        public long UserId { get; set; }
        /// <summary>
        /// 状态：1：收入，2消费
        /// </summary>
        public int StatusType { get; set; }
    }
}