using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 用户积分 Dto
    /// </summary>
    public class IntegralDetailsDto
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        /// <summary>
        /// 唯一Id(添加不传该参数)
        /// </summary>
        public long Id { get; set; }


        public long Number { get; set; }

        /// <summary>
        /// 状态：1：收入，2消费
        /// </summary>
        public int StatusType { get; set; }

        /// <summary>
        /// 来源 0:消费 1、每日登录
        ///2、答题
        ///3、关注公众号
        ///4、公众号设星标及置顶
        ///5、文章转载或阅读
        ///6、撰写文章
        ///7、推荐好友
        ///8、下载服务
        ///9、签到
        ///10、购买商品
        ///11、浏览商品
        /// </summary>
        public int FromType { get; set; }

        /// <summary>
        /// 收入和消费描述
        /// </summary>
        public string Desc { get; set; }

        /// <summary>
        /// 关联Id
        /// </summary>
        public long RelatedId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //public DateTime CreateTime { get; set; }
    }
}