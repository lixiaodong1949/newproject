﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    public class IntegralDetailsUserRequestDto : PageRequest
    {
        /// <summary>
        /// 用户Id 必填
        /// </summary>
        [JsonIgnore]
        public long UserId { get; set; }
        /// <summary>
        /// 状态：1：收入，2消费
        /// </summary>
        public int? StatusType { get; set; }
    }
}
