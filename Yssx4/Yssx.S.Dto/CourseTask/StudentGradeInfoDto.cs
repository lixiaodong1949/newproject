﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace Yssx.S.Dto
{
    /// <summary>
    ///  
    /// </summary>
    public class StudentGradeInfoDto
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }
        /// <summary>
        /// 题目明细
        /// </summary>
        public List<TaskTopicGradeDetailInfo> TopicGradeDetail { get; set; }


    }
    /// <summary>
    /// 题目对错信息
    /// </summary>
    public class TaskTopicGradeDetailInfo 
    {
        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 答题状态
        /// </summary>
        public AnswerResultStatus Status { get; set; }
    }
}
