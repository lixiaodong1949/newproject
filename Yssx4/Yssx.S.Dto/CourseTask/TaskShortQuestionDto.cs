﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 案例分析题列表dto
    /// </summary>
    public class TaskShortQuestionDto
    {
        /// <summary>
        /// 任务总人数
        /// </summary>
        public long TaskStudentCount { get; set; }
        /// <summary>
        /// 题目列表
        /// </summary>
        public List<ShortQuestionInfo> ShortQuestions { get; set; }
        

    }

    public class ShortQuestionInfo 
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 题目名称
        /// </summary>
        public string QuestionName { get; set; }

        /// <summary>
        /// 分值
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 已批阅人数
        /// </summary>
        public int CheckedCount { get; set; }

    }

}
