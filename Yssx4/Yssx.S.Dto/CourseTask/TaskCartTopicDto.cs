﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 任务篮习题Dto
    /// </summary>
    public class TaskCartTopicDto
    {
        /// <summary>
        /// 题型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 题目名
        /// </summary>
        public string QuetionName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long QuestionId { get; set; }


        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
    }
}
