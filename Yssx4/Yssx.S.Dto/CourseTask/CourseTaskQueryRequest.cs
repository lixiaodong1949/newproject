﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询课业任务列表入参
    /// </summary>
    public class CourseTaskQueryRequest : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 课程ID
        /// </summary>
        public long CourseId { get; set; } = 0;
        /// <summary>
        /// 状态（-2.所有 0.未开始 1.进行中 2.已结束）
        /// </summary>
        public int Status { get; set; }

    }
}
