﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程习题查询实体
    /// </summary>
    public  class CourseTopicQuery: PageRequest
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public long CenterKnowledgePointId { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public string QuestionTypes { get; set; }

        /// <summary>
        /// 难度等级  1-5
        /// </summary>
        public string DifficultyLevel { get; set; }

    }
}
