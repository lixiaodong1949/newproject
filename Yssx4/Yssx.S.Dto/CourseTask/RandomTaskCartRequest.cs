﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 智能组卷生成题目入参
    /// </summary>
    public class RandomTaskCartRequest
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 题目知识点id（多个id用,隔开）
        /// </summary>
        public string CenterKnowledgePointIds { get; set; }
        /// <summary>
        /// 章节Id集合
        /// </summary>
        public string SectionIds { get; set; }
        /// <summary>
        /// 随机题型列表
        /// </summary>
        public List<RandomQuestionType> RandomQuestionTypes { get; set; }

    }
    /// <summary>
    /// 随机题型
    /// </summary>
    public class RandomQuestionType
    { 
        /// <summary>
        /// 随机题型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 随机数量
        /// </summary>
        public int QuestionCount { get; set; }
    }
}
