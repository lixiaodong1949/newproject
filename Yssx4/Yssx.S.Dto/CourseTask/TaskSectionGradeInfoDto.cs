﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace Yssx.S.Dto
{
    /// <summary>
    ///  章节习题分析Dto
    /// </summary>
    public class TaskSectionGradeInfoDto
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 题目名称
        /// </summary>
        public string QuestionName { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 知识点
        /// </summary>
        public string CenterKnowledgePointNames { get; set; }
        /// <summary>
        /// 总人数
        /// </summary>
        public long TotalCount { get; set; }
        /// <summary>
        /// 错误人数
        /// </summary>
        public int ErrorCount { get; set; }
        /// <summary>
        /// 完成人数
        /// </summary>
        public int FinishCount { get; set; }


    }
}
