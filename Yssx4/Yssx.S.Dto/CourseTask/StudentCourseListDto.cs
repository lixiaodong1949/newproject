﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课业任务列表视图 - 学生端
    /// </summary>
    public class StudentCourseListDto
    {
        /// <summary>
        /// 任务ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>        
        public string Name { get; set; }
        ///// <summary>
        ///// 任务类型（0.作业 1.任务 2.考试 3.课堂测验）
        ///// </summary>
        //public TaskType TaskType { get; set; }
        /// <summary>
        /// 课程ID
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime CloseTime { get; set; }
        /// <summary>
        /// 任务时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 作答总用时
        /// </summary>
        public double UsedSeconds { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 结束后是否可以查看试卷 false:不可以查看  true:可以查看 【默认为true】
        /// </summary>
        public bool IsCanCheckExam { get; set; }
        ///// <summary>
        ///// 任务类型名称
        ///// </summary> 
        //public string TaskTypeName
        //{
        //    get { return TaskType.GetDescription(); }
        //    set { }
        //}
        ///// <summary>
        ///// 试卷类型
        ///// </summary>
        //public CourseExamType ExamType
        //{
        //    get
        //    {
        //        return TaskType == TaskType.ClassExam ? CourseExamType.ClassExam : (TaskType == TaskType.ClassTask ? CourseExamType.ClassTask : CourseExamType.ClassHomeWork);
        //    }
        //}
        /// <summary>
        /// 状态
        /// </summary>
        public LessonsTaskStatus Status { get; set; }
        /// <summary>
        /// 试卷状态
        /// </summary>
        public LessonsTaskStatus ExamStatus
        {
            get
            {
                if (Status == LessonsTaskStatus.End)
                    return LessonsTaskStatus.End;
                var dateNow = DateTime.Now;
                if (dateNow < StartTime)
                    return LessonsTaskStatus.Wait;
                if (StartTime <= dateNow && dateNow < CloseTime)
                    return LessonsTaskStatus.Started;
                if (CloseTime <= dateNow)
                    return LessonsTaskStatus.End;
                return LessonsTaskStatus.Wait;
            }
        }
        /// <summary>
        /// 作答记录状态
        /// </summary>
        public StudentExamStatus GradeStatus { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 作答记录ID
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 错题数
        /// </summary>
        public int ErrorCount { get; set; }
        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamPaperScore { get; set; } = 0;
        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; } = 0;

        /// <summary>
        /// 结束之前是否允许查看解析
        /// </summary>
        public bool CanShowAnswerBeforeEnd { get; set; }

    }
}
