﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程习题
    /// </summary>
    public class CourseTaskTopicDto : BaseDto
    {

        /// <summary>
        /// 题目名
        /// </summary>
        public string QuestionName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long SectionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目知识点id（多个id用,隔开）
        /// </summary>
        public string CenterKnowledgePointIds { get; set; }
        /// <summary>
        /// 知识点名称集合
        /// </summary>
        public string CenterKnowledgePointNames { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public int QuestionType { get; set; }
        
        /// <summary>
        /// 题干内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>
        public int DifficultyLevel { get; set; }
        /// <summary>
        /// 是否在任务篮中
        /// </summary>
        public bool IsExist { get; set; }
    }
}
