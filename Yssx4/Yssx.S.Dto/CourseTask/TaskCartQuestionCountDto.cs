﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 任务篮弹窗Dto
    /// </summary>
    public class TaskCartQuestionCountDto
    {

        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 题目数量
        /// </summary>
        public int QuestionCount { get; set; }

        /// <summary>
        /// 题型对应分数
        /// </summary>
        public decimal TotalScore { get; set; }
        
    }
}
