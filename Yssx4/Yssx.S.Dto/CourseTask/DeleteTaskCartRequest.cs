﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 任务篮移除入参
    /// </summary>
    public class DeleteTaskCartRequest
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>
        public string QuestionIds { get; set; }
        
    }
}
