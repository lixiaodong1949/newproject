﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课业任务入参
    /// </summary>
    public class PublicCourseTaskDto
    {
        /// <summary>
        /// 任务ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>        
        public string Name { get; set; }
        /// <summary>
        /// 任务类型（0.作业 1.任务 2.考试 3.课堂测验）
        /// </summary>
        public TaskType TaskType { get; set; }
        /// <summary>
        /// 课程ID
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime CloseTime { get; set; }
        /// <summary>
        /// 任务时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 作答总用时
        /// </summary>
        public double UsedSeconds { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 结束后是否可以查看试卷 false:不可以查看  true:可以查看 【默认为true】
        /// </summary>
        public bool IsCanCheckExam { get; set; }
        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }
        /// <summary>
        /// 题目排序方式
        /// </summary>
        public TaskTopicSortType TaskTopicSortType { get; set; }
        /// <summary>
        /// 结束之前是否允许查看解析
        /// </summary>
        public bool CanShowAnswerBeforeEnd { get; set; }

        /// <summary>
        /// 班级Id列表
        /// </summary>
        public List<CourseTaskClassInfo> ClassInfos { get; set; }

        /// <summary>
        /// 题目明细
        /// </summary>
        public List<CourseTaskTopic> TopicDetail { get; set; }

    }
    /// <summary>
    /// 班级
    /// </summary>
    public class CourseTaskClassInfo
    { 
        public long ClassId { get; set; }

        public string ClassName { get; set; }
    }
    /// <summary>
    /// 课业任务 - 题目
    /// </summary>
    public class CourseTaskTopic
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 题目ID
        /// </summary>
        public long TopicId { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
    }

}
