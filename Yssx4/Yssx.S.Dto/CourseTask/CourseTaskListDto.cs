﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课业任务列表视图
    /// </summary>
    public class CourseTaskListDto
    {

        /// <summary>
        /// 任务Id
        /// </summary>
        public long Id { get; set; }
        ///// <summary>
        ///// 老师作答记录Id
        ///// </summary>
        //public long GradeId { get; set; }
        ///// <summary>
        ///// 老师试卷Id
        ///// </summary>
        //public long TeacherExamId { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime CloseTime { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public LessonsTaskStatus Status { get; set; }
        /// <summary>
        /// 试卷状态
        /// </summary>
        public LessonsTaskStatus ExamStatus
        {
            get
            {
                if (Status == LessonsTaskStatus.End)
                    return LessonsTaskStatus.End;
                var dateNow = DateTime.Now;
                if (dateNow < StartTime)
                    return LessonsTaskStatus.Wait;
                if (StartTime <= dateNow && dateNow < CloseTime)
                    return LessonsTaskStatus.Started;
                if (CloseTime <= dateNow)
                    return LessonsTaskStatus.End;
                return LessonsTaskStatus.Wait;
            }
        }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 是否存在案例分析题 true 存在  
        /// </summary>
        public bool IsExistShortQuestion { get; set; } = false;
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 结束后是否可以查看试卷 false:不可以查看  true:可以查看 【默认为true】
        /// </summary>
        public bool IsCanCheckExam { get; set; }

        /// <summary>
        /// 结束之前是否允许查看解析
        /// </summary>
        public bool CanShowAnswerBeforeEnd { get; set; }

        /// <summary>
        /// 班级明细
        /// </summary>
        public List<CourseTaskClassInfo> ClassInfos { get; set; }

    }
}
