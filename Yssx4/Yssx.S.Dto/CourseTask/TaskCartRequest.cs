﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 批量
    /// </summary>
    public class BatchTaskCartRequest
    { 
        /// <summary>
        /// 任务篮数据集合
        /// </summary>
        public List<TaskCartRequest> TaskCarts { get; set; }
    }
    /// <summary>
    /// 任务篮新增入参
    /// </summary>
    public class TaskCartRequest
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 题目名称
        /// </summary>
        public string QuestionName { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
    }
}
