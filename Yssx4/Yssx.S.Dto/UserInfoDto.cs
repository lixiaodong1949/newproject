﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 用户基础信息
    /// </summary>
    public class UserInfoDto
    {
        public string MobilePhone { get; set; }

        public long Id { get; set; }

        public long TenantId { get; set; }

        public string RealName { get; set; }

        public string WeChat { get; set; }

        public string NickName { get; set; }

        public string QQ { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        public string Photo { get; set; }

        public string SchoolName { get; set; }

        public string CollegeName { get; set; }

        public string ProfessionName { get; set; }

        public string ClassName { get; set; }

        public long ClassId { get; set; }

        public string StudentNo { get; set; }

        /// <summary>
        /// 过期日期(废弃)
        /// </summary>
        public DateTime TempExpiredDate { get; set; }

        /// <summary>
        /// 过期日期
        /// </summary>
        public string RealExpiredDate { get; set; }

    }

    public class PartnerDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Mobile { get; set; }

        /// <summary>
        /// 0：还未联系,1:已联系
        /// </summary>
        public int Status { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
