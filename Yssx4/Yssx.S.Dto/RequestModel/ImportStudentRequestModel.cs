﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导入学生模板
    /// </summary>
    public class ImportStudentRequestModel
    {
        /// <summary>
        /// 学号
        /// </summary>
        [Description("学号")]
        public string StudentNo { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [Description("姓名")]
        public string Name { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [Description("手机号")]
        public string MobilePhone { get; set; }
    }
}
