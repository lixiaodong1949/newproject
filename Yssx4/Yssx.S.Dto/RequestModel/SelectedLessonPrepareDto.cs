﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 选中备课请求实体
    /// </summary>
    public class SelectedLessonPrepareDto
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 课程来源 0:系统赠送 1:客户创建(购买) 2:上传私有
        /// </summary>
        public Nullable<int> CourseSource { get; set; }

        /// <summary>
        /// 课程订单主键Id
        /// </summary>
        public Nullable<long> OrderId { get; set; }

        /// <summary>
        /// 课程有效期
        /// </summary>
        public Nullable<DateTime> EffectiveDate { get; set; }
    }
}
