﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课课程绑定班级请求实体
    /// </summary>
    public class PrepareCourseClassForAppDto
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

    }
}
