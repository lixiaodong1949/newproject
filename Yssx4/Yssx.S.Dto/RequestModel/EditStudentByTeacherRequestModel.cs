﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师编辑学生信息请求实体
    /// </summary>
    public class EditStudentByTeacherRequestModel
    {
        /// <summary>
        /// 用户ID
        /// </summary> 
        public long StudentUserId { get; set; }

        /// <summary>
        /// 名称
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }
    }
}
