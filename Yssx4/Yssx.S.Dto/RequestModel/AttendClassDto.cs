﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 上课请求实体
    /// </summary>
    public class AttendClassDto
    {
        /// <summary>
        /// 上课课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 上课课程名称
        /// </summary> 
        public string CourseName { get; set; }

        /// <summary>
        /// 学期主键Id
        /// </summary>
        public Nullable<long> SemesterId { get; set; }

        /// <summary>
        /// 学期名称
        /// </summary>
        public string SemesterName { get; set; }

        /// <summary>
        /// 上课班级列表
        /// </summary>
        public List<AttendClassClassInfo> ClassList { get; set; } = new List<AttendClassClassInfo>();
    }


    /// <summary>
    /// 上课班级
    /// </summary>
    public class AttendClassClassInfo
    {
        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }
}
