﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师绑定班级下学生查询实体
    /// </summary>
    public class YssxTeacherClassStudentRequestModel
    {
        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }
        
        /// <summary>
        /// 关键字搜索(姓名/学号)
        /// </summary>
        public string Keyword { get; set; }
    }
}
