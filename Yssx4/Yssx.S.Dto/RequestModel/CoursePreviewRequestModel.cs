﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程预习请求实体
    /// </summary>
    public class CoursePreviewRequestModel
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 课程预习名称
        /// </summary>
        public string CoursePreviewName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 预习班级列表
        /// </summary>
        public List<PreviewClassRequestModel> ClassList = new List<PreviewClassRequestModel>();
    }

    /// <summary>
    /// 预习班级实体
    /// </summary>
    public class PreviewClassRequestModel
    {
        /// <summary>
        /// 班级主键
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }

}
