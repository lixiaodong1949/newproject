﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课课程绑定班级请求实体
    /// </summary>
    public class PrepareCourseClassDto
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 课程班级列表
        /// </summary>
        public List<ClassInfo> ClassList { get; set; } = new List<ClassInfo>();
    }

    /// <summary>
    /// 课程班级
    /// </summary>
    public class ClassInfo
    {
        /// <summary>
        /// 课程班级主键Id
        /// </summary>
        public Nullable<long> CourseClassId { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }
}
