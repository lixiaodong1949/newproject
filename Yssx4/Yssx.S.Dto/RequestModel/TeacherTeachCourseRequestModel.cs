﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师上课请求实体
    /// </summary>
    public class TeacherTeachCourseRequestModel
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 课程上课名称
        /// </summary>
        public string TeachCourseName { get; set; }

        /// <summary>
        /// 上课班级列表
        /// </summary>
        public List<TeachClassRequestModel> ClassList = new List<TeachClassRequestModel>();
    }

    /// <summary>
    /// 上课班级实体
    /// </summary>
    public class TeachClassRequestModel
    {
        /// <summary>
        /// 班级主键
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }

}
