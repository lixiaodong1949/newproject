﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// Excel导入学生请求实体
    /// </summary>
    public class ImportStudentExcelRequestModel
    {
        /// <summary>
        /// 班级Id
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 文件
        /// </summary>
        public IFormFile FormFile { get; set; }
    }
}
