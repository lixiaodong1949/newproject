﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课上课请求实体
    /// </summary>
    public class PlanningAttendClassRequestModel
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary> 
        public string CourseName { get; set; }
        
        /// <summary>
        /// 课程备课Id
        /// </summary>
        public Nullable<long> LessonPlanId { get; set; }

        /// <summary>
        /// 课程备课名称
        /// </summary> 
        public string LessonPlanName { get; set; }

        /// <summary>
        /// 学期主键Id
        /// </summary>
        public Nullable<long> SemesterId { get; set; }

        /// <summary>
        /// 上课班级列表
        /// </summary>
        public List<PlanningAttendClassClassInfo> ClassList { get; set; } = new List<PlanningAttendClassClassInfo>();
    }

    /// <summary>
    /// 上课班级
    /// </summary>
    public class PlanningAttendClassClassInfo
    {
        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }

}
