﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课课程章节场景实训请求实体
    /// </summary>
    public class PrepareSectionSceneTrainingDto
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long SceneTrainingId { get; set; }

        /// <summary>
        /// 知识点Id(冗余字段)
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 课程资源来源 1:客户单独购买 2:上传私有
        /// </summary>
        public Nullable<int> DataSourceType { get; set; }

        /// <summary>
        /// 订单主键Id(资源订单)
        /// </summary>
        public Nullable<long> OrderId { get; set; }

        /// <summary>
        /// 资源有效期
        /// </summary>
        public Nullable<DateTime> EffectiveDate { get; set; }
    }
}
