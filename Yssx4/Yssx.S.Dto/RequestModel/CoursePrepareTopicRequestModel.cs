﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课题目请求实体
    /// </summary>
    public class CoursePrepareTopicRequestModel
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 题目主键
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目名称
        /// </summary>
        public string QuestionName { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 知识点Ids
        /// </summary>
        public string KnowledgePointIds { get; set; }

        /// <summary>
        /// 知识点名称
        /// </summary>
        public string KnowledgePointNames { get; set; }

        /// <summary>
        /// 题目来源 0:课程 1:案例
        /// </summary>
        public CoursePrepareTopicSource TopicSource { get; set; }

        /// <summary>
        /// 选中课程或案例Id
        /// </summary>
        public long CourseOrCaseId { get; set; }

        /// <summary>
        /// 课程或案例名称
        /// </summary>
        public string CourseOrCaseName { get; set; }

        /// <summary>
        /// 0:实习 1:实训
        /// </summary>
        public Nullable<int> ModuleType { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
