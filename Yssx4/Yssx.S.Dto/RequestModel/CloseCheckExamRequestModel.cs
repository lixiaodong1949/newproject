﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 关闭学生查看试卷请求实体
    /// </summary>
    public class CloseCheckExamRequestModel
    {
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 结束后是否可以查看试卷 false:不可以查看  true:可以查看 【默认为true】
        /// </summary>
        public bool IsCanCheckExam { get; set; }
    }
}
