﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课请求实体
    /// </summary>
    public class LessonPlanningRequestModel
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 备课名称
        /// </summary>
        public string LessonPlanName { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 学生是否可看 0:禁止(不可看) 1:可看
        /// </summary>
        public int IsStudentLook { get; set; }

        /// <summary>
        /// 学期主键
        /// </summary>
        public Nullable<long> SemesterId { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }
    }
}
