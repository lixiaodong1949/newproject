﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课素材、习题排序请求实体
    /// </summary>
    public class LessonPlanningCdrAndTopicRequestModel
    {
        /// <summary>
        /// 主键Id (习题/素材)
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 资源类型 0:习题 1:素材
        /// </summary>
        public int ResourceType { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }
    }
}
