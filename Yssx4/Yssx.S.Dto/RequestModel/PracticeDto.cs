﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    #region 自主练习 传入参数
    /// <summary>
    ///4个实训
    /// </summary>
    public class Practice : PageRequest
    {
        /// <summary>
        /// 行业Id
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CypName { get; set; }

        /// <summary>
        /// 0学生首页 1老师首页
        /// </summary>
        public int Type { get; set; }
    }

    /// <summary>
    ///场景实训
    /// </summary>
    public class PracticeScene : PageRequest
    {

        /// <summary>
        /// 行业Id
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string CypName { get; set; }
    }

    /// <summary>
    ///课程实训
    /// </summary>
    public class PracticeCourse : PageRequest
    {
        /// <summary>
        /// null全部-1无效0中职1高职2本科3国开
        /// </summary>
        public int? Education { get; set; }

        /// <summary>
        /// 课程名称/作者
        /// </summary>
        public string CrName { get; set; }
    }
    #endregion

    #region 自主练习 返回参数
    ///// <summary>
    ///// 场景实训列表
    ///// </summary>
    //public class TrainingServicePageRequest : PageRequest
    //{
    //    /// <summary>
    //    /// 实训名称
    //    /// </summary>
    //    public string Name { get; set; }
    //    /// <summary>
    //    /// 公司名称
    //    /// </summary>
    //    public long? SelectCompanyId { get; set; }
    //    /// <summary>
    //    /// 业务场景
    //    /// </summary>
    //    public long? YwcjId { get; set; }
    //    /// <summary>
    //    /// 状态(0,待上架,1,上架待审,2,已上架,3,已拒绝,4,全部)
    //    /// </summary>
    //    public long? Status { get; set; }
    //    /// <summary>
    //    /// 来源(1,云自营,2,其他,3,全部)
    //    /// </summary>
    //    public int? LyId { get; set; }
    //    /// <summary>
    //    /// 1,资源管理—场景实训列表，2，专业组上传—场景实训列表，3，我的实训-用户实训列表
    //    /// </summary>
    //    public int? Type { get; set; }
    //    /// <summary>
    //    /// 通过选择(1,已通关,2,未通关,3,全部)
    //    /// </summary>
    //    public int Tgxz { get; set; }
    //}

    ///// <summary>
    ///// 场景实训
    ///// </summary>
    //public class BusinessTrainingList 
    //{
    //    /// <summary>
    //    /// 场景实训公司列表
    //    /// </summary>
    //    public BusinessTraining BusinessTraining { get; set; }
    //}

    /// <summary>
    /// 场景实训&公司列表
    /// </summary>
    public class BusinessTraining
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CpyName { get; set; }

        /// <summary>
        /// 公司背景图
        /// </summary>
        public string CpyImg { get; set; }

        /// <summary>
        /// 行业名称
        /// </summary>
        public string IdtName { get; set; }

        /// <summary>
        /// 公司描述(列表内容)
        /// </summary>
        public string CypDescription { get; set; }

        /// <summary>
        /// 业务情景
        /// </summary>
        public int BusinessScenario { get; set; }

        /// <summary>
        /// 情景实训
        /// </summary>
        public int SituationalTraining { get; set; }

        /// <summary>
        /// 场景实训详情
        /// </summary>
        public List<BusinessTrainingDto> BtDto { get; set; }
    }

    /// <summary>
    /// 场景实训详情
    /// </summary>
    public class BusinessTrainingDto
    {
        /// <summary>
        /// 业务实训名称 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 业务情景配图
        /// </summary>
        public string FileUrl { get; set; }
        /// <summary>
        /// 业务场景名称
        /// </summary>
        public string YwcjName { get; set; }
        /// <summary>
        /// 业务往来(列表内容)
        /// </summary>
        public string Ywwl { get; set; }


        /// <summary>
        /// 公司名称
        /// </summary>
        public string SelectCompanyName { get; set; }
        /// <summary>
        /// 公司行业名称
        /// </summary>
        public string IdtName { get; set; }
        /// <summary>
        /// 公司背景图
        /// </summary>
        public string CpyImg { get; set; }
        /// <summary>
        /// 公司描述(列表内容)
        /// </summary>
        public string CypDescription { get; set; }

        #region xxxxxxxx
        /// <summary>
        /// 选择公司Id
        /// </summary>
        public long SelectCompanyId { get; set; }
        /// <summary>
        /// 业务场景
        /// </summary>
        public long YwcjId { get; set; }
        /// <summary>
        /// 排序序号
        /// </summary>
        public long Pxxh { get; set; }
        /// <summary>
        /// 流程信息
        /// </summary>
        //public long LcxxId { get; set; }
        /// <summary>
        /// 作者寄语
        /// </summary>
        public string Zzjy { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Describe { get; set; }
        /// <summary>
        /// 审核状态(0,待审核，1，审核成功，2，审核拒绝)
        /// </summary>
        public int AuditStatus { get; set; } = 0;

        /// <summary>
        /// 审核说明
        /// </summary>
        public string AuditStatusExplain { get; set; }

        /// <summary>
        /// 是否上架(0,待上架，1，上架待审，2，上架)
        /// </summary>
        public int Uppershelf { get; set; } = 0;

        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; }

        /// <summary>
        /// 状态(0,待上架,1,上架待审,2,已上架,3,已拒绝)
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 使用次数
        /// </summary>
        public int UseNumber { get; set; }
        /// <summary>
        /// 上架人
        /// </summary>
        public string NikeName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
        /// <summary>
        /// 通关次数
        /// </summary>
        public long Tgcs { get; set; }
        /// <summary>
        /// 购买时间
        /// </summary>
        public Nullable<DateTime> PaymentTime { get; set; }
        /// <summary>
        /// 当前进度(0,未开始,1,进行中)
        /// </summary>
        public int Dqjd { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<DateTime> CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 实训记录表Id
        /// </summary>
        public long YssxProgressId { get; set; }
        /// <summary>
        /// 场景实训购买Id
        /// </summary>
        public long SceneTrainingOrderId { get; set; }
        /// <summary>
        /// 当前步数
        /// </summary>
        public int Dqbs { get; set; }
        /// <summary>
        /// 总共步数
        /// </summary>
        public int Zgbs { get; set; }
        #endregion
    }

    /// <summary>
    /// 课程实训
    /// </summary>
    public class CourseInfoList
    {
        public long Id { get; set; }

        /// <summary>
        /// 课程ID
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; set; }
        /// <summary>
        /// 学历标识 -1无效0中职1高职2本科3国开
        /// </summary>
        public int Education { get; set; }
        /// <summary>
        /// 章节题
        /// </summary>
        public string Chapter { get; set; }


        /// <summary>
        /// 课程类型0:常规课
        /// </summary>
        public int CourseType { get; set; }
        /// <summary>
        /// 总题数
        /// </summary>
        public int QuestionCount { get; set; }
        /// <summary>
        /// 做题数
        /// </summary>
        public int DoQuestionCount { get; set; }
        /// <summary>
        /// 错题数
        /// </summary>
        public int ErrorCount { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 是否购买
        /// </summary>
        public bool IsBuy { get; set; }
    }
    #endregion

    #region 其他
    /// <summary>
    /// 行业下拉列表--返参
    /// </summary>
    public class IndustryDetails
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
    #endregion

    #region 试卷实训配置
    /// <summary>
    ///岗位实训--入参
    /// </summary>
    public class PositionPracticeDto
    {
        ///// <summary>
        ///// 0学生首页 1老师首页
        ///// </summary>
        //public int Type { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary>
        public long ClassId { get; set; }
    }

    /// <summary>
    /// 岗位列表--返参
    /// </summary>
    public class PositionPracticeInfo
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 实训Id
        /// </summary>
        public long PracticeId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 所属行业
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 实训分类  0：会计实训,1:出纳实训,2：大数据,3:业财税 4:中职高考冲刺模拟题
        /// </summary>
        public int SxType { get; set; }

        /// <summary>
        /// 题目 true选中 false未选中
        /// </summary>
        public bool State { get; set; }

        /// <summary>
        /// 答案 true选中 false未选中
        /// </summary>
        public bool AnswerState { get; set; }
    }

    /// <summary>
    ///绑定班级--入参
    /// </summary>
    public class BindClassDto
    {
        ///// <summary>
        ///// 案例Id/课程Id
        ///// </summary>
        //public long Id { get; set; }
    }

    /// <summary>
    /// 绑定班级列表--返参
    /// </summary>
    public class BindClassInfo
    { 
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string Name {get; set; }

        ///// <summary>
        ///// true选中 false未选中
        ///// </summary>
        //public bool State { get; set; }
    }

    /// <summary>
    /// 岗位/课程实训班级--入参
    /// </summary>
    public class PositionBindClassDto
    {
        /// <summary>
        /// 班级Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 类别 0题目 1答案
        /// </summary>
        public int AnswerType { get; set; }

        /// <summary>
        /// 实训信息
        /// </summary>
        public List<PracticeDto>  Practice { get; set; }

        #region
        ///// <summary>
        ///// 班级Id
        ///// </summary>
        //public long Id { get; set; }

        ///// <summary>
        ///// 岗位Id集合/课程章Id集合
        ///// </summary>
        //public List<long> Ids { get; set; }

        ///// <summary>
        ///// 实训 0 岗位 1课程
        ///// </summary>
        //public int Type { get; set; }

        ///// <summary>
        ///// 答案类别 0 题目 1答案
        ///// </summary>
        //public int AnswerType { get; set; }

        ///// <summary>
        ///// 课程Id
        ///// </summary>
        //public long CourseId { get; set; }
        #endregion

        #region
        ///// <summary>
        ///// 岗位/课程章主键Id
        ///// </summary>
        //public long Id { get; set; }

        ///// <summary>
        ///// 班级Id集合
        ///// </summary>
        //public List<long> Ids { get; set; }
        #endregion
    }

    /// <summary>
    /// 实训信息
    /// </summary>
    public class PracticeDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 实训Id
        /// </summary>
        public long PracticeId { get; set; }

        /// <summary>
        /// 新实训实习名称
        /// </summary>
        public string PracticeName { get; set; }

        /// <summary>
        /// 题目 true选中 false未选中
        /// </summary>
        public bool State { get; set; }

        /// <summary>
        /// 答案 true选中 false未选中
        /// </summary>
        public bool AnswerState { get; set; }
    }

    /// <summary>
    /// 课程实训列表--返参
    /// </summary>
    public class CoursePracticeInfo
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 课程章--返参
    /// </summary>
    public class CourseChapterInfo
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 实训Id
        /// </summary>
        public long PracticeId { get; set; }

        /// <summary>
        /// 章名称
        /// </summary>
        public string SectionName { get; set; }
        
        /// <summary>
        /// 节名称
        /// </summary>
        public string SectionTitle { get; set; }
        
        /// <summary>
        /// 题目 true选中 false未选中
        /// </summary>
        public bool State { get; set; }

        /// <summary>
        /// 答案 true选中 false未选中
        /// </summary>
        public bool AnswerState { get; set; }
    }

    /// <summary>
    /// 课程实训班级--入参
    /// </summary>
    public class CourseBindClassDto
    {
        /// <summary>
        /// 章主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 班级Id集合
        /// </summary>
        public List<long> Ids { get; set; }
    }

    /// <summary>
    /// 自主实训操作记录列表-返参
    /// </summary>
    public class OperationRecordInfo
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 班级
        /// </summary>
        public string Class { get; set; }

        /// <summary>
        /// 操作
        /// </summary>
        public string Operation { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 账户
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }
    }

    /// <summary>
    /// 自主实训操作记录列表-入参
    /// </summary>
    public class OperationRecordDto : PageRequest
    {
        /// <summary>
        /// 0岗位实训 1课程实训 2新实训实习
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 关键字 名称 班级
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }
    }

    /// <summary>
    /// 未绑定班级列表-返参
    /// </summary>
    public class UnboundClassInfo
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 未绑定班级-入参
    /// </summary>
    public class UnboundClassDto
    {
        /// <summary>
        /// 班级Id
        /// </summary>
        public long Id { get; set; }
    }
    #endregion
}
