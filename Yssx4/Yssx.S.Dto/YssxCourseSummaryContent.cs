﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程概要
    /// </summary>
    public class YssxSectionSummaryContent
    {
        /// <summary>
        /// 类型
        /// </summary>
        public int SummaryType { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string SummaryName { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Count { get; set; }
    }
}
