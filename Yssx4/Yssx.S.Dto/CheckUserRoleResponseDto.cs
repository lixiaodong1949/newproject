﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Authorizations;

namespace Yssx.S.Dto
{
    public class CheckUserRoleResponseDto
    {
        /// <summary>
        /// 用户类型是否改变
        /// </summary>
        public bool UserTypeChange { get; set; }

        /// <summary>
        /// 用户票据
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public UserTicket UserTicket { get; set; }
    }
}
