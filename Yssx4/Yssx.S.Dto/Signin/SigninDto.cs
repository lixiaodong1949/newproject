using System;
using System.ComponentModel.DataAnnotations;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 签到 Dto
    /// </summary>
    public class SigninDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>        
        [Required(ErrorMessage ="名称[Name]不能为空")]
        public string Name { get; set; }
    }
}