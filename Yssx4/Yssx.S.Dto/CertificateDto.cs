﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 凭证数据对象
    /// </summary>
    public class CertificateDto
    {
        /// <summary>
        /// 科目Id
        /// </summary>
        public long SubjectId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long ParentSubjectId { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public string DateStr { get; set; }
        /// <summary>
        /// 科目编码
        /// </summary>
        public long SubjectCode { get; set; }
        /// <summary>
        /// 凭证记号
        /// </summary>
        public string CertificateNo { get; set; }
        /// <summary>
        /// 科目名称
        /// </summary>

        public string SubjectName { get; set; }

        /// <summary>
        /// 摘要
        /// </summary>
        public string SummaryInfo { get; set; }
        ///// <summary>
        ///// 科目类型(1:资产,2:负债)
        ///// </summary>
        //public int SubjectType { get; set; }

        /// <summary>
        /// 余额方向(借,贷)
        /// </summary>
        public string Direction { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public int SubjectType { get; set; }


        /// <summary>
        /// 期间
        /// </summary>
        public string Stage { get; set; }
        /// <summary>
        /// 借方金额  (如果是初期余额、本期合计、本年合计，数据来自科目表。否则来在凭证记录数据表)
        /// </summary>
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额  (如果是初期余额、本期合计、本年合计，数据来自科目表。否则来在凭证记录数据表)
        /// </summary>
        public decimal CreditorAmount { get; set; }


        /// <summary>
        /// 凭证日期
        /// </summary>
        public DateTime CertificateDate { get; set; }
        /// <summary>
        /// 余额
        /// </summary>
        public decimal Balance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CreditorAmountStr
        { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BorrowAmountStr
        {
            get; set;
        }

        /// <summary>
        /// 标识
        /// </summary>
        public bool Flag { get; set; }
    }
}
