﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 人民币汇率指数
    /// </summary>
    public class RMBExchangRateIndexDto
    {
        [Description("日期")]
        public string Rq { get; set; }

        [Description("CFETS人民币汇率指数")]
        public string CFETS_Rmbhlzs { get; set; }

        [Description("BIS货币篮子人民币汇率指数")]
        public string BIS_Hblzrmbhlzs { get; set; }

        [Description("SDR货币篮子人民币汇率指数")]
        public string SDR_Hblzrmbhlzs { get; set; }
    }
}
