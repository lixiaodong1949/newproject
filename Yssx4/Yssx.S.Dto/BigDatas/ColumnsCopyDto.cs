﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 列一键复制
    /// </summary>
    public class ColumnsCopyDto
    {
        //string examId, string gradeId, string coseId, string questionId
        

        /// <summary>
        /// 试卷id
        /// </summary>
        public string ExamId { get; set; }
        /// <summary>
        /// 关联id
        /// </summary>
        public string GradeId { get; set; }
        /// <summary>
        /// 案例id
        /// </summary>
        public string CoseId { get; set; }
        /// <summary>
        /// 题目id
        /// </summary>
        public string QuestionId { get; set; }  
        /// <summary>
        /// 0,大列表复制，1，搜索列表复制
        /// </summary>
        public int TypeId { get; set; }
        /// <summary>
        /// 表id(1,大数据负债，2，活跃债券统计，3，年金终值系数，4，年金现值系数，5，A股指数0000026，6，金融机构人民币存款基准利率,7,金融机构人民币贷款基准利率,
        /// 8,国债,9,B股指数000003,10,成份B股指数390003, 11,复利终值系数，12，企业债，13，披露财务指标，14，LPR数据，
        /// 15，LPR均值数据，16，中期票据(AAA)2017，17，其他指数，18，政策性金融债（国开行）2017，19，政策性金融债（进出口行、开发行）2017，20，利润表，21，红利分配表，
        /// 22，人民币参考汇率_2014，23，人民币参考汇率_2015，24，人民币参考汇率_2016，25，人民币参考汇率_2017，26，人民币参考汇率_2018，27，人民币汇率指数，28，深证成份指数390001，
        /// 29，深证综指399106，30，深证成指R390002，31，Shibor数据， 32，Shibor均值数据，33，Shibor报价数据，34，短期融资债2017，35，统计数据CPI，
        /// 36，人民币月平均汇率，37，人民币汇率中间价，38，创业板指数399006，39，复利现值系数，40，沪深300指数399300，41，上证综指000001，42，国债2017)
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 筛选字段中文名集合
        /// </summary>
        public List<string> Cols { get; set; }
        /// <summary>
        /// 筛选字段英文名集合
        /// </summary>
        public List<string> Ecols { get; set; }
        /// <summary>
        /// 查询条件集合
        /// </summary>
        public List<Row> Rows { get; set; }
    }
}
