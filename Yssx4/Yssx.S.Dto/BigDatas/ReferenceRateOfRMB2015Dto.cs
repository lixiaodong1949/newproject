﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 人民币参考汇率_2015
    /// </summary>
    public class ReferenceRateOfRMB2015Dto
    {
        [Description("币种")]
        public string Bz { get; set; }

        [Description("12月")]
        public string Years2015_12 { get; set; }

        [Description("11月")]
        public string Years2015_11 { get; set; }

        [Description("10月")]
        public string Years2015_10 { get; set; }

        [Description("9月")]
        public string Years2015_09 { get; set; }

        [Description("8月")]
        public string Years2015_08 { get; set; }

        [Description("7月")]
        public string Years2015_07 { get; set; }

        [Description("6月")]
        public string Years2015_06 { get; set; }

        [Description("5月")]
        public string Years2015_05 { get; set; }

        [Description("4月")]
        public string Years2015_04 { get; set; }

        [Description("3月")]
        public string Years2015_03 { get; set; }

        [Description("2月")]
        public string Years2015_02 { get; set; }

        [Description("1月")]
        public string Years2015_01 { get; set; }
    }
}
