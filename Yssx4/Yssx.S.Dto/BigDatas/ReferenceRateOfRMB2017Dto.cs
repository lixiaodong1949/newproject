﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 人民币参考汇率_2017
    /// </summary>
    public class ReferenceRateOfRMB2017Dto
    {
        [Description("币种")]
        public string Bz { get; set; }

        [Description("12月")]
        public string Years2017_12 { get; set; }

        [Description("11月")]
        public string Years2017_11 { get; set; }

        [Description("10月")]
        public string Years2017_10 { get; set; }

        [Description("9月")]
        public string Years2017_09 { get; set; }

        [Description("8月")]
        public string Years2017_08 { get; set; }

        [Description("7月")]
        public string Years2017_07 { get; set; }

        [Description("6月")]
        public string Years2017_06 { get; set; }

        [Description("5月")]
        public string Years2017_05 { get; set; }

        [Description("4月")]
        public string Years2017_04 { get; set; }

        [Description("3月")]
        public string Years2017_03 { get; set; }

        [Description("2月")]
        public string Years2017_02 { get; set; }

        [Description("1月")]
        public string Years2017_01 { get; set; }
    }
}
