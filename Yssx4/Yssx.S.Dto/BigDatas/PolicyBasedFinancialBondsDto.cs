﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 政策性金融债（国开行）2017
    /// </summary>
    public class PolicyBasedFinancialBondsDto
    {
        [Description("日期")]
        public string Rq { get; set; }

        [Description("标准期限(年)")]
        public string Bzqx { get; set; }

        [Description("基准债券")]
        public string Jzzj { get; set; }

        [Description("剩余期限(年)")]
        public string Syqx { get; set; }

        [Description("报买入收益率(%)")]
        public string Bmrsyl { get; set; }

        [Description("报卖出收益率(%)")]
        public string Bmcsyl { get; set; }
    }
}
