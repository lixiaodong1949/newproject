﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 金融机构人民币存款基准利率
    /// </summary>
    public class BenchmarkRMBDepositRateOfFinancialInstitutionsDto
    {
        [Description("调整时间")]
        public string Tzsj { get; set; }

        [Description("活期存款(%)")]
        public string Hqck { get; set; }

        [Description("三个月定期存款(%)")]
        public string Sgydqck { get; set; }

        [Description("半年定期存款(%)")]
        public string Bndqck { get; set; }

        [Description("一年定期存款(%)")]
        public string Yndqck { get; set; }

        [Description("二年定期存款(%)")]
        public string Lndqck { get; set; }

        [Description("三年定期存款(%)")]
        public string Sndqck { get; set; }

        [Description("五年定期存款(%)")]
        public string Wndqck { get; set; }
    }
}
