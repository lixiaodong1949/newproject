﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 金融机构人民币贷款基准利率
    /// </summary>
    public class BenchmarkRMBLendingRateForFinancialInstitutionsDto
    {
        [Description("调整时间")]
        public string Tzsj { get; set; }

        [Description("六个月以内（含六个月）(%)")]
        public string Lgyyn { get; set; }

        [Description("六个月至一年（含一年）(%)")]
        public string Lgyzyn { get; set; }

        [Description("一至三年（含三年）(%)")]
        public string Yzsn { get; set; }

        [Description("三至五年（含五年）(%)")]
        public string Szwn { get; set; }

        [Description("五年以上(%)")]
        public string Wnys { get; set; }
    }
}
