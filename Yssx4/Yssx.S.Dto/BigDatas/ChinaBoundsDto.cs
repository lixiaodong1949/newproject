﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 中国国债收益率表
    /// </summary>
    public class ChinaBoundsDto
    {
        [Description("统计日期")]
        public string Tjrq { get; set; }

        [Description("1年期国债收益率(%)")]
        public string Ynqgzsyl { get; set; }

        [Description("10年期国债收益率(%)")]
        public string Snqgzsyl { get; set; }
    }
}
