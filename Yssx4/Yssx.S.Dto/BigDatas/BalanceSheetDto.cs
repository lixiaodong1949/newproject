﻿using FreeSql.DataAnnotations;
using System.ComponentModel;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 大数据负债
    /// </summary>
    [Table(Name = "yssx_bigData_balanceSheet")]
    public class BalanceSheetDto
    {

        [Description("证券简称")]
        public string SecurityShort { get; set; }

        [Description("行业名称")]
        public string Name { get; set; }

        [Description("公司名称")]
        public string CompanyName { get; set; }

        [Description("证券代码")]
        public string SecurityCode { get; set; }

        private string accountPeriod;

        [Description("会计期间")]
        public string AccountPeriod
        {
            get { return accountPeriod; }
            set
            {
                accountPeriod = value;
                //DateTime d = DateTime.Now;
                //if(DateTime.TryParse(value, out d))
                //{
                //    accountPeriod = d.ToString("yyyy-MM-dd");
                //}
                //else
                //{
                //    accountPeriod = value;
                //}
            }
        }

        [Description("报表类型")]
        public string ReportType { get; set; }

        [Description("货币资金")]
        public string Monetary { get; set; }

        [Description("客户资金存款")]
        public string CustmerDeposit { get; set; }

        [Description("结算备付金")]
        public string DepositRFB { get; set; }

        [Description("客户备付金")]
        public string CustmerRfB { get; set; }

        [Description("现金及存放中央银行款项")]
        public string CashCentBankItem { get; set; }

        [Description("存放同业款项")]
        public string BankMoney { get; set; }

        [Description("贵金属")]
        public string PreciousMetal { get; set; }

        [Description("拆出资金净额")]
        public string OutMoney { get; set; }

        [Description("交易性金融资产")]
        public string DealBankingAsset { get; set; }

        [Description("衍生金融资产")]
        public string DerivativeFinancialAsset { get; set; }

        [Description("短期投资净额")]
        public string NetBOCI { get; set; }

        [Description("应收票据净额")]
        public string NotesReceivableAmount { get; set; }

        [Description("应收账款净额")]
        public string AccountReceivableAmount { get; set; }

        [Description("预付款项净额")]
        public string PrepaymentAmount { get; set; }

        [Description("应收保费净额")]
        public string ReceivablePremiumAmount { get; set; }

        [Description("应收分保账款净额")]
        public string ReinsuranceAccountsReceivable { get; set; }

        [Description("应收代位追偿款净额")]
        public string ReceivableDWZC { get; set; }

        [Description("应收分保合同准备金净额")]
        public string ReceivableFBHTZBJ { get; set; }

        [Description("应收分保未到期责任准备金净额")]
        public string ReceivableFBWDQZRZBJ { get; set; }

        [Description("应收分保未决赔款准备金净额")]
        public string ReceivableFBWJPCZBJ { get; set; }

        [Description("应收分保寿险责任准备金净额")]
        public string ReceivableFBSXZRZBJ { get; set; }

        [Description("应收分保长期健康险责任准备金净额")]
        public string ReceivableFBCQJKXZRZBJ { get; set; }

        [Description("应收利息净额")]
        public string InterestReceivableAmount { get; set; }

        [Description("应收股利净额")]
        public string DividendReceivableAmount { get; set; }

        [Description("其他应收款净额")]
        public string OtherReceivableAmount { get; set; }

        [Description("买入返售金融资产净额")]
        public string BuyResaleMonetaryAssets { get; set; }

        [Description("存货净额")]
        public string AmountOfInvertory { get; set; }

        [Description("一年内到期的非流动资产")]
        public string Ncatmwoy { get; set; }

        [Description("存出保证金")]
        public string Ccbzj { get; set; }

        [Description("其他流动资产")]
        public string Qtldzc { get; set; }

        [Description("流动资产合计")]
        public string Ldzjhj { get; set; }

        [Description("保户质押贷款净额")]
        public string Bhzydk { get; set; }

        [Description("定期存款")]
        public string Dqck { get; set; }

        [Description("发放贷款及垫款净额")]
        public string Ffdkjdk { get; set; }

        [Description("可供出售金融资产净额")]
        public string Kgcsjr { get; set; }

        [Description("持有至到期投资净额")]
        public string Cyzdqtz { get; set; }

        [Description("长期应收款净额")]
        public string Cqysk { get; set; }

        [Description("长期股权投资净额")]
        public string Cqgqtz { get; set; }

        [Description("长期债权投资净额")]
        public string Cqzrtz { get; set; }

        [Description("长期投资净额")]
        public string Cqtz { get; set; }

        [Description("存出资本保证金")]
        public string Cczbbzj { get; set; }

        [Description("独立账户资产")]
        public string Dlzhzb { get; set; }
        [Description("投资性房地产净额")]
        public string Tzxfdc { get; set; }
        [Description("固定资产净额")]
        public string Gdzc { get; set; }
        [Description("在建工程净额")]
        public string Zjgc { get; set; }
        [Description("工程物资")]
        public string Gcwz { get; set; }
        [Description("固定资产清理")]
        public string Gdzcql { get; set; }
        [Description("生产性生物资产净额")]
        public string Swxswzc { get; set; }
        [Description("油气资产净额")]
        public string Qyzc { get; set; }
        [Description("无形资产净额")]
        public string Wxzc { get; set; }
        [Description("交易席位费")]
        public string Jyxwf { get; set; }
        [Description("开发支出")]
        public string Kfzc { get; set; }
        [Description("商誉净额")]
        public string Syze { get; set; }
        [Description("长期待摊费用")]
        public string Cqdtfy { get; set; }
        [Description("递延所得税资产")]
        public string Dysdszc { get; set; }
        [Description("其他非流动资产")]
        public string Qtfldzc { get; set; }
        [Description("非流动资产合计")]
        public string Fldzchj { get; set; }
        [Description("其他资产")]
        public string Qtzc { get; set; }
        [Description("资产总计")]
        public string Zczj { get; set; }
        [Description("短期借款")]
        public string Dqjk { get; set; }
        [Description("质押借款")]
        public string Zydk { get; set; }
        [Description("向中央银行借款")]
        public string Xzyyhjk { get; set; }
        [Description("吸收存款及同业存放")]
        public string Xsckjtycf { get; set; }
        [Description("同业及其他金融机构存放款项")]
        public string Tyjqtjrjgcfkx { get; set; }
        [Description("吸收存款")]
        public string Xsck { get; set; }
        [Description("拆入资金")]
        public string Crzj { get; set; }
        [Description("交易性金融负债")]
        public string Jyxjrfz { get; set; }
        [Description("衍生金融负债")]
        public string Ysjrfz { get; set; }
        [Description("应付票据")]
        public string Yfpj { get; set; }
        [Description("应付账款")]
        public string Yfzk { get; set; }
        [Description("预收款项")]
        public string Yskx { get; set; }
        [Description("卖出回购金融资产款")]
        public string Mchgjrzck { get; set; }
        [Description("应付手续费及佣金")]
        public string Yfsxfjyj { get; set; }
        [Description("应付职工薪酬")]
        public string Yfzgxc { get; set; }
        [Description("应交税费")]
        public string Yjsf { get; set; }
        [Description("应付利息")]
        public string Yflx { get; set; }
        [Description("应付股利")]
        public string Yfgl { get; set; }
        [Description("应付赔付款")]
        public string Yfpfk { get; set; }
        [Description("应付保单红利")]
        public string Yfbdhl { get; set; }
        [Description("保户储金及投资款")]
        public string Bhcjjtzk { get; set; }
        [Description("保险合同准备金")]
        public string Bxhtzbj { get; set; }
        [Description("未到期责任准备金")]
        public string Wdqzrzbj { get; set; }
        [Description("未决赔款准备金")]
        public string Wjpkzbj { get; set; }
        [Description("寿险责任准备金")]
        public string Sxzrzbj { get; set; }
        [Description("长期健康险责任准备金")]
        public string Cqjkxzrzbj { get; set; }
        [Description("其他应付款")]
        public string Qtyfk { get; set; }
        [Description("应付分保账款")]
        public string Yffbzk { get; set; }
        [Description("代理买卖证券款")]
        public string Dlmmzqk { get; set; }
        [Description("代理承销证券款")]
        public string Dlcszqk { get; set; }
        [Description("预收保费")]
        public string Ysbf { get; set; }
        [Description("一年内到期的非流动负债")]
        public string Ynndqdfldfz { get; set; }
        [Description("其他流动负债")]
        public string Qtldfz { get; set; }
        [Description("递延收益-流动负债")]
        public string Dysyldfz { get; set; }
        [Description("流动负债合计")]
        public string Ldfzhj { get; set; }
        [Description("长期借款")]
        public string Cqjk { get; set; }
        [Description("独立账户负债")]
        public string Dlzhfz { get; set; }
        [Description("应付债券")]
        public string Yfzq { get; set; }
        [Description("长期应付款")]
        public string Cqyfk { get; set; }
        [Description("专项应付款")]
        public string Zxyfk { get; set; }
        [Description("长期负债合计")]
        public string Cqfzhj { get; set; }
        [Description("预计负债")]
        public string Yjfz { get; set; }
        [Description("递延所得税负债")]
        public string Dysdsfz { get; set; }
        [Description("其他非流动负债")]
        public string Qtfldfz { get; set; }
        [Description("递延收益-非流动负债")]
        public string Dysyfldfz { get; set; }
        [Description("非流动负债合计")]
        public string Fldfzhj { get; set; }
        [Description("其他负债")]
        public string Qtfz { get; set; }
        [Description("负债合计")]
        public string Fzhj { get; set; }
        [Description("实收资本(或股本)")]
        public string Sszb { get; set; }
        [Description("其他权益工具")]
        public string Qtqygj { get; set; }
        [Description("优先股")]
        public string Yxg { get; set; }
        [Description("永续债")]
        public string Yxz { get; set; }
        [Description("其他")]
        public string Qt { get; set; }
        [Description("资本公积")]
        public string Zbgj { get; set; }
        [Description("库存股")]
        public string Kcg { get; set; }
        [Description("盈余公积")]
        public string Yygj { get; set; }
        [Description("一般风险准备")]
        public string Ybfxzb { get; set; }
        [Description("未分配利润")]
        public string Wfplr { get; set; }
        [Description("外币报表折算差额")]
        public string Wbbbzsce { get; set; }
        [Description("未确认的投资损失")]
        public string Wqrdtzss { get; set; }
        [Description("交易风险准备")]
        public string Jyfxzb { get; set; }
        [Description("专项储备")]
        public string Zxcb { get; set; }
        [Description("其他综合收益")]
        public string Qtzhsy { get; set; }
        [Description("归属于母公司所有者权益合计")]
        public string Gsymgssyzqyhj { get; set; }
        [Description("少数股东权益")]
        public string Ssgdqy { get; set; }
        [Description("所有者权益合计")]
        public string Sszqyhj { get; set; }
        [Description("负债与所有者权益总计")]
        public string Fzysyzqyzj { get; set; }
    }
}
