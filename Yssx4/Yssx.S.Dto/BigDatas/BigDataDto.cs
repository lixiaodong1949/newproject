﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    public class BigDataDto
    {
        /// <summary>
        /// 数据
        /// </summary>
        public ExcelContentModel Data;

        /// <summary>
        /// 总条数
        /// </summary>
        public long RecordCount;

        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// 页数
        /// </summary>
        public int PageSize { get; set; }
    }

    /// <summary>
    /// 返回数据
    /// </summary>
    public class BigDataDropDownDto
    {
        /// <summary>
        /// 数据
        /// </summary>
        public Dictionary<string, string> Data;
    }

    /// <summary>
    /// 返回数据
    /// </summary>
    public class BigDataParentListDto
    {
        /// <summary>
        /// 父模块
        /// </summary>
        public List<ParentListDto> parentModule { get; set; }
        /// <summary>
        /// 子模块
        /// </summary>
        public List<ParentListDto> childModule { get; set; }
        /// <summary>
        /// 孙模块（子模块下一级）
        /// </summary>
        public List<ParentListDto> grandsonModule { get; set; }

        /// <summary>
        /// 第四级
        /// </summary>
        public List<ParentListDto> TheFourthLevel { get; set; }
    }
}
