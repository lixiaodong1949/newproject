﻿using FreeSql.DataAnnotations;
using System.ComponentModel;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 国债2017
    /// </summary>
    [Table(Name = "yssx_bigData_treasurybonds2017")]
    public class TheYieldTreasuryBonds2017Dto
    {
        [Description("日期")]
        public string Rq { get; set; }

        [Description("标准期限(年)")]
        public string Bzqx { get; set; }

        [Description("基准债券")]
        public string Jzzw { get; set; }

        [Description("剩余期限(年)")]
        public string Syqx { get; set; }

        [Description("报买入收益率(%)")]
        public string Bmrsyl { get; set; }

        [Description("报卖出收益率(%)")]
        public string Bmcsyl { get; set; }
    }
}
