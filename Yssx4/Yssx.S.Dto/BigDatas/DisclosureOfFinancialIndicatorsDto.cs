﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 披露财务指标
    /// </summary>
    public class DisclosureOfFinancialIndicatorsDto
    {
        [Description("股票代码")]
        public string Gpdm { get; set; }

        [Description("截止日期")]
        public string Jzrq { get; set; }

        [Description("非经常性损益")]
        public string Fjcxsy { get; set; }

        [Description("归属于上市公司股东的扣除非经常性损益的净利润")]
        public string Gsyssgsgd { get; set; }

        [Description("加权平均净资产收益率")]
        public string Jqpjjzcsyl { get; set; }

        [Description("扣除非经常性损益后的加权平均净资产收益率")]
        public string Kcfjcx { get; set; }

        [Description("扣除非经常性损益后的基本每股收益")]
        public string Kcfjcxmgsy { get; set; }

        [Description("每股经营活动产生的现金流量净额")]
        public string Mgjyhdcsdxjllje { get; set; }

        [Description("归属于上市公司股东的每股净资产")]
        public string Gsyssgsgddmgjzc { get; set; }

        [Description("基本每股收益")]
        public string Jbmgsy { get; set; }

        [Description("稀释每股收益")]
        public string Xsmgsy { get; set; }
    }
}
