﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 红利分配表
    /// </summary>
    public class DividendDistributionModelDto
    {
        [Description("证券代码")]
        public string Zqdm { get; set; }

        [Description("证券简称")]
        public string Zqjc { get; set; }

        [Description("财政年度")]
        public string Cznd { get; set; }

        [Description("分配期标识")]
        public string Fpqbs { get; set; }

        [Description("预案公告日期")]
        public string Yaggrq { get; set; }

        [Description("预案内容")]
        public string Yanr { get; set; }

        [Description("方案实施进度")]
        public string Fassjd { get; set; }

        [Description("股东大会召开日期")]
        public string Gddhzkrq { get; set; }

        [Description("股东大会公告发布日期")]
        public string Gddhggfbrq { get; set; }

        [Description("股东大会公告刊物")]
        public string Gddhggkw { get; set; }

        [Description("实施方案公告日期")]
        public string Ssfaggrq { get; set; }

        [Description("分配方案公告刊物")]
        public string Fpfaggkw { get; set; }

        [Description("实施方案内容")]
        public string Ssfanr { get; set; }

        [Description("送股比")]
        public string Sgb { get; set; }

        [Description("转增比")]
        public string Zzb { get; set; }

        [Description("派息比税前")]
        public string Pxbsq { get; set; }

        [Description("派息比税后")]
        public string Pxbsh { get; set; }

        [Description("送转数量")]
        public string Szsl { get; set; }

        [Description("派息数")]
        public string Pxs { get; set; }

        [Description("股权登记日")]
        public string Gqdjr { get; set; }

        [Description("最后交易日")]
        public string Zhjyr { get; set; }

        [Description("除权除息日")]
        public string Cqcxr { get; set; }

        [Description("红股转增股上市日")]
        public string Hgzzgssr { get; set; }

        [Description("派息日")]
        public string Pxr { get; set; }

        [Description("分配对象")]
        public string Fpdx { get; set; }

        [Description("股权登记日收盘价")]
        public string Gqdjrspj { get; set; }

        [Description("除权除息日收盘价")]
        public string Cqcxspj { get; set; }
    }
}
