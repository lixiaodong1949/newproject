﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    public class ConditionsDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Description("Id")]
        public long Id { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        [Description("说明")]
        public string Name { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        [Description("值")]
        public string Value { get; set; }
        /// <summary>
        /// 符号
        /// </summary>
        [Description("符号")]
        public string Symbol { get; set; }
    }
}
