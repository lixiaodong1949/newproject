﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    public class ReferenceRateOfRMB2018Dto
    {
        [Description("币种")]
        public string Bz { get; set; }

        [Description("12月")]
        public string Years2018_12 { get; set; }

        [Description("11月")]
        public string Years2018_11 { get; set; }

        [Description("10月")]
        public string Years2018_10 { get; set; }

        [Description("9月")]
        public string Years2018_09 { get; set; }

        [Description("8月")]
        public string Years2018_08 { get; set; }

        [Description("7月")]
        public string Years2018_07 { get; set; }

        [Description("6月")]
        public string Years2018_06 { get; set; }

        [Description("5月")]
        public string Years2018_05 { get; set; }

        [Description("4月")]
        public string Years2018_04 { get; set; }

        [Description("3月")]
        public string Years2018_03 { get; set; }

        [Description("2月")]
        public string Years2018_02 { get; set; }

        [Description("1月")]
        public string Years2018_01 { get; set; }
    }
}
