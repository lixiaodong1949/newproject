﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 人民币月平均汇率
    /// </summary>
    public class TheAverageMonthlyExchangeRateOfRMBDto
    {
        [Description("日期")]
        public string Rq { get; set; }

        [Description("USD/CNY")]
        public string USD_CNY { get; set; }

        [Description("EUR/CNY")]
        public string EUR_CNY { get; set; }

        [Description("100JPY/CNY")]
        public string JPY_CNY { get; set; }

        [Description("HKD/CNY")]
        public string HKD_CNY { get; set; }

        [Description("GBP/CNY")]
        public string GBP_CNY { get; set; }

        [Description("AUD/CNY")]
        public string AUD_CNY { get; set; }

        [Description("NZD/CNY")]
        public string NZD_CNY { get; set; }

        [Description("SGD/CNY")]
        public string SGD_CNY { get; set; }

        [Description("CHF/CNY")]
        public string CHF_CNY { get; set; }

        [Description("CAD/CNY")]
        public string CAD_CNY { get; set; }

        [Description("CNY/MYR")]
        public string CAD_MYR { get; set; }

        [Description("CNY/RUB")]
        public string CNY_RUB { get; set; }

        [Description("CNY/ZAR")]
        public string CNY_ZAR { get; set; }

        [Description("CNY/KRW")]
        public string CNY_KRW { get; set; }

        [Description("CNY/AED")]
        public string CNY_AED { get; set; }

        [Description("CNY/SAR")]
        public string CNY_SAR { get; set; }

        [Description("CNY/HUF")]
        public string CNY_HUF { get; set; }

        [Description("CNY/PLN")]
        public string CNY_PLN { get; set; }

        [Description("CNY/DKK")]
        public string CNY_DKK { get; set; }

        [Description("CNY/SEK")]
        public string CNY_SEK { get; set; }

        [Description("CNY/NOK")]
        public string CNY_NOK { get; set; }

        [Description("CNY/TRY")]
        public string CNY_TRY { get; set; }

        [Description("CNY/MXN")]
        public string CNY_MXN { get; set; }

        [Description("CNY/THB")]
        public string CNY_THB { get; set; }
    }
}
