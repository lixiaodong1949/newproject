﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    public class SearchDto
    {
        /// <summary>
        /// 表id(1,大数据负债，2，活跃债券统计，3，年金终值系数，4，年金现值系数，5，A股指数0000026，6，金融机构人民币存款基准利率,7,金融机构人民币贷款基准利率,
        /// 8,国债,9,B股指数000003,10,成份B股指数390003, 11,复利终值系数，12，企业债，13，披露财务指标，14，LPR数据，
        /// 15，LPR均值数据，16，中期票据(AAA)2017，17，其他指数，18，政策性金融债（国开行）2017，19，政策性金融债（进出口行、开发行）2017，20，利润表，21，红利分配表，
        /// 22，人民币参考汇率_2014，23，人民币参考汇率_2015，24，人民币参考汇率_2016，25，人民币参考汇率_2017，26，人民币参考汇率_2018，27，人民币汇率指数，28，深证成份指数390001，
        /// 29，深证综指399106，30，深证成指R390002，31，Shibor数据， 32，Shibor均值数据，33，Shibor报价数据，34，短期融资债2017，35，统计数据CPI，
        /// 36，人民币月平均汇率，37，人民币汇率中间价，38，创业板指数399006，39，复利现值系数，40，沪深300指数399300，41，上证综指000001，42，国债2017)
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 模块id
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 年份
        /// </summary>
        public string Year { get; set; }
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 页数
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// 筛选字段中文名集合
        /// </summary>
        public List<string> Cols { get; set; }
        /// <summary>
        /// 筛选字段英文名集合
        /// </summary>
        public List<string> Ecols { get; set; }
        /// <summary>
        /// 查询条件集合
        /// </summary>
        public List<Row> Rows { get; set; }
    }
    /// <summary>
    /// 条件
    /// </summary>
    public class Row
    {
        /// <summary>
        /// 方式(0,并且 1,或)
        /// </summary>
        public Way Way { get; set; }
        /// <summary>
        /// 字段
        /// </summary>
        public string Field { get; set; }
        /// <summary>
        /// 条件(等于=0,不等于=1,大于=2,大于或等于=3,小于=4,小于或等于=5,（数值）等于=6,（数值）不等于 = 7,（数值）大于=8,
        /// （数值）大于或等于=9,（数值）小于=10,（数值）小于或等于=11,（日期）等于= 12,（日期）不等于 = 13,（日期）大于 = 14,（日期）大于或等于 = 15,（日期）,小于= 16,（日期）小于或等 = 17,包含=18, 不包含= 19,以开头=20,
        /// 以结尾=21,是空的=22,不是空的=23
        /// </summary>
        public Conditions Conditions { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }


        /// <summary>
        /// 是否有左括号（true,false）
        /// </summary>
        public bool LeftParentheses { get; set; }
        /// <summary>
        /// 是否有右括号(true,false)
        /// </summary>
        public bool RightParentheses { get; set; }
    }

    /// <summary>
    /// 搜索
    /// </summary>
    public class ExcelDto
    {
        /// <summary>
        /// 表id(1,大数据负债，2，活跃债券统计，3，年金终值系数，4，年金现值系数，5，A股指数0000026，6，金融机构人民币存款基准利率,7,金融机构人民币贷款基准利率,
        /// 8,国债,9,B股指数000003,10,成份B股指数390003, 11,复利终值系数，12，企业债，13，披露财务指标，14，LPR数据，
        /// 15，LPR均值数据，16，中期票据(AAA)2017，17，其他指数，18，政策性金融债（国开行）2017，19，政策性金融债（进出口行、开发行）2017，20，利润表，21，红利分配表，
        /// 22，人民币参考汇率_2014，23，人民币参考汇率_2015，24，人民币参考汇率_2016，25，人民币参考汇率_2017，26，人民币参考汇率_2018，27，人民币汇率指数，28，深证成份指数390001，
        /// 29，深证综指399106，30，深证成指R390002，31，Shibor数据， 32，Shibor均值数据，33，Shibor报价数据，34，短期融资债2017，35，统计数据CPI，
        /// 36，人民币月平均汇率，37，人民币汇率中间价，38，创业板指数399006，39，复利现值系数，40，沪深300指数399300，41，上证综指000001，42，国债2017)
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 模块id
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 年份
        /// </summary>
        public string Year { get; set; }
        /// <summary>
        /// 页数
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 筛选字段中文名集合
        /// </summary>
        public List<string> Cols { get; set; }
        /// <summary>
        /// 筛选字段英文名集合
        /// </summary>
        public List<string> Ecols { get; set; }
        /// <summary>
        /// 查询条件集合
        /// </summary>
        public List<Row> Rows { get; set; }
    }
}
