﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    public class AnswerDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Description("Id")]
        public long Id { get; set; }
        /// <summary>
        /// 试卷Id
        /// </summary>

        [Description("试卷Id")]
        public int ExamId { get; set; }
        /// <summary>
        /// 与学生关联字段id
        /// </summary>

        [Description("与学生关联字段id")]
        public int GradeId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>

        [Description("案例Id")]
        public int CoseId { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>

        [Description("题目Id")]
        public int QuestionId { get; set; }
        /// <summary>
        /// 答案
        /// </summary>

        [Description("答案")]
        public string Answer { get; set; }
    }
}
