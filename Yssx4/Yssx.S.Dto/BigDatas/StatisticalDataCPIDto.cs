﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 统计数据CPI
    /// </summary>
    public class StatisticalDataCPIDto
    {
        [Description("统计期间")]
        public string Tjqj { get; set; }

        [Description("居民消费价格指数（上年=100）")]
        public string Jmxfjgzs { get; set; }

        [Description("居民消费价格指数-食品（上年=100）")]
        public string Jmxfjgzs_sp { get; set; }

        [Description("居民消费价格指数-烟酒及用品（上年=100）")]
        public string Jmxfjgzs_yjjyp { get; set; }

        [Description("居民消费价格指数-衣着（上年=100）")]
        public string Jmxfjgzs_yz { get; set; }

        [Description("居民消费价格指数-家庭设备用品及服务（上年=100）")]
        public string Jmxfjgzs_jtsbypjfw { get; set; }

        [Description("居民消费价格指数-医疗保健及个人用品（上年=100）")]
        public string Jmxfjgzs_ylbjjgryp { get; set; }

        [Description("居民消费价格指数-交通和通信（上年=100）")]
        public string Jmxfjgzs_jthtx { get; set; }

        [Description("居民消费价格指数-娱乐教育文化用品及服务（上年=100）")]
        public string Jmxfjgzs_yljywhypjfw { get; set; }

        [Description("居民消费价格指数-居住（上年=100）")]
        public string Jmxfjgzs_jz { get; set; }
    }
}
