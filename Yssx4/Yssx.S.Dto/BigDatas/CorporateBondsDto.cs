﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 企业债
    /// </summary>
    public class CorporateBondsDto
    {
        [Description("证券代码")]
        public string Zqdm { get; set; }

        [Description("名称")]
        public string Mc { get; set; }

        [Description("收盘价（元）")]
        public string Spj { get; set; }

        [Description("到期收益率（%）")]
        public string Dqsyl { get; set; }

        [Description("应计利息额（元）")]
        public string Yjlxe { get; set; }

        [Description("全价（元）")]
        public string Qj { get; set; }

        [Description("发行人")]
        public string Fxr { get; set; }

        [Description("发行总额(亿元)")]
        public string Fxze { get; set; }

        [Description("期限(年)")]
        public string Qx { get; set; }

        [Description("发行价格(元)")]
        public string Fxjg { get; set; }

        [Description("票面利率（%）")]
        public string Pmll { get; set; }

        [Description("付息频率")]
        public string Fxpl { get; set; }

        [Description("发行日期")]
        public string Fxrq { get; set; }

        [Description("付息日说明")]
        public string Fxrsm { get; set; }

        [Description("到期日期")]
        public string Dqrq { get; set; }

        [Description("债券信用评级")]
        public string Zqxypj { get; set; }
    }
}
