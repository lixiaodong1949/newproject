﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    public class ParentListDto
    {
        /// <summary>
        /// Id
        /// </summary>
        [Description("Id")]
        public long Id { get; set; }

        /// <summary>
        /// 模块名称
        /// </summary>
        [Description("模块名称")]
        public string Name { get; set; }

        /// <summary>
        /// 表ID
        /// </summary>
        [Description("表ID")]
        public int TableId { get; set; }

        /// <summary>
        /// 父模块Id
        /// </summary>
        [Description("父模块Id")]
        public int ParentId { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        [Description("年份")]
        public string Year { get; set; }
    }
}
