﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 证券数据-指数2019所有数据
    /// </summary>
    public class AShareIndex2019Dto
    {
        [Description("股票代码")]
        public string Zsdm { get; set; }

        [Description("日期")]
        public string Date { get; set; }

        [Description("名称")]
        public string Name { get; set; }

        [Description("收盘价")]
        public string Spj { get; set; }

        [Description("最高价")]
        public string Zgj { get; set; }

        [Description("最低价")]
        public string Zdj { get; set; }

        [Description("开盘价")]
        public string Kpj { get; set; }

        [Description("前收盘")]
        public string Qsp { get; set; }

        [Description("涨跌额")]
        public string Zde { get; set; }

        [Description("涨跌幅")]
        public string Zdf { get; set; }

        [Description("成交量")]
        public string Cjl { get; set; }

        [Description("成交金额")]
        public string Cjje { get; set; }
    }
}
