﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    public enum Way
    {

        [Description("and")]
        And = 0,

        [Description("or")]
        Or = 1,
    }

    public enum Conditions
    {
        /// <summary>
        /// 等于
        /// </summary>
        Equal = 0,
        /// <summary>
        /// 不等于
        /// </summary>
        NotEqual = 1,
        /// <summary>
        /// 大于
        /// </summary>
        Greater = 2,
        /// <summary>
        /// 大于或等于
        /// </summary>
        GreaterOrEqual = 3,
        /// <summary>
        /// 小于
        /// </summary>
        LessThan = 4,
        /// <summary>
        /// 小于或等于
        /// </summary>
        LessThanOrEqual = 5,

        /// <summary>
        /// （数值）等于
        /// </summary>
        ValueIsEqualTo = 6,
        /// <summary>
        /// （数值）不等于
        /// </summary>
        ValueIsNotEqual = 7,
        /// <summary>
        /// （数值）大于
        /// </summary>
        ValueGreater = 8,
        /// <summary>
        /// （数值）大于或等于
        /// </summary>
        ValueGreaterOrEqual = 9,
        /// <summary>
        /// （数值）小于
        /// </summary>
        ValueLessThan = 10,
        /// <summary>
        /// （数值）小于或等于
        /// </summary>
        ValueLessThanOrEqual = 11,

        /// <summary>
        /// （日期）等于
        /// </summary>
        DateIsEqualTo = 12,
        /// <summary>
        /// （日期）不等于
        /// </summary>
        DateIsNotEqual = 13,
        /// <summary>
        /// （日期）大于
        /// </summary>
        DateGreater = 14,
        /// <summary>
        /// （日期）大于或等于
        /// </summary>
        DateGreaterOrEqual = 15,
        /// <summary>
        /// （日期）小于
        /// </summary>
        DateLessThan = 16,
        /// <summary>
        /// （日期）小于或等于
        /// </summary>
        DateLessThanOrEqual = 17,

        /// <summary>
        /// 包含
        /// </summary>
        Contains = 18,
        /// <summary>
        /// 不包含
        /// </summary>
        NotContains = 19,
        /// <summary>
        /// 以开头
        /// </summary>
        InTheBeginning = 20,
        /// <summary>
        /// 以结尾
        /// </summary>
        InTheEnd = 21,
        /// <summary>
        /// 是空的
        /// </summary>
        Null = 22,
        /// <summary>
        /// 不是空的
        /// </summary>
        NotNull = 23
    }
}
