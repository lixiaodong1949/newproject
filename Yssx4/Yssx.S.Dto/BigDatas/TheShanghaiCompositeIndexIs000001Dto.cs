﻿using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 上证综指000001
    /// </summary>
    public class TheShanghaiCompositeIndexIs000001Dto
    {
        [Description("指数代码")]
        public string Zsdm { get; set; }

        [Description("月度标识")]
        public string Ydbs { get; set; }

        [Description("月开盘日期")]
        public string Ykprq { get; set; }

        [Description("月收盘日期")]
        public string Ysprq { get; set; }

        [Description("开盘指数")]
        public string Kpzs { get; set; }

        [Description("最高指数")]
        public string Zgzs { get; set; }

        [Description("最低指数")]
        public string Zdzs { get; set; }

        [Description("收盘指数")]
        public string Spzs { get; set; }

        [Description("成份证券成交量（万股）")]
        public string Cfzjvjl { get; set; }

        [Description("成份证券成交金额（万元）")]
        public string Cfzjcjje { get; set; }

        [Description("指数回报率")]
        public string Zshbl { get; set; }
    }
}
