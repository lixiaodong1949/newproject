﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 国债
    /// </summary>
    public class BondsAreDebt2017Dto
    {
        [Description("证券代码")]
        public string Zqdm { get; set; }

        [Description("名称")]
        public string Mc { get; set; }

        [Description("收盘价（元）")]
        public string Spj { get; set; }

        [Description("到期收益率（%）")]
        public string Dqsyl { get; set; }

        [Description("应计利息额（元）")]
        public string Yjlse { get; set; }

        [Description("全价（元）")]
        public string Qj { get; set; }

        [Description("实际发行量")]
        public string Sjfsc { get; set; }

        [Description("发行价格（元）")]
        public string fXJG { get; set; }

        [Description("期限（年）")]
        public string Qx { get; set; }

        [Description("到期日")]
        public string Dqr { get; set; }

        [Description("票面利率（%）")]
        public string Pmll { get; set; }

        [Description("国债付息方式")]
        public string Gzdxfs { get; set; }

        [Description("计息日期")]
        public string Jxrq { get; set; }
    }
}
