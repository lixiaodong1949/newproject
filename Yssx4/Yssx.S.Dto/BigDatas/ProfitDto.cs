﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 利润表
    /// </summary>
    public class ProfitDto
    {
        [Description("证券简称")]
        public string SecurityShort { get; set; }

        [Description("行业名称")]
        public string Name { get; set; }

        [Description("公司名称")]
        public string CompanyName { get; set; }

        [Description("证券代码")]
        public string SecurityCode { get; set; }

        [Description("会计期间")]
        public string AccountPeriod { get; set; }

        [Description("报表类型")]
        public string ReportType { get; set; }

        /// <summary>
        /// 营业总收入
        /// </summary>
        [Description("营业总收入")]
        public string Yyzsr { get; set; }

        /// <summary>
        /// 营业收入
        /// </summary>
        [Description("营业收入")]
        public string Yysr { get; set; }

        /// <summary>
        /// 利息净收入
        /// </summary>
        [Description("利息净收入")]
        public string Lxjsr { get; set; }

        /// <summary>
        /// 利息收入
        /// </summary>
        [Description("利息收入")]
        public string Lxsr { get; set; }

        /// <summary>
        /// 利息支出
        /// </summary>
        [Description("利息支出")]
        public string Lxzc { get; set; }

        /// <summary>
        /// 已赚保费
        /// </summary>
        [Description("已赚保费")]
        public string Yzbf { get; set; }

        /// <summary>
        /// 保险业务收入
        /// </summary>
        [Description("保险业务收入")]
        public string Bxywsr { get; set; }

        /// <summary>
        /// 分保费收入
        /// </summary>
        [Description("分保费收入")]
        public string Fbfsr { get; set; }

        /// <summary>
        /// 分出保费
        /// </summary>
        [Description("分出保费")]
        public string Fcbf { get; set; }

        /// <summary>
        /// 提取未到期责任准备金
        /// </summary>
        [Description("提取未到期责任准备金")]
        public string Tqwdqzrzbj { get; set; }

        /// <summary>
        /// 手续费及佣金净收入
        /// </summary>
        [Description("手续费及佣金净收入")]
        public string Sxfjyjzsr { get; set; }

        /// <summary>
        /// 代理买卖证券业务净收入
        /// </summary>
        [Description("代理买卖证券业务净收入")]
        public string Dlmmzqywzsr { get; set; }

        /// <summary>
        /// 证券承销业务净收入
        /// </summary>
        [Description("证券承销业务净收入")]
        public string Zqcxyejsr { get; set; }

        /// <summary>
        /// 受托客户资产管理业务净收入
        /// </summary>
        [Description("受托客户资产管理业务净收入")]
        public string Stkhzcglywjsr { get; set; }

        /// <summary>
        /// 手续费及佣金收入
        /// </summary>
        [Description("手续费及佣金收入")]
        public string Sxfjyjsr { get; set; }

        /// <summary>
        /// 手续费及佣金支出
        /// </summary>
        [Description("手续费及佣金支出")]
        public string Sxfjyjzc { get; set; }

        /// <summary>
        /// 其他业务收入
        /// </summary>
        [Description("其他业务收入")]
        public string Qtywsr { get; set; }

        /// <summary>
        /// 营业总成本
        /// </summary>
        [Description("营业总成本")]
        public string Yyzcb { get; set; }

        /// <summary>
        /// 营业成本
        /// </summary>
        [Description("营业成本")]
        public string Yycb { get; set; }

        /// <summary>
        /// 退保金
        /// </summary>
        [Description("退保金")]
        public string Tbj { get; set; }

        /// <summary>
        /// 赔付支出净额
        /// </summary>
        [Description("赔付支出净额")]
        public string Pfzcje { get; set; }

        /// <summary>
        /// 赔付支出
        /// </summary>
        [Description("赔付支出")]
        public string Pfzc { get; set; }

        /// <summary>
        /// 摊回赔付支出
        /// </summary>
        [Description("摊回赔付支出")]
        public string Thpfzc { get; set; }

        /// <summary>
        /// 提取保险责任准备金净额
        /// </summary>
        [Description("提取保险责任准备金净额")]
        public string Thbxzrzbjje { get; set; }

        /// <summary>
        /// 提取保险责任准备金
        /// </summary>
        [Description("提取保险责任准备金")]
        public string Thbxzrzbj { get; set; }

        /// <summary>
        /// 摊回保险责任准备金
        /// </summary>
        [Description("摊回保险责任准备金")]
        public string Thbxzrzbje { get; set; }

        /// <summary>
        /// 保单红利支出
        /// </summary>
        [Description("保单红利支出")]
        public string Bdhlzc { get; set; }

        /// <summary>
        /// 分保费用
        /// </summary>
        [Description("分保费用")]
        public string Fbfy { get; set; }

        /// <summary>
        /// 营业税金及附加
        /// </summary>
        [Description("税金及附加")]
        public string Yysjjfj { get; set; }

        /// <summary>
        /// 业务及管理费
        /// </summary>
        [Description("业务及管理费")]
        public string Ywjklf { get; set; }

        /// <summary>
        /// 摊回分保费用
        /// </summary>
        [Description("摊回分保费用")]
        public string Thfbfy { get; set; }

        /// <summary>
        /// 销售费用
        /// </summary>
        [Description("销售费用")]
        public string Xsfy { get; set; }

        /// <summary>
        /// 管理费用
        /// </summary>
        [Description("管理费用")]
        public string Glfy { get; set; }

        /// <summary>
        /// 财务费用
        /// </summary>
        [Description("财务费用")]
        public string Cwfy { get; set; }

        /// <summary>
        /// 资产减值损失
        /// </summary>
        [Description("资产减值损失")]
        public string Jcjzss { get; set; }

        /// <summary>
        /// 其他业务成本
        /// </summary>
        [Description("其他业务成本")]
        public string Qtywcb { get; set; }

        /// <summary>
        /// 公允价值变动收益
        /// </summary>
        [Description("公允价值变动收益")]
        public string Gyjzbdsy { get; set; }

        /// <summary>
        /// 投资收益
        /// </summary>
        [Description("投资收益")]
        public string Tzsy { get; set; }

        /// <summary>
        /// 对联营企业和合营企业的投资收益
        /// </summary>
        [Description("对联营企业和合营企业的投资收益")]
        public string Lyqyhhyqytzsy { get; set; }

        /// <summary>
        /// 汇兑收益
        /// </summary>
        [Description("汇兑收益")]
        public string Hdsy { get; set; }

        /// <summary>
        /// 其他业务利润
        /// </summary>
        [Description("其他业务利润")]
        public string Qtywlr { get; set; }

        /// <summary>
        /// 营业利润
        /// </summary>
        [Description("营业利润")]
        public string Yylr { get; set; }

        /// <summary>
        /// 营业外收入
        /// </summary>
        [Description("营业外收入")]
        public string Yywsr { get; set; }

        /// <summary>
        /// 非流动资产处置利得
        /// </summary>
        [Description("非流动资产处置利得")]
        public string Fldzcczld { get; set; }



        /// <summary>
        /// 营业外支出
        /// </summary>
        [Description("营业外支出")]
        public string Yywzc { get; set; }

        /// <summary>
        /// 非流动资产处置净损益
        /// </summary>
        [Description("非流动资产处置净损益")]
        public string Fldzcczjsy { get; set; }

        /// <summary>
        /// 非流动资产处置损失
        /// </summary>
        [Description("非流动资产处置损失")]
        public string Fldzcczss { get; set; }

        /// <summary>
        /// 利润总额
        /// </summary>
        [Description("利润总额")]
        public string Lrze { get; set; }

        /// <summary>
        /// 所得税费用
        /// </summary>
        [Description("所得税费用")]
        public string Sdsfy { get; set; }

        /// <summary>
        /// 未确认的投资损失
        /// </summary>
        [Description("未确认的投资损失")]
        public string Wqrdtzss { get; set; }

        /// <summary>
        /// 影响净利润的其他项目
        /// </summary>
        [Description("影响净利润的其他项目")]
        public string Yxjlrdqtxm { get; set; }

        /// <summary>
        /// 净利润
        /// </summary>
        [Description("净利润")]
        public string Jlr { get; set; }

        /// <summary>
        /// 归属于母公司所有者的净利润
        /// </summary>
        [Description("归属于母公司所有者的净利润")]
        public string Gsymgssyzjlr { get; set; }

        /// <summary>
        /// 少数股东损益
        /// </summary>
        [Description("少数股东损益")]
        public string Ssgdsy { get; set; }

        /// <summary>
        /// 基本每股收益
        /// </summary>
        [Description("基本每股收益")]
        public string Jbmgsy { get; set; }

        /// <summary>
        /// 稀释每股收益
        /// </summary>
        [Description("稀释每股收益")]
        public string Xsmgsy { get; set; }

        /// <summary>
        /// 其他综合收益(损失)
        /// </summary>
        [Description("其他综合收益(损失)")]
        public string Qtzhsyss { get; set; }

        /// <summary>
        /// 综合收益总额
        /// </summary>
        [Description("综合收益总额")]
        public string Zhsyze { get; set; }

        /// <summary>
        /// 归属于母公司所有者的综合收益
        /// </summary>
        [Description("归属于母公司所有者的综合收益")]
        public string Gsymgssyzzhsy { get; set; }

        /// <summary>
        /// 归属少数股东的综合收益
        /// </summary>
        [Description("归属少数股东的综合收益")]
        public string Gsssgdzhsy { get; set; }
    }
}
