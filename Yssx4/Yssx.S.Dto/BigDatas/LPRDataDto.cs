﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    public class LPRDataDto
    {
        [Description("日期")]
        public string Rq { get; set; }

        [Description("1年期LPR（%）")]
        public string LPR_1 { get; set; }
    }
}
