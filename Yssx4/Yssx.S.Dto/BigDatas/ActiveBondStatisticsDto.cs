﻿using FreeSql.DataAnnotations;
using System.ComponentModel;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// 活跃债券统计
    /// </summary>
    [Table(Name = "yssx_bigData_activebondstatistics")]
    public class ActiveBondStatisticsDto
    {
        [Description("名次")]
        public string Mc { get; set; }

        [Description("债券简称")]
        public string Zqjc { get; set; }

        [Description("成交金额(亿元)")]
        public string Cjje { get; set; }

        [Description("到期收益率(%)")]
        public string Dqsyl { get; set; }

        [Description("日期")]
        public string Rq { get; set; }

        [Description("笔数")]
        public string Bs { get; set; }
    }
}
