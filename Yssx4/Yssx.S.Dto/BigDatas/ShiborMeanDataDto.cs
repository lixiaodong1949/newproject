﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto.BigDatas
{
    /// <summary>
    /// Shibor均值数据
    /// </summary>
    public class ShiborMeanDataDto
    {
        [Description("日期")]
        public string Rq { get; set; }

        [Description("均值类型")]
        public string Jzlx { get; set; }

        [Description("O/N(%)")]
        public string ONW { get; set; }

        [Description("1W(%)")]
        public string OneW { get; set; }

        [Description("2W(%)")]
        public string TwoW { get; set; }

        [Description("1M(%)")]
        public string OneM { get; set; }

        [Description("3M(%)")]
        public string ThreeM { get; set; }

        [Description("6M(%)")]
        public string SixM { get; set; }

        [Description("9M(%)")]
        public string NineM { get; set; }

        [Description("1Y(%)")]
        public string OneY { get; set; }
    }
}
