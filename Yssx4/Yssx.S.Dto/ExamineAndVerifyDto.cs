﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 审核dto
    /// </summary>
    public class ExamineAndVerifyDto
    {
        /// <summary>
        /// Id集合
        /// </summary>
        public List<long> IdList { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        public string Auditstatusexplain { get; set; }
        /// <summary>
        /// 类型（1，课程，2，课件，3，案例，4，题目，5，场景实训）
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 1，审核成功，2，审核拒绝
        /// </summary>
        public int Examinestate { get; set; }
        /// <summary>
        /// 案例等级  1A 2B 3C 4D 5E 6F
        /// </summary>
        public int CaseGrade { get; set; }
        ///// <summary>
        ///// 价值点
        ///// </summary>
        //public string ValuePoint { get; set; }
    }
}
