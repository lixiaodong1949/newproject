﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 保存课业任务 - 入参
    /// </summary>
    public class TaskDto : BaseTaskDto
    {
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 班级学生明细
        /// </summary>
        public List<TaskClassDto> ClassDetail { get; set; }

        /// <summary>
        /// 班级Id列表
        /// </summary>
        public List<long> ClassIds { get; set; }
        /// <summary>
        /// 题目列表
        /// </summary>
        public List<TaskTopicDto> TopicList { get; set; }
    }

    /// <summary>
    /// 课业任务 - 班级学生信息
    /// </summary>
    public class TaskClassDto
    {
        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 学生ID【废弃不用，用UserId】2020.02.11
        /// </summary>
        public long StudentId { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }
    }



}
