﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课业任务 - 题目分类信息
    /// </summary>
    public class TaskTopicGroupDto
    {
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 总分数
        /// </summary>
        public decimal TotalScore { get; set; }

    }
}
