﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Task
{
    /// <summary>
    /// 课堂任务详情
    /// </summary>
    public class TestTaskDetailsDto
    {
        /// <summary>
        /// 总人数
        /// </summary>
        public long PeopleCount { get; set; }

        /// <summary>
        /// 提交人数
        /// </summary>
        public long CommitCount { get; set; }

        /// <summary>
        /// 做题中人数
        /// </summary>
        public long DoingCount { get; set; }

        public string Date { get; set; }

        /// <summary>
        /// 用时
        /// </summary>
        public long TotalTime { get; set; }

        /// <summary>
        /// 学生列表
        /// </summary>
        public List<string> Students { get; set; }
    }
}
