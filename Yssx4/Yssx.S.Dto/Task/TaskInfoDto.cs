﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 单课业任务输出
    /// </summary>
    public class TaskInfoDto : BaseTaskDto
    {
        /// <summary>
        /// 任务类型名称
        /// </summary> 
        public string TaskTypeName
        {
            get { return TaskType.GetDescription(); }
            set { }
        }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public LessonsTaskStatus Status { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 总分数
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 班级明细
        /// </summary>
        public List<TaskInfoClassDto> ClassDetail { get; set; }

        /// <summary>
        /// 题目明细
        /// </summary>
        public List<TaskTopicDto> TopicDetail { get; set; }

        /// <summary>
        /// 题目分类信息
        /// </summary>
        public List<TaskTopicGroupDto> TopicGroupDetail { get; set; }

    }

    /// <summary>
    /// 课业任务 - 班级
    /// </summary>
    public class TaskInfoClassDto
    {
        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 学生数量
        /// </summary>
        public int StudentCount { get; set; }

        /// <summary>
        /// 学生明细
        /// </summary>
        public List<TaskInfoStudentDto> StudentDetail { get; set; }

    }
    /// <summary>
    /// 课业任务 - 学生
    /// </summary>
    public class TaskInfoStudentDto
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 学生ID
        /// </summary>
        public long StudentId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }
    }

}
