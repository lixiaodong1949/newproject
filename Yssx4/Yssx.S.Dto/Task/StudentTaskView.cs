﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课业任务列表视图 - 学生端
    /// </summary>
    public class StudentTaskView : BaseTaskDto
    {
        /// <summary>
        /// 任务类型名称
        /// </summary> 
        public string TaskTypeName
        {
            get { return TaskType.GetDescription(); }
            set { }
        }
        /// <summary>
        /// 试卷类型
        /// </summary>
        public CourseExamType ExamType
        {
            get
            {
                return TaskType == TaskType.ClassExam ? CourseExamType.ClassExam : (TaskType == TaskType.ClassTask ? CourseExamType.ClassTask : CourseExamType.ClassHomeWork);
            }
        }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public LessonsTaskStatus Status { get; set; }
        /// <summary>
        /// 试卷状态
        /// </summary>
        public LessonsTaskStatus ExamStatus
        {
            get
            {
                if (Status == LessonsTaskStatus.End)
                    return LessonsTaskStatus.End;
                var dateNow = DateTime.Now;
                if (dateNow < StartTime)
                    return LessonsTaskStatus.Wait;
                if (StartTime <= dateNow && dateNow < CloseTime)
                    return LessonsTaskStatus.Started;
                if (CloseTime <= dateNow)
                    return LessonsTaskStatus.End;
                return LessonsTaskStatus.Wait;
            }
        }
        /// <summary>
        /// 作答记录状态
        /// </summary>
        public StudentExamStatus GradeStatus { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 作答记录ID
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 错题数
        /// </summary>
        public int ErrorCount { get; set; }
        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamPaperScore { get; set; } = 0;
        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; } = 0;

    }
}
