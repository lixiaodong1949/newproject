﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Task
{
    /// <summary>
    /// 
    /// </summary>
    public class AnalyzeTaskResultDto
    {
        /// <summary>
        /// 总人数
        /// </summary>
        public int PeopleCount { get; set; }

        /// <summary>
        /// 提交人数
        /// </summary>
        public int CommitCount { get; set; }

        /// <summary>
        /// 做题时间
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// 题目统计分析
        /// </summary>
        public List<AnalyzeQuestionDetailsDto> QuestionDetails { get; set; }
    }

    /// <summary>
    /// 题目详情分析
    /// </summary>
    public class AnalyzeQuestionDetailsDto
    {

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题干
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 提交人数
        /// </summary>
        public int CommitCount { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 父级Id
        /// </summary>
        public long ParentQuestionId { get; set; }


    }

    public class AnalyzeQuestionDetailsStudentsDto
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 学生列表
        /// </summary>
        public List<string> Students { get; set; }
    }


}
