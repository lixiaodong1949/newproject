﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取手动配置记录列表
    /// </summary>
    public class FreeTestPaperDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 用户账户
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 来源 （1.云实习真账实操 2.课程同步实训 3.业税财实训 4.云上实训之岗位实训 5.课程实训 6.经验案例 7.初级会计考证）
        /// </summary>
        public int Source { get; set; }

        /// <summary>
        /// 标的物id
        /// </summary>
        public long TargetId { get; set; }

        /// <summary>
        /// 标的物名称
        /// </summary>
        public string TargetName { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public long CreateBy { get; set; }

        /// <summary>
        /// 创建人账户
        /// </summary>
        public string CreateByName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }

    /// <summary>
    /// 获取手动配置记录列表 - 入参
    /// </summary>
    public class GetFreeTestPaperRequest : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 来源 （1.云实习真账实操 2.课程同步实训 3.业税财实训 4.云上实训之岗位实训 5.课程实训 6.经验案例 7.初级会计考证）
        /// </summary>
        public int Source { get; set; } = 0;
    }
}
