﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto.Task
{
    /// <summary>
    /// 我的课堂实训
    /// </summary>
    public class MyCourseTaskDto
    {
        /// <summary>
        /// 任务ID
        /// </summary>
        public long TaskID { get; set; }

        /// <summary>
        /// 任务状态
        /// </summary>
        public LessonsTaskStatus TaskStatus { get; set; }

        /// <summary>
        /// 课程附标题
        /// </summary>
        public string CourseTitle { get; set; }

        /// <summary>
        /// 课程图片
        /// </summary>
        public string Images { get; set; }

        public long CourseID { get; set; }

        /// <summary>
        /// 源课程ID
        /// </summary>
        public long OriginalCourseId { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 出版日期
        /// </summary>
        public string PublishingDate { get; set; }

        /// <summary>
        /// 出版社
        /// </summary>
        public string PublishingHouse { get; set; }
        /// <summary>
        /// 创建人名称
        /// </summary>
        public string CreateByName { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        public List<CourseSummary> Summarys { get; set; }
    }


    public class CourseSummary
    {
        /// <summary>
        /// 章节
        /// </summary>
        //public int SectionTotal { get; set; }

        ///// <summary>
        ///// 教材
        ///// </summary>
        //public int TeachingMaterialTotal { get; set; }
        ///// <summary>
        ///// 课件
        ///// </summary>
        //public int CoursewareTotal { get; set; }
        ///// <summary>
        ///// 习题
        ///// </summary>
        //public int TopicTotal { get; set; }
        ///// <summary>
        ///// 实训
        ///// </summary>
        //public int TrainingTotal { get; set; }
        ///// <summary>
        ///// 视频
        ///// </summary>
        //public int VideoTotal { get; set; }
        ///// <summary>
        ///// 教案
        ///// </summary>
        //public int CaseTotal { get; set; }

        /// <summary>
        /// 主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 统计分类 0:教材 1:习题 2:案例 3:视频 4:章节 5:课件 6:场景实训 7:教案 8:授课计划
        /// </summary>
        public int SummaryType { get; set; }

        /// <summary>
        /// 个数
        /// </summary>
        public int Count { get; set; }

    }

    /// <summary>
    /// 课程测试任务分析
    /// </summary>
    public class CourseTestingTaskAnalysis
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }
        /// <summary>
        /// 任务时间
        /// </summary>
        public DateTime TaskDate { get; set; }
        /// <summary>
        /// 任务人数
        /// </summary>
        public int NumberOfTask { get; set; }
        /// <summary>
        /// 提交人数
        /// </summary>
        public int SubmitTotal { get; set; }
        /// <summary>
        /// 答题中人数
        /// </summary>
        public int NumberOfAnswer { get; set; }
        /// <summary>
        /// 回答错误学生列表
        /// </summary>
        public  List<AnswerTopicAnalysis> TopicAnalysisList { get; set; }
        /// <summary>
        /// 任务学生列表
        /// </summary>
        public List<string> StudentList { get; set; }
    }

    /// <summary>
    /// 课程测试任务-答题分析
    /// </summary>
    public class AnswerTopicAnalysis
    {
        /// <summary>
        /// 题目ID
        /// </summary>
        public long TopicID { get; set; }
        /// <summary>
        /// 题目
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 答题人数
        /// </summary>
        public int NumberOfTask { get; set; }
        /// <summary>
        /// 回答错误人数
        /// </summary>
        public int AnswerErrorTotal { get; set; }
        /// <summary>
        /// 回答错误学生
        /// </summary>
        public List<string> ErrorStudentList { get; set; }
    }

}
