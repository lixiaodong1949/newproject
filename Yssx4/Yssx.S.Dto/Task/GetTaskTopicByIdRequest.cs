﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询题目类型数量统计 - 入参
    /// </summary>
    public class GetTaskTopicByIdRequest
    {
        /// <summary>
        /// ID数据来源（1.知识点 2.课程章节）
        /// </summary>
        public int DataType { get; set; }

        /// <summary>
        /// 考察范围（知识点ID/课程章节ID）
        /// </summary>
        public List<long> SurveyScopeDetail { get; set; }

        /// <summary>
        /// 题目更新年份（多个年份以逗号分隔）
        /// </summary>
        public string TopicYear { get; set; }
    }

    /// <summary>
    /// 查询题目类型数量统计 - new
    /// </summary>
    public class GetTaskTopicGroupRequest
    {
        /// <summary>
        /// ID数据来源（1.知识点 2.课程章节）
        /// </summary>
        public int DataType { get; set; }

        /// <summary>
        /// 考察范围（知识点ID/课程章节ID）
        /// </summary>
        public List<long> SurveyScopeDetail { get; set; }

        /// <summary>
        /// 题目更新年份（多个年份以逗号分隔）
        /// </summary>
        public string TopicYear { get; set; }
    }

}
