﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课堂测验列表视图 - 教师端
    /// </summary>
    public class TestingTaskView : BaseTaskDto
    {
        /// <summary>
        /// 任务类型名称
        /// </summary> 
        public string TaskTypeName
        {
            get { return TaskType.GetDescription(); }
        }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public ExamStatus Status { get; set; }
        /// <summary>
        /// 试卷状态
        /// </summary>
        public ExamStatus ExamStatus
        {
            get
            {
                if (Status == ExamStatus.End)
                    return ExamStatus.End;
                var dateNow = DateTime.Now;
                if (dateNow < StartTime)
                    return ExamStatus.Wait;
                if (StartTime <= dateNow && (CloseTime == DateTime.MinValue || dateNow < CloseTime))
                    return ExamStatus.Started;
                if (CloseTime <= dateNow)
                    return ExamStatus.End;
                return ExamStatus.Wait;
            }
        }

        /// <summary>
        /// 组Id
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// 班级明细
        /// </summary>
        public List<TaskClassView> ClassDetail { get; set; }

        /// <summary>
        /// 任务学生总人数
        /// </summary>
        public int TotalStudentCount { get; set; }

    }

    /// <summary>
    /// 课堂测验列表视图 - 学生端
    /// </summary>
    public class StudentTestingTaskView : BaseTaskDto
    {
        /// <summary>
        /// 任务类型名称
        /// </summary> 
        public string TaskTypeName
        {
            get { return TaskType.GetDescription(); }
            set { }
        }
        /// <summary>
        /// 试卷类型
        /// </summary>
        public CourseExamType ExamType
        {
            get
            {
                return CourseExamType.ClassTest;
            }
        }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public LessonsTaskStatus Status { get; set; }
        /// <summary>
        /// 试卷状态
        /// </summary>
        public LessonsTaskStatus ExamStatus
        {
            get
            {
                if (Status == LessonsTaskStatus.End)
                    return LessonsTaskStatus.End;
                var dateNow = DateTime.Now;
                if (dateNow < StartTime)
                    return LessonsTaskStatus.Wait;
                if (StartTime <= dateNow && (CloseTime == DateTime.MinValue || dateNow < CloseTime))
                    return LessonsTaskStatus.Started;
                if (CloseTime <= dateNow)
                    return LessonsTaskStatus.End;
                return LessonsTaskStatus.Wait;
            }
        }
        /// <summary>
        /// 组Id
        /// </summary>
        public long GroupId { get; set; }
        /// <summary>
        /// 作答记录ID
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 错题数
        /// </summary>
        public int ErrorCount { get; set; }
        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamPaperScore { get; set; } = 0;
        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; } = 0;

    }

}
