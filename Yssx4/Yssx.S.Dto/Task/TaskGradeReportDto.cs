﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课业任务统计信息
    /// </summary>
    public class TaskGradeReportDto : BaseTaskDto
    {
        /// <summary>
        /// 任务类型名称
        /// </summary> 
        public string TaskTypeName
        {
            get { return TaskType.GetDescription(); }
            set { }
        }
        /// <summary>
        /// 总分数
        /// </summary>
        public decimal TotalScore { get; set; }
        
        /// <summary>
        /// 最高分
        /// </summary>
        public decimal MaxScore { get; set; }
        /// <summary>
        /// 最低分
        /// </summary>
        public decimal MinScore { get; set; }
        /// <summary>
        /// 平均分
        /// </summary>
        public decimal AvgScore { get; set; }

        /// <summary>
        /// 总分80%的分数
        /// </summary>
        public int ExcellentScore { get; set; }
        /// <summary>
        /// 总分70%的分数
        /// </summary>
        public int GoodScore { get; set; }
        /// <summary>
        /// 总分60%的分数
        /// </summary>
        public int QualifiedScore { get; set; }

        /// <summary>
        /// 优秀人数 (大于等于80%分数)
        /// </summary>
        public int ExcellentNumber { get; set; }
        /// <summary>
        /// 良好人数(70-79%分数)
        /// </summary>
        public int GoodNumber { get; set; }
        /// <summary>
        /// 合格人数(60-69%分数)
        /// </summary>
        public int QualifiedNumber { get; set; }
        /// <summary>
        /// 不合格(小于60%分数)
        /// </summary>
        public int UnqualifiedNumber { get; set; }
        /// <summary>
        /// 任务总人数
        /// </summary>
        public int TotalCount
        {
            get
            {
                return ExcellentNumber + GoodNumber + QualifiedNumber + UnqualifiedNumber;
            }
        }

        /// <summary>
        /// 优秀率
        /// </summary>
        public decimal ExcellentRate
        {
            get
            {
                if (TotalCount == 0)
                    return 0;
                else
                    return Math.Round(ExcellentNumber * 100 / (decimal)TotalCount, 2, MidpointRounding.AwayFromZero);
            }
        }
        /// <summary>
        /// 良好率
        /// </summary>
        public decimal GoodRate
        {
            get
            {
                if (TotalCount == 0)
                    return 0;
                else
                    return Math.Round(GoodNumber * 100 / (decimal)TotalCount, 2, MidpointRounding.AwayFromZero);
            }
        }
        /// <summary>
        /// 合格率
        /// </summary>
        public decimal QualifiedRate
        {
            get
            {
                if (TotalCount == 0)
                    return 0;
                else
                    return Math.Round(QualifiedNumber * 100 / (decimal)TotalCount, 2, MidpointRounding.AwayFromZero);
            }
        }
        /// <summary>
        /// 不合格率
        /// </summary>
        public decimal UnqualifiedRate
        {
            get
            {
                if (TotalCount == 0)
                    return 0;
                else
                    return Math.Round(UnqualifiedNumber * 100 / (decimal)TotalCount, 2, MidpointRounding.AwayFromZero);
            }
        }
        /// <summary>
        /// 交卷人数
        /// </summary>
        public int SubmitedCount { get; set; }
        /// <summary>
        /// 未交卷人数
        /// </summary>
        public int NoSubmitedCount { get; set; }
        /// <summary>
        /// 缺考人数
        /// </summary>
        public int MissCount { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public LessonsTaskStatus Status { get; set; }

        /// <summary>
        /// 试卷状态
        /// </summary>
        public LessonsTaskStatus ExamStatus
        {
            get
            {
                if (Status == LessonsTaskStatus.End)
                    return LessonsTaskStatus.End;
                var dateNow = DateTime.Now;
                if (dateNow < StartTime)
                    return LessonsTaskStatus.Wait;
                if (StartTime <= dateNow && dateNow < CloseTime)
                    return LessonsTaskStatus.Started;
                if (CloseTime <= dateNow)
                    return LessonsTaskStatus.End;
                return LessonsTaskStatus.Wait;
            }
        }

        /// <summary>
        /// 班级明细
        /// </summary>
        public List<TaskGradeReportClassDto> ClassDetail { get; set; }

        /// <summary>
        /// 作答明细
        /// </summary>
        public List<TaskGradeReportGradeDto> GradeDetail { get; set; }

        /// <summary>
        /// 题目明细
        /// </summary>
        public List<TaskTopicDto> TopicDetail { get; set; }

        /// <summary>
        /// 错题明细
        /// </summary>
        public List<TaskTopicDto> ErrorTopicDetail { get; set; }
    }
    /// <summary>
    /// 课业统计 - 班级
    /// </summary>
    public class TaskGradeReportClassDto
    {
        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 学生数量
        /// </summary>
        public int StudentCount { get; set; }

        /// <summary>
        /// 缺考学生数量
        /// </summary>
        public int AbsentStudentCount { get; set; }
    }
    /// <summary>
    /// 课业统计 - 试卷作答
    /// </summary>
    public class TaskGradeReportGradeDto
    {
        /// <summary>
        /// 作答记录ID
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 学生姓名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }
        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; }
        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 作答总用时
        /// </summary>
        public double UsedSeconds { get; set; }
        /// <summary>
        /// 作答总用时 - 描述
        /// </summary>
        public string UsedSecondsDesc
        {
            get
            {
                if (UsedSeconds == 0)
                    return string.Format("0秒");
                double rHour = Math.Floor(UsedSeconds / 3600);
                double rMin = Math.Floor(UsedSeconds % 3600 / 60);
                double rSec = Math.Round(UsedSeconds % 3600 % 60, 0);
                if (rHour == 0 && rMin == 0)
                    return string.Format("{0}秒", rSec);
                if (rHour == 0)
                    return string.Format("{0}分钟{1}秒", rMin, rSec);
                return string.Format("{0}小时{1}分钟{2}秒", rHour, rMin, rSec);
            }
        }
        /// <summary>
        /// 排名
        /// </summary>
        public int Ranking { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public StudentExamStatus Status { get; set; }
        /// <summary>
        /// 是否缺考
        /// </summary>
        public bool IsAbsent
        {
            get
            {
                return CorrectCount + ErrorCount + PartRightCount == 0;
            }
        }

        /// <summary>
        /// 答对题数
        /// </summary>
        public int CorrectCount { get; set; }
        /// <summary>
        /// 答错题数
        /// </summary>
        public int ErrorCount { get; set; }
        /// <summary>
        /// 部分对数量
        /// </summary>
        public int PartRightCount { get; set; }
        /// <summary>
        /// 未答题数
        /// </summary>
        public int BlankCount { get; set; }

        /// <summary>
        /// 正确率
        /// </summary>
        public string CorrectRate
        {
            get
            {
                var total = CorrectCount + ErrorCount + PartRightCount + BlankCount;
                if (total == 0)
                    return "--";
                else
                    return Math.Round(CorrectCount * 100 / (decimal)total, 2, MidpointRounding.AwayFromZero) + "%";
            }
        }
        /// <summary>
        /// 错误率
        /// </summary>
        public string ErrorRate
        {
            get
            {
                var total = CorrectCount + ErrorCount + PartRightCount + BlankCount;
                if (total == 0)
                    return "--";
                else
                    return Math.Round((ErrorCount + PartRightCount) * 100 / (decimal)total, 2, MidpointRounding.AwayFromZero) + "%";
            }
        }
        /// <summary>
        /// 总题数
        /// </summary>
        public int TotalQuestionCount { get; set; }
        /// <summary>
        /// 已做题数
        /// </summary>
        public int DoQuestionCount { get; set; }

        
    }
}
