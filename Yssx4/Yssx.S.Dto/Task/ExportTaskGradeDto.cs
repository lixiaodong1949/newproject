﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 导出课业统计 - 试卷作答
    /// </summary>
    public class ExportTaskGradeDto
    {
        /// <summary>
        /// 学号
        /// </summary>
        [Description("学号")]
        public string StudentNo { get; set; }
        /// <summary>
        /// 学生姓名
        /// </summary>
        [Description("学生姓名")]
        public string UserName { get; set; }
        /// <summary>
        /// 班级名称
        /// </summary>
        [Description("班级名称")]
        public string ClassName { get; set; }
        /// <summary>
        /// 得分
        /// </summary>
        [Description("得分")]
        public decimal Score { get; set; }
        /// <summary>
        /// 排名
        /// </summary>
        [Description("排名")]
        public int Ranking { get; set; }

        /// <summary>
        /// 正确题数
        /// </summary>
        [Description("正确题数")]
        public int CorrectCount { get; set; }
        /// <summary>
        /// 错误题数
        /// </summary>
        [Description("错误题数")]
        public int ErrorCount { get; set; }
        /// <summary>
        /// 未答题数
        /// </summary>
        [Description("未答题数")]
        public int BlankCount { get; set; }
        /// <summary>
        /// 做题时长
        /// </summary>
        [Description("做题时长")]
        public string UsedSeconds { get; set; }

    }
}
