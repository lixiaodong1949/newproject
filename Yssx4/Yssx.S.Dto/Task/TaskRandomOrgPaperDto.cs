﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 智能组卷
    /// </summary>
    public class TaskRandomOrgPaperDto : BaseTaskDto
    {
        /// <summary>
        /// 数据来源（1.知识点 2.课程章节）
        /// </summary>
        public int DataType { get; set; }

        /// <summary>
        /// 考察范围（知识点ID/课程章节ID）
        /// </summary>
        public List<long> SurveyScopeDetail { get; set; }

        /// <summary>
        /// 题目分类信息
        /// </summary>
        public List<TaskTopicGroupDto> TopicGroupDetail { get; set; }


        /// <summary>
        /// 题目更新年份（多个年份以逗号分隔）
        /// </summary>
        public string TopicYear { get; set; }

        /// <summary>
        /// 考试设置（1.所有人考同一套试卷 2.不同人考不同试卷）
        /// </summary>
        public TaskExamSetting TaskExamSetting { get; set; }

        /// <summary>
        /// 试卷套数
        /// </summary>
        public int ExamCount { get; set; }


        /// <summary>
        /// 班级学生明细
        /// </summary>
        public List<TaskClassDto> ClassDetail { get; set; }
    }

    /// <summary>
    /// 智能组卷 - 新
    /// </summary>
    public class TaskRandomCreatePaperDto : TaskRandomOrgPaperDto { }

}
