﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 手动组卷
    /// </summary>
    public class TaskManualOrgPaperDto : BaseTaskDto
    {
        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }


        /// <summary>
        /// 班级学生明细
        /// </summary>
        public List<TaskClassDto> ClassDetail { get; set; }

        /// <summary>
        /// 题目明细
        /// </summary>
        public List<TaskTopicDto> TopicDetail { get; set; }

    }

    /// <summary>
    /// 手动组卷 - 新
    /// </summary>
    public class TaskManualCreatePaperDto : TaskManualOrgPaperDto { }

}
