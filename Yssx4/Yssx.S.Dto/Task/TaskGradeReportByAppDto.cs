﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课业任务统计信息 - 移动端
    /// </summary>
    public class TaskGradeReportByAppDto : PageRequest
    {
        /// <summary>
        /// 课业任务ID
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; }

    }
}
