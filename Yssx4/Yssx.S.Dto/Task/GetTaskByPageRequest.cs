﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询课业任务列表 - 入参
    /// </summary>
    public class GetTaskByPageRequest : PageRequest
    {
        /// <summary>
        /// 任务类型（-1.所有 0.作业 1.任务 2.考试）
        /// </summary>
        public int TaskType { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 课程ID
        /// </summary>
        public long CourseId { get; set; } = 0;
        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; } = 0;
        /// <summary>
        /// 状态（-2.所有 0.未开始 1.进行中 2.已结束）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 是否筛选开始时间（1.开始时间 0.结束时间）
        /// </summary>
        public int IsBeginTime { get; set; } = CommonConstants.IsYes;
        /// <summary>
        /// 开始日期
        /// </summary>
        public DateTime? BeginDate { get; set; }
        /// <summary>
        /// 结束日期
        /// </summary>
        public DateTime? EndDate { get; set; }
    }

    /// <summary>
    /// 查询课业任务列表 - new
    /// </summary>
    public class GetTaskByPageAllRequest : PageRequest
    {
        /// <summary>
        /// 任务类型（-1.所有 0.作业 1.任务 2.考试）
        /// </summary>
        public int TaskType { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 课程ID
        /// </summary>
        public long OriginalCourseId { get; set; } = 0;
        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; } = 0;
        /// <summary>
        /// 状态（-2.所有 0.未开始 1.进行中 2.已结束）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 是否筛选开始时间（1.开始时间 0.结束时间）
        /// </summary>
        public int IsBeginTime { get; set; } = CommonConstants.IsYes;
        /// <summary>
        /// 开始日期
        /// </summary>
        public DateTime? BeginDate { get; set; }
        /// <summary>
        /// 结束日期
        /// </summary>
        public DateTime? EndDate { get; set; }
    }
}
