﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课业任务列表视图
    /// </summary>
    public class TaskView : BaseTaskDto
    {
        /// <summary>
        /// 任务类型名称
        /// </summary> 
        public string TaskTypeName
        {
            get { return TaskType.GetDescription(); }
            set { }
        }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public LessonsTaskStatus Status { get; set; }
        /// <summary>
        /// 试卷状态
        /// </summary>
        public LessonsTaskStatus ExamStatus
        {
            get
            {
                if (Status == LessonsTaskStatus.End)
                    return LessonsTaskStatus.End;
                var dateNow = DateTime.Now;
                if (dateNow < StartTime)
                    return LessonsTaskStatus.Wait;
                if (StartTime <= dateNow && dateNow < CloseTime)
                    return LessonsTaskStatus.Started;
                if (CloseTime <= dateNow)
                    return LessonsTaskStatus.End;
                return LessonsTaskStatus.Wait;
            }
        }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }


        /// <summary>
        /// 班级明细
        /// </summary>
        public List<TaskClassView> ClassDetail { get; set; }
        /// <summary>
        /// 任务学生总人数
        /// </summary>
        public int TotalStudentCount { get; set; }
        /// <summary>
        /// 已交卷人数
        /// </summary>
        public int SubmitCount { get; set; }

    }


    /// <summary>
    /// 课业任务 - 班级信息
    /// </summary>
    public class TaskClassView
    {
        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 学生数量
        /// </summary>
        public int StudentCount { get; set; }
    }
}
