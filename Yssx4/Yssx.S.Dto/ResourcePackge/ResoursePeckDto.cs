﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.ResourcePackge
{
    /// <summary>
    /// 套餐信息
    /// </summary>
    public class ResoursePeckDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 套餐名称
        /// </summary>
        public string PackgeName { get; set; }

        /// <summary>
        /// 学历
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }

    /// <summary>
    /// 套餐详细列表
    /// </summary>
    public class ResourcePackDetailDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 套餐Id
        /// </summary>
        public long ResourceId { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 资源模块名称
        /// </summary>
        public string ModuleName { get; set; }

        /// <summary>
        /// 资源模块类型
        /// </summary>
        public int ModuleType { get; set; }

        /// <summary>
        /// 入学年级
        /// </summary>
        //public int Year { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }

    public class AddResourceDetailsDto
    {
        /// <summary>
        /// 套餐Id
        /// </summary>
        public long ResourceId { get; set; }

        /// <summary>
        /// 案例集合
        /// </summary>

       public List<ResourceCaseDto> CaseList { get; set; }

        /// <summary>
        /// 资源模块名称
        /// </summary>
        public string ModuleName { get; set; }

        /// <summary>
        /// 资源模块类型
        /// </summary>
        public int ModuleType { get; set; }

        /// <summary>
        /// 入学年级
        /// </summary>
        //public int Year { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }

    public class ResourceCaseDto
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }
    }

    public class ModuleDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class BindingResourseDto
    {
        public long ResourceId { get; set; }
   
        public long SchoolId { get; set; }

        public string ResourceName { get; set; }

        public int Sort { get; set; }

        public int Year { get; set; }
    }
}
