﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课堂测验任务请求实体
    /// </summary>
    public class CourseTestTaskRequestModel
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程上课Id
        /// </summary>
        public long TeachCourseId { get; set; }

        /// <summary>
        /// 任务来源 0:课程 1:案例
        /// </summary>
        public CourseTestTaskSource TaskSource { get; set; }

        /// <summary>
        /// 选中课程或案例Id
        /// </summary>
        public long CourseOrCaseId { get; set; }

        /// <summary>
        /// 0:实习 1:实训
        /// </summary>
        public Nullable<int> ModuleType { get; set; }

        /// <summary>
        /// 测验题目列表
        /// </summary>
        public List<TestTopicRequestModel> TopicList { get; set; } = new List<TestTopicRequestModel>();

        /// <summary>
        /// 测验班级列表
        /// </summary>
        public List<TestClassRequestModel> ClassList { get; set; } = new List<TestClassRequestModel>();
    }

    /// <summary>
    /// 测验题目实体
    /// </summary>
    public class TestTopicRequestModel
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long TopicId { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 题目来源 0:课程 1:案例
        /// </summary>
        public CoursePrepareTopicSource TopicSource { get; set; }
    }

    /// <summary>
    /// 测验班级实体
    /// </summary>
    public class TestClassRequestModel
    {
        /// <summary>
        /// 班级Id
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }
}
