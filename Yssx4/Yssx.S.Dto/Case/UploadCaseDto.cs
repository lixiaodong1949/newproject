﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto//传入参数
{
    #region 上传案例
    /// <summary>
    /// 添加 更新 上传案例
    /// </summary>
    public class UploadCaseDto
    {
        /// <summary>
        /// 案例等级 1A 2B 3C 4D 5E
        /// </summary>
        //public int? CaseGrade { get; set; }
        /// <summary>
        /// 主键Id
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 类型(预留可不传) 0.PC 1.APP
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 编辑状态类型 0专业组（编辑不更新状态）
        /// </summary>
        public int? StatusType { get; set; }
        /// <summary>
        /// 状态 保存后默认0待上架 1上架待审 2已上架 3已拒绝
        /// </summary>
        public CaseStatus? CaseStatus { get; set; }
        /// <summary>
        /// 案例类型 0会计核算 1财务管理 2审计 3税务
        /// </summary>
        //public Classification? Classification { get; set; }
        public int? Classification { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 案例概述
        /// </summary>
        public string CaseSummary { get; set; }
        /// <summary>
        /// 提出方案
        /// </summary>
        public string ProposedScheme { get; set; }
        /// <summary>
        /// 执行方案
        /// </summary>
        public string Implementation { get; set; }
        /// <summary>
        /// 执行结果
        /// </summary>
        public string ExecutionResult { get; set; }
        /// <summary>
        /// 决策依据
        /// </summary>
        public string DecisionMakingBasis { get; set; }
        /// <summary>
        /// 案例概述图片 {"list":[{"name":"demo1.jpg","url":"http://cdn-ccc32.jpg!w1200"}]}
        /// </summary>
        public string CaseSummaryImg { get; set; } = "{\"list\":[]}";
        /// <summary>
        /// 提出方案图片 
        /// </summary>
        public string ProposedSchemeImg { get; set; } = "{\"list\":[]}";
        /// <summary>
        /// 执行方案图片 
        /// </summary>
        public string ImplementationImg { get; set; } = "{\"list\":[]}";
        /// <summary>
        /// 执行结果图片 
        /// </summary>
        public string ExecutionResultImg { get; set; } = "{\"list\":[]}";
        /// <summary>
        /// 决策依据图片 
        /// </summary>
        public string DecisionMakingBasisImg { get; set; } = "{\"list\":[]}";
        /// <summary>
        /// 资料上传 {"list":[{"name":"demo1.jpg","url":"http://cdn-ccc32.jpg!w1200"}]}
        /// </summary>
        public string DataUpload { get; set; } = "{\"list\":[]}";

        /// <summary>
        /// 封面
        /// </summary>
        public string Cover { get; set; }

        /// <summary>
        /// 摘要
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// 价值点
        /// </summary>
        public string ValuePoint { get; set; }

        /// <summary>
        /// 标签  {"list":[{"name":"失业保险金","id":0},{"name":"冒领保险金","id":0}]}
        /// </summary>
        public string CaseLabel { get; set; }

        /// <summary>
        /// 版权 true阅读 false没阅读
        /// </summary>
        public bool Copyright { get; set; }
    }

    /// <summary>
    /// 获取 上传案例
    /// </summary>
    public class UploadCaseQueryDao : PageRequest
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 状态 0待上架 1上架待审 2已上架 3已拒绝 4全部(不传也全部)
        /// </summary>
        public int? Status { get; set; }

        /// <summary>
        /// 案例类型 0会计核算 1财务管理 2审计 3税务 4全部(不传也全部)
        /// </summary>
        public int? Classification { get; set; }

        /// <summary>
        /// 来源类型 0云自营 1其他  2全部(不传也全部)
        /// </summary>
        public int? SourceType { get; set; }

        /// <summary>
        /// 排序(先用降序) 首页专用
        /// null/0(降序)默认Id   
        /// 1/11(降序/升序)点赞数   
        /// 2/22(降序/升序)使用次数   
        /// 3/33(降序/升序)更新时间
        /// 4/44(降序/升序)阅读次数
        /// </summary>
        public int? Sort { get; set; }

        /// <summary>
        /// 列表类型 null用户 0专业组 1购买 2PC商城页 3APP商城页
        /// </summary>
        public int? Type { get; set; }
    }

    /// <summary>
    /// 获取 无用户上传案例
    /// </summary>
    public class NoUserUploadCaseQueryDao : PageRequest
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 排序(先用降序) 首页专用
        /// null/0(降序)默认Id   
        /// 1/11(降序/升序)点赞数   
        /// 2/22(降序/升序)使用次数   
        /// 3/33(降序/升序)更新时间
        /// 4/44(降序/升序)阅读次数
        /// </summary>
        public int? Sort { get; set; }

        /// <summary>
        /// 列表类型 2PC商城页 3APP商城页
        /// </summary>
        public int? Type { get; set; }
    }

    /// <summary>
    /// 上架（发布,添加标签）
    /// </summary>
    public class CaseRackDao
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long Id { get; set; }

        ///// <summary>
        ///// 案例标签  {"list":[{"name":"测试8888","id":982580666236938}]}
        ///// </summary>
        //public string CaseLabel { get; set; }
    }
    #endregion

    #region 其他
    /// <summary>
    /// 标签
    /// </summary>
    public class UploadCaseTagLibDao : PageRequest
    {
        /// <summary>
        /// 要搜索的标签名字 模糊匹配
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 点赞
    /// </summary>
    public class CaseFabulousDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 点赞(产品说只用1点赞) 0取消点赞 1点赞
        /// </summary>
        public int? Fabulous { get; set; }
    }

    /// <summary>
    /// 证书
    /// </summary>
    public class CertificateDao
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 证书编号
        /// </summary>
        public string CertificateNumber { get; set; }
        /// <summary>
        /// 证书图片 格式：http://xxxxxxxxxx.png
        /// </summary>
        public string Certificate { get; set; }

    }

    /// <summary>
    /// 获取 案例标签同步标签库
    /// </summary>
    public class CaseTagDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 交易记录
    /// </summary>
    public class TransactionRecordsDao : PageRequest
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long Id { get; set; }
    }

    /// <summary>
    /// 添加 更新 批注
    /// </summary>
    public class CommentsDao
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long? Id { get; set; }

        /// <summary>
        /// 第一次新增批组模块传案例Id,后面在该批组模块追加拿获取的Id放Parent新增
        /// </summary>
        public long Parent { get; set; }

        ///// <summary>
        ///// 案例Id
        ///// </summary>
        //public long CaseId { get; set; }

        /// <summary>
        /// Y坐标（新增批组模块要，后面追加批注可以要也可以不要）
        /// </summary>
        public int YCoordinate { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Comment { get; set; }
    }

    /// <summary>
    /// 删除 批注
    /// </summary>
    public class DeleteCommentsDao
    {
        /// <summary>
        /// Id(批组Id追加Id和案例Id)
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 1删除批注(Id栏位传批组Id和追加Id)
        /// 2清空批注（Id栏位传案例Id）
        /// </summary>
        public int type { get; set; }
    }
    #endregion

    #region 扩展
    /// <summary>
    /// 获取 购买案例
    /// </summary>
    public class PurchUploadCaseDao : PageRequest
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 来源类型（不传查全部） 0云自营 1其他 2全部(不传也全部)
        /// </summary>
        public CaseSourceType? SourceType { get; set; }
    }

    /// <summary>
    /// 批量 上架 删除 违规处理
    /// </summary>
    public class BatchUploadCaseDao
    {
        /// <summary>
        /// 主键Id 集合逗号隔开
        /// </summary>
        public List<long> Id { get; set; }

        /// <summary>
        /// 类型 1上架 2删除 3违规处理
        /// </summary>
        public int? Type { get; set; }
    }
    #endregion
}
