﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto //返回参数
{
    #region 上传案例
    /// <summary>
    /// 上传案例
    /// </summary>
    public class UploadCaseResult
    {
        /// <summary>
        /// 上传案例
        /// </summary>

        public List<UploadCaseList> UploadCaseResponse { get; set; }
    }
    /// <summary>
    /// 上传案例--列表 通
    /// </summary>
    public class UploadCaseList
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Cover { get; set; }// = "{\"list\":[]}";

        /// <summary>
        /// 价值点
        /// </summary>
        public string ValuePoint { get; set; }

        /// <summary>
        /// 摘要
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string CertificateNumber { get; set; }

        /// <summary>
        /// 证书图片 格式：http://xxxxxxxxxx.png
        /// </summary>
        public string Certificate { get; set; }

        /// <summary>
        /// 点赞 false没点赞 true已点赞 (zan)
        /// </summary>
        public bool FabulousWhether { get; set; }

        /// <summary>
        /// 点赞总数
        /// </summary>
        public int Fabulous { get; set; }

        /// <summary>
        /// 拒绝时间
        /// </summary>
        public DateTime? RejectionTime { get; set; }

        /// <summary>
        /// 上架时间
        /// </summary>
        public DateTime? ShelfTime { get; set; }

        /// <summary>
        /// 案例等级 1A 2B 3C 4D 5E
        /// </summary>
        public CaseGrade? CaseGrade { get; set; }

        /// <summary>
        /// 购买时间
        /// </summary>
        public DateTime? PurchaseTime { get; set; }

        /// <summary>
        /// 购买类型 NULL没买0赠送1购买
        /// </summary>
        public int? PurchaseType { get; set; }

        /// <summary>
        /// 购买状态 null未支付 2.已付款
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 上传者
        /// </summary>
        public string Uploads { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 状态 保存后默认0待上架 1上架待审 2已上架 3已拒绝  4全部
        /// </summary>
        public int? CaseStatus { get; set; }

        /// <summary>
        /// 提交时间
        /// </summary>
        public DateTime? SubmissionTime { get; set; }

        /// <summary>
        /// 案例标签 {"list":[{"name":"测试8888","id":982580666236938}]}
        /// </summary>
        public string CaseLabel { get; set; }

        /// <summary>
        /// 阅读数量
        /// </summary>
        public int Read { get; set; }

        /// <summary>
        /// 案例类型 0会计核算 1财务管理 2审计 3税务 4全部
        /// </summary>
        public int? Classification { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 案例概述
        /// </summary>
        public string CaseSummary { get; set; }

        /// <summary>
        /// 提出方案
        /// </summary>
        public string ProposedScheme { get; set; }

    }

    /// <summary>
    /// 上传案例--详情
    /// </summary>
    //[Table(Name = "yssx_uploadcase")]
    public class UploadCaseResponse
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 版权 true阅读 false没阅读
        /// </summary>
        public bool Copyright { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public long CreateBy { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        //[Column(IsIgnore = true)]
        public string Cover { get; set; } //= "{\"list\":[]}";

        /// <summary>
        /// 价值点
        /// </summary>
        public string ValuePoint { get; set; }

        /// <summary>
        /// 摘要
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string CertificateNumber { get; set; }

        /// <summary>
        /// 证书图片 格式：http://xxxxxxxxxx.png
        /// </summary>
        public string Certificate { get; set; }

        /// <summary>
        /// 点赞 false没点赞 true已点赞
        /// </summary>
        //[Column(IsIgnore = true)]
        public bool FabulousWhether { get; set; }

        /// <summary>
        /// 作者头像
        /// </summary>
        public string UserAvatar { get; set; }

        /// <summary>
        /// 作者案例数量
        /// </summary>
        public int UserCaseNum { get; set; }

        /// <summary>
        /// 点赞总数
        /// </summary>
        public int Fabulous { get; set; }

        /// <summary>
        /// 拒绝时间
        /// </summary>
        public DateTime? RejectionTime { get; set; }

        /// <summary>
        /// 上架时间
        /// </summary>
        public DateTime? ShelfTime { get; set; }

        /// <summary>
        /// 案例等级 1A 2B 3C 4D 5E
        /// </summary>
        public CaseGrade? CaseGrade { get; set; }

        /// <summary>
        /// 购买时间
        /// </summary>
        //[Column(IsIgnore = true)]
        public DateTime? PurchaseTime { get; set; }

        /// <summary>
        /// 购买类型 NULL没买 0赠送 1购买
        /// </summary>
        //[Column(IsIgnore = true)]
        public int? PurchaseType { get; set; }

        /// <summary>
        /// 购买状态（0.未支付 1.已取消 2.已付款）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 上传者
        /// </summary>
        public string Uploads { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// 状态 保存后默认0待上架 1上架待审 2已上架 3已拒绝  4全部
        /// </summary>
        public int? CaseStatus { get; set; }

        /// <summary>
        /// 提交时间
        /// </summary>
        public DateTime? SubmissionTime { get; set; }

        /// <summary>
        /// 案例标签 {"list":[{"name":"测试8888","id":982580666236938}]}
        /// </summary>
        public string CaseLabel { get; set; }

        /// <summary>
        /// 阅读数量
        /// </summary>
        public int Read { get; set; }

        /// <summary>
        /// 案例类型 0会计核算 1财务管理 2审计 3税务 4全部
        /// </summary>
        public int? Classification { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 案例概述
        /// </summary>
        public string CaseSummary { get; set; }

        /// <summary>
        /// 提出方案
        /// </summary>
        public string ProposedScheme { get; set; }

        /// <summary>
        /// 执行方案
        /// </summary>
        public string Implementation { get; set; }

        /// <summary>
        /// 执行结果
        /// </summary>]
        public string ExecutionResult { get; set; }

        /// <summary>
        /// 决策依据
        /// </summary>
        public string DecisionMakingBasis { get; set; }

        /// <summary>
        /// 案例概述图片 {"list":[{"url":"","name":""}]}
        /// </summary>
        public string CaseSummaryImg { get; set; }

        /// <summary>
        /// 提出方案图片  {"list":[{"url":"","name":""}]}
        /// </summary>
        public string ProposedSchemeImg { get; set; }

        /// <summary>
        /// 执行方案图片  {"list":[{"url":"","name":""}]}
        /// </summary>
        public string ImplementationImg { get; set; }

        /// <summary>
        /// 执行结果图片  {"list":[{"url":"","name":""}]}
        /// </summary>
        public string ExecutionResultImg { get; set; }

        /// <summary>
        /// 决策依据图片  {"list":[{"url":"","name":""}]}
        /// </summary>
        public string DecisionMakingBasisImg { get; set; }

        /// <summary>
        /// 资料上传 {"list":[{"id":"","name":""}]}
        /// </summary>
        public string DataUpload { get; set; }

        /// <summary>
        /// 审核状态(1，审核成功，2，审核拒绝)
        /// </summary>
        public int? AuditStatus { get; set; }

        /// <summary>
        /// 审核说明
        /// </summary>
        public string AuditStatusExplain { get; set; }

        /// <summary>
        /// 是否上架(1，上架，2，上架拒绝)
        /// </summary>
        public int? Uppershelf { get; set; }

        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; }
    }

    /// <summary>
    /// 商城上传案例列表--无用户登陆
    /// </summary>
    public class NoUserUploadCaseListResponse
    {
        /// <summary>
        /// 主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Cover{ get; set; } //= "{\"list\":[]}";

        /// <summary>
        /// 摘要
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// 标签  {"list":[{"name":"失业保险金","id":0},{"name":"冒领保险金","id":0}]}
        /// </summary>
        public string CaseLabel { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 案例概述
        /// </summary>
        public string CaseSummary { get; set; }

        /// <summary>
        /// 阅读数量
        /// </summary>
        public int Read { get; set; }

        /// <summary>
        /// 点赞
        /// </summary>
        public int Fabulous { get; set; }

        /// <summary>
        /// 上传者
        /// </summary>
        public string Uploads { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

    }

    /// <summary>
    /// 商城上传案例预览--无用户登陆
    /// </summary>
    public class NoUserUploadCaseResponse
    {
        /// <summary>
        /// 主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 案例概述
        /// </summary>
        public string CaseSummary { get; set; }

        /// <summary>
        /// 提出方案
        /// </summary>
        public string ProposedScheme { get; set; }

        /// <summary>
        /// 案例标签
        /// </summary>
        public string CaseLabel { get; set; } = "{\"list\":[]}";

        /// <summary>
        /// 案例概述图片
        /// </summary>
        public string CaseSummaryImg { get; set; } = "{\"list\":[]}";

        /// <summary>
        /// 提出方案图片  
        /// </summary>
        public string ProposedSchemeImg { get; set; } = "{\"list\":[]}";

        /// <summary>
        /// 执行方案图片  
        /// </summary>
        public string ImplementationImg { get; set; } = "{\"list\":[]}";

        /// <summary>
        /// 执行结果图片  
        /// </summary>
        public string ExecutionResultImg { get; set; } = "{\"list\":[]}";

        /// <summary>
        /// 决策依据图片  
        /// </summary>
        public string DecisionMakingBasisImg { get; set; } = "{\"list\":[]}";

        /// <summary>
        /// 资料上传
        /// </summary>
        public string DataUpload { get; set; } = "{\"list\":[]}";

        /// <summary>
        /// 案例类型 0会计核算 1财务管理 2审计 3税务 4全部(不传也全部)
        /// </summary>
        public int? Classification { get; set; }

        /// <summary>
        /// 状态 保存后默认0待上架 1上架待审 2已上架 3已拒绝  4全部(不传也全部)
        /// </summary>
        public int? CaseStatus { get; set; }

        /// <summary>
        /// 阅读数量
        /// </summary>
        public int Read { get; set; }

        /// <summary>
        /// 点赞
        /// </summary>
        public int Fabulous { get; set; }

        /// <summary>
        /// 上传者
        /// </summary>
        public string Uploads { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string CertificateNumber { get; set; }

        /// <summary>
        /// 证书图片 格式：http://xxxxxxxxxx.png
        /// </summary>
        public string Certificate { get; set; }

        /// <summary>
        /// 作者头像
        /// </summary>
        public string UserAvatar { get; set; }

        /// <summary>
        /// 作者案例数量
        /// </summary>
        public int UserCaseNum { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public long CreateBy { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

    }
    #endregion

    #region 好文推存
    /// <summary>
    /// 好文推存
    /// </summary>
    public class GoodArticle
    {
        /// <summary>
        /// 案例总篇数
        /// </summary>
        public int Piece { get; set; }
        /// <summary>
        /// 热门案例
        /// </summary>
        public List<HotCase> HotCase { get; set; }
        /// <summary>
        /// 最新更新
        /// </summary>
        public List<LatestUpdates> LatestUpdates { get; set; }
        /// <summary>
        /// 点赞案例
        /// </summary>
        public List<PointPraiseCase> PointPraiseCase { get; set; }

    }

    /// <summary>
    /// 热门案例
    /// </summary>
    public class HotCase
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 阅读数量
        /// </summary>
        public int Read { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

    }

    /// <summary>
    /// 最新更新
    /// </summary>
    public class LatestUpdates
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

    }

    /// <summary>
    /// 点赞案例
    /// </summary>
    public class PointPraiseCase
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 点赞
        /// </summary>
        public int Fabulous { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
    #endregion

    #region 其他
    /// <summary>
    /// 案例标签
    /// </summary>
    public class UploadCaseTag
    {
        /// <summary>
        /// 标签Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 标签名字
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 交易记录
    /// </summary>
    public class TransactionRecords
    { 
        /// <summary>
        /// 交易人数
        /// </summary>
        public int NumberOfTraders { get; set; }

        /// <summary>
        /// 教师
        /// </summary>
        public string Teacher { get; set; }

        /// <summary>
        /// 学生
        /// </summary>
        public string Student { get; set; }

        /// <summary>
        /// 其他
        /// </summary>
        public string Other { get; set; }

        /// <summary>
        /// 交易明细
        /// </summary>
        public List<TransactionDetails> TransactionDetails { get; set; }
    }

    /// <summary>
    /// 交易明细
    /// </summary>
    public class TransactionDetails
    {
        /// <summary>
        /// 购买顾客
        /// </summary>
        public string BuyingCustomers { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// 所属学校
        /// </summary>
        public string School { get; set; }

        /// <summary>
        /// 购买时间
        /// </summary>
        public DateTime PurchaseTime { get; set; }
    }

    /// <summary>
    /// 获取 案例批注
    /// </summary>
    public class CommentsResponse
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Y坐标
        /// </summary>
        public string YCoordinate { get; set; }

        ///// <summary>
        ///// 批注内容
        ///// </summary>
        //public string Comment { get; set; }

        ///// <summary>
        ///// 创建时间
        ///// </summary>
        //public DateTime CreateTime { get; set; }

        ///// <summary>
        ///// 头像
        ///// </summary>
        //public string Photo { get; set; }

        ///// <summary>
        ///// 名字
        ///// </summary>
        //public string RealName { get; set; }

        /// <summary>
        /// 批注 详情(前端要多包一层)
        /// </summary>
        public List<CommentsDetails> CommentsDetails { get; set; }
    }

    public class CommentsDetails
    {
        /// <summary>
        /// 批组主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 批注 详情
        /// </summary>
        public List<CommentsDetails1> CommentsDetails1 { get; set; }
    }

    /// <summary>
    /// 批注 详情
    /// </summary>
    public class CommentsDetails1
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        ///// <summary>
        ///// 追加Id
        ///// </summary>
        //public long Parent { get; set; }

        /// <summary>
        /// 批注内容
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// 名字
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        public int Role { get; set; }
    }
    #endregion

    #region 扩展
    /// <summary>
    /// 竞赛用户
    /// </summary>
    public class SkillUser : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 邮箱
        /// </summary>
        [Column(Name = "Email", DbType = "varchar(255)", IsNullable = true)]
        public string Email { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [Column(Name = "MobilePhone", DbType = "varchar(255)", IsNullable = true)]
        public string MobilePhone { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [Column(Name = "NikeName", DbType = "varchar(255)", IsNullable = true)]
        public string NikeName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Column(Name = "Password", DbType = "varchar(255)", IsNullable = true)]
        public string Password { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        [Column(Name = "Photo", DbType = "varchar(255)", IsNullable = true)]
        public string Photo { get; set; }

        /// <summary>
        /// QQ
        /// </summary>
        [Column(Name = "QQ", DbType = "varchar(255)", IsNullable = true)]
        public string QQ { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        [Column(Name = "RealName", DbType = "varchar(255)", IsNullable = true)]
        public string RealName { get; set; }

        /// <summary>
        /// 客户端id
        /// </summary>
        public Guid ClientId { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        public Status Status { get; set; } = Status.Disable;

        /// <summary>
        /// 用户名称
        /// </summary>
        [Column(Name = "UserName", DbType = "varchar(255)", IsNullable = true)]
        public string UserName { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary> 
        public UserTypeEnums UserType { get; set; }

        /// <summary>
        /// 微信号
        /// </summary>
        [Column(Name = "WeChat", DbType = "varchar(255)", IsNullable = true)]
        public string WeChat { get; set; }

        /// <summary>
        /// 身份证号
        /// </summary>
        [Column(Name = "IdNumber", DbType = "varchar(20)", IsNullable = true)]
        public string IdNumber { get; set; }

        /// <summary>
        /// 其他联系方式
        /// </summary>
        [Column(Name = "OtherContacts", DbType = "varchar(255)", IsNullable = true)]
        public string OtherContacts { get; set; }

        /// <summary>
        /// 注册类型(1,用户激活，2，后台添加)
        /// </summary>
        public int RegistrationType { get; set; }
    }

    /// <summary>
    /// 技能用户
    /// </summary>
    public class YssxJnccUser : TenantBizBaseEntity<long>
    {
        [Column(Name = "Email", DbType = "varchar(255)", IsNullable = true)]
        public string Email { get; set; }


        [Column(Name = "MobilePhone", DbType = "varchar(255)", IsNullable = true)]
        public string MobilePhone { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; }


        [Column(Name = "NikeName", DbType = "varchar(255)", IsNullable = true)]
        public string NikeName { get; set; }


        [Column(Name = "Password", DbType = "varchar(255)", IsNullable = true)]
        public string Password { get; set; }


        [Column(Name = "Photo", DbType = "varchar(255)", IsNullable = true)]
        public string Photo { get; set; }


        [Column(Name = "QQ", DbType = "varchar(255)", IsNullable = true)]
        public string QQ { get; set; }


        [Column(Name = "RealName", DbType = "varchar(255)", IsNullable = true)]
        public string RealName { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        public Status Status { get; set; } = Status.Disable;

        /// <summary>
        /// 
        /// </summary>
        [Column(Name = "UserName", DbType = "varchar(255)", IsNullable = true)]
        public string UserName { get; set; }

        /// <summary>
        /// 
        /// </summary> 
        public UserTypeEnums UserType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(Name = "WeChat", DbType = "varchar(255)", IsNullable = true)]
        public string WeChat { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        [Column(Name = "IdNumber", DbType = "varchar(20)", IsNullable = true)]
        public string IdNumber { get; set; }
        /// <summary>
        /// 其他联系方式
        /// </summary>
        [Column(Name = "OtherContacts", DbType = "varchar(255)", IsNullable = true)]
        public string OtherContacts { get; set; }

        /// <summary>
        /// 是否已激活 true 激活 false 未激活
        /// </summary>
        [Column(Name = "IsActivate")]
        public bool IsActivate { get; set; } = false;

        /// <summary>
        /// 激活时间
        /// </summary>
        public DateTime ActivateTime { get; set; }

    }
    #endregion
}
