﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    public class ProductDto
    {
        public long Id { get; set; }

        public long CaseId { get; set; }

        public string CaseName { get; set; }

        public decimal OriginalPrice { get; set; }

        public decimal DiscountPrice { get; set; }

        public int OrderType { get; set; }

        public string EndDate { get; set; }
    }
}
