﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 行业
    /// </summary>
    public class IndustryDto : BaseDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 图片附件
        /// </summary>
        public string FileUrl { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }

    }
}
