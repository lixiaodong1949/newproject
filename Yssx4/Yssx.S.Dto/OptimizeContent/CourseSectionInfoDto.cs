﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程练习/预览dto
    /// </summary>
    public class CourseSectionInfoDto
    { 
        /// <summary>
        /// 课程id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 章节总数
        /// </summary>
        public int SectionCount { get; set; }
        /// <summary>
        /// 习题总数
        /// </summary>
        public long QuestionCount { get; set; }

        /// <summary>
        /// 课件总数
        /// </summary>
        public int CoursewareCount { get; set; }

        /// <summary>
        /// 视频总数
        /// </summary>
        public int VideoCount { get; set; }

        /// <summary>
        /// 教案总数
        /// </summary>
        public int TeachingPlanCount { get; set; }

        /// <summary>
        /// 教材总数
        /// </summary>
        public SectionFileInfoDto CourseJc { get; set; }
        /// <summary>
        /// 章节及附件信息
        /// </summary>
        public List<SectionInfoListDto> SectionInfoList { get; set; }
    }

    /// <summary>
    /// 章节列表dto
    /// </summary>
    public class SectionInfoListDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string SectionName { get; set; }

        /// <summary>
        /// 章节标题
        /// </summary>
        public string SectionTitle { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 父级Id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 0:章，1：节
        /// </summary>
        public int SectionType { get; set; }
        ///// <summary>
        ///// 章节对应附件集合
        ///// </summary>
        //public List<SectionFileDto> SectionFiles { get; set; }
        public string Title { get { return this.SectionTitle; } }
        public int Level { get; set; }

        public bool ShowChildren { get; set; }=false;

        public bool showTitleInput { get; set; } = false;
        /// <summary>
        /// 子集
        /// </summary>
        public List<SectionInfoListDto> Children { get; set; }

        /// <summary>
        /// 课件
        /// </summary>
        public List<SectionFileInfoDto> CoursewareList { get; set; }


        /// <summary>
        /// 视频
        /// </summary>
        public List<SectionFileInfoDto> VideoList { get; set; }

        /// <summary>
        /// 教案
        /// </summary>
        public List<SectionFileInfoDto> TeachingPlanList { get; set; }
        /// <summary>
        /// 思政案例
        /// </summary>
        public List<SectionFileInfoDto> SZCaseList { get; set; }
        /// <summary>
        /// 是否有题目 true 有  false 无
        /// </summary>
        public bool IsExistTopic { get; set; }
        /// <summary>
        /// 题目 false开 true关
        /// </summary>
        public bool State { get; set; }

        /// <summary>
        /// 答案 false开 true关
        /// </summary>
        public bool AnswerState { get; set; }
    }
    /// <summary>
    /// 附件Dto
    /// </summary>
    public class SectionFileInfoDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string SectionName { get; set; }

        /// <summary>
        /// title
        /// </summary>
        public string SectionTitle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件类型0:Excel,1:Word,2:PDF,3:PPT,4:Img,5:ZIP,6:视频
        /// </summary>
        public int FilesType { get; set; }

        /// <summary>
        /// 文件大小
        /// </summary>
        public int FileSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 1:课件 2:教案 3:视频 4:授课计划 5:素材(课程备课) 6:思政案例 7:教材
        /// </summary>
        public int SectionType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long CourseFileId { get; set; }
        /// <summary>
        /// 来源0：课件库.1:新增课件
        /// </summary>
        public int FromType { get; set; }

        /// <summary>
        /// 是否附件  区分超链接
        /// </summary>
        public bool IsFile { get; set; } = true;
        /// <summary>
        /// 知识点名称集合
        /// </summary>
        public string KnowledgePointNames { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
