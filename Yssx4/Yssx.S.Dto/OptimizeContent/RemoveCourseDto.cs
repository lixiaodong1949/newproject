﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto.OptimizeContent
{
    /// <summary>
    /// 移除课程dto
    /// </summary>
    public class RemoveCourseDto
    {
        /// <summary>
        /// 课程名称
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 0 移除 1引用
        /// </summary>
        public int OperateType { get; set; }
    }

}
