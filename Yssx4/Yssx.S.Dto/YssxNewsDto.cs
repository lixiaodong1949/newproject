﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    public class YssxNewsDto
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string NewsUrl { get; set; }

        public string ImgUrl { get; set; }

        public int Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ContentType { get; set; }

        /// <summary>
        /// 0：所有平台 1:技能抽查,2:技能竞赛,3:专一网
        /// </summary>
        public int Platform { get; set; }

        public int Sort { get; set; }
    }
}
