﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 通关记录
    /// </summary>
    public class JobTrainingEndRecordDto
    {
        /// <summary>
        /// 作答记录ID
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 通关时间
        /// </summary>
        public DateTime SubmitTime { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamPaperScore { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string SN { get; set; }

        /// <summary>
        /// 证书地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 证书状态（0.不可生成 1.未生成 2.已生成）
        /// </summary>
        public int ProveStatus
        {
            get
            {
                var rQualifiedScore = ExamPaperScore * (decimal)0.6;
                if (Score < rQualifiedScore)
                    return 0;
                else if (!string.IsNullOrEmpty(SN))
                    return 2;
                else
                    return 1;
            }
        }



    }
}
