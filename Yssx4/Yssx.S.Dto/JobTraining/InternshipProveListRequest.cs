﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询实训证书列表
    /// </summary>
    public class InternshipProveListRequest : PageRequest
    {
        /// <summary>
        /// 实训分类（0.会计实训 1.出纳实训 2.大数据 3.业财税）
        /// </summary>
        public int SxType { get; set; } = -1;

        /// <summary>
        /// 所属行业
        /// </summary>
        public long Industry { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

    }
}
