﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 实习证书列表
    /// </summary>
    public class InternshipProveListDto
    {
        /// <summary>
        /// 证书编号
        /// </summary>
        public string SN { get; set; }

        /// <summary>
        /// 证书地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 通关时间
        /// </summary>
        public DateTime SubmitTime { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 实训分类名称
        /// </summary>
        public string SxTypeName { get; set; }

        /// <summary>
        /// 所属行业名称
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamPaperScore { get; set; }

        /// <summary>
        /// 级别名称（0.合格[60-69] 1.良好[70-79] 2.优秀[>=80]）
        /// </summary>
        public string LevelName { get; set; }

        /// <summary>
        /// 证书生效日期
        /// </summary>
        public DateTime IssueTime { get; set; }
    }
}
