﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 通关记录 - 入参
    /// </summary>
    public class JobTrainingEndRecordRequest : PageRequest
    {
        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

    }
}
