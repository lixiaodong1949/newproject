﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 实训列表查询 - 我购买的实训
    /// </summary>
    public class GetPaidJobInternshipListDto
    {
        #region 和自主训练保持一致
        /*
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 案例简介
        /// </summary>
        public string ShortDesc { get; set; }

        /// <summary>
        /// 有效期开始
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 有效期结束
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// 所属行业ID
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// 所属行业名称
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 交卷次数
        /// </summary>
        public int ExamCount { get; set; }

        /// <summary>
        /// 0:会计实训 1:出纳实训 2:大数据 3:业财税 4:中职高考冲刺模拟题
        /// </summary>
        public Nullable<int> CaseType { get; set; }
        */
        #endregion

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 试卷名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 参考次数
        /// </summary>
        public int ExamCount { get; set; }

        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 总题数
        /// </summary>
        public int TotalQuestionCount { get; set; }

        /// <summary>
        /// 已做题数
        /// </summary>
        public int DoQuestionCount { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 创建年份
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public StudentExamStatus Status { get; set; }

        /// <summary>
        /// 卷面形式（可空 0:核算会计 1:方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 公司背景图
        /// </summary>
        public string BGFileUrl { get; set; }

        /// <summary>
        /// 实训描述
        /// </summary>
        public string SxInfo { get; set; }

        /// <summary>
        /// 所属行业
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 所属行业ID
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// 0:会计实训 1:出纳实训 2:大数据 3:业财税 4:中职高考冲刺模拟题
        /// </summary>
        public Nullable<int> CaseType { get; set; }
    }

    /// <summary>
    /// 实训列表查询 - 我购买的实训
    /// </summary>
    public class GetPaidJobInternshipListRequest : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

    }


}
