﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    public class ReadStatisticsDto
    {
        public long UserId { get; set; }

        public string Name { get; set; }

        public long Count { get; set; }
    }
}
