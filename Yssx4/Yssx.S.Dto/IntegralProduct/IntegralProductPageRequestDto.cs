using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 积分商品分页查询 请求Dto
    /// </summary>
    public class IntegralProductPageRequestDto : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 是否已经上架:true 上架  false 下架
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// 类型：1：实物 2虚拟物品
        /// </summary>
        public int Type { get; set; }
    }
}