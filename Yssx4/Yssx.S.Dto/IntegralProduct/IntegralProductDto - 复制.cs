namespace Yssx.S.Dto
{
    /// <summary>
    /// 积分商品 基本详情 Dto
    /// </summary>
    public class IntegralProductInfoDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>        
        public string Name { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string Logo { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 类型：1：实物 2虚拟物品
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 详情内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 兑换积分
        /// </summary>
        public int Integral { get; set; }

        /// <summary>
        /// 库存
        /// </summary>
        public int Inventory { get; set; }
    }
}