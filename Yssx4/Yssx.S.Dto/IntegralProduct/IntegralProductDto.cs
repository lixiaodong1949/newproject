using System;
using System.ComponentModel.DataAnnotations;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 积分商品 Dto
    /// </summary>
    public class IntegralProductDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>        
        [Required(ErrorMessage ="名称[Name]不能为空")]
        public string Name { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string Logo { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 类型：1：实物 2虚拟物品
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 详情内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 兑换积分
        /// </summary>
        public int Integral { get; set; }

        /// <summary>
        /// 库存
        /// </summary>
        public int Inventory { get; set; }

        /// <summary>
        /// 是否已经上架:true 上架  false 下架
        /// </summary>
        public bool IsActive { get; set; }

        ///// <summary>
        ///// 上架时间
        ///// </summary>
        //public DateTime ActiveDateTime { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}