﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    public class SetTaskStatusDto
    {
        public long TaskId { get; set; }

        public DateTime EndDate { get; set; }

        /// <summary>
        /// 结束后是否可以查看试卷 false:不可以查看
        /// </summary>
        public bool IsCanCheckExam { get; set; }
    }
}
