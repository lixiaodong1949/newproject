﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课件信息
    /// </summary>
    public class YssxCourceFilesDto : BaseDto
    {
        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string File { get; set; }

        public int Sort { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 知识点组Id
        /// </summary>
        public long KlgpGroupId { get; set; }
        
        /// <summary>
        /// 课件状态0:待上架,1:上架待审,2:已上架,3:已拒绝
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public int FilesType { get; set; }

        /// <summary>
        /// 审核状态(0,待审核，1，审核成功，2，审核拒绝)
        /// </summary>
        public int AuditStatus { get; set; }

        /// <summary>
        /// 审核说明
        /// </summary>
        public string AuditStatusExplain { get; set; } = string.Empty;

        /// <summary>
        /// 是否上架(true，上架，false，不上架)
        /// </summary>
        public int Uppershelf { get; set; }

        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; } = string.Empty;


        /// <summary>
        /// 文件大小
        /// </summary>
        public int FileSize { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadCount { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public string UpdateTime { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateByName { get; set; }

        /// <summary>
        /// 0:普通用，1:审核用
        /// </summary>
        public int UseType { get; set; }

        /// <summary>
        /// 文件来源 0:课件,1:视频,2:教案 3:授课计划 4:素材(课程备课)
        /// </summary>
        public int CategoryType { get; set; }

    }
}
