﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取App新版本
    /// </summary>
   public class AppNewVersionInfoDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// 是否强制更新
        /// </summary>
        public bool IsForcedUpdate { get; set; }
        /// <summary>
        /// 是否审核通过
        /// </summary>
        public bool IsReview { get; set; }

    }
}
