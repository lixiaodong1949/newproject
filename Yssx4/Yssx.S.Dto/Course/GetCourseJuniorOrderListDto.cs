﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 获取考证资源包列表 - 出参
    /// </summary>
    public class GetCourseJuniorOrderListDto
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 订单Id
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 课程简介
        /// </summary>
        public string ShortDesc { get; set; }

        /// <summary>
        /// 描述备注
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 有效期开始
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 有效期结束
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// 资源ID1
        /// </summary>
        public long ResourceId1 { get; set; }

        /// <summary>
        /// 资源名称1
        /// </summary>
        public string ResourceName1 { get; set; }

        /// <summary>
        /// 资源ID2
        /// </summary>
        public long ResourceId2 { get; set; }

        /// <summary>
        /// 资源名称2
        /// </summary>
        public string ResourceName2 { get; set; }

    }


    /// <summary>
    /// 获取考证资源包列表 - 入参
    /// </summary>
    public class GetCourseJuniorOrderListRequest : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

    }
}
