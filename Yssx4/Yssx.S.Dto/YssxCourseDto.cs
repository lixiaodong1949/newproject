﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程
    /// </summary>
    public class YssxCourseDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public long KnowledgePointId { get; set; }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 课程类型0:常规课
        /// </summary>
        public int CourseType { get; set; }

        /// <summary>
        /// 特点副标
        /// </summary>
        public string CourseTitle { get; set; }

        /// <summary>
        /// 课程类型
        /// </summary>
        public long CourseTypeId { get; set; }

        /// <summary>
        /// 学历标识
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 出版社
        /// </summary>
        public string PublishingHouse { get; set; }

        /// <summary>
        /// 出版时间
        /// </summary>
        public string PublishingDate { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string Intro { get; set; }

        /// <summary>
        /// 描述备注
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        public string CreateByName { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 审核状态
        /// </summary>
        public int AuditStatus { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadCount { get; set; }

        /// <summary>
        /// 订单数
        /// </summary>
        public int OrderCount { get; set; }

        /// <summary>
        /// 点赞数
        /// </summary>
        public int DianZanCount { get; set; }

        /// <summary>
        /// 审核说明
        /// </summary>
        public string AuditStatusExplain { get; set; }

        /// <summary>
        /// 是否上架(0,待上架，1，上架待审，2，上架)
        /// </summary>
        public int Uppershelf { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public string CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public string UpdateTime { get; set; }

        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; }
        /// <summary>
        /// 课程汇总信息
        /// </summary>
        public List<YssxSectionSummaryDto> SummaryListDto { get; set; }

        /// <summary>
        /// 0:普通，1：审核
        /// </summary>
        public int UseType { get; set; }

        /// <summary>
        /// 数据新增方式 0:自建数据 1:拷贝自有数据(深度拷贝) 2:引用平台数据(通过购买行为、深度拷贝)
        /// </summary>
        public int DataCreateWay { get; set; }

    }

    public class YssxCourseListDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 学历标识
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// 出版社
        /// </summary>
        public string PublishingHouse { get; set; }

        /// <summary>
        /// 出版时间
        /// </summary>
        public string PublishingDate { get; set; }

        /// <summary>
        /// 是否设置老师首页显示0:不显示,1:显示
        /// </summary>
        public int IsShow { get; set; }

        /// <summary>
        /// 克隆状态
        /// </summary>
        public int CloneStatus { get; set; }

        /// <summary>
        /// 是否编辑 0:可编辑,1:不可编辑
        /// </summary>
        public int IsEdit { get; set; }
        /// <summary>
        /// 是否移除
        /// </summary>
        public bool IsRemoved { get; set; }

        /// <summary>
        /// 是否是当前预览课程 false:否 true:是
        /// </summary>
        public bool IsPreviewNow { get; set; }
    }
}
