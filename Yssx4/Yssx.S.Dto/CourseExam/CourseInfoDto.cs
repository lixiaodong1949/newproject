﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程信息
    /// </summary>
    public class CourseInfoDto
    {
        /// <summary>
        /// 课程ID
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 课程名称
        /// </summary>
        public string CourseName { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; set; }
        /// <summary>
        /// 学历标识
        /// </summary>
        public int Education { get; set; }
        /// <summary>
        /// 课程类型0:常规课
        /// </summary>
        public int CourseType { get; set; }
        /// <summary>
        /// 总题数
        /// </summary>
        public int QuestionCount { get; set; }
        /// <summary>
        /// 做题数
        /// </summary>
        public int DoQuestionCount { get; set; }
        /// <summary>
        /// 错题数
        /// </summary>
        public int ErrorCount { get; set; }
    }
}
