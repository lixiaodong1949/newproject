﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 章信息
    /// </summary>
    public class SectionInfoDto
    {
        /// <summary>
        /// 章ID
        /// </summary>
        public long SectionId { get; set; }
        /// <summary>
        /// 章名称
        /// </summary>
        public string SectionName { get; set; }
        /// <summary>
        /// 章标题
        /// </summary>
        public string SectionTitle { get; set; }
        ///// <summary>
        ///// 章下面的节信息
        ///// </summary>
        //public List<SubSectionInfoDto> SubSectionInfoList { get; set; }

        /// <summary>
        /// 题目 false开 true关
        /// </summary>
        public bool Close { get; set; }

        /// <summary>
        /// 答案 false开 true关
        /// </summary>
        public bool AnswerClose { get; set; }
    }
    /// <summary>
    /// 节信息
    /// </summary>
    public class SubSectionInfoDto
    {
        /// <summary>
        /// 节ID
        /// </summary>
        public long SectionId { get; set; }
        /// <summary>
        /// 节名称
        /// </summary>
        public string SectionName { get; set; }
        /// <summary>
        /// 章标题
        /// </summary>
        public string SectionTitle { get; set; }
        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 总题数
        /// </summary>
        public int TotalQuestionCount { get; set; }

        /// <summary>
        /// 已做题数
        /// </summary>
        public int DoQuestionCount { get; set; }

        /// <summary>
        /// 主作答记录ID
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public StudentExamStatus Status { get; set; }
    }
    
}
