﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.CourseExam
{
    /// <summary>
    /// 课程实训历史详情
    /// </summary>
    public class ExamCourseDetailsDto
    {
        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

        public long GradeId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string SectionName { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Socre { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        public DateTime? Date { get; set; }
    }
}
