﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Yssx.Framework;
using Yssx.S.Dto.ExamPaper;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 试卷基本信息 
    /// </summary>
    public class ExamPaperCourseBasicInfo
    {
        #region 考试基本信息

        /// <summary>
        /// 考试ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答记录ID
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 学生作答状态
        /// </summary>
        public StudentExamStatus StudentExamStatus { get; set; }

        /// <summary>
        /// 考试总分
        /// </summary>
        public decimal ExamScore { get; set; }

        /// <summary>
        /// 合格分数
        /// </summary>
        public decimal PassScore { get; set; } = 60;

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 考试剩余时长
        /// </summary>
        public long LeftSeconds { get; set; }

        /// <summary>
        /// 作答总用时(秒)
        /// </summary>
        public long UsedSeconds { get; set; }

        #endregion

        /// <summary>
        /// 用户信息
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public UserInfo UserInfo { get; set; }

        /// <summary>
        /// 用户成绩信息
        /// </summary>
        public UserGradeInfo UserGradeInfo { get; set; }

        /// <summary>
        /// 试卷题目信息
        /// </summary>
        public List<CourseQuestionInfo> QuestionInfoList { get; set; }
    }
    /// <summary>
    /// 用户信息
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 名称
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary> 
        public string SchoolName { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IdNumber { get; set; }
    }
    /// <summary>
    /// 学生成绩信息
    /// </summary>
    public class UserGradeInfo
    {
        /// <summary>
        /// 成绩Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 考试分数
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 考试用时
        /// </summary>
        public string UsedTime { get; set; }

        /// <summary>
        /// 答对题数
        /// </summary>
        public int CorrectCount { get; set; }

        /// <summary>
        /// 答错题数
        /// </summary>
        public int ErrorCount { get; set; }

        /// <summary>
        /// 部分对数量
        /// </summary>
        public int PartRightCount { get; set; }

        /// <summary>
        /// 未答题数
        /// </summary>
        public int BlankCount { get; set; }

        /// <summary>
        /// 正确率
        /// </summary>
        public string CorrectRate
        {
            get
            {
                var total = CorrectCount + ErrorCount;
                if (total == 0)
                    return "--";
                else
                {
                    return CorrectCount * 100 / total + "%";
                }
            }

        }
        /// <summary>
        /// 是否及格
        /// </summary>
        public bool IsPass { get; set; }

        /// <summary>
        /// 考试状态
        /// </summary>
        public StudentExamStatus Status { get; set; }
    }
    /// <summary>
    /// 问题信息
    /// </summary>
    public class CourseQuestionInfo
    {
        /// <summary>
        /// 题目ID
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 父题目ID
        /// </summary>
        public long ParentQuestionId { get; set; }

        
        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }


        /// <summary>
        /// 是否标记
        /// </summary>
        [Obsolete]
        [JsonIgnore]
        public bool IsMark { get; set; }

        /// <summary>
        /// 题目类型:0单选题,1多选题，2判断题，3表格题，4分录题，5填空题，6图表题，7综合题，8岗位核心技能，9表格填空题
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 作答状态(0未作答,1已作答,2答对，3答错 4部分正确)
        /// </summary>

        public AnswerDTOStatus QuestionAnswerStatus { get; set; }

        /// <summary>
        /// 分录题状态
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>> AccountEntryStatusDic { get; set; }

        /// <summary>
        /// 分值
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 答题得分
        /// </summary>
        public decimal AnswerScore { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
    }
}
