﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课课程信息
    /// </summary>
    public class PrepareCourseInfoDto
    {
        /// <summary>
        /// 备课课程ID
        /// </summary>
        public long PrepareCourseId { get; set; }
        /// <summary>
        /// 备课课程名称
        /// </summary>
        public string PrepareCourseName { get; set; }
    }
}
