﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 班级学生查询实体
    /// </summary>
    public class YssxClassStudentQuery
    {
        /// <summary>
        /// 班级Id
        /// </summary> 
        public Nullable<long> ClassId { get; set; }

        /// <summary>
        /// 关键字搜索(姓名/学号)
        /// </summary>
        public string Keyword { get; set; }

    }
}
