﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程课件查询实体(购买、上传)
    /// </summary>
    public class YssxCourseFilesQuery : PageRequest
    {
        /// <summary>
        /// 课件来源 0:单独购买 1:私有(上传)
        /// </summary>
        public Nullable<int> CourseSource { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public Nullable<long> KnowledgePointId { get; set; }

        /// <summary>
        /// 课件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>        
        /// 课件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public Nullable<int> FilesType { get; set; }
    }
}
