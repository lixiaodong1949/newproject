﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    ///  课程备课题目查询实体
    /// </summary>
    public class CoursePrepareTopicQuery : PageRequest
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 章节Id
        /// </summary>
        public Nullable<long> SectionId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public Nullable<long> CenterKnowledgePointId { get; set; }

        /// <summary>
        /// 题目类型(可多选) eg: ,1,2,3,
        /// </summary>
        public string QuestionTypes { get; set; }

        /// <summary>
        /// 难度等级(可多选)
        /// </summary>
        public string DifficultyLevels { get; set; }
    }
}
