﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程学生预习查询实体
    /// </summary>
    public class CoursePreviewToStudentQuery : PageRequest
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程预习名称
        /// </summary>
        public string CoursePreviewName { get; set; }
    }
}
