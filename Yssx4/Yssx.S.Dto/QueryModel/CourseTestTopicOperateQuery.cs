﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课堂测验题目操作状态请求实体
    /// </summary>
    public class CourseTestTopicOperateQuery
    {
        /// <summary>
        /// 课程上课Id
        /// </summary>
        public long TeachCourseId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long TopicId { get; set; }
    }
}
