﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 进行中课程测验详情查询实体
    /// </summary>
    public class CourseTestDetailsInProgressQuery
    {
        /// <summary>
        /// 课堂测验任务Id
        /// </summary>
        public long TestTaskId { get; set; }

        /// <summary>
        /// 班级主键Id
        /// </summary>
        public Nullable<long> ClassId { get; set; }
    }
}
