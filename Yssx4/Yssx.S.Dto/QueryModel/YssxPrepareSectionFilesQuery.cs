﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课课程课件、视频、教案、授课计划查询实体
    /// </summary>
    public class YssxPrepareSectionFilesQuery
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 章Id
        /// </summary>
        public Nullable<long> SectionId { get; set; }

        /// <summary>
        /// 数据源 0:课程下所有 1:课程本身所有 2:课程章节下所有
        /// </summary>
        public Nullable<int> QuerySourceType { get; set; }
        
        /// <summary>
        /// 1:课件 2:教案 3:视频 4:授课计划
        /// </summary>
        public Nullable<int> SectionType { get; set; }
    }
}
