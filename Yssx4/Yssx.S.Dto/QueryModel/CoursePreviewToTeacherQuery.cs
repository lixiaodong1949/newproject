﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程教师预习查询实体
    /// </summary>
    public class CoursePreviewToTeacherQuery : PageRequest
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程预习名称
        /// </summary>
        public string CoursePreviewName { get; set; }

    }
}
