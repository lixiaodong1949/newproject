﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课文件查询实体
    /// </summary>
    public class CoursePrepareFilesQuery
    {
        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 资源类型 0:课件 1:教案 2:视频 3:思政案例 4:个人素材
        /// </summary>
        public Nullable<CoursePrepareResourceType> ResourceType { get; set; }

        /// <summary>
        /// 资源归属 0:课前预习 1:课中上课
        /// </summary>
        public Nullable<CoursePrepareResourcesBelong> ResourceBelong { get; set; }

        /// <summary>
        /// 资源名称
        /// </summary>
        public string FileName { get; set; }
    }
}
