﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课信息查询实体
    /// </summary>
    public class YssxLessonPlanningInfoQuery
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public Nullable<long> LessonPlanId { get; set; }

    }
}
