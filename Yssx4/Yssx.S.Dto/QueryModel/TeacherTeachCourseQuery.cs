﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师上课列表查询实体
    /// </summary>
    public class TeacherTeachCourseQuery : PageRequest
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 上课状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        public Nullable<TeachCourseStatus> TeachCourseStatus { get; set; }

        /// <summary>
        /// 上课名称
        /// </summary>
        public string TeachName { get; set; }
    }
}
