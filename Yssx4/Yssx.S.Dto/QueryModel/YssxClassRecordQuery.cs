﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课堂记录查询实体
    /// </summary>
    public class YssxClassRecordQuery
    {
        /// <summary>
        /// 数据来源 0:查自己的，1：查所有的
        /// </summary>
        public Nullable<int> DataSourceType { get; set; }

        /// <summary>
        /// 关键字搜索(课程/班级)
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public Nullable<DateTime> StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public Nullable<DateTime> EndTime { get; set; }

        /// <summary>
        /// 学期主键Id
        /// </summary>
        public Nullable<long> SemesterId { get; set; }
    }
}
