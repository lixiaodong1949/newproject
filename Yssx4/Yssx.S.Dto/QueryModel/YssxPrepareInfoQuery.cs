﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课课程章节信息查询实体
    /// </summary>
    public class YssxPrepareInfoQuery
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 节Id
        /// </summary>
        public Nullable<long> SectionId { get; set; }
    }
}
