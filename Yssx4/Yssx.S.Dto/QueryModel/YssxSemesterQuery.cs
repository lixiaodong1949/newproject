﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学期查询实体
    /// </summary>
    public class YssxSemesterQuery
    {
        /// <summary>
        /// 是否禁用 0:启用 1:禁用
        /// </summary>
        public Nullable<int> IsDisabled { get; set; }

        /// <summary>
        /// 学期名称
        /// </summary>
        public string Name { get; set; }
    }
}
