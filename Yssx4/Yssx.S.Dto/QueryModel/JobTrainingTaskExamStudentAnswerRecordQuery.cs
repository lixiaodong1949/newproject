﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 岗位实训任务试卷学生实时作答详情查询实体
    /// </summary>
    public class JobTrainingTaskExamStudentAnswerRecordQuery
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public Nullable<long> ExamId { get; set; }

        /// <summary>
        /// 任务Id
        /// </summary>
        public Nullable<long> JobTrainingTaskId { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary>
        public Nullable<long> ClassId { get; set; }

        /// <summary>
        /// 是否是实时数据 false:缓存数据(缓存时间过期取实时数据) true:实时数据
        /// </summary>
        public bool IsRealTimeData { get; set; }
    }
}
