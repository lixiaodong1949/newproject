﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学生查询实体
    /// </summary>
    public class YssxCourseClassStudentQuery
    {
        /// <summary>
        /// 关键字搜索(姓名/学号/班级)
        /// </summary>
        public string Keyword { get; set; }
        
    }
}
