﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课列表查询实体
    /// </summary>
    public class YssxPrepareCourseQuery : PageRequest
    {
        /// <summary>
        /// 数据来源 0:查自己的，1：查所有的
        /// </summary>
        public Nullable<int> DataSourceType { get; set; }

        /// <summary>
        /// 关键字搜索
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// 学历 0:中职 1:高职 2:本科
        /// </summary>
        public Nullable<int> Education { get; set; }

        /// <summary>
        /// 学期主键
        /// </summary>
        public Nullable<long> SemesterId { get; set; }

    }
}
