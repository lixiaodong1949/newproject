﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课课堂记录查询实体
    /// </summary>
    public class PlanningClassRecordQuery : PageRequest
    {
        /// <summary>
        /// 上课课程Id
        /// </summary> 
        public long CourseId { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public Nullable<DateTime> StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public Nullable<DateTime> EndTime { get; set; }

        /// <summary>
        /// 学期主键Id
        /// </summary>
        public Nullable<long> SemesterId { get; set; }
    }
}
