﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 岗位实训任务试卷作答记录查询实体
    /// </summary>
    public class YssxJobTrainingTaskExamAnswerRecordQuery
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public Nullable<long> ExamId { get; set; }

        /// <summary>
        /// 任务Id
        /// </summary>
        public Nullable<long> JobTrainingTaskId { get; set; }
    }
}
