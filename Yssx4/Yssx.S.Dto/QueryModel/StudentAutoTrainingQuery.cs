﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学生自主实训查询实体
    /// </summary>
    public class StudentAutoTrainingQuery : PageRequest
    {
        /// <summary>
        /// 用户主键
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public Nullable<long> ClassId { get; set; }
        
        /// <summary>
        /// 企业Id
        /// </summary> 
        public Nullable<long> CaseId { get; set; }
        
        /// <summary>
        /// 交卷日期(开始范围)
        /// </summary>
        public Nullable<DateTime> StartTime { get; set; }

        /// <summary>
        /// 交卷日期(结束范围)
        /// </summary>
        public Nullable<DateTime> EndTime { get; set; }

        /// <summary>
        /// 学生名称
        /// </summary>
        public string StudentName { get; set; }

        /// <summary>
        /// 排序类型 0:交卷时间 1:分数 2:用时
        /// </summary>
        public SortType SortingType { get; set; }
        /// <summary>
        /// 分数统计方式
        /// </summary>
        public StatisticalType StatisticalType { get; set; }
    }
}
