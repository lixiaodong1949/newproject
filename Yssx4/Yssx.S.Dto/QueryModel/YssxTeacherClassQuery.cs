﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 教师班级查询实体
    /// </summary>
    public class YssxTeacherClassQuery
    {
        /// <summary>
        /// 数据源 0:院系、专业下班级 1:学校下班级 2:已绑定课程班级
        /// </summary>
        public Nullable<int> QuerySourceType { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

    }
}
