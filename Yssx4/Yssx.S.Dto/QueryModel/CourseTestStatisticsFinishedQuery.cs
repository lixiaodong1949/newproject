﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 已结束课程测验统计查询实体
    /// </summary>
    public class CourseTestStatisticsFinishedQuery
    {
        /// <summary>
        /// 课堂测验任务Id
        /// </summary>
        public long TestTaskId { get; set; }

        /// <summary>
        /// 班级主键Id
        /// </summary>
        public Nullable<long> ClassId { get; set; }
    }
}
