﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学生查看预习详情查询实体
    /// </summary>
    public class StudentPreviewCourseDetailQuery : PageRequest
    {
        /// <summary>
        /// 课程预习Id
        /// </summary>
        public long CoursePreviewId { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary>
        public Nullable<long> ClassId { get; set; }

        /// <summary>
        /// 关键字(姓名/学号)
        /// </summary>
        public string KeyWord { get; set; }

        /// <summary>
        /// 状态 默认全部 0:未查看 1:已查看
        /// </summary>
        public Nullable<CheckStatus> CheckStatus { get; set; }
    }
}
