﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 备课课程章节查询实体
    /// </summary>
    public class YssxPrepareSectionQuery
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 父节点 章(Id)
        /// </summary>
        public Nullable<long> ParentId { get; set; }

        /// <summary>
        /// 0:章 1：节
        /// </summary>
        public Nullable<int> SectionType { get; set; }

    }
}
