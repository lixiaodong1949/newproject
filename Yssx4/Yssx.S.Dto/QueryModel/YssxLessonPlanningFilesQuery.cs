﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程备课素材、教案查询实体
    /// </summary>
    public class YssxLessonPlanningFilesQuery
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public Nullable<long> CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long LessonPlanId { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 2:教案 5:素材(课程备课)
        /// </summary>
        public Nullable<int> SectionType { get; set; }

    }
}
