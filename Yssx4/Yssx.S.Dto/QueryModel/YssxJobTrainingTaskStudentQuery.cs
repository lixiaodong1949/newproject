﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 学生岗位实训任务查询实体
    /// </summary>
    public class YssxJobTrainingTaskStudentQuery : PageRequest
    {
        /// <summary>
        /// 关键字搜索
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// 实训类型 0:综合岗位实训 1:分岗位实训
        /// </summary>
        public Nullable<int> TrainType { get; set; }

        /// <summary>
        /// 任务类型 0:作业 1:考试
        /// </summary>
        public Nullable<int> ExamType { get; set; }

        /// <summary>
        /// 任务状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        public Nullable<int> TaskStatus { get; set; }
    }
}
