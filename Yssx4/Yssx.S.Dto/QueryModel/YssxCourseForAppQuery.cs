﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程查询实体(App)
    /// </summary>
    public class YssxCourseForAppQuery : PageRequest
    {
        /// <summary>
        /// 课程来源 0:平台(购买/赠送) 1:私有(上传)
        /// </summary>
        public Nullable<int> CourseSource { get; set; }

        /// <summary>
        /// 学历 0:中职 1:高职 2:本科
        /// </summary>
        public Nullable<int> Education { get; set; }
    }
}
