﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto.TrainingCompany
{
    /// <summary>
    /// 公司筛选
    /// </summary>
   public class TrainingCompanyPageRequest : PageRequest
    {
        /// <summary>
        /// 公司Id
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 行业
        /// </summary>
        public Industry Industry { get; set; }
        /// <summary>
        /// 公司规模
        /// </summary>
        public CompanySize CompanySize { get; set; }
    }
}
