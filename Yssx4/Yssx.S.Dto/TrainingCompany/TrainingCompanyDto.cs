﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto.TrainingCompany
{
    /// <summary>
    /// 公司dto
    /// </summary>
    public class TrainingCompanyDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 行业
        /// </summary>
        public Industry Industry { get; set; }
        /// <summary>
        /// 纳税人方式
        /// </summary>
        public TaxpayerMethod TaxpayerMethod { get; set; }
        /// <summary>
        /// 公司规模
        /// </summary>
        public CompanySize CompanySize { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string Introduction { get; set; }
        /// <summary>
        /// 上传公司图
        /// </summary>
        public string CompanyFigure { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public int CompanyNamePicture { get; set; }
        /// <summary>
        /// 印章名称
        /// </summary>
        public int SealNamePicture { get; set; }
        /// <summary>
        /// 部门信息
        /// </summary>
        public List<DepartmentDto> DepartmentDto { get; set; }
    }

    /// <summary>
    /// 公司部门
    /// </summary>
    public class DepartmentDto
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        public string DepartmentName { get; set; }
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostName { get; set; }
        /// <summary>
        /// 职员名称
        /// </summary>
        public string StaffName { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string picture { get; set; }
    }
}
