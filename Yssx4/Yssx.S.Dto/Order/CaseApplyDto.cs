﻿using System;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 申请订单
    /// </summary>
    public class CaseApplyDto : CaseApplyRequest
    {
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
