﻿using System;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class SchoolListView
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 购买案例数 
        /// </summary>
        public int CaseCount { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 状态 0禁用 1启用
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
    }
}
