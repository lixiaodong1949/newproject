﻿using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询申请订单入参
    /// </summary>
    public class CaseApplyListRequest : PageRequest
    {
        /// <summary>
        /// 申请用户账号
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 所属学校Id
        /// </summary>
        public long SchoolId { get; set; }

        /// <summary>
        /// 申请用户角色 0教务 1教师
        /// </summary>
        public CaseUserTypeEnums? CaseUserType { get; set; }
        /// <summary>
        /// 待跟进0, 跟进中1, 已开通2, 已关闭 3
        /// </summary>
        public ApplyStatus? Status { get; set; }
        /// <summary>
        /// 申请时间(开始时间)
        /// </summary>
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 申请时间(结束时间)
        /// </summary>
        public DateTime? EndTime { get; set; }
    }
}
