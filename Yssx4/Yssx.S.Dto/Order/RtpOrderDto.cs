﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto.Order
{
    public class RtpOrderDto
    {
        /// <summary>
        /// 订单id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 用户id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 微信
        /// </summary>
        public string WeChat { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 推广人id
        /// </summary>
        public long PromoterId { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 订单编号（字母+年月日+6位随机）【ZZSC.云实习真账实操  KCTB.课程同步实训圆梦造纸 YESC.业税财实训】
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 订单类型（1.云实习 2.课程同步 3.业税财）
        /// </summary>
        public int OrderType
        {
            get
            {
                return OrderNo.StartsWith("ZZSC") ? 1 : (OrderNo.StartsWith("KCTB") ? 2 : (OrderNo.StartsWith("YESC") ? 3 : 0));
            }
        }

        /// <summary>
        /// 交易号
        /// </summary>
        public string TransactionNo { get; set; }

        /// <summary>
        /// 订单描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 状态（0.未支付 1.已取消 2.已付款）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 支付渠道（1.微信 2.支付宝）
        /// </summary>
        public int PaymentType { get; set; }

        /// <summary>
        /// 付款时间
        /// </summary>
        public DateTime? PaymentTime { get; set; }

        /// <summary>
        /// 平台 - 登录方式（0.PC 1.App 2.小程序 3.H5）
        /// </summary>
        public int LoginType { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal OriginalPrice { get; set; }

        /// <summary>
        /// 折扣价
        /// </summary>
        public decimal DiscountPrice { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }

    public class GetRtpOrderDto : PageRequest
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public long? UserId { get; set; }

        /// <summary>
        /// 产品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }
    }

    public class OrderStatsDto
    {
        /// <summary>
        /// 今日成交金额
        /// </summary>
        public decimal TodayAmount { get; set; }

        /// <summary>
        /// 今日成交
        /// </summary>
        public decimal TodayCount { get; set; }

        /// <summary>
        /// 成交总金额
        /// </summary>
        public decimal TotalAmount { get; set; }
    }

    public class GetUserRtpOrderDto : PageRequest
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public long UserId { get; set; }
    }

    public class UserRtpOrder
    {
        /// <summary>
        /// 账号
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 用户类型0管理员,1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户
        /// </summary> 
        public int UserType { get; set; }

        /// <summary>
        /// 微信
        /// </summary>
        public string Wechat { get; set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime RegisterTime { get; set; }

        /// <summary>
        /// 学校学名
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 订单
        /// </summary>
        public PageResponse<RtpOrderDto> Orders { get; set; }
    }

    public class CancelOrderInputDto
    {
        /// <summary>
        /// 订单id
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
