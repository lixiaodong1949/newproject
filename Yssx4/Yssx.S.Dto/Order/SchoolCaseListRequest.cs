﻿using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 查询学校案例入参
    /// </summary>
    public class SchoolCaseListRequest : PageRequest
    {
        /// <summary>
        /// 学校订单Id
        /// </summary>
        public long OrderId { get; set; }
        ///// <summary>
        ///// 申请用户账号
        ///// </summary>
        //public string UserName { get; set; }

        ///// <summary>
        ///// 所属学校Id
        ///// </summary>
        //public long SchoolId { get; set; }

        /// <summary>
        /// 申请用户角色  0教务 1教师
        /// </summary>
        public CaseUserTypeEnums? CaseUserType { get; set; }
        /// <summary>
        /// 支付状态
        /// </summary>
        public PayStatus? PayStatus { get; set; }
        /// <summary>
        /// 0关闭 1开启
        /// </summary>
        public Status? OpenStatus { get; set; }
        /// <summary>
        /// 跟进人
        /// </summary>
        public string PersonInCharge { get; set; }
        /// <summary>
        /// 开通时间(开始时间)
        /// </summary>
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 开通时间(结束时间)
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        public string KeyWords { get; set; }
    }
}
