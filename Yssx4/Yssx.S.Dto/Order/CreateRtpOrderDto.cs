﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Order
{
    /// <summary>
    /// 云上实习抢购订单（临时）
    /// </summary>
    public class CreateRtpOrderDto
    {
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 推广人id
        /// </summary>
        public long PromoterId { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 订单类型【1.云实习真账实操  2.课程同步实训圆梦造纸 3.业税财实训】
        /// </summary>
        public int OrderType { get; set; }

        /// <summary>
        /// 订单描述
        /// </summary>
        public string Description { get; set; }


        /// <summary>
        /// 平台 - 登录方式（0.PC 1.App 2.小程序 3.H5）
        /// </summary>
        public int LoginType { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal OriginalPrice { get; set; }

        /// <summary>
        /// 折扣价
        /// </summary>
        public decimal DiscountPrice { get; set; }
    }

    public class WebchatResponse
    {
        /// <summary>
        /// 支付url
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }
    }

    public class AlipayResponse
    {
        /// <summary>
        /// 支付表单
        /// </summary>
        public string Form { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }
    }

    public class OrderSms
    {
        public string cname { get; set; }

        public string cdate { get; set; }

        public string copen { get; set; }
    }
}
