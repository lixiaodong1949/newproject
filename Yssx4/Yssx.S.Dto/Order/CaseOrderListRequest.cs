﻿using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 订单列表入参
    /// </summary>
    public class CaseOrderListRequest : PageRequest
    {
        /// <summary>
        /// 所属学校Id
        /// </summary>
        public long SchoolId { get; set; }
        /// <summary>
        /// 状态 0禁用 1启用
        /// </summary>
        public Status? Status { get; set; }
        /// <summary>
        /// 创建时间(查询开始时间)
        /// </summary>
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 创建时间(查询结束时间)
        /// </summary>
        public DateTime? EndTime { get; set; }
    }
}
