﻿using System;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 给学校开通案例入参
    /// </summary>
    public class SchoolCaseRequest
    {
        /// <summary>
        /// 主键Id，修改时使用
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 订单Id
        /// </summary>
        public long OrderId { get; set; }
        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }
        ///// <summary>
        ///// 案例Id
        ///// </summary>
        //public long CaseId { get; set; }

        ///// <summary>
        ///// 案例名称
        ///// </summary>
        //public string CaseName { get; set; }
        /// <summary>
        /// 要开通的案例id数组
        /// </summary>
        public long[] CaseIds { get; set; }

        ///// <summary>
        ///// 要开通的案例名称数组
        ///// </summary>
        //public string[] CaseNames { get; set; }
        ///// <summary>
        ///// 学历标记（0 中职 1高职 2 本科）
        ///// </summary>
        //public int[] Educations { get; set; }

        /// <summary>
        /// 购买角色   0教务 1教师
        /// </summary>
        public CaseUserTypeEnums CaseUserType { get; set; }
        /// <summary>
        /// 购买账号
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 开通账号
        /// </summary>
        public OrderUserEnum OrderUser { get; set; }
        /// <summary>
        /// 开通时间
        /// </summary>
        public DateTime OpenTime { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpireTime { get; set; }
        /// <summary>
        /// 支付状态
        /// </summary>
        public PayStatus PayStatus { get; set; }
        /// <summary>
        /// 跟进人
        /// </summary>
        public string PersonInCharge { get; set; }
        /// <summary>
        /// 0关闭 1开启
        /// </summary>
        public Status OpenStatus { get; set; }

        /// <summary>
        /// 赛前训练启用状态（0 不启用 1启用）
        /// </summary>
        public int TestStatus { get; set; }
        /// <summary>
        /// 正式赛启用状态（0 不启用 1启用）
        /// </summary>
        public int OfficialStatus { get; set; }

        /// <summary>
        /// 序号 
        /// </summary>
        public int Sort { get; set; }
    }
}
