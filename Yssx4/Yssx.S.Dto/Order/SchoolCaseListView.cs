﻿using System;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class SchoolCaseListView
    {
        public long Id { get; set; }
        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科）
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 购买角色
        /// </summary>
        public CaseUserTypeEnums CaseUserType { get; set; }
        /// <summary>
        /// 购买账号
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 开通账号（权限）
        /// </summary>
        public OrderUserEnum OrderUser { get; set; }
        /// <summary>
        /// 开通时间
        /// </summary>
        public DateTime OpenTime { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpireTime { get; set; }
        /// <summary>
        /// 支付状态
        /// </summary>
        public PayStatus PayStatus { get; set; }
        /// <summary>
        /// 跟进人
        /// </summary>
        public string PersonInCharge { get; set; }
        /// <summary>
        /// 0关闭 1开启
        /// </summary>
        public Status OpenStatus { get; set; }

        /// <summary>
        /// 赛前训练启用状态（0 不启用 1启用）
        /// </summary>
        public int TestStatus { get; set; }
        /// <summary>
        /// 正式赛启用状态（0 不启用 1启用）
        /// </summary>
        public int OfficialStatus { get; set; }

        /// <summary>
        /// 序号 
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 试卷名称
        /// </summary>
        public string ExamPaperName { get; set; }
    }
}
