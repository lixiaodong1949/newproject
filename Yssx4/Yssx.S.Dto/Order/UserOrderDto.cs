﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto.Order
{
    /// <summary>
    /// 用户订单
    /// </summary>
    public class UserOrderDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 订单描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 状态（0.未支付 1.已取消 2.已付款）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 支付渠道（1.微信 2.支付宝）
        /// </summary>
        public int PaymentType { get; set; }

        /// <summary>
        /// 付款时间
        /// </summary>
        public DateTime? PaymentTime { get; set; }

        /// <summary>
        /// 平台 - 登录方式（0.PC 1.App 2.小程序 3.H5 4.微店）
        /// </summary>
        public int LoginType { get; set; }

        /// <summary>
        /// 订单总额
        /// </summary>
        public decimal OrderTotal { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Qty { get; set; }

        /// <summary>
        /// 源订单ID（赠送的需要赋值）
        /// </summary>
        public long OriginalOrderId { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 微信
        /// </summary>
        public string WeChat { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }
    }

    public class UserOrderStatsDto
    {
        /// <summary>
        /// 今日成交金额
        /// </summary>
        public decimal TodayAmount { get; set; }

        /// <summary>
        /// 今日成交
        /// </summary>
        public decimal TodayCount { get; set; }

        /// <summary>
        /// 成交总金额
        /// </summary>
        public decimal TotalAmount { get; set; }
    }

    /// <summary>
    /// 单个用户的订单信息
    /// </summary>
    public class SingleUserOrder
    {
        /// <summary>
        /// 账号
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 用户类型0管理员,1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户
        /// </summary> 
        public int UserType { get; set; }

        /// <summary>
        /// 微信
        /// </summary>
        public string Wechat { get; set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        public DateTime RegisterTime { get; set; }

        /// <summary>
        /// 学校学名
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 该用户的订单
        /// </summary>
        public PageResponse<UserOrderDto> Orders { get; set; }
    }

    public class GetSingleUserOrderDto : PageRequest
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public long UserId { get; set; }
    }

    public class GetUserOrdersDto : PageRequest
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public long? UserId { get; set; }

        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        public string RealName { get; set; }
    }

    public class CreateUserOrderDto
    {
        /// <summary>
        /// 产品id
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// 平台 - 登录方式（0.PC 1.App 2.小程序 3.H5 4.微店）
        /// </summary>
        public int LoginType { get; set; }
    }
}
