﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 用户登录实体
    /// </summary>
    public class UserLoginRequestModel
    {
        /// <summary>
        /// 班级Id
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 登录方式 0:PC 1:App 2:小程序
        /// </summary>
        public int LoginType { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
        
        /// <summary>
        /// 验证码
        /// </summary>
        public string VerificationCode { get; set; }

        /// <summary>
        /// 微信openid
        /// </summary>
        public string WeChat { get; set; }

        /// <summary>
        /// 微信unionid
        /// </summary>
        public string Unionid { get; set; }
        
        /// <summary>
        /// 微信昵称
        /// </summary>
        public string NikeName { get; set; }
    }
}
