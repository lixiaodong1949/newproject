﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 课程案例
    /// </summary>
    public class YssxSectionCaseDto: BaseDto
    {
        public long SectionId { get; set; }

        public long CourseId { get; set; }

        public long CaseId { get; set; }

        public string KnowledgePointId { get; set; }

        public int Sort { get; set; }
    }

    public class CourseCaseDto 
    {
        public long CourseId { get; set; }

        public long CaseId { get; set; }

        public string Name { get; set; }
    }
}
