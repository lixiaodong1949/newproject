﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Topics
{
    /// <summary>
    /// 复制案例入参
    /// </summary>
    public class CloneCaseRequest
    {
        /// <summary>
        /// 源案例ID
        /// </summary>
        public long SourceId { get; set; }
        /// <summary>
        /// 目标案例名称
        /// </summary>
        public string TargetName { get; set; }
        
        /// <summary>
        /// 对应岗位信息
        /// </summary>
        public List<ClonePostionInfo> ClonePostionInfos { get; set; }
    }
    
    public class ClonePostionInfo
    {
        /// <summary>
        /// 源岗位ID
        /// </summary>
        public long SourcePostionId { get; set; }
        /// <summary>
        /// 目标岗位ID
        /// </summary>
        public long TargetPostionId { get; set; }
        /// <summary>
        /// 源岗位名称
        /// </summary>
        public string SourcePostionName { get; set; }

        /// <summary>
        /// 源岗位名称
        /// </summary>
        public string TargetPostionName { get; set; }
    }
}
