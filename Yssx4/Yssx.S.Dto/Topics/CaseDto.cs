﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Topics
{
    public class CaseDto : CaseRequest
    {
        /// <summary>
        /// 创建人
        /// </summary>
        public long? CreateBy { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public long? UpdateBy { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 题量
        /// </summary>
        public long QuestionCount { get; set; }

    }
}
