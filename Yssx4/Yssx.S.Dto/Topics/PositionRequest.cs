﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 岗位操作入参类
    /// </summary>
    public class PositionRequest
    {
        /// <summary>
        /// 岗位ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 缩略图标文字
        /// </summary>
        public string ThumbnailText { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 岗位图标地址
        /// </summary>
        public string FileUrl { get; set; }
        /// <summary>
        /// 操作人-用户ID
        /// </summary>
        public long UserId { get; set; }
    }
}
