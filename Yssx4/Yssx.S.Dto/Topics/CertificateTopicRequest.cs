﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 凭证题 
    /// </summary>
    public class CertificateTopicRequest
    {
        public long Id { get; set; }
        /// <summary>
        /// 父节点ID-用于多题型(一个题目 多个作答区)
        /// </summary>
        public long ParentId { get; set; } = 0;
        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 题干内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 题目内容类型(1:纯文字,2:表格,3:图片)
        /// </summary>
        public int QuestionContentType { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>

        public int? DifficultyLevel { get; set; }
        /// <summary>
        /// 模板数据源内容(不含答案)
        /// </summary>

        public string TopicContent { get; set; }

        /// <summary>
        /// 题目数据源内容(含答案)
        /// </summary>
        public string FullContent { get; set; }
        /// <summary>
        /// 答案 内容（选择题答案a,b,c等，判断题true/false，表格json等）
        /// </summary>
        public string AnswerValue { get; set; }

        /// <summary>
        /// 图片文件
        /// </summary>
        public List<QuestionFile> QuestionFile { get; set; }

        /// <summary>
        /// 答案提示
        /// </summary>
        public string Hint { get; set; }

        /// <summary>
        /// 发生日期
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }

        /// <summary>
        /// 题库Id
        /// </summary>
        public long TopicId { get; set; }

        /// <summary>
        /// 企业Id
        /// </summary>
        public long EnterpriseId { get; set; }

        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertificateDate { get; set; }

        /// <summary>
        /// 单据数据记录
        /// </summary>
        public List<CertificateDataRecord> DataRecords { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 会计期间
        /// </summary>
        public string AccountingPeriodDate { get; set; }

        #region 新增
        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 是否结账题（0 否 1是）
        /// </summary>
        public int IsSettlement { get; set; }
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科） 此处不可编辑，来自案例主表学历标记
        /// </summary>
        public int Education { get; set; }
        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountingManager { get; set; }
        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }
        /// <summary>
        /// 制单人
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 会计主管是否禁用 (0 禁用 1 启用)
        /// </summary>
        public Status IsDisableAM { get; set; }
        /// <summary>
        /// 出纳是否禁用 (0 禁用 1 启用)
        /// </summary>
        public Status IsDisableCashier { get; set; }
        /// <summary>
        /// 审核人是否禁用 (0 禁用 1 启用)
        /// </summary>
        public Status IsDisableAuditor { get; set; }
        /// <summary>
        /// 制单人是否禁用 (0 禁用 1 启用)
        /// </summary>
        public Status IsDisableCreator { get; set; }
        /// <summary>
        /// 计分方式
        /// </summary>
        public AccountEntryCalculationType CalculationScoreType { get; set; }

        /// <summary>
        /// 外围
        /// </summary>
        public int OutVal { get; set; }
        /// <summary>
        /// 里围
        /// </summary>
        public int InVal { get; set; }
        /// <summary>
        /// 非完整性得分
        /// </summary>
        public decimal PartialScore { get; set; } = 0;

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
        #endregion
    }
    /// <summary>
    /// 单据数据记录
    /// </summary>
    public class CertificateDataRecord
    {

        /// <summary>
        /// 摘要信息
        /// </summary>
        public string SummaryInfo { get; set; }
        /// <summary>
        /// 科目Id
        /// </summary>
        public long SubjectId { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        public decimal CreditorAmount { get; set; }
    }
}
