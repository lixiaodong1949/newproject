﻿
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto.Topics
{
    public class CaseListRequest : PageRequest
    {
        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科）查询全部此参数就不传
        /// </summary>
        public int? Education { get; set; }
        /// <summary>
        /// 赛事类型（0 个人赛 1团体赛）查询全部此参数就不传
        /// </summary>
        public int? CompetitionType { get; set; }
        /// <summary>
        /// 赛前训练启用状态（0 不启用 1启用）查询全部此参数就不传
        /// </summary>
        public int? TestStatus { get; set; }
        /// <summary>
        /// 区域
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 创建人 查询全部此参数就不传
        /// </summary>
        public long? CreateBy { get; set; }
        /// <summary>
        /// 创建时间(开始时间)
        /// </summary>
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 创建时间(结束时间)
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// 创建人  查询全部传-1
        /// </summary>
        public int OptionType { get; set; }
    }
}
