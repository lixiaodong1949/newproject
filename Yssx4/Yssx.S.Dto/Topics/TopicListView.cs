﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace Yssx.S.Dto
{
    /// <summary>
    ///  题目列表视图
    /// </summary>
    public class TopicListView
    {
        /// <summary>
        /// 题目ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>        
        public string Title { get; set; }
        /// <summary>
        /// 所属岗位ID
        /// </summary>        
        public long PositionId { get; set; }
        /// <summary>
        /// 所属岗位名称
        /// </summary>        
        public string PositionName { get; set; }
        /// <summary>
        /// 是否压缩（0 否 1是）
        /// </summary>
        public int IsGzip { get; set; }
        /// <summary>
        /// 题干内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        [JsonIgnore]
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 题目类型名称
        /// </summary> 
        public string QuestionTypeName
        {
            get { return QuestionType.GetDescription(); }
            set { }
        }
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科）
        /// </summary>
        public int Education { get; set; }
        /// <summary>
        /// 凭证单据张数
        /// </summary>
        public int Certificate { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 非完整性得分
        /// </summary>
        public decimal PartialScore { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 录题时间
        /// </summary>
        public DateTime CreateTime { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
