﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 图表题excel导入数据
    /// </summary>
    public class TopicFillGridDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 图表模板名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 导入的内容
        /// </summary>
        public string Content { get; set; }
    }
}
