﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 岗位人员入参
    /// </summary>
    public class PostPersonRequest : BaseDto
    {
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PostName { get; set; }
        /// <summary>
        /// 岗位人员
        /// </summary>
        public string Persons { get; set; }
        /// <summary>
        /// 所属部门
        /// </summary>
        public long DepartmentId { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
    }
}
