﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Dto
{
    /// <summary>
    /// 岗位人员查询入参
    /// </summary>
    public class PostPersonListRequest : PageRequest
    {
        /// <summary>
        /// 部门ID
        /// </summary>
        public long DepartmentId { get; set; }
    }
}
