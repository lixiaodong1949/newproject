﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Dto.Topics
{
    public class CaseNameDto
    {
        /// <summary>
        /// 案例ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }
        /// <summary>
        /// 试卷名称【暂时用于.赛前训练绑定案例】
        /// </summary>
        public string PaperName { get; set; }
    }
}
