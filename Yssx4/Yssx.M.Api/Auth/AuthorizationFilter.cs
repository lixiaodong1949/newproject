﻿using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;


namespace Yssx.M.Api.Auth
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthorizationFilter : IAuthorizationFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param> 
        /// <param name="claims"></param> 
        private static bkUserTicket SetUserTicket(HttpContext httpContext, IEnumerable<Claim> claims)
        {
            var dicClaim = claims.ToDictionary(m => m.Type, m => m.Value);
            dicClaim.TryGetValue(JwtClaimTypes.Name, out var name);
            dicClaim.TryGetValue(JwtClaimTypes.Id, out var id);
            //dicClaim.TryGetValue(CommonConstants.ClaimsTenantId, out var tenantId);
            dicClaim.TryGetValue(CommonConstants.ClaimsUserType, out var userType);
            int.TryParse(userType, out var usertypeInt);
            //dicClaim.TryGetValue(CommonConstants.ClaimsMobile, out var mobile);
            //dicClaim.TryGetValue(CommonConstants.ClaimsRealName, out var realName);
            //dicClaim.TryGetValue(CommonConstants.ClaimsIdNumber, out var idNumber);
            dicClaim.TryGetValue(CommonConstants.ClaimsLoginTimespan, out var timestamp);
            var userTicket = new bkUserTicket
            {
                Name = name,
                Id = long.Parse(id),
                //TenantId = long.Parse(tenantId),
               //Mobile = mobile,
                //RealName = realName,
                Type = usertypeInt,
                //IdNumber = idNumber
                //timestamp=timestamp
            };
            httpContext.Items[CommonConstants.ContextUserPropertyKey] = userTicket;
            return userTicket;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var anonymous = ((ControllerActionDescriptor)context.ActionDescriptor).MethodInfo.GetCustomAttributes(typeof(AllowAnonymousAttribute), true);
            if (anonymous.Any())
            {
                return;
            }

            if (!context.HttpContext.Request.Headers.TryGetValue("token", out var token) || string.IsNullOrEmpty(token))
            {
                context.Result = new ObjectResult(new { code = CommonConstants.Unauthorized, sub_msg = "请求缺少token", msg = "请求缺少token" });
                return;
            }

            var jwt = new JwtSecurityTokenHandler().ReadJwtToken(token);
            var ticket = SetUserTicket(context.HttpContext, jwt.Claims);
            var userId = ticket.Id.ToString();
            var cacheToken = RedisHelper.HGet(CacheKeys.UserLoginTokenHashKey, $"{userId}-{ticket.LoginType}");
            if (string.IsNullOrEmpty(cacheToken))  //测试环境先注释
            {
                //var log = UserService.GetLastLoginLog(ticket.Id);
                //if (log.Status == Status.Disable || !Equals(log.AccessToken, token))
                //{
                //    context.Result = new ObjectResult(new { code = CommonConstants.Unauthorized, sub_msg = "无效的token请求", msg = "无效的token请求" });
                //    return;
                //}
                RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{userId}-{ticket.LoginType}", token);
            }
            else if (!Equals(token, cacheToken))
            {
                context.Result = new ObjectResult(new { code = CommonConstants.Unauthorized, sub_msg = "此账号在另一处登录，您已被迫下线!", msg = "此账号在另一处登录，您已被迫下线!" });
                return;
            }


        }
    }
}
