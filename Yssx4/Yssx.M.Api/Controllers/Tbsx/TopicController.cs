﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 同步实训 题目管理（后台）
    /// </summary>
    public class TopicController : BaseController
    {
        ITopicServiceNew _topicService;
        public TopicController(ITopicServiceNew topicService)
        {
            _topicService = topicService;
        }

        /// <summary>
        /// 添加 更新 题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveSimpleQuestion(SimpleQuestionRequestNew model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (model.CaseId <= 0)
            {
                result.Msg = "CaseId 为无效数据。";
                return result;
            }
            else if (model.Options == null || model.Options.Count == 0)
            {
                result.Msg = "请添加选项。";
                return result;
            }

            return await _topicService.SaveSimpleQuestion(model, CurrentUser);
        }

        /// <summary>
        /// 添加 更新 复杂题目（表格题、填空题、票据题）
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveComplexQuestion(ComplexQuestionRequestNew model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (model.CaseId <= 0)
            {
                result.Msg = "CaseId 为无效数据。";
                return result;
            }
            //else if (model.TaskId <= 0)
            //{
            //    result.Msg = "TaskId 为无效数据。";
            //    return result;
            //}

            return await _topicService.SaveComplexQuestion(model, CurrentUser);
        }

        /// <summary>
        /// 添加 更新 综合题
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveMainSubQuestion(MainSubQuestionRequestNew model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (model.CaseId <= 0)
            {
                result.Msg = "CaseId 为无效数据。";
                return result;
            }

            return await _topicService.SaveMainSubQuestion(model, CurrentUser);
        }

        /// <summary>
        /// 根据id获取题目详情
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TopicInfoDtoNew>> GetTopicInfoById(long id)
        {
            var result = new ResponseContext<TopicInfoDtoNew>(CommonConstants.BadRequest, "", null);
            if (id <= 0)
            {
                result.Msg = "id不正确。";
                return result;
            }

            return await _topicService.GetTopicInfoById(id);
        }

        /// <summary>
        /// 分页查询题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<TopicListPageDto>> GetTopicListPage(TopicQueryPageDto queryDto)
        {
            var response = new ResponseContext<TopicListPageDto>(CommonConstants.BadRequest, "");
            if (queryDto == null)
            {
                response.Msg = "请传入正确的参数";
                return response;
            }
            return await _topicService.GetTopicListPage(queryDto);
        }

        /// <summary>
        /// 题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<TopicListDataDto>>> GetTopicList(TopicQueryDto queryDto)
        {
            var response = new ResponseContext<List<TopicListDataDto>>(CommonConstants.BadRequest, "");
            if (queryDto == null)
            {
                response.Msg = "请传入正确的参数";
                return response;
            }
            return await _topicService.GetTopicList(queryDto);
        }

        /// <summary>
        /// 获取所有题目的名称（包含子题目）
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<TopicNamesResponseDto>>> GetAllTopicNames(TopicNamesDto queryDto)
        {
            var response = new ResponseContext<List<TopicNamesResponseDto>>(CommonConstants.BadRequest, "");
            if (queryDto == null)
            {
                response.Msg = "请传入正确的参数";
                return response;
            }

            return await _topicService.GetAllTopicNames(queryDto);
        }

        /// <summary>
        /// 题目删除 根据Id删除题目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteQuestion(long id)
        {
            if (id <= 0)
                return new ResponseContext<bool>(CommonConstants.BadRequest, "", false);

            return await _topicService.DeleteTopicById(id, CurrentUser);
        }

        /// <summary>
        /// 分录题  添加 更新
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveCertificateTopic(CertificateTopicRequestNew model)
        {
            var result = new ResponseContext<bool>(CommonConstants.BadRequest, "", false);

            #region 数据校验

            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }

            #endregion

            result = await _topicService.SaveCertificateTopic(model, CurrentUser);

            return result;
        }
        /// <summary>
        /// 获取分录题 信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CertificateTopicRequestNew>> GetCertificateById(long id)
        {
            if (id <= 0)
                return new ResponseContext<CertificateTopicRequestNew>(CommonConstants.BadRequest, "请求参数无效", null);

            return await _topicService.GetCertificateById(id);
        }
    }
}