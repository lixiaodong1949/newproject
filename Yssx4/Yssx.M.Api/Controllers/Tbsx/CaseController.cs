﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 同步实训 案例管理（后台）
    /// </summary>
    public class CaseController : BaseController
    {
        private ICaseServiceNew _caseService;

        public CaseController(ICaseServiceNew caseService)
        {
            _caseService = caseService;
        }

        #region 案例
        /// <summary>
        /// 保存案例
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveCase(CaseDtoNew input)
        {
            return await _caseService.SaveCase(input, CurrentUser);
        }

        /// <summary>
        /// 根据id删除案例
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCase(long id)
        {
            return await _caseService.DeleteCase(id, CurrentUser);
        }

        /// <summary>
        /// 根据id获取案例
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CaseDtoNew>> GetCase(long id)
        {
            return await _caseService.GetCase(id);
        }

        /// <summary>
        /// 根据条件查询案例
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<GetCaseListDtoNew>> GetCaseList(GetCaseInput input)
        {
            return await _caseService.GetCaseList(input);
        }

        /// <summary>
        /// 启用或禁用案例
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Toggle(ToggleInput input)
        {
            return await _caseService.Toggle(input, CurrentUser);
        }
        #endregion

    }
}