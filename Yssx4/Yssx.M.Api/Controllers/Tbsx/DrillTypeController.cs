﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 同步实训 实训类型（后台）
    /// </summary>
    public class DrillTypeController : BaseController
    {
        IDrillTypeService _service;
        public DrillTypeController(IDrillTypeService DrillTypeService)
        {
            _service = DrillTypeService;
        }

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteById(long id)
        {
            ResponseContext<bool> response = null;
            if (id <= 0)
            {
                response = new ResponseContext<bool>(CommonConstants.BadRequest, "Id不正确！");
                return response;
            }

            response = await _service.DeleteById(id, CurrentUser);

            return response;
        }
        /// <summary>
        /// 根据Id获取单条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<DrillTypeDto>> GetInfoById(long id)
        {
            ResponseContext<DrillTypeDto> response = null;
            if (id <= 0)
            {
                response = new ResponseContext<DrillTypeDto>(CommonConstants.BadRequest, "Id不正确！");
                return response;
            }

            response = await _service.GetInfoById(id);

            return response;
        }
        /// <summary>
        /// 保存 新增和修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(DrillTypeDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (string.IsNullOrWhiteSpace(model.Name))
            {
                result.Msg = "Name 为无效数据。";
                return result;
            }

            result = await _service.Save(model, CurrentUser);

            return result;
        }
        /// <summary>
        /// 指导编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SetGuideInfo(SetGuideInfoDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (model.Id <= 0)
            {
                result.Msg = "Id 为无效数据。";
                return result;
            }

            result = await _service.SetGuideInfo(model, CurrentUser);

            return result;
        }
        /// <summary>
        /// 获取指导编辑信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<SetGuideInfoDto>> GetGuideInfoById(long id)
        {
            var result = new ResponseContext<SetGuideInfoDto> { Code = CommonConstants.BadRequest, Msg = "" };
            if (id <= 0)
            {
                result.Msg = "Id 为无效数据。";
                return result;
            }

            result = await _service.GetGuideInfoById(id);

            return result;
        }
        /// <summary>
        /// 查询列表 分页
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<DrillTypeListDto>>> GetList(DrillTypeQueryDto query)
        {
            var response = await _service.GetList(query);

            return response;
        }
    }
}