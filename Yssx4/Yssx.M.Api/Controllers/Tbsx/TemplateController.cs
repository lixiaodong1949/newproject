﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 同步实训 模板管理
    /// </summary>
    public class TemplateController : BaseController
    {
        private ITemplateServiceNew _templateService;
        /// <summary>
        /// 
        /// </summary>
        public TemplateController(ITemplateServiceNew templateService)
        {
            _templateService = templateService;
        }

        #region 模板类型
        /// <summary>
        /// 获取模板树列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<TemplateTreeItemDtoNew>> GetTemplateTree()
        {
            return await _templateService.GetTemplateTree();
        }

        /// <summary>
        /// 获取指定模板类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TemplateTreeItemDtoNew>> GetTemplateTypeById(long id)
        {
            return await _templateService.GetTemplateTypeById(id);
        }

        /// <summary>
        /// 添加/编辑模板类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateTemplateType(TemplateTreeItemDtoNew model)
        {
            return await _templateService.AddUpdateTemplateType(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除模板类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteTemplateType(long tId)
        {
            return await _templateService.DeleteTemplateType(tId, CurrentUser.Id);
        }

        #endregion

        #region 模板

        /// <summary>
        /// 获取模板列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<TemplateDtoNew>> GetTemplates(GetTemplatesRequestNew model)
        {
            return await _templateService.GetTemplates(model);
        }

        /// <summary>
        /// 获取指定模板
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TemplateDtoNew>> GetTemplateById(long id)
        {
            return await _templateService.GetTemplateById(id);
        }

        /// <summary>
        /// 添加模板信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateTemplate(TemplateDtoNew model)
        {
            return await _templateService.AddUpdateTemplate(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除指定模板
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteTemplate(long tId)
        {
            return await _templateService.DeleteTemplate(tId, CurrentUser.Id);
        }

        /// <summary>
        /// 获取模板树（下拉框）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<LayuiTreeDtoNew>>> GetTemplateSelectTreeDatas()
        {
            return await _templateService.GetTemplateSelectTreeDatas();
        }

        #endregion

    }
}