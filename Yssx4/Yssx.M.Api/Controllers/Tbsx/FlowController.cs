﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 同步实训 流程管理
    /// </summary>
    public class FlowController : BaseController
    {
        IFlowService _service;
        public FlowController(IFlowService flowService)
        {
            _service = flowService;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteById(long id)
        {
            ResponseContext<bool> response = null;
            if (id <= 0)
            {
                response = new ResponseContext<bool>(CommonConstants.BadRequest, "Id不正确！");
                return response;
            }

            response = await _service.DeleteById(id, CurrentUser);

            return response;
        }
        /// <summary>
        /// 根据Id获取单条详细信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<FlowDto>> GetInfoById(long id)
        {
            ResponseContext<FlowDto> response = null;
            if (id <= 0)
            {
                response = new ResponseContext<FlowDto>(CommonConstants.BadRequest, "Id不正确！");
                return response;
            }

            response = await _service.GetInfoById(id);

            return response;
        }
        /// <summary>
        /// 新增和修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(FlowDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (string.IsNullOrWhiteSpace(model.Name))
            {
                result.Msg = "Name 为无效数据。";
                return result;
            }

            result = await _service.Save(model, CurrentUser);

            return result;
        }
        /// <summary>
        /// 根据条件查询列表 分页
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<FlowListDto>>> GetList(FlowQueryDto query)
        {
            var response = await _service.GetList(query);

            return response;
        }
    }
}