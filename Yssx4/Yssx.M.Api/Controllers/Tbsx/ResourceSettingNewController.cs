﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 同步实训 资源设置（课程） 部门管理 岗位管理
    /// </summary>
    public class ResourceSettingNewController : BaseController
    {
        ICaseDepartmentServiceNew _service;
        public ResourceSettingNewController(ICaseDepartmentServiceNew settingService)
        {
            _service = settingService;
        }

        #region 部门管理
        /// <summary>
        /// 获取部门管理列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<DepartmentDtoNew>> GetDepartmentList(DepartmentRequestNew model)
        {
            return await _service.GetDepartmentList(model);
        }

        /// <summary>
        /// 新增或修改部门管理
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditDepartment(DepartmentDtoNew model)
        {
            return await _service.AddOrEditDepartment(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除部门管理
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveDepartment(long id)
        {
            return await _service.RemoveDepartment(id, CurrentUser.Id);
        }
        #endregion

        #region 岗位管理
        /// <summary>
        /// 获取岗位管理列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<PositionDtoNew>> GetPositionList(PositionRequestNew model)
        {
            return await _service.GetPositionList(model);
        }
        /// <summary>
        /// 根据Id获取岗位管理详情
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<PositionDtoNew>> GetPositionById(long id)
        {
            return await _service.GetPositionById(id);
        }

        /// <summary>
        /// 新增或修改岗位管理
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditPosition(PositionDtoNew model)
        {
            return await _service.AddOrEditPosition(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除岗位管理
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemovePosition(long id)
        {
            return await _service.RemovePosition(id, CurrentUser.Id);
        }
        #endregion
    }
}