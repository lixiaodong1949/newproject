﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 同步实训 科目Api
    /// </summary>
    public class SubjectController : BaseController
    {
        ISubjectServiceNew service;
        ICourseSubjectServiceNew courseSubjectService;
        public SubjectController(ISubjectServiceNew s1, ICourseSubjectServiceNew s2)
        {
            service = s1;
            courseSubjectService = s2;
        }

        /// <summary>
        /// 新增科目
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddSubject(SubjectDtoNew model)
        {
            return await service.AddSubject(model);
        }

        /// <summary>
        /// 删除科目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveSubject(long id)
        {
            return await service.RemoveSubject(id);
        }

        /// <summary>
        /// 新增科目获取子科目的序号
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<string>> GetNextNum(long id)
        {
            return await service.GetNextNum(id);
        }

        /// <summary>
        /// 根据案例Id获取科目列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubjectDtoNew>>> GetSubjectsList(long id)
        {
            return await service.GetSubjectsList(id);
        }

        /// <summary>
        /// 根据案例Id获取最后一级科目
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="type">类型Type</param>
        /// <param name="keyword">类型Type</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubjectDtoNew>>> GetSubjectLastNodeList(long id, int type, string keyword)
        {
            return await service.GetSubjectLastNodeList(id, type, keyword);
        }

        /// <summary>
        /// 根据案例科目类型获取科目列表
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubjectDtoNew>>> GetSubjectByType(long id, int type)
        {
            return await service.GetSubjectByType(id, type);
        }

        /// <summary>
        /// 添加期初数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddValues(SubjectDataDtoNew model)
        {
            return await service.AddValues(model);
        }

        /// <summary>
        /// 试算平衡
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TrialBalancingDtoNew>> TrialBalancing(long caseId)
        {
            return await service.TrialBalancing(caseId);
        }

        /// <summary>
        /// 新增课程科目
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddCourseSubject(SubjectDtoNew model)
        {
            model.CreateBy = CurrentUser.Id;
            return await courseSubjectService.AddSubject(model);
        }

        /// <summary>
        /// 删除课程科目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveCourseSubject(long id)
        {
            return await courseSubjectService.RemoveSubject(id);
        }

        /// <summary>
        /// 获取课程科目获取子科目的序号
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<string>> GetCourseNextNum(long id)
        {
            return await courseSubjectService.GetNextNum(id);
        }

        /// <summary>
        /// 根据课程Id获取课程科目列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubjectDtoNew>>> GetCourseSubjectsList(long cid)
        {

            return await courseSubjectService.GetSubjectsList(cid);
        }

        /// <summary>
        /// 根据用户Id获取最后一级科目
        /// </summary>
        /// <param name="cid">课程Id</param>
        /// <param name="type">类型Type</param>
        /// <param name="keyword">类型Type</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubjectDtoNew>>> GetCourseSubjectLastNodeList(long cid, int type, string keyword)
        {
            return await courseSubjectService.GetSubjectLastNodeList(cid, type, keyword);
        }

        /// <summary>
        /// 根据案例科目类型获取科目列表
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubjectDtoNew>>> GetCourseSubjectByType(long cid, int type)
        {
            return await courseSubjectService.GetSubjectByType(cid, type);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="type">科目分类</param>
        /// <param name="option">0：岗位实训,1：课程实训</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubjectDtoNew>>> GetAppSubjectByType(long cid, int type, int option)
        {
            if (option == 1)
            {
                return await courseSubjectService.GetSubjectByType(cid, type);
            }
            else
            {
                return await service.GetSubjectByType(cid, type);
            }
        }

        /// <summary>
        /// 添加期初数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddCourseValues(SubjectDataDtoNew model)
        {
            return await courseSubjectService.AddValues(model);
        }

        /// <summary>
        /// 试算平衡
        /// </summary>
        ///
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TrialBalancingDtoNew>> TrialCourseBalancing(long cid)
        {
            return await courseSubjectService.TrialBalancing(cid);
        }
    }
}