﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 认证
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : BaseController
    {
        /// <summary>
        /// 自定义策略参数
        /// </summary>
        PermissionRequirement _requirement;
        IbkAuthenticateService _service;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="requirement"></param>
        public LoginController(PermissionRequirement requirement)
        {
            _requirement = requirement;
            _service = new bkAuthenticateService();
        }

        /// <summary>
        /// 后端系统认证
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>      
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bkUserTicket>> UserAuthentication(UserbkAuthentication model)
        {
            return await _service.bkUserAuthentication(model, HttpContext.GetClientUserIp(), _requirement);
        }
    }
}