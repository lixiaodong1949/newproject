﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.M.IServices;
using Yssx.M.ServiceImpl;
using Yssx.S.Dto;
using Yssx.S.Pocos;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 用户
    /// </summary>
    public class UserController : BaseController
    {
        #region 前端用户
        private IbkUserService _service;
        public UserController()
        {
            _service = new bkUserService();
        }

        /// <summary>
        /// 添加 更新 前端登陆用户
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUserinfo(YssxbkUserDao model)
        {
            return await _service.AddUserinfo(model, CurrentUser.Id);
        }

        [HttpPost]
        public async Task<ResponseContext<bool>> AddUserList(UserListInfoDto dto)
        {
            return await _service.AddUserList(dto);
        }

        /// <summary>
        ///  删除 前端用户
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteUserinfo(long Id)
        {
            return await _service.DeleteUserinfo(Id, CurrentUser.Id);
        }

        /// <summary>
        /// 获取 前端用户【废弃】
        /// </summary>
        /// <param name="mb"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<YssxUserResponse>>> FindUserByName(YssxQuerybkUserDao model)
        {
            return await _service.FindUserByName(model);
        }

        /// <summary>
        /// 获取用户列表（分页） - new
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<UserDto>> GetUserList(GetUserListRequest model)
        {
            return await _service.GetUserList(model);
        }

        /// <summary>
        /// 查询用户统计列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<GetUserCountByPageDto>> GetUserCountByPage(GetUserCountByPageRequest model)
        {
            return await _service.GetUserCountByPage(model);
        }



        /// <summary>
        /// 重置 前端用户密码
        /// </summary>
        /// <param name="mb"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> ResetPassword(ResetPassword md)
        {
            return await _service.ResetPassword(md, CurrentUser.Id);
        }

        /// <summary>
        /// 启用禁用 前端用户状态 
        /// </summary>
        /// <param name="mb"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> StateControl(StateControl md)
        {
            return await _service.StateControl(md, CurrentUser.Id);
        }

        ///// <summary>
        ///// 批量生成 前端用户
        ///// </summary>
        ///// <param name="id">Id</param>
        ///// <returns></returns>    
        //[HttpPost]
        ////[AllowAnonymous]
        //public async Task<ResponseContext<bool>> AddBatchUserinfo(YssxbkUserDao model)
        //{
        //    return await _service.AddBatchUserinfo(model);
        //}
        #endregion

        #region 激活码
        /// <summary>
        /// 随机 生成学校激活码
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<YssxRandomCodeDao>> GetRandomCodeDao()
        {
            return await _service.GetRandomCodeDao();
        }

        /// <summary>
        /// 添加 更新 禁用 学校激活码
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddActivationCode(YssxActivationCodeListDto model)
        {
            return await _service.AddActivationCode(model, CurrentUser.Id);
        }

        /// <summary>
        /// 新增学生激活码
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddStudentActivCode(YssxActivationCodeDto dto)
        {
            return await _service.AddStudentActivCode(dto, CurrentUser.Id);
        }

        /// <summary>
        /// 获取 学校激活码
        /// </summary>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<PageResponse<YssxActivationCodeDto>>> GetActivationCode(YssxQueryActivationCode model)
        {
            return await _service.GetActivationCode(model);
        }

        /// <summary>
        /// 获取学校学生激活码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<YssxActivationCodeDto>>> GetActivationCodeByTypeId(YssxQueryActivationCode model)
        {
            return await _service.GetActivationCodeByTypeId(model);
        }

        /// <summary>
        ///  删除 学校激活码
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteActivationCode(long Id)
        {
            return await _service.DeleteActivationCode(Id, CurrentUser.Id);
        }

        /// <summary>
        /// 导出 学校激活码
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpGet]
        public async Task<IActionResult> ExportActivationCode()
        {
            return await _service.ExportActivationCode();
        }
        #endregion

        #region 后端用户
        /// <summary>
        /// 添加 更新 后端用户
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddbackUserinfo(YssxbkbkUserDao model)
        {
            return await _service.AddBackUserinfo(model, CurrentUser.Id);
        }
        /// <summary>
        /// 重置 后端用户密码
        /// </summary>
        /// <param name="md"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> BkResetPassword(BkResetPassword md)
        {
            return await _service.BkResetPassword(md, CurrentUser.Id);
        }
        #endregion

        /// <summary>
        /// 合伙人报名列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<PartnerDto>>> GetPartnerList()
        {
            return await _service.GetPartnerList();
        }
        /// <summary>
        /// 设置合伙人状态是否已经联系
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpGet]
        public async Task<ResponseContext<bool>> SetPartnerStatus(long id)
        {
            return await _service.SetPartnerStatus(id);
        }

    }
}