﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.Dto.Topics;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 后台订单管理(给学校添加案例，生成试卷)服务
    /// </summary>
    public class SchoolCaseController : BaseController
    {
        private ISchoolCaseService _service;

        public SchoolCaseController(ISchoolCaseService service)
        {
            _service = service;
        }
        /// <summary>
        /// 获取学校订单列表 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<SchoolListView>> GetSchoolListByPage([FromQuery]CaseOrderListRequest model)
        {
            return await _service.GetSchoolListByPage(model);
        }

        /// <summary>
        /// 创建订单
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddCaseOrder(CaseOrderRequest model)
        {
            return await _service.AddCaseOrder(model);
        }
        /// <summary>
        /// 删除订单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCaseOrder(long id)
        {
            return await _service.DeleteCaseOrder(id);
        }
        /// <summary>
        /// 订单开启/禁用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status">状态：0禁用, 1启用 </param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> UpdateCaseOrderStatus(long id, Status status)
        {
            return await _service.UpdateCaseOrderStatus(id, status, CurrentUser.Id);
        }
        /// <summary>
        /// 获取学校案例列表 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<SchoolCaseListView>> GetSchoolCaseListByPage([FromQuery]SchoolCaseListRequest model)
        {
            return await _service.GetSchoolCaseListByPage(model);
        }
        /// <summary>
        /// 给学校添加/修改案例（添加是批量，修改时是单条记录，对应的caseids传一个就好了）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SchoolCaseSet(SchoolCaseRequest model)
        {
            return await _service.SchoolCaseSet(model,CurrentUser.Id);
        }

        /// <summary>
        /// 删除订单明细
        /// </summary>
        /// <param name="id">订单明细id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteSchoolCase(long id)
        {
            return await _service.DeleteSchoolCase(id);
        }
        /// <summary>
        /// 订单明细开启/禁用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status">状态：0禁用, 1启用 </param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> UpdateSchoolCaseStatus(long id, Status status)
        {
            return await _service.UpdateSchoolCaseStatus(id, status, CurrentUser.Id);
        }

        /// <summary>
        /// 开启案例（生成试卷）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> OpenSchoolCase(ShoolCaseStatusSetRequest model)
        {
            return await _service.OpenSchoolCase(model, CurrentUser.Id);
        }
        /// <summary>
        /// 查询案例列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<CaseDto>> GetCaseList(CaseListRequest model)
        {
            return await _service.GetCaseList(model);
        }

        /// <summary>
        /// 套餐案例筛选
        /// </summary>
        /// <param name="moduleId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CaseDto>>> GetCaseByTypeList(int moduleId)
        {
            return await _service.GetCaseByTypeList(moduleId);
        }

        /// <summary>
        /// 自动校准学校免费案例
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> AutoCalibrationFreeCase(long id)
        {
            return await _service.AutoCalibrationFreeCase(id, CurrentUser.Id);
        }

    }
}