﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.M.Api.Auth;

namespace Yssx.M.Api.Controllers
{
    [Produces("application/json")]
    [ApiVersion("1.0", Deprecated = false)]
    //[Route("api/v{api-version:apiVersion}/[controller]/[action]")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors(CommonConstants.AnyOrigin)]//跨域
    [ServiceFilter(typeof(CustomExceptionFilter))]
    public class BaseController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        protected bkUserTicket CurrentUser { get; private set; }

        /// <summary>
        ///  Called before the action method is invoked.
        /// </summary>
        /// <param name="context"></param>
        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            HttpContext.Items.TryGetValue(CommonConstants.ContextUserPropertyKey, out var requestUser);
            CurrentUser = requestUser as bkUserTicket;
        }
    }
}