﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.M.Api.Controllers;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.ServiceImpl;

namespace Yssx.S.Api.Controllers.Manage
{
    /// <summary>
    /// 老师
    /// </summary>
    public class TeacherController : BaseController
    {
        private ITeacherService _service;

        public TeacherController()
        {
            _service = new TeacherService();
        }

        #region 基础操作
        /// <summary>
        /// 添加 更新 教师
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateTeacher(TeacherDto model) {
            return await _service.AddUpdateTeacher(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除老师
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteTeacher(TeacherDto model) {
            return await _service.DeleteTeacher(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取教师列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<TeacherDto>>> GetTeachers(TeacherRequest model) {
            return await _service.GetTeachers(model);
        }

        /// <summary>
        /// 获取指定教师信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TeacherDto>> GetTeacherById(long Id) {
            return await _service.GetTeacherById(Id);
        }

        /// <summary>
        /// 导入老师
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<ImportTeacherResponse>>> ImportTeacher(List<IFormFile> form) {
            return await _service.ImportTeacher(form, CurrentUser.Id);
        }

        /// <summary>
        /// 导出老师
        /// </summary>
        /// <returns></returns>
        //[Microsoft.AspNetCore.Authorization.AllowAnonymous]
        //Task<IActionResult> ExportTeacher(TeacherType TeacherType, long TenantId, string KeyWord);
        #endregion

        #region 其他操作
        /// <summary>
        /// 学生审核 (只需要传入Id、Type和Status 三个个参数)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> TeacherAudit(TeacherDto model) {
            return await _service.TeacherAudit(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取全部教师列表
        /// </summary>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<TeacherDto>>> GetWhole(TeacherRequest model) {
            return await _service.GetWhole(model);
        }

        /// <summary>
        /// 修改指定教师 用户状态
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> ChangeTeacherStatus(long Id) {
            return await _service.ChangeTeacherStatus(Id, CurrentUser.Id);
        }

        /// <summary>
        /// 绑定班级
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddClassToTeacher(TeacherClassRequest model) {
            return await _service.AddClassToTeacher(model, CurrentUser.Id);
        }

        #endregion
    }
}