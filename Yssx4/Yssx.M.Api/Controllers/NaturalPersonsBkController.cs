﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 自然人-后台管理
    /// </summary>
    public class NaturalPersonsBkController :  BaseController
    {
        private INaturalPersonsService _service;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public NaturalPersonsBkController(INaturalPersonsService service)
        {
            _service = service;
        }

        /// <summary>
        /// 获取 人员列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<PersonnelListResponse>> GetPersonnelbkList(PersonnelListBkDao model)
        {
            return await _service.GetPersonnelbkList(model);
        }

        /// <summary>
        /// 获取 子女教育列表列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<ChildrenListResponse>> GetChildrenbkList(DeductionIncomeListBkDao model)
        {
            return await _service.GetChildrenbkList(model);
        }

        /// <summary>
        /// 获取 继续教育支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<ContinueListResponse>> GetContinuebkList(DeductionIncomeListBkDao model)
        {
            return await _service.GetContinuebkList(model);
        }

        /// <summary>
        /// 获取 住房贷款利息支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<InterestListResponse>> GetInterestbkList(DeductionIncomeListBkDao model)
        {
            return await _service.GetInterestbkList(model);
        }


        /// <summary>
        /// 获取 住房租金支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<RentListResponse>> GetRentbkList(DeductionIncomeListBkDao model)
        {
            return await _service.GetRentbkList(model);
        }

        /// <summary>
        /// 获取 赡养老人支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<SupportListResponse>> GetSupportbkList(DeductionIncomeListBkDao model)
        {
            return await _service.GetSupportbkList(model);
        }

        /// <summary>
        /// 获取 综合所得申报列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<IncomeListResponse>> GetIncomebkList(DeductionIncomeListBkDao model)
        {
            return await _service.GetIncomebkList(model);
        }





        /// <summary>
        /// 添加 更新 人员
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdatePersonnel(PersonnelDao model)
        {
            return await _service.AddUpdatePersonnel(model, CurrentUser.Id);
        }

        /// <summary>
        /// 添加 更新 子女教育支出
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateChildren(ChildrenDao model)
        {
            return await _service.AddUpdateChildren(model, CurrentUser.Id);
        }

        /// <summary>
        /// 添加 更新 继续教育支出
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateContinue(ContinueDao model)
        {
            return await _service.AddUpdateContinue(model, CurrentUser.Id);
        }

        /// <summary>
        /// 添加 更新 住房贷款利息支出
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateInterest(InterestDao model)
        {
            return await _service.AddUpdateInterest(model, CurrentUser.Id);
        }

        /// <summary>
        /// 添加 更新 住房租金支出
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateRent(RentDao model)
        {
            return await _service.AddUpdateRent(model, CurrentUser.Id);
        }

        /// <summary>
        /// 添加 更新 赡养老人支出
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateSupport(SupportDao model)
        {
            return await _service.AddUpdateSupport(model, CurrentUser.Id);
        }

        /// <summary>
        /// 添加 更新 综合所得申报
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateIncome(IncomeDao model)
        {
            return await _service.AddUpdateIncome(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> BkDelete(IdDao model)
        {
            return await _service.BkDelete(model, CurrentUser.Id);
        }



        /// <summary>
        /// 批量导入 自然人各模块
        /// </summary>
        /// <param name="file"></param>
        /// <param name="caseid">案例id</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> ImportNaturalPersons(IFormFile file,long caseid)
        {
             return await _service.ImportNaturalPersons(file, caseid, CurrentUser.Id);
        }
    }
}
