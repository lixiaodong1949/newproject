﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.IServices.Topics;
using Yssx.S.ServiceImpl;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 配置
    /// </summary>
    public class ResourceSettingController : BaseController
    {
        IResourceSettingService settingService;
        public ResourceSettingController()
        {
            settingService = new ResourceSettingService();
        }

        #region 案例标签
        /// <summary>
        /// 获取案例标签列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<TagDto>> GetTagList(TagRequest model)
        {
            return await settingService.GetTagList(model);
        }

        /// <summary>
        /// 新增，编辑案例标签
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> AddOrEditTag(TagDto model)
        {
            if (model.Id == 0)
                model.TagSourceType = TagSourceType.BackManage;
            return await settingService.AddOrEditTag(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除案例标签
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveTag(long id)
        {
            return await settingService.RemoveTag(id, CurrentUser.Id);
        }

        #endregion

        #region 知识点
        /// <summary>
        /// 获取知识点下拉列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<KnowledgePointDto>>> GetKnowledgePointList(long ignoreId)
        {
            return await settingService.GetKnowledgePointList(ignoreId);
        }

        /// <summary>
        /// 新增，编辑知识点
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<long>> AddOrEditKnowledgePoint(KnowledgePointDto model)
        {
            return await settingService.AddOrEditKnowledgePoint(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除知识点
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveKnowledgePoint(long id)
        {
            return await settingService.RemoveKnowledgePoint(id, CurrentUser.Id);
        }
        #endregion

        #region 行业
        /// <summary>
        /// 获取行业列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<IndustryDto>> GetIndustryList(IndustryRequest model)
        {
            return await settingService.GetIndustryList(model);
        }

        /// <summary>
        /// 新增，编辑行业
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditIndustry(IndustryDto model)
        {
            return await settingService.AddOrEditIndustry(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除行业
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveIndustry(long id)
        {
            return await settingService.RemoveIndustry(id, CurrentUser.Id);
        }
        #endregion

        #region 业务场景
        /// <summary>
        /// 获取业务场景列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<BusinessSceneDto>> GetBusinessSceneList(BusinessSceneRequest model)
        {
            return await settingService.GetBusinessSceneList(model);
        }

        /// <summary>
        /// 新增，编辑业务场景
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditBusinessScene(BusinessSceneDto model)
        {
            return await settingService.AddOrEditBusinessScene(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除业务场景
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveBusinessScene(long id)
        {
            return await settingService.RemoveBusinessScene(id, CurrentUser.Id);
        }
        #endregion




    }
}