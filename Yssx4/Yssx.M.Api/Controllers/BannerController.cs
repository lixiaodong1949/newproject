﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.M.IServices;
using Yssx.M.ServiceImpl;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// Banner图
    /// </summary>
    public class BannerController : BaseController
    {
        private IBannerService _service;
        public BannerController(IBannerService service)
        {
            _service = service;
        }
        /// <summary>
        /// 增加/修改
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateBanner(BannerRequest model)
        {
            return await _service.AddUpdateBanner(model, CurrentUser.Id);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteBanner(long id)
        {
            return await _service.DeleteBanner(id);
        }

        /// <summary>
        /// 查询Banner列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<BannerInfoDto>>> GetBannerInfoList(ClientType? clientType)
        {
            return await _service.GetBannerInfoList(clientType);
        }

    }
}