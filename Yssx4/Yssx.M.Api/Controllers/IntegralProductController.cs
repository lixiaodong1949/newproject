using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 积分商品
    /// </summary>
    public class IntegralProductController : BaseController
    {
        private IIntegralProductService _service;

        public IntegralProductController(IIntegralProductService service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(IntegralProductDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }

            result = await _service.Save(model, CurrentUser.Id);

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<IntegralProductDto>> Get(long id)
        {
            var response = new ResponseContext<IntegralProductDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Get(id);

            return response;
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        [HttpDelete]
        public async Task<ResponseContext<bool>> Delete(long id)
        {
            var response = new ResponseContext<bool>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Delete(CurrentUser.Id, id);

            return response;
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<IntegralProductPageResponseDto>> ListPage(IntegralProductPageRequestDto query)
        {
            var response = new PageResponse<IntegralProductPageResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.ListPage(query);

            return response;
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        private async Task<ListResponse<IntegralProductListResponseDto>> List(IntegralProductListRequestDto query)
        {
            var response = new ListResponse<IntegralProductListResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.List(query);

            return response;
        }

        /// <summary>
        /// 积分商品上下架
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> Active(long id, bool IsActive)
        {
            var response = new ResponseContext<bool> { Code = CommonConstants.BadRequest };
            //if (query == null)
            //{
            //    response.Msg = "参数不能为空";
            //    return response;
            //}

            response = await _service.Active(id, IsActive, CurrentUser.Id);

            return response;
        }
    }
}