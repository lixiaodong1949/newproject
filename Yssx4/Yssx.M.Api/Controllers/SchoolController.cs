﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.IServices.Means;
using Yssx.S.ServiceImpl;
using Yssx.S.ServiceImpl.Means;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 学校
    /// </summary>
    public class SchoolController : BaseController
    {
        private ISchoolService _service;
        private IActivateSchoolService _activateService;

        public SchoolController()
        {
            _service = new SchoolService();
            _activateService = new ActivateSchoolService();
        }

        #region 学校
        /// <summary>
        /// 添加 更新 学校
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateSchool(SchoolDto model) {
            return await _service.AddUpdateSchool(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除学校
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteSchool(SchoolDto model) {
            return await _service.DeleteSchool(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取学校列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<SchoolDto>>> GetSchoolList(SchoolPageRequest model) {
            return await _service.GetSchoolList(model);
        }

        /// <summary>
        /// 导出学校
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> ExportSchool(long tenantId, string name) {
            return await _service.ExportSchool(tenantId, name);
        }
        #endregion

        #region 院系
        /// <summary>
        /// 添加 更新 院系
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateSchoolCollege(CollegeDto model){
            return await _service.AddUpdateSchoolCollege(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除院系
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteSchoolCollege(CollegeDto model) {
            return await _service.DeleteSchoolCollege(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取 学校-院系 列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<CollegeDto>>> GetSchoolColleges(SchoolCollegeRequest model) {
            return await _service.GetSchoolColleges(model);
        }

        /// <summary>
        /// 导出院系
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> ExportCollege(long tenantId, string name) {
            return await _service.ExportCollege(tenantId, name);
        }
        #endregion

        #region 专业
        /// <summary>
        /// 添加 更新 专业
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateCollegeProfession(ProfessionDto model) {
            return await _service.AddUpdateCollegeProfession(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除专业
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteCollegeProfession(ProfessionDto model) {
            return await _service.DeleteCollegeProfession(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取 学校-院系-专业 列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<ProfessionDto>>> GetCollegeProfessions(CollegeProfessionsRequest model) {
            return await _service.GetCollegeProfessions(model);
        }

        /// <summary>
        /// 导出专业
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> ExportProfession(long tenantId, long collegesId, string name) {
            return await _service.ExportProfession(tenantId, collegesId, name);
        }
        #endregion

        #region 班级
        /// <summary>
        /// 添加 更新 班级
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateClass(ClassDto model) {
            return await _service.AddUpdateClass(model, CurrentUser.Id);
        }
        /// <summary>
        /// 删除班级
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteClass(ClassDto model) {
            return await _service.DeleteClass(model, CurrentUser.Id);
        }
        /// <summary>
        /// 获取 学校-院系-专业-班级 列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<ClassDto>>> GetClasses(ClassRequest model) {
            return await _service.GetClasses(model);
        }
        #endregion

        #region 激活记录
        /// <summary>
        /// 学校激活记录
        /// </summary>
        /// <param name="model">搜索条件</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<ActivationRecordDto>>> ActivationRecord(ActivationRecordPageRequest model) {
            return await _service.ActivationRecord(model);
        }

        /// <summary>
        /// 禁用,启用,换码
        /// </summary>
        /// <param name="id">禁用启用传UserId，换码传记录Id</param>
        /// <param name="type">0禁用1启用2换码</param>
        /// <param name="code">激活码</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> UpdateActivationRecord(long id, int type, string code) {
            return await _service.UpdateActivationRecord(id,type,code);
        }

        /// <summary>
        /// 解绑激活码
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> UntieActivationCode(long userId)
        {
            return await _activateService.UntieActivationCode(userId, CurrentUser.Id);
        }

        
        #endregion

    }
}