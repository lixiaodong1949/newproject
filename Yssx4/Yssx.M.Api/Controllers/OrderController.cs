﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto.Order;
using Yssx.S.IServices;
using Yssx.S.IServices.Order;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 订单 - 待废弃
    /// </summary>
    public class OrderController : BaseController
    {
        private IPaymentService _paymentService;
        private IProductCouponsService couponsService;

        public OrderController(IPaymentService paymentService, IProductCouponsService _couponsService)
        {
            _paymentService = paymentService;
            couponsService = _couponsService;
        }

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<RtpOrderDto>> GetOrderList(GetRtpOrderDto input)
        {
            return await _paymentService.GetOrderList(input);
        }

        /// <summary>
        /// 统计
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<OrderStatsDto>> GetStats()
        {
            return await _paymentService.GetStats();
        }

        /// <summary>
        /// 获取用户所有订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<UserRtpOrder>> GetUserOrders([FromQuery]GetUserRtpOrderDto input)
        {
            return await _paymentService.GetUserOrders(input);
        }

        /// <summary>
        /// 取消订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> CancelOrder(CancelOrderInputDto input)
        {
            return await _paymentService.CancelOrder(input, CurrentUser);
        }

        /// <summary>
        /// 自动验证产品卡券
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> AutoExchangeCouponsCode(string ignoreMobilePhone)
        {
            return await couponsService.AutoExchangeCouponsCode(ignoreMobilePhone, CurrentUser.Id);
        }
    }
}