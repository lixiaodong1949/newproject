﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 常见问题
    /// </summary>
    public class FAQController : BaseController
    {
        private IFAQService _service;
        public FAQController()
        {
            _service = new FAQService();
        }

        #region 问题分类
        /// <summary>
        /// 添加/编辑 问题分类
        /// </summary>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditFAQClass(FAQClassDto model)
        {
            return await _service.AddOrEditFAQClass(model, CurrentUser.Id);
        }

        /// <summary>
        ///  删除 问题分类
        /// </summary>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteFAQClass(long id)
        {
            return await _service.DeleteFAQClass(id, CurrentUser.Id);
        }

        /// <summary>
        /// 获取问题分类列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<FAQClassDto>> GetFAQClassByPage(GetFAQClassByPageRequest model)
        {
            return await _service.GetFAQClassByPage(model);
        }

        #endregion

        #region 常见问题
        /// <summary>
        /// 添加/编辑 常见问题
        /// </summary>
        /// <returns></returns>    
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditFAQInfo(FAQInfoDto model)
        {
            return await _service.AddOrEditFAQInfo(model, CurrentUser.Id);
        }

        /// <summary>
        ///  删除 常见问题
        /// </summary>
        /// <returns></returns>    
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteFAQInfo(long id)
        {
            return await _service.DeleteFAQInfo(id, CurrentUser.Id);
        }

        /// <summary>
        /// 获取常见问题列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<FAQInfoDto>> GetFAQInfoByPage(GetFAQInfoByPageRequest model)
        {
            return await _service.GetFAQInfoByPage(model);
        }

        /// <summary>
        /// 获取指定常见问题
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<FAQInfoOneDto>> GetFAQInfoById(long id)
        {
            return await _service.GetFAQInfoById(id);
        }

        #endregion

    }
}