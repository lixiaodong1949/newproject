﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.ServiceImpl;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 学生
    /// </summary>
    public class StudentController : BaseController
    {
        /// <summary>
        /// 接口
        /// </summary>
        private IStudentService _service;

        public StudentController()
        {
            _service = new StudentService();
        }

        /// <summary>
        /// 学生信息审核
        /// </summary>
        /// <returns>学生</returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> StudentAudit(StudentDto model) {
                return await _service.StudentAudit(model, CurrentUser.Id);
            }

        /// <summary>
        /// 添加 更新 学生
        /// </summary>
        /// <returns>学生</returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddUpdateStudent(StudentDto model) {
                return await _service.AddUpdateStudent(model, CurrentUser.Id);
            }

        /// <summary>
        /// 获取学生列表
        /// </summary>
        [HttpPost]
        public async Task<ResponseContext<StudentDtoSummary>> GetStudents(GetStudentRequest model) {
                return await _service.GetStudents(model, CurrentUser.Id);
            }

        /// <summary>
        /// 删除学生
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteStudent(StudentDto model) {
                return await _service.DeleteStudent(model, CurrentUser.Id);
            }

        /// <summary>
        /// 导出学生
        /// </summary>
        /// <returns></returns>
            //[Microsoft.AspNetCore.Authorization.AllowAnonymous]
            //Task<IActionResult> ExportStudent(long classId, long entranceYear, string keyWord, string mobilePhone);

            /// <summary>
            /// 导入学生
            /// </summary>
            /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<ImportStudentResponse>>> ImportStudent(IFormFile file) {
            {
                return await _service.ImportStudent(file, CurrentUser.Id);
            }
        }
    }
}