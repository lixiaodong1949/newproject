﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto.ResourcePackge;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 资源套餐
    /// </summary>
    public class ResourcePackController : BaseController
    {
        private IResoursePackDetailsService _service;
        private IBindingResourseToSchool _rService;
        public ResourcePackController(IResoursePackDetailsService service, IBindingResourseToSchool resourseService)
        {
            _service = service;
            _rService = resourseService;
        }

        /// <summary>
        /// 绑定学校资源
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> BindingResourse(BindingResourseDto dto)
        {
            return await _rService.BindingResourse(dto);
        }

        /// <summary>
        /// 获取学校下面的套餐
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<BindingResourseDto>>> GetSchoolResourseList(long schoolId)
        {
            return await _rService.GetSchoolResourseList(schoolId);
        }

        /// <summary>
        /// 新增套餐资源
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddResourceDetails(AddResourceDetailsDto dto)
        {
            return await _service.AddResourceDetails(dto);
        }

        /// <summary>
        /// 新增套餐
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]

        public async Task<ResponseContext<bool>> AddResoursePeck(ResoursePeckDto dto)
        {
            return await _service.AddResoursePeck(dto);
        }

        /// <summary>
        /// 删除资源
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ResponseContext<bool>> DeleteResourceDetails(long id)
        {
            return await _service.DeleteResourceDetails(id);
        }

        /// <summary>
        /// 删除套餐
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ResponseContext<bool>> DeleteResoursePeck(long id)
        {
            return await _service.DeleteResoursePeck(id);
        }

        /// <summary>
        /// 获取套餐资源详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<ResourcePackDetailDto>>> GetResourcePackDetailsList(long id)
        {
            return await _service.GetResourcePackDetailsList(id);
        }

        /// <summary>
        /// 获取套餐列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<ResoursePeckDto>>> GetResoursePeckList(int eType = -1)
        {
            return await _service.GetResoursePeckList(eType);
        }

        /// <summary>
        /// 模块列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ResponseContext<List<ModuleDto>> GetModuleList()
        {
            List<ModuleDto> list = new List<ModuleDto>
            {
                new ModuleDto{Id=1,Name="岗位实训"},
                new ModuleDto{Id=2,Name="课程实训"},
                new ModuleDto{Id=3,Name="财务大数据"},
                new ModuleDto{Id=4,Name="业税财实训"},
                new ModuleDto{Id=5,Name="管理会计"},
                new ModuleDto{Id=6,Name="云实习"},
                new ModuleDto{Id=7,Name="同步课程实习"},
                new ModuleDto{Id=8,Name="业财税实习"},
                new ModuleDto{Id=9,Name="经典案例"},
            };
            return new ResponseContext<List<ModuleDto>> { Data = list };
        }
    }
}
