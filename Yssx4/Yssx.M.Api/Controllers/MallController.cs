﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.Dto.Topics;
using Yssx.S.IServices;
using Yssx.S.IServices.Mall;
using Yssx.S.IServices.Topics;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 商城订单和商品
    /// </summary>
    public class MallController : BaseController
    {
        private readonly IMallOrderService _mallOrderService;
        private readonly IMallProductService _mallProductService;
        private readonly IUploadCaseService _uploadCaseService;
        private readonly ICourseService _courseService;
        private readonly ICaseService _caseService;
        private readonly ICaseServiceNew _caseServiceNew;
        private readonly IInternshipProveService _internshipProveService;

        public MallController(IMallOrderService mallOrderService,
            IMallProductService mallProductService, 
            IUploadCaseService uploadCaseService, 
            ICourseService courseService, 
            ICaseService caseService,
            ICaseServiceNew caseServiceNew,
            IInternshipProveService internshipProveService)
        {
            _mallOrderService = mallOrderService;
            _mallProductService = mallProductService;
            _uploadCaseService = uploadCaseService;
            _courseService = courseService;
            _caseService = caseService;
            _caseServiceNew = caseServiceNew;
            _internshipProveService = internshipProveService;
        }

        #region 订单
        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<MallOrderDto>> GetOrderList(GetMallOrderDto input)
        {
            return await _mallOrderService.GetOrderList(input);
        }

        /// <summary>
        /// 统计
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<MallOrderStatsDto>> GetStats()
        {
            return await _mallOrderService.GetStats();
        }

        /// <summary>
        /// 获取用户订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<UserMallOrder>> GetUserOrders([FromQuery] GetUserMallOrder input)
        {
            return await _mallOrderService.GetUserOrders(input);
        }
        #endregion

        #region 商品

        /// <summary>
        /// 保存商品
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveProduct(SaveProductDto model)
        {
            return await _mallProductService.SaveProduct(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除商品
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteProduct(long id)
        {
            return await _mallProductService.DeleteProduct(id, CurrentUser.Id);
        }

        /// <summary>
        /// 获取商品列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<GetProductByPageDto>> GetProductByPage(GetProductByPageRequest model)
        {
            return await _mallProductService.GetProductByPage(model);
        }

        /// <summary>
        /// 获取指定商品数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<SaveProductDto>> GetProductInfoById(long id)
        {
            return await _mallProductService.GetProductInfoById(id);
        }

        /// <summary>
        /// 商品上下架
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetProductStatus(long id, bool isActive)
        {
            return await _mallProductService.SetProductStatus(id, isActive, CurrentUser.Id);
        }



        /// <summary>
        /// 保存商品明细
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveProductDetail(SaveProductDetailDto model)
        {
            return await _mallProductService.SaveProductDetail(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除商品明细
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteProductDetail(long id, long productId)
        {
            return await _mallProductService.DeleteProductDetail(id, productId, CurrentUser.Id);
        }

        /// <summary>
        /// 商品移出指定分类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteProductCategory(long id, long categoryId)
        {
            return await _mallProductService.DeleteProductCategory(id, categoryId, CurrentUser.Id);
        }

        /// <summary>
        /// 设置商品分类信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SetProductCategoryInfo(SetProductCategoryInfoDto model)
        {
            return await _mallProductService.SetProductCategoryInfo(model, CurrentUser.Id);
        }

        /// <summary>
        /// 设置商品排序
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SetProductSortInfo(SetProductSortInfoDto model)
        {
            return await _mallProductService.SetProductSortInfo(model, CurrentUser.Id);
        }


        #endregion

        #region 商品分类
        /// <summary>
        /// 获取商品分类列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<GetMallCategoryListDto>> GetMallCategoryList()
        {
            return await _mallProductService.GetMallCategoryList();
        }

        /// <summary>
        /// 保存商品分类
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveMallCategory(SaveMallCategoryDto model)
        {
            return await _mallProductService.SaveMallCategory(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除商品分类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteMallCategory(long id)
        {
            return await _mallProductService.DeleteMallCategory(id, CurrentUser.Id);
        }

        /// <summary>
        /// 获取二级分类下商品列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<GetSecondLevelProductByPageDto>> GetSecondLevelProductByPage(GetSecondLevelProductByPageRequest model)
        {
            return await _mallProductService.GetSecondLevelProductByPage(model);
        }

        /// <summary>
        /// 分类升为一级分类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetMallCategoryLevel(long id)
        {
            return await _mallProductService.SetMallCategoryLevel(id, CurrentUser.Id);
        }


        #endregion

        #region 获取来源数据列表（经典案例、平台课程、岗位实训案例）

        /// <summary>
        /// 获取关联数据 经典案例列表
        /// </summary>
        /// <returns></returns>    
        [HttpPost]
        public async Task<PageResponse<UploadCaseResult>> GetBindDataInClassicCase(UploadCaseQueryDao model)
        {
            return await _uploadCaseService.FindUpdateCase(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取关联数据 课程列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<YssxCourseDto>> GetBindDataInCourse(GetBindDataInCourseDto model)
        {
            return await _mallProductService.GetBindDataInCourse(model);
        }

        /// <summary>
        /// 查询关联数据 案例列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<CaseDto>> GetBindDataInCase(CaseListRequest model)
        {
            return await _caseService.GetCaseList(model);
        }
        #endregion

        #region 生成试卷
        /// <summary>
        /// 购买后自动生成试卷 - 同步实训
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AutoGenerateTestPaperSync(AutoGenerateTestPaperDto model)
        {
            model.Category = 1;
            return await _caseServiceNew.AutoGenerateTestPaper(model);
        }

        /// <summary>
        /// 购买后自动生成试卷 - 岗位实训
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AutoGenerateTestPaperJob(AutoGenerateTestPaperDto model)
        {
            return await _internshipProveService.AutoGenerateTestPaper(model);
        }

        /// <summary>
        /// 购买后自动生成试卷 - 经典案例
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AutoGenerateTestPaperClassic(AutoGenerateTestPaperDto model)
        {
            return await _uploadCaseService.AutoGenerateTestPaper(model);
        }

        /// <summary>
        /// 购买后自动生成试卷 - 初级会计考证
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AutoGenerateTestPaperPrimary(AutoGenerateTestPaperDto model)
        {
            return await _courseService.AutoGenerateTestPaperPrimary(model, CurrentUser.Id);
        }

        /// <summary>
        /// 购买后自动生成试卷 - 课程实训
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AutoGenerateCourse(AutoGenerateTestPaperDto model)
        {
            return await _courseService.AutoGenerateCourse(model, CurrentUser.Id);
        }



        /// <summary>
        /// 获取手动配置记录列表（分页）
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<FreeTestPaperDto>> GetFreeTestPaper(GetFreeTestPaperRequest model)
        {
            return await _mallOrderService.GetFreeTestPaper(model);
        }
        #endregion

    }
}
