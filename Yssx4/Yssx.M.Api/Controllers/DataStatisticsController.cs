﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.S.Dto.Statistics;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 平台数据统计
    /// </summary>
    public class DataStatisticsController : BaseController
    {
        private IDataStatistics _service;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sevice"></param>
        public DataStatisticsController(IDataStatistics sevice)
        {
            _service = sevice;
        }

        /// <summary>
        /// 平台资源汇总
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<ResourceStatisticsDto>>> GetResourceStatistics()
        {
            return await _service.GetResourceStatistics();
        }

        /// <summary>
        /// 获取省份统计日期数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<string>>> GetDates()
        {
            return await _service.GetDates();
        }

        /// <summary>
        /// 省份统计数据
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<RegionDataDto>>> GetRegionStatistics(string date)
        {
            return await _service.GetRegionStatistics(date);
        }

        /// <summary>
        /// 每天做题量
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<StudentTopicDetailDto>>> GetStudentTopicDetails()
        {
            return await _service.GetStudentTopicDetails();
        }

        /// <summary>
        /// 任务发布数
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<TaskStatisticsDto>>> GetTaskStatistics()
        {
            return await _service.GetTaskStatistics();
        }

        /// <summary>
        /// 每天活跃人数
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<UserStatisticsDto>>> GetUserStatistics()
        {
            return await _service.GetUserStatistics();
        }

        /// <summary>
        /// 学校活跃每天统计
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<UserStatisticsDto>>> GetSchoolUseStatistics()
        {
            return await _service.GetSchoolUseStatistics();
        }
        /// <summary>
        /// 学校使用排名
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<SchoolUseStatis>>> GetSchoolUseDataOrderBy()
        {
            return await _service.GetSchoolUseDataOrderBy();
        }

        /// <summary>
        /// 学校每月活跃统计
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<UserStatisticsDto>>> GetSchoolUseMonthStat()
        {
            return await _service.GetSchoolUseMonthStat();
        }

        /// <summary>
        /// 月活用户
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<UserStatisticsDto>>> GetUserByMonthStatistics()
        {
            return await _service.GetUserByMonthStatistics();
        }
    }
}