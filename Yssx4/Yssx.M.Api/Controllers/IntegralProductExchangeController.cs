using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.M.Api.Controllers
{
    /// <summary>
    /// 积分商品兑换
    /// </summary>
    public class IntegralProductExchangeController : BaseController
    {
        private IIntegralProductExchangeService _service;

        public IntegralProductExchangeController(IIntegralProductExchangeService service)
        {
            _service = service;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<IntegralProductExchangeDto>> Get(long id)
        {
            var response = new ResponseContext<IntegralProductExchangeDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Get(id);

            return response;
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        [HttpDelete]
        public async Task<ResponseContext<bool>> Delete(long id)
        {
            var response = new ResponseContext<bool>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Delete(CurrentUser.Id, id);

            return response;
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<IntegralProductExchangePageResponseDto>> ListPage(IntegralProductExchangePageRequestDto query)
        {
            var response = new PageResponse<IntegralProductExchangePageResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.ListPage(query);

            return response;
        }

        /// <summary>
        /// 修改发货信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UpdateDeliveryInfo(UpdateDeliveryInfoRequestDto model)
        {
            var response = new ResponseContext<bool> { Code = CommonConstants.BadRequest };
            if (model == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.UpdateDeliveryInfo(model, CurrentUser.Id);

            return response;
        }

        /// <summary>
        /// 修改收货地址
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UpdateAddressInfo(ProductExchangeAddressRequestDto model)
        {
            var response = new ResponseContext<bool> { Code = CommonConstants.BadRequest };
            if (model == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.UpdateAddressInfo(model, CurrentUser.Id);

            return response;
        }
    }
}