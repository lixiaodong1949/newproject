﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.M.Api.Controllers
{

    public class NewsController : BaseController
    {
        private INewsService _service;
        public NewsController(INewsService service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加新闻
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditNews(YssxNewsDto dto)
        {
            return await _service.AddOrEditNews(dto);
        }

        /// <summary>
        /// 新闻列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<YssxNewsDto>>> GetNewsList()
        {
            return await _service.GetNewsList();
        }

        /// <summary>
        /// 设置新闻状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> SetStatus(long id, int status)
        {
            return await _service.SetStatus(id,status);
        }
    }
}
