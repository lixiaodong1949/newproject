using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Text;
using Tas.Common.Configurations;
using Yssx.Framework;
using Yssx.Framework.Logger;
using Yssx.Framework.Web.Filter;
using Yssx.Swagger;
using NLog.Extensions.Logging;
using Yssx.Redis;
using Yssx.Framework.Dal;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Auth;
using Yssx.M.Api.Auth;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;
using Yssx.S.ServiceImpl.Examination;
using Yssx.S.Service;
using Yssx.S.IServices.Mall;
using Yssx.S.ServiceImpl.Mall;
using Yssx.S.IServices.Topics;
using Yssx.S.ServiceImpl.Topics;

namespace Yssx.M.Api
{
    public class Startup
    {
        private CustsomSwaggerOptions CURRENT_SWAGGER_OPTIONS = new CustsomSwaggerOptions()
        {
            ProjectName = "后台管理云上实训",
            ApiVersions = new string[] { "v1" },//要显示的版本
            UseCustomIndex = false,
            RoutePrefix = "swagger",

            AddSwaggerGenAction = c =>
            {
                var xmlPaths = Directory.GetFiles(System.AppContext.BaseDirectory, "Yssx.*.xml").ToList();
                foreach (var path in xmlPaths)
                {
                    c.IncludeXmlComments(path, true);
                }
            },
            UseSwaggerAction = c =>
            {

            },
            UseSwaggerUIAction = c =>
            {
            }
        };
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        private void AddTokenValidation(IServiceCollection services)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(CommonConstants.IdentServerSecret));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var permissionRequirement = new PermissionRequirement(signingCredentials);
            services.AddSingleton(permissionRequirement);
        }
        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<CustomExceptionFilter>();
            services.AddMvc(options =>
            {
                options.Filters.Add<ActionModelStateFilter>();
                options.Filters.Add<AuthorizationFilter>();
                //options.Filters.Add<Filters.HttpGlobalExceptionFilter>();

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddJsonOptions(options =>
            {
                //忽略循环引用
                //options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                //options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                //设置时间格式
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";

            }).ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            //版本控制
            // services.AddMvcCore().AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'VVV");

            services.AddApiVersioning(option =>
            {
                // allow a client to call you without specifying an api version
                // since we haven't configured it otherwise, the assumed api version will be 1.0
                option.AssumeDefaultVersionWhenUnspecified = true;
                option.ReportApiVersions = false;
            })
            .AddCustomSwagger(CURRENT_SWAGGER_OPTIONS)
            .AddCors(options =>
            {
                options.AddPolicy(CommonConstants.AnyOrigin, builder =>
                {
                    builder.AllowAnyOrigin() //允许任何来源的主机访问                             
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();//指定处理cookie                                   
                });
            });
            AddTokenValidation(services);
            InitService(services);
        }
        private void InitService(IServiceCollection services)
        {
            //yssxDI,服务注入
            services.AddTransient<IIntegralProductExchangeService, IntegralProductExchangeService>();//积分商品兑换
            services.AddTransient<IIntegralProductService, IntegralProductService>();//积分商品
            services.AddTransient<IIntegralService, IntegralService>();//用户积分
            services.AddTransient<IBannerService, BannerService>();
            services.AddTransient<IDataStatistics, DataStatisticsService>();
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<IProductCouponsService, ProductCouponsService>();
            services.AddTransient<IInformation, InformationService>(); 
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IMallOrderService, MallOrderService>();
            services.AddTransient<IMallProductService, MallProductService>();
            services.AddTransient<IUploadCaseService, UploadCaseService>();
            services.AddTransient<ICaseService, CaseService>();
            services.AddTransient<IInternshipProveService, InternshipProveService>();
            services.AddTransient<IResoursePackDetailsService, ResoursePackService>();
            services.AddTransient<IBindingResourseToSchool, BindingResourseToSchool>();
            services.AddTransient<INewsService, NewsService>();
            services.AddTransient<INoticeService, NoticeService>(); 
            services.AddTransient<IAdvertisingSpaceService, AdvertisingSpaceService>();
            services.AddTransient<INaturalPersonsService, NaturalPersonsService>();
            #region 同步实训
            //yssxnewDI,服务注入
            services.AddTransient<ICaseServiceNew, CaseServiceNew>();
            services.AddTransient<IDrillTypeService, DrillTypeService>();
            services.AddTransient<IFlowService, FlowService>();
            services.AddTransient<ITopicServiceNew, TopicServiceNew>();
            services.AddTransient<ICaseDepartmentServiceNew, CaseDepartmentServiceNew>();
            services.AddTransient<ISubjectServiceNew, SubjectServiceNew>();
            services.AddTransient<ICourseSubjectServiceNew, CourseSubjectServiceNew>();
            services.AddTransient<ITemplateServiceNew, TemplateServiceNew>();
            services.AddTransient<ISummaryServiceNew, SummaryServiceNew>();
            services.AddTransient<ICourseSummaryServiceNew, CourseSummaryServiceNew>();
            #endregion
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseExceptionHandler(builder =>
            {
                builder.Run(async context =>
                {
                    context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                    context.Response.ContentType = "application/json";
                    var ex = context.Features.Get<Microsoft.AspNetCore.Diagnostics.IExceptionHandlerFeature>();
                    if (ex?.Error != null)
                    {
                        CommonLogger.Error("UseExceptionHandler", ex?.Error);
                    }
                    await context.Response.WriteAsync(ex?.Error?.Message ?? "an error occure");
                });
            }).UseHsts();

            app.UseMvc()
                .UseCustomSwagger(CURRENT_SWAGGER_OPTIONS);

            app.UseCors(CommonConstants.AnyOrigin);
            app.UseStaticFiles();

            //DataMergeHelper.Start();

            ProjectConfiguration.GetInstance().
                RegisterConfiguration(Configuration).
                UseCommonLogger(loggerFactory.AddNLog()).
                UseRedis(Configuration["RedisConnection"]).
                UseFreeSqlRepository(Configuration["DbConnection"], Configuration["ReadDbConnection1"], Configuration["ReadDbConnection2"]).
                UseFreeSqlJSRepository(Configuration["JSDbConnection"]).
                UseFreeSqlJNRepository(Configuration["JNDbConnection"]).
                UseFreeSqlRepositoryYsx(Configuration["YsxDbConnection"]).
                UseFreeSqlTCRepository(Configuration["TCDbConnection"]).
                UseFreeSqlPracticeRepository(Configuration["UPDbConnection"]).
                UseAutoMapper(Framework.Utils.RuntimeHelper.GetAllAssemblies());
        }
    }
}
