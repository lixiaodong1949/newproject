﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace PushMessageService
{
    /// <summary>
    /// 返回上下文
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PushResponseContext<T> : ResponseContext<T>
    {
        public PushResponseContext() : base() { }
        public PushResponseContext(int code, string msg) : base(code, msg) { }
        public PushResponseContext(int code, string msg, T data) : base(code, msg, data) { }

        public PushResponseContext(T data) : base(data) { }

        ///// <summary>
        ///// 设置失败
        ///// </summary>
        ///// <param name="baseResponse"></param>
        ///// <param name="msg"></param>
        ///// <param name="data"></param>
        ///// <param name="code"></param>
        //public virtual void SetError<T2>(T2 baseResponse, int code = CommonConstants.ErrorCode_ThirdAPI) where T2 : class
        //{
        //    var msg = JsonConvert.SerializeObject(baseResponse);
        //    base.SetError(msg, code);
        //}

        /// <summary>
        /// 设置失败
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="code"></param>
        public override void SetError(string msg, int code = CommonConstants.ErrorCode_ThirdAPI)
        {
            base.SetError(msg, code);
        }
    }
}
