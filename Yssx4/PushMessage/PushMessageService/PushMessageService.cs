﻿using Newtonsoft.Json;
using PushMessageService.Gt;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using Yssx.Framework.Utils;
using Yssx.S.Pocos;

namespace PushMessageService
{
    /// <summary>
    /// 消息推送服务
    /// </summary>
    public class PushMessageService : IPushMessageService, IPushUserService
    {

        GtPushService pushService = new GtPushService();
        /// <summary>
        /// 根据 cid 推送透传消息(暂不使用，还未完全实现)
        /// </summary>
        /// <param name="pushMessage"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> PushTransmissionByCId(UserTicket user, TransmissionBody transmissionBody)
        {
            var cid = "";
            var pushMessage = new PushTransmissionMessage
            {
                Id = IdWorker.NextId(),
                OwnerId = cid,
                MessageBody = transmissionBody
            };
            var result = await pushService.PushTransmissionByAlias(pushMessage);

            return result;
        }


        /// <summary>
        /// 根据别名推送穿透消息
        /// </summary>
        /// <param name="pushMessage"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> VerifyRoleActCodePushMessage(long userid)
        {
            var pushMessage = new PushTransmissionMessage
            {
                Id = IdWorker.NextId(),
                OwnerId = userid.ToString(),
                MessageBody = new TransmissionBody { UserId = userid, Content = "", Type = 1 }
            };

            var result = await pushService.PushTransmissionByAlias(pushMessage);

            return result;
        }

        /// <summary>
        /// 绑定别名
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BindUserAlias(string cid, string deviceid, UserTicket user)
        {
            var result = new ResponseContext<bool>();
            var alias = user.Id.ToString();//"a6257ecf48aec7915cf65462223f0b32", "13023121793" ，6dbb67ccd16e8fcf8932e1175b9e2fa3

            int date = DateTime.Now.Day;
            var pushDeviceInfo = await DbContext.FreeSql.GetRepository<YssxPushDeviceInfo>(e => e.IsDelete == CommonConstants.IsNotDelete)
               .Where(e => e.DeviceId == deviceid && e.PushCid == cid && e.UserId == user.Id).FirstAsync();
            if (pushDeviceInfo == null)//没有绑定
            {
                result = await pushService.BindUserAlias(cid, alias);
                if (result.Code == 200)
                    DbContext.FreeSql.GetRepository<YssxPushDeviceInfo>().Insert(new YssxPushDeviceInfo { Id = IdWorker.NextId(), TenantId = user.TenantId, DeviceId = deviceid, PushCid = cid, UserId = user.Id });
            }
            else//已经绑定
            {
                await DbContext.FreeSql.GetRepository<YssxPushDeviceInfo>().UpdateDiy.Set(c => new YssxPushDeviceInfo
                {
                    Enable = true,
                    UpdateTime = DateTime.Now,
                }).Where(a => a.Id == pushDeviceInfo.Id).ExecuteAffrowsAsync();

                result.SetSuccess(true);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UnBindUserAlias(string cid, string deviceid, UserTicket user)
        {
            var result = new ResponseContext<bool>();
            var alias = user.Id.ToString();

            var pushDeviceInfo = await DbContext.FreeSql.GetRepository<YssxPushDeviceInfo>(e => e.IsDelete == CommonConstants.IsNotDelete)
               .Where(e => e.DeviceId == deviceid && e.PushCid == cid && e.UserId == user.Id).FirstAsync();

            if (pushDeviceInfo != null && pushDeviceInfo.Enable)//用户的设备绑定了别名，去解绑
            {
                result = await pushService.UnBindUserAlias(cid, alias);
                if (result.Code == 200)
                {
                    await DbContext.FreeSql.GetRepository<YssxPushDeviceInfo>().UpdateDiy.Set(c => new YssxPushDeviceInfo
                    {
                        Enable = false,
                        UpdateTime = DateTime.Now,
                    }).Where(a => a.Id == pushDeviceInfo.Id).ExecuteAffrowsAsync();
                }
            }

            return result;
        }

        #region 根据课程发布课堂测验任务推送消息
        /// <summary>
        /// 根据课程发布课堂测验任务推送消息
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> PushCourseTestTaskMessage(long courseId)
        {
            var pushMessage = new PushTransmissionMessage
            {
                Id = IdWorker.NextId(),
                MessageBody = new TransmissionBody { Content = string.Format("{0}", courseId), Type = 2 }
            };

            var result = await pushService.PushTransmissionToAllByCourseTestTask(pushMessage);

            return result;
        }
        #endregion

    }
}
