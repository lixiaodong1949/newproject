﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushMessageService
{
    /// <summary>
    /// 穿透消息
    /// </summary>
    public class PushTransmissionMessage
    {
        public long Id { get; set; }
        /// <summary>
        /// alias 或者 cid
        /// </summary>
        public string OwnerId { get; set; }
        /// <summary>
        /// 消息体（消息模板）
        /// </summary>
        public TransmissionBody MessageBody { get; set; }
    }

    public class TransmissionBody
    {
        /// <summary>
        /// 消息类型：1.注册激活 2:课堂测验群推
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 消息内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 扩展字段
        /// </summary>
        public long UserId { get; set; }

    }


}
