﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Utils;

namespace PushMessageService.Gt
{
    /// <summary>
    /// 个推：http://docs.getui.com/getui/server/rest_v2/introduction/
    /// </summary>
    public class GtPushService
    {
        //private ILogger logger;
        HttpClientHelper httpClient = new HttpClientHelper();
        //public GtPushService()
        //{
        //    //logger = _logger;
        //}

        /// <summary>
        /// 根据 cid 推送穿透消息
        /// </summary>
        /// <param name="pushMessage"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> PushTransmissionByCId(PushTransmissionMessage pushMessage)
        {
            var result = new PushResponseContext<bool>();
            try
            {

                var url = PushConst.BaseUrl + "/push/single/cid";
                var message = JsonConvert.SerializeObject(pushMessage.MessageBody);
                var data = new
                {
                    request_id = pushMessage.Id,
                    settings = new { ttl = PushConst.TTL },
                    audience = new { cid = new string[] { pushMessage.OwnerId }, },
                    push_message = new { transmission = message }
                };

                var token = await GetToken();
                var headers = new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>("token", token) };

                var response = await httpClient.PostAsync<GtBaseResponse>(url, data, headers);

                if (response.code == 0)
                    result.SetSuccess(true);
                else
                    result.SetError(response.ToJson());

            }
            catch (Exception ex)
            {
                result.SetError($"pushMessage:{pushMessage.ToJson()},ex:{ex}");
            }

            return result;
        }

        /// <summary>
        /// 根据别名推送穿透消息
        /// </summary>
        /// <param name="pushMessage"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> PushTransmissionByAlias(PushTransmissionMessage pushMessage)
        {
            var result = new PushResponseContext<bool>();
            try
            {
                var url = PushConst.BaseUrl + "/push/single/alias";

                var message = JsonConvert.SerializeObject(pushMessage.MessageBody);
                var data = new
                {
                    request_id = pushMessage.Id,
                    settings = new { ttl = PushConst.TTL },
                    audience = new { alias = new string[] { pushMessage.OwnerId }, },
                    push_message = new { transmission = message }
                };

                var token = await GetToken();
                var headers = new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>("token", token) };

                var response = await httpClient.PostAsync<GtBaseResponse>(url, data, headers);

                if (response.code == 0)
                    result.SetSuccess(true);
                else
                    result.SetError(response.ToJson());
            }
            catch (Exception ex)
            {
                result.SetError($"pushMessage:{pushMessage.ToJson()},ex:{ex}");
            }
            return result;
        }

        /// <summary>
        /// 绑定别名
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BindUserAlias(string cid, string alias)
        {
            var result = new PushResponseContext<bool>();
            try
            {
                var url = PushConst.BaseUrl + "/user/alias";

                var data = new
                {
                    data_list = new[] { new { cid = cid, alias = alias } },
                };

                var token = await GetToken();
                var headers = new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>("token", token) };

                var response = await httpClient.PostAsync<GtBaseResponse>(url, data, headers);

                if (response.code == 0)
                    result.SetSuccess(true);
                else
                    result.SetError(response.ToJson());
                //var datastr = JsonConvert.SerializeObject(data);
                //var response = await HttpWebRequestHelper.PostAsync(url, datastr, heards: headers);

                //if (response.StatusCode == HttpStatusCode.OK)
                //    result.SetSuccess(true);
                //else
                //    result.SetError(response.ToJson());

            }
            catch (Exception ex)
            {
                result.SetError($"cid:{cid},alias:{alias},ex:{ex}");
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UnBindUserAlias(string cid, string alias)
        {
            var result = new PushResponseContext<bool>();
            try
            {
                var url = PushConst.BaseUrl + "/user/alias";

                var data = new
                {
                    data_list = new[] { new { cid = cid, alias = alias } },
                };

                var token = await GetToken();

                var headers = new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>("token", token) };
                var datastr = JsonConvert.SerializeObject(data);
                var response = await HttpWebRequestHelper.DeleteAsync(url, datastr, "application/json;charset=utf-8", headers);

                if (response.StatusCode == HttpStatusCode.OK)
                    result.SetSuccess(true);
                else
                    result.SetError(response.ToJson());
            }
            catch (Exception ex)
            {
                result.SetError($"cid:{cid},alias:{alias},ex:{ex}");
            }
            return result;
        }
        /// <summary>
        /// 获取token
        /// </summary>
        /// <returns></returns>
        private async Task<string> GetToken()
        {
            var url = PushConst.BaseUrl + "/auth";
            var timestamp = DateTime.Now.ToTimeStamp();
            var data = new
            {
                sign = (PushConst.APPKEY + timestamp + PushConst.MASTERSECRET).SHA256(),//签名，加密算法: SHA256，格式: sha256(appkey+timestamp+mastersecret)
                timestamp = timestamp,
                appkey = PushConst.APPKEY,
            };

            var result = await httpClient.PostAsync<GtBaseResponse<GtTokenResponse>>(url, data);

            if (result.code != 0)
                return null;
            else
            {
                //var date = result.data.expire_time.ToDateTime();

                return result.data.token;

            }
        }

        #region 群推课程课堂测验任务
        /// <summary>
        /// 群推课程课堂测验任务
        /// </summary>
        /// <param name="pushMessage"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> PushTransmissionToAllByCourseTestTask(PushTransmissionMessage pushMessage)
        {
            var result = new PushResponseContext<bool>();
            try
            {
                var url = PushConst.BaseUrl + "/push/all";

                var message = JsonConvert.SerializeObject(pushMessage.MessageBody);
                var data = new
                {
                    request_id = pushMessage.Id,
                    group_name = "课堂测验任务",
                    settings = new { ttl = PushConst.TTL },
                    audience = "all",
                    push_message = new
                    {
                        notification = new
                        {
                            title = "课堂测验任务推送",
                            body = message,
                            click_type = "url",
                            url = "https//:xxx",
                        }
                    }
                };

                var token = await GetToken();
                var headers = new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>("token", token) };

                var response = await httpClient.PostAsync<GtBaseResponse>(url, data, headers);

                if (response.code == 0)
                    result.SetSuccess(true);
                else
                    result.SetError(response.ToJson());
            }
            catch (Exception ex)
            {
                result.SetError($"pushMessage:{pushMessage.ToJson()},ex:{ex}");
            }
            return result;
        }
        #endregion

    }
}
