﻿namespace PushMessageService
{
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Net.Security;
    using System.Runtime.CompilerServices;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Threading.Tasks;

    internal static class HttpAsyncClientExtension
    {
        internal static async Task<T> ReadAsAsync<T>(this HttpContent Content)
        {
            var resp = await Content.ReadAsStringAsync();
            return await Task.Run(() =>
            {
                return JsonConvert.DeserializeObject<T>(resp);
            });
        }
        //internal static Task<HttpResponseMessage> PostAsJsonAsync(this HttpClient Client, string requestUri, object data)
        //{
        //    var jsondata = JsonConvert.SerializeObject(data);
        //    HttpContent content = new StringContent(jsondata);
        //    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //    //由HttpClient发出异步Post请求
        //    return Client.PostAsync(requestUri, content);
        //}

        /// <summary>
        /// 取时间戳，高并发情况下会有重复。想要解决这问题请使用sleep线程睡眠1毫秒。
        /// </summary>
        /// <param name="AccurateToMilliseconds">精确到毫秒</param>
        /// <returns>返回一个长整数时间戳</returns>
        public static long ToTimeStamp(this DateTime dateTime, bool AccurateToMilliseconds = true)
        {
            if (AccurateToMilliseconds)
            {
                // 使用当前时间计时周期数（636662920472315179）减去1970年01月01日计时周期数（621355968000000000）除去（删掉）后面4位计数（后四位计时单位小于毫秒，快到不要不要）再取整（去小数点）。
                //备注：DateTime.Now.ToUniversalTime不能缩写成DateTime.Now.Ticks，会有好几个小时的误差。
                //621355968000000000计算方法 long ticks = (new DateTime(1970, 1, 1, 8, 0, 0)).ToUniversalTime().Ticks;
                return (dateTime.ToUniversalTime().Ticks - 621355968000000000) / 10000;

            }
            else
            {
                //上面是精确到毫秒，需要在最后除去（10000），这里只精确到秒，只要在10000后面加三个0即可（1秒等于1000毫米）。
                return (dateTime.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
            }
        }

        /// <summary>
        /// 时间戳反转为时间，有很多中翻转方法，但是，请不要使用过字符串（string）进行操作，大家都知道字符串会很慢！
        /// </summary>
        /// <param name="TimeStamp">时间戳</param>
        /// <param name="AccurateToMilliseconds">是否精确到毫秒</param>
        /// <returns>返回一个日期时间</returns>
        public static DateTime ToDateTime(this long TimeStamp, bool AccurateToMilliseconds = true)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
            if (AccurateToMilliseconds)
            {
                return startTime.AddTicks(TimeStamp * 10000);
            }
            else
            {
                return startTime.AddTicks(TimeStamp * 10000000);
            }
        }
        /// <summary>
        /// 对象序列化成json字符串
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string ToJson(this object o)
        {
            if (o == null)
                return null;
            return JsonConvert.SerializeObject(o);
        }
        /// <summary>
        /// json字符串反序列化成对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static T ToObject<T>(this string str) where T : class
        {
            if (string.IsNullOrWhiteSpace(str))
                return default(T);
            return JsonConvert.DeserializeObject<T>(str);
        }
    }

    internal class HttpClientHelper
    {
        private static readonly object LockObj = new object();
        private static HttpClient client = null;
        public HttpClientHelper()
        {
            GetInstance();
        }
        private static HttpClient GetInstance()
        {

            if (client == null)
            {
                lock (LockObj)
                {
                    if (client == null)
                    {
                        client = new HttpClient();
                    }
                }
            }
            return client;
        }
        /// <summary>
        /// post异步请求方法
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public async Task<string> PostAsync(string url, string data, List<KeyValuePair<string, string>> headers = null)//post异步请求方法
        {
            try
            {
                client.DefaultRequestHeaders.Accept.Clear();
                BuildHeaders(client.DefaultRequestHeaders, headers);
                HttpContent content = new StringContent(data);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //由HttpClient发出异步Post请求
                HttpResponseMessage res = await client.PostAsync(url, content);
                if (res.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string str = await res.Content.ReadAsStringAsync();
                    return str;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /// <summary>
        /// post异步请求方法
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public async Task<T> PostAsync<T>(string url, object data, List<KeyValuePair<string, string>> headers = null)
        {
            try
            {
                client.DefaultRequestHeaders.Accept.Clear();
                BuildHeaders(client.DefaultRequestHeaders, headers);

                var jsondata = JsonConvert.SerializeObject(data);

                HttpContent content = new StringContent(jsondata);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //由HttpClient发出异步Post请求

                var response = await client.PostAsync(url, content);

                if (response.IsSuccessStatusCode)//请求成功
                {
                    return await response.Content.ReadAsAsync<T>();
                }
                else//请求失败，抛出请求失败异常
                {
                    await VerifyStatus(response);
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public string Post(string url, string strJson)//post同步请求方法
        //{
        //    try
        //    {
        //        HttpContent content = new StringContent(strJson);
        //        content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
        //        //client.DefaultRequestHeaders.Connection.Add("keep-alive");
        //        //由HttpClient发出Post请求
        //        Task<HttpResponseMessage> res = client.PostAsync(url, content);
        //        if (res.Result.StatusCode == System.Net.HttpStatusCode.OK)
        //        {
        //            string str = res.Result.Content.ReadAsStringAsync().Result;
        //            return str;
        //        }
        //        else
        //            return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        //public string Get(string url)
        //{
        //    try
        //    {
        //        var responseString = client.GetStringAsync(url);
        //        return responseString.Result;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}
        public async Task<T> DeleteAsync<T>(string url, List<KeyValuePair<string, string>> headers = null)
        {
            try
            {
                client.DefaultRequestHeaders.Accept.Clear();

                BuildHeaders(client.DefaultRequestHeaders, headers);

                //由HttpClient发出异步Post请求
                var response = await client.DeleteAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<T>();
                }
                else
                {
                    await VerifyStatus(response);
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<T> GetAsync<T>(string baseAddress, string requestUri, List<KeyValuePair<string, string>> headers = null)
        {
            client.DefaultRequestHeaders.Accept.Clear();
            BuildHeaders(client.DefaultRequestHeaders, headers);
            var response = await client.GetAsync(requestUri);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<T>();
            }
            else
            {
                await VerifyStatus(response);
            }
            return default(T);

        }

        public async Task<string> GetAsync(string url, List<KeyValuePair<string, string>> headers = null)
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.AcceptCharset.Add(new StringWithQualityHeaderValue("utf-8"));

            BuildHeaders(client.DefaultRequestHeaders, headers);

            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                await VerifyStatus(response);
            }
            return null;

        }

        public async Task<T> PutAsync<T>(string url, object data, List<KeyValuePair<string, string>> headers = null)
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            BuildHeaders(client.DefaultRequestHeaders, headers);

            var jsondata = JsonConvert.SerializeObject(data);
            HttpContent content = new StringContent(jsondata);

            //由HttpClient发出异步Post请求
            var response = await client.PutAsync(url, content);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<T>();
            }
            else
            {
                await VerifyStatus(response);
            }
            return default(T);
        }

        private void BuildHeaders(HttpRequestHeaders requestHeaders, List<KeyValuePair<string, string>> headers = null)
        {
            if (headers == null) return;

            try
            {
                foreach (var item in headers)
                {
                    if (requestHeaders.Contains(item.Key))
                    {
                        requestHeaders.Remove(item.Key);
                    }
                    requestHeaders.Add(item.Key, item.Value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task VerifyStatus(HttpResponseMessage response)
        {
            string errormsg = null;
            try
            {
                errormsg = await response.Content.ReadAsStringAsync();
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                throw new Exception(errormsg, e);
                //var ex = new HttpRequestException(response.RequestMessage.ToString(), e);
                //if (IsThrowException)
                //{
                //    throw ex;
                //}
                //else
                //{
                //    logger.LogError(e.ToString());
                //}
            }
        }

    }

    public class HttpWebRequestHelper
    {
        #region 同步方法
        /// <summary>
        /// 以POST方式提交数据给指定的url，返回以UTF8编码的响应文本（出错时返回字符串“ERROR”）
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="data">数据</param>
        /// <param name="contentType">请求的ContentType</param>
        /// <param name="heards">自定义头内容</param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="cert">证书</param>
        /// <param name="useGZIP">是否gzip压缩</param>
        /// <returns>
        /// 请求返回结果<see cref="T:WOne.Common.Model.HttpResult"/>
        /// </returns>
        public static HttpResult Post(string url, string data, string contentType = "application/json;charset=utf-8", List<KeyValuePair<string, string>> heards = null, int timeout = 30000, X509Certificate2 cert = null, bool useGZIP = false)
        {
            return Request("Post", url, contentType, data, heards, timeout, cert, useGZIP);
        }

        /// <summary>
        /// 文件请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postParaList">请求参数</param>
        /// <returns></returns>
        public static HttpResult Post(string url, List<PostDateClass> postParaList)
        {
            HttpResult httpResult = new HttpResult();
            try
            {
                string responseContent = "";
                var memStream = new MemoryStream();
                var webRequest = (HttpWebRequest)WebRequest.Create(url);
                // 边界符
                var boundary = "---------------" + DateTime.Now.Ticks.ToString("x");
                // 边界符
                var beginBoundary = Encoding.ASCII.GetBytes("--" + boundary + "\r\n");
                // 最后的结束符
                var endBoundary = Encoding.ASCII.GetBytes("--" + boundary + "--\r\n");
                memStream.Write(beginBoundary, 0, beginBoundary.Length);
                // 设置属性
                webRequest.Method = "POST";
                webRequest.Timeout = 10000;
                webRequest.ContentType = "multipart/form-data; boundary=" + boundary;
                for (int i = 0; i < postParaList.Count; i++)
                {
                    PostDateClass temp = postParaList[i];
                    if (temp.Type == 1)
                    {
                        var fileStream = new FileStream(temp.Value, FileMode.Open, FileAccess.Read);
                        // 写入文件
                        const string filePartHeader = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n" + "Content-Type: application/octet-stream\r\n\r\n";
                        var header = string.Format(filePartHeader, temp.Prop, temp.Value);
                        var headerbytes = Encoding.UTF8.GetBytes(header);
                        memStream.Write(headerbytes, 0, headerbytes.Length);
                        var buffer = new byte[1024];
                        int bytesRead; // =0
                        while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                        string end = "\r\n";
                        headerbytes = Encoding.UTF8.GetBytes(end);
                        memStream.Write(headerbytes, 0, headerbytes.Length);
                        fileStream.Close();
                    }
                    else if (temp.Type == 0)
                    {
                        // 写入字符串的Key
                        var stringKeyHeader = "Content-Disposition: form-data; name=\"{0}\"" + "\r\n\r\n{1}\r\n";
                        var header = string.Format(stringKeyHeader, temp.Prop, temp.Value);
                        var headerbytes = Encoding.UTF8.GetBytes(header);
                        memStream.Write(headerbytes, 0, headerbytes.Length);
                    }
                    if (i != postParaList.Count - 1)
                        memStream.Write(beginBoundary, 0, beginBoundary.Length);
                    else
                        // 写入最后的结束边界符
                        memStream.Write(endBoundary, 0, endBoundary.Length);
                }
                webRequest.ContentLength = memStream.Length;
                var requestStream = webRequest.GetRequestStream();
                memStream.Position = 0;
                var tempBuffer = new byte[memStream.Length];
                memStream.Read(tempBuffer, 0, tempBuffer.Length);
                memStream.Close();
                requestStream.Write(tempBuffer, 0, tempBuffer.Length);
                requestStream.Close();
                using (HttpWebResponse res = (HttpWebResponse)webRequest.GetResponse())
                {

                    using (Stream resStream = res.GetResponseStream())
                    {
                        byte[] buffer = new byte[1024];
                        int read;
                        while ((read = resStream.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            responseContent += Encoding.UTF8.GetString(buffer, 0, read);
                        }
                        httpResult.StatusCode = res.StatusCode;
                        httpResult.StatusDescription = res.StatusDescription;
                        httpResult.Html = responseContent;
                    }
                    res.Close();
                }
            }
            catch (WebException ex)
            {
                httpResult.StatusCode = ((HttpWebResponse)ex.Response).StatusCode;
                httpResult.StatusDescription = ex.Message;
                httpResult.Html = ex.ToString();
            }
            catch (Exception ex)
            {
                httpResult.StatusDescription = ex.Message;
                httpResult.Html = ex.ToString();
            }
            return httpResult;
        }

        /// <summary>
        /// 以GET方式请求指定的url，返回以UTF8编码的响应文本
        /// 
        /// </summary>
        /// <param name="url">请求地址</param><param name="heards">自定义头内容</param>
        /// <param name="timeout"></param>
        /// <param name="cert"></param>
        /// <param name="useGZIP"></param>
        /// <returns>
        /// HTTP请求返回结果<see cref="T:WOne.Common.Model.HttpResult"/>
        /// </returns>
        public static HttpResult Get(string url, List<KeyValuePair<string, string>> heards = null, int timeout = 30000, X509Certificate2 cert = null, bool useGZIP = false)
        {
            return Request("GET", url, "application/x-www-form-urlencoded", "", heards, timeout, cert, useGZIP);
        }

        /// <summary>
        /// 以POST方式提交数据给指定的url，返回以UTF8编码的响应文本
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="data">数据</param>
        /// <param name="contentType">请求的ContentType</param>
        /// <param name="heards">自定义头内容</param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="cert"></param>
        /// <param name="useGZIP"></param>
        /// <returns>
        /// 请求返回结果<see cref="T:WOne.Common.Model.HttpResult"/>
        /// </returns>
        public static HttpResult Delete(string url, string data, string contentType, List<KeyValuePair<string, string>> heards = null, int timeout = 30000, X509Certificate2 cert = null, bool useGZIP = false)
        {
            return Request("Delete", url, contentType, data, heards, timeout, cert, useGZIP);
        }
        #endregion

        #region 异步方法
        /// <summary>
        /// 以POST方式提交数据给指定的url，返回以UTF8编码的响应文本（出错时返回字符串“ERROR”）
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="data">数据</param>
        /// <param name="contentType">请求的ContentType</param>
        /// <param name="heards">自定义头内容</param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="cert">证书</param>
        /// <param name="useGZIP">是否gzip压缩</param>
        /// <returns>
        /// 请求返回结果<see cref="T:WOne.Common.Model.HttpResult"/>
        /// </returns>
        public static Task<HttpResult> PostAsync(string url, string data, string contentType = "application/json;charset=utf-8", List<KeyValuePair<string, string>> heards = null, int timeout = 30000, X509Certificate2 cert = null, bool useGZIP = false)
        {
            return RequestAsync("Post", url, contentType, data, heards, timeout, cert, useGZIP);
        }

        /// <summary>
        /// 以GET方式请求指定的url，返回以UTF8编码的响应文本
        /// 
        /// </summary>
        /// <param name="url">请求地址</param><param name="heards">自定义头内容</param>
        /// <param name="timeout"></param>
        /// <param name="cert"></param>
        /// <param name="useGZIP"></param>
        /// <returns>
        /// HTTP请求返回结果<see cref="T:WOne.Common.Model.HttpResult"/>
        /// </returns>
        public static Task<HttpResult> GetAsync(string url, List<KeyValuePair<string, string>> heards = null, int timeout = 30000, X509Certificate2 cert = null, bool useGZIP = false)
        {
            return RequestAsync("GET", url, "application/x-www-form-urlencoded", "", heards, timeout, cert, useGZIP);
        }

        /// <summary>
        /// 以POST方式提交数据给指定的url，返回以UTF8编码的响应文本
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="data">数据</param>
        /// <param name="contentType">请求的ContentType</param>
        /// <param name="heards">自定义头内容</param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="cert"></param>
        /// <param name="useGZIP"></param>
        /// <returns>
        /// 请求返回结果<see cref="T:WOne.Common.Model.HttpResult"/>
        /// </returns>
        public static Task<HttpResult> DeleteAsync(string url, string data, string contentType, List<KeyValuePair<string, string>> heards = null, int timeout = 30000, X509Certificate2 cert = null, bool useGZIP = false)
        {
            return RequestAsync("Delete", url, contentType, data, heards, timeout, cert, useGZIP);
        }
        #endregion

        #region 私有方法
        /// <summary>
        /// 以POST方式提交数据给指定的url，返回以UTF8编码的响应文本
        /// </summary>
        /// <param name="method">请求方法</param>
        /// <param name="url">请求地址</param>
        /// <param name="data">参数</param>
        /// <param name="contentType">请求的ContentType</param>
        /// <param name="heards">自定义头内容</param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="cert"></param>
        /// <param name="useGZIP"></param>
        /// <returns>
        /// 请求返回结果<see cref="T:WOne.Common.Model.HttpResult"/>
        /// </returns>
        private static HttpResult Request(string method, string url, string contentType, string data = "", List<KeyValuePair<string, string>> heards = null, int timeout = 30000, X509Certificate2 cert = null, bool useGZIP = false)
        {
            HttpResult httpResult = new HttpResult();
            HttpWebRequest httpWebRequest = (HttpWebRequest)null;
            HttpWebResponse httpWebResponse = null;

            try
            {
                if (url.StartsWith("https", StringComparison.OrdinalIgnoreCase))
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);

                httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                if (useGZIP)
                {
                    httpWebRequest.Headers["Accept-Encoding"] = "gzip,deflate";

                    httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip;
                }
                httpWebRequest.Method = method;
                httpWebRequest.ContentType = contentType;
                httpWebRequest.Timeout = timeout;

                byte[] bytes = Encoding.UTF8.GetBytes(data);
                httpWebRequest.ContentLength = (long)bytes.Length;
                if (heards != null)
                {
                    foreach (KeyValuePair<string, string> keyValuePair in heards)
                        httpWebRequest.Headers.Add(keyValuePair.Key, keyValuePair.Value);
                }
                if (cert != null)
                {
                    httpWebRequest.ClientCertificates.Add(cert);
                }

                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                    requestStream.Close();
                }

                httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                if (useGZIP)
                {
                    using (Stream responseStream = new GZipStream(httpWebResponse.GetResponseStream(), CompressionMode.Decompress))
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            httpResult.StatusCode = httpWebResponse.StatusCode;
                            httpResult.StatusDescription = httpWebResponse.StatusDescription;
                            httpResult.Html = streamReader.ReadToEnd();
                            streamReader.Close();
                        }
                        responseStream.Close();
                    }
                }
                else
                {
                    using (Stream responseStream = httpWebResponse.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            httpResult.StatusCode = httpWebResponse.StatusCode;
                            httpResult.StatusDescription = httpWebResponse.StatusDescription;
                            httpResult.Html = streamReader.ReadToEnd();
                            streamReader.Close();
                        }
                        responseStream.Close();
                    }
                }

                httpWebRequest.Abort();
                httpWebResponse.Close();
            }
            catch (WebException ex)
            {
                httpWebResponse = (HttpWebResponse)ex.Response;
                if (httpWebResponse != null) httpResult.StatusCode = httpWebResponse.StatusCode;
                httpResult.StatusDescription = ex.Message;
                httpResult.Html = ex.ToString();
            }
            catch (Exception ex)
            {
                httpResult.StatusDescription = ex.Message;
                httpResult.Html = ex.ToString();
            }

            return httpResult;
        }
        /// <summary>
        /// 以POST方式提交数据给指定的url，返回以UTF8编码的响应文本
        /// </summary>
        /// <param name="method">请求方法</param>
        /// <param name="url">请求地址</param>
        /// <param name="data">参数</param>
        /// <param name="contentType">请求的ContentType</param>
        /// <param name="heards">自定义头内容</param>
        /// <param name="timeout">超时时间（毫秒）</param>
        /// <param name="cert"></param>
        /// <param name="useGZIP"></param>
        /// <returns>
        /// 请求返回结果<see cref="T:WOne.Common.Model.HttpResult"/>
        /// </returns>
        private static async Task<HttpResult> RequestAsync(string method, string url, string contentType, string data = "", List<KeyValuePair<string, string>> heards = null, int timeout = 30000, X509Certificate2 cert = null, bool useGZIP = false)
        {
            HttpResult httpResult = new HttpResult();
            HttpWebRequest httpWebRequest = (HttpWebRequest)null;
            HttpWebResponse httpWebResponse = null;

            try
            {
                #region 构建请求参数
                if (url.StartsWith("https", StringComparison.OrdinalIgnoreCase))
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);

                httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                if (useGZIP)
                {
                    httpWebRequest.Headers["Accept-Encoding"] = "gzip,deflate";

                    httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip;
                }
                httpWebRequest.Method = method;
                httpWebRequest.ContentType = contentType;
                httpWebRequest.Timeout = timeout;

                byte[] bytes = Encoding.UTF8.GetBytes(data);
                httpWebRequest.ContentLength = (long)bytes.Length;
                if (heards != null)
                {
                    foreach (KeyValuePair<string, string> keyValuePair in heards)
                        httpWebRequest.Headers.Add(keyValuePair.Key, keyValuePair.Value);
                }
                if (cert != null)
                {
                    httpWebRequest.ClientCertificates.Add(cert);
                }
                #endregion

                using (Stream requestStream = httpWebRequest.GetRequestStream())
                {
                    await requestStream.WriteAsync(bytes, 0, bytes.Length);
                    requestStream.Close();
                }

                httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                if (useGZIP)
                {
                    using (Stream responseStream = new GZipStream(httpWebResponse.GetResponseStream(), CompressionMode.Decompress))
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            httpResult.StatusCode = httpWebResponse.StatusCode;
                            httpResult.StatusDescription = httpWebResponse.StatusDescription;
                            httpResult.Html = await streamReader.ReadToEndAsync();
                            streamReader.Close();
                        }
                        responseStream.Close();
                    }
                }
                else
                {
                    using (Stream responseStream = httpWebResponse.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            httpResult.StatusCode = httpWebResponse.StatusCode;
                            httpResult.StatusDescription = httpWebResponse.StatusDescription;
                            httpResult.Html = await streamReader.ReadToEndAsync();
                            streamReader.Close();
                        }
                        responseStream.Close();
                    }
                }

                httpWebRequest.Abort();
                httpWebResponse.Close();
            }
            catch (WebException ex)
            {
                httpWebResponse = (HttpWebResponse)ex.Response;
                if (httpWebResponse != null) httpResult.StatusCode = httpWebResponse.StatusCode;
                httpResult.StatusDescription = ex.Message;
                httpResult.Html = ex.ToString();
            }
            catch (Exception ex)
            {
                httpResult.StatusDescription = ex.Message;
                httpResult.Html = ex.ToString();
            }

            return httpResult;
        }

        private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        #endregion
    }

    public class HttpResult
    {
        public HttpStatusCode StatusCode { get; internal set; }
        public string StatusDescription { get; internal set; }
        public string Data { get; internal set; }
        /// <summary>
        /// 请求接口放回的原始内容
        /// </summary>
        public string Html { get; internal set; }
    }
    public class PostDateClass
    {
        public string Prop { get; internal set; }
        public string Value { get; internal set; }
        public int Type { get; internal set; }
    }
}
