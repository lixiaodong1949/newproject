﻿using ECommon.Components;
using Sx.Course.Domain.Models.Course;
using Sx.Course.Entitys;
using System;
using System.Collections.Generic;
using Yssx.Common;
using System.Linq;
using Tas.Common.IdGenerate;
using Sx.Course.Domain.Repositories;

namespace Sx.Course.Domain.Dapper
{
    [Component]
    public class CourseSubjectRepository : ICourseSubjectRepository
    {
        public void AddCourseSubject(long cid, List<CourseSubjectInfo> list)
        {
            if (list.Count > 0)
            {
                List<YssxCourseSubject> datas = list.Select(x => new YssxCourseSubject
                {
                    Id = IdWorker.NextId(),
                    CourseId = cid,
                    Code1 = x.Code1,
                    Direction = x.Direction,
                    SubjectName = x.SubjectName,
                    SubjectCode = x.SubjectCode,
                    SubjectType = x.SubjectType,
                    ParentId = 0,
                    CreateTime = DateTime.Now
                }).ToList();
                DbContext.FreeSql.GetRepository<YssxCourseSubject>().Insert(datas);
            }
        }

        public bool ExistSubjectById(long cid)
        {
            return DbContext.FreeSql.GetRepository<YssxCourseSubject>().Where(x => x.CourseId == cid).Any();
        }

        public List<CourseSubjectInfo> GetSubjectTempList()
        {
            List<YssxCourseSubjectTemp> list = DbContext.FreeSql.GetRepository<YssxCourseSubjectTemp>().Where(x => x.Id > 0).ToList();

            return list.Select(x => new CourseSubjectInfo
            {
                Code1 = x.Code1,
                Direction = x.Direction,
                SubjectName = x.SubjectName,
                SubjectCode = x.SubjectCode,
                SubjectType = x.SubjectType,
            }).ToList();
        }
    }
}
