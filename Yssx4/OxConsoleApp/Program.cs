﻿using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Tas.Common.Configurations;
using Tas.Common.IdGenerate;
using Yssx.Framework.Dal;
using Yssx.S.Pocos;

namespace OxConsoleApp
{
    public class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            ConnectionInit();
            InsertData();
            //DbContext.FreeSql.GetRepository<OneXPreviouExamUser>()

        }

        static void ConnectionInit()
        {
            string sqlConncetion = "Data Source=139.9.202.108;Port=3306;User ID=root;Password=wYRvnH@6Ryfsn4lh;Initial Catalog=oneandx;Charset=utf8;SslMode=none;Max pool size=150;";
            string sqlConncetion2 = "Data Source=139.9.202.108;Port=3306;User ID=root;Password=wYRvnH@6Ryfsn4lh;Initial Catalog=trainingcloud;Charset=utf8;SslMode=none;Max pool size=150;";
            string sqlConncetion3 = "Data Source=139.159.192.49;Port=3306;User ID=yssx-dev;Password=Wah%#*903;Initial Catalog=yssx;Charset=utf8;SslMode=none;Max pool size=150;";

            ProjectConfiguration.GetInstance().UseFreeSqlRepository2(sqlConncetion, sqlConncetion2);
            ProjectConfiguration.GetInstance().UseFreeSqlJNRepository(sqlConncetion3);
        }

        static async void InsertData()
        {
            List<UserDto> dtos = new List<UserDto>();
            List<UserDto> dtos_jn = new List<UserDto>();
            List<UserDto> dtos_sx = new List<UserDto>();
            List<int> years = new List<int> { 2020 };
            string sql_sx = "SELECT b.Name schoolName,a.Id UserId,a.realName FROM yssx_user a, yssx_school b WHERE b.id not in (981239287939076,1002924952322128,1017556916879364,1017963646926889,1018092328173620,1019280413261827,1019796471611460,1020223774163030,1023227117191852,1023229411476205,1023229637968685,1023230540775742, 1078977911537665,1020220590686287,1020221698539627,1021530243793075,1022443713740801,1022454397304833)  AND a.TenantId = b.Id ORDER BY a.createtime";
            string sql = "SELECT SchoolName,UserName as RealName FROM onex_user_info";
            string sql_jn = "SELECT a.RealName,a.TenantId,s.`Name` schoolName FROM yssx_user a inner JOIN yssx_school s on a.TenantId=s.Id  WHERE a.IsDelete=0 and a.TenantId in (945406696270088,905344759634448,902315299291224,905232003783021,905293807226284,905312211467050,905318175767725,905730469866915,906228117948560,907824950802515,909203623993827,910883024715970,911416934453369,911418368905587,912232322660564,912546366996737,914364804890649,915229025255480,915610421059496,917743679457429,918608932308838,915610421059496,906165384334022,930892869773714,935125707018370,945408936029107,945548827331129,946241211769191,946369947549951,946381683214435,947730568315688,906165384334022,948526402642747,948864695585986,948890004559265,962846057020842,948864695585986,986529131448004,1018909349409514,1021433190951058,1023612992246646)";

            dtos = await DbContext.FreeSql.Ado.QueryAsync<UserDto>(sql);
            dtos_jn = await DbContext.JNFreeSql.Ado.QueryAsync<UserDto>(sql_jn);
            dtos_sx = await DbContext.FreeSql2.Ado.QueryAsync<UserDto>(sql_sx);
 

            dtos.AddRange(dtos_jn);
            dtos.AddRange(dtos_sx);


            dtos = dtos.Where(x => !string.IsNullOrWhiteSpace(x.RealName) && x.RealName.Length <= 3).ToList();

            List<CertificateModel> certificates = InitData();

            List<OneXPreviouExamUser> instList = new List<OneXPreviouExamUser>();
            long count = 0;
            foreach (var item in years)
            {
                List<CertificateModel> temps = certificates.Where(x => x.Year == item).ToList();

                foreach (var item2 in temps)
                {
                    for (int i = 0; i < item2.CertificateNum; i++)
                    {
                        int num = new Random().Next(1, dtos.Count);
                        int day = new Random().Next(1, 30);
                        if (item2.CertificateDate.Contains("-2"))
                        {
                            day = new Random().Next(1, 28);
                        }

                        UserDto user = dtos[num];
                        DateTime date = DateTime.Parse($"{item2.CertificateDate}-{day}");
                        OneXPreviouExamUser xuser = new OneXPreviouExamUser
                        {
                            Id = IdWorker.NextId(),
                            ExamId = item2.ExamId,
                            IsIssueCertificate = true,
                            SchoolName = user.SchoolName,
                            UserName = user.RealName,
                            CertificateDate = date,
                            CertificateNo = RandomNo(date),
                        };
                        instList.Add(xuser);
                    }

                    await DbContext.FreeSql.GetRepository<OneXPreviouExamUser>().InsertAsync(instList);
                    count += instList.Count();
                    instList = new List<OneXPreviouExamUser>();
                    Thread.Sleep(100);

                }
            }

            Console.WriteLine("总人数 :{0}", count);
        }

        static string RandomNo(DateTime dtNow)
        {
            //生成证书编号CreateTime
            var dtNowStr = dtNow.ToString("yyyyMMdd");
            Random r = new Random();
            var rFourNumber = r.Next(1000, 10000).ToString();
            string sn = "SX" + dtNowStr + rFourNumber;
            return sn;
        }

        static List<CertificateModel> InitData()
        {
            List<CertificateModel> models = new List<CertificateModel>
            {

                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=658,CertificateDate="2015-1",Year=2015},
                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=607,CertificateDate="2015-2",Year=2015},
                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=988,CertificateDate="2015-3",Year=2015},
                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=996,CertificateDate="2015-4",Year=2015},
                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=1055,CertificateDate="2015-5",Year=2015},
                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=1159,CertificateDate="2015-6",Year=2015},
                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=1098,CertificateDate="2015-7",Year=2015},
                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=1076,CertificateDate="2015-8",Year=2015},
                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=1011,CertificateDate="2015-9",Year=2015},
                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=1136,CertificateDate="2015-10",Year=2015},
                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=1357,CertificateDate="2015-11",Year=2015},
                //new CertificateModel{ExamId=1083781789319174,Name="财务会计核算应用能力",CertificateNum=1259,CertificateDate="2015-12",Year=2015},

                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=300,CertificateDate="2015-1",Year=2015},
                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=350,CertificateDate="2015-2",Year=2015},
                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=330,CertificateDate="2015-3",Year=2015},
                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=332,CertificateDate="2015-4",Year=2015},
                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=355,CertificateDate="2015-5",Year=2015},
                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=453,CertificateDate="2015-6",Year=2015},
                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=467,CertificateDate="2015-7",Year=2015},
                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=537,CertificateDate="2015-8",Year=2015},
                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=294,CertificateDate="2015-9",Year=2015},
                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=310,CertificateDate="2015-10",Year=2015},
                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=320,CertificateDate="2015-11",Year=2015},
                //new CertificateModel{ExamId=1083782449348616,Name="投融资分析技术证",CertificateNum=252,CertificateDate="2015-12",Year=2015},

                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=400,CertificateDate="2015-1",Year=2015},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=200,CertificateDate="2015-2",Year=2015},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=600,CertificateDate="2015-3",Year=2015},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=500,CertificateDate="2015-4",Year=2015},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=700,CertificateDate="2015-5",Year=2015},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=900,CertificateDate="2015-6",Year=2015},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=1500,CertificateDate="2015-7",Year=2015},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=1200,CertificateDate="2015-8",Year=2015},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=600,CertificateDate="2015-9",Year=2015},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=700,CertificateDate="2015-10",Year=2015},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=900,CertificateDate="2015-11",Year=2015},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=600,CertificateDate="2015-12",Year=2015},


                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 996, CertificateDate = "2016-1", Year = 2016 },
                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 886, CertificateDate = "2016-2", Year = 2016 },
                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 1055, CertificateDate = "2016-3", Year = 2016 },
                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 1134, CertificateDate = "2016-4", Year = 2016 },
                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 1463, CertificateDate = "2016-5", Year = 2016 },
                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 1864, CertificateDate = "2016-6", Year = 2016 },
                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 1054, CertificateDate = "2016-7", Year = 2016 },
                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 985, CertificateDate = "2016-8", Year = 2016 },
                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 1077, CertificateDate = "2016-9", Year = 2016 },
                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 965, CertificateDate = "2016-10", Year = 2016 },
                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 1342, CertificateDate = "2016-11", Year = 2016 },
                //new CertificateModel { ExamId = 1083781789319174, Name = "财务会计核算应用能力", CertificateNum = 1379, CertificateDate = "2016-12", Year = 2016 },

                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 325, CertificateDate = "2016-1", Year = 2016 },
                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 328, CertificateDate = "2016-2", Year = 2016 },
                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 356, CertificateDate = "2016-3", Year = 2016 },
                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 320, CertificateDate = "2016-4", Year = 2016 },
                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 365, CertificateDate = "2016-5", Year = 2016 },
                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 542, CertificateDate = "2016-6", Year = 2016 },
                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 456, CertificateDate = "2016-7", Year = 2016 },
                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 556, CertificateDate = "2016-8", Year = 2016 },
                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 594, CertificateDate = "2016-9", Year = 2016 },
                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 350, CertificateDate = "2016-10", Year = 2016 },
                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 443, CertificateDate = "2016-11", Year = 2016 },
                //new CertificateModel { ExamId = 1083782449348616, Name = "投融资分析技术证", CertificateNum = 365, CertificateDate = "2016-12", Year = 2016 },

                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 600, CertificateDate = "2016-1", Year = 2016 },
                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 400, CertificateDate = "2016-2", Year = 2016 },
                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 800, CertificateDate = "2016-3", Year = 2016 },
                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 700, CertificateDate = "2016-4", Year = 2016 },
                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 900, CertificateDate = "2016-5", Year = 2016 },
                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 1100, CertificateDate = "2016-6", Year = 2016 },
                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 3500, CertificateDate = "2016-7", Year = 2016 },
                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 2900, CertificateDate = "2016-8", Year = 2016 },
                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 1900, CertificateDate = "2016-9", Year = 2016 },
                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 1700, CertificateDate = "2016-10", Year = 2016 },
                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 1200, CertificateDate = "2016-11", Year = 2016 },
                //new CertificateModel { ExamId = 1083781990645767, Name = "成本管控专业技术证书", CertificateNum = 1800, CertificateDate = "2016-12", Year = 2016 },


                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1032,CertificateDate="2017-1",Year=2017},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1220,CertificateDate="2017-2",Year=2017},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1201,CertificateDate="2017-3",Year=2017},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1410,CertificateDate="2017-4",Year=2017},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1210,CertificateDate="2017-5",Year=2017},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1322,CertificateDate="2017-6",Year=2017},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1622,CertificateDate="2017-7",Year=2017},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1720,CertificateDate="2017-8",Year=2017},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1230,CertificateDate="2017-9",Year=2017},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1701,CertificateDate="2017-10",Year=2017},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1302,CertificateDate="2017-11",Year=2017},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=1030,CertificateDate="2017-12",Year=2017},

                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1005,CertificateDate="2017-1",Year=2017},
                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1254,CertificateDate="2017-2",Year=2017},
                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1543,CertificateDate="2017-3",Year=2017},
                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1596,CertificateDate="2017-4",Year=2017},
                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1605,CertificateDate="2017-5",Year=2017},
                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1756,CertificateDate="2017-6",Year=2017},
                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1896,CertificateDate="2017-7",Year=2017},
                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1674,CertificateDate="2017-8",Year=2017},
                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1415,CertificateDate="2017-9",Year=2017},
                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1669,CertificateDate="2017-10",Year=2017},
                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1899,CertificateDate="2017-11",Year=2017},
                //new CertificateModel{ExamId=1083782231244807,Name="管理会计应用能力",CertificateNum=1988,CertificateDate="2017-12",Year=2017},


                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=1659,CertificateDate="2018-1",Year=2018},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=1459,CertificateDate="2018-2",Year=2018},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=1769,CertificateDate="2018-3",Year=2018},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=1989,CertificateDate="2018-4",Year=2018},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=2006,CertificateDate="2018-5",Year=2018},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=2159,CertificateDate="2018-6",Year=2018},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=1996,CertificateDate="2018-7",Year=2018},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=2014,CertificateDate="2018-8",Year=2018},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=2125,CertificateDate="2018-9",Year=2018},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=2132,CertificateDate="2018-10",Year=2018},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=2056,CertificateDate="2018-11",Year=2018},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=2136,CertificateDate="2018-12",Year=2018},

                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=2875,CertificateDate="2018-1",Year=2018},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=2832,CertificateDate="2018-2",Year=2018},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=3920,CertificateDate="2018-3",Year=2018},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=2880,CertificateDate="2018-4",Year=2018},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=2913,CertificateDate="2018-5",Year=2018},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=2520,CertificateDate="2018-6",Year=2018},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=2720,CertificateDate="2018-7",Year=2018},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=2590,CertificateDate="2018-8",Year=2018},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=2890,CertificateDate="2018-9",Year=2018},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=2850,CertificateDate="2018-10",Year=2018},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=2820,CertificateDate="2018-11",Year=2018},
                //new CertificateModel{ExamId=1083780988207109,Name="财务大数据应用能力证",CertificateNum=2890,CertificateDate="2018-12",Year=2018},


                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=1500,CertificateDate="2018-1",Year=2018},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=700,CertificateDate="2018-2",Year=2018},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=1900,CertificateDate="2018-3",Year=2018},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=2200,CertificateDate="2018-4",Year=2018},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=2600,CertificateDate="2018-5",Year=2018},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=3700,CertificateDate="2018-6",Year=2018},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=7700,CertificateDate="2018-7",Year=2018},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=6900,CertificateDate="2018-8",Year=2018},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=6800,CertificateDate="2018-9",Year=2018},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=4200,CertificateDate="2018-10",Year=2018},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=3700,CertificateDate="2018-11",Year=2018},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=2200,CertificateDate="2018-12",Year=2018},


                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3469,CertificateDate="2019-1",Year=2019},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=2985,CertificateDate="2019-2",Year=2019},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3654,CertificateDate="2019-3",Year=2019},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3785,CertificateDate="2019-4",Year=2019},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3896,CertificateDate="2019-5",Year=2019},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3945,CertificateDate="2019-6",Year=2019},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3745,CertificateDate="2019-7",Year=2019},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3689,CertificateDate="2019-8",Year=2019},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3965,CertificateDate="2019-9",Year=2019},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=4051,CertificateDate="2019-10",Year=2019},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=4101,CertificateDate="2019-11",Year=2019},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=4015,CertificateDate="2019-12",Year=2019},


                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=625,CertificateDate="2019-1",Year=2019},
                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=628,CertificateDate="2019-2",Year=2019},
                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=756,CertificateDate="2019-3",Year=2019},
                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=820,CertificateDate="2019-4",Year=2019},
                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=365,CertificateDate="2019-5",Year=2019},
                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=542,CertificateDate="2019-6",Year=2019},
                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=956,CertificateDate="2019-7",Year=2019},
                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=856,CertificateDate="2019-8",Year=2019},
                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=794,CertificateDate="2019-9",Year=2019},
                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=550,CertificateDate="2019-10",Year=2019},
                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=743,CertificateDate="2019-11",Year=2019},
                //new CertificateModel{ExamId=1083781262360581,Name="大数据分析决策能力证",CertificateNum=565,CertificateDate="2019-12",Year=2019},

                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=3895,CertificateDate="2019-1",Year=2019},
                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=4905,CertificateDate="2019-2",Year=2019},
                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=4905,CertificateDate="2019-3",Year=2019},
                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=4585,CertificateDate="2019-4",Year=2019},
                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=4955,CertificateDate="2019-5",Year=2019},
                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=4975,CertificateDate="2019-6",Year=2019},
                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=4600,CertificateDate="2019-7",Year=2019},
                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=5085,CertificateDate="2019-8",Year=2019},
                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=4095,CertificateDate="2019-9",Year=2019},
                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=4600,CertificateDate="2019-10",Year=2019},
                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=1500,CertificateDate="2019-11",Year=2019},
                //new CertificateModel{ExamId=1083781526601734,Name="大数据投融资分析能力证",CertificateNum=2000,CertificateDate="2019-12",Year=2019},

                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=1700,CertificateDate="2019-1",Year=2019},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=1500,CertificateDate="2019-2",Year=2019},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=2700,CertificateDate="2019-3",Year=2019},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=3900,CertificateDate="2019-4",Year=2019},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=4500,CertificateDate="2019-5",Year=2019},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=5100,CertificateDate="2019-6",Year=2019},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=8700,CertificateDate="2019-7",Year=2019},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=8100,CertificateDate="2019-8",Year=2019},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=7700,CertificateDate="2019-9",Year=2019},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=6900,CertificateDate="2019-10",Year=2019},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=5100,CertificateDate="2019-11",Year=2019},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=4900,CertificateDate="2019-12",Year=2019},


                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3265,CertificateDate="2020-1",Year=2020},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3695,CertificateDate="2020-2",Year=2020},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3945,CertificateDate="2020-3",Year=2020},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=3856,CertificateDate="2020-4",Year=2020},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=4080,CertificateDate="2020-5",Year=2020},
                //new CertificateModel{ExamId=1083782640762888,Name="业财税一体化技能实操能手",CertificateNum=4459,CertificateDate="2020-6",Year=2020},

                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=2100,CertificateDate="2020-1",Year=2020},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=1900,CertificateDate="2020-2",Year=2020},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=5200,CertificateDate="2020-3",Year=2020},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=6300,CertificateDate="2020-4",Year=2020},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=7700,CertificateDate="2020-5",Year=2020},
                //new CertificateModel{ExamId=1083781990645767,Name="成本管控专业技术证书",CertificateNum=8500,CertificateDate="2020-6",Year=2020},


            };

            return models;
        }
    }
}


































































































































































































































