﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OxConsoleApp
{
    public class UserDto
    {
        public long UserId { get; set; }

        public string RealName { get; set; }

        public string SchoolName { get; set; }

        public string StudentNo { get; set; }

    }
}
