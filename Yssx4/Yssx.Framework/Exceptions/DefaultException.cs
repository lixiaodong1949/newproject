﻿using System;

namespace Yssx.Framework.Exceptions
{
    public class DefaultException:BaseException
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        public DefaultException(string code, string message) :this(code,message,new Exception())
        {
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        public DefaultException(string code, string message, Exception exception) : base(code, message, exception)
        {
        }
    }
}
