﻿using System;
using System.Text; 
using Yssx.Framework.Logger;

namespace Yssx.Framework.Exceptions
{
    public abstract class BaseException : Exception
    {
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message">异常消息</param>
        protected BaseException(string code, string message)
            : this(code, message, new Exception())
        {
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="code">异常编码</param>
        /// <param name="message">异常消息</param>
        /// <param name="exception">嵌套异常类</param>
        protected BaseException(string code, string message, Exception exception)
            : base(FormatMessage(code, message), exception)
        {
            if (exception == null) return;

            //Data["ErrorCode"] = code;
            var sb = new StringBuilder(FormatMessage(code, message));
            sb.AppendLine($"Message: {exception.Message}");
            sb.AppendLine($"StackTrace:{exception.StackTrace}");
            sb.AppendLine($"Source:{exception.Source}");
            CommonLogger.Error(sb.ToString());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private static string FormatMessage(string code, string message)
        {
            return $"[ErrorCode: {code}]{message}";
        }
    }
}
