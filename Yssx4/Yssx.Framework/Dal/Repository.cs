﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Tas.Orm;
using Tas.Orm.Common;
using Tas.Orm.Interface;

namespace Report.Framework.Dal
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam> 
    //[Component(componentType:ComponentType.ClosedGeneric)]
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity.Entity
    {
        /// <summary>
        /// 
        /// </summary>
        protected DbEntityProvider EntityProvider { get; }
        /// <summary>
        /// 
        /// </summary>
        protected IEntityTable<TEntity> EntityTable { get; }

        protected Repository(IDbContext dbContext)
        {
            EntityProvider = dbContext.Provider;
            EntityTable = EntityProvider.GetTable<TEntity>(string.Empty);
        }

        /// <summary>
        ///   获取数据
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetAll()
        {
            return EntityTable;
        }
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual int Insert(TEntity entity)
        {
            return EntityTable.Insert(entity);
        }
        /// <summary>
        /// 批量新增
        /// </summary>
        /// <param name="entitys"></param>
        /// <returns></returns>
        public virtual int BatchInsert(IList<TEntity> entitys)
        {
            return EntityTable.Batch(entitys, (i, m) => i.Insert(m)).Sum();
        }

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="updateCheck"></param>
        /// <returns></returns>
        public virtual int Update(TEntity entity, Expression<Func<TEntity, bool>> updateCheck)
        {
            return Update(entity, null, updateCheck, false);
        }
        /// <summary>
        /// 更新实体指定列  不带条件，默认根据主键更新
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="express">列集合</param>
        /// <param name="isInclude"></param>
        /// <returns></returns>
        public virtual int Update(TEntity entity, Expression<Func<TEntity, object>> express, bool isInclude = true)
        {
            return Update(entity, express, null, isInclude);
        }

        /// <summary>
        /// 更新实体指定列  待条件的
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="express">列集合</param>
        /// <param name="updateCheck">条件</param>
        /// <param name="isInclude"></param>
        /// <returns></returns>
        public virtual int Update(TEntity entity, Expression<Func<TEntity, object>> express,
            Expression<Func<TEntity, bool>> updateCheck, bool isInclude = true)
        {
            return EntityTable.Update(entity, express, updateCheck, isInclude);
        }


        /// <summary>
        ///批量 更新实体
        /// </summary>
        /// <param name="entitys"></param> 
        /// <returns></returns>
        public virtual int BatchUpdate(IList<TEntity> entitys)
        {
            return EntityTable.Batch(entitys, (u, c) => u.Update(c)).Sum();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="deleteCheck"></param>
        /// <returns></returns>
        public virtual int Delete(TEntity entity, Expression<Func<TEntity, bool>> deleteCheck)
        {
            return EntityTable.Delete(entity, deleteCheck);
        }

        public virtual int Delete(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public virtual int Update(TEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
