﻿using System;
using System.Linq.Expressions;
using Report.Framework.Entity;

namespace Report.Framework.Dal
{
    public interface IDelete<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// 更新实体
        /// </summary> 
        /// <param name="entity"></param>
        /// <returns></returns>
        int Delete(TEntity entity);

        /// <summary>
        /// 
        /// </summary> 
        /// <param name="entity"></param>
        /// <param name="updateCheck"></param>
        /// <returns></returns>
        int Delete(TEntity entity, Expression<Func<TEntity, bool>> updateCheck);
    }
}
