﻿using Report.Framework.Entity;

namespace Report.Framework.Dal
{
    /// <summary>
    /// 数据访问
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity>:IInsert<TEntity>,IDelete<TEntity>,IUpdate<TEntity>,IQuery<TEntity> where TEntity : class, IEntity
    {
    }
}
