﻿using Tas.Orm;

namespace Report.Framework.Dal
{
    public interface IDbContext
    {
        DbEntityProvider Provider { get; }
    }
}
