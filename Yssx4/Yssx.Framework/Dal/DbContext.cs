﻿using MySql.Data.MySqlClient;
using System.Data;

namespace Yssx.Framework.Dal
{
    public static class DbContext
    {
        ///// <summary>
        ///// 数据库上下文单例
        ///// </summary>
        //public static DbContext Instance { get; }
        /// <summary>
        /// 数据库上下文单例
        /// </summary>
        public static IFreeSql FreeSql { get; private set; }

        public static IFreeSql FreeSql2 { get; private set; }

        /// <summary>
        /// 大数据库连接
        /// </summary>
        public static IFreeSql BDFreeSql { get; set; }
        
        /// <summary>
        /// 1+x
        /// </summary>
        public static IFreeSql OaxFreeSql { get; set; }
        /// <summary>
        /// 竞赛
        /// </summary>
        public static IFreeSql JSFreeSql { get; set; }

        /// <summary>
        /// 行业赛
        /// </summary>
        public static IFreeSql HysFreeSql { get; set; }

        /// <summary>
        /// 云上实训
        /// </summary>
        public static IFreeSql SxFreeSql { get; set; }

        /// <summary>
        /// 云实习
        /// </summary>
        public static IFreeSql YsxFreeSql { get; set; }

        /// <summary>
        /// 技能抽查
        /// </summary>
        public static IFreeSql JNFreeSql { get; set; }
        /// <summary>
        /// 新案例库
        /// </summary>
        public static IFreeSql TCFreeSql { get; set; }
        /// <summary>
        /// 新做题库
        /// </summary>
        public static IFreeSql UPFreeSql { get; set; }
        /// <summary>
        /// DapperConn
        /// </summary>
        public static IDbConnection DapperConn { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="freeSql"></param>
        internal static void RegisterFreeSql(IFreeSql  freeSql)
        {
            FreeSql = freeSql;
        }
        internal static void RegisterPracticeFreeSql(IFreeSql freeSql)
        {
            UPFreeSql = freeSql;
        }
        internal static void RegisterTCFreeSql(IFreeSql freeSql)
        {
            TCFreeSql = freeSql;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        internal static void RegisterDapper(string connectionString)
        {
            DapperConn = new MySqlConnection(connectionString);
        }

        internal static void RegisterBDFreeSql(IFreeSql freeSql)
        {
            BDFreeSql = freeSql;
        }

        internal static void RegisterFreeSql2(IFreeSql freeSql)
        {
            FreeSql2 = freeSql;
        }

        internal static void RegisterJSFreeSql(IFreeSql freeSql)
        {
            JSFreeSql = freeSql;
        }
        internal static void RegisterHysFreeSql(IFreeSql freeSql)
        {
            HysFreeSql = freeSql;
        }
        internal static void RegisterOaxFreeSql(IFreeSql freeSql)
        {
            OaxFreeSql = freeSql;
        }
        internal static void RegisterSxFreeSql(IFreeSql freeSql)
        {
            SxFreeSql = freeSql;
        }

        internal static void RegisterYsxFreeSql(IFreeSql freeSql)
        {
            YsxFreeSql = freeSql;
        }

        internal static void RegisterJNFreeSql(IFreeSql freeSql)
        {
            JNFreeSql = freeSql;
        }
    }
}
