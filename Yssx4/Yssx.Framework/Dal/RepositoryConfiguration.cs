﻿using System.Diagnostics;
using Tas.Common.Configurations;
using Yssx.Framework.Logger;

namespace Yssx.Framework.Dal
{
    /// <summary>
    /// 
    /// </summary>
    public static class RepositoryConfiguration
    {
        /// <summary> 
        /// </summary>
        /// <returns></returns>
        public static ProjectConfiguration UseFreeSqlRepository(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
                .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
                .UseSlave(slaveConnectionString)
                                            .UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
                                            //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                            .UseMonitorCommand(a =>
                                            {
                                                Trace.WriteLine(a.CommandText);
                                            })
                .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            fsql.Aop.ConfigEntityProperty = (s, e) =>
            {
                if (e.Property.PropertyType.IsEnum)
                    e.ModifyResult.MapType = typeof(int);
            };
            DbContext.RegisterFreeSql(fsql);
            DbContext.RegisterDapper(sqlConnection);
            return projectConfiguration;
        }
        public static ProjectConfiguration UseFreeSqlPracticeRepository(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
                .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
                .UseSlave(slaveConnectionString)
                                            //.UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
                                                                        //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                            .UseMonitorCommand(a =>
                                            {
                                                Trace.WriteLine(a.CommandText);
                                            })
                .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            fsql.Aop.ConfigEntityProperty = (s, e) =>
            {
                if (e.Property.PropertyType.IsEnum)
                    e.ModifyResult.MapType = typeof(int);
            };
            DbContext.RegisterPracticeFreeSql(fsql);
            return projectConfiguration;
        }
        public static ProjectConfiguration UseFreeSqlTCRepository(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
                .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
                .UseSlave(slaveConnectionString)
                                            //.UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
                                            //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                            .UseMonitorCommand(a =>
                                            {
                                                Trace.WriteLine(a.CommandText);
                                            })
                .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            fsql.Aop.ConfigEntityProperty = (s, e) =>
            {
                if (e.Property.PropertyType.IsEnum)
                    e.ModifyResult.MapType = typeof(int);
            };
            DbContext.RegisterTCFreeSql(fsql);
            return projectConfiguration;
        }
        public static ProjectConfiguration UseFreeSqlRepositoryOax(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
                .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
                .UseSlave(slaveConnectionString)
                                            //.UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】       
                                            .UseMonitorCommand(a =>
                                            {
                                                Trace.WriteLine(a.CommandText);
                                            })
                .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            fsql.Aop.ConfigEntityProperty = (s, e) =>
            {
                if (e.Property.PropertyType.IsEnum)
                    e.ModifyResult.MapType = typeof(int);
            };
            DbContext.RegisterOaxFreeSql(fsql);
            return projectConfiguration;
        }
        public static ProjectConfiguration UseFreeSqlRepositorySx(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
                .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
                .UseSlave(slaveConnectionString)
                                            //.UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
                                                                        //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                            .UseMonitorCommand(a =>
                                            {
                                                Trace.WriteLine(a.CommandText);
                                            })
                .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            fsql.Aop.ConfigEntityProperty = (s, e) =>
            {
                if (e.Property.PropertyType.IsEnum)
                    e.ModifyResult.MapType = typeof(int);
            };
            DbContext.RegisterSxFreeSql(fsql);
            return projectConfiguration;
        }

        /// <summary>
        /// 云实习
        /// </summary>
        /// <param name="projectConfiguration"></param>
        /// <param name="sqlConnection"></param>
        /// <param name="slaveConnectionString"></param>
        /// <returns></returns>
        public static ProjectConfiguration UseFreeSqlRepositoryYsx(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
               .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
               .UseSlave(slaveConnectionString)
                                           //.UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
                                           //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                           .UseMonitorCommand(a =>
                                           {
                                               Trace.WriteLine(a.CommandText);
                                           })
               .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            fsql.Aop.ConfigEntityProperty = (s, e) =>
            {
                if (e.Property.PropertyType.IsEnum)
                    e.ModifyResult.MapType = typeof(int);
            };
            DbContext.RegisterYsxFreeSql(fsql);
            return projectConfiguration;
        }

        public static ProjectConfiguration UseFreeSqlRepositoryJs(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
                .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
                .UseSlave(slaveConnectionString)
                                            //.UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
                                                                        //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                            .UseMonitorCommand(a =>
                                            {
                                                Trace.WriteLine(a.CommandText);
                                            })
                .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            //fsql.Aop.ConfigEntityProperty = (s, e) =>
            //{
            //    if (e.Property.PropertyType.IsEnum)
            //        e.ModifyResult.MapType = typeof(int);
            //};
            DbContext.RegisterJSFreeSql(fsql);
            return projectConfiguration;
        }
        public static ProjectConfiguration UseFreeSqlRepositoryHys(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
                .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
                .UseSlave(slaveConnectionString)
                                            //.UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
                                                                        //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                            .UseMonitorCommand(a =>
                                            {
                                                Trace.WriteLine(a.CommandText);
                                            })
                .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            //fsql.Aop.ConfigEntityProperty = (s, e) =>
            //{
            //    if (e.Property.PropertyType.IsEnum)
            //        e.ModifyResult.MapType = typeof(int);
            //};
            DbContext.RegisterHysFreeSql(fsql);
            return projectConfiguration;
        }
        /// <summary>
        /// 大数据连接库
        /// </summary>
        /// <returns></returns>
        public static ProjectConfiguration UseFreeSqlBDRepository(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
              .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
              .UseSlave(slaveConnectionString)
                                          .UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
                                                                      //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                          //.UseMonitorCommand(a =>
                                          //{
                                          //    Trace.WriteLine(a.CommandText);
                                          //})
              .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            fsql.Aop.ConfigEntityProperty = (s, e) =>
            {
                if (e.Property.PropertyType.IsEnum)
                    e.ModifyResult.MapType = typeof(int);
            };
            DbContext.RegisterBDFreeSql(fsql);
          
            return projectConfiguration;
        }

        public static ProjectConfiguration UseFreeSqlJSRepository(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
             .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
             .UseSlave(slaveConnectionString)
             //.UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
             //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
             //.UseMonitorCommand(a =>
             //{
             //    Trace.WriteLine(a.CommandText);
             //})
             .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            fsql.Aop.ConfigEntityProperty = (s, e) =>
            {
                if (e.Property.PropertyType.IsEnum)
                    e.ModifyResult.MapType = typeof(int);
            };
            DbContext.RegisterJSFreeSql(fsql);

            return projectConfiguration;
        }

        public static ProjectConfiguration UseFreeSqlJNRepository(this ProjectConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
            .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
            .UseSlave(slaveConnectionString)
            //.UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
            //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
            //.UseMonitorCommand(a =>
            //{
            //    Trace.WriteLine(a.CommandText);
            //})
            .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            fsql.Aop.ConfigEntityProperty = (s, e) =>
            {
                if (e.Property.PropertyType.IsEnum)
                    e.ModifyResult.MapType = typeof(int);
            };
            DbContext.RegisterJNFreeSql(fsql);

            return projectConfiguration;
        }

        public static ProjectConfiguration UseFreeSqlRepository2(this ProjectConfiguration projectConfiguration, string sqlConnection, string sqlConnection2)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
                .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
                                            .UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
                                                                        //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                            .UseMonitorCommand(a =>
                                            {
                                                Trace.WriteLine(a.CommandText);
                                            })
                .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            //fsql.Aop.ConfigEntityProperty = (s, e) =>
            //{
            //    if (e.Property.PropertyType.IsEnum)
            //        e.ModifyResult.MapType = typeof(int);
            //};


            var fsql2 = new FreeSql.FreeSqlBuilder()
              .UseConnectionString(FreeSql.DataType.MySql, sqlConnection2)
                                          .UseAutoSyncStructure(false) //自动同步实体结构【开发环境必备】                
                                                                       //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                          .UseMonitorCommand(a =>
                                          {
                                              Trace.WriteLine(a.CommandText);
                                          })
              .Build();
            fsql2.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            //fsql2.Aop.ConfigEntityProperty = (s, e) =>
            //{
            //    if (e.Property.PropertyType.IsEnum)
            //        e.ModifyResult.MapType = typeof(int);
            //};
            DbContext.RegisterFreeSql(fsql);
            DbContext.RegisterFreeSql2(fsql2);
            return projectConfiguration;
        }
    }
}
