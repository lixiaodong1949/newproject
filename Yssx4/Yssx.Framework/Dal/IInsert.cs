﻿using System.Collections.Generic;
using Report.Framework.Entity;

namespace Report.Framework.Dal
{
    public interface IInsert<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// 新增实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int Insert(TEntity entity);

        /// <summary>
        /// 新增实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        int BatchInsert(IList<TEntity> entity);
    }
}
