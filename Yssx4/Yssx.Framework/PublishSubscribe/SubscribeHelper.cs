﻿using System;
using System.Collections.Concurrent;
using Tas.Common.Utils;
using Yssx.Framework.Logger;

namespace Yssx.Framework.PublishSubscribe
{
    /// <summary>
    /// 
    /// </summary>
    public class SubscribeHelper
    {
        private static ConcurrentDictionary<string, byte> keyValuePairs = new ConcurrentDictionary<string, byte>();
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="channel"></param>
        /// <param name="data"></param>
        /// <param name="action"></param>
        public static void SubscribeAndPublish<T>(string channel, T data, Action<T> action)
        {
            if (!keyValuePairs.ContainsKey(channel))
            {
                keyValuePairs.TryAdd(channel, 0);
                RedisHelper.PSubscribe(new string[] { channel }, (a) =>
                {
                    var json = a.Body;
                    if (string.IsNullOrEmpty(json))
                        return;
                    try
                    {
                        action(JsonHelper.DeserializeObject<T>(json));
                    }
                    catch (Exception ex)
                    {
                        CommonLogger.Error("SubscribeRegister", ex);
                    }
                });
            }
            RedisHelper.Publish(channel, JsonHelper.SerializeObject(data));
        }
    }
}
