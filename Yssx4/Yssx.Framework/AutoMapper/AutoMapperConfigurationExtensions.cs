﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Tas.Common.Configurations;
using Yssx.Framework.AutoMapper;

namespace Yssx.Framework.AutoMapper
{
    /// <summary> 
    /// 
    /// </summary>
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="assemblies"></param>
        private static void AddProfiles(IMapperConfigurationExpression configuration, IList<Assembly> assemblies)
        {
            var baseType = typeof(AutoMapperProfile);
            foreach (var assembly in assemblies)
            {
                foreach (var type in assembly.GetTypes().Where(m => m.IsClass && !m.IsAbstract && baseType.IsAssignableFrom(m)))
                {
                    var profile = Activator.CreateInstance(type) as AutoMapperProfile;
                    configuration.AddProfile(profile);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="assemblies"></param>
        /// <returns></returns>
        public static ProjectConfiguration UseAutoMapper(this ProjectConfiguration configuration, IList<Assembly> assemblies)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMissingTypeMaps = true;
                AddProfiles(cfg, assemblies);
            });

            Mapper.AssertConfigurationIsValid();
            return configuration;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static IMappingExpression<TSource, TDestination> IgnoreAllNonExisting<TSource, TDestination>(this IMappingExpression<TSource, TDestination> expression)
        {
            var flags = BindingFlags.Public | BindingFlags.Instance;
            var sourceType = typeof(TSource);
            var destinationProperties = typeof(TDestination).GetProperties(flags);

            foreach (var property in destinationProperties)
            {
                if (sourceType.GetProperty(property.Name, flags) == null)
                {
                    expression.ForMember(property.Name, opt => opt.Ignore());
                }
            }
            return expression;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDestination"></typeparam>
        /// <param name="autoMapperProfile"></param>
        public static AutoMapperProfile BidirectMapIgnoreAllNonExisting<TSource, TDestination>(this AutoMapperProfile autoMapperProfile)
        {
            autoMapperProfile.CreateMap<TSource, TDestination>().IgnoreAllNonExisting();
            autoMapperProfile.CreateMap<TDestination, TSource>().IgnoreAllNonExisting();
            return autoMapperProfile;
        } 
    }
}