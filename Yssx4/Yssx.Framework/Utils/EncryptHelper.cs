﻿using System.Security.Cryptography;
using System.Text;

namespace Yssx.Framework.Utils
{
    public static class EncryptHelper
    {
        /// <summary>
        /// MD5加密服务
        /// </summary>
        /// <param name="strEncrypt">密码明文</param>
        /// <returns>密码密文</returns>
        public static string Md5(this string strEncrypt)
        {
            var strResult = new StringBuilder(1000);
            var md5 = new MD5CryptoServiceProvider();

            var bEncrypt = Encoding.UTF8.GetBytes(strEncrypt ?? string.Empty);
            var bResult = md5.ComputeHash(bEncrypt);

            foreach (var b in bResult) //十六进制方式输出
            {
                strResult.AppendFormat("{0:X2}", b);
            }
            return strResult.ToString();
        }
        /// <summary>
        /// MD5加密服务
        /// </summary>
        /// <param name="strEncrypt">密码明文</param>
        /// <returns>密码密文</returns>
        public static string SHA256(this string strEncrypt)
        {
            var strResult = new StringBuilder(1000);
            var md5 = new SHA256Managed();

            var bEncrypt = Encoding.UTF8.GetBytes(strEncrypt ?? string.Empty);
            var bResult = md5.ComputeHash(bEncrypt);

            foreach (var b in bResult) //十六进制方式输出
            {
                strResult.AppendFormat("{0:X2}", b);
            }
            return strResult.ToString();

           
        }


    }
}
