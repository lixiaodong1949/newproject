﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text; 
using Yssx.Framework.Logger;

namespace Yssx.Framework.Utils
{

    public class CsvHelper
    {
        public static string List2String<T>(List<T> items)
        {
            var type = typeof(T);
            using (var swCsv = new StringWriter())
            {

                var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(prop => prop.IsDefined(typeof(DisplayNameAttribute), false)).ToList();
                var titleStr = string.Empty;
                foreach (var description in properties.Select(prop => prop.GetCustomAttributes(typeof(DisplayNameAttribute), false).Cast<DisplayNameAttribute>().ToList()).Where(description => description.Count > 0))
                {
                    titleStr += (description[0].DisplayName + ",");
                }
                swCsv.WriteLine(titleStr.TrimEnd(','));

                foreach (var item in items)
                {
                    var rowStr = string.Empty;
                    foreach (var prop in properties)
                    {
                        var value = prop.GetValue(item);
                        if (value == null)
                        {
                            rowStr += ",";
                        }
                        else if (prop.PropertyType == typeof(int) || prop.PropertyType == typeof(long))
                        {
                            rowStr += $"{value},";
                        }
                        else if (prop.PropertyType == typeof(decimal))
                        {
                            rowStr += $"{Convert.ToDecimal(value):F2},";
                        }
                        else
                        {
                            var content = value.ToString().Trim();
                            if (content.Contains(','))
                            {
                                content = "\"" + content + "\"";
                            }
                            rowStr += $"{content}\t,";
                        }
                    }
                    swCsv.WriteLine(rowStr.TrimEnd(','));
                }
                return swCsv.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="dateFormat"></param>
        /// <param name="moneyFormat"></param>
        /// <returns></returns>
        public static List<T> DataTable2List<T>(DataTable table,string dateFormat="yyyy-MM-dd HH:mm:ss",string moneyFormat="F2") where T : new()
        {
            var ts = new List<T>();

            if (table == null)
                return ts;
            foreach (DataRow dr in table.Rows)
            {
                var t = new T();
                var propertys = t.GetType().GetProperties();
                foreach (var pi in propertys)
                {
                    var tempName = pi.Name; // 检查DataTable是否包含此列    

                    if (!table.Columns.Contains(tempName)) continue;
                   
                    if (!pi.CanWrite) continue;  

                    var value = dr[tempName];
                    if (value == DBNull.Value) continue;

                    var type = value.GetType();
                    if (type == typeof(DateTime))
                    { 
                        pi.SetValue(t, Convert.ToDateTime(value).ToString(dateFormat), null);
                    }
                    //else if (type == typeof(decimal))
                    //{
                    //    pi.SetValue(t, Convert.ToDecimal(value).ToString(moneyFormat), null);
                    //}
                    else
                        pi.SetValue(t, value, null);

                }
                ts.Add(t);
            }
            return ts;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static string DataTableString(DataTable table)
        {
            using (var swCsv = new StringWriter())
            {
                var titleStr = string.Empty;
                foreach (DataColumn column in table.Columns)
                {
                    titleStr += (column.ColumnName + ",");
                }
                swCsv.WriteLine(titleStr.TrimEnd(','));

                foreach (DataRow row in table.Rows)
                {
                    var rowStr = string.Empty;
                    foreach (DataColumn column in table.Columns)
                    {
                        var obj = row[column.ColumnName];
                        var content = obj?.ToString().Trim() ?? string.Empty;
                        if (content.Contains(','))
                        {
                            content = "\"" + content + "\"";
                        }
                        rowStr += $"{content},";
                    }
                    swCsv.WriteLine(rowStr.TrimEnd(','));
                }
                return swCsv.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="strFilePath"></param>
        /// <param name="fileName"></param>
        public static void Table2Csv(DataTable dt, string strFilePath, string fileName)
        {
            try
            {
                if (!Directory.Exists(strFilePath))
                {
                    Directory.CreateDirectory(strFilePath);
                }
                if (dt == null)
                    return;
                var content = DataTableString(dt);
                using (var fs = new FileStream(strFilePath + "\\" + fileName, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    var bytes = Encoding.Default.GetBytes(content);
                    fs.Write(bytes, 0, bytes.Length);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                CommonLogger.Error("Table2Csv", ex);
            }
        }

    }

}