﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading; 

namespace Yssx.Framework.Utils
{
    public static class ReflectionExtensions
    {
        /// <summary>
        /// 获取表达式中属性的名称
        /// </summary>
        /// <typeparam name="T"></typeparam> 
        /// <param name="t"></param>
        /// <param name="express"></param>
        /// <returns></returns>
        public static IEnumerable<string> ExpressionMemberNames<T>(this T t, Expression<Func<T, object>> express)
        {
            return t.ExpressionMemberNames<T, object>(express);
        }
        /// <summary>
        /// 获取表达式中属性的名称
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="t"></param>
        /// <param name="express"></param>
        /// <returns></returns>
        public static IEnumerable<string> ExpressionMemberNames<T, TResult>(this T t, Expression<Func<T, TResult>> express)
        {
            if (express == null)
                return null;
            switch (express.Body.NodeType)
            {
                case ExpressionType.New:
                    return ((NewExpression)express.Body).Members.Select(m => m.Name);
                default:
                    throw new NotImplementedException();
            }
        }
        /// <summary>
        /// 多线程状态下的读写锁
        /// </summary>
        private static readonly ReaderWriterLockSlim HandlerSlim = new ReaderWriterLockSlim();
        /// <summary>
        /// 排序的字典，提高查找效率
        /// </summary>
        private static readonly SortedDictionary<string, FastPropertySetHandler> FastPropertySetHandlers = new SortedDictionary<string, FastPropertySetHandler>();
        /// <summary>
        /// 排序的字典，提高查找效率
        /// </summary>
        private static readonly SortedDictionary<string, FastPropertyGetHandler> FastPropertyGetHandlers = new SortedDictionary<string, FastPropertyGetHandler>();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="member"></param>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static object GetValue(this MemberInfo member, object instance)
        {
            switch (member.MemberType)
            {
                case MemberTypes.Property:
                    var type = instance.GetType();
                    var prop = (PropertyInfo)member;
                    FastPropertyGetHandler getHandler;
                    var key = $"{type.Name}_{prop.Name}";
                    if (!FastPropertyGetHandlers.TryGetValue(key, out getHandler))
                    {
                        HandlerSlim.ExitReadLock();
                        if (!FastPropertyGetHandlers.TryGetValue(key, out getHandler))
                        {
                            getHandler = DynamicCalls.GetPropertyGetter(prop);
                            FastPropertyGetHandlers.Add(key, getHandler);
                        }
                        HandlerSlim.ExitReadLock();
                    }
                    //return ((PropertyInfo)member).GetValue(instance, null);
                    return prop.GetValue(instance, null);
                case MemberTypes.Field:
                    return ((FieldInfo)member).GetValue(instance);
                default:
                    throw new InvalidOperationException();
            }
        }

        public static void SetValue(this MemberInfo member, object instance, object value)
        {
            switch (member.MemberType)
            {
                case MemberTypes.Property:
                    var type = instance.GetType();
                    var prop = (PropertyInfo)member;
                    FastPropertySetHandler setHandler;
                    var key = $"{type.Name}_{prop.Name}";
                    if (!FastPropertySetHandlers.TryGetValue(key, out setHandler))
                    {
                        HandlerSlim.ExitReadLock();
                        if (!FastPropertySetHandlers.TryGetValue(key, out setHandler))
                        {
                            setHandler = DynamicCalls.GetPropertySetter(prop);
                            FastPropertySetHandlers.Add(key, setHandler);
                        }
                        HandlerSlim.ExitReadLock();
                    }
                    setHandler(instance, value);
                    //var pi = (PropertyInfo)member;
                    //pi.SetValue(instance, value, null);
                    break;
                case MemberTypes.Field:
                    var fi = (FieldInfo)member;
                    fi.SetValue(instance, value);
                    break;
                default:
                    throw new InvalidOperationException();
            }
        }
    }
}
