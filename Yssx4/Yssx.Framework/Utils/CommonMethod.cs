﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tas.Common.Utils;

namespace Yssx.Framework.Utils
{
    public static class CommonMethod
    {
        private static readonly DateTime SystemStartDataTime = new DateTime(1970, 1, 1);
        private static readonly DateTime LocalSystemStartDataTime = TimeZone.CurrentTimeZone.ToLocalTime(SystemStartDataTime);
        public static long ConvertDateTimeToLong(DateTime time)
        {
            return Convert.ToInt64((time - LocalSystemStartDataTime).TotalMilliseconds);
        }
        /// <summary>
        /// 从1970.01.01开始计时的所有秒数
        /// </summary>
        /// <returns></returns>
        public static long GetTotalSeconds()
        {
            return (long)(DateTime.Now - SystemStartDataTime).TotalSeconds;
        }
        /// <summary>
        /// 获取时间
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static DateTime GetDateTimeBySeconds(long seconds)
        {
            return SystemStartDataTime.AddSeconds(seconds);
        }

        /// <summary>
        /// 获取当前月最后一天
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime GetMonthLastDay(this DateTime dateTime)
        {
            int monthDay = DateTime.DaysInMonth(dateTime.Year, dateTime.Month); //获取某年某月有多少天

            return new DateTime(dateTime.Year, dateTime.Month, monthDay);
        }
        /// <summary>
        /// 对象深克隆
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static T DeepClone<T>(this T t)
        {
            var json = JsonHelper.SerializeObject(t);
            return JsonHelper.DeserializeObject<T>(json);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string RemoveJsonSpecialChar(string s)
        {
            var sb = new StringBuilder();
            foreach (var c in s)
            {
                switch (c)
                {
                    case '\"':
                        break;
                    case '\\':
                        break;
                    case '/':
                        break;
                    case '\b':
                        break;
                    case '\f':
                        break;
                    case '\n':
                        break;
                    case '\r':
                        break;
                    case '\t':
                        break;
                    default:
                        if ((c >= 0 && c <= 31) || c == 127) //在ASCⅡ码中，第0～31号及第127号(共33个)是控制字符或通讯专用字符
                            break;
                        sb.Append(c);
                        break;
                }
            }
            return sb.ToString();
        }
        /// <summary>
        /// 字符和
        /// </summary>
        public static int GetCharSum(this string str)
        {
            var sum = 0;
            foreach (var c in str)
            {
                sum += c;
            }
            return sum;
        }
        /// <summary>
        /// 字符和
        /// </summary>
        public static int GetCharSum(this string[] arr)
        {
            var sum = 0;
            foreach (var str in arr)
            {
                foreach (var c in str)
                {
                    sum += c;
                }
            }
            return sum;
        }
        /// <summary>
        /// 比较数组是否元素类容相同
        /// </summary>
        public static bool CompareArrayElement(string[] arr, string[] arr1)
        {
            if (arr.Length != arr1.Length)
                return false;
            foreach (var str in arr)
            {
                if (!arr1.Contains(str))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// 比较数组内容有多少个元素相同
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="arr1"></param>
        /// <returns></returns>
        public static int CompareArrayElement2(string[] arr, string[] arr1)
        {
            int count = 0;
            foreach (var str in arr1)
            {
                if (arr.Contains(str))
                    count++;
                else 
                { 
                    count = 0;
                    break; 
                }
            }
            return count;
        }
        /// <summary>
        /// 分隔数组转换为指定类型
        /// </summary>
        public static T[] SplitToArray<T>(this string @string, char splitChar)
        {
            if (@string == null || @string.Length == 0)
                return null;

            var arr = @string.Split(new[] { splitChar }, StringSplitOptions.RemoveEmptyEntries).Select(m =>
            {
                return (T)Convert.ChangeType(m, typeof(T));
            }).ToArray();
            return arr;
        }

        /// <summary>
        /// 字符串转换成集合
        /// </summary>
        public static List<T> SplitToList<T>(this string @string, char splitChar)
        {
            if (string.IsNullOrWhiteSpace(@string))
                return null;

            var list = @string.Split(new[] { splitChar }, StringSplitOptions.RemoveEmptyEntries).Select(m =>
            {
                return (T)Convert.ChangeType(m, typeof(T));
            }).ToList();

            return list;
        }
        /// <summary>
        /// 数组转换为特定字符分隔的字符串
        /// </summary>
        public static string Join(this object[] array, string splitChar)
        {
            if (array == null || array.Length == 0)
                return "";

            return string.Join(splitChar, array);
        }

        /// <summary>
        /// 集合转换为特定字符分隔的字符串
        /// </summary>
        public static string Join<T>(this IEnumerable<T> array, string splitChar)
        {
            if (array == null || array.Count() == 0)
                return "";

            return string.Join(splitChar, array);
        }
    }
}
