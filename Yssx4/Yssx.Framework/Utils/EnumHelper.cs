﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace Yssx.Framework.Utils
{

    public static class EnumHelper
    {

        #region FetchDescription
        /// <summary>
        /// 获取枚举值的描述文本
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum value)
        {

            try
            {
                if (value == null)
                    return null;

                FieldInfo fi = value.GetType().GetField(value.ToString());
                if (fi == null)
                    return value.ToString();
                DescriptionAttribute[] attributes =
                      (DescriptionAttribute[])fi.GetCustomAttributes(
                      typeof(DescriptionAttribute), false);
                return ((attributes?.Length ?? 0) > 0) ? attributes[0]?.Description : value.ToString();
            }
            catch
            {
                return value.ToString();
            }

        }

        #endregion
    }
}
