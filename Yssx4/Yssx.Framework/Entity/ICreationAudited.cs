﻿using System;

namespace Yssx.Framework.Entity
{
    public interface ICreationAudited
    {
        /// <summary>
        /// 
        /// </summary>
        long CreateBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        DateTime CreateTime { get; set; }
    }
}
