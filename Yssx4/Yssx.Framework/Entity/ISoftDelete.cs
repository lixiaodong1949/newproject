﻿namespace Yssx.Framework.Entity
{
    public interface ISoftDelete:IEntity
    {
        /// <summary>
        /// 0 位 未删除 1为已删除
        /// </summary>
        int IsDelete { get; set; }
    }
}
