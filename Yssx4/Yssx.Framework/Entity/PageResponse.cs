﻿using System;
using System.Collections.Generic;

namespace Yssx.Framework.Entity
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PageResponse<T> : BaseResponse where T : class
    {
        public PageResponse()
        {

        }

        public PageResponse(int pageIndex, int pageSize, long recordCount, List<T> result)
        {
            RecordCount = recordCount;
            PageIndex = pageIndex;
            PageSize = pageSize;
            Data = result;
        }
        /// <summary>
        /// 
        /// </summary>
        public long RecordCount { get; set; }

        /// <summary>
        /// 总行数
        /// </summary>
        public long Count { get { return RecordCount; } }
        /// <summary>
        /// 
        /// </summary>
        public int PageCount => PageSize == 0 ? 0 : (int)Math.Ceiling((decimal)RecordCount / PageSize);

        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; } = 1;

        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; } = 20;
        /// <summary>
        /// 
        /// </summary>
        public List<T> Data { get; set; }
    }
    public class PageQuery<T>
    {
        public PageQuery()
        {

        }
        public PageQuery(int pageIndex, int pageSize, T query)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
            Query = query;
        }



        private int _pageIndex;
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex
        {
            get
            {
                if (_pageIndex <= 0)
                    _pageIndex = 1;
                return _pageIndex;
            }
            set { _pageIndex = value; }
        }

        private int _pageSize;
        /// <summary>
        /// 每页数量
        /// </summary>
        public int PageSize
        {
            get
            {
                if (_pageSize <= 0 || _pageSize > 100)
                    _pageSize = 20;
                return _pageSize;
            }
            set { _pageSize = value; }
        }
        /// <summary>
        /// 查询信息
        /// </summary>
        public T Query { get; set; }

        /// <summary>
        /// 状态-1所有 0未开始 1已开始 2已结束
        /// </summary>
        public ExamStatusForQuery Status { get; set; } = ExamStatusForQuery.All;
    }
    public class PageQuery
    {
        public PageQuery()
        {

        }
        public PageQuery(int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }



        private int _pageIndex;
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex
        {
            get
            {
                if (_pageIndex <= 0)
                    _pageIndex = 1;
                return _pageIndex;
            }
            set { _pageIndex = value; }
        }

        private int _pageSize;
        /// <summary>
        /// 每页数量
        /// </summary>
        public int PageSize
        {
            get
            {
                if (_pageSize <= 0 || _pageSize > 100)
                    _pageSize = 20;
                return _pageSize;
            }
            set { _pageSize = value; }
        }

    }
}
