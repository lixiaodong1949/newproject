﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Entity
{
    /// <summary>
    /// 租户接口
    /// </summary>
    public interface ITenant
    {
        /// <summary>
        /// 租户ID
        /// </summary>
        long TenantId { get; set; }
    }
}
