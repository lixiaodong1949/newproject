using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Entity
{
    public class PageRequest
    {
        public PageRequest()
        {

        }
        public PageRequest(int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }



        private int _pageIndex;
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex
        {
            get
            {
                if (_pageIndex <= 0)
                    _pageIndex = 1;
                return _pageIndex;
            }
            set { _pageIndex = value; }
        }

        private int _pageSize;
        /// <summary>
        /// 每页数量
        /// </summary>
        public int PageSize
        {
            get
            {
                if (_pageSize <= 0 || _pageSize > 10000)
                    _pageSize = 20;
                return _pageSize;
            }
            set { _pageSize = value; }
        }

        /// <summary>
        /// 0：会计实训,1:出纳实训,2：大数据,3:业财税，  4:中职高考冲刺模拟题 （非必填）
        /// </summary>
        public Nullable<int> CaseType { get; set; }

        /// <summary>
        /// 排序字段:对应的字段自行定义对应的值（非必填）
        /// </summary>
        public int Orderby { get; set; } = 0;
    }

    public class PageRequest<T>
    {
        private int _pageIndex;
        /// <summary>
        /// 页码
        /// </summary>
        public int PageIndex
        {
            get
            {
                if (_pageIndex <= 0)
                    _pageIndex = 1;
                return _pageIndex;
            }
            set { _pageIndex = value; }
        }

        private int _pageSize;
        /// <summary>
        /// 每页数量
        /// </summary>
        public int PageSize
        {
            get
            {
                if (_pageSize <= 0 || _pageSize > 100)
                    _pageSize = 20;
                return _pageSize;
            }
            set { _pageSize = value; }
        }
        /// <summary>
        /// 查询信息
        /// </summary>
        public T Query { get; set; }

    }


}
