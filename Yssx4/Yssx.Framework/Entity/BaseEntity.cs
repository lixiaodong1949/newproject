﻿using FreeSql.DataAnnotations;

namespace Yssx.Framework.Entity
{
    public abstract class BaseEntity<TPKey>:Entity
    {
        [Column(Name ="Id",IsPrimary =true)]
        public virtual TPKey Id { get; set; }
    }
}
