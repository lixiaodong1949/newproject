﻿using System;

namespace Yssx.Framework.Entity
{
    public abstract class TenantBizBaseEntity<TPKey> : BizBaseEntity<TPKey>, ITenant
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual long TenantId { get; set; }
        ///// <summary>
        ///// 
        ///// </summary>
        //public int IsDelete { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        //public long CreateBy { get; set; }
        ///// <summary>
        ///// 
        ///// </summary>
        //public DateTime CreateTime { get; set; }
        ///// <summary>
        ///// 
        ///// </summary>
        //public long? UpdateBy { get; set; }
        ///// <summary>
        ///// 
        ///// </summary>
        //public DateTime? UpdateTime { get; set; }

    }
}
