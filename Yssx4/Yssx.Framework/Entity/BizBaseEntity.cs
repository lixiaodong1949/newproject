﻿using System;

namespace Yssx.Framework.Entity
{
    public abstract class BizBaseEntity<TPKey> : BaseEntity<TPKey>, IBizAudited
    {
        /// <summary>
        /// 
        /// </summary>
        public int IsDelete { get; set; } = CommonConstants.IsNotDelete;

        /// <summary>
        /// 
        /// </summary>
        public long CreateBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 
        /// </summary>
        public long UpdateBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdateTime { get; set; }

        ///// <summary>
        ///// 状态
        ///// </summary>
        //public virtual int Status { get; set; }
    }
}
