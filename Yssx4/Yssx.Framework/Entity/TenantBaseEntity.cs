﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Entity
{
   /// <summary>
   /// 租户基类
   /// </summary>
   /// <typeparam name="TPKey"></typeparam>
    public class TenantBaseEntity<TPKey> : BaseEntity<TPKey>, ITenant
    {
        /// <summary>
        /// 学校Id
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IsDelete { get; set; } = 0;

        /// <summary>
        /// 用户Id
        /// </summary>
        public long CreateBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 
        /// </summary>
        public DateTime? UpdateTime { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TPKey"></typeparam>
    public class TenantAndUserEntity<TPKey> : BaseEntity<TPKey>, ITenant
    {
        public long TenantId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int IsDelete { get; set; } = 0;

        /// <summary>
        /// 
        /// </summary>
        public long CreateBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
