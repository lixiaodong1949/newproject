﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Entity
{
    /// <summary>
    /// 返回集合
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ListResponse<T> : BaseResponse
    {
        public ListResponse() { }

        public ListResponse(List<T> data)
        {
            Code = CommonConstants.SuccessCode;
            Data = data;
        }
        public ListResponse(int code, string msg)
        {
            Code = code;
            Msg = msg;
        }
        /// <summary>
        /// 设置成功
        /// </summary>
        /// <param name="data"></param>
        public void SetSuccess(List<T> data)
        {
            Code = CommonConstants.SuccessCode;
            Msg = "";
            Data = data;
        }

        /// <summary>
        /// 设置失败
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="data"></param>
        /// <param name="code"></param>
        public virtual void SetError(string msg, List<T> data, int code = CommonConstants.BadRequest)
        {
            Data = data;
            SetError(msg, code);
        }
        public List<T> Data { get; set; }
    }
}
