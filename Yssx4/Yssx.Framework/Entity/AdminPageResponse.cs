﻿using System;
using System.Collections.Generic;

namespace Yssx.Framework.Entity
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AdminPageResponse<T> : BaseResponse where T : class
    { 
        /// <summary>
        /// 
        /// </summary>
        public long Count { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PageIndex { get; set; } = 1;

        /// <summary>
        /// 
        /// </summary>
        public int PageSize { get; set; } = 20;
        /// <summary>
        /// 
        /// </summary>
        public List<T> Data { get; set; } 
    } 
}
