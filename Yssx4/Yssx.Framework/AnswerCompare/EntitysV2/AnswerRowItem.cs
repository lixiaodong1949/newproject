﻿using System;

namespace Yssx.Framework.AnswerCompare.EntitysV2
{
    /// <summary>
    /// 
    /// </summary>
    public class AnswerRowItem : IComparable<AnswerRowItem>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal? Score { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsRight { get; set; }

        /// <summary>
        /// 是否有效值
        /// </summary>
        public bool IsValid { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public AnswerItemType ItemType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(AnswerRowItem other)
        {
            if (Name == null) return 1;
            if (other == null || other.Name == null) return 1;
            if (int.TryParse(Name.Replace("item_", string.Empty), out var num) && int.TryParse(other.Name.Replace("item_", string.Empty), out var otherNum))
            {
                return num.CompareTo(otherNum);
            }
            return Name.CompareTo(other.Name);
        }
    }
}
