﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.AnswerCompare.EntitysV2
{
    /// <summary>
    /// 
    /// </summary>
    public class AnswerCountInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public ItemCountInfo HeaderCountInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ItemCountInfo GeneralCountInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ItemCountInfo BodyCountInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ItemCountInfo ColumnCountInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ItemCountInfo FooterCountInfo { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public ItemCountInfo SealCountInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        private ItemCountInfo _totalCountInfo = new ItemCountInfo();
        /// <summary>
        /// 
        /// </summary>
        public ItemCountInfo TotalCountInfo
        {
            get
            {
                _totalCountInfo.ValidCount = HeaderCountInfo.ValidCount + GeneralCountInfo.ValidCount +
                    BodyCountInfo.ValidCount + FooterCountInfo.ValidCount + SealCountInfo.ValidCount + ColumnCountInfo.ValidCount;

                _totalCountInfo.RightCount = HeaderCountInfo.RightCount + GeneralCountInfo.RightCount +
                    BodyCountInfo.RightCount + FooterCountInfo.RightCount + SealCountInfo.RightCount + ColumnCountInfo.RightCount;

                var totalScord = HeaderCountInfo.Scored + GeneralCountInfo.Scored +
                   BodyCountInfo.Scored + FooterCountInfo.Scored + SealCountInfo.Scored + ColumnCountInfo.Scored;
                _totalCountInfo.Scored = totalScord < 0 ? 0 : totalScord;
                return _totalCountInfo;
            }
        }
    }
}
