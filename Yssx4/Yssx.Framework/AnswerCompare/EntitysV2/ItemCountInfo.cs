﻿namespace Yssx.Framework.AnswerCompare.EntitysV2
{
    /// <summary>
    /// 
    /// </summary>
    public class ItemCountInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int ValidCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int RightCount { get; set; }


        /// <summary>
        /// 得分
        /// </summary>
        public decimal Scored { get; set; }
    }
}
