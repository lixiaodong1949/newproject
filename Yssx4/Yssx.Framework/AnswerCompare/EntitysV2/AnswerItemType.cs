﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.AnswerCompare.EntitysV2
{
    public enum AnswerItemType
    {
        /// <summary>
        /// 一般的
        /// </summary>
        General = 0,
        /// <summary>
        /// 印章
        /// </summary>
        Seal = 1
    }
}
