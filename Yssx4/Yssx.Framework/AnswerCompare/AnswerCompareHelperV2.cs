﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Tas.Common.Utils;
using Yssx.Framework.AnswerCompare.EntitysV2;

namespace Yssx.Framework.AnswerCompare
{
    public class AnswerCompareHelperV2
    {
        const string ExpStr = "_exp";
        private static readonly DataTable computeDt = new DataTable();
        /// <summary>
        /// 本地区域
        /// </summary>
        private static readonly CultureInfo LocalCultureInfo = new CultureInfo("zh-cn");
        /// <summary>
        /// 
        /// </summary>
        private static readonly Regex numberRegex = new Regex(@"^[-]?[1-9]{1}\d+[.]?\d*$");
        /// <summary>
        /// 带千分位的
        /// </summary>
        private static readonly Regex thousandthNumberRegex = new Regex(@"^(([-]?[1-9][0-9]{0,2}(,\d{3})*)|0)(\.\d*)?$");
        /// <summary>
        /// 日期正则
        /// </summary>
        private static readonly Regex dateRegex = new Regex(@"^(?:(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(\/|-|\.)(?:0?2\1(?:29))$)|(?:(?:1[6-9]|[2-9]\d)?\d)(\/|-|\.)(?:(?:(?:0?[13578]|1[02])\2(?:31))|(?:(?:0?[1,3-9]|1[0-2])\2(29|30))|(?:(?:0?[1-9])|(?:1[0-2]))\2(?:0?[1-9]|1\d|2[0-8]))$");
        /// <summary>
        /// 
        /// </summary>
        /// <param name="exp"></param>
        /// <returns></returns>
        private static object GetExpVal(string exp)
        {
            try
            {
                if (exp.Contains('+') || exp.Contains('-') || exp.Contains('*') || exp.Contains('/'))
                    return computeDt.Compute(exp, null);
                return exp;
            }
            catch (Exception ex)
            {
                throw new Exception($"AnswerCompareHelper.GetExpVal:{exp},表达式转换失败!", ex);
            }
        }
        /// <summary>
        /// 设置为错误
        /// </summary>
        /// <param name="row"></param>
        private static void SetRowItemValidStatus(AnswerRow row)
        {
            foreach (var item in row.Items)
            {
                if (!string.IsNullOrWhiteSpace(item.Value))
                    item.IsValid = true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="standradStr"></param>
        /// <param name="answerStr"></param>
        /// <returns></returns>
        private static bool CompareNumberString(string standradStr, string answerStr)
        {
            //decimal.TryParse(standradStr, out var snumber);
            //decimal.TryParse(answerStr,out var anumber);
            decimal.TryParse(standradStr, NumberStyles.AllowLeadingSign | NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint, LocalCultureInfo, out var snumber);
            decimal.TryParse(answerStr, NumberStyles.AllowLeadingSign | NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint, LocalCultureInfo, out var anumber);
            return snumber == anumber;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="standradStr"></param>
        /// <param name="answerStr"></param>
        /// <returns></returns>
        private static bool CompareValue(string standradStr, string answerStr)
        {
            var rst = standradStr == answerStr;
            if (rst)
                return true;
            //去掉时间大写为零的判断逻辑
            //if (standradStr.IndexOf('0') == 0 || standradStr.IndexOf('零') == 0 || answerStr.IndexOf('0') == 0 || answerStr.IndexOf('零') == 0)
            //{
            //    if (standradStr.Length <= 2)
            //    {
            //        standradStr = standradStr.TrimStart('0').TrimStart('零');
            //        answerStr = answerStr.TrimStart('0').TrimStart('零');
            //        return standradStr == answerStr;
            //    }
            //}
            if (standradStr.IndexOf('0') == 0  || answerStr.IndexOf('0') == 0)
            {
                if (standradStr.Length <= 2)
                {
                    standradStr = standradStr.TrimStart('0');
                    answerStr = answerStr.TrimStart('0');
                    return standradStr == answerStr;
                }
            }
            if (numberRegex.IsMatch(standradStr, 0) || thousandthNumberRegex.IsMatch(standradStr, 0))
            {
                standradStr = standradStr.Replace(",", "");
                answerStr = answerStr.Replace(",", "");
                return CompareNumberString(standradStr, answerStr);
            }
            if (standradStr.IndexOf('￥') == 0 || standradStr.IndexOf('¥') == 0)
            {
                if (!(answerStr.IndexOf('￥') == 0 || answerStr.IndexOf('¥') == 0))
                    return false;
                return CompareNumberString(standradStr.Substring(1), answerStr.Substring(1));
            }
            if (standradStr.EndsWith('%') && answerStr.EndsWith('%'))
            {
                standradStr = standradStr.Replace("%", "");
                answerStr = answerStr.Replace("%", "");
                return CompareNumberString(standradStr, answerStr);
            }
            if (dateRegex.IsMatch(standradStr))
            {
                DateTime.TryParse(standradStr, out var sDateTime);
                DateTime.TryParse(answerStr, out var aDateTime);
                return sDateTime == aDateTime;
            }
            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="standradPropValue"></param>
        /// <param name="answerPropValue"></param>
        /// <returns></returns>
        private static bool CompareRowItem(AnswerRowItem standradRowItem, AnswerRowItem answerRowItem, bool isCurrencyFullCompare = false)
        {
            var standradRowItemValue = standradRowItem.Value?.Trim();
            var answerRowItemValue = answerRowItem.Value?.Trim();
            if (string.IsNullOrWhiteSpace(standradRowItemValue))
            {
                if ((standradRowItem.Score ?? 0) == 0)
                    return false;
                standradRowItem.IsValid = true;
                answerRowItem.IsValid = true;
                if (decimal.TryParse(answerRowItem.Value.Trim(), out decimal number) && number == 0 || string.IsNullOrWhiteSpace(answerRowItemValue))
                    return true;
                return false;
            }
            standradRowItem.IsValid = true;
            answerRowItem.IsValid = true;

            if (string.IsNullOrWhiteSpace(answerRowItemValue))
            {
                return false;
            }
            if (standradRowItemValue.Contains("|"))
            {
                var standradPropValueArr = standradRowItemValue.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var str in standradPropValueArr)
                {
                    if (CompareValue(str, answerRowItemValue))
                        return true;
                }
                return false;
            }
            return CompareValue(standradRowItemValue, answerRowItemValue);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="standardAnswerRows"></param>
        private static void MarkNoCompareItemValidState(List<AnswerRow> standardAnswerRows)
        {
            foreach (var row in standardAnswerRows)
            {
                foreach (var item in row.Items)
                {
                    var val = item.Value?.Trim();
                    if (string.IsNullOrWhiteSpace(val) && (item.Score ?? 0) == 0)
                    {
                        continue;
                    }
                    item.IsValid = true;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="standardAnswerRow"></param>
        /// <param name="answerRow"></param>
        private static void CompareAnswerRows(List<AnswerRow> standardAnswerRows, List<AnswerRow> answerRows)
        {
            if (standardAnswerRows == null)
                return;
            if (answerRows == null)
                return;
            var index = 0;
            var standardAnswerDic = standardAnswerRows.ToDictionary((item) => index++);
            var tmpRowIndexs = new List<int>();
            foreach (var answerRow in answerRows)
            {
                var answerItem = answerRow.Items.FirstOrDefault();
                var standardKeyValuePairs = standardAnswerDic.Where(m => !tmpRowIndexs.Contains(m.Key)).ToList();
                for (var i = 0; i < standardKeyValuePairs.Count; i++)
                {
                    var standardAnswerKvp = standardKeyValuePairs[i];
                    var standardItem = standardAnswerKvp.Value.Items.FirstOrDefault();
                    if (string.IsNullOrEmpty(answerItem.Value))
                    {
                        SetRowItemValidStatus(answerRow);
                        break;
                    }
                    if (answerItem.Value == standardItem.Value)
                    {
                        CompareAnswerRow(standardAnswerKvp.Value, answerRow);
                        tmpRowIndexs.Add(standardAnswerKvp.Key);
                        break;
                    }
                    if (i == standardKeyValuePairs.Count - 1)
                    {
                        SetRowItemValidStatus(answerRow);
                    }
                }
            }
            MarkNoCompareItemValidState(standardAnswerDic.Where(m => !tmpRowIndexs.Contains(m.Key)).Select(m => m.Value).ToList());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="standardAnswerRow"></param>
        /// <param name="answerRow"></param>
        private static void CompareAnswerColumns(List<AnswerRow> standardAnswerRows, List<AnswerRow> answerRows)
        {
            if (standardAnswerRows == null)
                return;
            if (answerRows == null)
                return;
            if (standardAnswerRows.Count != answerRows.Count)
                return;
            for (var i = 0; i < standardAnswerRows.Count; i++)
            {
                var sRow = standardAnswerRows[i];
                var aRow = answerRows[i];
                CompareAnswerRow(sRow, aRow);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="standardAnswerRow"></param>
        /// <param name="answerRow"></param>
        private static void CompareAnswerRow(AnswerRow standardAnswerRow, AnswerRow answerRow)
        {
            if (standardAnswerRow == null)
                return;

            var standardAnswerRowItems = standardAnswerRow.Items.ToList();
            var answerRowItems = answerRow.Items.ToList();
            for (var i = 0; i < standardAnswerRowItems.Count; i++)
            {
                var standardItem = standardAnswerRowItems[i];
                var item = answerRowItems[i];
                if (CompareRowItem(standardItem, item))
                {
                    item.IsRight = true;
                    item.IsValid = true;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="standardAnswerSeal"></param>
        /// <param name="answerSeal"></param>
        private static void CompareSealRow(AnswerRow standardAnswerRow, Answer answer, int floatingTopPixel, int floatingLeftPixel)
        {
            var answerRow = answer.SealCtrlRow;
            if (standardAnswerRow == null)
                return;
            var index = 0;
            var itemDic = answerRow.Items.Take(standardAnswerRow.Items.Count).ToDictionary((item) => index++);
            var tmpItemIndexs = new List<int>();
            var newAnswerRow = new AnswerRow() { Items = new SortedSet<AnswerRowItem>() };
            foreach (var item in standardAnswerRow.Items)
            {
                var seal = JsonHelper.DeserializeObject<Seal>(item.Value);
                item.IsValid = true;
                var answerRowItemsDic = itemDic.Where(m => !tmpItemIndexs.Contains(m.Key)).ToList();
                for (var i = 0; i < answerRowItemsDic.Count; i++)
                {
                    var kvp = answerRowItemsDic[i];
                    var answerSeal = JsonHelper.DeserializeObject<Seal>(kvp.Value.Value);
                    if (CompareSealItem(seal, answerSeal, floatingTopPixel, floatingLeftPixel))
                    {
                        tmpItemIndexs.Add(kvp.Key);
                        kvp.Value.IsRight = true;
                        kvp.Value.IsValid = true;
                        newAnswerRow.Items.Add(kvp.Value);
                        break;
                    }
                    if (i == answerRowItemsDic.Count - 1)
                    {
                        item.IsRight = false;
                        newAnswerRow.Items.Add(item);
                    }
                }
            }
            answer.SealCtrlRow = newAnswerRow;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="standardAnswerSeal"></param>
        /// <param name="answerSeal"></param>
        private static bool CompareSealItem(Seal standardAnswerSeal, Seal answerSeal, int floatingTopPixel, int floatingLeftPixel)
        {
            if (!(answerSeal.Top < standardAnswerSeal.Top + floatingTopPixel && answerSeal.Top > standardAnswerSeal.Top - floatingTopPixel))
                return false;

            if (!(answerSeal.Left < standardAnswerSeal.Left + floatingLeftPixel && answerSeal.Left > standardAnswerSeal.Left - floatingLeftPixel))
                return false;

            return answerSeal.Url == standardAnswerSeal.Url;
        }


        /// <summary>
        /// 比较答案对象
        /// </summary>
        /// <param name="standardAnswer">标准答案</param>
        /// <param name="answer">做题答案</param>
        /// <param name="topicScore">题目分值</param>
        /// <returns></returns>
        public static AnswerCountInfo Compare(Answer standardAnswer, Answer answer, int floatingTopPixel = 130, int floatingLeftPixel = 300)
        {
            CompareAnswerRow(standardAnswer.HeaderCtrlRow, answer.HeaderCtrlRow);
            CompareAnswerRow(standardAnswer.FooterCtrlRow, answer.FooterCtrlRow);
            CompareAnswerRow(standardAnswer.ItemCtrlRow, answer.ItemCtrlRow);

            CompareSealRow(standardAnswer.SealCtrlRow, answer, floatingTopPixel, floatingLeftPixel);

            CompareAnswerRows(standardAnswer.BodyCtrlRows, answer.BodyCtrlRows);
            CompareAnswerColumns(standardAnswer.BodyCtrlColumns, answer.BodyCtrlColumns);
            var countInfo = new AnswerCountInfo() { BodyCountInfo = new ItemCountInfo(), ColumnCountInfo = new ItemCountInfo() };

            int GetValidCount(AnswerRow standardAnswerRow)
            {
                if (standardAnswerRow == null)
                    return 0;
                var validCount = 0;
                foreach (var standardItem in standardAnswerRow.Items)
                {
                    if (standardItem.IsValid)
                    {
                        validCount++;
                    }
                }
                return validCount;
            }

            Tuple<int, decimal> CalcScored(AnswerRow answerRow)
            {
                if (answerRow == null)
                    return new Tuple<int, decimal>(0, 0);

                var rightCounts = 0;
                var scored = 0m;

                foreach (var item in answerRow.Items)
                {
                    if (item.IsRight && item.IsValid)
                    {
                        rightCounts++;
                        var score = item.Score ?? 0;
                        if (score > 0)
                            scored += score;
                    }
                    else if (!item.IsRight && item.IsValid)
                    {
                        var score = item.Score ?? 0;
                        if (score < 0)
                            scored += score;
                    }
                }
                return new Tuple<int, decimal>(rightCounts, scored);
            }
            var hInfo = CalcScored(answer.HeaderCtrlRow);
            countInfo.HeaderCountInfo = new ItemCountInfo { ValidCount = GetValidCount(standardAnswer.HeaderCtrlRow), RightCount = hInfo.Item1, Scored = hInfo.Item2 };
            var fInfo = CalcScored(answer.FooterCtrlRow);
            countInfo.FooterCountInfo = new ItemCountInfo { ValidCount = GetValidCount(standardAnswer.FooterCtrlRow), RightCount = fInfo.Item1, Scored = fInfo.Item2 };
            var gInfo = CalcScored(answer.ItemCtrlRow);
            countInfo.GeneralCountInfo = new ItemCountInfo { ValidCount = GetValidCount(standardAnswer.ItemCtrlRow), RightCount = gInfo.Item1, Scored = gInfo.Item2 };
            var sInfo = CalcScored(answer.SealCtrlRow);
            countInfo.SealCountInfo = new ItemCountInfo { ValidCount = GetValidCount(standardAnswer.SealCtrlRow), RightCount = sInfo.Item1, Scored = sInfo.Item2 };

            standardAnswer.BodyCtrlRows?.ForEach((m) => countInfo.BodyCountInfo.ValidCount += GetValidCount(m));
            standardAnswer.BodyCtrlColumns?.ForEach((m) => countInfo.BodyCountInfo.ValidCount += GetValidCount(m));

            answer.BodyCtrlRows?.ForEach((m) =>
            {
                var bInfo = CalcScored(m);
                countInfo.BodyCountInfo.RightCount += bInfo.Item1;
                countInfo.BodyCountInfo.Scored += bInfo.Item2;
            });
            answer.BodyCtrlColumns?.ForEach((m) =>
            {
                var bInfo = CalcScored(m);
                countInfo.BodyCountInfo.RightCount += bInfo.Item1;
                countInfo.BodyCountInfo.Scored += bInfo.Item2;
            });
            return countInfo;
        }
    }
}