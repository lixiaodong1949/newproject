﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yssx.Framework.Extensions
{
    public static class NumberExtension
    {

        /// <summary>
        /// 转罗马转换字符串
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static string ToRoman(this int num)
        {
            string res = String.Empty;
            List<int> val = new List<int> { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
            List<string> str = new List<string> { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
            for (int i = 0; i < val.Count; ++i)
            {
                while (num >= val[i])
                {
                    num -= val[i];
                    res += str[i];
                }
            }
            return res;
        }
        /// <summary>
        /// 转中文小写数字
        /// </summary>
        /// <param name="inputNum"></param>
        /// <returns></returns>
        public static string ToLowerChinese(this int inputNum)
        {
            string[] intArr = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
            string[] strArr = { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九", };
            string[] Chinese = { "", "十", "百", "千", "万", "十", "百", "千", "亿" };
            //金额
            char[] tmpArr = inputNum.ToString().ToArray();
            string tmpVal = "";
            for (int i = 0; i < tmpArr.Length; i++)
            {
                tmpVal += strArr[tmpArr[i] - 48];//ASCII编码 0为48
                tmpVal += Chinese[tmpArr.Length - 1 - i];//根据对应的位数插入对应的单位
            }
            return tmpVal;
        }


    }
}
