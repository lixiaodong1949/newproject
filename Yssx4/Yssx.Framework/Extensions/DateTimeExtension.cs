﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Extensions
{
    /// <summary>
    /// 时间类型扩展方法
    /// </summary>
    public static class DateTimeExtension
    {
        /// <summary>
        /// 判断时间是否为空，为空或者"0000-00-00 00:00:00"返回true，否则false
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static bool IsEmpty(this DateTime dateTime)
        {
            if (dateTime == null || dateTime == new DateTime(0))
                return true;
            return false;
        }
        /// <summary>
        /// 判断时间是否为空，为空或者"0000-00-00 00:00:00"返回true，否则false
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static bool IsEmpty(this DateTime? dateTime)
        {
            if (dateTime == null || !dateTime.HasValue)
                return true;

            return IsEmpty(dateTime.Value);
        }
    }
}
