﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Extensions
{
    public static class StringExtensions
    {
        public static decimal ToDecimal(this string s)
        {
            decimal d = 0;
            try
            {
                if (!string.IsNullOrEmpty(s) && s.Contains("E"))//科学计数法转换
                    d = Convert.ToDecimal(Decimal.Parse(s, System.Globalization.NumberStyles.Float));
                else
                    decimal.TryParse(s, out d);
            }
            catch (Exception ex)
            {

            }

            return d;
        }
        public static decimal ToDecimal(this string s, int decimals = 2)
        {
            decimal d = 0;
            if (!string.IsNullOrEmpty(s) && s.Contains("E"))//科学计数法转换
                d = Convert.ToDecimal(Decimal.Parse(s, System.Globalization.NumberStyles.Float));
            else
                decimal.TryParse(s, out d);

            return Math.Round(d, decimals);
        }

        public static int ToInt(this string s)
        {
            int d = 0;
            int.TryParse(s, out d);
            return d;
        }
        public static long ToLong(this string s)
        {
            long d = 0;
            long.TryParse(s, out d);
            return d;
        }

        public static string ToStringNoZero(this decimal s)
        {
            if (s == 0)
                return string.Empty;
            return s.ToString();
        }
    }
}
