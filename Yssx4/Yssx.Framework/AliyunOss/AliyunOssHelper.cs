﻿using Aliyun.OSS;
using System;
using System.IO;
using Tas.Common.Configurations;

namespace Yssx.Framework.AliyunOss
{
    /// <summary>
    /// 阿里云 oss helper
    /// </summary>
    public class AliyunOssHelper
    {
        #region 文件流上传
        /// <summary>
        /// 文件流上传
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="stream"></param>
        /// <param name="requestBucketName"></param>
        /// <param name="watermark"></param>
        /// <param name="iscompressed"></param>
        /// <param name="istestSpace"></param>
        /// <returns></returns>
        public static Tuple<string, string> OssUpload(string fileName, Stream stream, string requestBucketName, bool watermark = false, bool iscompressed = true, bool istestSpace = false)
        {
            requestBucketName = "freessoul";
            var extensionStr = AppSettingConfig.GetSection("ExtensionStr").ToString();//".txt,.doc,.docx,.wps,.pdf,.xlsx,.xls,.apk,.pptx,.ppt";
            var extension = Path.GetExtension(fileName);
            string suffix = "";
            if (!extensionStr.Contains(extension.ToLower()))
            {
                if (watermark && iscompressed)//添加水印,压缩图片
                {
                    suffix = string.Equals(extension, ".png", StringComparison.OrdinalIgnoreCase) ? "!sypng" : "!syw1200";
                }
                else if (watermark == false && iscompressed == false)// 不添加水印、不压缩图片
                {
                    suffix = string.Equals(extension, ".png", StringComparison.OrdinalIgnoreCase) ? "!zd" : "!zd";
                }
                else if (watermark && iscompressed == false)// 添加水印、不压缩图片
                {
                    suffix = string.Equals(extension, ".png", StringComparison.OrdinalIgnoreCase) ? "!syzd" : "!syzd";
                }
                else//不添加水印、压缩图片，默认
                {
                    suffix = string.Equals(extension, ".png", StringComparison.OrdinalIgnoreCase) ? "!png" : "!w1200";
                }
            }
            else
                requestBucketName = "test";
            var ossUrlPrefix = AppSettingConfig.GetSection("OssUrlPrefix").ToString().Replace("{BucketName}", requestBucketName);

            if (string.IsNullOrEmpty(ossUrlPrefix))
                throw new Exception("ossUrlPrefix为空!");

            var endpoint = "oss-cn-shenzhen.aliyuncs.com";
            var accessKeyId = "LTAI5tGt5heAbVFJhj28omuq";
            var accessKeySecret = "XIZVfBryKhmsK5GGhvNrom1bacSG1I";
            var bucketName = requestBucketName;

            var newFileName = Guid.NewGuid().ToString().Replace("-", string.Empty) + extension;
            var objectName = (istestSpace ? "zdap-test" : DateTime.Now.ToString("yyyy-MM-dd")) + "/" + newFileName;
            try
            {
                var client = new OssClient(endpoint, accessKeyId, accessKeySecret);

                var rst = client.PutObject(bucketName, objectName, stream);

                Console.WriteLine("Put object succeeded");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Put object failed, {0}", ex.ToString());
                throw;
            }

            return new Tuple<string, string>(newFileName, ossUrlPrefix + objectName + suffix);
        }
        #endregion

    }
}
