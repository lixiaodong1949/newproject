﻿using System.Diagnostics;
using Tas.Common.Configurations;
using Yssx.Framework.Logger;

namespace Yssx.Framework.Redis
{
    /// <summary>
    /// 
    /// </summary>
    public static class RedisConfiguration
    {
        /// <summary> 
        /// </summary>
        /// <returns></returns>
        public static ProjectConfiguration UseRedisJs(this ProjectConfiguration projectConfiguration, string connection)
        {
            RedisHelperExtension.InitializationJsInstance(connection);
            return projectConfiguration;
        }
        public static ProjectConfiguration UseRedisHys(this ProjectConfiguration projectConfiguration, string connection)
        {
            RedisHelperExtension.InitializationHysInstance(connection);
            return projectConfiguration;
        }
        public static ProjectConfiguration UseRedisSx(this ProjectConfiguration projectConfiguration, string connection)
        {
            RedisHelperExtension.InitializationSxInstance(connection);
            return projectConfiguration;
        }
    }
}
