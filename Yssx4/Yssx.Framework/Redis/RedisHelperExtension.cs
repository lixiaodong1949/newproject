﻿using CSRedis;

namespace Yssx.Framework.Redis
{
    public class RedisHelperExtension
    {
        public RedisHelperExtension() { }
        public static CSRedisClient JsInstance { get; set; }
        public static CSRedisClient HysInstance { get; set; }
        public static CSRedisClient SxInstance { get; set; }

        public static void InitializationJsInstance(string connectionString)
        {
            JsInstance = new CSRedisClient(connectionString);
        }
        public static void InitializationHysInstance(string connectionString)
        {
            HysInstance = new CSRedisClient(connectionString);
        }
        public static void InitializationSxInstance(string connectionString)
        {
            SxInstance = new CSRedisClient(connectionString);
        }
    }
}
