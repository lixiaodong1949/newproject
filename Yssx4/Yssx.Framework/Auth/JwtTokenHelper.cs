﻿using IdentityModel;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Yssx.Framework.Authorizations;

namespace Yssx.Framework.Auth
{
    /// <summary>
    /// JWTToken生成类
    /// </summary>
    public class JwtTokenHelper
    {
        /// <summary>
        /// 获取基于JWT的Token
        /// </summary> 
        /// <returns></returns>
        public static UserTicket BuildJwtToken(UserTicket ticket, PermissionRequirement permissionRequirement)
        {
            var claims = new Claim[]
                {
                    new Claim(JwtClaimTypes.Id, ticket.Id.ToString()),
                    new Claim(JwtClaimTypes.Name, ticket.Name??string.Empty),
                    new Claim(CommonConstants.ClaimsRealName, ticket.RealName??string.Empty),
                    new Claim(CommonConstants.ClaimsClientId, ticket.ClientId.ToString()),
                    new Claim(CommonConstants.ClaimsUserType, ticket?.Type.GetHashCode().ToString()),
                    new Claim(CommonConstants.ClaimsTenantId, ticket?.TenantId.ToString()),
                    new Claim(CommonConstants.ClaimsLoginTimespan, DateTime.Now.Ticks.ToString()),
                    new Claim(CommonConstants.LoginType, ticket.LoginType.ToString()),
                    new Claim(CommonConstants.SchoolType, ticket.SchoolType.ToString()),
                    new Claim(CommonConstants.Account, ticket.MobilePhone??string.Empty),
                    new Claim(CommonConstants.SchoolName,ticket.SchoolName),
                    new Claim(CommonConstants.IsVip,ticket.IsVip??string.Empty),
                };
            var jwt = new JwtSecurityToken(claims: claims, signingCredentials: permissionRequirement.SigningCredentials);
            var token = new JwtSecurityTokenHandler().WriteToken(jwt);
            ticket.AccessToken = token;
            return ticket;
        }

        public static string ExperienceToken(UserTicket ticket, PermissionRequirement permissionRequirement)
        {
            var claims = new Claim[]
               {
                    new Claim(JwtClaimTypes.Id, ticket.Id.ToString()),
                    new Claim(JwtClaimTypes.Name, ticket.Name??string.Empty),
                    new Claim(CommonConstants.ClaimsRealName, ticket.RealName??string.Empty),
                    new Claim(CommonConstants.ClaimsClientId, ticket.ClientId.ToString()),
                    new Claim(CommonConstants.ClaimsUserType, ticket?.Type.GetHashCode().ToString()),
                    new Claim(CommonConstants.ClaimsTenantId, ticket?.TenantId.ToString()),
                    new Claim(CommonConstants.LoginType, ticket.LoginType.ToString()),
                    new Claim(CommonConstants.SchoolType, ticket.SchoolType.ToString()),
                    new Claim(CommonConstants.Account, ticket.MobilePhone),
                    new Claim(CommonConstants.SchoolName,ticket.SchoolName),
               };
            var jwt = new JwtSecurityToken(claims: claims, signingCredentials: permissionRequirement.SigningCredentials);
            var token = new JwtSecurityTokenHandler().WriteToken(jwt);
            ticket.AccessToken = token;
            return token;
        }

        /// <summary>
        /// 获取基于JWT的Token
        /// </summary> 
        /// <returns></returns>
        public static bkUserTicket BuildJwtToken(bkUserTicket ticket, PermissionRequirement permissionRequirement)
        {
            var claims = new Claim[]
                {
                    new Claim(JwtClaimTypes.Id, ticket.Id.ToString()),
                    new Claim(JwtClaimTypes.Name, ticket.Name??string.Empty),
                    new Claim(CommonConstants.LoginType, ticket.LoginType.ToString()),
                    //new Claim(CommonConstants.ClaimsRealName, ticket.RealName??string.Empty),
                    //new Claim(CommonConstants.ClaimsClientId, ticket.ClientId.ToString()),
                    new Claim(CommonConstants.ClaimsUserType, ticket?.Type.GetHashCode().ToString()),
                    //new Claim(CommonConstants.ClaimsTenantId, ticket?.TenantId.ToString()),
                    new Claim(CommonConstants.ClaimsLoginTimespan, DateTime.Now.Ticks.ToString())
                };
            var jwt = new JwtSecurityToken(claims: claims, signingCredentials: permissionRequirement.SigningCredentials);
            var token = new JwtSecurityTokenHandler().WriteToken(jwt);
            ticket.AccessToken = token;
            return ticket;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param> 
        /// <param name="claims"></param> 
        public static UserTicket GetUserTicket(string token)
        {
            var jwt = new JwtSecurityTokenHandler().ReadJwtToken(token);
            var claims = jwt.Claims;
            var dicClaim = claims.ToDictionary(m => m.Type, m => m.Value);
            dicClaim.TryGetValue(JwtClaimTypes.Name, out var name);
            dicClaim.TryGetValue(JwtClaimTypes.Id, out var id);
            dicClaim.TryGetValue(CommonConstants.ClaimsClientId, out var clientId);
            dicClaim.TryGetValue(CommonConstants.ClaimsRealName, out var realName);
            dicClaim.TryGetValue(CommonConstants.ClaimsTenantId, out var tenantId);
            dicClaim.TryGetValue(CommonConstants.ClaimsUserType, out var userType);
            int.TryParse(userType, out var usertypeInt);
            var userTicket = new UserTicket
            {
                Name = name,
                RealName = realName,
                ClientId = string.IsNullOrEmpty(clientId) ? Guid.Empty : Guid.Parse(clientId),
                Id = long.Parse(id),
                TenantId = long.Parse(tenantId),
                Type = usertypeInt,
            };
            return userTicket;
        }
    }
}
