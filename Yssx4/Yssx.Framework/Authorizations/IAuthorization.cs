﻿namespace Yssx.Framework.Authorizations
{
    public interface IAuthorization
    {
        /// <summary>
        /// 需要授权的用户
        /// </summary>
        UserTicket User { get;}
    }
}
