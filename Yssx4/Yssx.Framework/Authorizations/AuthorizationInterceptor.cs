﻿//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using Castle.DynamicProxy;
//using YiBai.Common.Attributes;
//using YiBai.Common.Utilites;

//namespace YiBai.NewPms.Framework
//{
//    [Component]
//    public class AuthorizationInterceptor : IInterceptor
//    {
//        public void Intercept(IInvocation invocation)
//        {
//            if (invocation.TargetType.IsAssignableTo<IAuthorization>())
//            {
//                var customAttributes = invocation.MethodInvocationTarget.CustomAttributes.FirstOrDefault(m => m.AttributeType == typeof(PermissionAttribute));
//                if (customAttributes != null)
//                {
//                    var user = (invocation.InvocationTarget as IAuthorization)?.User;
//                    var permissionEnum = customAttributes.ConstructorArguments[0].Value;
//                    RbacContext.Assert(user, (PermissionEnum)permissionEnum);
//                }
//            }
//            invocation.Proceed();
//        }
//    }
//}
