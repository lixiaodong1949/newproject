﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Authorizations
{
    /// <summary>
    /// 学生考试登录
    /// </summary>
    public class StudentExamTicket : UserTicket
    {
        /// <summary>
        /// 学生id
        /// </summary>
        public long StudentId { get; set; }
        /// <summary>
        /// 当前考试ID
        /// </summary>
        public long CurrentExamId { get; set; }
    }


    /// <summary>
    /// 微信登录
    /// </summary>
    public class WeChartTicket : UserTicket
    {
        ///// <summary>
        ///// 手机号
        ///// </summary>
        //public string MobilePhone { get; set; }
    }

}
