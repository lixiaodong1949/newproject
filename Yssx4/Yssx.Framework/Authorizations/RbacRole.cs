﻿using System.Collections.Generic;

namespace Yssx.Framework.Authorizations
{
    public class RbacRole
    {
        /// <summary>
        /// 角色Id
        /// </summary>
        public long RoleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<RbacPermission> Permissions { get; set; }
    }
}
