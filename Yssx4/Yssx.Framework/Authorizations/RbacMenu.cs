﻿namespace Yssx.Framework.Authorizations
{
    public class RbacMenu
    {
        /// <summary>
        /// 
        /// </summary>
        public long MenuId { get; set; }

        public string Name { get; set; }

        public long Pid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Icon { get; set; }
    }
}
