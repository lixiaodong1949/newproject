﻿using Microsoft.Extensions.Logging;
using Tas.Common.Configurations;

namespace Yssx.Framework.Logger
{
    /// <summary>
    /// 
    /// </summary>
    public static class LoggerConfiguration
    {
        /// <summary> 
        /// </summary>
        /// <returns></returns>
        public static ProjectConfiguration UseCommonLogger(this ProjectConfiguration projectConfiguration, ILoggerFactory loggerFactory)
        {
            CommonLogger.RegisterLoggerFactory(loggerFactory);
            return projectConfiguration;
        }
    }
}
