﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using Tas.Common.Configurations;

namespace Yssx.Framework.Payment.Webchat
{
    /// <summary>
    /// 配置账号信息
    /// </summary>
    public static class WxPayConfig
    {

        private static volatile IConfig config;
        private static object syncRoot = new object();

        public static IConfig GetConfig()
        {
            if (config == null)
            {
                lock (syncRoot)
                {
                    if (config == null)
                        config = new WebchatPayConfig();
                }
            }
            return config;
        }

        public static ProjectConfiguration UseWebchat(this ProjectConfiguration projectConfiguration,IConfiguration configuration)
        {
            WebchatPayConfig.Configuration = configuration;
            return projectConfiguration;
        }
    }
}
