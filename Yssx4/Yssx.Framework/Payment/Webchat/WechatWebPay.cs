﻿using System;
using System.Web;

namespace Yssx.Framework.Payment.Webchat
{
    public class WechatWebPay
    {
        private Order _order;

        public WechatWebPay(Order order)
        {
            _order = order;
        }

        public WechatWebPay()
        {
        }

        /// <summary>
        /// 面对面二维码支付
        /// </summary>
        /// <returns></returns>
        public PaymentResult Pay()
        {
            WxPayData data = new WxPayData();
            data.SetValue("body", _order.Name);//商品名称
            data.SetValue("detail", _order.Body);//商品描述
            data.SetValue("attach", "专一网");//附加数据
            data.SetValue("out_trade_no", _order.OrderNo);//订单号
            data.SetValue("total_fee", (int)(decimal.Parse(_order.Amount) * 100));//订单总金额，单位为分
            data.SetValue("time_start", DateTime.Now.ToString("yyyyMMddHHmmss"));//交易起始时间
            data.SetValue("time_expire", DateTime.Now.AddMinutes(10).ToString("yyyyMMddHHmmss"));//交易结束时间
            data.SetValue("goods_tag", "jjj");//商品标记
            data.SetValue("trade_type", "NATIVE");//交易类型
            data.SetValue("product_id", _order.ProductCode);//商品ID

            WxPayData wxPayData = WxPayApi.UnifiedOrder(data, WxPayConfig.GetConfig().GetAppID());//调用统一下单接口
            PaymentResult result = new PaymentResult();
            if ("FAIL".Equals(wxPayData.GetValue("return_code")?.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw new Exception("支付失败");
            }
            else
            {
                result.Code = wxPayData.GetValue("return_code")?.ToString();
                result.Msg = wxPayData.GetValue("return_msg")?.ToString();
                result.Content = wxPayData.GetValue("code_url").ToString();//获得统一下单接口返回的二维码链接
            }

            return result;
        }

        /// <summary>
        /// H5支付
        /// </summary>
        /// <returns></returns>
        public PaymentResult PayWap()
        {
            WxPayData data = new WxPayData();
            data.SetValue("body", _order.Name);//商品名称
            data.SetValue("detail", _order.Body);//商品描述
            data.SetValue("attach", "专一网");//附加数据
            data.SetValue("out_trade_no", _order.OrderNo);//订单号
            data.SetValue("total_fee", (int)(decimal.Parse(_order.Amount) * 100));//订单总金额，单位为分
            data.SetValue("time_start", DateTime.Now.ToString("yyyyMMddHHmmss"));//交易起始时间
            data.SetValue("time_expire", DateTime.Now.AddMinutes(10).ToString("yyyyMMddHHmmss"));//交易结束时间
            data.SetValue("goods_tag", "WXG");//商品标记
            data.SetValue("trade_type", "MWEB");//交易类型
            data.SetValue("product_id", _order.ProductCode);//商品ID
            data.SetValue("scene_info", "{\"h5_info\": {\"type\":\"Wap\",\"wap_url\": \"http://www.chinazdap.com/\",\"wap_name\": \"云上实习\"}}");

            WxPayData wxPayData = WxPayApi.UnifiedOrder(data, WxPayConfig.GetConfig().GetAppID());//调用统一下单接口
            PaymentResult result = new PaymentResult();
            if ("FAIL".Equals(wxPayData.GetValue("return_code")?.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw new Exception("支付失败");
            }
            else
            {
                result.Code = wxPayData.GetValue("return_code")?.ToString();
                result.Msg = wxPayData.GetValue("return_msg")?.ToString();
                result.Content = wxPayData.GetValue("mweb_url").ToString() + "&redirect_url=" + HttpUtility.UrlEncode(WxPayConfig.GetConfig().GetWapReturnUrl());//获得统一下单接口返回的二维码链接
            }

            return result;
        }

        /// <summary>
        /// APP支付
        /// </summary>
        /// <returns></returns>
        public PaymentWechatResult PayApp(string appId)
        {
            WxPayData data = new WxPayData();
            data.SetValue("body", "专一网 - " + _order.Name);//商品名称
            data.SetValue("detail", _order.Body);//商品描述
            data.SetValue("attach", "专一网");//附加数据
            data.SetValue("out_trade_no", _order.OrderNo);//订单号
            data.SetValue("total_fee", (int)(decimal.Parse(_order.Amount) * 100));//订单总金额，单位为分
            data.SetValue("time_start", DateTime.Now.ToString("yyyyMMddHHmmss"));//交易起始时间
            data.SetValue("time_expire", DateTime.Now.AddMinutes(10).ToString("yyyyMMddHHmmss"));//交易结束时间
            data.SetValue("goods_tag", "WXG");//商品标记
            data.SetValue("trade_type", "APP");//交易类型
            data.SetValue("product_id", _order.ProductCode);//商品ID

            WxPayData wxPayData = WxPayApi.UnifiedOrder(data, appId);//调用统一下单接口
            PaymentWechatResult result = new PaymentWechatResult();
            if ("FAIL".Equals(wxPayData.GetValue("return_code")?.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw new Exception("支付失败");
            }
            else
            {
                TimeSpan ts1 = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
                var rTimestamp = Convert.ToInt64(ts1.TotalSeconds).ToString();
                result.Code = wxPayData.GetValue("return_code")?.ToString();
                result.Msg = wxPayData.GetValue("return_msg")?.ToString();

                result.AppId = wxPayData.GetValue("appid").ToString();
                result.MchId = wxPayData.GetValue("mch_id").ToString();
                result.NonceStr = wxPayData.GetValue("nonce_str").ToString();
                result.PrepayId = wxPayData.GetValue("prepay_id").ToString();//获得统一下单接口返回的预支付交易会话标识
                result.Timestamp = rTimestamp;

                WxPayData wxPaySignData = new WxPayData();
                wxPaySignData.SetValue("appid", appId);//应用ID
                wxPaySignData.SetValue("partnerid", WxPayConfig.GetConfig().GetMchID());//商户号
                wxPaySignData.SetValue("prepayid", wxPayData.GetValue("prepay_id").ToString());//预支付交易会话标识
                wxPaySignData.SetValue("noncestr", wxPayData.GetValue("nonce_str").ToString());//随机字符串
                wxPaySignData.SetValue("timestamp", rTimestamp);
                wxPaySignData.SetValue("package", "Sign=WXPay");
                result.Sign = wxPaySignData.MakeSign().ToUpper();
            }

            return result;
        }
        /// <summary>
        /// 小程序支付
        /// </summary>
        /// <returns></returns>
        public PaymentWechatResult PayJsapi(string openId)
        {
            WxPayData data = new WxPayData();
            data.SetValue("body", "专一网 - " + _order.Name);//商品名称
            data.SetValue("detail", _order.Body);//商品描述
            data.SetValue("attach", "专一网");//附加数据
            data.SetValue("out_trade_no", _order.OrderNo);//订单号
            data.SetValue("total_fee", (int)(decimal.Parse(_order.Amount) * 100));//订单总金额，单位为分
            data.SetValue("time_start", DateTime.Now.ToString("yyyyMMddHHmmss"));//交易起始时间
            data.SetValue("time_expire", DateTime.Now.AddMinutes(10).ToString("yyyyMMddHHmmss"));//交易结束时间
            data.SetValue("goods_tag", "WXG");//商品标记
            data.SetValue("trade_type", "JSAPI");//交易类型
            data.SetValue("product_id", _order.ProductCode);//商品ID
            data.SetValue("openid", openId);

            WxPayData wxPayData = WxPayApi.UnifiedOrder(data, WxPayConfig.GetConfig().GetMiniProgramsAppID());//调用统一下单接口
            PaymentWechatResult result = new PaymentWechatResult();
            if ("FAIL".Equals(wxPayData.GetValue("return_code")?.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw new Exception("支付失败");
            }
            else
            {
                if ("FAIL".Equals(wxPayData.GetValue("result_code")?.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    result.Code = wxPayData.GetValue("result_code")?.ToString();
                    result.Msg = wxPayData.GetValue("err_code_des")?.ToString();
                }
                else
                {
                    TimeSpan ts1 = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
                    var rTimestamp = Convert.ToInt64(ts1.TotalSeconds).ToString();
                    result.Code = wxPayData.GetValue("return_code")?.ToString();
                    result.Msg = wxPayData.GetValue("return_msg")?.ToString();

                    result.AppId = wxPayData.GetValue("appid").ToString();
                    result.MchId = wxPayData.GetValue("mch_id").ToString();
                    result.NonceStr = wxPayData.GetValue("nonce_str").ToString();
                    result.PrepayId = wxPayData.GetValue("prepay_id").ToString();//获得统一下单接口返回的预支付交易会话标识
                    result.Timestamp = rTimestamp;

                    WxPayData wxPaySignData = new WxPayData();
                    wxPaySignData.SetValue("appId", WxPayConfig.GetConfig().GetMiniProgramsAppID());//应用ID
                    wxPaySignData.SetValue("nonceStr", result.NonceStr);//商户号
                    wxPaySignData.SetValue("package", "prepay_id="+wxPayData.GetValue("prepay_id").ToString());//预支付交易会话标识
                    wxPaySignData.SetValue("signType", "HMAC-SHA256");
                    wxPaySignData.SetValue("timeStamp", rTimestamp);
                    result.Sign = wxPaySignData.MakeSign().ToUpper();
                }
            }

            return result;
        }
        /// <summary>
        /// 微信分账
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public PaymentResult ProfitSharing(ProfitSharingOrder order)
        {
            PaymentResult result = new PaymentResult();
            if ("Y".Equals(WxPayConfig.GetConfig().GetProfitSharing(), StringComparison.OrdinalIgnoreCase))
            {
                WxPayData data = new WxPayData();
                data.SetValue("transaction_id", order.TransactionId);
                data.SetValue("out_order_no", order.OrderNo);
                data.SetValue("receivers", order.Receivers);
                WxPayData wxPayData = WxPayApi.ProfitSharing(data);

                if ("SUCCESS".Equals(wxPayData.GetValue("return_code")?.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if ("SUCCESS".Equals(wxPayData.GetValue("result_code")?.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        result.Code = wxPayData.GetValue("result_code").ToString();
                        result.Content = wxPayData.GetValue("order_id").ToString();
                    }
                    else
                    {
                        // 分账失败
                        result.Code = wxPayData.GetValue("err_code").ToString();
                        result.Msg = wxPayData.GetValue("err_code_des").ToString();
                    }
                }
                else
                {
                    // 通信失败
                    result.Code = wxPayData.GetValue("return_code").ToString();
                    result.Msg = wxPayData.GetValue("return_msg").ToString();
                }
            }
            else
            {
                result.Msg = "未启用分账标识";
            }
            return result;
        }
    }
}
