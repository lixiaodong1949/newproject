﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Tas.Common.Configurations;

namespace Yssx.Framework.Payment.Alipay
{
    /// <summary>
    /// 支付宝配置参数
    /// </summary>
    public static class AlipayConfig
    {
        public static IConfiguration Configuration;

        /// <summary>
        /// 应用ID - PC端
        /// </summary>
        public static string appId;
        /// <summary>
        /// 商户私钥，您的原始格式RSA私钥 - PC端
        /// </summary>
        public static string privateKey;
        /// <summary>
        /// 支付宝公钥 - PC端
        /// </summary>
        public static string publicKey;

        /// <summary>
        /// 应用ID - 移动端
        /// </summary>
        public static string mobileAppId;
        /// <summary>
        /// 商户私钥，您的原始格式RSA私钥 - 移动端
        /// </summary>
        public static string mobilePrivateKey;
        /// <summary>
        /// 支付宝公钥 - 移动端
        /// </summary>
        public static string mobilePublicKey;

        /// <summary>
        /// 应用ID - 3d移动端
        /// </summary>
        public static string mobile3dAppId;
        /// <summary>
        /// 商户私钥，您的原始格式RSA私钥 - 3d移动端
        /// </summary>
        public static string mobile3dPrivateKey;
        /// <summary>
        /// 应用公钥 - 3d移动端
        /// </summary>
        public static string mobile3dPublicKey;

        /// <summary>
        /// 支付宝网关
        /// </summary>
        public static string gatewayUrl;

        /// <summary>
        /// 签名方式
        /// </summary>
        public static string signType = "RSA2";

        /// <summary>
        /// 编码格式
        /// </summary>
        public static string charset = "UTF-8";

        /// <summary>
        /// 同步回调地址
        /// </summary>
        public static string returnUrl;

        /// <summary>
        /// 异步回调地址
        /// </summary>
        public static string notifyUrl;

        /// <summary>
        /// 异步回调地址 - 移动端
        /// </summary>
        public static string mobileNotifyUrl;

        /// <summary>
        /// 异步回调地址 - 3d移动端
        /// </summary>
        public static string mobile3dNotifyUrl;

        /// <summary>
        /// 手机支付同步回调地址
        /// </summary>
        public static string wapReturnUrl;

        public static ProjectConfiguration UseAlipay(this ProjectConfiguration projectConfiguration, IConfiguration configuration)
        {
            Configuration = configuration;
            appId = Configuration["Payment:Alipay:app_id"];
            privateKey = Configuration["Payment:Alipay:private_key"];
            publicKey = Configuration["Payment:Alipay:public_key"];
            notifyUrl = Configuration["Payment:Alipay:notify_url"];
            mobileAppId = Configuration["Payment:Alipay:mobile_app_id"];
            mobilePrivateKey = Configuration["Payment:Alipay:mobile_private_key"];
            mobilePublicKey = Configuration["Payment:Alipay:mobile_public_key"];
            mobileNotifyUrl = Configuration["Payment:Alipay:mobile_notify_url"];
            mobile3dAppId = Configuration["Payment:Alipay:mobile_3d_app_id"];
            mobile3dPrivateKey = Configuration["Payment:Alipay:mobile_3d_private_key"];
            mobile3dPublicKey = Configuration["Payment:Alipay:mobile_3d_public_key"];
            mobile3dNotifyUrl = Configuration["Payment:Alipay:mobile_3d_notify_url"];
            gatewayUrl = Configuration["Payment:Alipay:gateway"];
            returnUrl = Configuration["Payment:Alipay:return_ur"];
            wapReturnUrl = Configuration["Payment:Alipay:wap_return_url"];
            return projectConfiguration;
        }
    }
}
