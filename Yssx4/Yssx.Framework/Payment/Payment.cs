﻿using Aop.Api.Domain;
using Aop.Api.Util;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Logger;
using Yssx.Framework.Payment.Alipay;
using Yssx.Framework.Payment.Webchat;

namespace Yssx.Framework.Payment
{
    /// <summary>
    /// 支付
    /// </summary>
    public class Payment
    {
        /// <summary>
        /// 支付宝Web支付
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static PaymentResult Alipay(Order order)
        {
            AlipayWebPay alipay = new AlipayWebPay(order);
            return alipay.Pay();
        }

        /// <summary>
        /// 支付宝h5支付
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static PaymentResult AlipayWap(Order order)
        {
            AlipayWebPay alipay = new AlipayWebPay(order);
            return alipay.PayWap();
        }

        /// <summary>
        /// 验证支付宝回调参数
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool CheckAlipay(Dictionary<string, string> input)
        {
            bool flag = AlipaySignature.RSACheckV1(input, AlipayConfig.publicKey, AlipayConfig.charset, AlipayConfig.signType, false);
            return flag;
        }

        /// <summary>
        /// 支付宝-云实习App支付
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static PaymentResult AlipayApp(Order order)
        {
            AlipayWebPay alipay = new AlipayWebPay(order);
            return alipay.PayApp(AlipayConfig.mobileAppId, AlipayConfig.mobileNotifyUrl, AlipayConfig.mobilePrivateKey, AlipayConfig.mobilePublicKey);
        }

        /// <summary>
        /// 支付宝-云实习3d-App支付
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static PaymentResult AlipayApp3d(Order order)
        {
            AlipayWebPay alipay = new AlipayWebPay(order);
            return alipay.PayApp(AlipayConfig.mobile3dAppId, AlipayConfig.mobile3dNotifyUrl, AlipayConfig.mobile3dPrivateKey, AlipayConfig.mobilePublicKey);
        }

        /// <summary>
        /// 验证支付宝App回调参数
        /// </summary>
        /// <param name="input"></param>
        /// <param name="appEnum">1.云实习 2.云实习3D</param>
        /// <returns></returns>
        public static bool CheckAlipayApp(Dictionary<string,string> input, int appEnum)
        {
            bool flag = false;
            if (appEnum == 1)
                flag = AlipaySignature.RSACheckV1(input, AlipayConfig.mobilePublicKey, AlipayConfig.charset, AlipayConfig.signType, false);
            if (appEnum == 2)
                flag = AlipaySignature.RSACheckV1(input, AlipayConfig.mobilePublicKey, AlipayConfig.charset, AlipayConfig.signType, false);
            return flag;
        }

        /// <summary>
        /// 微信
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static PaymentResult Webchat(Order order)
        {
            WechatWebPay webchat = new WechatWebPay(order);
            return webchat.Pay();
        }

        /// <summary>
        /// 微信H5
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static PaymentResult WebchatWap(Order order)
        {
            WechatWebPay webchat = new WechatWebPay(order);
            return webchat.PayWap();
        }

        /// <summary>
        /// 微信App
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static PaymentWechatResult WechatApp(Order order)
        {
            WechatWebPay webchat = new WechatWebPay(order);
            string appId = WxPayConfig.GetConfig().GetMobileAppID();
            return webchat.PayApp(appId);
        }
        /// <summary>
        /// 微信App - 3D云实习
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static PaymentWechatResult WechatApp3d(Order order)
        {
            WechatWebPay webchat = new WechatWebPay(order);
            string appId = WxPayConfig.GetConfig().GetMobile3dAppID();
            return webchat.PayApp(appId);
        }
        /// <summary>
        /// 微信小程序
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public static PaymentWechatResult WechatJsapi(Order order,string openId)
        {
            WechatWebPay webchat = new WechatWebPay(order);
            return webchat.PayJsapi(openId);
        }
        /// <summary>
        /// 验证微信回调参数
        /// </summary>
        /// <param name="dict"></param>
        /// <returns></returns>
        public static bool CheckWebchat(string xmlString)
        {
            WxPayData data = new WxPayData();
            try
            {
                data.FromXml(xmlString);

                if ("SUCCESS".Equals(data.GetValue("return_code")?.ToString(), StringComparison.OrdinalIgnoreCase) 
                    && "SUCCESS".Equals(data.GetValue("result_code")?.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
                else
                {
                    WxPayData res = new WxPayData();
                    res.SetValue("return_code", "FAIL");
                    res.SetValue("return_msg", data.GetValue("return_msg"));
                    CommonLogger.Error("Sign check error : " + res.ToXml());
                }
            }
            catch (Exception ex)
            {
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", ex.Message);
                CommonLogger.Error("Sign check error : " + res.ToXml());
            }

            return false;
        }
    }
}
