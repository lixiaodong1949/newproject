﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Payment
{
    /// <summary>
    /// 支付订单
    /// </summary>
    public class Order
    {
        /// <summary>
        /// 订单号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 订单名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 订单金额
        /// </summary>
        public string Amount { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// 商品编号
        /// </summary>
        public string ProductCode { get; set; }
    }

    /// <summary>
    /// 分账订单
    /// </summary>
    public class ProfitSharingOrder
    {
        /// <summary>
        /// 微信订单号
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// 商户分账单号
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 分账接收方列表
        /// </summary>
        public string Receivers { get; set; }
    }

    /// <summary>
    /// 分账接收方
    /// </summary>
    public class Receiver
    {
        /// <summary>
        /// 分账接收方类型
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// 分账接收方帐号
        /// </summary>
        [JsonProperty("account")]
        public string Account { get; set; }

        /// <summary>
        /// 分账金额
        /// </summary>
        [JsonProperty("amount")]
        public int Amount { get; set; }

        /// <summary>
        /// 分账描述
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }
    }
}
