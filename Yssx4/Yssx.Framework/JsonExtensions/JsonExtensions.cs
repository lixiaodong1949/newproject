﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Yssx.Framework
{
    public static class JsonExtensions
    {
        /// <summary>
        ///   Serializes the specified object to a JSON string.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string SerializeObject(this object o)
        {
            return JsonConvert.SerializeObject(o);

        }
        /// <summary>
        /// Deserializes the JSON to the specified .NET type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="o"></param>
        /// <returns></returns>
        public static T DeserializeObject<T>(this string o)
        {
            return JsonConvert.DeserializeObject<T>(o);

        }
    }
}
