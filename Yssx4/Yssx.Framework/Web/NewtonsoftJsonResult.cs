﻿//using System;
//using Newtonsoft.Json;

//namespace Yssx.Framework.Web
//{
//    public class NewtonsoftJsonResult : JsonResult
//    {
//        public override void ExecuteResult(ControllerContext context)
//        {
//            if (context == null)
//                throw new ArgumentNullException(nameof(context));
//            if (this.JsonRequestBehavior == JsonRequestBehavior.DenyGet && string.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
//                throw new InvalidOperationException("Get Request NotAllowed");
//            var response = context.HttpContext.Response;
//            response.ContentType = string.IsNullOrEmpty(this.ContentType) ? "application/json" : this.ContentType;
//            if (this.ContentEncoding != null)
//                response.ContentEncoding = this.ContentEncoding;
//            if (this.Data == null)
//                return;
//            var jSetting = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };

//            response.Write(JsonConvert.SerializeObject(this.Data,Formatting.Indented, jSetting));
//        }
//    }
//}
