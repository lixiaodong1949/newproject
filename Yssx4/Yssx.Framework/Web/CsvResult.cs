﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Web;
//using Yssx.Framework.Utils;

//namespace Yssx.Framework.Web
//{
//    public class CsvResult<T> : ActionResult
//    {

//        private readonly string _fileName;

//        private readonly string _content;


//        /// <summary>
//        /// 文件名
//        /// </summary>
//        public string FileName
//        {
//            get { return _fileName; }
//        }


//        /// <summary>
//        /// 内容
//        /// </summary>
//        public string Content
//        {
//            get { return _content; }
//        }




//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="items"></param>
//        /// <param name="fileName"></param>
//        public CsvResult(List<T> items, string fileName)
//        {
//            _content = CsvHelper.List2String(items);
//            _fileName = fileName;
//        } 
//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="context"></param>
//        public override void ExecuteResult(ControllerContext context)
//        {
//            WriteFile(_fileName, "text/comma-separated-values", _content);
//        }


//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="fileName"></param>
//        /// <param name="contentType"></param>
//        /// <param name="content"></param>
//        private static void WriteFile(string fileName, string contentType, string content)
//        {
//            var context = HttpContext.Current;
//            fileName = HttpUtility.UrlEncode(fileName, Encoding.UTF8);    //对中文文件名进行HTML转码
//            var buffer = Encoding.UTF8.GetBytes(content);
//            context.Response.ContentEncoding = Encoding.UTF8;
//            var outBuffer = new byte[buffer.Length + 3];
//            outBuffer[0] = (byte)0xEF;//有BOM,解决乱码
//            outBuffer[1] = (byte)0xBB;
//            outBuffer[2] = (byte)0xBF;
//            Array.Copy(buffer, 0, outBuffer, 3, buffer.Length);

//            var cpara = Encoding.UTF8.GetChars(outBuffer); // byte[] to char[]
//            context.Response.Clear();
//            context.Response.AddHeader("content-disposition", "attachment;filename=" + fileName+".csv");
//            context.Response.Charset = "";
//            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
//            context.Response.ContentType = contentType;
//            context.Response.Write(cpara, 0, cpara.Length);
//            context.Response.End();
//        }
//    }
//}
