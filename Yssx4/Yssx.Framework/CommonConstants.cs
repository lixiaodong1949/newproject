﻿namespace Yssx.Framework
{
    public static class CommonConstants
    {

        /// <summary>
        /// 第三方API调用错误
        /// </summary>
        public const int ErrorCode_ThirdAPI = 5000;
        /// <summary>
        /// 成功
        /// </summary>
        public const int SuccessCode = 200;
        public const int NotFund = 404;
        public const int ErrorCode = 500;
        /// <summary>
        /// 参数错误
        /// </summary>
        public const int BadRequest = 400;

        /// <summary>
        /// 已经自动交卷 
        /// </summary>
        public const int Overdue = 402;
        /// <summary>
        ///  未登录
        /// </summary>       
        public const int Unauthorized = 401;
        /// <summary>
        /// 未授权
        /// </summary>
        public const int Forbidden = 403;
        /// <summary>
        /// 未删除
        /// </summary>
        public const int IsNotDelete = 0;
        /// <summary>
        /// 已删除
        /// </summary>
        public const int IsDelete = 1;

        /// <summary>
        /// 否
        /// </summary>
        public const int IsNo = 0;
        /// <summary>
        /// 是
        /// </summary>
        public const int IsYes = 1;

        /// <summary>
        /// 已压缩
        /// </summary>
        public const int IsGzip = 1;
        /// <summary>
        /// 未压缩
        /// </summary>
        public const int IsNoGzip = 0;

        /// <summary>
        /// 可复制
        /// </summary>
        public const int IsCopy = 1;
        /// <summary>
        /// 不可复制
        /// </summary>
        public const int IsNoCopy = 0;
        /// <summary>
        /// 本年利润科目名称
        /// </summary>
        public const string ProfitSubjectName = "本年利润";

        /// <summary>
        /// 无序
        /// </summary>
        public const int IsDisorder = 1;
        /// <summary>
        /// 有序
        /// </summary>
        public const int IsNoDisorder = 0;

        /// <summary>
        /// 未审核
        /// </summary>
        public const int IsNoApproval = 0;
        /// <summary>
        /// 已审核
        /// </summary>
        public const int IsApproval = 1;

        /// <summary>
        /// 会计主管岗位id
        /// </summary>
        public const long AccountingManager = 931254353051679;

        /// <summary>
        /// 财务主管岗位id
        /// </summary>
        public const long FinanceManager = 935994308264025;
        /// <summary>
        /// 出纳岗位Id
        /// </summary>
        public const long Cashier = 931254034284571;
        /// <summary>
        /// 大数据分析师岗位Id
        /// </summary>
        public const long BigDataPosition = 934156270338053;
        /// <summary>
        /// 不可看
        /// </summary>
        public const int IsNotLook = 0;

        /// <summary>
        /// 普通用户 - 默认学校【中国测试大学】
        /// </summary>
        public const long DefaultTenantId = 1017666019115035;

        /// <summary>
        /// 游客 - 默认用户ID
        /// </summary>
        public const long DefaultUserId = 1054708947419999;

        /// <summary>
        /// X学院 - 默认学校
        /// </summary>
        public const long OneXTenantId = 1078977911537665;

        #region 缓存常量

        public const string Cache_GetCaseById = "Cache:GetCaseById:";

        public const string Cache_GetQuestionCountByCourseId = "Cache:GetQuestionCountByCourseId:";

        public const string Cache_GetPositionById = "Cache:GetPositionById:";

        public const string Cache_GetCasePositionsByCaseId = "Cache:GetCasePositionsByCaseId:";

        public const string Cache_GetCasePosition = "Cache:GetCasePosition:";

        public const string Cache_GetExamById = "Cache:GetExamById:";

        public const string Cache_GetExamCourseById = "Cache:GetExamCourseById:";

        public const string Cache_GetExamCourseByCourseSectionId = "Cache:GetExamCourseByCourseSectionId:";

        public const string Cache_GetExamCourseByTaskId = "Cache:GetExamCourseByTaskId:";

        public const string Cache_GetExamCourseByPCourseSectionId = "Cache:GetExamCourseByPCourseSectionId:";

        public const string Cache_GetTopicById = "Cache:GetTopicById:";

        public const string Cache_GetPublicTopicById = "Cache:GetPublicTopicById:";

        public const string Cache_GetSubTopicByParentId = "Cache:GetSubTopicByParentId:";

        public const string Cache_GetSubPublicTopicByParentId = "Cache:GetSubPublicTopicByParentId:";

        public const string Cache_GetTopicPostionsByCaseId = "Cache:GetTopicPostionsByCaseId:";

        public const string Cache_GetTopicBySectionId = "Cache:GetTopicBySectionId:";

        public const string Cache_GetTopicByTaskId = "Cache:GetTopicByTaskId:";

        public const string Cache_GetTopicByCourseTaskId = "Cache:GetTopicByCourseTaskId:";

        public const string Cache_GetPositionTopicCountByCaseId = "Cache:GetPositionTopicCountByCaseId:";

        public const string Cache_GetTopicFilesByTopicId = "Cache:GetTopicFilesByTopicId:";

        public const string Cache_GetPublicTopicFilesByTopicId = "Cache:GetPublicTopicFilesByTopicId:";

        public const string Cache_GetOptionsByTopicId = "Cache:GetOptionsByTopicId:";

        public const string Cache_GetPublicOptionsByTopicId = "Cache:GetPublicOptionsByTopicId:";

        public const string Cache_GetCertificateTopicByTopicId = "Cache:GetCertificateTopicByTopicId:";

        public const string Cache_GetCertificateTopicByTopicIdAll = "Cache:GetCertificateTopicViewByTopicIdAll:";

        public const string Cache_GetCertificateTopicPublicByTopicId = "Cache:GetCertificateTopicPublicByTopicId:";

        public const string Cache_GetCertificateTopicPublicByTopicIdAll = "Cache:GetCertificateTopicPublicByTopicIdAll:";

        public const string Cache_GetProExamListByTaskId = "Cache:GetProExamListByTaskId:";

        public const string Cache_GetCertificateListByCaseId = "Cache:GetCertificateListByCaseId:";

        public const string Cache_GetAssistacListByCaseId = "Cache:GetAssistacListByCaseId:";


        public const string Cache_GetOnlineTaskByTaskId = "Cache:GetTaskFristById:";

        public const string Cache_GetExamPaperCourseById = "Cache:GetExamPaperCourseById:";

        public const string Cache_GetTaskStudentsByTaskId = "Cache:GetTaskStudentsByTaskId:";

        public const string Cache_GetCourseSummaryById = "Cache:GetCourseSummaryById:";

        public const string Cache_GetExamCourseUserGradeById = "Cache:GetExamCourseUserGradeById:";

        public const string Cache_GetExamCourseTaskQuestionById = "Cache:GetExamCourseTaskQuestionById:";

        public const string Cache_GetExamCourseGradeDetailById = "Cache:GetExamCourseGradeDetailById:";

        public const string Cache_GetCourseSummary = "Cache:GetCourseSummary:";

        public const string Cache_GetSchoolById = "Cache:GetSchoolById:";

        public const string Cache_GetUserByMobile = "Cache:GetUserByMobile:";

        public const string Cache_GetTaskById = "Cache:GetTaskById:";

        public const string Cache_GetTaskGradeByTaskId = "Cache:GetTaskGradeById:";

        public const string Cache_GetTaskTopicByTaskId = "Cache:GetTaskTopicByTaskId:";

        public const string Cache_GetTaskErrorTopicByTaskId = "Cache:GetTaskErrorTopicByTaskId:";

        public const string Cache_GetGradeReportByTaskId = "Cache:GetGradeReportByTaskId:";

        public const string Cache_GetHomeAccountantList = "Cache:GetHome:AccountantList";

        public const string Cache_GetHomeCaseList = "Cache:GetHome:HomeCaseList";

        public const string Cache_GetHomeCourseList = "Cache:GetHome:HomeCourseList";

        public const string Cache_GetCourseById = "Cache:GetHome:GetCourseById:";

        public const string Cache_GetHomeSceneList = "Cache:GetHome:HomeSceneList";

        public const string Cache_GetHomeCaseById = "Cache:GetHomeCaseById:";

        public const string Cache_GetUploadCaseTagLibraryList = "Cache:GetUploadCaseTagLibraryList:";

        public const string Cache_GetUploadCasePreview = "Cache:GetUploadCasePreview:";

        public const string Cache_GetUploadCaseGoodArticle = "Cache:GetUploadCaseGoodArticle:";

        public const string Cache_GetNoUserUploadCase = "Cache:GetNoUserUploadCase:"; 

        public const string Cache_GetNoUserUploadCaseList = "Cache:GetNoUserUploadCaseList:";

        public const string Cache_GetUploadCaseList = "Cache:GetGetUploadCaseList:";

        public const string Cache_GetUserCountByPage = "Cache:GetUserCountByPage:";

        public const string Cache_GetGradeByUserIdExamId = "Cache:GetGradeByUserIdExamId:";

        /// <summary>
        /// 备课课程章节
        /// </summary>
        public const string Cache_GetPrepareCourseSection = "Cache:GetPrepareCourseSection:";

        /// <summary>
        /// 备课课程节教材详情
        /// </summary>
        public const string Cache_GetPrepareCourseSectionTextBook = "Cache:GetPrepareCourseSectionTextBook:";

        /// <summary>
        /// 试卷作答记录
        /// </summary>
        public const string Cache_GetExamAnswerRecordByExamId = "Cache:GetExamAnswerRecordByExamId:";

        /// <summary>
        /// 已交学生试卷作答记录
        /// </summary>
        public const string Cache_GetHaveHandExamAnswerRecordByExamId = "Cache:GetHaveHandExamAnswerRecordByExamId:";

        /// <summary>
        /// 未交学生试卷作答记录
        /// </summary>
        public const string Cache_GetNotHandExamAnswerRecordByExamId = "Cache:GetNotHandExamAnswerRecordByExamId:";

        /// <summary>
        /// 学生试卷作答记录
        /// </summary>
        public const string Cache_GetStudentExamAnswerRecordByExamId = "Cache:GetStudentExamAnswerRecordByExamId:";

        /// <summary>
        /// 学生作答记录明细
        /// </summary>
        public const string Cache_GetStudentAnswerRecordDetailByExamId = "Cache:GetStudentAnswerRecordDetailByExamId:";

        /// <summary>
        /// 岗位实训任务
        /// </summary>
        public const string Cache_GetJobTrainingTaskEntityByTaskId = "Cache:GetJobTrainingTaskEntityByTaskId:";

        /// <summary>
        /// 试卷列表
        /// </summary>
        public const string Cache_GeExamPaperListByJobTrainingTaskId = "Cache:GeExamPaperListByJobTrainingTaskId:";

        /// <summary>
        /// 用户实训列表
        /// </summary>
        public const string Cache_GetHomeUserTrainingList = "Cache:GetUser:UserTrainingList:";

        /// <summary>
        /// 用户业务场景实训
        /// </summary>
        public const string Cache_GetHomeUserSecneTrainingList = "Cache:GetUser:UserSecneTrainingList:";

        /// <summary>
        /// 用户课程实训
        /// </summary>
        public const string Cache_GetHomeUserCourseList = "Cache:GetUser:UserCourseList:";

        /// <summary>
        /// 用户课堂测验、课程任务
        /// </summary>
        public const string Cache_GetHomeUserTestTaskList = "Cache:GetUser:UserTestTaskList:";

        /// <summary>
        /// 用户岗位实训任务
        /// </summary>
        public const string Cache_GetHomeUserJobTrainingTaskList = "Cache:GetUser:UserJobTrainingTaskList:";

        /// <summary>
        /// 微店 access_token
        /// </summary>
        public const string Cache_WeiDianAccessToken = "Cache:GetWeiDianAccessToken:";

        #region 积分相关缓存

        public const string Cache_GetIntegralProductBaseListPage = "Cache:IntegralProduct:BaseListPage";
        public const string Cache_GetIntegralProductInfo = "Cache:IntegralProduct:Info:";
        public const string Cache_GetIntegralTop = "Cache:Integral:GetTop:";
        /// <summary>
        /// 用户兑换商品lock
        /// </summary>
        public const string Lock_IntegralExchangeProduct = "Integral:ExchangeProduct:";

        #endregion

        #endregion

        /// <summary>
        /// 临时使用
        /// </summary>
        public const long StudentId = 1;

        public const string ImMessagePre = "ImMessage:Groups:";

        public const string ExamMaxGroupNo = "ExamPaper:MaxGroupNo:";

        public const string BigDataPositionStatus = "BigDataPosition:BigDataPositionStatus:";

        /// <summary>
        /// ClaimKey
        /// </summary>
        public const string ClaimsClientId = "clientId";
        public const string ClaimsRealName = "realName";
        public const string ClaimsTenantId = "tenantId";
        public const string ClaimsUserType = "userType";
        public const string ClaimsLoginTimespan = "timestamp";
        public const string ClaimsTaskId = "taskId";
        public const string LoginType = "loginType";
        public const string SchoolType = "schoolType";
        public const string Account = "account";
        public const string SchoolName = "schoolName";
        public const string IsVip = "isVip";

        /// <summary>
        /// 
        /// </summary>
        public const string IdentServerSecret = "Yunshangshixun_123~#$%#%^321";
        /// <summary>
        /// 新用户默认密码
        /// </summary>
        public const string NewUserDefaultPwd = "123456";
        /// <summary>
        /// cors
        /// </summary>
        public const string AnyOrigin = "AnyOrigin";
        /// <summary>
        /// 
        /// </summary>
        public const string ContextUserPropertyKey = "REQUEST_USER";

        public const string ClaimsIdNumber = "idNumber";

        #region 云上实习

        /// <summary>
        /// 会计主管岗位id
        /// </summary>
        public const long AccountingManager_C = 1054798843936770; //1111802346455080 （测试） 1054798843936770(线上)
        /// <summary>
        /// 财务主管岗位id
        /// </summary>
        public const long FinanceManager_C = 1054798843936770;
        /// <summary>
        /// 出纳岗位Id
        /// </summary>
        public const long Cashier_C = 1054799280668673;//1111802346455080 （测试） 1054799280668673(线上)

        /// <summary>
        /// 会计主管岗位id 3D
        /// </summary>
        //public const long AccountingManager_C3D = 1111803835433004;
        /// <summary>
        /// 财务主管岗位id
        /// </summary>
        public const long FinanceManager_C3D = 1111803835433004;
        /// <summary>
        /// 出纳岗位id 3D
        /// </summary>
        public const long Cashier_C3D = 1111804049342509;

        public const string Cache_GetExamById_C = "Cache:GetExamById_C:";
        public const string Cache_GetTopicFilesByTopicId_C = "Cache:GetTopicFilesByTopicId_C:";
        public const string Cache_GetTopicById_C = "Cache:GetTopicById_C:";
        public const string Cache_GetCertificateTopicByParentId_C = "Cache:GetCertificateTopicByParentId_C:";
        public const string Cache_GetOptionsByTopicId_C = "Cache:GetOptionsByTopicId_C:";
        public const string Cache_GetSubTopicByParentId_C = "Cache:GetSubTopicByParentId_C:";
        public const string Cache_GetCertificateTopicByTopicId_C = "Cache:GetCertificateTopicByTopicId_C:";
        public const string Cache_GetCertificateTopicAllByTopicId_C = "Cache:GetCertificateTopicAllByTopicId_C:";
        public const string Cache_GetTopicPostionsByCaseId_C = "Cache:GetTopicPostionsByCaseId_C:";
        /// <summary>
        /// 任务Id下面的所有题目
        /// </summary>
        public const string Cache_GetTopicByTaskId_C = "Cache:GetTopicByTaskId_C:";
        /// <summary>
        /// 案例信息
        /// </summary>
        public const string Cache_GetCaseById_C = "Cache:GetCaseById_C:";
        /// <summary>
        /// 行业信息
        /// </summary>
        public const string Cache_GetIndustryById_C = "Cache:GetIndustryById_C:";
        
        /// <summary>
        /// 获取考试关联的案例信息
        /// </summary>
        public const string Cache_GetExamCaseInfoByCaseId_C = "Cache:GetCaseById_C:";

        /// <summary>
        /// 获取3D场景列表
        /// </summary>
        public const string Cache_GetGameSceneList_C3D = "Cache:GetGameSceneList_C3D";
        /// <summary>
        /// 获取案例的所有任务的试卷
        /// </summary>
        public const string Cache_GetGameExamPapers_C3D = "Cache:GetGameExamPapers_C3D:";
        

        /// <summary>
        /// 获取3D场景基本信息
        /// </summary>
        public const string Cache_GetGameSceneBaseInfo_C3D = "Cache:GetGameSceneBaseInfo_C3D:";
        /// <summary>
        /// 获取3D场景 题目列表
        /// </summary>
        public const string Cache_GetGameTopicList_C3D = "Cache:GetGameTopicList_C3D:";

        /// <summary>
        /// 任务Id下面的所有题目
        /// </summary>
        public const string Cache_GetGameLeveLGradeTopicList_C3D = "Cache:GetGameLeveLGradeTopicList_C3D:";
        /// <summary>
        /// 用户技能
        /// </summary>
        //public const string Cache_GetUserSkills_C3D = "Cache:GetUserSkills_C3D:";
        /// <summary>
        /// 技能
        /// </summary>
        public const string Cache_GetSkills_C3D = "Cache:GetSkills_C3D:";

        public const string Cache_GetBanner = "Cache:Banner:";

        #endregion

    }
}
