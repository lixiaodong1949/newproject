﻿using System;
using System.Collections.Generic;
using System.Text;
using Tas.Common.Configurations;

namespace Yssx.Framework.IM
{
    public static class ImClientConfiguration
    {
        public static ProjectConfiguration UseImClient(this ProjectConfiguration projectConfiguration, string redisConnectionString, string[] servers)
        {
            ImHelper.Initialization(new ImClientOptions
            {
                Redis = new CSRedis.CSRedisClient(redisConnectionString),
                Servers = servers
            });

            ImHelper.EventBus(
                online =>
                {
                    Console.WriteLine(online.clientId + "上线了");
                },
                offline =>
                {
                    Console.WriteLine(offline.clientId + "下线了");
                });

            return projectConfiguration;
        }
    }
}
