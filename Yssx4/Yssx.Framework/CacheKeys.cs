﻿namespace Yssx.Framework
{
    /// <summary>
    /// 
    /// </summary>
    public static class CacheKeys
    {  
        ///// <summary>
        ///// 用户token
        ///// </summary>
        //public const string UserLoginToken = "Users:Tokens:{0}";
        /// <summary>
        /// 用户token
        /// </summary>
        public const string UserLoginTokenHashKey = "Hash:Users:LoginTokens";

        /// <summary>
        /// 微信Token
        /// </summary>
        public const string WeiXinTokenHashKey = "Hash:WeiXin:Token";

        /// <summary>
        /// 试算平衡
        /// </summary>
        public const string BeginningStatusHashKey = "Hash:CoreQuestion:BeginningStatus";

    }
}
