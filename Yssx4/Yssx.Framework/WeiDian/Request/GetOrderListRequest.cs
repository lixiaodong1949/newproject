﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.WeiDian
{
    /// <summary>
    /// 获取订单列表请求对象
    /// </summary>
    public class GetOrderListRequest
    {
        public GetOrderListRequest()
        {
            // 默认查询参数
            IsWeiOrder = OrderListType.All;
            PageNum = 1;
            OrderType = OrderType.finish;
            PageSize = 30;
            AddStart = DateTime.Now.AddMonths(-1);
            AddEnd = DateTime.Now;
            UpdateStart = DateTime.Now.AddMonths(-1);
            UpdateEnd = DateTime.Now;
            GroupType = 1;
        }

        /// <summary>
        /// 列表类型，0表示全部订单，1表示作为供货商的微订单，2表示普通订单
        /// </summary>
        [JsonProperty(PropertyName = "is_wei_order")]
        public OrderListType IsWeiOrder { get; set; }

        /// <summary>
        /// 订单翻页，初始页为1
        /// </summary>
        [JsonProperty(PropertyName = "page_num")]
        public int PageNum { get; set; }

        /// <summary>
        /// 单页条数，必填
        /// </summary>
        [JsonProperty(PropertyName = "page_size")]
        public int PageSize { get; set; }

        /// <summary>
        /// 订单单类型，必填
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(PropertyName = "order_type")]
        public OrderType OrderType { get; set; }

        /// <summary>
        /// 订单创建时间段的开始时间
        /// </summary>
        [JsonConverter(typeof(DateTimeFormat))]
        [JsonProperty(PropertyName = "add_start")]
        public DateTime AddStart { get; set; }

        /// <summary>
        /// 订单创建时间段的结束时间
        /// </summary>
        [JsonConverter(typeof(DateTimeFormat))]
        [JsonProperty(PropertyName = "add_end")]
        public DateTime AddEnd { get; set; }

        /// <summary>
        /// 订单更新时间段的开始时间
        /// </summary>
        [JsonConverter(typeof(DateTimeFormat))]
        [JsonProperty(PropertyName = "update_start")]
        public DateTime UpdateStart { get; set; }

        /// <summary>
        /// 订单更新时间段的结束时间
        /// </summary>
        [JsonConverter(typeof(DateTimeFormat))]
        [JsonProperty(PropertyName = "update_end")]
        public DateTime UpdateEnd { get; set; }

        /// <summary>
        /// 1不包含微团购未成团订单，0包含，默认是1
        /// </summary>
        [JsonProperty(PropertyName = "group_type")]
        public int GroupType { get; set; }
    }

    public class DateTimeFormat : IsoDateTimeConverter
    {
        public DateTimeFormat()
        {
            base.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        }
    }

    /// <summary>
    /// 订单列表类型
    /// </summary>
    public enum OrderListType
    {
        /// <summary>
        /// 全部订单
        /// </summary>
        All,
        /// <summary>
        /// 作为供货商的微订单
        /// </summary>
        Micro,
        /// <summary>
        /// 普通订单
        /// </summary>
        Ordinary
    }

    /// <summary>
    /// 订单类型
    /// </summary>
    public enum OrderType
    {
        /// <summary>
        /// 待发货
        /// </summary>
        unship,
        /// <summary>
        /// 待付款
        /// </summary>
        unpay,
        /// <summary>
        /// 已发货
        /// </summary>
        shiped,
        /// <summary>
        /// 退款中
        /// </summary>
        refunding,
        /// <summary>
        /// 已完成
        /// </summary>
        finish,
        /// <summary>
        /// 已关闭
        /// </summary>
        close,
        /// <summary>
        /// 全部
        /// </summary>
        all
    }
}
