﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.WeiDian
{
    public class GetOrderRequest
    {
        /// <summary>
        /// 订单ID
        /// </summary>
        [JsonProperty(PropertyName = "order_id")]
        public string OrderId { get; set; }
    }
}
