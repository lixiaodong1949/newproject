﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.WeiDian
{
    /// <summary>
    /// API 公共参数
    /// </summary>
    public class CommonArgs
    {
        /// <summary>
        /// API 方法名
        /// </summary>
        [JsonProperty(PropertyName = "method")]
        public string Method { get; set; }

        /// <summary>
        /// access_token
        /// </summary>
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// API 版本
        /// </summary>
        [JsonProperty(PropertyName = "version")]
        public string Version { get; set; }
    }
}
