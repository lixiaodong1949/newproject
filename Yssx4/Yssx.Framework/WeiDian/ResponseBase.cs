﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.WeiDian
{
    /// <summary>
    /// 微店 API 响应对象基类
    /// </summary>
    public class ResponseBase
    {
        /// <summary>
        /// 状态
        /// </summary>
        [JsonProperty(PropertyName = "status")]
        public ResponseStatus Status { get; set; }
    }

    /// <summary>
    /// 响应状态
    /// </summary>
    public class ResponseStatus
    {
        /// <summary>
        /// 状态代码
        /// </summary>
        [JsonProperty(PropertyName = "status_code")]
        public string StatusCode { get; set; }

        /// <summary>
        /// 状态说明
        /// </summary>
        [JsonProperty(PropertyName = "status_reason")]
        public string StatusReason { get; set; }
    }
}
