﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.WeiDian
{
    /// <summary>
    /// 获取凭证请求响应
    /// </summary>
    public class GetTokenResponse : ResponseBase
    {
        /// <summary>
        /// 响应结果
        /// </summary>
        [JsonProperty(PropertyName = "result")]
        public Token Result { get; set; }
    }

    /// <summary>
    /// 凭证
    /// </summary>
    public class Token
    {
        /// <summary>
        /// 获取到的凭证
        /// </summary>
        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// 凭证有效时间，单位：秒
        /// </summary>
        [JsonProperty(PropertyName = "expires_in")]
        public int ExpiresIn { get; set; }
    }
}
