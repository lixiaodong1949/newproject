﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.Framework
{
    #region 公共
    /// <summary>
    /// 注册系统
    /// </summary>
    public enum RegisterSourceEnum
    {
        /// <summary>
        /// 云上实训
        /// </summary>
        Yssx = 1,
        /// <summary>
        /// 技能抽查
        /// </summary>
        Jncc = 2,
        /// <summary>
        /// 竞赛
        /// </summary>
        Js = 3,
        /// <summary>
        /// 1+X考证
        /// </summary>
        OnePlusX = 4
    }
    public enum Sex
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("未知")]
        None = -1,
        /// <summary>
        /// 
        /// </summary>
        [Description("男")]
        Man = 0,
        /// <summary>
        /// 
        /// </summary>
        [Description("女")]
        Woman = 1
    }

    /// <summary>
    /// 用户类型
    /// </summary>
    public enum UserTypeEnums
    {
        /// <summary>
        /// 管理员
        /// </summary>
        Admin = 0,
        /// <summary>
        /// 学生
        /// </summary>
        Student = 1,
        /// <summary>
        /// 教师
        /// </summary>
        Teacher = 2,
        /// <summary>
        /// 教务
        /// </summary>
        Educational = 3,
        /// <summary>
        /// 专家
        /// </summary>
        Specialist = 4,

        /// <summary>
        /// 开发
        /// </summary>
        Devloper = 5,

        /// <summary>
        /// 专业组
        /// </summary>
        Professional = 6,

        /// <summary>
        /// 普通用户
        /// </summary>
        OrdinaryUser = 7,
    }
    public enum CaseUserTypeEnums
    {
        /// <summary>
        /// 教师
        /// </summary>
        Teacher = 0,
        /// <summary>
        /// 教务
        /// </summary>
        Educational = 1
    }
    /// <summary>
    /// 题目类型
    /// </summary>
    public enum QuestionType
    {
        /// <summary>
        /// 单选题
        /// </summary>
        [Description("单选题")]
        SingleChoice = 0,
        /// <summary>
        /// 多选题
        /// </summary>
        [Description("多选题")]
        MultiChoice = 1,
        /// <summary>
        /// 判断题
        /// </summary>
        [Description("判断题")]
        Judge = 2,
        /// <summary>
        /// 表格题
        /// </summary>
        [Description("表格题")]
        FillGrid = 3,
        /// <summary>
        /// 分录题
        /// </summary>
        [Description("分录题")]
        AccountEntry = 4,
        /// <summary>
        /// 填空题
        /// </summary>
        [Description("填空题")]
        FillBlank = 5,
        /// <summary>
        /// 图表题
        /// </summary>
        [Description("图表题")]
        FillGraphGrid = 6,
        /// <summary>
        /// 综合题(一个题干内含多个子题目,真岗实训财务分析题)
        /// </summary>
        [Description("综合题")]
        MainSubQuestion = 7,

        /// <summary>
        /// 结账题
        /// </summary>
        [Description("结账题")]
        SettleAccounts = 8,
        /// <summary>
        /// 表格填空题
        /// </summary>
        [Description("表格填空题")]
        GridFillBank = 9,
        /// <summary>
        /// 财务报表题
        /// </summary>
        [Description("财务报表题")]
        FinancialStatements = 10,
        /// <summary>
        /// 票据题
        /// </summary>
        [Description("票据题")]
        TeticketTopic = 11,
        /// <summary>
        /// 综合分录题
        /// </summary>
        [Description("综合分录题")]
        CertificateMainSubTopic = 12,
        /// <summary>
        /// 收款题
        /// </summary>
        [Description("收款题")]
        ReceiptTopic = 13,
        /// <summary>
        /// 付款题
        /// </summary>
        [Description("付款题")]
        PayTopic = 14,
        /// <summary>
        /// 结转损益题
        /// </summary>
        [Description("结转损益题")]
        ProfitLoss = 15,
        /// <summary>
        /// 分录题 综合分录题子分录题
        /// </summary>
        [Description("分录题")]
        CertifMSub_AccountEntry = 16,
        /// <summary>
        /// 申报表题
        /// </summary>
        [Description("申报表题")]
        DeclarationTpoic = 17,
        /// <summary>
        /// 财务报表纳税申报题
        /// </summary>
        [Description("财务报表纳税申报题")]
        FinancialStatementsDeclaration = 18,

        /// <summary>
        /// 不定项选择题
        /// </summary>
        [Description("不定项选择题")]
        UndefinedTermChoice = 19,

        /// <summary>
        /// 新规则多选题
        /// </summary>
        [Description("新规则多选题")]
        NewMultiChoice = 20,

        /// <summary>
        /// 财务分析题
        /// </summary>
        [Description("财务分析题")]
        FinancialAnalysis = 21,

        /// <summary>
        /// 简答题
        /// </summary>
        [Description("简答题")]
        ShortAnswerQuestion = 22,
    }
    /// <summary>
    /// 题目内容类型
    /// </summary>
    public enum QuestionContentType
    {
        /// <summary>
        /// 纯文本
        /// </summary>
        Text,
        /// <summary>
        /// 表格
        /// </summary>
        Grid,
        /// <summary>
        /// 图片
        /// </summary>
        Img,

    }
    /// <summary>
    /// 状态
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// 启用（审核）
        /// </summary>
        [Description("启用")]
        Enable = 1,
        /// <summary>
        /// 禁用（未审核）
        /// </summary>
        [Description("禁用")]
        Disable = 0
    }
    /// <summary>
    /// 考试状态
    /// </summary>
    public enum ExamStatus
    {
        /// <summary>
        /// 未开始
        /// </summary>
        [Description("未开始")]
        Wait = 0,
        /// <summary>
        /// 已开始
        /// </summary>
        [Description("已开始")]
        Started = 1,
        /// <summary>
        /// 已结束
        /// </summary>
        [Description("已结束")]
        End = 2,
    }
    public enum ExamStatusForQuery
    {
        /// <summary>
        /// 所有
        /// </summary>
        All = -1,
        /// <summary>
        /// 未开始
        /// </summary>
        [Description("未开始")]
        Wait,
        /// <summary>
        /// 已开始
        /// </summary>
        [Description("已开始")]
        Started,
        /// <summary>
        /// 已结束
        /// </summary>
        [Description("已结束")]
        End
    }
    /// <summary>
    /// 学生考试状态
    /// </summary>
    public enum StudentExamStatus
    {
        /// <summary>
        /// 未开始
        /// </summary>
        [Description("未开始")]
        Wait = 0,

        /// <summary>
        /// 已开始
        /// </summary>
        [Description("进行中")]
        Started = 1,

        /// <summary>
        /// 答题结束
        /// </summary>
        [Description("已结束")]
        End = 2,


    }

    /// <summary>
    /// 分组状态
    /// </summary>
    public enum GroupStatus
    {
        /// <summary>
        /// 已开始，等待成员加入
        /// </summary>
        [Description("已开始")]
        Started,

        /// <summary>
        /// 成员已满，等待组员准备完毕
        /// </summary>
        [Description("成员已满")]
        Full,

        /// <summary>
        /// 组员准备完毕（此时成员仍有可能退出房间，创建人也能解散）
        /// </summary>
        [Description("成员准备完毕 ")]
        AllReady,

        /// <summary>
        /// 已完成，开始考试（成员不能再退出）
        /// </summary>
        [Description("已完成")]
        Complete,

        /// <summary>
        /// 已结束
        /// </summary>
        [Description("已结束")]
        End,
    }

    /// <summary>
    /// 成绩计算方式
    /// </summary>
    public enum GradeCaculateMethod
    {
        /// <summary>
        /// 每题计算一次-每题计算一次
        /// </summary>
        EveryQuestion,
        /// <summary>
        /// 全部一次计算-一般用于正式考试
        /// </summary>
        AllQuestion
    }
    /// <summary>
    /// 考试类型
    /// </summary>
    public enum ExamType
    {
        /// <summary>
        /// 自主练习
        /// </summary>
        [Description("自主练习")]
        PracticeTest,
        /// <summary>
        /// 全真模拟
        /// </summary>
        [Description("全真模拟")]
        EmulationTest,
        /// <summary>
        /// 难点攻克
        /// </summary>
        DifficultTest,
        /// <summary>
        /// 错题重练
        /// </summary>
        [Description("错题重练")]
        ErrorTest,
        /// <summary>
        /// 校内赛
        /// </summary>
        [Description("校内赛")]
        SchoolCompetition,
        /// <summary>
        /// 省内赛
        /// </summary>
        [Description("省内赛")]
        ProvincialCompetition

    }


    /// <summary>
    /// 题目作答状态
    /// </summary>
    public enum AnswerDTOStatus
    {
        /// <summary>
        /// 未作答
        /// </summary>
        [Description("未作答")]
        NoAnswer,
        /// <summary>
        /// 已作答
        /// </summary>
        [Description("已作答")]
        Answered,
        /// <summary>
        /// 对
        /// </summary>
        [Description("对")]
        Right,
        /// <summary>
        /// 部分正确
        /// </summary>
        PartRight,
        /// <summary>
        /// 错
        /// </summary>
        [Description("错")]
        Error


    }
    /// <summary>
    /// 考试学生分配方式
    /// </summary>
    public enum StudentDistribute
    {
        /// <summary>
        /// 自动
        /// </summary>
        Auto,
        /// <summary>
        /// 手动选择
        /// </summary>
        Manual
    }
    /// <summary>
    /// 答案结果状态
    /// </summary>
    public enum AnswerResultStatus
    {
        /// <summary>
        /// 待处理--未作答
        /// </summary>
        [Description("待处理")]
        None,
        /// <summary>
        /// 处理中--正在计算答案
        /// </summary>
        [Description("处理中")]
        Pending,
        /// <summary>
        /// 对
        /// </summary>
        [Description("对")]
        Right,
        /// <summary>
        /// 半对（也认为是错题）
        /// </summary>
        [Description("半对")]
        PartRight,
        /// <summary>
        /// 错
        /// </summary>
        [Description("错")]
        Error
    }

    /// <summary>
    /// 分录题状态
    /// </summary>
    public enum AccountEntryStatus
    {
        /// <summary>
        /// 草稿
        /// </summary>
        [Description("草稿")]
        None,
        /// <summary>
        /// 出纳盖章
        /// </summary>
        [Description("出纳盖章")]
        Cashier,
        /// <summary>
        /// 会计主管盖章
        /// </summary>
        [Description("会计主管盖章")]
        AccountingManager,
        /// <summary>
        /// 审核人审核
        /// </summary>
        [Description("审核人审核")]
        Audit,
        /// <summary>
        /// 结束
        /// </summary>
        [Description("结束")]
        Over
    }

    /// <summary>
    /// 分录题审核状态
    /// </summary>
    public enum AccountEntryAuditStatus
    {
        /// <summary>
        /// 草稿
        /// </summary>
        [Description("草稿")]
        None = 0,
        /// <summary>
        /// 待核
        /// </summary>
        [Description("待核")]
        Wait = 1,
        /// <summary>
        /// 已审
        /// </summary>
        [Description("已审")]
        Audit = 2,
        /// <summary>
        /// 退回
        /// </summary>
        [Description("退回")]
        Back = 3,
        /// <summary>
        /// 拒绝
        /// </summary>
        [Description("拒绝")]
        Refuse = 4,

    }

    enum PracticeEnum
    {
        /// <summary>
        /// 练习未开始
        /// </summary>
        Init,
        /// <summary>
        /// 已开始
        /// </summary>
        Started,
        End,

    }
    /// <summary>
    /// 院校类型
    /// </summary>
    public enum SchoolType
    {
        /// <summary>
        /// 中职
        /// </summary>
        [Description("中职")]
        SecondaryCollege = 0,
        /// <summary>
        /// 高职
        /// </summary>
        [Description("高职")]
        JuniorCollege = 1,
        /// <summary>
        /// 本科
        /// </summary>
        [Description("本科")]
        UndergraduateSchool = 2

    }
    /// <summary>
    /// 赛事类型（0 个人赛 1团体赛）
    /// </summary>
    public enum CompetitionType
    {
        /// <summary>
        /// 个人赛
        /// </summary>
        IndividualCompetition = 0,
        /// <summary>
        /// 团体赛
        /// </summary>
        TeamCompetition = 1
    }
    /// <summary>
    /// 计算方式
    /// </summary>
    public enum CalculationType
    {
        /// <summary>
        /// 未知
        /// </summary>
        [Description("未知")]
        None = 0,
        /// <summary>
        /// 按行计算
        /// </summary>
        [Description("按行计算")]
        CalculatedRow = 1,
        /// <summary>
        /// 按列计算
        /// </summary>
        [Description("按列计算")]
        CalculatedColumn = 2,
        /// <summary>
        /// 按单元格平均计算
        /// </summary>
        [Description("按单元格平均计算")]
        AvgCellCount = 3,
        /// <summary>
        /// 自由计算
        /// </summary>
        [Description("自由计算")]
        FreeCalculation = 4
    }
    /// <summary>
    /// 
    /// </summary>
    public enum CalculationScoreType
    {
        /// <summary>
        /// 设置权重比计分
        /// </summary>
        Set = 0,
        /// <summary>
        /// 根据单元格计分
        /// </summary>
        Cell = 1
    }
    /// <summary>
    /// 分录题计分方式
    /// </summary>
    public enum AccountEntryCalculationType
    {
        /// <summary>
        /// 统一设置权重
        /// </summary>
        SetAll = 0,
        /// <summary>
        /// 单独设置权重
        /// </summary>
        SetSingle = 1,
        /// <summary>
        /// 单元格计分
        /// </summary>
        Cell = 2
    }
    /// <summary>
    /// 申请单状态
    /// </summary>
    public enum ApplyStatus
    {
        /// <summary>
        /// 待跟进
        /// </summary>
        Pending = 0,
        /// <summary>
        /// 跟进中
        /// </summary>
        Processing = 1,
        /// <summary>
        /// 已开通
        /// </summary>
        Solved = 2,
        /// <summary>
        /// 已关闭
        /// </summary>
        Closed = 3
    }
    /// <summary>
    /// 开通账号
    /// </summary>
    public enum OrderUserEnum
    {
        /// <summary>
        /// 名下老师及学生
        /// </summary>
        StudentAndTeacher = 0,
        /// <summary>
        /// 名下教师
        /// </summary>
        Teacher = 1,
        /// <summary>
        /// 名下学生
        /// </summary>
        Student = 2
    }
    /// <summary>
    /// 支付状态
    /// </summary>
    public enum PayStatus
    {
        /// <summary>
        /// 未付款
        /// </summary>
        NonPayment = 0,
        /// <summary>
        /// 已付款
        /// </summary>
        Paid = 1
    }
    /// <summary>
    /// 老师类别
    /// </summary>
    public enum TeacherType
    {
        /// <summary>
        /// 老师
        /// </summary>
        Teacher = 0,
        /// <summary>
        /// 教务
        /// </summary>
        Educational = 1,
        /// <summary>
        /// 专家
        /// </summary>
        Specialist = 2
    }
    /// <summary>
    /// 竞赛用户类型
    /// </summary>
    public enum TaskUserType
    {
        /// <summary>
        /// 学生
        /// </summary>
        Student = 0,
        /// <summary>
        /// 老师
        /// </summary>
        Teacher = 1
    }
    public enum AnswerShowType
    {
        /// <summary>
        /// 完成后显示
        /// </summary>
        CompletedShow = 0,
        /// <summary>
        /// 不显示
        /// </summary>
        NotShow = 1
    }
    public enum ExamSourceType
    {
        /// <summary>
        /// 学校订单
        /// </summary>
        Order = 0,
        /// <summary>
        /// 赛前训练
        /// </summary>
        LiveApp = 1,
        /// <summary>
        /// 校内赛任务
        /// </summary>
        Task = 2,
        /// <summary>
        /// 省赛任务
        /// </summary>
        ProTask = 3,
        /// <summary>
        /// 岗位实训任务
        /// </summary>
        TrainingTask = 4,
        /// <summary>
        /// 用户订单
        /// </summary>
        UserOrder = 5,
    }
    /// <summary>
    /// 案例费用类型（0.收费 1.免费）
    /// </summary>
    public enum CaseFeeType
    {
        /// <summary>
        /// 收费
        /// </summary>
        Paid = 0,
        /// <summary>
        /// 免费
        /// </summary>
        Free = 1
    }
    /// <summary>
    /// 省内赛场次类型 0 大数据 1 业财税
    /// </summary>
    public enum CompetitionVenuesType
    {
        /// <summary>
        /// 大数据
        /// </summary>
        BigData = 0,
        /// <summary>
        /// 业财税
        /// </summary>
        Ycs = 1
    }

    /// <summary>
    /// 案例密码状态
    /// </summary>
    public enum CasePasswordStatus
    {
        Disable = 0,
        Enabled = 1,
    }

    /// <summary>
    /// 章节汇总类型
    /// </summary>
    public enum SectionSummaryType
    {
        /// <summary>
        /// 教材
        /// </summary>
        TextBook,

        /// <summary>
        /// 习题
        /// </summary>
        Exercises,

        /// <summary>
        /// 案例
        /// </summary>
        Case,
        /// <summary>
        /// 视频
        /// </summary>
        Video,
        /// <summary>
        /// 章节
        /// </summary>
        Section,

        /// <summary>
        /// 课件
        /// </summary>
        Files,

        /// <summary>
        /// 场景实训
        /// </summary>
        SectionSceneTraining,

        /// <summary>
        /// 教案
        /// </summary>
        LessonPlan,

        /// <summary>
        /// 授课计划
        /// </summary>
        TeachingPlan,


    }
    /// <summary>
    /// 标签来源
    /// </summary>
    public enum TagSourceType
    {
        /// <summary>
        /// 后台管理
        /// </summary>
        BackManage = 0,
        /// <summary>
        /// 案例
        /// </summary>
        Case = 1
    }

    /// <summary>
    /// 行业
    /// </summary>
    public enum Industry
    {
        /// <summary>
        /// 行业
        /// </summary>
        Industry,
        /// <summary>
        /// 农业
        /// </summary>
        Agricultural,
        /// <summary>
        /// 工业
        /// </summary>
        Industrial,
        /// <summary>
        /// 互联网
        /// </summary>
        Internet
    }

    /// <summary>
    /// 纳税人方式
    /// </summary>
    public enum TaxpayerMethod
    {
        /// <summary>
        /// 一般纳税人
        /// </summary>
        GeneralTaxpayer
    }

    /// <summary>
    /// 公司规模
    /// </summary>
    public enum CompanySize
    {
        /// <summary>
        /// 公司规模
        /// </summary>
        CompanySize,
        /// <summary>
        /// 上市公司
        /// </summary>
        ListedCompany,
        /// <summary>
        /// 大公司
        /// </summary>
        LargeCompany,
        /// <summary>
        /// 中公司
        /// </summary>
        ChinaSteel,
        /// <summary>
        /// 小公司
        /// </summary>
        SmallCompany
    }

    /// <summary>
    /// 案例类型
    /// </summary>
    public enum Classification
    {
        /// <summary>
        /// 会计核算
        /// </summary>
        Accounting = 0,
        /// <summary>
        /// 财务管理
        /// </summary>
        FinancialManagement = 1,
        /// <summary>
        /// 审计
        /// </summary>
        Audit = 2,
        /// <summary>
        /// 税务
        /// </summary>
        Tax = 3,
        /// <summary>
        /// 全部
        /// </summary>
        all = 4
    }

    /// <summary>
    /// 案例状态
    /// </summary>
    public enum CaseStatus
    {
        /// <summary>
        /// 待上架
        /// </summary>
        StayOnTheShelf = 0,
        /// <summary>
        /// 上架待审
        /// </summary>
        WaitingForTrial = 1,
        /// <summary>
        /// 已上架
        /// </summary>
        OnShelves = 2,
        /// <summary>
        /// 已拒绝
        /// </summary>
        Rejected = 3,
        /// <summary>
        /// 全部
        /// </summary>
        all = 4
    }

    /// <summary>
    /// 案例来源类型
    /// </summary>
    public enum CaseSourceType
    {
        /// <summary>
        /// 云自营
        /// </summary>
        CloudSelfCamp = 0,
        /// <summary>
        /// 其他
        /// </summary>
        Other = 1,
        /// <summary>
        /// 全部
        /// </summary>
        all = 2
    }

    /// <summary>
    /// 案例购买类型
    /// </summary>
    public enum CasePurchaseType
    {
        /// <summary>
        /// 赠送
        /// </summary>
        Give = 0,
        /// <summary>
        /// 购买
        /// </summary>
        Purchase = 1
    }
    /// <summary>
    /// 案例等级
    /// </summary>
    public enum CaseGrade
    {
        /// <summary>
        /// A级
        /// </summary>
        A = 1,
        /// <summary>
        /// B级
        /// </summary>
        B = 2,
        /// <summary>
        /// C级
        /// </summary>
        C = 3,
        /// <summary>
        /// D级
        /// </summary>
        D = 4,
        /// <summary>
        /// E级
        /// </summary>
        E = 5,
    }
    /// <summary>
    /// 纳税人类型
    /// </summary>
    public enum TaxPayerType
    {
        /// <summary>
        /// 一般纳税人
        /// </summary>
        General = 0,
        /// <summary>
        /// 小规模纳税人
        /// </summary>
        SmallScale = 1
    }
    /// <summary>
    /// 终端类型
    /// </summary>
    public enum ClientType
    {
        /// <summary>
        /// 
        /// </summary>
        PC = 0,
        /// <summary>
        /// 
        /// </summary>
        Android = 1,
        /// <summary>
        /// 
        /// </summary>
        IOS = 2
    }

    /// <summary>
    /// 登录方式
    /// </summary>
    public enum LoginType
    {
        /// <summary>
        /// PC端登录
        /// </summary>
        PC = 0,

        /// <summary>
        /// PC端登录
        /// </summary>
        App = 1,
    }

    /// <summary>
    /// 数据新增方式
    /// </summary>
    public enum DataCreateWay
    {
        /// <summary>
        /// 自建数据
        /// </summary>
        OnselfBuild = 0,

        /// <summary>
        /// 拷贝自有数据(深度拷贝)
        /// </summary>
        CopyOther = 1,

        /// <summary>
        /// 引用平台数据(通过购买行为、深度拷贝)
        /// </summary>
        Quotation = 2,
    }

    #endregion


    #region 课业任务
    /// <summary>
    /// 课业任务类型
    /// </summary>
    public enum TaskType
    {
        /// <summary>
        /// 作业-老师组卷
        /// </summary>
        ClassHomeWork = 0,
        /// <summary>
        /// 任务-老师组卷
        /// </summary>
        ClassTask = 1,
        /// <summary>
        /// 考试-老师组卷
        /// </summary>
        ClassExam = 2,
        /// <summary>
        /// 课堂测验-老师组卷
        /// </summary>
        ClassTest = 3
    }
    /// <summary>
    /// 课业任务状态
    /// </summary>
    public enum LessonsTaskStatus
    {
        /// <summary>
        /// 未开始
        /// </summary>
        [Description("未开始")]
        Wait = 0,
        /// <summary>
        /// 进行中
        /// </summary>
        [Description("进行中")]
        Started = 1,
        /// <summary>
        /// 已结束
        /// </summary>
        [Description("已结束")]
        End = 2,
    }
    /// <summary>
    /// 课业任务 - 考试设置
    /// </summary>
    public enum TaskExamSetting
    {
        /// <summary>
        /// 所有人考同一套试卷
        /// </summary>
        [Description("所有人考同一套试卷")]
        One = 1,
        /// <summary>
        /// 不同人考不同试卷
        /// </summary>
        [Description("不同人考不同试卷")]
        Multiple = 2,
    }
    /// <summary>
    /// 课业任务 - 成绩分布
    /// </summary>
    public enum TaskScoreDistributionType
    {
        /// <summary>
        /// 所有数据
        /// </summary>
        [Description("所有数据")]
        All = -1,
        /// <summary>
        /// 优秀(大于90)
        /// </summary>
        [Description("优秀(大于等于80 %)")]
        Excellent = 0,
        /// <summary>
        /// 良好(80-89)
        /// </summary>
        [Description("良好(80-89 %)")]
        Good = 1,
        /// <summary>
        /// 合格(60-69)
        /// </summary>
        [Description("合格(60-69 %)")]
        Qualified = 2,
        /// <summary>
        /// 不合格(小于60)
        /// </summary>
        [Description("不合格(小于60 %)")]
        Unqualified = 3
    }

    #endregion

    #region 操作类型
    /// <summary>
    /// 操作类型
    /// </summary>
    public enum OperationType
    {
        /// <summary>
        /// 查询
        /// </summary>
        [Description("查询")]
        Search = 1,

        /// <summary>
        /// 添加
        /// </summary>
        [Description("添加")]
        Add = 2,

        /// <summary>
        /// 修改
        /// </summary>
        [Description("修改")]
        Update = 4,

        /// <summary>
        /// 删除
        /// </summary>
        [Description("删除")]
        Delete = 8,
    }
    #endregion

    #region 订单状态
    /// <summary>
    /// 订单状态
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// 系统创建(赠送)【Obsolete】
        /// </summary>
        [Description("系统创建(赠送)")]
        Presented = 0,

        /// <summary>
        /// 创建订单(未支付)
        /// </summary>
        [Description("创建订单(未支付)")]
        Created = 1,

        /// <summary>
        /// 取消订单
        /// </summary>
        [Description("取消订单")]
        Canceled = 2,

        /// <summary>
        /// 删除取消订单
        /// </summary>
        [Description("删除取消订单")]
        DeleteCancel = 3,

        /// <summary>
        /// 支付成功订单(已支付)
        /// </summary>
        [Description("支付成功订单(已支付)")]
        PaymentSuccess = 4,

        /// <summary>
        /// 删除支付成功订单
        /// </summary>
        [Description("删除支付成功订单")]
        DeletePayment = 5,

        /// <summary>
        /// 删除失效订单(赠送资源存在有效期)
        /// </summary>
        [Description("删除失效订单(赠送资源存在有效期)")]
        DeleteExpire = 6,
    }
    #endregion

    #region 章节类型
    /// <summary>
    /// 章节类型
    /// </summary>
    public enum SectionType
    {
        /// <summary>
        /// 章
        /// </summary>
        [Description("章")]
        Chapter = 0,

        /// <summary>
        /// 节
        /// </summary>
        [Description("节")]
        Section = 1,
    }
    #endregion

    #region 查询数据源类型
    /// <summary>
    /// 查询数据源类型
    /// </summary>
    public enum DataSourceType
    {
        /// <summary>
        /// 自己的
        /// </summary>
        [Description("自己的")]
        Oneself = 0,

        /// <summary>
        /// 所有的
        /// </summary>
        [Description("所有的")]
        All = 1,
    }
    /// <summary>
    /// 实训状态
    /// </summary>
    public enum TrainType
    {
        /// <summary>
        /// 未开始
        /// </summary>
        Wait = 0,
        /// <summary>
        /// 进行中
        /// </summary>
        Started = 1,
        /// <summary>
        /// 已结束
        /// </summary>
        End = 2
    }
    #endregion

    #region 课程来源
    /// <summary>
    /// 课程来源
    /// </summary>
    public enum CourseDataSource
    {
        /// <summary>
        /// 系统赠送
        /// </summary>
        [Description("系统赠送")]
        SystemPresent = 0,

        /// <summary>
        /// 客户单独购买
        /// </summary>
        [Description("客户单独购买")]
        IndividualBuy = 1,

        /// <summary>
        /// 上传私有
        /// </summary>
        [Description("上传私有")]
        UploadPrivate = 2,
    }
    #endregion

    #region 课程资源类型
    /// <summary>
    /// 课程资源类型
    /// </summary>
    public enum CourseResourcesType
    {
        /// <summary>
        /// 购买课程下的资源
        /// </summary>
        [Description("购买课程下的资源")]
        UnderCourse = 0,

        /// <summary>
        /// 客户单独购买
        /// </summary>
        [Description("客户单独购买")]
        IndividualBuy = 1,

        /// <summary>
        /// 上传私有
        /// </summary>
        [Description("上传私有")]
        UploadPrivate = 2,
    }
    /// <summary>
    /// 练习类型
    /// </summary>
    public enum ExercisesType
    {
        /// <summary>
        /// 场景实训
        /// </summary>
        Training = 0,
        /// <summary>
        /// 课程练习
        /// </summary>
        CourceExercises = 1
    }
    /// <summary>
    /// 课程考试类型
    /// </summary>
    public enum CourseExamType
    {
        /// <summary>
        /// 课程练习
        /// </summary>
        CourceTest = 0,
        /// <summary>
        /// 备课课程练习
        /// </summary>
        PrepareCourseTest = 1,
        /// <summary>
        /// 考试
        /// </summary>
        ClassExam = 2,
        /// <summary>
        /// 任务
        /// </summary>
        ClassTask = 3,
        /// <summary>
        /// 作业
        /// </summary>
        ClassHomeWork = 4,
        /// <summary>
        /// 课堂测验
        /// </summary>
        ClassTest = 5,
        /// <summary>
        /// 新课程任务
        /// </summary>
        NewCourseTask=6,
        /// <summary>
        /// 新课程任务--老师
        /// </summary>
        TeacherCourseTask = 7
    }
    #endregion

    #region 纳税主体

    /// <summary>
    /// 纳税主体
    /// </summary>
    public enum Taxpayer
    {
        /// <summary>
        /// 一般纳税人
        /// </summary>
        General = 0,

        /// <summary>
        /// 小规模纳税人
        /// </summary>
        SmallScale = 1,
        /// <summary>
        /// 其他
        /// </summary>
        Other = 2
    }

    #endregion

    #region 实习模式

    /// <summary>
    /// 实习模式
    /// </summary>
    public enum PracticePattern
    {
        /// <summary>
        /// 综岗实习
        /// </summary>
        composite = 0,

        /// <summary>
        /// 分岗实习
        /// </summary>
        exclusive = 1,

        /// <summary>
        /// 综岗和分岗
        /// </summary>
        Both = 2,
    }

    /// <summary>
    /// 岗位类型
    /// </summary>
    public enum PositionType
    {
        /// <summary>
        /// 综岗
        /// </summary>
        composite = 0,

        /// <summary>
        /// 分岗
        /// </summary>
        exclusive = 1,
    }



    #endregion

    #region 审票模式（工作模式）

    /// <summary>
    /// 审票模式（工作模式）
    /// </summary>
    public enum WorkPattern
    {
        /// <summary>
        /// 每日
        /// </summary>
        Daily,

        /// <summary>
        /// 每周
        /// </summary>
        Weekly,

        /// <summary>
        /// 每月
        /// </summary>
        Monthly
    }

    #endregion

    #region 利润结转模式

    /// <summary>
    /// 利润结转模式
    /// </summary>
    public enum ProfitCarryOverPattern
    {
        /// <summary>
        /// 每月
        /// </summary>
        Monthly,

        /// <summary>
        /// 每年
        /// </summary>
        Yearly
    }

    #endregion

    #region 会计准则

    /// <summary>
    /// 会计准则
    /// </summary>
    public enum AccountingStandard
    {
        /// <summary>
        /// 小企业
        /// </summary>
        SmallEnterprise,
        /// <summary>
        /// 企业
        /// </summary>
        Enterprise,
        /// <summary>
        /// 事业单位
        /// </summary>
        Institution,
        /// <summary>
        /// 非营利组织
        /// </summary>
        NPO
    }

    #endregion

    #region 实习试卷类型
    /// <summary>
    /// 实习试卷类型
    /// </summary>
    public enum SxExamSourceType
    {
        /// <summary>
        /// 订单
        /// </summary>
        Order = 0,

        /// <summary>
        /// 平台免费
        /// </summary>
        PlatformFree = 1,
    }
    #endregion

    #region 创建账套状态
    /// <summary>
    /// 创建账套状态
    /// </summary>
    public enum CreateSetBookStatus
    {
        /// <summary>
        /// 不需要创建
        /// </summary>
        NotCreate = 0,

        /// <summary>
        /// 未创建
        /// </summary>
        NoCreate = 1,

        /// <summary>
        /// 已创建
        /// </summary>
        HasCreated = 2
    }
    #endregion

    #region 日期任务状态
    /// <summary>
    /// 日期任务状态
    /// </summary>
    public enum DateJobStatus
    {
        /// <summary>
        /// 未完成
        /// </summary>
        Unfinished = 0,

        /// <summary>
        /// 已完成
        /// </summary>
        Done = 1,
    }
    #endregion

    #region 票据类型
    /// <summary>
    /// 票据类型
    /// </summary>
    public enum BillTopicType
    {
        /// <summary>
        /// 普通票据(非干扰票据)
        /// </summary>
        CommonBill = 0,

        /// <summary>
        /// 干扰票据
        /// </summary>
        ObstructBill = 1,

        /// <summary>
        /// 推送票据
        /// </summary>
        PushBill = 2,
    }
    #endregion

    #region 票据状态
    /// <summary>
    /// 票据状态
    /// </summary>
    public enum BillStatus
    {
        /// <summary>
        /// 未审核
        /// </summary>
        Unreviewed = 0,

        /// <summary>
        /// 已审核
        /// </summary>
        Checked = 1,

        /// <summary>
        /// 已退回
        /// </summary>
        Returned = 2,
    }
    #endregion

    #region 票据题目来源
    /// <summary>
    /// 票据题目来源
    /// </summary>
    public enum BillQuestionSource
    {
        /// <summary>
        /// 未审核
        /// </summary>
        Unreviewed = 0,

        /// <summary>
        /// 已作答
        /// </summary>
        HasBeenAnswered = 1,
    }
    #endregion

    #region 审核票据操作类型
    /// <summary>
    /// 审核票据操作类型
    /// </summary>
    public enum CheckBillType
    {
        /// <summary>
        /// 审核通过
        /// </summary>
        Pass = 0,

        /// <summary>
        /// 退回
        /// </summary>
        SendBack = 1,
    }
    #endregion

    #region 突发机制类别
    /// <summary>
    /// 突发机制类别
    /// </summary>
    public enum EmergencyType
    {
        /// <summary>
        /// 正确票据退回
        /// </summary>
        CorrectBillReturn = 1,

        /// <summary>
        /// 错误票据退回,原因错误
        /// </summary>
        IncorrectBillReturnedByWrongReason = 2,

        /// <summary>
        /// 错误票据通过
        /// </summary>
        PassageErrorBill = 3,

        /// <summary>
        /// 付款错误
        /// </summary>
        PaymentError = 4,

        /// <summary>
        /// 无提示无答案
        /// </summary>
        NoHintNoAnswer = 5,
    }
    #endregion

    #region 收付款题类型
    /// <summary>
    /// 收付款题类型
    /// </summary>
    public enum ReceivePaymentType
    {
        /// <summary>
        /// 无收付款
        /// </summary>
        None = 0,

        /// <summary>
        /// 付款题
        /// </summary>
        PaymentTopic = 1,

        /// <summary>
        /// 收款题
        /// </summary>
        ReceiveTopic = 2,

        /// <summary>
        /// 收款题和付款题
        /// </summary>
        ReceivePayTopic = 3,
    }
    #endregion

    #region 申报状态
    /// <summary>
    /// 申报状态
    /// </summary>
    public enum DeclareStatus
    {
        /// <summary>
        /// 未填写
        /// </summary>
        NotFilled = 0,

        /// <summary>
        /// 待申报
        /// </summary>
        ToDeclare = 1,

        /// <summary>
        /// 已申报
        /// </summary>
        HasDeclared = 2,
    }
    #endregion

    /// <summary>
    /// 题目文件类型
    /// </summary>
    public enum TopicFileType
    {
        /// <summary>
        /// 正确文件（正常文件）
        /// </summary>
        [Description("正确")]
        Normal = 0,
        /// <summary>
        /// 错误文件（干扰文件）
        /// </summary>
        [Description("错误")]
        Fault = 1,
        /// <summary>
        /// 推送文件
        /// </summary>
        [Description("推送")]
        Push = 2,
        /// <summary>
        /// 回执文件
        /// </summary>
        [Description("回执")]
        Reply = 3
    }
    /// <summary>
    /// 辅助核算类型
    /// </summary>
    public enum AssistAccountingType
    {
        /// <summary>
        /// 供应商
        /// </summary>
        [Description("供应商")]
        Vendor = 0,
        /// <summary>
        /// 职员
        /// </summary>
        [Description("职员")]
        employee = 1,
        /// <summary>
        /// 客户
        /// </summary>
        [Description("客户")]
        customer = 2,
        /// <summary>
        /// 部门
        /// </summary>
        [Description("部门")]
        Department = 3,
        /// <summary>
        /// 项目
        /// </summary>
        [Description("项目")]
        Project = 4,
        /// <summary>
        /// 其它
        /// </summary>
        [Description("其它")]
        Other = 5,
    }
    /// <summary>
    /// 题目票据封面底面
    /// </summary>
    public enum TopicFileCoverBack
    {
        /// <summary>
        /// 默认值
        /// </summary>
        None = 0,
        /// <summary>
        /// 封面
        /// </summary>
        [Description("封面")]
        Cover = 1,
        /// <summary>
        /// 地板
        /// </summary>
        [Description("底板")]
        Back = 2
    }

    /// <summary>
    /// 支付渠道
    /// </summary>
    public enum PaymentChannel
    {
        /// <summary>
        /// 支付宝
        /// </summary>
        [Description("支付宝")]
        Alipay,
        /// <summary>
        /// 微信
        /// </summary>
        [Description("微信")]
        Webchat
    }

    /// <summary>
    /// 支付类型
    /// </summary>
    public enum PaymentType
    {
        /// <summary>
        /// PC
        /// </summary>
        [Description("PC")]
        PC,
        /// <summary>
        /// APP
        /// </summary>
        [Description("APP")]
        APP
    }
    /// <summary>
    /// 凭证字
    /// </summary>
    public enum CertificateWord
    {
        /// <summary>
        /// 银行
        /// </summary>
        [Description("银行")]
        Bank = 0,
        /// <summary>
        /// 现金
        /// </summary>
        [Description("现金")]
        Cash = 1,
        /// <summary>
        /// 转账
        /// </summary>
        [Description("转账")]
        Transfer = 2,
        /// <summary>
        /// 记
        /// </summary>
        [Description("记")]
        Remember = 3
    }
    /// <summary>
    /// 损溢科目类别
    /// </summary>
    public enum ProfitLossSubjectType
    {
        A类 = 0,
        B类 = 1,
        C类 = 2,
        D类 = 3,
        E类 = 4
    }

    /// <summary>
    /// 考试成绩级别（百分比 如果总分是400分，则用400分乘以这个比例）
    /// </summary>
    public enum GradeLevel
    {
        /// <summary>
        /// 所有数据
        /// </summary>
        [Description("未知")]
        All = -1,
        /// <summary>
        /// 优秀(80-100)
        /// </summary>
        [Description("优秀")]
        Excellent = 0,
        /// <summary>
        /// 良好(70-79.99)
        /// </summary>
        [Description("良好")]
        Good = 1,
        /// <summary>
        /// 合格(60-69.99)
        /// </summary>
        [Description("合格")]
        Qualified = 2,
        /// <summary>
        /// 不合格(0-59.99)
        /// </summary>
        [Description("需改进")]
        Unqualified = 3
    }

    #region 1+X
    /// <summary>
    /// 证书
    /// </summary>
    public enum Certificate
    {
        /// <summary>
        /// 默认
        /// </summary>
        DEFAULT = 0,
        /// <summary>
        /// 业财税融合成本管控职业技能等级证书
        /// </summary>
        CBGK = 1,
        /// <summary>
        /// 业财税融合大数据投融资分析职业技能等级证书
        /// </summary>
        DSJTRZ = 2,
        /// <summary>
        /// 业财税融合项目财务管理职业技能等级证书
        /// </summary>
        CWGL = 3,
    }

    /// <summary>
    /// 证书级别
    /// </summary>
    public enum CertificateLevel
    {
        /// <summary>
        /// 默认
        /// </summary>
        DEFAULT = 0,
        /// <summary>
        /// 初级
        /// </summary>
        Primary = 1,
        /// <summary>
        /// 中级
        /// </summary>
        Intermediate = 2,
        /// <summary>
        /// 高级
        /// </summary>
        Advanced = 3,
    }
    /// <summary>
    /// 案例所属源系统
    /// </summary>
    public enum CaseSourceSystem
    {
        /// <summary>
        /// 竞赛
        /// </summary>
        JS = 1,
        /// <summary>
        /// 行业赛
        /// </summary>
        HYS = 2,
        /// <summary>
        /// 云实训
        /// </summary>
        YSSX = 3,
    }

    /// <summary>
    /// 座位号类型
    /// </summary>
    public enum SeatType
    {
        /// <summary>
        /// 奇数
        /// </summary>
        OddNumber = 1,
        /// <summary>
        /// 偶数
        /// </summary>
        EvenNumber = 2,
    }

    /// <summary>
    /// 历届考试证书类型
    /// </summary>
    public enum PreviouExamType
    {
        /// <summary>
        /// 默认
        /// </summary>
        DEFAULT = 0,

        /// <summary>
        /// 大数据分析决策能力证
        /// </summary>
        [Description("大数据分析决策能力证")]
        BigDataAnalysisDecision = 1,

        /// <summary>
        /// 大数据投融资分析能力证
        /// </summary>
        [Description("大数据投融资分析能力证")]
        BigDataInvestmentFinancing = 2,

        /// <summary>
        /// 财务大数据应用能力证
        /// </summary>
        [Description("财务大数据应用能力证")]
        BigDataApplication = 3,

        /// <summary>
        /// 业财税一体化技能实操能手
        /// </summary>
        [Description("业财税一体化技能实操能手")]
        IndustryFinanceTaxIntegration = 4,

        /// <summary>
        /// 财务会计核算应用能力证
        /// </summary>
        [Description("财务会计核算应用能力证")]
        FinancialAccountApplication = 5,

        /// <summary>
        /// 管理会计应用能力证
        /// </summary>
        [Description("管理会计应用能力证")]
        ManagementAccountApplication = 6,

        /// <summary>
        /// 投融资分析技术证
        /// </summary>
        [Description("投融资分析技术证")]
        InvestmentFinancingAnalysis = 7,

        /// <summary>
        /// 成本管控专业技术证书
        /// </summary>
        [Description("成本管控专业技术证书")]
        CostControl = 8,
    }

    #endregion

    /// <summary>
    /// App
    /// </summary>
    public enum AppIdEnum
    {
        /// <summary>
        /// 云实训
        /// </summary>
        yssx = 1,
    }

    #region 活跃度类型
    /// <summary>
    /// 活跃度类型
    /// </summary>
    public enum ActivityType
    {
        /// <summary>
        /// 阅读
        /// </summary>
        Read = 0,

        /// <summary>
        /// 点赞
        /// </summary>
        Like = 1,

        /// <summary>
        /// 转发
        /// </summary>
        Foeward = 2,
    }
    #endregion

    #region 统计资源类型
    /// <summary>
    /// 统计资源类型
    /// </summary>
    public enum StatisticsResourceType
    {
        /// <summary>
        /// 案例
        /// </summary>
        Case = 0,

    }
    #endregion

    /// <summary>
    /// 云实习答题顺序，不能调整顺序
    /// </summary>
    public enum GameTopicNode
    {
        /// <summary>
        /// 审票
        /// </summary>
        SP = 1,
        /// <summary>
        /// 收款
        /// </summary>
        SK = 2,
        /// <summary>
        /// 付款
        /// </summary>
        FK = 3,
        /// <summary>
        ///分录  
        /// </summary>
        FL = 4,
        /// <summary>
        /// 出纳审核
        /// </summary>
        CNSH = 5,
        /// <summary>
        /// 主管审核
        /// </summary>
        ZGSH = 6,
        /// <summary>
        /// 结转损益
        /// </summary>
        JZSY = 7,
        /// <summary>
        /// 结账
        /// </summary>
        JZ = 8,
        /// <summary>
        /// 其他
        /// </summary>
        Other = 100,
    }

    /// <summary>
    /// 关卡状态
    /// </summary>
    public enum GameLevelStatus
    {
        /// <summary>
        /// 未解锁
        /// </summary>
        [Description("未解锁")]
        Lock = 0,
        /// <summary>
        /// 解锁，未开始
        /// </summary>
        [Description("解锁")]
        UnLock = 1,
        /// <summary>
        /// 闯关中
        /// </summary>
        [Description("闯关中")]
        Doing = 2,

        /// <summary>
        /// 通关
        /// </summary>
        [Description("通关")]
        Done = 3,
    }

    public enum GameResourceKey
    {
        /// <summary>
        /// 招牌图片文件夹名称（3D）
        /// </summary>
        ZPLogo = 1,
        /// <summary>
        /// 会议室图片文件夹名称（3D）
        /// </summary>
        MeetingRoomPicture = 2,
    }
    /// <summary>
    /// 科目类型
    /// </summary>
    public enum SubjectType
    {
        /// <summary>
        /// 全部
        /// </summary>
        AllSubject = 0,
        /// <summary>
        /// 资产
        /// </summary>
        AssetsSubject = 1,
        /// <summary>
        /// 负债
        /// </summary>
        LiabilitiesSubject = 2,
        /// <summary>
        /// 权益
        /// </summary>
        RightsSubject = 4,
        /// <summary>
        /// 成本
        /// </summary>
        CostSubject = 5,
        /// <summary>
        /// 损益
        /// </summary>
        ProfitAndLossSubject = 6,
    }

    public enum StatisticalType
    { 
        /// <summary>
        /// 全部
        /// </summary>
        All=0,
        /// <summary>
        /// 最高分
        /// </summary>
        MaxScore=1,
        /// <summary>
        /// 最新
        /// </summary>
        SubmitTime=2
    }
    /// <summary>
    /// 排序类型
    /// </summary>
    public enum SortType
    {
        /// <summary>
        /// 学号升序
        /// </summary>
        StudentNo=0,
        /// <summary>
        /// 学号降序
        /// </summary>
        StudentNoDesc=1,
        /// <summary>
        /// 得分升序
        /// </summary>
        Score=2,
        /// <summary>
        /// 得分降序
        /// </summary>
        ScoreDesc=3,
        /// <summary>
        /// 交卷时间升序
        /// </summary>
        SubmitTime=4,
        /// <summary>
        /// 交卷时间降序
        /// </summary>
        SubmitTimeDesc=5
    }
    /// <summary>
    /// 排序类型
    /// </summary>
    public enum TaskTopicSortType
    { 
        /// <summary>
        /// 默认排序
        /// </summary>
        Default=0,
        /// <summary>
        /// 随机排序
        /// </summary>
        Random=1
    }
    #region 文件类型
    /// <summary>
    /// 文件类型
    /// </summary>
    public enum FilesType
    {
        /// <summary>
        /// Excel
        /// </summary>
        Excel = 0,
        /// <summary>
        /// Word
        /// </summary>
        Word = 1,
        /// <summary>
        /// PDF
        /// </summary>
        PDF = 2,
        /// <summary>
        /// PPT
        /// </summary>
        PPT = 3,
        /// <summary>
        /// Img
        /// </summary>
        Img = 4,
        /// <summary>
        /// ZIP
        /// </summary>
        ZIP = 5,
        /// <summary>
        /// Mp4
        /// </summary>
        Mp4 = 6,
        /// <summary>
        /// Threegp
        /// </summary>
        Threegp = 7,
        /// <summary>
        /// Wmv
        /// </summary>
        Wmv = 8,
        /// <summary>
        /// Rmvb
        /// </summary>
        Rmvb = 9,
        /// <summary>
        /// Avi
        /// </summary>
        Avi = 10,
        /// <summary>
        /// Dat
        /// </summary>
        Dat = 11,
        /// <summary>
        /// Mkv
        /// </summary>
        Mkv = 12,
        /// <summary>
        /// Flv
        /// </summary>
        Flv = 13,
        /// <summary>
        /// Vob
        /// </summary>
        Vob = 14,
        /// <summary>
        /// M4v
        /// </summary>
        M4v = 15,
    };
    #endregion

    #region 课程备课资源类型
    /// <summary>
    /// 课程备课资源类型
    /// </summary>
    public enum CoursePrepareResourceType
    {
        /// <summary>
        /// 课件
        /// </summary>
        CourseWare = 0,
        /// <summary>
        /// 教案
        /// </summary>
        TeachingPlan = 1,
        /// <summary>
        /// 视频
        /// </summary>
        Video = 2,
        /// <summary>
        /// 思政案例
        /// </summary>
        EducationCase = 3,
        /// <summary>
        /// 个人素材
        /// </summary>
        PersonalStuff = 4,
    }
    #endregion

    #region 课程备课题目来源
    /// <summary>
    /// 课程备课题目来源
    /// </summary>
    public enum CoursePrepareTopicSource
    {
        /// <summary>
        /// 课程
        /// </summary>
        Course = 0,
        /// <summary>
        /// 案例
        /// </summary>
        Case = 1,
    }
    #endregion

    #region 课堂测验任务来源
    /// <summary>
    /// 课堂测验任务来源
    /// </summary>
    public enum CourseTestTaskSource
    {
        /// <summary>
        /// 课程
        /// </summary>
        Course = 0,
        /// <summary>
        /// 案例
        /// </summary>
        Case = 1,
    }
    #endregion

    #region 备课资源归属
    /// <summary>
    /// 备课资源归属
    /// </summary>
    public enum CoursePrepareResourcesBelong
    {
        /// <summary>
        /// 课前预习
        /// </summary>
        PreviewBeforeClass = 0,
        /// <summary>
        /// 课中上课
        /// </summary>
        CourseInClass = 1,
    }
    #endregion

    #region 查看状态
    /// <summary>
    /// 查看状态
    /// </summary>
    public enum CheckStatus
    {
        /// <summary>
        /// 未查看
        /// </summary>
        DidNotCheck = 0,
        /// <summary>
        /// 已查看
        /// </summary>
        HadCheck = 1,
    }
    #endregion

    #region 课堂测验题目操作状态
    /// <summary>
    /// 课堂测验题目操作状态
    /// </summary>
    public enum CourseTestTopicOperateStatus
    {
        /// <summary>
        /// 未测验
        /// </summary>
        DidNotTest = 0,
        /// <summary>
        /// 测验进行中
        /// </summary>
        TestProgress = 1,
        /// <summary>
        /// 测验完成
        /// </summary>
        TestFinish = 2,
    }
    #endregion

    #region 上课状态
    /// <summary>
    /// 上课状态
    /// </summary>
    public enum TeachCourseStatus
    {
        /// <summary>
        /// 未开始
        /// </summary>
        [Description("未开始")]
        Wait = 0,
        /// <summary>
        /// 进行中
        /// </summary>
        [Description("进行中")]
        Started = 1,
        /// <summary>
        /// 已结束
        /// </summary>
        [Description("已结束")]
        End = 2,
    }
    #endregion

    #region 课堂测验任务学生操作状态
    /// <summary>
    /// 课堂测验任务学生操作状态
    /// </summary>
    public enum CourseTestTaskStudentOperateStatus
    {
        /// <summary>
        /// 开始测验
        /// </summary>
        [Description("开始测验")]
        StartTest = 0,
        /// <summary>
        /// 查看作答
        /// </summary>
        [Description("查看作答")]
        CheckAnswer = 1,
        /// <summary>
        /// 未作答
        /// </summary>
        [Description("未作答")]
        NotAnswer = 2,
    }
    #endregion

}
