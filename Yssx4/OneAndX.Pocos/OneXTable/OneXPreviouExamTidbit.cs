﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 历届考试 - 花絮
    /// </summary>
    [Table(Name = "onex_previou_exam_tidbit")]
    public class OneXPreviouExamTidbit : BizBaseEntity<long>
    {
        /// <summary>
        /// 考试Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(Name = "Description", DbType = "varchar(255)", IsNullable = true)]
        public string Description { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

    }
}
