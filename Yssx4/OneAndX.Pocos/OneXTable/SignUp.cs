﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 报名_表 每次报名都会生成新的 报名Id
    /// </summary>
    [Table(Name = "1x_SignUp")]
    public class SignUp : BizBaseEntity<long>
    {
        /// <summary>
        /// 0学生 1社会人士
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 考试计划Id
        /// </summary>
        public long ExamPlanId { get; set; }

        /// <summary>
        /// 报考证书
        /// </summary>
        [Column(Name = "Certificate", DbType = "int")]
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 报考证书等级
        /// </summary>
        [Column(Name = "CertificateLevel", DbType = "int")]
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 考试时间
        /// </summary>
        public DateTime ExamTime { get; set; }

        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [Column(Name = "Sex", DbType = "int")]
        public Sex Sex { get; set; }

        /// <summary>
        /// 证件类型
        /// </summary>
        public string DocumentType { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IDNumber { get; set; }

        /// <summary>
        /// 所选考点省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 所选考点市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 区
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// 学历
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// 所属/毕业学校
        /// </summary>
        public string School { get; set; }

        /// <summary>
        /// 专业
        /// </summary>
        public string Major { get; set; }

        /// <summary>
        /// 正面身份证
        /// </summary>
        public string FrontIDCard { get; set; }

        /// <summary>
        /// 背面身份证
        /// </summary>
        public string ReverseSideIDCard { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 行业名称
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>

        public string CompanyName { get; set; }

        /// <summary>
        /// 职位名称
        /// </summary>
        public string PositionName { get; set; }

        #region 考试信息字段

        /// <summary>
        /// 考点Id
        /// </summary>
        public Nullable<long> ExamSiteId { get; set; }

        /// <summary>
        /// 考场Id
        /// </summary>
        public Nullable<long> ExamRoomId { get; set; }

        /// <summary>
        /// 座位号
        /// </summary>
        public Nullable<int> SeatNumber { get; set; }

        /// <summary>
        /// 座位号类型 1:奇数 2:偶数
        /// </summary>
        public int SeatType { get; set; }

        /// <summary>
        /// 准考证号
        /// </summary>
        public string ExamCardNumber { get; set; }

        /// <summary>
        /// 考试密码
        /// </summary>
        public string ExamPassword { get; set; }

        #endregion

        /// <summary>
        /// 租户Id
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 客户端id
        /// </summary>
        public Guid ClientId { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary> 
        [Column(Name = "UserTypeEnums", DbType = "int")]
        public UserTypeEnums UserType { get; set; }


    }
}
