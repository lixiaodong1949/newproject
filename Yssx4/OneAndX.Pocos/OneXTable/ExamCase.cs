﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 试卷案例信息
    /// </summary>
    [Table(Name = "exam_case")]
    public class ExamCase : BizBaseEntity<long>
    {
        /// <summary>
        /// 试卷名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)")]
        public string Name { get; set; }

        /// <summary>
        /// 自主训练名称
        /// </summary>
        [Column(Name = "TrainingName", DbType = "varchar(255)")]
        public string TrainingName { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        [Column(Name = "Certificate", DbType = "int")]
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        [Column(Name = "CertificateLevel", DbType = "int")]
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(Name = "Status", DbType = "int")]
        public Status Status { get; set; }

        /// <summary>
        /// 是否加入自主训练
        /// </summary>
        [Column(Name = "IsCanTraining")]
        public bool IsCanTraining { get; set; } = false;

        /// <summary>
        /// 案例所属系统
        /// </summary>
        [Column(Name = "SourceSystem", DbType = "int", IsNullable = true)]
        public CaseSourceSystem SourceSystem { get; set; }

        /// <summary>
        /// 案例原系统中的Id
        /// </summary>
        [Column(Name = "SourceCaseId", IsNullable = true)]
        public long SourceCaseId { get; set; }

        /// <summary>
        /// 案例原系统中的名称
        /// </summary>
        [Column(Name = "SourceCaseName", IsNullable = true)]
        public string SourceCaseName { get; set; }

        /// <summary>
        /// 考试难度
        /// </summary>
        [Column(Name = "Level", IsNullable = true)]
        public int Level { get; set; } = 0;

        /// <summary>
        /// 考试案例拥有的题目类型名称
        /// </summary>
        [Column(Name = "QuestionTypeName", IsNullable = true)]
        public string QuestionTypeName { get; set; }

        /// <summary>
        /// 试卷排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 自主训练排序
        /// </summary>
        [Column(Name = "TrainingSort", DbType = "int")]
        public string TrainingSort { get; set; }

        /// <summary>
        /// Logo
        /// </summary>
        public string Logo { get; set; }

        /// <summary>
        /// 总时长
        /// </summary>
        public int TotalTime { get; set; }

    }
}
