﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 考试计划
    /// </summary>
    [Table(Name = "exam_plan")]
    public class ExamPlan : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)", IsNullable = true)]
        public string Name { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        [Column(Name = "Certificate", DbType = "int")]
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        [Column(Name = "CertificateLevel", DbType = "int")]
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 考试开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 考试结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 考试状态
        /// </summary>
        [Column(DbType = "int")]
        public ExamStatus Status { get; set; } = 0;

        /// <summary>
        /// 排序
        /// </summary>
        [Column(Name = "Sort", DbType = "int", IsNullable = true)]
        public int Sort { get; set; } = 0;

        /// <summary>
        /// 报名人数
        /// </summary>
        [Column(Name = "ApplyCount", DbType = "int", IsNullable = true)]
        public int ApplyCount { get; set; } = 0;

        /// <summary>
        /// 报名开始时间
        /// </summary>
        [Column(Name = "ApplyBeginTime")]
        public DateTime ApplyBeginTime { get; set; }

        /// <summary>
        /// 报名结束时间
        /// </summary>
        [Column(Name = "ApplyEndTime")]
        public DateTime ApplyEndTime { get; set; }


        ///// <summary>
        ///// 发布时间
        ///// </summary>
        //public DateTime? ReleaseTime { get; set; }

        ///// <summary>
        ///// 是否已发布
        ///// 发布后学生任务列表可见
        ///// </summary>
        //public bool IsRelease { get; set; } = false;


        ///// <summary>
        ///// 学生分配方式
        ///// </summary>
        //[Column(DbType = "int")]
        //public StudentDistribute DistributeMethod { get; set; }

        ///// <summary>
        ///// 机位数
        ///// </summary>
        //public int SeatCount { get; set; }

        ///// <summary>
        ///// 随机数量--实际的学生数量
        ///// </summary>
        //public int RandomCount { get; set; }


    }
}
