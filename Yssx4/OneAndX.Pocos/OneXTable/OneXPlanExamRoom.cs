﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 考试计划考场表
    /// </summary>
    [Table(Name = "onex_plan_exam_room")]
    public class OneXPlanExamRoom : BizBaseEntity<long>
    {
        /// <summary>
        /// 考试计划Id
        /// </summary>
        public long ExamPlanId { get; set; }

        /// <summary>
        /// 考点Id
        /// </summary>
        public long ExamSiteId { get; set; }
        
        /// <summary>
        /// 考场Id
        /// </summary>
        public long ExamRoomId { get; set; }

    }
}
