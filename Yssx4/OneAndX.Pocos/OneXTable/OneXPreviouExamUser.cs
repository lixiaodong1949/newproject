﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 历届考试 - 报名用户
    /// </summary>
    [Table(Name = "onex_previou_exam_user")]
    public class OneXPreviouExamUser : BizBaseEntity<long>
    {
        /// <summary>
        /// 考试Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 考生姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 学籍号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }


        /// <summary>
        /// 排名
        /// </summary>
        public int Ranking { get; set; }

        /// <summary>
        /// 是否发证
        /// </summary>
        public bool IsIssueCertificate { get; set; } = false;


        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; } = Sex.None;

        /// <summary>
        /// 出生年月
        /// </summary>
        public string BirthDate { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string CertificateNo { get; set; }

        public DateTime CertificateDate { get; set; }

    }
}
