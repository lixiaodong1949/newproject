﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 历届考试 - 培训教材
    /// </summary>
    [Table(Name = "onex_previou_exam_file")]
    public class OneXPreviouExamFile : BizBaseEntity<long>
    {
        /// <summary>
        /// 考试Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 文件地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
