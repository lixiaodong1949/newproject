﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 历届考试
    /// </summary>
    [Table(Name = "onex_previou_exam")]
    public class OneXPreviouExam : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)", IsNullable = true)]
        public string Name { get; set; }
        
        /// <summary>
        /// 历届考试证书类型
        /// </summary>
        [Column(Name = "PreviouExamType", DbType = "int")]
        public PreviouExamType PreviouExamType { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        [Column(Name = "Certificate", DbType = "int")]
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        [Column(Name = "CertificateLevel", DbType = "int")]
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 发证日期
        /// </summary>
        public DateTime IssueDate { get; set; }

        /// <summary>
        /// 报名开始时间
        /// </summary>
        public DateTime ApplyBeginTime { get; set; }

        /// <summary>
        /// 报名结束时间
        /// </summary>
        public DateTime ApplyEndTime { get; set; }

        /// <summary>
        /// 考试开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 考试结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 正式图片模板
        /// </summary>
        public string XImg { get; set; }

    }
}
