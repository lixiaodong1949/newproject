﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 考试计划进度表
    /// </summary>
    [Table(Name = "onex_plan_exam_schedule")]
    public class OneXPlanExamSchedule : BaseEntity<long>
    {
        /// <summary>
        /// 考试计划Id
        /// </summary>
        public long ExamPlanId { get; set; }

        /// <summary>
        /// 是否已分配考场
        /// </summary>
        public bool IsDistributedExamRoom { get; set; }

        /// <summary>
        /// 是否已生成准考证
        /// </summary>
        public bool IsCreatedExamCard{ get; set; }

        /// <summary>
        /// 是否已生成试卷
        /// </summary>
        public bool IsCreatedExamPaper { get; set; }
    }
}
