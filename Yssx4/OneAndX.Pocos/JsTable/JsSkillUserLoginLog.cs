﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    [Table(Name = "skill_userloginlog")]
    public class JsSkillUserLoginLog : BaseEntity<long>
    {
        /// <summary>
        /// 登录用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 登录Ip
        /// </summary>
        public string LoginIp { get; set; }

        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTime LoginTime { get; set; }

        /// <summary>
        /// 登录token
        /// </summary>
        [Column(Name = "AccessToken", DbType = "nvarchar(1000)")]
        public string AccessToken { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
    }
}
