﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 学校
    /// </summary>
    [Table(Name = "yssx_school")]
    public class SxYssxSchool : BizBaseEntity<long>
    {
        /// <summary>
        /// 学校名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 院校代码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 院校类型
        /// </summary>
        [Column(DbType = "int")]
        public SchoolType Type { get; set; }

        /// <summary>
        /// 院校简称
        /// </summary> 
        public string ShortName { get; set; }

        /// <summary>
        /// 院长姓名
        /// </summary>
        public string DeanName { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 学校地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 校徽
        /// </summary>
        public string Logo { get; set; }

        /// <summary>
        /// 账号开始时间
        /// </summary>
        public DateTime AccountStartTime { get; set; }
        /// <summary>
        /// 账号到期时间
        /// </summary>
        public DateTime AccountEndTime { get; set; }

        /// <summary>
        /// 是否测试学校(0.正常 1.测试)
        /// </summary>
        public int IsDisplay { get; set; } = 0;

        /// <summary>
        /// 状态(0,禁用，1，启用)
        /// </summary>
        public int Status { get; set; } = 1;
        /// <summary>
        /// 当前开启案例
        /// </summary>
        public long OpenCaseId { get; set; }
        
    }
}
