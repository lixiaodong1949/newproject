﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 通关记录
    /// </summary>
    [Table(Name = "grade_record")]
    public class GradeRecord : BizBaseEntity<long>
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        [Column(Name = "CaseName", DbType = "varchar(255)")]
        public string CaseName { get; set; }
        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 交卷时间
        /// </summary>
        public DateTime SubmitTime { get; set; }
        /// <summary>
        /// 得分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        [Column(Name = "Certificate", DbType = "int")]
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        [Column(Name = "CertificateLevel", DbType = "int")]
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 案例所属系统 
        /// </summary>
        [Column(Name = "SourceSystem", IsNullable = true)]
        public int SourceSystem { get; set; }
        /// <summary>
        /// 测试类型（1 自主训练 2考前自测）
        /// </summary>
        public int ExamType { get; set; }
        ///// <summary>
        ///// 1+1案例时，多条记录GroupId一致 前端生成这个ID
        ///// </summary>
        //public int GroupId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int RollUpType { get; set; }

        public string CaseYear { get; set; }
    }
}
