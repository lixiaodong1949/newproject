﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace OneAndX.Pocos
{
    /// <summary>
    /// 教材_表
    /// </summary>
    [Table(Name = "1x_TextBook")]
    public class TextBook : BizBaseEntity<long>
    {
        /// <summary>
        /// 教材名称
        /// </summary>
        public string TextbookName { get; set; }

        /// <summary>
        /// 所属模块
        /// </summary>
        public int Module { get; set; }

        /// <summary>
        /// 等级
        /// </summary>
        public int Grade { get; set; }

        /// <summary>
        /// 状态 -1全部 0开启 1关闭
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 类型 0教材 1案例 2课件 3培训拓展资源
        /// </summary>
        public int Type { get; set; }
    }

    /// <summary>
    /// 教材_详情表
    /// </summary>
    [Table(Name = "1x_TextBook_Details")]
    public class TextBookDetails : BaseEntity<long>
    {
        /// <summary>
        /// 父级Id 0根Id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string ChapterName { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 教材内容
        /// </summary>
        [Column(Name = "TextbookContent", DbType = "longtext", IsNullable = true)]
        public string TextbookContent { get; set; }

        /// <summary>
        /// 上传文件
        /// </summary>
        public string UploadFiles { get; set; }

        /// <summary>
        /// 教材Id
        /// </summary>
        public long TextbookId { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
    }
}
