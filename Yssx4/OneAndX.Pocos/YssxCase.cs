﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Pocos
{
    [Table(Name = "yssx_case")]
    public class YssxCase
    {
        public long Id { get; set; }

        public int IsX { get; set; }
    }
}
