﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Tas.Common.Configurations;
using Yssx.Framework.Dal;
using Yssx.Redis;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.JobTask.App
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Init();
            //WeiDianHostedService host = new WeiDianHostedService();

            //await host.StartAsync(new System.Threading.CancellationToken());
            //  int seconds = int.Parse(ConfigurationManager.AppSettings["seconds"]);
            //await QuartzHelper.Instance.StartJobExecuteInterval<CourseJob>(seconds, "course");
            // await QuartzHelper.Instance.StartJobExecuteInterval<CourseOrderJob>(5, "courseOrder");
            List<long> ids = new List<long> 
            {
                996931209561979,
                998085746346665,
                1000354462977087,
                1018143014551569,
                1020196893402645,
                1020298920084644,
                1020568221172435,
                1020978602837226,
                1021007377990872,
                1021104365388081,
                1021532949890001,
                1022303132320298,
                1022708746617149,
                1022720838306037,
                1022777862422765,
                1023131432255536,
                1024125621696416,
                1024128838727988,
                1024129394094923,
                1024137217340486,
                1024147554089789,
                1024499653758315,
                1024599725976923,
                1024965896388564,
                1025002892461015,
                1025771046256674,
                1030050725118106,
                1032225338197522,
                1032236580555070,
                1032255211211280,
                1033188425810964,
                1037599597588193,
                1042362794208654,
                1044861292059131,
                1044861722941737,
                1044870993995732,
                1044873745459367,
                1044950406170118,
                1045171697993687,
                1045272793308257,
                1050372807079275,
                1050466237089883,
                1055745726529993,
                1056010839055171,
                1056029213541561,
                1056158795550212,
                1058244012114038,
                1060334651145591,
                1060412907601924,
                1061174141403552,
                1061174896754285,
                1062471030854502,
                1065464578536374,
                1065620224730932,
                1068549417439335,
                1069517490696807,
                1071731193952514,
                1090107387611755,
                1091392855869209,
                1105664846992731,
                1106621441926673,
                1106945581939684,
                1110529558938763,
                1114830153190153,
                1125939208423651,
                1125956945791732,
                1126386060867197,
                1126472794886058,
                1126822117989019,
                1127368937825450,
                1128319945949195,
                1129205239253942,
                1129422244164912,
                1129424717269245,
                1129427015748400,
                1129434562839265,
                1129438702618049,
                1129451147120020,
                1129458573694630,
                1129460936626820,
                1129465456547715,
                1129466408654777,
                1129467848840264,
                1129471668312086,
                1129474512050213,
                1129500425404760,
                1129501293920987,
                1129569669471755,
                1129570214731288,
                1129584987070261,
                1129589936349063,
                1129590502281583,
                1129592524234714,
                1129592662348163,
                1129592960143759,
                1129595724488883,
                1129596047450309,
                1130112225600817,
                1130962067117927,
                1131269257945823,
                1131603166454089,
                1131654181775698,
                1132505363024481,
                1132601685225472,
                1132877832508070,
                1132879858357456,
                1133743499179619,
                1133745181095554,
                1134341900547908,
                1134778548607745,
                1136880880095256,
                1138633114733595,
                1138758344507533,
                1139006238066880,
                1139080040498130,
                1139368482816377,
                1139389299157977,
                1139390422343864,
                1139399134805830,
                1139575068204820,
                1139601861419716,
                1139821329294348,
                1140767997112251,
                1141520816546141,
                1141525492198873,
                1141643896619009,
                1143790573935463,
                1143805930182807,
                1143826095533675,
                1143831762039011,
                1143840909816735,
                1144140542643552,
                1144247884465549,
                1144539052023247,
                1144583097516901,
                1145086426710775,
                1145145914564717,
                1145416857384827,
                1145546789464913,
                1146724545188500,
                1146736381529645,
                1146831071572266,
                1146931638412696,
                1147146181299322,
                1147321897549886,
                1147351559672536,
                1147363463108916,
                1147572939562772,
                1147673439343846,
                1148061407464011,
            };
            IZhenShuOrderService service = new ZhenShuOrderService();
            await service.AddCourseOrder(ids);
            Console.WriteLine($"任务已启动！ ");
            Console.ReadLine();
        }

        static void Init()
        {
            string sqlConncetion_r = ConfigurationManager.ConnectionStrings["yssx_r"].ConnectionString;
            string sqlConnection_w = ConfigurationManager.ConnectionStrings["yssx_w"].ConnectionString;
            string redisConnection = ConfigurationManager.ConnectionStrings["RedisConnection"].ConnectionString;
            //string sqlConnection2 = "Data Source=139.9.51.48;Port=3306;User ID=yssx-dev;Password=Wah%#*903;Initial Catalog=skillscompetition;Charset=utf8;SslMode=none;Max pool size=50;";
            ProjectConfiguration.GetInstance().UseFreeSqlRepository(sqlConnection_w, sqlConncetion_r);
            ProjectConfiguration.GetInstance().UseRedis(redisConnection);

        }
    }
}
