﻿using Quartz;
using System;
using System.Threading.Tasks;
using Yssx.S.IServices;
using Yssx.S.ServiceImpl;

namespace Yssx.JobTask.App
{
    public class CourseOrderJob : IJob
    {
        private IZhenShuOrderService service;
        public CourseOrderJob()
        {
            service = new ZhenShuOrderService();
        }
        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                if (service != null)
                {

                    await service.AddCourseOrder();
                    Console.WriteLine($"{DateTime.Now},订单正在处理.....");
                }
                else
                {
                    Console.WriteLine("同步初级会计考证任务没有执行！");
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine($"执行发生错误：{DateTime.Now}{Environment.NewLine}{ex.StackTrace}");
            }
        }
    }
}
