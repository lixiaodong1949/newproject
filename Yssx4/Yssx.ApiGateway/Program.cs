﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Yssx.ApiGateway
{
    public class Program
    {
        //public static void Main(string[] args)
        //{
        //    CreateWebHostBuilder(args).Build().Run();
        //}

        //public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        //    WebHost.CreateDefaultBuilder(args)
        //        .ConfigureAppConfiguration((hostingContext, config) =>
        //        {
        //            //config
        //            //    .SetBasePath(hostingContext.HostingEnvironment.ContentRootPath)
        //            //    .AddJsonFile("appsettings.json", true, true)
        //            //    //.AddJsonFile($"appsettings.{hostingContext.HostingEnvironment.EnvironmentName}.json", true, true)
        //            //    .AddJsonFile("Ocelot.json", true, true)

        //            //    // .AddOcelot(hostingContext.HostingEnvironment)
        //            //    .AddEnvironmentVariables();
        //            config.AddJsonFile("ocelot.json", optional: false, reloadOnChange: true);
        //        })
        //        .UseUrls("http://127.0.0.1:9527")
        //        .UseStartup<Startup>();
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile("ocelot.json", optional: false, reloadOnChange: true)
                .Build();
            BuildWebHost(args, config).Run();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        public static IWebHost BuildWebHost(string[] args, IConfiguration config) =>
            WebHost.CreateDefaultBuilder(args)
            .UseConfiguration(config)
                .UseStartup<Startup>()
                .Build();
    }
}
