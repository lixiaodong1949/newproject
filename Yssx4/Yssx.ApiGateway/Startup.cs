﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Consul;
using Swashbuckle.AspNetCore.Swagger;
using Yssx.Swagger;

namespace Yssx.ApiGateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddOcelot().AddConsul();//注入Ocelot服务
            services.AddOcelot(Configuration);//注入Ocelot服务
            //添加验证逻辑
            //services.AddAuthentication("Bearer")
            //    .AddIdentityServerAuthentication("usergateway", options =>
            //    {
            //        options.Authority = "http://localhost:9524";
            //        options.ApiName = "api1";
            //        //options.SupportedTokens = IdentityServer4.AccessTokenValidation.SupportedTokens.Jwt;
            //        //options.ApiSecret = "Users";
            //        options.RequireHttpsMetadata = false;
            //    });
            //services.AddAuthentication("Bearer")
            //    .AddIdentityServerAuthentication(options =>
            //    {
            //        options.Authority = "http://localhost:9524";
            //        options.RequireHttpsMetadata = false;

            //        options.ApiName = "api1";
            //    });
            services.AddMvc(options=>
            {
                //options.Filters.Add<AuthorizationFilter>();
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            //services.AddCustomSwagger(CURRENT_SWAGGER_OPTIONS);
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("ApiGateway", new Info { Title = "网关服务", Version = "v1" });
            });
            services.AddCors(options =>
             {
                 options.AddPolicy("AnyOrigin", builder =>
                 {
                     builder.AllowAnyOrigin() //允许任何来源的主机访问                             
                     .AllowAnyMethod()
                     .AllowAnyHeader()
                     .AllowCredentials();//指定处理cookie                                   
                 });
             });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            var apis = new List<string> { "yssxs", "sxcourse", "yssxc","ZhenShu" };
            app.UseMvc()
               .UseSwagger()
               .UseSwaggerUI(options =>
               {
                   apis.ForEach(m =>
                   {
                       options.SwaggerEndpoint($"/{m}/swagger.json", m);
                   });
               })
               .UseCors("AnyOrigin");
            app.UseOcelot().Wait();//使用Ocelot中间件
        }
    }
}
