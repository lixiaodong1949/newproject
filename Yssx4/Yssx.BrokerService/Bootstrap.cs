﻿using ECommon.Components;
using ECommon.Configurations;
using ECommon.Logging;
using ECommon.Extensions;
using EQueue.Broker;
using EQueue.Configurations;
using System.Collections.Generic;
using System.Net;
using ECommonConfiguration = ECommon.Configurations.Configuration;
using System.IO;
using System.Configuration;
using ECommon.Socketing;
using System.Linq;

namespace Yssx.BrokerService
{
    public class Bootstrap
    {
        private static ECommonConfiguration _ecommonConfiguration;
        private static BrokerController _broker;
        //private static IConfiguration _configuration;
        public static void Initialize()
        {
            //var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            //var configuration = builder.Build();
            //AppSettingConfig.RegisterConfiguration(configuration);
            //_configuration = configuration;
            InitializeEQueue();
        }


        public static void Start()
        {
            _broker.Start();
        }
        public static void Stop()
        {
            if (_broker != null)
            {
                _broker.Shutdown();
            }
        }

        private static void InitializeEQueue()
        {
            _ecommonConfiguration = ECommonConfiguration
                .Create()
                .UseAutofac()
                .RegisterCommonComponents()
                .UseSerilog()
                .UseJsonNet()
                .RegisterUnhandledExceptionHandler()
                .RegisterEQueueComponents()
                .BuildContainer();
            var storePath = ConfigurationManager.AppSettings["fileStoreRootPath"];          //enode文件存储地址
            var bindingAddress = ConfigurationManager.AppSettings["bindingAddress"];        //绑定IP地址
            var address = ConfigurationManager.AppSettings["nameServerAddress"];            //nameServer IP地址
            var nameServerPort = ConfigurationManager.AppSettings["nameServerPort"];        //nameServer 端口
            var brokerProducerPort = ConfigurationManager.AppSettings["producerPort"];      //brokerProducerPort(生产者) 端口
            var brokerConsumerPort = ConfigurationManager.AppSettings["consumerPort"];      //brokerConsumerPort(消费者) 端口
            var adminPort = ConfigurationManager.AppSettings["adminPort"];                  //监控端口
            

            var brokerAddress = string.IsNullOrEmpty(bindingAddress) ? SocketUtils.GetLocalIPV4() : IPAddress.Parse(bindingAddress);    //BorkerServer IP地址设置
            var nameServerAddress = string.IsNullOrEmpty(address) ? SocketUtils.GetLocalIPV4() : IPAddress.Parse(address);              //nameServer IP地址设置
            var nameServerEndpoints = new List<IPEndPoint> { new IPEndPoint(nameServerAddress, int.Parse(nameServerPort)) };            //完整设置nameServer连接IP地址
            var brokerSetting = new BrokerSetting(false, storePath)
            {
                NameServerList = nameServerEndpoints,
            };
            brokerSetting.BrokerInfo.BrokerName = ConfigurationManager.AppSettings["brokerName"];
            brokerSetting.BrokerInfo.GroupName = ConfigurationManager.AppSettings["groupName"];
            brokerSetting.BrokerInfo.ProducerAddress = new IPEndPoint(brokerAddress, int.Parse(brokerProducerPort)).ToAddress();
            brokerSetting.BrokerInfo.ConsumerAddress = new IPEndPoint(brokerAddress, int.Parse(brokerConsumerPort)).ToAddress();
            brokerSetting.BrokerInfo.AdminAddress = new IPEndPoint(brokerAddress, int.Parse(adminPort)).ToAddress();
            _broker = BrokerController.Create(brokerSetting);
            ObjectContainer.Resolve<ILoggerFactory>().Create(typeof(Bootstrap).FullName).Info("Broker initialized.");
        }
    }
}
