﻿using Aop.Api.Domain;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Tas.Common.Configurations;
using Tas.Common.IdGenerate;
using Tas.Common.Utils;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Redis;
using Yssx.S.Pocos;

namespace RedisTaskApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Init();
            RedisHelper.Subscribe(("login", async (msg) =>
             {
                 long uid = 0;
                 long.TryParse(msg.Body, out uid);
                 if (uid > 0)
                 {
                     await AddIntegral(uid, 1);
                 }
             }
            ), ("SubmitAnswer", async (msg2) =>
              {
                  QuestionData data = JsonHelper.DeserializeObject<QuestionData>(msg2.Body);
                  if (null != data && data.UserId != 1054708947419999)
                  {
                      await AddIntegral(data.UserId, 2, data.QuestionId);
                  }
              }
            ), ("register", async (msg3) =>
              {
                  UserInfo info = JsonHelper.DeserializeObject<UserInfo>(msg3.Body);

                  if (null != info)
                  {
                      await RegisterUser(info);
                  }
              }
            ), ("", async (msg4) =>
              {

              }
            ));
        }

        static void Init()
        {
            var redisString = ConfigurationManager.AppSettings["RedisConnection"];
            string sqlConncetion = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
            ProjectConfiguration.GetInstance().UseRedis(redisString);
            ProjectConfiguration.GetInstance().UseFreeSqlRepository(sqlConncetion);
        }

        static async Task<bool> AddIntegral(long userId, int fromType, long rid = 0)
        {
            bool isExist = await DbContext.FreeSql.GetRepository<YssxIntegralDetails>()
                .WhereIf(fromType == 1, x => x.Date == DateTime.Now.ToString("yyyy-MM-dd") && x.FromType == 1 && x.UserId == userId)
                .WhereIf(fromType == 2, x => x.FromType == 2 && x.UserId == userId && x.RelatedId == rid).AnyAsync();

            if (isExist) return true;

            long number = 0;
            YssxIntegralDetails integralDetails = null;
            switch (fromType)
            {
                case 1: number = 5; integralDetails = new YssxIntegralDetails { Id = IdWorker.NextId(), UserId = userId, CreateTime = DateTime.Now, Desc = "登录获得奖励积分", FromType = 1, Number = 5, StatusType = 1, Date = DateTime.Now.ToString("yyyy-MM-dd") }; break;
                case 2: number = 1; integralDetails = new YssxIntegralDetails { Id = IdWorker.NextId(), UserId = userId, CreateTime = DateTime.Now, Desc = "答题正确获得奖励积分", FromType = 2, Number = 1, StatusType = 1, RelatedId = rid }; break;
                default:
                    break;
            }

            string key = $"user_ntegral:{userId}";
            RedisLock.SkipLock(key, () =>
             {
                 //YssxIntegral integral = DbContext.FreeSql.GetRepository<YssxIntegral>().Where(x => x.UserId == userId).First();
                 //if (integral == null)
                 //{
                 //    DbContext.FreeSql.GetRepository<YssxIntegral>().Insert(new YssxIntegral { Id = IdWorker.NextId(), UserId = userId, Number = number, UpdateTime = DateTime.Now });
                 //}
                 //else
                 //{
                 //    DbContext.FreeSql.GetRepository<YssxIntegral>().UpdateDiy.Set(x => x.Number, integral.Number + number).Where(x => x.Id == integral.Id).ExecuteAffrows();
                 //}

                 using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元    
                 {
                     try
                     {
                         var repo_i = uow.GetRepository<YssxIntegral>();
                         var repo_id = uow.GetRepository<YssxIntegralDetails>();

                         YssxIntegral integral = repo_i.Where(x => x.UserId == userId).First();
                         if (integral == null)
                         {
                             integralDetails.IntegralSum = number;
                             repo_i.Insert(new YssxIntegral { Id = IdWorker.NextId(), UserId = userId, Number = number, UpdateTime = DateTime.Now });
                         }
                         else
                         {
                             integralDetails.IntegralSum = integral.Number + number;
                             repo_i.UpdateDiy.Set(x => x.Number, integral.Number + number).Where(x => x.Id == integral.Id).ExecuteAffrows();
                         }

                         repo_id.Insert(integralDetails);

                         uow.Commit();
                     }
                     catch (Exception ex)
                     {
                         uow.Rollback();
                     }
                 }
             }, 3);

            return true;
        }

        static async Task<bool> RegisterUser(UserInfo userInfo)
        {
            bool isExist = await DbContext.FreeSql.GetRepository<YssxUser>().Where(x => x.MobilePhone == userInfo.MobilePhone).AnyAsync();
            if (!isExist)
            {
                await DbContext.FreeSql.GetRepository<YssxUser>().InsertAsync(new YssxUser
                {
                    Id = userInfo.Id,
                    MobilePhone = userInfo.MobilePhone,
                    ClientId = Guid.NewGuid(),
                    RealName = userInfo.RealName,
                    Password = userInfo.Password,
                    IdNumber = userInfo.IdNumber,
                    Status = 1,
                    NikeName = userInfo.RealName,
                    UserType = 7
                });
            }

            return true;

        }

        public class QuestionData
        {
            public long QuestionId { get; set; }

            public long UserId { get; set; }

            public long GradeId { get; set; }
        }

        public class UserInfo
        {
            public long Id { get; set; }
            public int UserType { get; set; }
            public string MobilePhone { get; set; }
            public string Password { get; set; }
            public string RealName { get; set; }
            public string IdNumber { get; set; }
            public int Sex { get; set; }

        }

        public class CourseOrder
        {

        }
    }
}
