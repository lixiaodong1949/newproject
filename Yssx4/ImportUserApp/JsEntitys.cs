﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;

using System.Data;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace ImportUserApp
{
    public class JsEntitys
    {
    }

    [Table(Name = "yssx_case")]
    public class YssxCaseJs: BizBaseEntity<long>
    {
        public string Name { get; set; }
        /// <summary>
        /// 案例编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科）
        /// </summary>
        public int Education { get; set; }
        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }
        /// <summary>
        /// 是否启用大数据中心（0不启用 1启用）
        /// </summary>
        public int IsBigData { get; set; }
        /// <summary>
        /// 赛事类型（0 个人赛 1团体赛）
        /// </summary>
        [Column(DbType = "int")]
        public CompetitionType CompetitionType { get; set; }
        /// <summary>
        /// 总时长
        /// </summary>
        public int TotalTime { get; set; }
        /// <summary>
        /// 设置类型（0统一设置  1单独设置 2 单元格计分） 统一设置时以当前案例主表外围里围为准
        /// 否则以各个分录提的设置为准
        /// </summary>
        [Column(DbType = "int")]
        public AccountEntryCalculationType SetType { get; set; }
        /// <summary>
        /// 外围
        /// </summary>
        public int OutVal { get; set; }
        /// <summary>
        /// 里围
        /// </summary>
        public int InVal { get; set; }
        /// <summary>
        /// 企业相关信息
        /// </summary>
        [Column(Name = "EnterpriseInfo", DbType = "LongText", IsNullable = true)]
        public string EnterpriseInfo { get; set; }
        /// <summary>
        /// 赛前训练启用状态（0 不启用 1启用）
        /// </summary>
        public int TestStatus { get; set; }
        /// <summary>
        /// 正式赛启用状态（0 不启用 1启用）
        /// </summary>
        public int OfficialStatus { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountingManager { get; set; }
        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }
        /// <summary>
        /// 成本会计
        /// </summary>
        public string CostAccountant { get; set; }
        /// <summary>
        /// 税务会计
        /// </summary>
        public string TaxAccountant { get; set; }
        /// <summary>
        /// 业务会计
        /// </summary>
        public string BusinessAccountant { get; set; }
        /// <summary>
        /// 分录题分数占比
        /// </summary>
        [Column(Name = "AccountEntryScale", DbType = "decimal(6,3)", IsNullable = true)]
        public decimal AccountEntryScale { get; set; }
        /// <summary>
        /// 表格题分数占比
        /// </summary>
        [Column(Name = "FillGridScale", DbType = "decimal(6,3)", IsNullable = true)]
        public decimal FillGridScale { get; set; }
        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }
        /// <summary>
        /// 会计期间
        /// </summary>
        [Column(Name = "AccountingPeriodDate", DbType = "varchar(50)", IsNullable = true)]
        public string AccountingPeriodDate { get; set; }
        /// <summary>
        /// 区域
        /// </summary>
        [Column(Name = "Region", DbType = "varchar(50)")]
        public string Region { get; set; }

       // public int SxType { get; set; }


    }

    [Table(Name = "yssx_topic")]
    public class YssxTopic2 : BizBaseEntity<long>
    {
        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 是否结账题（0 否 1是）
        /// </summary>
        public int IsSettlement { get; set; }
        /// <summary>
        /// 是否压缩（0 否 1是）
        /// </summary>
        public int IsGzip { get; set; }
        /// <summary>
        /// 题目标题
        /// </summary>
        [Column(Name = "Title", DbType = "varchar(255)", IsNullable = true)]
        public string Title { get; set; }
        /// <summary>
        /// 题干内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        [Column(DbType = "int")]
        public int QuestionType { get; set; }

        /// <summary>
        /// 题目内容类型(1:纯文字,2:表格,3:图片)
        /// </summary>
        [Column(DbType = "int")]
        public int QuestionContentType { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>
        [Column(Name = "DifficultyLevel", DbType = "int(255)", IsNullable = true)]
        public int? DifficultyLevel { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        public string Keywords { get; set; }

        /// <summary>
        /// 模板数据源内容(不含答案)
        /// </summary>
        [Column(Name = "TopicContent", DbType = "LongText", IsNullable = true)]
        public string TopicContent { get; set; }
        /// <summary>
        /// 模板数据源内容(含答案)
        /// </summary>
        [Column(Name = "FullContent", DbType = "LongText", IsNullable = true)]
        public string FullContent { get; set; }

        /// <summary>
        /// 答案 内容（选择题答案a,b,c等，判断题true/false，表格json等）
        /// </summary>
        [Column(Name = "AnswerValue", DbType = "LongText", IsNullable = true)]
        public string AnswerValue { get; set; }
        /// <summary>
        /// 答案提示
        /// </summary>
        [Column(Name = "Hint", DbType = "Text", IsNullable = true)]
        public string Hint { get; set; }

        /// <summary>
        /// 父节点ID-用于多题型(一个题目 多个作答区)
        /// </summary>
        [Column(Name = "ParentId", DbType = "bigint(20)", IsNullable = true)]
        public long ParentId { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10,4)")]
        public decimal Score { get; set; }
        /// <summary>
        /// 非完整性得分
        /// </summary>
        [Column(Name = "PartialScore", DbType = "decimal(10,4)", IsNullable = true)]
        public decimal PartialScore { get; set; } = 0;
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科）
        /// </summary>
        public int Education { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column(DbType = "int")]
        public int Status { get; set; } 
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 会计期间
        /// </summary>
        [Column(Name = "AccountingPeriodDate", DbType = "varchar(50)", IsNullable = true)]
        public string AccountingPeriodDate { get; set; }

        /// <summary>
        /// 题目附件文件id（多个id用,隔开）
        /// </summary>
        [Column(Name = "TopicFileIds", DbType = "varchar(500)", IsNullable = true)]
        public string TopicFileIds { get; set; }
        /// <summary>
        /// 表格题-计算方式（按行计算、按列计算、按单元格平均计算、自由计算）
        /// </summary>
        [Column(DbType = "int")]
        public int CalculationType { get; set; }
        /// <summary>
        /// 表格题-是否开启复制Excel数据（0 否 1是）
        /// </summary>
        public int IsCopy { get; set; } = CommonConstants.IsNoCopy;
        /// <summary>
        /// 表格题-是否开启复制大数据（0 否 1是）
        /// </summary>
        public int IsCopyBigData { get; set; } = CommonConstants.IsNoCopy;
        /// <summary>
        /// 表格题-是否无序（0 否 1是）
        /// </summary>
        public int IsDisorder { get; set; } = CommonConstants.IsNoDisorder;

        /// <summary>
        /// 审核状态(0,待审核，1，审核成功，2，审核拒绝)
        /// </summary>
        public int AuditStatus { get; set; }

        /// <summary>
        /// 审核说明
        /// </summary>
        public string AuditStatusExplain { get; set; }

        /// <summary>
        /// 是否上架(0,待上架，1，上架待审，2，上架)
        /// </summary>
        public int Uppershelf { get; set; }

        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; }
    }

    [Table(Name = "yssx_certificate_topic")]
    public class YssxCertificateTopic2 : BizBaseEntity<long>
    {
        /// <summary>
        /// 发生日期
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// 题干
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }

        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertificateDate { get; set; }

        /// <summary>
        /// 单据张数
        /// </summary>
        public int InvoicesNum { get; set; }

        /// <summary>
        /// 题库Id
        /// </summary>
        public long TopicId { get; set; }

        /// <summary>
        /// 图片文件Id
        /// </summary>
        public string ImageIds { get; set; }

        /// <summary>
        /// 企业Id
        /// </summary>
        public long EnterpriseId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 0:核心，1:B模块
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountingManager { get; set; }
        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }
        /// <summary>
        /// 制单人
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 会计主管是否禁用
        /// </summary>
         [Column(DbType = "int")]
        public int IsDisableAM { get; set; }
        /// <summary>
        /// 出纳是否禁用
        /// </summary>
        [Column(DbType = "int")]
        public int IsDisableCashier { get; set; }
        /// <summary>
        /// 审核人是否禁用
        /// </summary>
        [Column(DbType = "int")]
        public int IsDisableAuditor { get; set; }
        /// <summary>
        /// 制单人是否禁用
        /// </summary>
        [Column(DbType = "int")]
        public int IsDisableCreator { get; set; }
        /// <summary>
        /// 计分方式
        /// </summary>
        [Column(DbType = "int")]
        public int CalculationScoreType { get; set; }

        /// <summary>
        /// 外围
        /// </summary>
        public int OutVal { get; set; }
        /// <summary>
        /// 里围
        /// </summary>
        public int InVal { get; set; }
    }
}
