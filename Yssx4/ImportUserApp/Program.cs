﻿using System;
using System.Collections.Generic;
using Tas.Common.Configurations;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Pocos;
using System.Linq;
using Yssx.S.Pocos.Topics;
using Yssx.Framework.Utils;
using Aop.Api.Domain;
using Yssx.S.Poco;

namespace ImportUserApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Init();
            //ExamPer();
            // FindImg();
            //AddProduct();
            //Code();
            //RegionData();
            ImportStudents(1020221698539627);
            //ChangeScore(1029894770136921);
            //ImportStudents(856734443014979584, 1);
            //JsData(2);
            //UpdateUser(1020223774163030, 1);
            //HysTest();
        }

        public static async void ExamPer()
        {
            List<ExamPaper> exams =await DbContext.FreeSql.GetRepository<ExamPaper>().Where(x => x.TenantId == 1078977911537665).ToListAsync();

            exams.ForEach(x => 
            {
                x.Id = IdWorker.NextId();
                x.ExamType = ExamType.EmulationTest;
                x.Sort = 2;
            });

            await DbContext.FreeSql.GetRepository<ExamPaper>().InsertAsync(exams);
        }

        public static async void Code()
        {
            List<YssxCouponCode> list = new List<YssxCouponCode>();
            string code = string.Empty;
            for (int i = 1; i < 4001; i++)
            {
                code = RandomChars.NumChar(6, Yssx.Framework.Utils.RandomChars.UppLowType.upper);
                YssxCouponCode data = new YssxCouponCode { Code = code, CreateTime = DateTime.Now, Index = i, Id = IdWorker.NextId() };
                list.Add(data);
            }
            await DbContext.FreeSql.GetRepository<YssxCouponCode>().InsertAsync(list);
        }

        public static async void AddProduct()
        {
            List<YssxProduct> list = new List<YssxProduct> 
            {
                new YssxProduct{Id=IdWorker.NextId(),CaseId=1,CaseName="圆梦造纸厂",Date=1,DiscountPrice=498m, OrderType=1,OriginalPrice=498,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=1,CaseName="圆梦造纸厂",Date=2,DiscountPrice=498m, OrderType=1,OriginalPrice=498,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=1,CaseName="圆梦造纸厂",Date=3,DiscountPrice=498m,OrderType=1,OriginalPrice=498,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=1,CaseName="圆梦造纸厂",Date=4,DiscountPrice=498m,OrderType=1,OriginalPrice=498,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=1,CaseName="圆梦造纸厂",Date=5,DiscountPrice=498m,OrderType=1,OriginalPrice=498,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=1,CaseName="圆梦造纸厂",Date=6,DiscountPrice=498m,OrderType=1,OriginalPrice=498,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=1,CaseName="圆梦造纸厂",Date=7,DiscountPrice=498m,OrderType=1,OriginalPrice=498,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=1,CaseName="圆梦造纸厂",Date=8,DiscountPrice=498m,OrderType=1,OriginalPrice=498,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=1,CaseName="圆梦造纸厂",Date=9,DiscountPrice=498m,OrderType=1,OriginalPrice=498,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=1,CaseName="圆梦造纸厂",Date=10,DiscountPrice=498m,OrderType=1,OriginalPrice=498,CreateTime=DateTime.Now},
       
                                                                                                          
                new YssxProduct{Id=IdWorker.NextId(),CaseId=2,CaseName="麓山煤矿",Date=1,DiscountPrice=698m, OrderType=2,OriginalPrice=698,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=2,CaseName="麓山煤矿",Date=2,DiscountPrice=698, OrderType=2,OriginalPrice=698,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=2,CaseName="麓山煤矿",Date=3,DiscountPrice=698,OrderType=2,OriginalPrice=698,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=2,CaseName="麓山煤矿",Date=4,DiscountPrice=698,OrderType=2,OriginalPrice=698,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=2,CaseName="麓山煤矿",Date=5,DiscountPrice=698,OrderType=2,OriginalPrice=698,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=2,CaseName="麓山煤矿",Date=6,DiscountPrice=698,OrderType=2,OriginalPrice=698,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=2,CaseName="麓山煤矿",Date=7,DiscountPrice=698,OrderType=2,OriginalPrice=698,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=2,CaseName="麓山煤矿",Date=8,DiscountPrice=698,OrderType=2,OriginalPrice=698,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=2,CaseName="麓山煤矿",Date=9,DiscountPrice=698,OrderType=2,OriginalPrice=698,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=2,CaseName="麓山煤矿",Date=10,DiscountPrice=698,OrderType=2,OriginalPrice=698,CreateTime=DateTime.Now},
   

                new YssxProduct{Id=IdWorker.NextId(),CaseId=3,CaseName="南邦水泥厂",Date=1,DiscountPrice=1198m, OrderType=3,OriginalPrice=1198,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=3,CaseName="南邦水泥厂",Date=2,DiscountPrice=1198, OrderType=3,OriginalPrice=1198,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=3,CaseName="南邦水泥厂",Date=3,DiscountPrice=1198m,OrderType=3,OriginalPrice=1198,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=3,CaseName="南邦水泥厂",Date=4,DiscountPrice=1198m,OrderType=3,OriginalPrice=1198,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=3,CaseName="南邦水泥厂",Date=5,DiscountPrice=1198m,OrderType=3,OriginalPrice=1198,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=3,CaseName="南邦水泥厂",Date=6,DiscountPrice=1198m,OrderType=3,OriginalPrice=1198,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=3,CaseName="南邦水泥厂",Date=7,DiscountPrice=1198m,OrderType=3,OriginalPrice=1198,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=3,CaseName="南邦水泥厂",Date=8,DiscountPrice=1198m,OrderType=3,OriginalPrice=1198,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=3,CaseName="南邦水泥厂",Date=9,DiscountPrice=1198m,OrderType=3,OriginalPrice=1198,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=3,CaseName="南邦水泥厂",Date=10,DiscountPrice=1198m,OrderType=3,OriginalPrice=1198,CreateTime=DateTime.Now},


                new YssxProduct{Id=IdWorker.NextId(),CaseId=4,CaseName="打包价",Date=1,DiscountPrice=2394m, OrderType=4,  OriginalPrice=2394,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=4,CaseName="打包价",Date=2,DiscountPrice=2394m, OrderType=4,  OriginalPrice=2394,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=4,CaseName="打包价",Date=3,DiscountPrice=2394m,OrderType=4,   OriginalPrice=2394,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=4,CaseName="打包价",Date=4,DiscountPrice=2394m,OrderType=4,   OriginalPrice=2394,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=4,CaseName="打包价",Date=5,DiscountPrice=2394m,OrderType=4,   OriginalPrice=2394,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=4,CaseName="打包价",Date=6,DiscountPrice=2394m,OrderType=4,   OriginalPrice=2394,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=4,CaseName="打包价",Date=7,DiscountPrice=2394m,OrderType=4,   OriginalPrice=2394,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=4,CaseName="打包价",Date=8,DiscountPrice=2394m,OrderType=4,   OriginalPrice=2394,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=4,CaseName="打包价",Date=9,DiscountPrice=2394m,OrderType=4,   OriginalPrice=2394,CreateTime=DateTime.Now},
                new YssxProduct{Id=IdWorker.NextId(),CaseId=4,CaseName="打包价",Date=10,DiscountPrice=2394m,OrderType=4,   OriginalPrice=2394,CreateTime=DateTime.Now},

            };

            await DbContext.FreeSql.GetRepository<YssxProduct>().InsertAsync(list);

        }

        public static void HysTest()
        {
            List<HysModel> list = new List<HysModel>
            {
                new HysModel{Id=1000,ParentId=0,RootId=0,QuestionName="1111222",QuetionsType=7},

                new HysModel{Id=1002,ParentId=0,RootId=0,QuestionName="1111333",QuetionsType=1},
                new HysModel{Id=1003,ParentId=0,RootId=0,QuestionName="1111444",QuetionsType=1},
                new HysModel{Id=1005,ParentId=0,RootId=0,QuestionName="1111555",QuetionsType=1},
                new HysModel{Id=1006,ParentId=0,RootId=0,QuestionName="1111666",QuetionsType=1},
                new HysModel{Id=1007,ParentId=0,RootId=0,QuestionName="1111777",QuetionsType=1},
                new HysModel{Id=1008,ParentId=0,RootId=0,QuestionName="1111888",QuetionsType=1},

                new HysModel{Id=1009,ParentId=1000,RootId=1000,QuestionName="1111999",QuetionsType=1},
                new HysModel{Id=1010,ParentId=1000,RootId=1000,QuestionName="1111100",QuetionsType=1},

                new HysModel{Id=1011,ParentId=0,RootId=0,QuestionName="1111101",QuetionsType=1},

                new HysModel{Id=1012,ParentId=1009,RootId=1000,QuestionName="1111101",QuetionsType=1},
                new HysModel{Id=1013,ParentId=1012,RootId=1000,QuestionName="1111102",QuetionsType=1},
            };

            List<HysModel> statusList = new List<HysModel>
            {
                new HysModel{Id=1009,Status=1,RootId=1000},
            };

            List<HysModel> childList = new List<HysModel>();

            foreach (var item in statusList)
            {
                List<HysModel> temps = list.Where(x => x.RootId == item.RootId).ToList();
                childList.Add(item);
                FindChildNode(item.Id, temps, childList);
            }

            list.ForEach(x =>
            {
                if (childList.Any(o => o.Id == x.Id))
                {
                    x.Status = 1;
                }
            });

            foreach (var item in list)
            {
                if (item.Status == 1) continue;
                Console.WriteLine($"{item.Id},{item.QuestionName}");
            }

        }

        public static void FindChildNode(long id, List<HysModel> list, List<HysModel> retList)
        {
            if (id == 0) return;

            List<HysModel> yssxCoreSubjects = list.Where(x => x.ParentId == id).ToList();
            if (null == yssxCoreSubjects) return;
            foreach (var item in yssxCoreSubjects)
            {
                retList.Add(item);
                FindChildNode(item.Id, list, retList);
            }
        }

        static void Init()
        {
            string sqlConncetion = "Data Source=139.9.202.108;Port=3306;User ID=root;Password=wYRvnH@6Ryfsn4lh;Initial Catalog=trainingcloud;Charset=utf8;SslMode=none;Max pool size=150;";
            //string sqlConnection2 = "Data Source=139.9.51.48;Port=3306;User ID=yssx-dev;Password=Wah%#*903;Initial Catalog=skillscompetition;Charset=utf8;SslMode=none;Max pool size=50;";
            //sqlConncetion = "Data Source=192.168.1.210;Port=3306;User ID=root;Password=nssb123;Initial Catalog=trainingcloud;charset=utf8;SslMode=none;Max pool size=50;";
            string sqlConnection2 = "Data Source=192.168.1.210;Port=3306;User ID=root;Password=nssb123;Initial Catalog=trainingcloud;charset=utf8;SslMode=none;Max pool size=150;";
            ProjectConfiguration.GetInstance().UseFreeSqlRepository2(sqlConncetion, sqlConnection2);

        }

        static async void FindImg()
        {
            List<long> filds = new List<long> 
            {
                1021495812033025,
                1020121604178325,
                1020124263367336,
                1035269877368682,
                1035269877368683,
                1035269877368684,
                1035270401656698,
                1035270401656699,
                1035270401656700,
                1035270718459153,
                1035272186284254,
                1035272186284255,
                1035272186284256,
                1035271194740836,
                1035271194740837,
                1035271194740838,
                1035271194740839,
                1035271194740840,
                1035272358924696,
                1035272436041569,
                1035272436041570,
                1035272436041571,
                1035271410698951,
                1035271410698952,
                1035271410698953,
                1035271462637774,
                1035272556185680,
                1023676698234561,
                1035269447585016,
                1035270607324975,
                1035273052604279,
                1035272052427490,
                1035272142653556,
                1035273219671860,
                1035272236976877,
                1035272236976878,
                1035273336326261,
                1035273391638331,
                1035273391638332,
                1035273391638333,
                1035273462026705,
                1035273462026706,
                1035273462026707,
                1035273702329110,
                1035274812592628,
                1035274812592629,
                1035274763175768,
                1035274763175769,
                1035274847097932,
                1035274956113758,
                1035274956113759,
                1035273943879024,
                1035273943879025,
                1035273943879026,
                1035273943879027,
                1035273943879028,
                1035275026630804,
                1035275118776826,
                1035275118776827,
                1035275118776828,
                1035275118776829,
                1035275118776830,
                1035275118776831,
                1035275170727848,
                1035275170727849,
                1035275170727850,
                1035275170727851,
                1035275170727852,
                1035275170727853,
                1035275170727854,
                1035275170727855,
                1035275170727856,
                1035274369829010,
                1035275468523455,
                1035275534141603,
                1035274547858813,
                1035275693396507,
                1035275693396508,
                1035275751442722,
                1035274753559315,
                1035274753559316,
                1026615462298370,
                1035274893578496,
                1035274893578497,
                1035274973808816,
                1035274973808817,
                1026616746886013,
                1026616838636275,
                1026616918328052,
                1035275080535445,
                1035275080535446,
                1027454208070691,
                1035276196841659,
                1035276196841660,
                1035276251367617,
                1035276251367618,
                1035276361241710,
                1035275275260170,
                1035275275260171,
                1035276633049288,
                1035276633049289,
                1035276711938344,
                1035276711938345,
                1035275722263972,
                1035275722263973,
                1035275722263974,
                1028808048608184,
                1028808048608185,
                1028849132079141,
                1028849132079142,
                1028843125835636,
                1028843125835637,
                1035275946887365,
                1035275946887366,
                1035275946887367,
                1035277077516862,
                1035277152340271,
                1035277152340272,
                1035277152340273,
                1035277199280345,
                1035277255149550,
                1035277255149551,
                1035277255149552,
                1028886280768615,
                1028886280768616,
                1035277325931642,
                1035277325931643,
                1028885173015388,
                1028885173015389,
                1035277418874837,
                1035277628590049,
                1035277628590050,
                1035277628590051,
                1035277691504618,
                1029315349687503,
                1035276686953913,
                1035276720508347,
                1035276720508348,
                1035277812991992,
                1035277812991993,
                1035277871859695,
                1035277871859696,
                1030048395002062,
                1030048395002063,
                1029752568057604,
                1029752568057605,
                1030581150474769,
                1035278039631863,
                1035278039631864,
                1030623647915787,
                1030623647915788,
                1035278428082769,
                1035278428082770,
                1035278428082771,
                1030949641055257,
                1031814159604970,
                1031815102136673,
            };
            List<YssxTopicPublicFile> files = new List<YssxTopicPublicFile>();
            foreach (var item in filds)
            {
                if(await DbContext.FreeSql.GetRepository<YssxTopicPublicFile>().Where(x=> x.Id == item).AnyAsync())
                {
                    continue;
                }

                YssxTopicPublicFile file = await DbContext.FreeSql2.GetRepository<YssxTopicPublicFile>().Where(x => x.Id == item).FirstAsync();
                files.Add(file);
            }

            if (files.Count > 0)
            {
               await DbContext.FreeSql.GetRepository<YssxTopicPublicFile>().InsertAsync(files);
            }
           
        }

        static async void ChangeScore(long examid)
        {
            List<ExamStudentGradeDetail> details = await DbContext.FreeSql.GetRepository<ExamStudentGradeDetail>().Where(x => x.ExamId == examid && x.QuestionId == 944631340763461 && x.Score > 12).ToListAsync();
            details.ForEach(x =>
            {
                x.Score = x.Score / 2;
            });

            List<ExamStudentGrade> grades = await DbContext.FreeSql.GetRepository<ExamStudentGrade>().Where(x => x.ExamId == examid && x.IsDelete == 0).ToListAsync();


            grades.ForEach(x =>
            {

                var data = details.Where(o => o.GradeId == x.Id).FirstOrDefault();
                if (data != null)
                {

                    x.Score = x.Score - data.Score;
                }

            });

            int a = await DbContext.FreeSql.GetRepository<ExamStudentGrade>().UpdateDiy.SetSource(grades).UpdateColumns(x => new { x.Score }).ExecuteAffrowsAsync();
            int b = await DbContext.FreeSql.GetRepository<ExamStudentGradeDetail>().UpdateDiy.SetSource(details).UpdateColumns(x => new { x.Score }).ExecuteAffrowsAsync(); ;
            bool c = a > 0 && b > 0;
            Console.WriteLine(c);
            //ExamStudentGrade examStudent = DbContext.FreeSql.GetRepository<ExamStudentGrade>().UpdateDiy.Set(x => x.Score,)
        }

        static async void ImportStudents(long schoolId)
        {
            List<YssxUser> yssxUsers = new List<YssxUser>();
            List<YssxTeacher> yssxStudents = new List<YssxTeacher>();
            for (int i = 10; i < 41; i++)
            {
                YssxUser u = new YssxUser
                {
                    Id = IdWorker.NextId(),
                    ClientId = Guid.NewGuid(),
                    MobilePhone = $"gzjs{i}",
                    NikeName = $"ccdy{i}",
                    Password = "E10ADC3949BA59ABBE56E057F20F883E", //x.Password,
                    TenantId = schoolId,
                    UserType = 2,
                    Status = 1,
                    RegistrationType = 2,
                    IsDelete = 0,
                    CreateTime = DateTime.Now,
                    CreateBy = 0
                };
                yssxUsers.Add(u);
                YssxTeacher s = new YssxTeacher
                {
                    Id = IdWorker.NextId(),
                    UserId = u.Id,
                    Name = u.NikeName,
                    TenantId = schoolId,
                    CreateTime = DateTime.Now

                };
                yssxStudents.Add(s);
            }
            

            List<YssxUser> e = await DbContext.FreeSql.GetRepository<YssxUser>().InsertAsync(yssxUsers);
            List<YssxTeacher> f = await DbContext.FreeSql.GetRepository<YssxTeacher>().InsertAsync(yssxStudents);

        }
        static async void ImportStudents(long schoolId, int schoolType)
        {
            ScSchoolEntity scSchool = await DbContext.FreeSql2.GetRepository<ScSchoolEntity>().Where(x => x.Id == schoolId).FirstAsync();
            if (scSchool != null)
            {
                List<ScUser> scUserlist = await DbContext.FreeSql2.GetRepository<ScUser>().Where(x => x.SchoolId == schoolId && x.UserState == 1 && x.ClassNo > 0).ToListAsync();

                YssxSchool yssxSchool = new YssxSchool
                {
                    Id = IdWorker.NextId(),
                    Name = scSchool.SchoolFullName,
                    Type = GetType(schoolType),
                    CreateTime = DateTime.Now
                };


                List<ScTie> scTies = await DbContext.FreeSql2.GetRepository<ScTie>().Where(x => x.SchoolNo == schoolId).ToListAsync();
                List<YssxCollege> colleges = scTies.Select(x => new YssxCollege
                {
                    Id = IdWorker.NextId(),
                    Name = x.Name,
                    CreateBy = x.TieNo,
                    TenantId = yssxSchool.Id,
                }).ToList();

                List<YssxProfession> professions = colleges.Select(x => new YssxProfession
                {
                    Id = IdWorker.NextId(),
                    Name = x.Name.Replace("系", ""),
                    TenantId = yssxSchool.Id,
                    CollegeId = x.Id,
                    CreateBy = x.CreateBy,
                    CreateTime = DateTime.Now
                }).ToList();

                List<ScClass> scClases = await DbContext.FreeSql2.GetRepository<ScClass>().Select.From<ScTie>((a, b) => a.InnerJoin(o => o.TieNo == b.TieNo)).Where((a, b) => b.SchoolNo == schoolId).ToListAsync();

                List<YssxClass> classes = scClases.Select(x => new YssxClass
                {
                    Id = IdWorker.NextId(),
                    CollegeId = colleges.Find(a => a.CreateBy == x.TieNo).Id,
                    ProfessionId = professions.Find(a => a.CreateBy == x.TieNo).Id,
                    TenantId = yssxSchool.Id,
                    Name = x.Name,
                    CreateTime = DateTime.Now,
                    CreateBy = x.ClassNo
                }).ToList();

                List<YssxUser> yssxUsers = scUserlist.Select(x => new YssxUser
                {
                    Id = IdWorker.NextId(),
                    ClientId = Guid.NewGuid(),
                    MobilePhone = x.UserName,
                    NikeName = x.Name,
                    RealName = x.Name,
                    Password = "E10ADC3949BA59ABBE56E057F20F883E", //x.Password,
                    TenantId = yssxSchool.Id,
                    UserType = x.UserTypeId,
                    Status = 1,
                    RegistrationType = 1,
                    IsDelete = 0,
                    CreateTime = DateTime.Now,
                    CreateBy = x.Id
                }).ToList();

                List<ScUser> studentsUser = scUserlist.Where(x => x.UserTypeId == 1).ToList();
                List<YssxStudent> yssxStudents = studentsUser.Select(x => new YssxStudent
                {
                    Id = IdWorker.NextId(),
                    UserId = yssxUsers.Find(a => a.CreateBy == x.Id).Id,
                    StudentNo = x.StudentId > 0 ? x.StudentId.ToString() : null,
                    ClassId = classes.Where(a => a.CreateBy == x.ClassNo).FirstOrDefault().Id,
                    CollegeId = colleges.Where(a => a.CreateBy == x.TieNo).FirstOrDefault().Id,
                    Name = x.Name,
                    ProfessionId = professions.Find(a => a.CreateBy == x.TieNo).Id,
                    TenantId = yssxSchool.Id,
                    CreateTime = DateTime.Now

                }).ToList();

                List<ScUser> teachersUser = scUserlist.Where(x => x.UserTypeId == 2).ToList();

                List<YssxTeacher> teachers = teachersUser.Select(x => new YssxTeacher
                {
                    Id = IdWorker.NextId(),
                    Name = x.Name,
                    TenantId = yssxSchool.Id,
                    UserId = yssxUsers.Find(a => a.CreateBy == x.Id).Id,
                    TeacherNo = x.StudentId > 0 ? x.StudentId.ToString() : null,
                    CreateTime = DateTime.Now
                }).ToList();

                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    YssxSchool a = await DbContext.FreeSql.GetRepository<YssxSchool>().InsertAsync(yssxSchool);
                    List<YssxCollege> b = await DbContext.FreeSql.GetRepository<YssxCollege>().InsertAsync(colleges);
                    List<YssxProfession> c = await DbContext.FreeSql.GetRepository<YssxProfession>().InsertAsync(professions);
                    List<YssxClass> d = await DbContext.FreeSql.GetRepository<YssxClass>().InsertAsync(classes);
                    List<YssxUser> e = await DbContext.FreeSql.GetRepository<YssxUser>().InsertAsync(yssxUsers);
                    List<YssxStudent> f = await DbContext.FreeSql.GetRepository<YssxStudent>().InsertAsync(yssxStudents);
                    List<YssxTeacher> g = await DbContext.FreeSql.GetRepository<YssxTeacher>().InsertAsync(teachers);
                    bool state = a != null && b.Count > 0 && c.Count() > 0 && d.Count() > 0 && e.Count() > 0 && f.Count > 0 && g.Count > 0;
                    if (state)
                        uow.Commit();
                    else uow.Rollback();
                }
            }
        }


        static async void UpdateUser(long schoolId, int type)
        {
            if (type == 0)
            {
                List<YssxStudent> students = await DbContext.FreeSql.GetRepository<YssxStudent>().Where(a => a.TenantId == schoolId).ToListAsync();
                foreach (var item in students)
                {
                    int n = await DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.Set(x => x.UserType, 1).Set(x => x.TenantId, schoolId).Where(x => x.Id == item.UserId).ExecuteAffrowsAsync();
                }
            }
            else
            {
                List<YssxTeacher> students = await DbContext.FreeSql.GetRepository<YssxTeacher>().Where(a => a.TenantId == schoolId).ToListAsync();
                foreach (var item in students)
                {
                    int n = await DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.Set(x => x.UserType, 2).Set(x => x.TenantId, schoolId).Where(x => x.Id == item.UserId).ExecuteAffrowsAsync();
                }
            }

        }

        static async void RegionData()
        {
            List<YssxRegionStatistics> list = new List<YssxRegionStatistics>();
            List<string> regs = new List<string>
            {
                "安徽",
                "北京",
                "重庆",
                "福建",
                "甘肃",
                "广东",
                "广西",
                "贵州",
                "海南",
                "河北",
                "黑龙江",
                "河南",
                "湖北",
                "湖南",
                "江苏",
                "江西",
                "吉林",
                "辽宁",
                "内蒙",
                "宁夏",
                "青海",
                "陕西",
                "山东",
                "上海",
                "山西",
                "四川",
                "天津",
                "香港",
                "新疆",
                "西藏",
                "云南",
                "浙江",

            };
            List<long> counts = new List<long>
            {
                20777,
                133430,
                11564,
                18731,
                149477,
                386988,
                11611606,
                162623,
                53376,
                197019,
                5388,
                221166,
                23558,
                20799787,
                244094,
                61392,
                5357,
                1255,
                21643,
                28786,
                4774,
                47922,
                78251,
                47519,
                210803,
                88690,
                1187510,
                1015,
                66708,
                1394,
                20625,
                28925
            };

            for (int i = 0; i < regs.Count; i++)
            {
                YssxRegionStatistics x = new YssxRegionStatistics();
                x.Name = regs[i];
                x.Month = 7;
                x.Date = "2020-11-18";
                x.Count = counts[i];
                x.Id = IdWorker.NextId();
                list.Add(x);
            }

            await DbContext.FreeSql.GetRepository<YssxRegionStatistics>().InsertAsync(list);
        }

        static async void JsData(int eType = 2)
        {
            // List<YssxPosition> yssxPositions = await DbContext.FreeSql2.GetRepository<YssxPosition>().Where(x => x.IsDelete == 0).ToListAsync();
            //List<YssxCasePosition> positions = await DbContext.FreeSql2.GetRepository<YssxCasePosition>().Where(x => x.IsDelete == 0).ToListAsync();

            List<YssxCaseJs> cases = await DbContext.FreeSql2.GetRepository<YssxCaseJs>().Where(x => x.IsDelete == 0 && x.Id == 972246573513601).ToListAsync();

            List<YssxCase> listCase = cases.Select(x => new YssxCase
            {
                Id = x.Id,
                AccountEntryScale = x.AccountEntryScale,
                AccountingManager = x.AccountingManager,
                AccountingPeriodDate = x.AccountingPeriodDate,
                Auditor = x.Auditor,
                BusinessAccountant = x.BusinessAccountant,
                CaseYear = x.CaseYear,
                Code = x.Code,
                Cashier = x.Cashier,
                CostAccountant = x.CostAccountant,
                CompetitionType = x.CompetitionType,
                CreateBy = x.CreateBy,
                CreateTime = DateTime.Now,
                Education = x.Education,
                EnterpriseInfo = x.EnterpriseInfo,
                FillGridScale = x.FillGridScale,
                InVal = x.InVal,
                IsBigData = x.IsBigData,
                Name = x.Name,
                OfficialStatus = x.OfficialStatus,
                OutVal = x.OutVal,
                Region = x.Region,
                RollUpType = x.RollUpType,
                SetType = x.SetType,
                Sort = x.Sort,
                TaxAccountant = x.TaxAccountant,
                TestStatus = x.TestStatus,
                TotalTime = x.TotalTime,

            }).ToList();

            //List<YssxTopic> topics = await DbContext.FreeSql2.GetRepository<YssxTopic>().Where(x => x.IsDelete == 0).ToListAsync();
            //List<YssxAnswerOption> options = await DbContext.FreeSql2.GetRepository<YssxAnswerOption>().Where(x => x.IsDelete == 0).ToListAsync();
            //List<YssxTopicFile> topicFiles = await DbContext.FreeSql2.GetRepository<YssxTopicFile>().Where(x => x.IsDelete == 0).ToListAsync();
            //List<YssxCertificateTopic> certificateTopics = await DbContext.FreeSql2.GetRepository<YssxCertificateTopic>().Where(x => x.IsDelete == 0).ToListAsync();
            //List<YssxCertificateTopic2> certificateTopics2 = certificateTopics.Select(x => new YssxCertificateTopic2 
            //{
            //    Id=x.Id,
            //    AccountingManager=x.AccountingManager,
            //    Auditor=x.Auditor,
            //    CalculationScoreType=(int)x.CalculationScoreType,
            //    Cashier=x.Cashier,
            //    CertificateDate=x.CertificateDate,
            //    CertificateNo=x.CertificateNo,
            //    CreateTime=x.CreateTime,
            //    CreateBy=200,
            //    Creator=x.Creator,
            //    Date=x.Date,
            //    EnterpriseId=x.EnterpriseId,
            //    ImageIds=x.ImageIds,
            //    InVal=x.InVal,
            //    InvoicesNum=x.InvoicesNum,
            //    IsDisableAM=(int)x.IsDisableAM,
            //    IsDisableAuditor=(int)x.IsDisableAuditor,
            //    IsDisableCashier=(int)x.IsDisableCashier,
            //    IsDisableCreator=(int)x.IsDisableCreator,
            //    OutVal=x.OutVal,
            //    Sort=x.Sort,
            //    Title=x.Title,
            //    TopicId=x.TopicId,
            //    Type=x.Type,
            //    UpdateTime=x.UpdateTime,

            //}).ToList();
            //List<YssxSubject> subjects = await DbContext.FreeSql2.GetRepository<YssxSubject>().Where(x => x.IsDelete == 0).ToListAsync();
            //List<YssxSummary> summaries = await DbContext.FreeSql2.GetRepository<YssxSummary>().Where(x => x.IsDelete == 0).ToListAsync();
            // List<YssxCertificateDataRecord> records = await DbContext.FreeSql2.GetRepository<YssxCertificateDataRecord>().Where(x => x.IsDelete == 0).ToListAsync();
            // topics= topics.Skip()
            //topics.ForEach(x => 
            //{
            //    x.CalculationType=
            //});
            //List<YssxTopic2> topics2 = new List<YssxTopic2>();

            //foreach (var x in topics)
            //{
            //    YssxTopic2 d = new YssxTopic2();
            //    d.Id = x.Id;
            //    d.AnswerValue = x.AnswerValue;
            //    d.CalculationType = GetCalculationType(x.CalculationType);
            //    d.CaseId = x.CaseId;
            //    d.Content = x.Content;
            //    d.CreateTime = x.CreateTime;
            //    d.DifficultyLevel = x.DifficultyLevel;
            //    d.Education = x.Education;
            //    d.FullContent = x.FullContent;
            //    d.Hint = x.Hint;
            //    d.IsCopy = x.IsCopy;
            //    d.IsCopyBigData = x.IsCopyBigData;
            //    d.IsDisorder = x.IsDisorder;
            //    d.IsGzip = x.IsGzip;
            //    d.IsSettlement = x.IsSettlement;
            //    d.Keywords = x.Keywords;
            //    d.ParentId = x.ParentId;
            //    d.PartialScore = x.PartialScore;
            //    d.PositionId = x.PositionId;
            //    d.QuestionContentType = GetQuestionContentType(x.QuestionContentType);
            //    d.QuestionType = GetQuestionType(x.QuestionType);
            //    d.Score = x.Score;
            //    d.Sort = x.Sort;
            //    d.Status = 1;
            //    d.Title = x.Title;
            //    d.TopicContent = x.TopicContent;
            //    d.TopicFileIds = x.TopicFileIds;
            //    d.AuditStatus = 1;
            //    topics2.Add(d);
            //}
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    // List<YssxPosition> list0 = await DbContext.FreeSql.GetRepository<YssxPosition>().InsertAsync(yssxPositions);
                    //List<YssxCasePosition> list1 = await DbContext.FreeSql.GetRepository<YssxCasePosition>().InsertAsync(positions);
                    List<YssxCase> list2 = await DbContext.FreeSql.GetRepository<YssxCase>().InsertAsync(listCase);
                    //List<YssxTopic2> list3 = await DbContext.FreeSql.GetRepository<YssxTopic2>().InsertAsync(topics2);
                    // List<YssxAnswerOption> list4 = await DbContext.FreeSql.GetRepository<YssxAnswerOption>().InsertAsync(options);
                    //List<YssxTopicFile> list5 = await DbContext.FreeSql.GetRepository<YssxTopicFile>().InsertAsync(topicFiles);
                    //List<YssxCertificateTopic2> list6 = await DbContext.FreeSql.GetRepository<YssxCertificateTopic2>().InsertAsync(certificateTopics2);
                    //List<YssxSubject> list7 = await DbContext.FreeSql.GetRepository<YssxSubject>().InsertAsync(subjects);
                    //List<YssxSummary> list8 = await DbContext.FreeSql.GetRepository<YssxSummary>().InsertAsync(summaries);
                    //List<YssxCertificateDataRecord> list9 = await DbContext.FreeSql.GetRepository<YssxCertificateDataRecord>().InsertAsync(records);

                    //  bool state = list0.Count > 0 && list1.Count > 0 && list2.Count > 0  && list4.Count > 0 && list5.Count > 0 && list6.Count > 0 && list7.Count > 0 && list8.Count > 0 && list9.Count > 0;
                    bool state = true; //list3.Count > 0;
                    if (state) uow.Commit();
                    else uow.Rollback();
                }
                catch (Exception ex)
                {

                    uow.Rollback();
                }

            }

        }

        static int GetCalculationType(CalculationType type)
        {
            int x = 0;
            switch (type)
            {
                case CalculationType.None:
                    x = 0;
                    break;
                case CalculationType.CalculatedRow:
                    x = 1;
                    break;
                case CalculationType.CalculatedColumn:
                    x = 2;
                    break;
                case CalculationType.AvgCellCount:
                    x = 3;
                    break;
                case CalculationType.FreeCalculation:
                    x = 4;
                    break;
                default:
                    break;
            }
            return x;
        }


        static int GetQuestionContentType(QuestionContentType type)
        {
            int x = 0;
            switch (type)
            {
                case QuestionContentType.Text:
                    x = 0;
                    break;
                case QuestionContentType.Grid:
                    x = 1;
                    break;
                case QuestionContentType.Img:
                    x = 2;
                    break;
                default:
                    break;
            }
            return x;
        }

        static int GetQuestionType(QuestionType type)
        {
            int x = 0;
            switch (type)
            {
                case QuestionType.SingleChoice:
                    x = 0;
                    break;
                case QuestionType.MultiChoice:
                    x = 1;
                    break;
                case QuestionType.Judge:
                    x = 2;
                    break;
                case QuestionType.FillGrid:
                    x = 3;
                    break;
                case QuestionType.AccountEntry:
                    x = 4;
                    break;
                case QuestionType.FillBlank:
                    x = 5;
                    break;
                case QuestionType.FillGraphGrid:
                    x = 6;
                    break;
                case QuestionType.MainSubQuestion:
                    x = 7;
                    break;
                case QuestionType.SettleAccounts:
                    x = 8;
                    break;
                case QuestionType.GridFillBank:
                    x = 9;
                    break;
                case QuestionType.FinancialStatements:
                    x = 10;
                    break;
                default:
                    break;
            }
            return x;
        }

        static SchoolType GetType(int t)
        {
            SchoolType schoolType = SchoolType.SecondaryCollege;
            switch (t)
            {
                case 0: schoolType = SchoolType.SecondaryCollege; break;
                case 1: schoolType = SchoolType.JuniorCollege; break;
                case 2: schoolType = SchoolType.UndergraduateSchool; break;
                default:
                    break;
            }
            return schoolType;
        }
    }
}













































































































