﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 已审核票据表
    /// </summary>
    [Table(Name = "sx_audited_bill")]
    public class SxAuditedBill : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 月度任务Id【Obsolete】
        /// </summary>
        public long MonthTaskId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 父级题目Id
        /// </summary>
        public long ParentQuestionId { get; set; }

        /// <summary>
        /// 题目所属岗位Id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 业务名称
        /// </summary>
        public string BusinessName { get; set; }

        /// <summary>
        /// 业务日期(0:无干扰票记录业务日期 1:有干扰票及时推送记录业务日期 2:有干扰票选择时间推送记录推送日期)
        /// </summary>
        public DateTime BusinessDate { get; set; }

        /// <summary>
        /// 是否有收付款 false:没有 true:有
        /// </summary>
        public bool IsHasReceivePayment { get; set; }

        /// <summary>
        /// 收付款题类型
        /// </summary>
        [Column(DbType = "int")]
        public ReceivePaymentType ReceivePaymentType { get; set; }

        /// <summary>
        /// 收付款是否已完成 false:未完成 true:已完成
        /// </summary>
        public bool IsDoneReceivePayment { get; set; }

    }
}
