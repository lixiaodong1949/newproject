﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 票据推送表
    /// </summary>
    [Table(Name = "sx_bill_push")]
    public class SxBillPush : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 月度任务Id【Obsolete】
        /// </summary>
        public long MonthTaskId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 父级题目Id
        /// </summary>
        public long ParentQuestionId { get; set; }

        /// <summary>
        /// 题目所属岗位Id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 业务名称
        /// </summary>
        public string BusinessName { get; set; }

        /// <summary>
        /// 推送日期
        /// </summary>
        public DateTime PushDate { get; set; }
    }
}
