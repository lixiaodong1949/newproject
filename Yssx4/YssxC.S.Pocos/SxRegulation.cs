﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 规章制度模板表
    /// </summary>
    [Table(Name = "sx_regulation")]
    public class SxRegulation : BizBaseEntity<long>
    {
        /// <summary>
        /// 制度名称
        /// </summary>
        [Column(Name = "name", DbType = "varchar(200)",IsNullable =false)]
        public string Name { get; set; }
        /// <summary>
        /// 制度描述
        /// </summary>
        [Column(Name = "description", DbType = "varchar(4000)", IsNullable = true)]
        public string Description { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }
        /// <summary>
        /// 父节点
        /// </summary>
        public long ParentId { get; set; }

    }
}
