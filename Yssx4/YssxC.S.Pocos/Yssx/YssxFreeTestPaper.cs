﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 赠送试卷表 - 操作记录
    /// </summary>
    [Table(Name = "yssx_free_test_paper")]
    public class YssxFreeTestPaper : BizBaseEntity<long>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 标的物id
        /// </summary>
        public long TargetId { get; set; }

        /// <summary>
        /// 标的物名称
        /// </summary>
        public string TargetName { get; set; }

        /// <summary>
        /// 来源 （1.云实习真账实操 2.课程同步实训 3.业税财实训 4.云上实训之岗位实训 5.课程实训 6.经验案例 7.初级会计考证）
        /// </summary>
        public int Source { get; set; }

    }
}
