﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 案例标签关联
    /// </summary>
    [Table(Name = "sx_case_tag")]
    public class SxCaseTag : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 标签Id
        /// </summary>
        public long TagId { get; set; }
    }
}
