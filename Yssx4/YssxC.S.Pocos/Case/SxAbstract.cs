﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 摘要
    /// </summary>
    [Table(Name = "sx_abstract")]
    public class SxAbstract : BizBaseEntity<long>
    {
        /// <summary>
        /// 摘要名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 外键，案例Id
        /// </summary>
        public long CaseId { get; set; }
    }
}
