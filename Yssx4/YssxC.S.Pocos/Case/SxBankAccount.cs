﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 公司资产账户
    /// </summary>
    [Table(Name = "sx_bankaccount")]
    public class SxBankAccount : BizBaseEntity<long>
    {
        /// <summary>
        /// 开户行
        /// </summary>
        public string OpeningBank { get; set; }

        /// <summary>
        /// 支行
        /// </summary>
        public string Branch { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string AccountNo { get; set; }

        /// <summary>
        /// 余额
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// 外键，案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 设置默认账户
        /// </summary>
        public bool Default { get; set; }
    }
}
