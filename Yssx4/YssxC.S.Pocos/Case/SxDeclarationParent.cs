﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 申报表主表
    /// </summary>
    [Table(Name = "sx_declaration_parent")]
    public class SxDeclarationParent : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 子表数
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 报表内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 所属开始日期
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// 所属终止日期
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// 申报日期
        /// </summary>
        public DateTime DeclarationDate { get; set; }

        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }
    }
}
