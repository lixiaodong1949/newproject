﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 案例规章制度
    /// </summary>
    [Table(Name = "sx_case_regulation")]
    public class SxCaseRegulation : BizBaseEntity<long>
    {
        /// <summary>
        /// 制度名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(200)", IsNullable = false)]
        public string Name { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }
        /// <summary>
        /// 规章制度模板Id
        /// </summary>
        public long RegulationId { get; set; }

        /// <summary>
        /// 规章制度模板父Id
        /// </summary>
        public long RegulationParentId { get; set; }
        /// <summary>
        /// 公司Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 应用时间
        /// </summary>
        [Column(Name = "UseDate", DbType = "varchar(50)", IsNullable = true)]
        public string UseDate { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column(DbType = "int")]
        public Status Status { get; set; } = Status.Enable;
    }
}
