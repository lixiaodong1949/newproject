﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 案例岗位关联
    /// </summary>
    [Table(Name = "sx_case_position")]
    public class SxCasePosition : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 岗位id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 岗位类型
        /// </summary>
        [Column(DbType = "int")]
        public PositionType PositionType { get; set; } = PositionType.composite;
    }
}
