﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 任务详情
    /// </summary>
    [Table(Name = "sx_task_item")]
    public class SxTaskItem : BizBaseEntity<long>
    {

        /// <summary>
        /// 任务日期
        /// </summary>
        public DateTime IssueDate { get; set; }

        /// <summary>
        /// 任务描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 外键，任务Id
        /// </summary>
        public long TaskId { get; set; }
    }
}
