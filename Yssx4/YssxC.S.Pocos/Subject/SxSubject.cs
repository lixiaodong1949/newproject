﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos.Subject
{

    /// <summary>
    /// 案例科目表
    /// </summary>
    [Table(Name = "zdap_subject")]
    public class SxSubject : BizBaseEntity<long>
    {
        /// <summary>
        /// 科目代码
        /// </summary>
        [Column(Name = "SubjectCode", DbType = "varchar(20)", IsNullable = false)]
        public long SubjectCode { get; set; }

        /// <summary>
        /// 父级ID 最多四级
        /// </summary>
        [Column(Name = "ParentId", DbType = "varchar(20)", IsNullable = true)]
        public long ParentId { get; set; }

        /// <summary>
        /// 案列ID
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 一级代码
        /// </summary>
        [Column(Name = "Code1", DbType = "varchar(20)", IsNullable = true)]
        public string Code1 { get; set; }

        /// <summary>
        /// 二级代码
        /// </summary>
        [Column(Name = "Code2", DbType = "varchar(20)", IsNullable = true)]
        public string Code2 { get; set; }

        /// <summary>
        /// 三级代码
        /// </summary>
        [Column(Name = "Code3", DbType = "varchar(20)", IsNullable = true)]
        public string Code3 { get; set; }

        /// <summary>
        /// 四级代码
        /// </summary>
        [Column(Name = "Code4", DbType = "varchar(20)", IsNullable = true)]
        public string Code4 { get; set; }

        /// <summary>
        /// 科目名称
        /// </summary>
        [Column(Name = "SubjectName", DbType = "varchar(100)", IsNullable = true)]
        public string SubjectName { get; set; }

        /// <summary>
        /// 余额方向
        /// </summary>
        [Column(Name = "Direction", DbType = "varchar(20)", IsNullable = true)]
        public string Direction { get; set; }

        /// <summary>
        /// 科目类型(0 全部 资产1 负债 2 权益 4 成本5 损益6)
        /// </summary>
        public int SubjectType { get; set; }

        /// <summary>
        /// 准则类型 0:小企业1:企业会计2:事业单位3:非盈利组织
        /// </summary>
       // public int NormType { get; set; }

        /// <summary>
        /// 助记码
        /// </summary>
        [Column(Name = "AssistCode", DbType = "varchar(50)", IsNullable = true)]
        public string AssistCode { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        [Column(Name = "BorrowAmount", DbType = "decimal(15,2)")]
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        [Column(Name = "CreditorAmount", DbType = "decimal(15,2)")]
        public decimal CreditorAmount { get; set; }

        /// <summary>
        /// 是否有核算0:没有 1:有
        /// </summary>
        public int IsHaveCheck { get; set; }

        [Column(IsIgnore = true)]
        public long AssistacId { get; set; }

        [Column(IsIgnore = true)]
        public long AssistacParentId { get; set; }
        /// <summary>
        /// 损溢科目类别 0-A类 1-B类 2-C类 3-D类 4-E类
        /// </summary>
        public ProfitLossSubjectType ProfitLossSubjectType {get;set;}
    }



}
