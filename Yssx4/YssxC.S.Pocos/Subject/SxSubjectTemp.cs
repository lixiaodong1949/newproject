﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Pocos.Subject
{
    [Table(Name = "sx_subject_temp")]
    public class SxSubjectTemp
    {
        public long Id { get; set; }

        public long SubjectCode { get; set; }

        /// <summary>
        /// 科目名称
        /// </summary>
        public string SubjectName { get; set; }

        /// <summary>
        /// 一级代码
        /// </summary>
        [Column(Name = "Code1", DbType = "varchar(20)", IsNullable = true)]
        public string Code1 { get; set; }

        /// <summary>
        /// 余额方向
        /// </summary>
        [Column(Name = "Direction", DbType = "varchar(20)", IsNullable = true)]
        public string Direction { get; set; }

        /// <summary>
        /// 科目类型(1:资产,2:负债)
        /// </summary>
        public int SubjectType { get; set; }

        /// <summary>
        /// 准则类型 0:小企业1:企业会计2:事业单位3:非盈利组织
        /// </summary>
        public int NormType { get; set; }

        /// <summary>
        /// 助记码
        /// </summary>
        [Column(Name = "AssistCode", DbType = "varchar(50)", IsNullable = true)]
        public string AssistCode { get; set; }
    }
}
