﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 分录题目中凭证选择的科目辅助核算信息
    /// </summary>
    [Table(Name = "sx_certificate_assist_accounting")]
    public class SxCertificateAssistAccounting : BizBaseEntity<long>
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long TopicId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 辅助核算类型
        /// </summary>
        [Column(DbType = "int")]
        public AssistAccountingType AssistAccountingType { get; set; }
        /// <summary>
        /// 科目Id
        /// </summary>
        public long SubjectId { get; set; }
    }
}
