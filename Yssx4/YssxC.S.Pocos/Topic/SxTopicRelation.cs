﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 题目管理表
    /// </summary>
    [Table(Name = "sx_topic_relation")]
    public class SxTopicRelation : BizBaseEntity<long>
    {
        /// <summary>
        /// 题目Id(关联父题目)
        /// </summary>
        public long TopicId { get; set; }
        /// <summary>
        ///  前置题目的题目id
        /// </summary>
        public long PreTopicId { get; set; }
        /// <summary>
        /// 前置题目的父题目id
        /// </summary>
        public long PreTopicParentId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        public bool IsShow { get; set; }
        public int RelationTopicType { get; set; }
    }
}
