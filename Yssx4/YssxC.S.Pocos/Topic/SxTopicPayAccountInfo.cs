﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 题目银行收付款账户信息
    /// </summary>
    [Table(Name = "sx_topic_pay_accountinfo")]
    public class SxTopicPayAccountInfo : BizBaseEntity<long>
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long TopicId { get; set; }
        /// <summary>
        /// 父题目Id
        /// </summary>
        public long ParentTopicId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 付款人银行Id【付款题使用】
        /// </summary>
        public long PayBankId { get; set; }
        /// <summary>
        /// 付款单位（公司、个人）【收款题使用】
        /// </summary>
        public long PayCompanyId { get; set; }
        /// <summary>
        /// 收款银行【收款题使用】
        /// </summary>
        public long PayeeBankId { get; set; }
        /// <summary>
        /// 收款单位（公司、个人）【付款题使用】
        /// </summary>
        public long PayeeCompanyId { get; set; }
        /// <summary>
        /// 金额
        /// </summary>  
        [Column(Name = "Amount", DbType = "decimal(15,4)")]
        public decimal Amount { get; set; }
    }
}
