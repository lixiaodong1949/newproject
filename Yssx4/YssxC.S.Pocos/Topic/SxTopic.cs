﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 题目主表
    /// </summary>
    [Table(Name = "sx_topic")]
    public class SxTopic : BizBaseEntity<long>
    {
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime BusinessDate { get; set; }
        /// <summary>
        /// 题目分组Id
        /// </summary>
        public long GroupId { get; set; }
        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 分岗所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PointPositionId { get; set; }
        /// <summary>
        /// 是否结账题（0 否 1是）
        /// </summary>
        public int IsSettlement { get; set; }
        /// <summary>
        /// 结账类型（0 转账 1现金）(默认：0)
        /// </summary>
        public int SettlementType { get; set; }
        /// <summary>
        /// 是否压缩（0 否 1是）
        /// </summary>
        public int IsGzip { get; set; }
        /// <summary>
        /// 题目标题
        /// </summary>
        [Column(Name = "Title", DbType = "varchar(255)", IsNullable = true)]
        public string Title { get; set; }
        /// <summary>
        /// 题干内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        [Column(DbType = "int")]
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 题目内容类型(1:纯文字,2:表格,3:图片)
        /// </summary>
        [Column(DbType = "int")]
        public QuestionContentType QuestionContentType { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>
        [Column(Name = "DifficultyLevel", DbType = "int(255)", IsNullable = true)]
        public int? DifficultyLevel { get; set; }
        ///// <summary>
        ///// 关键字
        ///// </summary>
        //public string Keywords { get; set; }

        /// <summary>
        /// 模板数据源内容(不含答案)
        /// </summary>
        [Column(Name = "TopicContent", DbType = "LongText", IsNullable = true)]
        public string TopicContent { get; set; }
        /// <summary>
        /// 模板数据源内容(含答案)
        /// </summary>
        [Column(Name = "FullContent", DbType = "LongText", IsNullable = true)]
        public string FullContent { get; set; }

        /// <summary>
        /// 答案 内容（选择题答案a,b,c等，判断题true/false，表格json等）
        /// </summary>
        [Column(Name = "AnswerValue", DbType = "LongText", IsNullable = true)]
        public string AnswerValue { get; set; }
        /// <summary>
        /// 完整答案（包含不计分内容）
        /// </summary>
        [Column(Name = "AllAnswerValue", DbType = "LongText", IsNullable = true)]
        public string AllAnswerValue { get; set; }
        /// <summary>
        /// 答案解析
        /// </summary>
        [Column(Name = "Hint", DbType = "LongText", IsNullable = true)]
        public string Hint { get; set; }
        /// <summary>
        /// 题目提示（做题时的提示，分开结转提示）
        /// </summary>
        [Column(Name = "TopicHint", DbType = "LongText", IsNullable = true)]
        public string TopicHint { get; set; }
        /// <summary>
        /// 师傅提示
        /// </summary>
        [Column(Name = "TeacherHint", DbType = "LongText", IsNullable = true)]
        public string TeacherHint { get; set; }

        /// <summary>
        /// 父节点ID-用于多题型(一个题目 多个作答区)
        /// </summary>
        [Column(Name = "ParentId", DbType = "bigint(20)", IsNullable = true)]
        public long ParentId { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10,4)")]
        public decimal Score { get; set; }
        ///// <summary>
        ///// 非完整性得分
        ///// </summary>
        //[Column(Name = "PartialScore", DbType = "decimal(10,4)", IsNullable = true)]
        //public decimal PartialScore { get; set; } = 0;
        ///// <summary>
        ///// 学历标记（0 中职 1高职 2 本科）
        ///// </summary>
        //public int Education { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column(DbType = "int")]
        public Status Status { get; set; } = Status.Enable;
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 会计期间
        /// </summary>
        [Column(Name = "AccountingPeriodDate", DbType = "varchar(50)", IsNullable = true)]
        public string AccountingPeriodDate { get; set; }

        /// <summary>
        /// 题目附件文件id（多个id用,隔开）
        /// </summary>
        [Column(Name = "TopicFileIds", DbType = "varchar(500)", IsNullable = true)]
        public string TopicFileIds { get; set; }

        ///// <summary>
        ///// 审核状态(0,待审核，1，审核成功，2，审核拒绝)
        ///// </summary>
        //public int AuditStatus { get; set; }

        ///// <summary>
        ///// 审核说明
        ///// </summary>
        //public string AuditStatusExplain { get; set; }

        ///// <summary>
        ///// 是否上架(0,待上架，1，上架待审，2，上架)
        ///// </summary>
        //public int Uppershelf { get; set; }

        ///// <summary>
        ///// 上架说明
        ///// </summary>
        //public string UppershelfExplain { get; set; }

        /// <summary>
        /// 查看答案
        /// </summary>
        public bool IsShowAnswer { get; set; }

        /// <summary>
        /// 查看解析
        /// </summary>
        public bool IsShowHint { get; set; }
        /// <summary>
        /// 是否是干扰题
        /// </summary>
        public bool IsTrap { get; set; } = false;
        /// <summary>
        /// 题目关联是否开启
        /// </summary>
        public bool TopicRelationStatus { get; set; }

        #region 综合分录题

        /// <summary>
        /// 是否需要收付款
        /// </summary>
        public bool IsReceipt { get; set; } = false;
        /// <summary>
        /// 收付款题类型
        /// </summary>
        [Column(DbType = "int")]
        public ReceivePaymentType ReceivePaymentType { get; set; } = ReceivePaymentType.None;

        /// <summary>
        /// 推送时间
        /// </summary>
        public DateTime? PushDate { get; set; }

        /// <summary>
        /// 会计主管分数 (保存在综合分录题中的子分录题中)
        /// </summary>
        public decimal DisableAMScore { get; set; }
        /// <summary>
        /// 出纳分数 (保存在综合分录题中的子分录题中)
        /// </summary>
        public decimal DisableCashierScore { get; set; }

        #endregion

        #region 财务表报题专业

        ///// <summary>
        ///// 纳税申报报表分数
        ///// </summary>
        //public decimal DeclarationScore { get; set; }
        /// <summary>
        /// 纳税申报所属期起
        /// </summary>
        public DateTime? DeclarationDateStart { get; set; }
        /// <summary>
        /// 纳税申报所属期止
        /// </summary>
        public DateTime? DeclarationDateEnd { get; set; }
        /// <summary>
        /// 纳税申报期限
        /// </summary>
        public DateTime? DeclarationLimitDate { get; set; }

        #endregion

        #region 表格题专用
        /// <summary>
        /// 表格题-计算方式（按行计算、按列计算、按单元格平均计算、自由计算）
        /// </summary>
        [Column(DbType = "int")]
        public CalculationType CalculationType { get; set; } = CalculationType.None;
        /// <summary>
        /// 表格题-是否开启复制Excel数据（0 否 1是）
        /// </summary>
        public int IsCopy { get; set; } = CommonConstants.IsNoCopy;
        /// <summary>
        /// 表格题-是否开启复制大数据（0 否 1是）
        /// </summary>
        public int IsCopyBigData { get; set; } = CommonConstants.IsNoCopy;
        /// <summary>
        /// 表格题-是否无序（0 否 1是）
        /// </summary>
        public int IsDisorder { get; set; } = CommonConstants.IsNoDisorder;

        #endregion

        #region 申报表题 专用

        /// <summary>
        /// 申报表Id 申报表题
        /// </summary>
        public long DeclarationId { get; set; }
        /// <summary>
        /// 申报表Id 申报表题
        /// </summary>
        public long ParentDeclarationId { get; set; }
        /// <summary>
        /// 申报表分数，仅用作后台题目列表分数统计使用
        /// </summary>
        [Column(Name = "DeclarationScore", DbType = "decimal(10,4)")]
        public decimal DeclarationScore { get; set; }

        #endregion

        /// <summary>
        /// 技能Id
        /// </summary>
        public long SkillId { get; set; }
    }
}
