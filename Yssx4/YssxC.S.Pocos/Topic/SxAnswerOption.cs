﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 题目选项和答案对象
    /// </summary>
    [Table(Name = "sx_answer_option")]
    public class SxAnswerOption : BizBaseEntity<long>
    {
        /// <summary>
        /// 题目ID
        /// </summary>
        public long TopicId { get; set; }
        /// <summary>
        /// 题目父ID
        /// </summary>
        public long TopicParentId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 复杂表格- 答案 内容（Json文件）
        /// </summary>
        [Column(Name = "AnswerKeysContent", DbType = "text", IsNullable = true)]
        public string AnswerKeysContent { get; set; }

        /// <summary>
        /// 选项名称(如A B C )
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 答案选项(具体选项内容)
        /// </summary>
        [Column(Name = "AnswerOption", DbType = "varchar(255)", IsNullable = true)]
        public string AnswerOption { get; set; }
        /// <summary>
        /// 图片附件--用于其他竞赛
        /// </summary>
        public string AnswerFileUrl { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

    }
}
