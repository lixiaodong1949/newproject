﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 考生练习凭证记录数据
    /// </summary>
    [Table(Name = "sx_exam_certificate_datarecord")]
    public class SxExamCertificateDataRecord : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 考试ID
        /// </summary>
        public long ExamId { get; set; }
        /// <summary>
        /// 作答ID
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 作答明细Id
        /// </summary>
        public long GradeDetailId { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }

        /// <summary>
        /// 凭证字名称
        /// </summary>
        public string CertificateName { get; set; }

        /// <summary>
        /// 案例ID
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 摘要信息
        /// </summary>
        public string SummaryInfo { get; set; }

        /// <summary>
        /// 科目Id
        /// </summary>
        public long SubjectId { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        [Column(DbType = "decimal(18,2)")]
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        [Column(DbType = "decimal(18,2)")]
        public decimal CreditorAmount { get; set; }

        /// <summary>
        /// 凭证日期
        /// </summary>
        public DateTime? CertificateDate { get; set; }
        /// <summary>
        /// 辅助核算ID
        /// </summary>
        public long AssistId { get; set; }
    }
}
