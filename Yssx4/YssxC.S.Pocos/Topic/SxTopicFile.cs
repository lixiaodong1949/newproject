﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 题目附件表
    /// </summary>
    [Table(Name = "sx_topic_file")]
    public class SxTopicFile : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 题目id
        /// </summary>
        public long TopicId { get; set; }
        /// <summary>
        /// 父题目id
        /// </summary>
        public long ParentTopicId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 图片路径
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 水印
        /// </summary>
        public bool IsWatermark { get; set; }
        /// <summary>
        /// 封面底面：1封面 2底面
        /// </summary>
        [Column(DbType = "int")]
        public TopicFileCoverBack CoverBack { get; set; }
        /// <summary>
        /// 文件类型
        /// </summary>
        [Column(DbType = "int")]
        public TopicFileType Type { get; set; }
    }
}
