﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 账簿-辅助核算
    /// </summary>
    [Table(Name = "sx_assistac_counting")]
    public class SxAssistAccounting : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 父Id (该字段不在使用，辅助核算没有层级关系)
        /// </summary>
        [Column(DbType = "long", IsNullable = true)]
        public long ParentId { get; set; }
        /// <summary>
        /// 分组Id
        /// </summary>
        [Column(DbType = "long", IsNullable = true)]
        public long GroupId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 类别（供应商vendor、职员employee、客户 customer）
        /// </summary>
        [Column(DbType = "int", IsNullable = true)]
        public AssistAccountingType? Type { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 科目
        /// </summary>
        [Column(DbType = "long", IsNullable = true)]
        public long? SubjectId { get; set; }
        /// <summary>
        /// 科目
        /// </summary>
        [Column(DbType = "long", IsNullable = true)]
        public long SubjectCode { get; set; }
        /// <summary>
        /// 账户
        /// </summary>
        public string BankAccountNo { get; set; }
        /// <summary>
        /// 开户行名称
        /// </summary>
        public string BankName { get; set; }
        /// <summary>
        /// 支行名称
        /// </summary>
        public string BankBranchName { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        [Column(Name = "BorrowAmount", DbType = "decimal(15,2)")]
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        [Column(Name = "CreditorAmount", DbType = "decimal(15,2)")]
        public decimal CreditorAmount { get; set; }
    }
}
