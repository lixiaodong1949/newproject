﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 能力维度表
    /// </summary>
    [Table(Name = "sx_user_skill")]
    public class SxUserSkill : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "SkillName", DbType = "varchar(255)")]
        public string SkillName { get; set; }

        /// <summary>
        /// 技能Id
        /// </summary>
        public long SkillId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        ///// <summary>
        ///// 内容
        ///// </summary>
        //[Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        //public string Content { get; set; }

        /// <summary>
        /// 总分
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10, 2)", IsNullable = true)]
        public decimal Score { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Column(Name = "Sort", DbType = "int(11)")]
        public int Sort { get; set; } = 0;

        ///// <summary>
        ///// 最大值
        ///// </summary>
        //[Column(Name = "Max", DbType = "int(11)")]
        //public int Max { get; set; }

    }
}
