using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 游戏场景表
    /// </summary>
    [Table(Name = "sx_game_scene")]
    public class SxGameScene : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)")]
        public string Name { get; set; }

        /// <summary>
        /// 分类Id
        /// </summary>
        [Column(Name = "CatgoryId", DbType = "bigint(20)", IsNullable = true)]
        public long CatgoryId { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 3D场景文件
        /// </summary>
        [Column(Name = "File3D", DbType = "varchar(4000)", IsNullable = true)]
        public string File3D { get; set; }

        /// <summary>
        /// 3D场景文件总时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 会话信息
        /// </summary>
        [Column(Name = "Dialogues", DbType = "LongText", IsNullable = true)]
        public string Dialogues { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; } = 0;

        /// <summary>
        /// 视频封面
        /// </summary>
        public string Logo { get; set; }
    }
}