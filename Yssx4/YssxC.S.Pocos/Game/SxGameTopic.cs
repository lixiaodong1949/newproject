using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 游戏关卡题目表 （包含综合分录题的子题）
    /// </summary>
    [Table(Name = "sx_game_topic")]
    public class SxGameTopic : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 关卡Id
        /// </summary>
        public long GameLevelId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long TopicId { get; set; }

        /// <summary>
        /// 题目名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)")]
        public string TopicTitle { get; set; }

        /// <summary>
        /// 分岗所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PointPositionId { get; set; }

        /// <summary>
        /// 积分
        /// </summary>
        [Column(Name = "Point", DbType = "int(11)")]
        public int Point { get; set; } = 0;

        /// <summary>
        /// 题目排序
        /// </summary>
        [Column(Name = "Sort", DbType = "int(11)")]
        public int Sort { get; set; } = 0;

        /// <summary>
        /// 用时
        /// </summary>
        [Column(Name = "UseTime")]
        public DateTime UseTime { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10,4)")]
        public decimal Score { get; set; }

        /// <summary>
        /// 题目标题
        /// </summary>
        [Column(Name = "Title", DbType = "varchar(255)", IsNullable = true)]
        public string Title { get; set; }
        /// <summary>
        /// 父节点ID-用于多题型(一个题目 多个作答区)
        /// </summary>
        [Column(Name = "TopicParentId", DbType = "bigint(20)", IsNullable = true)]
        public long TopicParentId { get; set; }

    }
}