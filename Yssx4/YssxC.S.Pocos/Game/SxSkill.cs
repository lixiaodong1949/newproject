using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 能力维度表
    /// </summary>
    [Table(Name = "sx_skill")]
    public class SxSkill : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)")]
        public string Name { get; set; }
       
        /// <summary>
        /// 合同分类Id
        /// </summary>
        [Column(Name = "CatgoryId", DbType = "bigint(20)", IsNullable = true)]
        public long CatgoryId { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10, 4)", IsNullable = true)]
        public decimal Score { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Column(Name = "Sort", DbType = "int(11)")]
        public int Sort { get; set; } = 0;

        /// <summary>
        /// 最大值
        /// </summary>
        [Column(Name = "Max", DbType = "int(11)")]
        public int Max { get; set; } = 0;
 

    }
}