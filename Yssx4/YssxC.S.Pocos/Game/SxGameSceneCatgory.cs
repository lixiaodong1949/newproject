using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 游戏场景分类表
    /// </summary>
    [Table(Name = "sx_gamescenecatgory")]
    public class SxGameSceneCatgory : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)")]
        public string Name { get; set; }
       
        /// <summary>
        /// 父Id
        /// </summary>
        [Column(Name = "PId", DbType = "bigint(20)", IsNullable = true)]
        public long PId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Column(Name = "Sort", DbType = "int(11)")]
        public int Sort { get; set; } = 0;

    }
}