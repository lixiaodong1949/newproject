using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 游戏关卡表
    /// </summary>
    [Table(Name = "sx_game_level")]
    public class SxGameLevel : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(Name = "Content", DbType = "varchar(4000)", IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 积分
        /// </summary>
        [Column(Name = "Point", DbType = "int(11)")]
        public int Point { get; set; } = 0;

        /// <summary>
        /// 排序
        /// </summary>
        [Column(Name = "Sort", DbType = "int(11)")]
        public int Sort { get; set; } = 0;

        /// <summary>
        /// 用时 分钟(单位：s)
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 是否免费：true免费，false 收费
        /// </summary>
        public bool Free { get; set; }

        /// <summary>
        /// 拥有的题目岗位
        /// </summary>
        [Column(Name = "Positions", DbType = "varchar(4000)")]
        public string Positions { get; set; }

        /// <summary>
        /// 拥有的题目类型
        /// </summary>
        [Column(Name = "QuestionTypes", DbType = "varchar(4000)")]
        public string QuestionTypes { get; set; }

        /// <summary>
        /// 拥有的做题节点
        /// </summary>
        [Column(Name = "TopicNodes", DbType = "varchar(4000)")]
        public string TopicNodes { get; set; }

        /// <summary>
        /// 总分
        /// </summary>
        [Column(Name = "TotalScore", DbType = "decimal(10,2)")]
        public decimal TotalScore { get; set; }

    }
}