﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 用户作答明细表
    /// </summary>
    [Table(Name = "sx_exam_user_grade_detail")]
    public class SxExamUserGradeDetail : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }
        
        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        [Column(DbType = "decimal(10,4)")]
        public decimal Score { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 父级题目Id
        /// </summary>
        public long ParentQuestionId { get; set; }

        /// <summary>
        /// 题目所属岗位Id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 答案解析
        /// </summary>
        [Column(DbType = "longtext")]
        public string Answer { get; set; }

        /// <summary>
        /// 作答与正确答案的对比信息
        /// </summary>
        [Column(DbType = "longtext")]
        public string AnswerCompareInfo { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(DbType = "int")]
        public AnswerResultStatus Status { get; set; }

        /// <summary>
        /// 分录题状态
        /// </summary>
        [Column(DbType = "int")]
        public AccountEntryStatus AccountEntryStatus { get; set; }

        /// <summary>
        /// 分录题审核状态
        /// </summary>
        [Column(DbType = "int")]
        public AccountEntryAuditStatus AccountEntryAuditStatus { get; set; }

        /// <summary>
        /// 业务日期(0:无干扰票记录业务日期 1:有干扰票及时推送记录业务日期 2:有干扰票选择时间推送记录推送日期)
        /// </summary>
        public DateTime? BusinessDate { get; set; }

        /// <summary>
        /// 是否有收付款 false:没有 true:有
        /// </summary>
        public bool IsHasReceivePayment { get; set; }
        /// <summary>
        /// 凭证字
        /// </summary>
        public CertificateWord CertificateWord { get; set; }
        /// <summary>
        /// 凭证号
        /// </summary>
        public int CertificateNo { get; set; }
        /// <summary>
        /// 答案里的摘要信息
        /// </summary>
        [Column(Name = "SummaryInfos", DbType = "LongText", IsNullable = true)]
        public string SummaryInfos { get; set; }

        /// <summary>
        /// 辅助核算名称
        /// </summary>
        [Column(Name = "AssistNames", DbType = "LongText", IsNullable = true)]
        public string AssistNames { get; set; }

        /// <summary>
        /// 结转损益专用 分开结转时保存用户作答信息
        /// </summary>
        [Column(Name = "ProfitLossUserAnswerValues", DbType = "LongText", IsNullable = true)]
        public string ProfitLossUserAnswerValues { get; set; }
    }
}
