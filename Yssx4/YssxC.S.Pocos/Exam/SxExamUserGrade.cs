﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 用户作答信息主表
    /// </summary>
    [Table(Name = "sx_exam_user_grade")]
    public class SxExamUserGrade : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        [Column(DbType = "decimal(10,4)")]
        public decimal Score { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamPaperScore { get; set; }

        /// <summary>
        /// 正确题数
        /// </summary>
        public int CorrectCount { get; set; }

        /// <summary>
        /// 错误题数
        /// </summary>
        public int ErrorCount { get; set; }

        /// <summary>
        /// 部分对题数
        /// </summary>
        public int PartRightCount { get; set; }

        /// <summary>
        /// 未作答题数
        /// </summary>
        public int BlankCount { get; set; }

        /// <summary>
        /// 是否手动提交考卷
        /// </summary>
        public bool IsManualSubmit { get; set; } = true;

        /// <summary>
        /// 交卷时间
        /// </summary>
        public Nullable<DateTime> SubmitTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime RecordTime { get; set; }

        /// <summary>
        /// 作答总用时
        /// </summary>
        public double UsedSeconds { get; set; }

        /// <summary>
        /// 剩余时间：秒
        /// </summary>
        public double LeftSeconds { get; set; }

        /// <summary>
        /// 是否已结账
        /// </summary>
        public bool IsSettled { get; set; }

        /// <summary>
        /// 考试状态
        /// </summary>
        [Column(DbType = "int")]
        public StudentExamStatus Status { get; set; }

        /// <summary>
        /// 账套创建状态 0:不需要创建 1:未创建 2:已创建
        /// </summary>
        [Column(DbType = "int")]
        public CreateSetBookStatus CreateSetBookStatus { get; set; }

        /// <summary>
        /// 平台：0：网页版 ，1：3D版,兼容以前网页版系统必须设置默认值0
        /// </summary>
        [Column(DbType = "int")]
        public int Platform { get; set; } = 0;
    }
}
