﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos.Exam
{
    /// <summary>
    /// 凭证号记录表
    /// </summary>
    [Table(Name = "sx_certificateno_record")]
    public class SxCertificateNoRecord : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 凭证类型
        /// </summary>
        public CertificateWord CertificateType { get; set; }
        /// <summary>
        /// 最后一次凭证号
        /// </summary>
        public int LastCertificateNo { get; set; }

    }
}
