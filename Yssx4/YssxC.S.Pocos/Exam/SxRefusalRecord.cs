﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos.Exam
{
    /// <summary>
    /// 拒绝记录表
    /// </summary>
    [Table(Name = "sx_refusal_record")]
    public class SxRefusalRecord : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 题目id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 拒绝人 0 出纳 1主管
        /// </summary>
        public int RefusalRole { get; set; }
        /// <summary>
        /// 拒绝原因
        /// </summary>
        public string RefusalCause{ get; set; }

    }
}
