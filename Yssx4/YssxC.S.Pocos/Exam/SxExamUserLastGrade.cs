﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 用户最近作答记录表
    /// </summary>
    [Table(Name = "sx_exam_user_last_grade")]
    public class SxExamUserLastGrade : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 平台：0：网页版 ，1：3D版,兼容以前网页版系统必须设置默认值0
        /// </summary>
        [Column(DbType = "int")]
        public int Platform { get; set; } = 0;

    }
}
