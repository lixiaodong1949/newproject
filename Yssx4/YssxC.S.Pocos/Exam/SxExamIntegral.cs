﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 用户考试获得积分表
    /// </summary>
    [Table(Name = "sx_exam_integral")]
    public class SxExamIntegral : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 试卷所属人Id
        /// </summary>
        [Column(Name = "UserId")]
        public long UserId { get; set; }

        /// <summary>
        /// 公司(案例)Id
        /// </summary>
        [Column(Name = "CaseId")]
        public long CaseId { get; set; }

        /// <summary>
        /// 任务Id
        /// </summary>
        [Column(Name = "TaskId")]
        public long TaskId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        [Column(Name = "ExamId")]
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主记录Id
        /// </summary>
        [Column(Name = "GradeId")]
        public long GradeId { get; set; }

        /// <summary>
        /// 本次获得积分
        /// </summary>
        [Column(Name = "Integral", DbType = "decimal(10,2)")]
        public decimal Integral { get; set; } = 0;
        /// <summary>
        /// 本次获得积分
        /// </summary>
        [Column(Name = "TotalIntegral", DbType = "decimal(10,2)")]
        public decimal TotalIntegral { get; set; } = 0;
    }
}
