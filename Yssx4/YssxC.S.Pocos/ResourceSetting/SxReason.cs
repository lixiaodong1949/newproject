﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 理由管理
    /// </summary>
    [Table(Name = "sx_reason")]
    public class SxReason : BizBaseEntity<long>
    {
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Classification { get; set; }
    }
}
