﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 突发事件详情管理
    /// </summary>
    [Table(Name = "sx_emergencydetails")]
    public class SxEmergencyDetails : BizBaseEntity<long>
    {

        /// <summary>
        /// 租户Id（理由分类Id）
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 详情
        /// </summary>
        public string Details { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string ImgUrl { get; set; }

        /// <summary>
        /// 0开启 1关闭
        /// </summary>
        public int Status { get; set; }
    }
}
