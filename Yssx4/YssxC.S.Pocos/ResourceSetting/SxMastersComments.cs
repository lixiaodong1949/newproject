﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 理由管理
    /// </summary>
    [Table(Name = "sx_masterscomments")]
    public class SxMastersComments : BizBaseEntity<long>
    {
        /// <summary>
        /// 评语详情
        /// </summary>
        [Column(Name = "Details", DbType = "varchar(4000)", IsNullable = true)]
        public string Details { get; set; }
        ///// <summary>
        ///// 分数范围
        ///// </summary>
        //public string ScoreRange { get; set; }
        /// <summary>
        /// 最小分数
        /// </summary>
        [Column(Name = "MinScore", DbType = "decimal(10,4)", IsNullable = true)]
        public decimal MinScore { get; set; }
        /// <summary>
        /// 最大分数
        /// </summary>
        [Column(Name = "MaxScore", DbType = "decimal(10,4)", IsNullable = true)]
        public decimal MaxScore { get; set; }

    }
}
