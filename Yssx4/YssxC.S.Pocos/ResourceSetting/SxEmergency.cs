﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 突发管理
    /// </summary>
    [Table(Name = "sx_Emergency")]
    public class SxEmergency : BizBaseEntity<long>
    {
        /// <summary>
        /// 类别名称
        /// </summary>
        public string Classification { get; set; }

        /// <summary>
        /// 类别标记 
        /// </summary>
        public int CategoryMark { get; set; }

        /// <summary>
        /// 0开启 1关闭
        /// </summary>
        public int Status { get; set; }
    }
}
