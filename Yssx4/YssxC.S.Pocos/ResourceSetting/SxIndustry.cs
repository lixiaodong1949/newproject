﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 行业管理
    /// </summary>
    [Table(Name = "sx_industry")]
    public class SxIndustry : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 地图
        /// </summary>
        public string Map { get; set; }

        /// <summary>
        /// X坐标
        /// </summary>
        public int Xcoordinate { get; set; }

        /// <summary>
        /// Y坐标
        /// </summary>
        public int Ycoordinate { get; set; }


        /// <summary>
        /// 描述视频
        /// </summary>
        public string VideoUrl { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string Synopsis { get; set; }

        /// <summary>
        /// 介绍
        /// </summary>
        [Column(Name = "Introduce", DbType = "text", IsNullable = true)]
        public string Introduce { get; set; }

        /// <summary>
        /// 状态 0开启1关闭
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 地图logo
        /// </summary>
        [Column(Name = "MapLogo3D", IsNullable = true)]
        public string MapLogo3D { get; set; }
    }
}
