﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Pocos
{
    /// <summary>
    /// 理由详情管理
    /// </summary>
    [Table(Name = "sx_reasondetails")]
    public class SxReasonDetails : BizBaseEntity<long>
    {

        /// <summary>
        /// 租户Id（理由分类Id）
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 详情
        /// </summary>
        public string Details { get; set; }
    }
}
