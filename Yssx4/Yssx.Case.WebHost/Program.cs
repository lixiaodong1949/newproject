﻿using Microsoft.AspNetCore.Hosting;

namespace Yssx.Case.WebHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            Microsoft.AspNetCore.WebHost.CreateDefaultBuilder(args)
             .UseUrls("http://localhost:9525")
                .UseStartup<Startup>();
    }
}
