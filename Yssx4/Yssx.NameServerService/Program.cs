﻿using System;
using System.Linq;

namespace Yssx.NameServerService
{
    class Program
    {
        static void Main(string[] args)
        {
            string ipv4=System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList.FirstOrDefault(address1 => address1.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)?.ToString();
            Console.WriteLine("linux ipv4:{0}", ipv4);
            Bootstrap.Initialize();
            Bootstrap.Start();
            Console.WriteLine("Press enter to exit...");
            var line = Console.ReadLine();
            while (line != "exit")
            {
                switch (line)
                {
                    case "cls":
                        Console.Clear();
                        break;
                    default:
                        return;
                }
                line = Console.ReadLine();
            }
        }
    }
}
