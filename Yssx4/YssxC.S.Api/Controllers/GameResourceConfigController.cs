using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YssxC.S.Dto;
using YssxC.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 游戏资源设置
    /// </summary>
    public class GameResourceConfigController : BaseController
    {
        private IGameResourceConfigService _service;

        public GameResourceConfigController(IGameResourceConfigService service)
        {
            _service = service;
        }
        
        /// <summary>
        /// 获取案例游戏资源配置
        /// </summary>
        /// <param name="caseId">案例Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ListResponse<GameResourceConfigResponseDto>> GetGameResourceConfig(long caseId)
        {
            var response = new ListResponse<GameResourceConfigResponseDto> { Code = CommonConstants.BadRequest };
            if (caseId <= 0)
            {
                response.Msg = "参数无效";
                return response;
            }

            response = await _service.GetGameResourceConfigList(caseId);

            return response;
        }
    }
}