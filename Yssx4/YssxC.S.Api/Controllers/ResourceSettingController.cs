﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework.Entity;
using YssxC.S.IServices;
using YssxC.S.Dto;
using Yssx.Framework;
using Microsoft.AspNetCore.Authorization;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 资源设置
    /// </summary>
    public class ResourceSettingController : BaseController
    {
        private IResourceSettingService _service;
        public ResourceSettingController(IResourceSettingService service)
        {
            this._service = service;
        }

        #region 行业管理
        /// <summary>
        /// 获取行业列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<IndustryDto>> GetIndustryList(IndustryRequest model)
        {
            return await _service.GetIndustryList(model);
        }


        #endregion

        #region 岗位管理
        /// <summary>
        /// 获取岗位列表（不分页）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<PositionManageDto>>> GetPositionList(PositionType positionType = PositionType.composite)
        {
            return await _service.GetPositionListByPracticePattern(positionType);
        }

        /// <summary>
        /// 获取岗位列表（分页）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<PageResponse<PositionManageDto>>> GetPositionListByPage([FromQuery] PositionManageRequest model)
        {
            return await _service.GetPositionListByPage(model);
        }

        #endregion

        #region 理由管理
        /// <summary>
        /// 获取理由分类列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<ReasonDto>> GetReasonList(ReasonRequest model)
        {
            return await _service.GetReasonList(model);
        }


        #region >>>理由详情
        /// <summary>
        /// 获取理由详情列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<ReasonDetailsDto>> GetReasonDetailsList(ReasonDetailsRequest model)
        {
            return await _service.GetReasonDetailsList(model);
        }

        /// <summary>
        /// </summary>
        /// 根据Id获取理由详情
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ReasonDetailsDto>> GetReasonDetailsById(long id)
        {
            return await _service.GetReasonDetailsById(id);
        }
        #endregion
        #endregion

        #region 师傅评语管理
        /// <summary>
        /// 获取师傅评语列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<MastersCommentsDto>> GetMastersCommentsList(MastersCommentsRequest model)
        {
            return await _service.GetMastersCommentsList(model);
        }


        #endregion

        #region 突发事件管理
        /// <summary>
        /// 获取突发事件列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<EmergencyDto>> GetEmergencyList(EmergencyRequest model)
        {
            return await _service.GetEmergencyList(model);
        }


        #region >>>突发详情
        /// <summary>
        /// 获取突发详情列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<EmergencyDetailsDto>> GetEmergencyDetailsList(EmergencyDetailsRequest model)
        {
            return await _service.GetEmergencyDetailsList(model);
        }


        #endregion
        #endregion




        #region 致用户信


        /// <summary>
        /// 获取最新致用户信
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<LetterToUsersDto>> GetLetterToUsers()
        {
            return await _service.GetLetterToUsers();
        }
        #endregion

    }
}