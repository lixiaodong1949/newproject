﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Utils;
using YssxC.S.Dto.Subject;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{

    /// <summary>
    /// 账簿
    /// </summary>
    public class CertificateController : BaseController
    {
        ICertificateService service;
        public CertificateController(ICertificateService _service)
        {
            service = _service;
        }

        /// <summary>
        /// 明细账查询
        /// </summary>
        /// <param name="subjectId">科目Id</param>
        /// <param name="id">案例Id</param>
        /// <param name="type">请求类型0:后台，1：学生端</param>
        /// <param name="gradeId">gradeId</param>
        /// <param name="dateStr">会计期间</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<CertificateDto>>> GetCertificateDetailDataList(long id, long subjectId, long gradeId, int type, string dateStr)
        {
            return await service.GetCertificateDetailDataList(id, subjectId, gradeId, type, dateStr);
        }

        /// <summary>
        /// 总账查询
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="type">请求类型0:后台，1：学生端(前台) </param>
        /// <param name="gradeId">gradeId</param>
        /// <param name="subjectType">科目类型 科目类型(0 全部 资产1 负债 2 权益 4 成本5 损益6)</param>
        /// <param name="keyword">查询关键字</param>
        ///  <param name="dateStr">会计期间</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CertificateDto>>> GetCertificateSummerDataList(long id, long gradeId, int subjectType, string keyword, int type, string dateStr)
        {
            return await service.GetCertificateSummerDataList(id, gradeId, subjectType, keyword, type, dateStr);
        }

        /// <summary>
        /// 余额查询
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="gradeId">gradeId</param>
        /// <param name="type">请求类型0:后台，1：学生端</param>
        /// <param name="subjectType">科目类型(0 全部 资产1 负债 2 权益 4 成本5 损益6)</param>
        /// <param name="keyword">查询关键字</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<CertificateBalanceDto>>> GetCertificateBalanceDataList(long id, long gradeId, int subjectType, string keyword, int type)
        {
            return await service.GetCertificateBalanceDataList(id, gradeId, subjectType, keyword, type);
        }

        /// <summary>
        /// 辅助核算明细
        /// </summary>
        /// <param name="id"></param>
        /// <param name="gradeId"></param>
        /// <param name="dateStr"></param>
        /// <param name="subjectId"></param>
        /// <param name="assisName"></param>
        /// <param name="isHaveCheck"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<CertificateDto>>> GetAssistacDetailDataList(long id, long gradeId, string dateStr, long subjectId, string assisName,int isHaveCheck)
        {
            return await service.GetAssistacDetailDataList(id, gradeId, dateStr, subjectId, assisName, isHaveCheck);
        }

        /// <summary>
        /// 辅助核算总账
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="gradeId">作答主记录Id</param>
        /// <param name="dateStr"></param>
        /// <param name="assistType">辅助核算类型</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CertificateBalanceDto>>> GetAssistacSummerDataList(long id, long gradeId, string dateStr, int assistType)
        {
            return await service.GetAssistacSummerDataList(id, gradeId, dateStr, assistType);
        }

        /// <summary>
        /// 根据科目Id余额查询
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="subjectType">科目类型(0 全部 资产1 负债 2 权益 4 成本5 损益6)</param>
        /// <param name="subjectId">科目Id</param>
        /// <param name="gradeId">作答记录Id</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<CertificateBalanceDto>>> GetCertificateBalanceById(long id, int subjectType, long subjectId, long gradeId, string assisName)
        {
            return await service.GetCertificateBalanceById(id, subjectType, subjectId, gradeId, assisName);
        }

        /// <summary>
        /// 下载辅助核算明细表
        /// </summary>
        /// <param name="id"></param>
        /// <param name="gradeId"></param>
        /// <param name="dateStr"></param>
        /// <param name="subjectId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<FileResult> DownloadDetailData(long id, long gradeId, string dateStr, long subjectId, string assisName)
        {
            ResponseContext<List<CertificateDto>> response = await service.GetAssistacDetailDataList(id, gradeId, dateStr, subjectId, assisName,0);
            List<CertificateDtoExcel> list = response.Data.Select(x => new CertificateDtoExcel
            {
                Balance = x.Balance,
                BorrowAmountStr = x.BorrowAmountStr,
                CreditorAmountStr = x.CreditorAmountStr,
                CertificateDate = x.CertificateDate,
                CertificateNo = x.CertificateNo,
                SubjectCode = x.SubjectCode,
                SubjectName = x.SubjectName,
                SummaryInfo = x.SummaryInfo
            }).ToList();
            byte[] ms = NPOIHelper<CertificateDtoExcel>.OutputExcel(list, new string[] { "Assistac" });
            return File(ms, "application/ms-excel", "AssistacDetail.xlsx");
        }
    }
}
