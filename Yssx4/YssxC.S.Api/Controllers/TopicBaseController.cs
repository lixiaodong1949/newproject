﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 题目公共基础服务
    /// </summary>
    public class TopicBaseController : BaseController
    {
        private readonly ITopicBaseService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public TopicBaseController(ITopicBaseService service)
        {
            _service = service;
        }

        #region 用户标记/取消标记题目操作
        /// <summary>
        /// 用户标记/取消标记题目操作
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UserMarkOrUnMarkQuestion(UserMarkQuestionDto dto)
        {
            return await _service.UserMarkOrUnMarkQuestion(dto, this.CurrentUser);
        }
        #endregion

        #region 获取公司下岗位列表
        /// <summary>
        /// 获取公司下岗位列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="positionType">岗位类型：0综岗，1分岗（默认：0）</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<UserCasePositionViewModel>>> GetUserCasePositionList(long caseId, PositionType positionType = PositionType.composite)
        {
            return await _service.GetUserCasePositionList(caseId, positionType);
        }
        #endregion

        #region 获取公司客户列表
        /// <summary>
        /// 获取公司客户列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<UserCaseCustomerViewModel>>> GetUserCaseCustomerList(long caseId)
        {
            return await _service.GetUserCaseCustomerList(caseId);
        }
        #endregion

    }
}