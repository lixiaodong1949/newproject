﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 用户考试获得积分
    /// </summary>
    public class ExamIntegralController : BaseController
    {

        private readonly IExamIntegralService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public ExamIntegralController(IExamIntegralService service)
        {
            _service = service;
        }

        /// <summary>
        /// 获取总积分
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<decimal>> GetSum(long userId)
        {
            if (userId <= 0)
                return new ResponseContext<decimal>(CommonConstants.BadRequest, "userId无效。");
            return await _service.GetSum(userId);
        }
    }
}