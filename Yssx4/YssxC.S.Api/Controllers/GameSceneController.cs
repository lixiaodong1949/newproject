using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YssxC.S.Dto;
using YssxC.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 3D游戏场景（前台）
    /// </summary>
    public class GameSceneController : BaseController
    {
        private IGameSceneService _service;

        public GameSceneController(IGameSceneService service)
        {
            _service = service;
        }

        /// <summary>
        /// 根据Id获取3D游戏场景信息
        /// </summary>
        /// <param name="id">场景Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GameSceneBaseDto>> GetGameScene(long id)
        {
            var response = new ResponseContext<GameSceneBaseDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.GetGameScene(id);

            return response;
        }

        /// <summary>
        /// 获取3D游戏场景列表(不分页)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ListResponse<GameScenesResponseDto>> GetGameSceneList()
        {
            var response = new ListResponse<GameScenesResponseDto> { Code = CommonConstants.BadRequest };
            //if (query == null)
            //{
            //    response.Msg = "参数不能为空";
            //    return response;
            //}

            response = await _service.GetGameSceneList();

            return response;
        }
    }
}