﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 收付款服务
    /// </summary>
    public class ReceivePaymentController : BaseController
    {
        private readonly IReceivePaymentService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public ReceivePaymentController(IReceivePaymentService service)
        {
            _service = service;
        }

        #region 获取用户收付款票据列表(未做)
        /// <summary>
        /// 获取用户收付款票据列表(未做)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<UserReceivingBillViewModel>>> GetUserReceivingBillTopicList([FromQuery]UserCalendarReceivingQuery query)
        {
            return await _service.GetUserReceivingBillTopicList(query, this.CurrentUser);
        }
        #endregion

        #region 获取付款题列表
        /// <summary>
        /// 获取付款题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<PaymentTopicInfoViewModel>>> GetPaymentTopicInfoList([FromQuery]ReceivePaymentTopicInfoQuery query)
        {
            return await _service.GetPaymentTopicInfoList(query, this.CurrentUser);
        }
        #endregion

        #region 获取收款题列表
        /// <summary>
        /// 获取收款题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<ReceiveTopicInfoViewModel>>> GetReceiveTopicInfoList([FromQuery]ReceivePaymentTopicInfoQuery query)
        {
            return await _service.GetReceiveTopicInfoList(query, this.CurrentUser);
        }
        #endregion

        #region 付款操作
        /// <summary>
        /// 付款操作
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<UserPaymentTopicViewModel>> PaymentOperate(UserPaymentTopicDto dto)
        {
            return await _service.PaymentOperate(dto, this.CurrentUser);
        }
        #endregion

        #region 付款操作
        /// <summary>
        /// 收款操作
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<UserReceiveTopicViewModel>> ReceiveOperate(UserReceiveTopicDto dto)
        {
            return await _service.ReceiveOperate(dto, this.CurrentUser);
        }
        #endregion

        #region 查询用户收付款列表(已做)
        /// <summary>
        /// 查询用户收付款列表(已做)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<UserReceivingBillViewModel>>> QueryUserHasDoneReceivingList([FromQuery]UserHasDoneReceivingQuery query)
        {
            return await _service.QueryUserHasDoneReceivingList(query, this.CurrentUser);
        }
        #endregion

    }
}