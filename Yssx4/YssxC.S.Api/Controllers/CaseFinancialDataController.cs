using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YssxC.S.Dto;
using YssxC.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 案例财务数据
    /// </summary>
    public class CaseFinancialDataController : BaseController
    {
        private ICaseFinancialDataService _service;

        public CaseFinancialDataController(ICaseFinancialDataService service)
        {
            _service = service;
        }
        /// <summary>
        /// 根据条件查询案例财务数据列表信息 (不分页)
        /// </summary>
        /// <param name="caseId">案例id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ListResponse<CaseFinancialDataListResponseDto>> GetCaseFinancialDataList(long caseId)
        {
            var response = new ListResponse<CaseFinancialDataListResponseDto> { Code = CommonConstants.BadRequest };
            if (caseId<=0)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GetCaseFinancialDataList(caseId);

            return response;
        }
    }
}