﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.Dto.ExamPaper;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 答题相关服务
    /// </summary>
    public class UserPracticeController : BaseController
    {
        private readonly IUserPracticeService _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public UserPracticeController(IUserPracticeService service)
        {
            _service = service;
        }
        /// <summary>
        /// 填写分录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<AccountingEntryTopicDto>>> GetAccountingEntryTopicList([FromQuery]AccountingEntryTopicRequest model)
        {
            return await _service.GetAccountingEntryTopicList(model);
        }
        /// <summary>
        /// 查询分录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="auditType">-1查全部 0查未审 1查已审</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<AccountingEntryAnsweredDto>> GetAccountingEntryAnsweredList([FromQuery]AccountingEntryTopicRequest model, int auditType = -1)
        {
            return await _service.GetAccountingEntryAnsweredList(model, auditType);
        }
        /// <summary>
        /// 出纳(会计主管)审核分录 auditType（0代表出纳  1代表会计主管）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<AccountingEntryTopicDto>>> GetAuditAccountingEntryTopicList([FromQuery]AccountingEntryTopicRequest model, int auditType)
        {
            return await _service.GetAuditAccountingEntryTopicList(model, auditType);
        }
        /// <summary>
        /// 获取月度任务题目集合(questionType：7--财务分析题   10--财务报表 17--纳税申报  18--财务报表申报题)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="taskId"></param>
        /// <param name="questionType"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<ExamOtherQuestionInfo>>> GetOtherTopicList([FromQuery]AccountingEntryTopicRequest model, long taskId, long declarationId, QuestionType questionType)
        {
            return await _service.GetOtherTopicList(model, taskId, declarationId, questionType);
        }
        /// <summary>
        /// 结转损溢
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="caseId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamProfitLossTopicDto>> GetProfitLossTopicInfo(long gradeId, long caseId, long taskId)
        {
            return await _service.GetProfitLossTopicInfo(gradeId, caseId, taskId);
        }

        /// <summary>
        /// 预览试卷作答信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamPaperBasicInfo>> GetExamPaperBasicInfoView(long gradeId)
        {
            var data = await _service.GetExamPaperBasicInfoView(gradeId, CurrentUser);
            return data;
        }
        /// <summary>
        /// 预览题目作答信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfoView(long gradeId, long questionId)
        {
            return await _service.GetQuestionInfoView(gradeId, questionId, CurrentUser.Id);
        }
        /// <summary>
        /// 获取题目信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfo(long gradeId, long questionId)
        {
            return await _service.GetQuestionInfo(gradeId, questionId, CurrentUser.Id);
        }
        /// <summary>
        /// 提交答案--接口方法
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <returns>作答成功信息</returns>
        [HttpPost]
        public async Task<ResponseContext<QuestionResult>> SubmitAnswer(QuestionAnswer model)
        {
            return await _service.SubmitAnswer(model, CurrentUser.Id);
        }
        /// <summary>
        /// 提交考卷---接口方法 
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GradeInfo>> SubmitExam(long gradeId)
        {
            if (gradeId <= 0)
                return new ResponseContext<GradeInfo> { Code = CommonConstants.BadRequest, Msg = "gradeId无效" };

            return await _service.SubmitExam(gradeId, CurrentUser.Id);
        }
        /// <summary>
        /// 新增拒绝记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddRefusalRecord(RefusalRecordDto model)
        {
            return await _service.AddRefusalRecord(model);
        }

        #region 申报题目记录草稿
        /// <summary>
        /// 申报题目记录草稿
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeclareSaveDrafts(UserDeclareDto dto)
        {
            return await _service.DeclareSaveDrafts(dto, this.CurrentUser);
        }
        #endregion

        #region 用户申报
        /// <summary>
        /// 用户申报
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> UserDeclare(UserDeclareDto dto)
        {
            return await _service.UserDeclare(dto, this.CurrentUser);
        }
        #endregion

        #region 实习小结 获取其他任务列表
        /// <summary>
        /// 获取其他实习工作列表
        /// </summary>
        /// <param name="currtTaskId">用户Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<GetOtherTasksResponseDto>>> GetOtherTasks(long currtTaskId)
        {
            return await _service.GetOtherTasks(currtTaskId);
        }
        #endregion
    }
}