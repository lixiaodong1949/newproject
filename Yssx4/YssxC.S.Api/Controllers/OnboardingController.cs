using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 入职
    /// </summary>
    public class OnboardingController : BaseController
    {
        IOnboardingService _onboardingService;
        IResourceSettingService _resourceSettingService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="onboardingService"></param>
        public OnboardingController(IOnboardingService onboardingService, IResourceSettingService resourceSettingService)
        {
            _onboardingService = onboardingService;
            _resourceSettingService = resourceSettingService;
        }

        #region 选择行业
        /// <summary>
        /// 获取所有行业 - 地图数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<GetIndustryInMapDto>>> GetIndustryInMap()
        {
            var rUserType = CurrentUser == null ? (int)UserTypeEnums.OrdinaryUser : CurrentUser.Type;
            return await _onboardingService.GetIndustryInMap(rUserType);
        }

        /// <summary>
        /// 获取所有行业 - 下拉框数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<GetIndustryAllDataDto>>> GetIndustryAllData()
        {
            var rUserType = CurrentUser == null ? (int)UserTypeEnums.OrdinaryUser : CurrentUser.Type;
            return await _onboardingService.GetIndustryAllData(rUserType);
        }

        /// <summary>
        /// 获取指定行业信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<IndustryDto>> GetIndustryById(long id)
        {
            var rUserType = CurrentUser == null ? (int)UserTypeEnums.OrdinaryUser : CurrentUser.Type;
            return await _onboardingService.GetIndustryById(id, rUserType);
        }

        /// <summary>
        /// 根据行业id获取行业基本信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<IndustryBaseInfoDto>> GetIndustryBaseInfo(long id)
        {
            var rUserType = CurrentUser == null ? (int)UserTypeEnums.OrdinaryUser : CurrentUser.Type;
            return await _onboardingService.GetIndustryBaseInfo(id, rUserType);
        }
        #endregion

        #region 选择行业
        /// <summary>
        /// 获取行业下所有企业 - 地图数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<GetCaseByIndustryDto>>> GetCaseByIndustry(long id)
        {
            var rUserType = CurrentUser == null ? (int)UserTypeEnums.OrdinaryUser : CurrentUser.Type;
            return await _onboardingService.GetCaseByIndustry(id, rUserType);
        }

        /// <summary>
        /// 获取指定企业详细数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GetCaseInfoOnboardingDto>> GetCaseInfoOnboarding(long id)
        {
            var rUserType = CurrentUser == null ? (int)UserTypeEnums.OrdinaryUser : CurrentUser.Type;
            return await _onboardingService.GetCaseInfoOnboarding(id, rUserType);
        }

        #endregion

        #region 月度任务

        /// <summary>
        ///  根据企业查询月度任务 
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="platform">平台：0：网页版 ，1：3D版,兼容以前网页版必须设置默认值0</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<GetMonthTaskListDto>>> GetMonthTaskList(long id, int platform = 0)
        {
            return await _onboardingService.GetMonthTaskList(id, CurrentUser.Id, CurrentUser.Type, platform);
        }

        /// <summary>
        /// 查询指定月度任务 - PC端
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GetMonthTaskListDto>> GetMonthTaskById(long id)
        {
            return await _onboardingService.GetMonthTaskById(id, CurrentUser.Id, CurrentUser.Type);
        }

        /// <summary>
        /// 查询指定月度任务 - APP商城
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<GetMonthTaskListDto>> GetMonthTaskByIdApp(long id)
        {
            return await _onboardingService.GetMonthTaskByIdApp(id, CurrentUser.Id);
        }
        #endregion

        #region 部门介绍
        /// <summary>
        /// 根据企业查询部门介绍
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<GetCaseRegulationListDto>>> GetCaseRegulationList(long id)
        {
            return await _onboardingService.GetCaseRegulationList(id);
        }
        #endregion

        #region 生成试卷
        /// <summary>
        /// 购买后自动生成试卷 - 云实习
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> AutoGenerateTestPaper(AutoGenerateTestPaperDto model)
        {
            return await _onboardingService.AutoGenerateTestPaper(model);
        }
        #endregion

        #region 致用户信
        /// <summary>
        /// 获取最新致用户信
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<LetterToUsersDto>> GetLetterToUsers()
        {
            return await _resourceSettingService.GetLetterToUsers();
        }
        #endregion

        #region 实习列表
        /// <summary>
        /// 实习列表 - 顶岗实习
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<GetCloudInternshipListDto>> GetCloudInternshipList(GetCloudInternshipListRequest model)
        {
            return await _onboardingService.GetCloudInternshipList(model, CurrentUser.Id, CurrentUser.Type);
        }

        /// <summary>
        /// 查询通关记录
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<GetCloudInternshipRecordDto>> GetCloudInternshipRecord(GetCloudInternshipRecordRequest model)
        {
            return await _onboardingService.GetCloudInternshipRecord(model, CurrentUser.Id);
        }
        #endregion

    }
}