﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class SummaryController : BaseController
    {
        private ISummaryService _abstractService;

        public SummaryController(ISummaryService abstractService)
        {
            _abstractService = abstractService;
        }

        /// <summary>
        /// 根据id获取摘要
        /// </summary>
        /// <param name="id">案例id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SummaryDto>>> GetSummary(long id)
        {
            return await _abstractService.GetSummaryList(id);
        }

    }
}
