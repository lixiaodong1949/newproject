﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 摘要信息
    /// </summary>
    public class AbstractBackController : BaseController
    {
        private IAbstractService _abstractService;

        public AbstractBackController(IAbstractService abstractService)
        {
            _abstractService = abstractService;
        }

        /// <summary>
        /// 保存摘要信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveAbstract(AbstractDto input)
        {
            return await _abstractService.SaveAbstract(input, CurrentUser);
        }

        /// <summary>
        /// 根据id删除摘要
        /// </summary>
        /// <param name="id">摘要id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteAbstract(long id)
        {
            return await _abstractService.DeleteAbstract(id, CurrentUser);
        }

        /// <summary>
        /// 根据id获取摘要
        /// </summary>
        /// <param name="id">摘要id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<AbstractDto>> GetAbstract(long id)
        {
            return await _abstractService.GetAbstract(id);
        }

        /// <summary>
        /// 根据条件查询摘要
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<AbstractDto>> GetAbstractList([FromQuery]GetAbstractInput input)
        {
            return await _abstractService.GetAbstractList(input);
        }
    }
}