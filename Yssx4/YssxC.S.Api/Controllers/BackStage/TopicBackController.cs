﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Extensions;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 题目管理（后台）
    /// </summary>
    public class TopicBackController : BaseController
    {
        ITopicService _topicService;
        public TopicBackController(ITopicService topicService)
        {
            _topicService = topicService;
        }

        /// <summary>
        /// 添加 更新 题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveSimpleQuestion(SimpleQuestionRequest model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (model.CaseId <= 0)
            {
                result.Msg = "CaseId 为无效数据。";
                return result;
            }
            else if (model.TaskId <= 0)
            {
                result.Msg = "TaskId 为无效数据。";
                return result;
            }
            else if (model.Options == null || model.Options.Count == 0)
            {
                result.Msg = "请添加选项。";
                return result;
            }

            return await _topicService.SaveSimpleQuestion(model, CurrentUser);
        }

        /// <summary>
        /// 添加 更新 复杂题目（表格题、案例题、申报表题）
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveComplexQuestion(ComplexQuestionRequest model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (model.CaseId <= 0)
            {
                result.Msg = "CaseId 为无效数据。";
                return result;
            }
            else if (model.QuestionType != QuestionType.DeclarationTpoic && model.TaskId <= 0)
            {
                result.Msg = "TaskId 为无效数据。";
                return result;
            }

            return await _topicService.SaveComplexQuestion(model, CurrentUser);

        }

        /// <summary>
        /// 添加 更新 财务分析题
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveMainSubQuestion(MainSubQuestionRequest model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (model.CaseId <= 0)
            {
                result.Msg = "CaseId 为无效数据。";
                return result;
            }
            else if (model.TaskId <= 0)
            {
                result.Msg = "TaskId 为无效数据。";
                return result;
            }

            return await _topicService.SaveMainSubQuestion(model, CurrentUser);
        }

        /// <summary>
        /// 添加 更新 财务报表题
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveFinancialStatements(FinancialStatementsRequest model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (model.CaseId <= 0)
            {
                result.Msg = "CaseId 为无效数据。";
                return result;
            }
            else if (model.TaskId <= 0)
            {
                result.Msg = "TaskId 为无效数据。";
                return result;
            }

            return await _topicService.SaveFinancialStatements(model, CurrentUser);
        }

        /// <summary>
        /// 添加 更新 综合题分录题  
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveCertificateMainSubTopic(CertificateMainSubTopic model)
        {
            var result = new ResponseContext<bool>(CommonConstants.BadRequest, "", false);

            #region 数据校验
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (model.MainTopic == null)
            {
                result.Msg = "MainTopic 为空。";
                return result;
            }
            else if (string.IsNullOrWhiteSpace(model.MainTopic.Title))
            {
                result.Msg = "业务名称不能为空。";
                return result;
            }
            else if (model.MainTopic.BusinessDate.IsEmpty())
            {
                result.Msg = "业务日期不能为空。";
                return result;
            }
            else if (model.MainTopic.CaseId <= 0)
            {
                result.Msg = "CaseId 为无效数据。";
                return result;
            }
            else if (model.MainTopic.TaskId <= 0)
            {
                result.Msg = "TaskId 为无效数据。";
                return result;
            }
            else if (model.MainTopic.TopicRelationStatus && (model.MainTopic.TopicRelations == null || model.MainTopic.TopicRelations.Count == 0))
            {
                result.Msg = "题目关联启用，必须选择关联的题目。";
                return result;
            }
            else if (model.CertificateTopic == null)
            {
                result.Msg = "CertificateTopic 为空。";
                return result;
            }
            else if (model.TeticketTopic == null)
            {
                result.Msg = "TeticketTopic 为空。";
                return result;
            }
            else if (model.MainTopic.Id > 0)//修改
            {
                if (model.CertificateTopic.Id <= 0)
                {
                    result.Msg = "CertificateTopic 为Id不正确。";
                    return result;
                }
                else if (model.TeticketTopic.Id <= 0)
                {
                    result.Msg = "TeticketTopic 为Id不正确。";
                    return result;
                }
                if (model.MainTopic.TopicRelations != null && model.MainTopic.TopicRelations.Count > 0)
                {
                    var r = model.MainTopic.TopicRelations.Any(e => e.TopicParentId == model.MainTopic.Id);
                    if (r)
                    {
                        result.Msg = "题目关联不能关联自己。";
                        return result;
                    }
                }
            }
            #region 收付款题
            if (model.ReceiptTopics != null && model.ReceiptTopics.Count > 0)
            {
                foreach (var item in model.ReceiptTopics)
                {
                    if (item.Amount <= 0)
                    {
                        result.Msg = "收款题的金额必填。";
                        return result;
                    }
                    if (item.PayCompanyId <= 0)
                    {
                        result.Msg = "收款题的单位必填。";
                        return result;
                    }
                    if (item.PayeeBankId <= 0)
                    {
                        result.Msg = "收款题的银行必填。";
                        return result;
                    }
                }
            }
            if (model.PayTopics != null && model.PayTopics.Count > 0)
            {
                foreach (var item in model.PayTopics)
                {
                    if (item.Amount <= 0)
                    {
                        result.Msg = "付款题的金额必填。";
                        return result;
                    }
                    if (item.PayeeCompanyId <= 0)
                    {
                        result.Msg = "付款题的单位必填。";
                        return result;
                    }
                    if (item.PayBankId <= 0)
                    {
                        result.Msg = "付款题的银行必填。";
                        return result;
                    }
                }
            }
            if (model.ReceiptTopics != null && model.ReceiptTopics.Count > 0 && model.PayTopics != null && model.PayTopics.Count > 0)
            {
                result.Msg = "收付款题目不能同时录，只能二选一。";
                return result;
            }
            #endregion

            #region 票据题
            if (model.TeticketTopic.Files == null || model.TeticketTopic.Files.Count == 0)
            {
                result.Msg = "必须添加票据。";
                return result;
            }
            else if (model.TeticketTopic.Files != null && model.TeticketTopic.Files.Count > 0)
            {
                var hasS = model.TeticketTopic.Files.Any(e => (e.Type == TopicFileType.Fault || e.Type == TopicFileType.Normal));
                if (!hasS)
                {
                    result.Msg = "原始票据不能为空。";
                    return result;
                }
                var pushCount = model.TeticketTopic.Files.Count(e => (e.Type == TopicFileType.Push));
                var FaultCount = model.TeticketTopic.Files.Count(e => (e.Type == TopicFileType.Fault));
                if (pushCount != FaultCount)
                {
                    result.Msg = "推送票据张数和原始票据中错误票据数量不一致。";
                    return result;
                }
                if (FaultCount > 0)//有错误票据，干扰题
                {
                    if (string.IsNullOrWhiteSpace(model.TeticketTopic.AnswerValue))
                    {
                        result.Msg = "退回理由不能为空。";
                        return result;
                    }
                    else if (model.TeticketTopic.PushDate.IsEmpty())
                    {
                        result.Msg = "推送时间不能为空。";
                        return result;
                    }
                }
                else//无错误票据
                {
                    model.TeticketTopic.AnswerValue = null;
                    model.TeticketTopic.PushDate = null;
                }
                #region 封面
                //原始票据封面
                var hascover = model.TeticketTopic.Files.Any(e => e.CoverBack == TopicFileCoverBack.Cover);
                if (!hascover)
                {
                    result.Msg = "必须要选择封面。";
                    return result;
                }
                //原始票据封面
                var cover = model.TeticketTopic.Files.Count(e => (e.Type == TopicFileType.Fault || e.Type == TopicFileType.Normal) && e.CoverBack == TopicFileCoverBack.Cover);
                if (cover > 1)
                {
                    result.Msg = "原始票据封面大于1。";
                    return result;
                }
                //原始票据底板
                var back = model.TeticketTopic.Files.Count(e => (e.Type == TopicFileType.Fault || e.Type == TopicFileType.Normal) && e.CoverBack == TopicFileCoverBack.Back);
                if (back > 1)
                {
                    result.Msg = "原始票据底板大于1。";
                    return result;
                }
                //推送票据封面
                var pushCover = model.TeticketTopic.Files.Count(e => (e.Type == TopicFileType.Push || e.Type == TopicFileType.Normal) && e.CoverBack == TopicFileCoverBack.Cover);
                if (pushCover > 1)
                {
                    result.Msg = "推送票据封面大于1。";
                    return result;
                }
                //推送票据底板
                var pushBack = model.TeticketTopic.Files.Count(e => (e.Type == TopicFileType.Push) && e.CoverBack == TopicFileCoverBack.Back);
                if (pushBack > 1)
                {
                    result.Msg = "推送票据底板大于1。";
                    return result;
                }

                #endregion
            }
            #endregion

            #endregion

            if (model.MainTopic.Id <= 0)//新增
                result = await _topicService.AddCertificateMainSubTopic(model, CurrentUser);
            else//修改
                result = await _topicService.UpdateCertificateMainSubTopic(model, CurrentUser);

            return result;
        }

        /// <summary>
        /// 添加 更新 结转损益题
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveProfitLossTopic(ProfitLossTopicDto model)
        {
            var result = new ResponseContext<bool>(CommonConstants.BadRequest, "", false);
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }
            else if (model.CaseId <= 0)
            {
                result.Msg = "CaseId 为无效数据。";
                return result;
            }
            else if (model.TaskId <= 0)
            {
                result.Msg = "TaskId 为无效数据。";
                return result;
            }

            if (model.Id <= 0)//新增
                result = await _topicService.AddProfitLossTopic(model, CurrentUser);
            else//修改
                result = await _topicService.UpdateProfitLossTopic(model, CurrentUser);

            return result;
        }

        /// <summary>
        /// 根据id获取题目详情
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TopicInfoDto>> GetTopicInfoById(long id)
        {
            var result = new ResponseContext<TopicInfoDto>(CommonConstants.BadRequest, "", null);
            if (id <= 0)
            {
                result.Msg = "id不正确。";
                return result;
            }

            return await _topicService.GetTopicInfoById(id);
        }

        /// <summary>
        /// 根据申报表id获取申报表题目详情
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TopicInfoDto>> GetTopicInfoByDeclarationId(long declarationId)
        {
            var result = new ResponseContext<TopicInfoDto>(CommonConstants.BadRequest, "", null);
            if (declarationId <= 0)
            {
                result.Msg = "id不正确。";
                return result;
            }

            return await _topicService.GetTopicInfoByDeclarationId(declarationId);
        }

        /// <summary>
        /// 综合题分录题 根据题目Id获取单个综合题分录题信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CertificateMainSubTopic>> GetCertificateMainSubTopicInfoById(long id)
        {
            var result = new ResponseContext<CertificateMainSubTopic>(CommonConstants.BadRequest, "", null);
            if (id <= 0)
            {
                result.Msg = "id不正确。";
                return result;
            }

            return await _topicService.GetCertificateMainSubTopicInfoById(id);
        }

        /// <summary>
        /// 结转损益题 根据题目Id获取单个结转损益题信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<ProfitLossTopicDto>> GetProfitLossTopicInofById(long id)
        {
            var result = new ResponseContext<ProfitLossTopicDto>(CommonConstants.BadRequest, "", null);
            if (id <= 0)
            {
                result.Msg = "id不正确。";
                return result;
            }

            return await _topicService.GetProfitLossTopicInofById(id);
        }

        /// <summary>
        /// 分页查询题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<TopicListPageDto>> GetTopicListPage(TopicQueryPageDto queryDto)
        {
            var response = new ResponseContext<TopicListPageDto>(CommonConstants.BadRequest, "");
            if (queryDto == null)
            {
                response.Msg = "请传入正确的参数";
                return response;
            }
            return await _topicService.GetTopicListPage(queryDto);
        }

        /// <summary>
        /// 题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<TopicListDataDto>>> GetTopicList(TopicQueryDto queryDto)
        {
            var response = new ResponseContext<List<TopicListDataDto>>(CommonConstants.BadRequest, "");
            if (queryDto == null)
            {
                response.Msg = "请传入正确的参数";
                return response;
            }
            return await _topicService.GetTopicList(queryDto);
        }

        /// <summary>
        /// 获取所有题目的名称（包含子题目）
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<TopicNamesResponseDto>>> GetAllTopicNames(TopicNamesDto queryDto)
        {
            var response = new ResponseContext<List<TopicNamesResponseDto>>(CommonConstants.BadRequest, "");
            if (queryDto == null)
            {
                response.Msg = "请传入正确的参数";
                return response;
            }

            return await _topicService.GetAllTopicNames(queryDto);
        }

        /// <summary>
        /// 题目删除 根据Id删除题目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteQuestion(long id)
        {
            if (id <= 0)
                return new ResponseContext<bool>(CommonConstants.BadRequest, "", false);

            return await _topicService.DeleteTopicById(id, CurrentUser);
        }
    }
}