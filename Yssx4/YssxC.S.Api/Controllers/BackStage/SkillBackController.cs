using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YssxC.S.Dto;
using YssxC.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 能力维度(后台)
    /// </summary>
    public class SkillBackController : BaseController
    {
        private ISkillService _service;

        public SkillBackController(ISkillService service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(SkillDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }

            result = await _service.Save(model, CurrentUser.Id);

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<SkillDto>> Get(long id)
        {
            var response = new ResponseContext<SkillDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Get(id);

            return response;
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        [HttpDelete]
        public async Task<ResponseContext<bool>> Delete(long id)
        {
            var response = new ResponseContext<bool>(CommonConstants.BadRequest, "");
            if (id < 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.Delete(CurrentUser.Id, id);

            return response;
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        private async Task<PageResponse<SkillPageResponseDto>> ListPage(SkillPageRequestDto query)
        {
            var response = new PageResponse<SkillPageResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.ListPage(query);

            return response;
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ListResponse<SkillListResponseDto>> List(SkillListRequestDto query)
        {
            var response = new ListResponse<SkillListResponseDto> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.List(query);

            return response;
        }
    }
}