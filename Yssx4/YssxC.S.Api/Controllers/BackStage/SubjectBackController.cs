﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using YssxC.S.Dto.Subject;
using YssxC.S.IServices.Subject;

namespace YssxC.S.Api.Controllers.BackStage
{
    /// <summary>
    /// 后台-科目管理
    /// </summary>
    public class SubjectBackController : BaseController
    {
        ISubjectService service;
        public SubjectBackController(ISubjectService s1)
        {
            service = s1;
        }

        /// <summary>
        /// 新增科目
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        
        public async Task<ResponseContext<bool>> AddSubject(SubjectDto model)
        {
            return await service.AddSubject(model);
        }

        /// <summary>
        /// 删除科目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveSubject(long id)
        {
            return await service.RemoveSubject(id);
        }

        /// <summary>
        /// 新增科目获取子科目的序号
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<string>> GetNextNum(long id)
        {
            return await service.GetNextNum(id);
        }

        /// <summary>
        /// 根据案例Id获取科目列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectsList(long id)
        {
            return await service.GetSubjectsList(id);
        }


        /// <summary>
        /// 根据案例Id获取最后一级科目
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="subjectType">类型Type 科目类型(0 全部 资产1 负债 2 权益 4 成本5 损益6)</param>
        /// 
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectLastNodeList(long id, int subjectType)
        {
            return await service.GetSubjectLastNodeList(id, subjectType);
        }

        /// <summary>
        /// 根据案例科目类型获取科目列表
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet]
      //  [AllowAnonymous]
        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectByType(long id, int type)
        {
            return await service.GetSubjectByType(id, type);
        }

        /// <summary>
        /// 添加期初数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ResponseContext<bool>> AddValues(SubjectValueDto model)
        {
            return await service.AddValues(model);
        }

        /// <summary>
        /// 试算平衡
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<TrialBalancingDto>> TrialBalancing(long caseId)
        {
            return await service.TrialBalancing(caseId);
        }
    }
}
