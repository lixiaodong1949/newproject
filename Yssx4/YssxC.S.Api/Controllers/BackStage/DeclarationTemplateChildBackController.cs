﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers.BackStage
{
    /// <summary>
    /// 申报表子表模板
    /// </summary>
    public class DeclarationTemplateChildBackController : BaseController
    {
        private readonly IDeclarationTemplateChildService _declarationTemplateService;

        public DeclarationTemplateChildBackController(IDeclarationTemplateChildService declarationTemplateService)
        {
            _declarationTemplateService = declarationTemplateService;
        }
        /// <summary>
        /// 根据条件查询申报表子表模板
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<DeclarationTemplateChildDto>>> GetList([FromQuery]GetDeclarationTemplateInput input)
        {
            return await _declarationTemplateService.GetList(input);
        }
    }
}