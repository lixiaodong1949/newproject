﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 公司案例规章制度信息（后台）
    /// </summary>
    public class CaseRegulationBackController : BaseController
    {
        ICaseRegulationService _service;
        public CaseRegulationBackController(ICaseRegulationService regulationService)
        {
            _service = regulationService;
        }

        /// <summary>
        /// 删除规章制度信息
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteById(long id)
        {
            ResponseContext<bool> response = new ResponseContext<bool>(CommonConstants.BadRequest, "", false);
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.DeleteById(id, CurrentUser);

            return response;
        }
        /// <summary>
        /// 根据Id获取单条规章制度信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CaseRegulationDto>> GetInfoById(long id)
        {
            ResponseContext<CaseRegulationDto> response = new ResponseContext<CaseRegulationDto>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.GetInfoById(id);

            return response;
        }
        /// <summary>
        /// 保存规章制度
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(CaseRegulationDto model)
        {
            var response = new ResponseContext<bool>(CommonConstants.BadRequest, "");
            if (model == null)
            {
                response.Msg = "请求参数无效";
                return response;
            }
            if (model.CaseId <= 0)
            {
                response.Msg = "caseId不正确!";
                return response;
            }
            if (string.IsNullOrWhiteSpace(model.Name))
            {
                response.Msg = "Name不正确!";
                return response;
            }

            response = await _service.Save(model, CurrentUser);

            return response;
        }
        /// <summary>
        /// 根据案例Id获取案例的规章制度列表
        /// </summary>
        /// <param name="caseId">父类Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CaseRegulationListDto>>> GetListByCaseId(long caseId)
        {
            var response = new ResponseContext<List<CaseRegulationListDto>>(CommonConstants.BadRequest, "");
            if (caseId <= 0)
            {
                response.Msg = "caseId不正确!";
                return response;
            }
            response = await _service.GetListByCaseId(caseId);

            return response;
        }

        /// <summary>
        /// 保存规章制度 应用规章制度模板
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveCaseUseRegulation(CaseUseRegulationsDto model)
        {
            ResponseContext<bool> response = new ResponseContext<bool>(CommonConstants.BadRequest, "");
            if (model == null)
            {
                response.Msg = "请传入正确的参数";
                return response;
            }
            if (model.CaseId <= 0)
            {
                response.Msg = "CaseId不正确";
                return response;
            }
            if (model.RegulationIds == null && model.RegulationIds.Count == 0)
            {
                response.Msg = "RegulationIds不正确";
                return response;
            }

            response = await _service.SaveCaseUseRegulation(model, CurrentUser);

            return response;
        }

    }
}