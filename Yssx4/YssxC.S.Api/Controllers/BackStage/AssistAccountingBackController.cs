﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 账簿-辅助核算（后台） 
    /// </summary>
    public class AssistAccountingBackController : BaseController
    {
        IAssistAccountingService _assistAccountingService;
        public AssistAccountingBackController(IAssistAccountingService assistAccountingService)
        {
            _assistAccountingService = assistAccountingService;
        }

        /// <summary>
        /// 删除 账簿-辅助核算信息
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteById(long id)
        {
            ResponseContext<bool> response;
            if (id <= 0)
            {
                return new ResponseContext<bool>(CommonConstants.BadRequest, "Id不正确！");
            }
            response = await _assistAccountingService.DeleteById(id, CurrentUser);

            return response;
        }
        /// <summary>
        /// 根据Id获取单条账簿-辅助核算信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<AssistAccountingDto>> GetInfoById(long id)
        {
            ResponseContext<AssistAccountingDto> response;
            if (id <= 0)
            {
                return new ResponseContext<AssistAccountingDto>(CommonConstants.BadRequest, "Id不正确！");
            }
            response = await _assistAccountingService.GetInfoById(id);

            return response;
        }

        /// <summary>
        /// 保存账簿-辅助核算业务服务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(AssistAccountingDto model)
        {
            ResponseContext<bool> response = new ResponseContext<bool>(CommonConstants.BadRequest, "", false);
            if (model == null)
            {
                response.Msg = "请求参数无效！";
                return response;
            }
            if (string.IsNullOrWhiteSpace(model.Name))
            {
                response.Msg = "名称不能为空！";
                return response;
            }
            if (!model.Type.HasValue)
            {
                response.Msg = "主类型不能为空！";
                return response;
            }
            response = await _assistAccountingService.Save(model, CurrentUser);

            return response;
        }
        /// <summary>
        /// 根据公司id获取账簿-辅助核算信息
        /// </summary>
        /// <param name="caseId">企业Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<AssistAccountingQueryListResponseDto>>> GetListByCaseId(long caseId)
        {
            var response = new ResponseContext<List<AssistAccountingQueryListResponseDto>>(CommonConstants.BadRequest, "");
            if (caseId <= 0)
            {
                response.Msg = "caseId不正确！";
                return response;
            }
            response = await _assistAccountingService.GetListByCaseId(caseId);

            return response;
        }

        /// <summary>
        /// 根据公司id获取账簿-辅助核算分页列表
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<PageResponse<AssistAccountingQueryPageResponseDto>>> GetListPage(AssistAccountingQueryPageDto queryModel)
        {
            ResponseContext<PageResponse<AssistAccountingQueryPageResponseDto>> response =
                new ResponseContext<PageResponse<AssistAccountingQueryPageResponseDto>>(CommonConstants.BadRequest, "", null);

            if (queryModel == null)
            {
                response.Msg = "请求参数无效";
                return response;
            }

            response = await _assistAccountingService.GetListPage(queryModel);

            return response;
        }

        /// <summary>
        /// 查询账簿-辅助核算名称列表
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<GetNameListResponseDto>>> GetNameList(GetNameListDto queryModel)
        {
            ResponseContext<List<GetNameListResponseDto>> response = new ResponseContext<List<GetNameListResponseDto>>(CommonConstants.BadRequest, "", null);
            if (queryModel == null)
            {
                response.Msg = "请求参数无效";
                return response;
            }

            response = await _assistAccountingService.GetNameList(queryModel);

            return response;
        }

        /// <summary>
        /// 根据caseid获取所有收款账号公司名称
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<GetNameListByCaseIdResponseDto>>> GetNameListByCaseId(long caseId)
        {
            var result = new ResponseContext<List<GetNameListByCaseIdResponseDto>>(CommonConstants.BadRequest, "", null);
            if (caseId <= 0)
            {
                result.Msg = "id不正确。";
                return result;
            }

            result = await _assistAccountingService.GetNameListByCaseId(caseId);

            return result;
        }
        /// <summary>
        /// 根据caseid获取所有收款账号公司名称(去重)
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<GetNameDistinctListByCaseIdResponseDto>>> GetNameDistinctListByCaseId(AssistAccountingQueryDto queryDto)
        {
            var result = new ResponseContext<List<GetNameDistinctListByCaseIdResponseDto>>(CommonConstants.BadRequest, "", null);

            if (queryDto==null)
            {
                result.Msg = "id不正确。";
                return result;
            }
            else if (queryDto.CaseId <= 0)
            {
                result.Msg = "id不正确。";
                return result;
            }

            result = await _assistAccountingService.GetNameDistinctListByCaseId(queryDto);

            return result;
        }
    }
}