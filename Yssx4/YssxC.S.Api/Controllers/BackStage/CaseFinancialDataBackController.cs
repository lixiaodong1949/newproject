using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YssxC.S.Dto;
using YssxC.S.IServices;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 案例财务数据
    /// </summary>
    public class CaseFinancialDataBackController : BaseController
    {
        private ICaseFinancialDataService _service;

        public CaseFinancialDataBackController(ICaseFinancialDataService service)
        {
            _service = service;
        }

        /// <summary>
        /// 添加案例财务数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveCaseFinancialData(CaseFinancialDataDto model)
        {
            var result = new ResponseContext<bool> { Code = CommonConstants.BadRequest, Msg = "" };
            if (model == null)
            {
                result.Msg = "请求参数无效";
                return result;
            }

            result = await _service.SaveCaseFinancialData(model, CurrentUser.Id);

            return result;
        }

        /// <summary>
        /// 根据Id删除案例财务数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        [HttpDelete]
        public async Task<ResponseContext<bool>> DeleteCaseFinancialData(long id)
        {
            var response = new ResponseContext<bool>(CommonConstants.BadRequest, "");
            if (id <= 0)
            {
                response.Msg = "Id不正确！";
                return response;
            }

            response = await _service.DeleteCaseFinancialData(CurrentUser.Id, id);

            return response;
        }

        /// <summary>
        /// 根据条件查询案例财务数据列表信息 (不分页)
        /// </summary>
        /// <param name="caseId">案例id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ListResponse<CaseFinancialDataListResponseDto>> GetCaseFinancialDataList(long caseId)
        {
            var response = new ListResponse<CaseFinancialDataListResponseDto> { Code = CommonConstants.BadRequest };
            if (caseId <= 0)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GetCaseFinancialDataList(caseId);

            return response;
        }
    }
}