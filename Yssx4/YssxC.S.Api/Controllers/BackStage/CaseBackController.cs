using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 案例管理
    /// </summary>
    public class CaseBackController : BaseController
    {
        private ICaseService _service;

        public CaseBackController(ICaseService caseService)
        {
            _service = caseService;
        }

        /// <summary>
        /// 保存案例
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveCase(CaseDto input)
        {
            return await _service.SaveCase(input, CurrentUser);
        }

        /// <summary>
        /// 根据id删除案例
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteCase(long id)
        {
            return await _service.DeleteCase(id, CurrentUser);
        }

        /// <summary>
        /// 根据id获取案例
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<CaseDto>> GetCase(long id)
        {
            return await _service.GetCase(id);
        }

        /// <summary>
        /// 根据条件查询案例
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<CaseDto>> GetCaseList([FromQuery]GetCaseInput input)
        {
            return await _service.GetCaseList(input);
        }

        /// <summary>
        /// 启用或禁用案例
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Toggle(ToggleInput input)
        {
            return await _service.Toggle(input, CurrentUser);
        }

        /// <summary>
        /// 查询企业列表 - 关联产品
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<GetCaseListByNameDto>> GetCaseListByName(GetCaseInput model)
        {
            return await _service.GetCaseListByName(model);
        }
    
        /// <summary>
        /// 获取所有案例列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<List<CaseGetAllCaseListResponseDto>>> GetAllCaseList(CaseGetAllCaseListRequestDto query)
        {
            var response = new ResponseContext<List<CaseGetAllCaseListResponseDto>> { Code = CommonConstants.BadRequest };
            if (query == null)
            {
                response.Msg = "参数不能为空";
                return response;
            }

            response = await _service.GetAllCaseList(query);

            return response;
        }
    }
}