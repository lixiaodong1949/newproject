﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 银行账户
    /// </summary>
    public class BankAccountBackController : BaseController
    {
        private IBankAccountService _bankAccountService;

        public BankAccountBackController(IBankAccountService bankAccountService)
        {
            _bankAccountService = bankAccountService;
        }

        /// <summary>
        /// 保存银行账户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> SaveBankAccount(BankAccountDto input)
        {
            return await _bankAccountService.SaveBankAccount(input, CurrentUser);
        }

        /// <summary>
        /// 根据id删除银行账户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeleteBankAccount(long id)
        {
            return await _bankAccountService.DeleteBankAccount(id, CurrentUser);
        }

        /// <summary>
        /// 根据id获取银行账户
        /// </summary>
        /// <param name="id">银行账户id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<BankAccountDto>> GetBankAccount(long id)
        {
            return await _bankAccountService.GetBankAccount(id);
        }

        /// <summary>
        /// 根据条件查询银行账户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<BankAccountDto>> GetBankAccountList([FromQuery]GetBankAccountInput input)
        {
            return await _bankAccountService.GetBankAccountList(input);
        }
    }
}