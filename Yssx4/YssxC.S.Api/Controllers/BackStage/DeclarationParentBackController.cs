﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers.BackStage
{
    /// <summary>
    /// 申报主表
    /// </summary>
    public class DeclarationParentBackController : BaseController
    {
        private readonly IDeclarationParentService _declarationParentService;

        public DeclarationParentBackController(IDeclarationParentService declarationParentService)
        {
            _declarationParentService = declarationParentService;
        }

        /// <summary>
        /// 保存申报表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(CreateDeclarationParentInput[] input)
        {
            return await _declarationParentService.Save(input, CurrentUser);
        }

        /// <summary>
        /// 根据id删除申报表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> Delete(long id)
        {
            return await _declarationParentService.Delete(id, CurrentUser);
        }

        /// <summary>
        /// 根据id获取申报表
        /// </summary>
        /// <param name="id">申报表id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<DeclarationParentDto>> GetById(long id)
        {
            return await _declarationParentService.GetById(id);
        }

        /// <summary>
        /// 根据条件获取申报表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<DeclarationParentDto>>> GetList([FromQuery]GetDeclarationParentInput input)
        {
            return await _declarationParentService.GetList(input);
        }

        /// <summary>
        /// 修改主表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Update(UpdateDeclarationParentInput input)
        {
            return await _declarationParentService.Update(input, CurrentUser);
        }
    }
}