﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework;
using YssxC.S.Dto.Subject;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers.BackStage
{
    /// <summary>
    /// 
    /// </summary>
    public class CertificateBackController : BaseController
    {
        ICertificateService service;
        public CertificateBackController(ICertificateService _service)
        {
            service = _service;
        }

        /// <summary>
        /// 明细账查询
        /// </summary>
        /// <param name="subjectId">科目Id</param>
        /// <param name="id">案例Id</param>
        /// <param name="type">请求类型0:后台，1：学生端</param>
        /// <param name="gradeId">gradeId</param>
        /// <param name="dateStr">会计期间</param>
        ///  <param name="taskId">任务Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CertificateDto>>> GetCertificateDetailDataList(long id, long subjectId, long gradeId, int type, string dateStr,long taskId)
        {
            return await service.GetCertificateDetailDataList(id, subjectId, gradeId, type, dateStr, taskId);
        }

        /// <summary>
        /// 总账查询
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="type">请求类型0:后台，1：学生端</param>
        /// <param name="gradeId">gradeId</param>
        /// <param name="subjectType">科目类型</param>
        /// <param name="keyword">查询关键字</param>
        ///  <param name="dateStr">会计期间</param>
        /// <param name="taskId">任务Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CertificateDto>>> GetCertificateSummerDataList(long id, long gradeId, int subjectType, string keyword, int type, string dateStr, long taskId)
        {
            return await service.GetCertificateSummerDataList(id, gradeId, subjectType, keyword, type, dateStr, taskId);
        }

        /// <summary>
        /// 余额查询
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="gradeId">gradeId</param>
        /// <param name="type">请求类型0:后台，1：学生端</param>
        /// <param name="subjectType">科目类型(0 全部 资产1 负债 2 权益 4 成本5 损益6)</param>
        /// <param name="keyword">查询关键字</param>
        /// <param name="taskId">任务Id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<CertificateBalanceDto>>> GetCertificateBalanceDataList(long id, long gradeId, int subjectType, string keyword, int type, long taskId)
        {
            return await service.GetCertificateBalanceDataList(id, gradeId, subjectType, keyword, type,taskId);
        }
    }
}
