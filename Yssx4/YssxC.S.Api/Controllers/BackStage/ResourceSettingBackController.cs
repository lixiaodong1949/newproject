﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Yssx.Framework.Entity;
using YssxC.S.IServices;
using YssxC.S.Dto;
using Yssx.Framework;
using Microsoft.AspNetCore.Authorization;

namespace YssxC.S.Api.Controllers
{
    /// <summary>
    /// 资源设置（后台）
    /// </summary>
    public class ResourceSettingBackController : BaseController
    {
        private IResourceSettingService _service;
        public ResourceSettingBackController(IResourceSettingService service)
        {
            this._service = service;
        }

        #region 行业管理
        /// <summary>
        /// 获取行业列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<IndustryDto>> GetIndustryList(IndustryRequest model)
        {
            return await _service.GetIndustryList(model);
        }

        /// <summary>
        /// 新增，编辑行业
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditIndustry(IndustryDto model)
        {
            return await _service.AddOrEditIndustry(model, CurrentUser.Id);
        }

        /// <summary>
        /// 删除行业
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveIndustry(long id)
        {
            return await _service.RemoveIndustry(id, CurrentUser.Id);//CurrentUser.Id
        }
        #endregion

        #region 岗位管理
        /// <summary>
        /// 获取岗位列表（不分页）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<List<PositionManageDto>>> GetPositionList()
        {
            return await _service.GetPositionList();
        }

        /// <summary>
        /// 获取岗位列表（分页）
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<PageResponse<PositionManageDto>>> GetPositionListByPage([FromQuery]PositionManageRequest model)
        {
            return await _service.GetPositionListByPage(model);
        }

        /// <summary>
        /// 新增，编辑岗位
        /// </summary>
        /// <returns></returns> 
        [HttpPost]
        public async Task<ResponseContext<bool>> SavePosition(PositionManageDto model)
        {
            return await _service.SavePosition(model, CurrentUser.Id);// 
        }

        /// <summary>
        /// 删除岗位
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> DeletePosition(long id)
        {
            return await _service.DeletePosition(id, CurrentUser.Id);
        }
        #endregion

        #region 理由管理
        /// <summary>
        /// 获取理由分类列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<ReasonDto>> GetReasonList(ReasonRequest model)
        {
            return await _service.GetReasonList(model);
        }

        /// <summary>
        /// 新增，编辑理由分类
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditReason(ReasonAddOrEditDto model)
        {
            return await _service.AddOrEditReason(model, 123);
        }

        /// <summary>
        /// 删除理由分类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveReason(long id)
        {
            return await _service.RemoveReason(id, 342);//CurrentUser.Id
        }

        #region >>>理由详情
        /// <summary>
        /// 获取理由详情列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<ReasonDetailsDto>> GetReasonDetailsList(ReasonDetailsRequest model)
        {
            return await _service.GetReasonDetailsList(model);
        }

        /// <summary>
        /// 新增，编辑理由详情
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditReasonDetails(ReasonDetailsDto model)
        {
            return await _service.AddOrEditReasonDetails(model, 123);
        }

        /// <summary>
        /// 删除理由详情
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveReasonDetails(long id)
        {
            return await _service.RemoveReasonDetails(id, 342);//CurrentUser.Id
        }
        #endregion
        #endregion

        #region 师傅评语管理
        /// <summary>
        /// 获取师傅评语列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<MastersCommentsDto>> GetMastersCommentsList(MastersCommentsRequest model)
        {
            return await _service.GetMastersCommentsList(model);
        }

        /// <summary>
        /// 新增，编辑师傅评语
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditMastersComments(MastersCommentsDto model)
        {
            return await _service.AddOrEditMastersComments(model, 123);
        }

        /// <summary>
        /// 删除师傅评语
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveMastersComments(long id)
        {
            return await _service.RemoveMastersComments(id, 342);//CurrentUser.Id
        }
        #endregion

        #region 突发事件管理
        /// <summary>
        /// 获取突发事件列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<EmergencyDto>> GetEmergencyList(EmergencyRequest model)
        {
            return await _service.GetEmergencyList(model);
        }

        /// <summary>
        /// 新增，编辑突发事件
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditEmergency(EmergencyAddOrEditDto model)
        {
            return await _service.AddOrEditEmergency(model, 123);
        }

        /// <summary>
        /// 删除突发事件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveEmergency(long id)
        {
            return await _service.RemoveEmergency(id, 342);//CurrentUser.Id
        }

        #region >>>突发详情
        /// <summary>
        /// 获取突发详情列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<PageResponse<EmergencyDetailsDto>> GetEmergencyDetailsList(EmergencyDetailsRequest model)
        {
            return await _service.GetEmergencyDetailsList(model);
        }

        /// <summary>
        /// 新增，编辑突发详情
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddOrEditEmergencyDetails(EmergencyDetailsDto model)
        {
            return await _service.AddOrEditEmergencyDetails(model, 123);
        }

        /// <summary>
        /// 删除突发详情
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> RemoveEmergencyDetails(long id)
        {
            return await _service.RemoveEmergencyDetails(id, 342);//CurrentUser.Id
        }
        #endregion
        #endregion

        #region 功能提示管理

        #endregion

        #region 开票管理（暂不做）

        #endregion

        #region 致用户信
        /// <summary>
        /// 致用户信
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> AddLetterToUsers(LetterToUsersDto model)
        {
            return await _service.AddLetterToUsers(model, CurrentUser.Id);
        }

        /// <summary>
        /// 获取最新致用户信
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ResponseContext<LetterToUsersDto>> GetLetterToUsers()
        {
            return await _service.GetLetterToUsers();
        }
        #endregion

    }
}