﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Api.Controllers.BackStage
{
    /// <summary>
    /// 申报子表
    /// </summary>
    public class DeclarationChildBackController : BaseController
    {
        private readonly IDeclarationChildService _declarationChildService;

        public DeclarationChildBackController(IDeclarationChildService declarationChildService)
        {
            _declarationChildService = declarationChildService;
        }

        /// <summary>
        /// 保存申报子表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> Save(DeclarationChildDto input)
        {
            return await _declarationChildService.Save(input, CurrentUser);
        }

        /// <summary>
        /// 根据id删除申报子表
        /// </summary>
        /// <param name="id">子表id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<bool>> Delete(long id)
        {
            return await _declarationChildService.Delete(id, CurrentUser);
        }

        /// <summary>
        /// 根据id获取申报子表
        /// </summary>
        /// <param name="id">子表id</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<DeclarationChildDto>> GetById(long id)
        {
            return await _declarationChildService.GetById(id);
        }

        /// <summary>
        /// 根据条件获取申报子表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ResponseContext<List<DeclarationChildDto>>> GetList([FromQuery]GetDeclarationChildDto input)
        {
            return await _declarationChildService.GetList(input);
        }
            
    }
}