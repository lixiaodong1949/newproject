using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using NLog.Extensions.Logging;
using System;
using System.IO;
using System.Linq;
using System.Text;
using Tas.Common.Configurations;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Logger;
using Yssx.Framework.Payment.Alipay;
using Yssx.Framework.Payment.Webchat;
using Yssx.Framework.Utils;
using Yssx.Framework.Web.Filter;
using Yssx.Redis;
using Yssx.Repository.Extensions;
using Yssx.S.ServiceImpl.Examination;
using Yssx.Swagger;
using YssxC.S.Api.Auth;
using YssxC.S.IServices;
using YssxC.S.IServices.Subject;
using YssxC.S.Service;
using YssxC.S.Service.Subject;

namespace YssxC.S.Api
{
    public class Startup
    {
        private CustsomSwaggerOptions CURRENT_SWAGGER_OPTIONS = new CustsomSwaggerOptions()
        {
            ProjectName = "云上实习",
            ApiVersions = new string[] { "v1" },//要显示的版本
            UseCustomIndex = false,
            RoutePrefix = "swagger",

            AddSwaggerGenAction = c =>
            {
                var xmlPaths = Directory.GetFiles(System.AppContext.BaseDirectory, "YssxC.*.xml").ToList();
                foreach (var path in xmlPaths)
                {
                    c.IncludeXmlComments(path, true);
                }
            },
            UseSwaggerAction = c =>
            {

            },
            UseSwaggerUIAction = c =>
            {
            }
        };
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        private void AddTokenValidation(IServiceCollection services)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(CommonConstants.IdentServerSecret));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var permissionRequirement = new PermissionRequirement(signingCredentials);
            services.AddSingleton(permissionRequirement);
        }
        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddScoped<CustomExceptionFilter>();
            services.AddMvc(options =>
            {
                options.Filters.Add<ActionModelStateFilter>();
                options.Filters.Add<AuthorizationFilter>();

                //options.Filters.Add<Filters.HttpGlobalExceptionFilter>();


            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddJsonOptions(options =>
            {
                //忽略循环引用
                //options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                //options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                //设置时间格式
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();//json字符串大小写原样输出
            }).ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            //版本控制
            // services.AddMvcCore().AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'VVV");

            services.AddApiVersioning(option =>
            {
                // allow a client to call you without specifying an api version
                // since we haven't configured it otherwise, the assumed api version will be 1.0
                option.AssumeDefaultVersionWhenUnspecified = true;
                option.ReportApiVersions = false;
            })
            .AddCustomSwagger(CURRENT_SWAGGER_OPTIONS)
            .AddCors(options =>
            {
                options.AddPolicy(CommonConstants.AnyOrigin, builder =>
                {
                    builder.AllowAnyOrigin() //允许任何来源的主机访问                             
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();//指定处理cookie                                   
                });
            });
            services.AddHttpClient();
            //services.AddAutoMapper();
            AddTokenValidation(services);
            InitService(services);
        }

        /// <summary>
        /// 依赖注入业务服务
        /// </summary>
        /// <param name="services"></param>
        private void InitService(IServiceCollection services)
        {
            //DI,服务注入
            services.AddTransient<ICaseFinancialDataService, CaseFinancialDataService>();//案例财务数据
            services.AddTransient<IGameResourceConfigService, GameResourceConfigService>();//游戏资源设置
            services.AddTransient<ISkillService, SkillService>();//能力维度
            services.AddTransient<IGameSceneService, GameSceneService>();//游戏场景
            services.AddTransient<IGameSceneCatgoryService, GameSceneCatgoryService>();//游戏场景分类
            services.AddTransient<IGameTopicService, GameTopicService>();//游戏关卡题目
            services.AddTransient<IGameLevelService, GameLevelService>();//游戏关卡
            services.AddTransient<IAssistAccountingService, AssistAccountingService>();//账簿-辅助核算业务服务
            services.AddTransient<IRegulationService, RegulationService>();//规章制度模板业务服务
            services.AddTransient<ICertificateTopicService, CertificateTopicService>();//凭证题业务服务			
            services.AddTransient<ICaseService, CaseService>(); // 案例服务
            services.AddTransient<IResourceSettingService, ResourceSettingService>();//资源设置服务
            services.AddTransient<IAbstractService, AbstractService>(); // 摘要服务
            services.AddTransient<IBankAccountService, BankAccountService>(); // 银行账户服务
            services.AddTransient<ITaskService, TaskService>(); // 任务服务
            services.AddTransient<ITagService, TagService>(); // 标签服务
            services.AddTransient<ITopicService, TopicService>();//题目业务服务
            services.AddTransient<IOnboardingService, OnboardingService>();//入职业务服务
            services.AddTransient<ISubjectService, SubjectService>();
            services.AddTransient<IDeclarationTemplateParentService, DeclarationTemplateParentService>(); // 申报表主表模板服务
            services.AddTransient<IDeclarationTemplateChildService, DeclarationTemplateChildService>(); // 申报表子表模板服务
            services.AddTransient<IDeclarationParentService, DeclarationParentService>(); // 申报主表服务
            services.AddTransient<IDeclarationChildService, DeclarationChildService>(); // 申报子表服务
            services.AddTransient<ICalendarBillService, CalendarBillService>(); //日历票据服务
            services.AddTransient<ICaseRegulationService, CaseRegulationService>(); // 公司制度
            services.AddTransient<ITopicBaseService, TopicBaseService>(); //题目公共基础服务
            services.AddTransient<IUserPracticeService, UserPracticeService>();
            services.AddTransient<IReceivePaymentService, ReceivePaymentService>();
            services.AddTransient<ITemplateService, TemplateService>();
            services.AddTransient<ICertificateService, CertificateService>();
            services.AddTransient<IGameCertificateService, GameCertificateService>(); 
            services.AddTransient<IExamIntegralService, ExamIntegralService>(); //用户考试获得积分
            services.AddTransient<ISummaryService, SummaryService>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime lifetime)
        {
            app.UseExceptionHandler(builder =>
            {
                builder.Run(async context =>
                {
                    context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                    context.Response.ContentType = "application/json";
                    var ex = context.Features.Get<Microsoft.AspNetCore.Diagnostics.IExceptionHandlerFeature>();
                    if (ex?.Error != null)
                    {
                        CommonLogger.Error("UseExceptionHandler", ex?.Error);
                    }
                    await context.Response.WriteAsync(ex?.Error?.Message ?? "an error occure");
                });
            }).UseHsts();
            //string enableSwagger = Configuration["swaggerEnable"];
            #region cosul
            //ServiceEntity serviceEntity = new ServiceEntity
            //{
            //    IP = "localhost",   //服务运行地址
            //    Port = Convert.ToInt32(Configuration["Consul:ServicePort"]), //服务运行端口
            //    ServiceName = Configuration["Consul:Name"], //服务标识,Ocelot中会用到
            //    ConsulIP = Configuration["Consul:IP"], //Consul运行地址
            //    ConsulPort = Convert.ToInt32(Configuration["Consul:Port"])  //Consul运行端口（默认8500）
            //};
            //app.RegisterConsul(lifetime, serviceEntity);
            #endregion
            app.UseMvc().UseCustomSwagger(CURRENT_SWAGGER_OPTIONS);
            //app.UseMvc();
            //if (enableSwagger == "1")
            //{
            //    app.UseCustomSwagger(CURRENT_SWAGGER_OPTIONS);
            //}
            app.UseCors(CommonConstants.AnyOrigin);
            app.UseStaticFiles();
            ////加载Nlog的nlog.config配置文件
            //NLog.LogManager.LoadConfiguration("nlog.config");

            string[] servers = Configuration["ImServers"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);//im服务器地址
            ProjectConfiguration.GetInstance().
                RegisterConfiguration(Configuration).
                UseCommonLogger(loggerFactory.AddNLog()).
                UseRedis(Configuration["RedisConnection"]).
                UseFreeSqlRepository(Configuration["DbConnection"], Configuration["ReadDbConnection1"]).
                UseFreeSqlRepositorySx(Configuration["SxDbConnection"], Configuration["SxReadDbConnection1"]).
                //UseFreeSqlBDRepository(Configuration["DbBigDataConnection"], Configuration["BigDbConnection1"], Configuration["BigDbConnection2"], Configuration["BigDbConnection3"], Configuration["BigDbConnection4"]).
                //UseFreeSqlBDRepository(Configuration["DbBigDataConnection"], Configuration["BigDbConnection1"], Configuration["BigDbConnection2"]).
                UseAutoMapper(RuntimeHelper.GetAllAssemblies())
                .UseWebchat(Configuration)
                .UseAlipay(Configuration);
            //UseImClient(Configuration["ImRedisConnection"], servers);

            DataMergeHelper.Start();
        }
    }
}
