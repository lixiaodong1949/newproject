﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Tas.Common.Configurations;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Redis;
using Yssx.S.Pocos;

namespace Yssx.Intergral.JobServer
{
    class Program
    {
        static void Main(string[] args)
        {
            ab();
            Console.WriteLine("Hello World!");
            Console.ReadKey();
        }

        public static async void ab()
        {
            string sqlConncetion = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
            string sqlJsConncetion = ConfigurationManager.ConnectionStrings["JsConncetion"].ConnectionString;
            string sqlJnConncetion = ConfigurationManager.ConnectionStrings["JnConncetion"].ConnectionString;

            ProjectConfiguration.GetInstance().UseFreeSqlRepository(sqlConncetion)
                .UseFreeSqlJSRepository(sqlJsConncetion)
                .UseFreeSqlJNRepository(sqlJnConncetion);

            var strTop = ConfigurationManager.AppSettings["top"];
            int top = 1000;
            if (!int.TryParse(strTop, out top))
                top = 1000;

            var repo = DbContext.FreeSql.GetRepository<YssxIntegral>();

            var list = await repo.Where(x => x.IsDelete == 0).OrderByDescending(e => e.Number).OrderByDescending(e => e.UpdateTime).Limit(top)
                //.From<YssxStudent>((a, b) => a.InnerJoin(aa => aa.Id == b.UserId))
                .ToListAsync(a => new YssxIntegralTop
                { Id = a.Id, UserId = a.UserId, Number = a.Number });

            GetYssx(list);//云实训
            GetJn(list);//技能抽查
                        //GetJS(list);//竞赛          

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            {
                try
                {
                    var repo1 = uow.GetRepository<YssxIntegralTop>();
                    await repo1.DeleteAsync(e => 1 == 1);
                    await repo1.InsertAsync(list);
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    throw;
                }
            }
        }

        private static void GetYssx(List<YssxIntegralTop> yssxIntegrals)
        {
            var userIds = yssxIntegrals.Where(e => string.IsNullOrWhiteSpace(e.RealName)).Select(e => e.UserId).ToArray();
            var users = DbContext.FreeSql.GetRepository<YssxUser>().Select
                 .WhereCascade(e => e.IsDelete == CommonConstants.IsNotDelete)
                 .Where(e => userIds.Contains(e.Id))
                 .From<YssxStudent>((a, b) => a.LeftJoin(aa => aa.Id == b.UserId))
                 .ToList((a, b) => new YssxIntegralTop
                 {
                     Sex = a.Sex,
                     UserId = a.Id,
                     MobilePhone = a.MobilePhone,
                     RealName = a.RealName,
                     SchoolName = b.SchoolName,
                 });

            geta(yssxIntegrals, users, RegisterSourceEnum.Yssx);

        }
        private static void GetJn(List<YssxIntegralTop> yssxIntegrals)
        {
            var userIds = yssxIntegrals.Where(e => string.IsNullOrWhiteSpace(e.RealName)).Select(e => e.UserId).ToArray();
            var users = DbContext.JNFreeSql.GetRepository<YssxUser>().Select
                 .WhereCascade(e => e.IsDelete == CommonConstants.IsNotDelete)
                 .Where(e => userIds.Contains(e.Id))
                 .From<YssxSchool>((a, b) => a.LeftJoin(aa => aa.TenantId == b.Id))
                 .ToList((a, b) => new YssxIntegralTop
                 {
                     Sex = a.Sex,
                     UserId = a.Id,
                     MobilePhone = a.MobilePhone,
                     RealName = a.RealName,
                     SchoolName = b.Name,
                 });

            geta(yssxIntegrals, users, RegisterSourceEnum.Jncc);


        }

        private static void GetJS(List<YssxIntegralTop> yssxIntegrals)
        {
            var userIds = yssxIntegrals.Where(e => string.IsNullOrWhiteSpace(e.RealName)).Select(e => e.UserId).ToArray();
            var users = DbContext.JSFreeSql.GetRepository<YssxUser>().Select
                 .WhereCascade(e => e.IsDelete == CommonConstants.IsNotDelete)
                 .Where(e => userIds.Contains(e.Id))
                 .From<YssxSchool>((a, b) => a.LeftJoin(aa => aa.TenantId == b.Id))
                 .ToList((a, b) => new YssxIntegralTop
                 {
                     UserId = a.Id,
                     MobilePhone = a.MobilePhone,
                     RealName = a.RealName,
                     SchoolName = b.Name,
                 });

            geta(yssxIntegrals, users, RegisterSourceEnum.Js);

        }
        private static void geta(List<YssxIntegralTop> yssxIntegrals, List<YssxIntegralTop> users, RegisterSourceEnum registerSource)
        {
            yssxIntegrals.ForEach(e =>
            {
                var item = users.Where(a => a.UserId == e.UserId).FirstOrDefault();
                if (item != null)
                {
                    e.RealName = item.RealName;
                    e.SchoolName = item.SchoolName;
                    e.MobilePhone = item.MobilePhone;
                    e.Sex = item.Sex;
                    e.RegisterSource = registerSource;
                }
            });

        }


        public static async void aa()
        {
            var redisString = ConfigurationManager.AppSettings["RedisConnection"];
            string sqlConncetion = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
            string sqlJsConncetion = ConfigurationManager.ConnectionStrings["JsConncetion"].ConnectionString;
            string sqlJnConncetion = ConfigurationManager.ConnectionStrings["JnConncetion"].ConnectionString;
            ProjectConfiguration.GetInstance().UseRedis(redisString);
            ProjectConfiguration.GetInstance().UseFreeSqlRepository(sqlConncetion)
                .UseFreeSqlJSRepository(sqlJsConncetion)
                .UseFreeSqlJNRepository(sqlJnConncetion);

            var repo = DbContext.JNFreeSql.GetRepository<YssxUser>();

            var registerSource = RegisterSourceEnum.Jncc;
            var list = await repo.Where(x => x.IsDelete == 0)
                //.From<YssxStudent>((a, b) => a.InnerJoin(aa => aa.Id == b.UserId))
                .ToListAsync(a => new YssxUser
                {
                    Id = a.Id,
                    MobilePhone = a.MobilePhone,
                    RealName = a.RealName,
                    Password = a.Password,
                    IdNumber = a.IdNumber,
                    Status = 1,
                    NikeName = a.RealName,
                    UserType = 7,
                    RegisterSource = registerSource,
                    //b.SchoolName,
                    //b.ClassName,
                    //b.ClassId,
                    //StudentId = b.Id
                });
            int s = 0;
            var f = new List<string>();
            var error = new List<long>();
            List<YssxUser> users = new List<YssxUser>();
            foreach (var userInfo in list)
            {
                Console.WriteLine($"{userInfo.Id}");
                bool isExist = await DbContext.FreeSql.GetRepository<YssxUser>().Where(x => x.MobilePhone == userInfo.MobilePhone).AnyAsync();
                if (!isExist)
                {
                    s++;
                    users.Add(userInfo);
                    if (users.Count >= 2)
                    {
                        try
                        {
                            await DbContext.FreeSql.GetRepository<YssxUser>().InsertAsync(users);
                        }
                        catch (Exception ex)
                        {
                            foreach (var item in users)
                            {
                                try
                                {
                                    await DbContext.FreeSql.GetRepository<YssxUser>().InsertAsync(item);
                                }
                                catch (Exception ex1)
                                {
                                    error.Add(item.Id);
                                }
                            }
                        }
                        finally
                        {
                            users.Clear();
                        }

                        //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
                        //{
                        //    try
                        //    {
                        //        await uow.GetRepository<YssxUser>().InsertAsync(users);
                        //        //.InsertAsync(new YssxUser
                        //        //{
                        //        //    Id = userInfo.Id,
                        //        //    MobilePhone = userInfo.MobilePhone,
                        //        //    ClientId = Guid.NewGuid(),
                        //        //    RealName = userInfo.RealName,
                        //        //    Password = userInfo.Password,
                        //        //    IdNumber = userInfo.IdNumber,
                        //        //    Status = 1,
                        //        //    NikeName = userInfo.RealName,
                        //        //    UserType = 7,
                        //        //    RegisterSource = registerSource,
                        //        //});
                        //        //await uow.GetRepository<YssxStudent>().InsertAsync(new YssxStudent
                        //        //{
                        //        //    Id = userInfo.StudentId,
                        //        //    SchoolName = userInfo.SchoolName,
                        //        //    ClassName = userInfo.ClassName,
                        //        //    ClassId = userInfo.ClassId,
                        //        //    RegisterSource = registerSource,
                        //        //});
                        //        uow.Commit();
                        //        users.Clear();

                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        Console.WriteLine($"{ex.Message}");
                        //        uow.Rollback();
                        //        e.Add(userInfo.Id);
                        //    }
                        //}
                    }
                }
                else
                {
                    f.Add($"{userInfo.Id} {userInfo.RealName} {userInfo.MobilePhone};");
                }
            }

            Console.WriteLine($"总共需要同步用户数：{list.Count}");
            Console.WriteLine($"总共同步成功用户数：{s}");
            Console.WriteLine($"已存在的用户数：{f.Count}");
            //foreach (var item in f)
            //{
            //    Console.Write(item);
            //}
            Console.ReadKey();



        }

    }
}
