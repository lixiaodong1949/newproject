﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Domain.Models
{
    /// <summary>
    /// 课程审核状态更新
    /// </summary>
    public class CourseEditAuditStatus
    {
        public long CourseId { get;private set; }

        public int AuditStatus { get; private set; }

        /// <summary>
        /// 审核描述
        /// </summary>
        public string Description { get;private set; }

        public CourseEditAuditStatus() { }

        public CourseEditAuditStatus(long cid,int auditStatus,string description) 
        {
            this.CourseId = cid;
            this.AuditStatus = auditStatus;
            this.Description = description;
        }
    }
}
