﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Sx.Course.Domain.Models
{
    /// <summary>
    /// 课程信息
    /// </summary>
    public class CourseInfo
    {
        public long Id { get;private set; }

        public string CourseName { get;private set; }

        /// <summary>
        /// 特点副标
        /// </summary>
        public string CourseTitle { get;private set; }

        /// <summary>
        /// 课程类型
        /// </summary>
        public long CourseTypeId { get;private set; }

        /// <summary>
        /// 知识点
        /// </summary>
        public long KnowledgePointId { get;private set; }

        /// <summary>
        /// 学历标识
        /// </summary>
        public int Education { get;private set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get;private set; }

        /// <summary>
        /// 出版社
        /// </summary>
        public string PublishingHouse { get;private set; }

        /// <summary>
        /// 出版时间
        /// </summary>
        public string PublishingDate { get;private set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get;private set; }

        /// <summary>
        /// 
        /// </summary>
        public long TenantId { get;private set; }


        /// <summary>
        /// 
        /// </summary>
        public long CreateBy { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public int AuditStatus { get; private set; }

        /// <summary>
        /// 审核描述
        /// </summary>
        public string Description { get; private set; }



        public CourseInfo() { }

        public CourseInfo(long id,string courseName,string courseTitle,long courseTypeId,long knowledgePointId,int education,string author,string publishingHouse,string publishingDate, string images,long tenantId,long createBy,int auditStatus=0,string desc=null)
        {
            this.Id = id;
            this.CourseName = courseName;
            this.CourseTitle = courseTitle;
            this.CourseTypeId = courseTypeId;
            this.KnowledgePointId = knowledgePointId;
            this.Education = education;
            this.Author = author;
            this.PublishingHouse = publishingHouse;
            this.PublishingDate = publishingDate;
            this.Images = images;
            this.TenantId = tenantId;
            this.CreateBy = createBy;
            this.AuditStatus = auditStatus;
            this.Description = desc;
        }
    }
}
