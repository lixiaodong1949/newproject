﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Domain.Models
{
    /// <summary>
    /// 编辑更新课程
    /// </summary>
    public class CourseEditInfo
    {
        public long CourseId { get; set; }
        public string CourseName { get; private set; }

        /// <summary>
        /// 特点副标
        /// </summary>
        public string CourseTitle { get; private set; }

        /// <summary>
        /// 课程类型
        /// </summary>
        public long CourseTypeId { get; private set; }

        /// <summary>
        /// 知识点
        /// </summary>
        public long KnowledgePointId { get; private set; }

        /// <summary>
        /// 学历标识
        /// </summary>
        public int Education { get; private set; }

        /// <summary>
        /// 作者
        /// </summary>
        public string Author { get; private set; }

        /// <summary>
        /// 出版社
        /// </summary>
        public string PublishingHouse { get; private set; }

        /// <summary>
        /// 出版时间
        /// </summary>
        public string PublishingDate { get; private set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; private set; }

        public CourseEditInfo() { }

        public CourseEditInfo(long courseId,string courseName, string courseTitle, long courseTypeId, long knowledgePointId, int education, string author, string publishingHouse, string publishingDate, string images)
        {
            this.CourseId = courseId;
            this.CourseName = courseName;
            this.CourseTitle = CourseTitle;
            this.CourseTypeId = courseTypeId;
            this.KnowledgePointId = knowledgePointId;
            this.Education = education;
            this.Author = author;
            this.PublishingHouse = publishingHouse;
            this.PublishingDate = publishingDate;
            this.Images = images;
        }
    }
}
