﻿using ENode.Domain;
using Sx.Course.Domain.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Domain.Models
{
    /// <summary>
    /// 课程领域模型
    /// </summary>
    public class SxCourse : AggregateRoot<long>
    {
        private CourseInfo _courseInfo;

        public SxCourse(long id, CourseEditInfo editInfo) : base(id)
        {
            ApplyEvent(new CourseUpdated(editInfo));
        }

        /// <summary>
        /// 创建新课程
        /// </summary>
        /// <param name="id"></param>
        /// <param name="info"></param>
        public SxCourse(long id, CourseInfo info) : base(id)
        {
            ApplyEvent(new CourseCreated(info));
        }

        /// <summary>
        /// 修改课程
        /// </summary>
        /// <param name="info"></param>
        public void Update(CourseEditInfo info)
        {
            ApplyEvent(new CourseUpdated(info));
        }

        /// <summary>
        /// 修改删除状态
        /// </summary>
        public void EditToStatus(long courseId)
        {
            ApplyEvent(new CourseStatusEdited(courseId));
        }

        /// <summary>
        /// 专业组审核课程
        /// </summary>
        public void EditAuditStatus(CourseEditAuditStatus auditStatus)
        {
            ApplyEvent(new CourseAuditStausEdited(auditStatus));
        }

        private void Handle(CourseCreated evnt)
        {
            _id = evnt.AggregateRootId;
            _courseInfo = evnt.Info;
        }

        private void Handle(CourseUpdated evnt)
        {
            var editInfo = evnt.Info;
            _courseInfo = new CourseInfo(editInfo.CourseId, editInfo.CourseName, editInfo.CourseTitle, editInfo.CourseTypeId, editInfo.KnowledgePointId, editInfo.Education, editInfo.Author, editInfo.PublishingHouse, editInfo.PublishingDate, editInfo.Images, 0, 0, 0, string.Empty);
        }

        private void Handle(long courseId)
        {
            _courseInfo = new CourseInfo(
              courseId,
               _courseInfo.CourseName,
               _courseInfo.CourseTitle,
               _courseInfo.CourseTypeId,
               _courseInfo.KnowledgePointId,
               _courseInfo.Education,
               _courseInfo.Author,
               _courseInfo.PublishingHouse,
               _courseInfo.PublishingDate,
               _courseInfo.Images,
               _courseInfo.TenantId,
               _courseInfo.CreateBy,
               _courseInfo.AuditStatus,
               _courseInfo.Description);
        }

        private void Handle(CourseAuditStausEdited evnt)
        {
            var editInfo = evnt.editAuditStatus;
            _courseInfo = new CourseInfo(
                editInfo.CourseId,
                _courseInfo.CourseName,
                _courseInfo.CourseTitle,
                _courseInfo.CourseTypeId,
                _courseInfo.KnowledgePointId,
                _courseInfo.Education,
                _courseInfo.Author,
                _courseInfo.PublishingHouse,
                _courseInfo.PublishingDate,
                _courseInfo.Images,
                _courseInfo.TenantId,
                _courseInfo.CreateBy,
                editInfo.AuditStatus,
                editInfo.Description);
        }
    }
}
