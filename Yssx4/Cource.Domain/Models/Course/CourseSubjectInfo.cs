﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Domain.Models.Course
{
    public class CourseSubjectInfo
    {
        /// <summary>
        /// 科目代码
        /// </summary>
        public long SubjectCode { get; set; }


        /// <summary>
        /// 父级ID 最多四级
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 一级代码
        /// </summary>
        public string Code1 { get; set; }

        /// <summary>
        /// 科目名称
        /// </summary>
        public string SubjectName { get; set; }

        /// <summary>
        /// 余额方向
        /// </summary>
        public string Direction { get; set; }

        /// <summary>
        /// 科目类型(1:资产,2:负债)
        /// </summary>
        public int SubjectType { get; set; }
    }
}
