﻿using ECommon.Components;
using Sx.Course.Domain.Repositories;

namespace Sx.Course.Domain.Services
{
    [Component]
    public class RegistCourseSubjectService
    {
        private readonly ICourseSubjectRepository _subjectRepository;

        public RegistCourseSubjectService(ICourseSubjectRepository subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public void AddCourseSubject(long cid)
        {
            if (_subjectRepository.ExistSubjectById(cid)) return;
            var courseSubjects = _subjectRepository.GetSubjectTempList();
            if (courseSubjects != null)
            {
                _subjectRepository.AddCourseSubject(cid, courseSubjects);
            }
        }
    }
}
