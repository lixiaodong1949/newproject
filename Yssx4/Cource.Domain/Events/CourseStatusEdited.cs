﻿using ENode.Eventing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Domain.Events
{
    public class CourseStatusEdited : DomainEvent<long>
    {
        public long CourseId { get; private set; }
        public CourseStatusEdited() { }


        public CourseStatusEdited(long courseId)
        {
            this.CourseId = courseId;
        }
    }
}
