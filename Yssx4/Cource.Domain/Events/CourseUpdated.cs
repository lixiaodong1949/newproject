﻿using ENode.Eventing;
using Sx.Course.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Domain.Events
{
    public class CourseUpdated:DomainEvent<long>
    {
        public CourseEditInfo Info { get; private set; }
        public CourseUpdated() { }

        public CourseUpdated(CourseEditInfo info)
        {
            this.Info = info;
        }
    }
}
