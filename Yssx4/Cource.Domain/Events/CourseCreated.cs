﻿using Sx.Course.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Domain.Events
{
    [Serializable]
    public class CourseCreated : CourseEvent
    {
        public CourseCreated() { }

        public CourseCreated(CourseInfo info) : base(info)
        {

        }
    }
}
