﻿using ENode.Eventing;
using Sx.Course.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sx.Course.Domain.Events
{
    public class CourseEvent: DomainEvent<long>
    {
        public CourseInfo Info { get; set; }

        public CourseEvent() { }

        public CourseEvent(CourseInfo info)
        {
            Info = info;
        }
    }
}
