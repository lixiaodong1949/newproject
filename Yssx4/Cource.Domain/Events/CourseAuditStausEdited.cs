﻿using ENode.Eventing;
using Sx.Course.Domain.Models;

namespace Sx.Course.Domain.Events
{
    public class CourseAuditStausEdited : DomainEvent<long>
    {
        public CourseEditAuditStatus editAuditStatus { get; private set; }
        public CourseAuditStausEdited()
        {
        }

        public CourseAuditStausEdited(CourseEditAuditStatus editAuditStatus)
        {
            this.editAuditStatus = editAuditStatus;
        }
    }
}
