﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Yssx.Framework.Logger;

namespace Yssx.Repository.Extensions
{
    public class PerformanceMoniter : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        private Stopwatch stopwatch = new Stopwatch();
        private string _message;
        public PerformanceMoniter(string message)
        {
            _message = message;
            stopwatch.Start();
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            stopwatch.Stop();
            CommonLogger.Info(_message + $",总共耗时:({stopwatch.ElapsedMilliseconds})毫秒");
        }
    }
}
