﻿using FreeSql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using DbContext = Yssx.Framework.Dal.DbContext;

namespace Yssx.Repository
{
    public class FreeSqlRepository<TEntity, TKey> : BaseRepository<TEntity, TKey> where TEntity : BaseEntity<TKey>
    {
        public FreeSqlRepository() :
            base(DbContext.FreeSql, null)
        {

        }
        /// <summary>
        /// 更新实体，忽略指定列
        /// </summary>
        /// <param name="t"></param>
        /// <param name="columns"></param>
        /// <returns></returns>

        public int UpdateIgnoreColumns(TEntity t, Expression<Func<TEntity, object>> columns)
        {
            return DbContext.FreeSql.Update<TEntity>().SetSource(t).IgnoreColumns(columns).ExecuteAffrows();
        }
        /// <summary>
        /// 更新指定列
        /// </summary>
        /// <param name="t"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public int Update(TEntity t, Expression<Func<TEntity, object>> columns)
        {
            return DbContext.FreeSql.Update<TEntity>().SetSource(t).UpdateColumns(columns).ExecuteAffrows();
        }
        ///// <summary>
        ///// 更新实体，指定更新列
        ///// </summary>
        ///// <param name="t"></param>
        ///// <param name="express"></param>
        ///// <returns></returns>
        //public int UpdateDefineColumns(TEntity t, Expression<Func<TEntity, object>> express)
        //{
        //    var fileds = t.GetType().GetFields().Cast<MemberInfo>();
        //    var members = t.ExpressionMemberNames(express);
        //    var update = _fsql.Update<TEntity>().SetSource(t);
        //    foreach (var mName in members)
        //    {
        //        var memberInfo = fileds.FirstOrDefault(m => m.Name == mName);
        //        if (memberInfo == null)
        //            continue;
        //        //update.Set(,);
        //        update.SetRaw($"{memberInfo.Name} = {0}", memberInfo.GetValue(t));
        //    }

        //    //foreach (var col in columns)
        //    //{

        //    //}
        //    return update.ExecuteAffrows();
        //}

        ///// <summary>
        ///// 更新实体，指定更新列
        ///// </summary>
        ///// <param name="t"></param>
        ///// <param name="columns"></param>
        ///// <returns></returns>
        //public int UpdateDefineColumns(TEntity t, IEnumerable<Expression<Func<TEntity, object>>> columns)
        //{
        //    var update = _fsql.Update<TEntity>().SetSource(t);
        //    foreach (var col in columns)
        //    {
        //        update.setr(col);
        //    }
        //    return update.ExecuteAffrows();
        //}
        ///// <summary>
        ///// 根据主键更新
        ///// </summary>
        ///// <param name="t"></param>
        ///// <param name="columns"></param>
        ///// <returns></returns>
        //public int UpdateDefineColumns(TKey t, IEnumerable<Expression<Func<TEntity, object>>> columns)
        //{
        //    var update = _fsql.Update<TEntity>(t);
        //    foreach (var col in columns)
        //    {
        //        update.Set(col);
        //    }
        //    return update.ExecuteAffrows();
        //}

    }
}
