﻿using Yssx.Framework.Entity;

namespace Yssx.Repository.Base
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TKey"></typeparam>
    public class TenantBizBaseRepository<TEntity, TKey> : BizBaseRepository<TEntity, TKey> where TEntity : TenantBizBaseEntity<TKey>
    {
    }
}
