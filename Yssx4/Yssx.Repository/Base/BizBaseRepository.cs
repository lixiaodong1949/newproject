﻿using FreeSql;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.Repository.Base
{
    public class BizBaseRepository<TEntity, TKey> : BaseRepository<TEntity, TKey> where TEntity : BizBaseEntity<TKey>
    {
        /// <summary>
        /// 
        /// </summary>
        public BizBaseRepository() : base(Framework.Dal.DbContext.FreeSql, null)
        {
            //Select = base.Select.Where(m => m.IsDelete == CommonConstants.IsNotDelete);
        }
        ///// <summary>
        ///// 
        ///// </summary>
        //public new ISelect<TEntity> Select { get;private set; }
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="exp"></param>
        ///// <returns></returns>
        //public new ISelect<TEntity> Where(Expression<Func<TEntity, bool>> exp)
        //{
        //    return Select.Where(exp);
        //}
    }
}
