﻿using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    public interface IBankAccountService
    {
        /// <summary>
        /// 保存银行账户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveBankAccount(BankAccountDto input, UserTicket currentUser);

        /// <summary>
        /// 根据id获取银行账户信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<BankAccountDto>> GetBankAccount(long id);

        /// <summary>
        /// 查询银行账户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResponse<BankAccountDto>> GetBankAccountList(GetBankAccountInput input);

        /// <summary>
        /// 根据id删除银行账户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteBankAccount(long id, UserTicket currentUser);
    }
}
