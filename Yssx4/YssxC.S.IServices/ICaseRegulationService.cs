﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 案例规章制度业务服务接口
    /// </summary>
    public interface ICaseRegulationService
    {
        /// <summary>
        /// 删除规章制度信息
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteById(long id, UserTicket currentUser);
        /// <summary>
        /// 根据Id获取单条规章制度信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<CaseRegulationDto>> GetInfoById(long id);
        /// <summary>
        /// 保存规章制度
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Save(CaseRegulationDto model, UserTicket currentUser);
        /// <summary>
        /// 根据ParentId获取子类规章制度
        /// </summary>
        /// <param name="caseId">父类Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<CaseRegulationListDto>>> GetListByCaseId(long caseId);
        /// <summary>
        /// 保存规章制度 应用规章制度模板
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveCaseUseRegulation(CaseUseRegulationsDto model, UserTicket currentUser);
    }
}
