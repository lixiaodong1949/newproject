﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 资源设置
    /// </summary>
    public interface IResourceSettingService
    {
        #region 行业管理
        /// <summary>
        /// 获取行业列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<IndustryDto>> GetIndustryList(IndustryRequest model);

        /// <summary>
        /// 新增或修改行业
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditIndustry(IndustryDto model, long currentUserId);

        /// <summary>
        /// 删除行业
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveIndustry(long id, long currentUserId);
        #endregion

        #region 岗位管理
        /// <summary>
        /// 获取岗位列表（不分页）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<PositionManageDto>>> GetPositionListByPracticePattern(PositionType positionType);

        /// <summary>
        /// 获取岗位列表（不分页）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<PositionManageDto>>> GetPositionList();

        /// <summary>
        /// 获取岗位列表（分页）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<PositionManageDto>>> GetPositionListByPage(PositionManageRequest model);

        /// <summary>
        /// 保存岗位
        /// </summary>
        /// <returns></returns> 
        Task<ResponseContext<bool>> SavePosition(PositionManageDto model, long userId);

        /// <summary>
        /// 删除岗位
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeletePosition(long id, long userId);
        #endregion

        #region 理由管理
        /// <summary>
        /// 获取理由分类列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<ReasonDto>> GetReasonList(ReasonRequest model);

        /// <summary>
        /// 新增或修改理由分类
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditReason(ReasonAddOrEditDto model, long currentUserId);

        /// <summary>
        /// 删除理由分类
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveReason(long id, long currentUserId);

        #region >>>理由详情
        /// <summary>
        /// 获取理由详情列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<ReasonDetailsDto>> GetReasonDetailsList(ReasonDetailsRequest model);

        /// <summary>
        /// 新增或修改理由详情
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditReasonDetails(ReasonDetailsDto model, long currentUserId);

        /// <summary>
        /// 删除理由详情
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveReasonDetails(long id, long currentUserId);
        #endregion

        /// <summary>
        /// </summary>
        /// 根据Id获取理由详情
        /// <returns></returns>
        Task<ResponseContext<ReasonDetailsDto>> GetReasonDetailsById(long id);

        #endregion

        #region 师傅评语管理
        /// <summary>
        /// 获取师傅评语列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<MastersCommentsDto>> GetMastersCommentsList(MastersCommentsRequest model);

        /// <summary>
        /// 新增或修改师傅评语
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditMastersComments(MastersCommentsDto model, long currentUserId);

        /// <summary>
        /// 删除师傅评语
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveMastersComments(long id, long currentUserId);
        #endregion

        #region 突发事件
        /// <summary>
        /// 获取理由分类列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<EmergencyDto>> GetEmergencyList(EmergencyRequest model);

        /// <summary>
        /// 新增或修改理由分类
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditEmergency(EmergencyAddOrEditDto model, long currentUserId);

        /// <summary>
        /// 删除理由分类
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveEmergency(long id, long currentUserId);

        #region >>>理由详情
        /// <summary>
        /// 获取理由详情列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<EmergencyDetailsDto>> GetEmergencyDetailsList(EmergencyDetailsRequest model);

        /// <summary>
        /// 新增或修改理由详情
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditEmergencyDetails(EmergencyDetailsDto model, long currentUserId);

        /// <summary>
        /// 删除理由详情
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveEmergencyDetails(long id, long currentUserId);
        #endregion
        #endregion

        #region 功能提示

        #endregion

        #region 开票管理（暂不做）

        #endregion

        #region 致用户信
        /// <summary>
        /// 新增致用户信
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddLetterToUsers(LetterToUsersDto model, long currentUserId);

        /// <summary>
        /// 获取最新致用户信
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<LetterToUsersDto>> GetLetterToUsers();
        #endregion
    }
}
