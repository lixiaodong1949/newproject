﻿using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    public interface ITagService
    {
        /// <summary>
        /// 根据条件查询标签
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResponse<TagDto>> GetTagList(GetTagInput input);

        /// <summary>
        /// 保存标签
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveTag(TagDto input, UserTicket currentUser);

        /// <summary>
        /// 根据id删除标签
        /// </summary>
        /// <param name="id">标签id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTag(long id, UserTicket currentUser);

        /// <summary>
        /// 启用或禁用标签
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Toggle(ToggleInput input,UserTicket currentUser);
    }
}
