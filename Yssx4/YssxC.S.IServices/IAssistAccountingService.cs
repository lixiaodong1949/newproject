﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 账簿-辅助核算业务服务接口
    /// </summary>
    public interface IAssistAccountingService
    {
        /// <summary>
        /// 保存账簿-辅助核算
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Save(AssistAccountingDto model, UserTicket CurrentUser);
        /// <summary>
        /// 根据Id获取一条账簿-辅助核算信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<AssistAccountingDto>> GetInfoById(long id);
        /// <summary>
        /// 根据凭证id删除账簿-辅助核算
        /// </summary>
        /// <param name="id">分录题id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteById(long id, UserTicket CurrentUser);
        /// <summary>
        /// 根据公司id获取账簿-辅助核算信息
        /// </summary>
        /// <param name="EnterpriseId">企业Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<AssistAccountingQueryListResponseDto>>> GetListByCaseId(long caseId);
        /// <summary>
        /// 根据公司id获取账簿-辅助核算分页列表
        /// </summary>
        /// <param name="caseId">企业Id</param>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<AssistAccountingQueryPageResponseDto>>> GetListPage(AssistAccountingQueryPageDto queryModel);
        /// <summary>
        /// 根据caseid获取所有收款账号公司名称
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<GetNameListByCaseIdResponseDto>>> GetNameListByCaseId(long caseId);

        /// <summary>
        /// 根据公司id获取账簿-辅助核算分页列表
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        Task<ResponseContext<List<GetNameListResponseDto>>> GetNameList(GetNameListDto queryModel);
        /// <summary>
        /// 根据caseid获取所有收款账号公司名称(去重)
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<GetNameDistinctListByCaseIdResponseDto>>> GetNameDistinctListByCaseId(AssistAccountingQueryDto queryDto);
    }
}
