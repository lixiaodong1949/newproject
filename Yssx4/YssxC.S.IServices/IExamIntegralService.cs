﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 用户考试获得积分
    /// </summary>
    public interface IExamIntegralService
    {
        /// <summary>
        /// 获取总积分
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        Task<ResponseContext<decimal>> GetSum(long userId);
    }
}
