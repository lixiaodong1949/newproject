﻿using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    public interface IAbstractService
    {
        /// <summary>
        /// 保存摘要
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser">当前用户</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveAbstract(AbstractDto input,UserTicket currentUser);

        /// <summary>
        /// 根据id获取摘要
        /// </summary>
        /// <param name="id">摘要id</param>
        /// <returns></returns>
        Task<ResponseContext<AbstractDto>> GetAbstract(long id);

        /// <summary>
        /// 根据条件查询摘要
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResponse<AbstractDto>> GetAbstractList(GetAbstractInput input);

        /// <summary>
        /// 根据id删除摘要
        /// </summary>
        /// <param name="id">摘要id</param>
        /// <param name="currentUser">当前用户</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteAbstract(long id,UserTicket currentUser);
    }
}
