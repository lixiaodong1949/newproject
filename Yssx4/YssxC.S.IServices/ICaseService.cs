using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    public interface ICaseService
    {
        /// <summary>
        /// 保存案例
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveCase(CaseDto input, UserTicket currentUser);

        /// <summary>
        /// 根据id获取案例
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<CaseDto>> GetCase(long id);

        /// <summary>
        /// 查询案例
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<CaseDto>> GetCaseList(GetCaseInput input);

        /// <summary>
        /// 根据id删除案例
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCase(long id, UserTicket currentUser);

        /// <summary>
        /// 启用或禁用 true:启用 false:禁用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Toggle(ToggleInput input, UserTicket currentUser);

        /// <summary>
        /// 查询企业列表 - 关联产品
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<GetCaseListByNameDto>> GetCaseListByName(GetCaseInput model);
    
        /// <summary>
        /// 获取所有案例列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<CaseGetAllCaseListResponseDto>>> GetAllCaseList(CaseGetAllCaseListRequestDto query);
    }
}