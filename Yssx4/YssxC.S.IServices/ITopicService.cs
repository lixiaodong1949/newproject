﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 题目业务服务接口
    /// </summary>
    public interface ITopicService
    {
        /// <summary>
        /// 添加 更新 题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveSimpleQuestion(SimpleQuestionRequest model, UserTicket CurrentUser);

        /// <summary>
        /// 添加 更新 复杂题目（分录题、表格题、案例题、财务报表题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveComplexQuestion(ComplexQuestionRequest model, UserTicket CurrentUser);
        /// <summary>
        ///  财务报表题 新增、修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveFinancialStatements(FinancialStatementsRequest model, UserTicket currentUser);

        /// <summary>
        /// 添加 更新 综合题
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveMainSubQuestion(MainSubQuestionRequest model, UserTicket CurrentUser);

        /// <summary>
        /// 根据id获取题目详情
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<TopicInfoDto>> GetTopicInfoById(long id);
        /// <summary>
        /// 根据申报表id获取申报表题目详情
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<TopicInfoDto>> GetTopicInfoByDeclarationId(long declarationId);

        /// <summary>
        /// 综合题分录题 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddCertificateMainSubTopic(CertificateMainSubTopic model, UserTicket currentUser);
        /// <summary>
        /// 综合题分录题 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateCertificateMainSubTopic(CertificateMainSubTopic model, UserTicket currentUser);
        /// <summary>
        /// 获取根据题目Id获取单个综合题分录题
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<CertificateMainSubTopic>> GetCertificateMainSubTopicInfoById(long id);

        /// <summary>
        /// 新增结转损益题
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddProfitLossTopic(ProfitLossTopicDto model, UserTicket currentUser);
        /// <summary>
        /// 修改结转损益题
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateProfitLossTopic(ProfitLossTopicDto model, UserTicket currentUser);
        /// <summary>
        /// 获取结转损益题信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<ProfitLossTopicDto>> GetProfitLossTopicInofById(long id);
        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<TopicListPageDto>> GetTopicListPage(TopicQueryPageDto queryDto);
        /// <summary>
        /// 题目删除 根据Id删除题目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTopicById(long id, UserTicket currentUser);
        /// <summary>
        /// 题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<List<TopicListDataDto>>> GetTopicList(TopicQueryDto queryDto);
        /// <summary>
        /// 获取所有题目的名称（包含子题目）
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<List<TopicNamesResponseDto>>> GetAllTopicNames(TopicNamesDto queryDto);
    }
}
