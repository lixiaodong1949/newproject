using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YssxC.S.Dto;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 游戏场景接口服务类
    /// </summary>
    public interface IGameSceneService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> Save(GameSceneDto model, long currentUserId);

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<GameSceneDto>> Get(long id);

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> Delete(long currentUserId, params long[] id);

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<GameScenePageResponseDto>> ListPage(GameScenePageRequestDto query);   

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<GameSceneListResponseDto>> List(GameSceneListRequestDto query);

        /// <summary>
        /// 获取3D游戏场景列表(不分页)
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<GameScenesResponseDto>> GetGameSceneList();
        /// <summary>
        /// 根据Id获取3D游戏场景信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<GameSceneBaseDto>> GetGameScene(long id);
    
        /// <summary>
        /// 添加场景会话信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddGameSceneDialogues(GameSceneDialoguesDto query);
    }
}