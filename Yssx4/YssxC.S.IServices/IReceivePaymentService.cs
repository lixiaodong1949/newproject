﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 收付款服务接口
    /// </summary>
    public interface IReceivePaymentService
    {
        /// <summary>
        /// 获取用户收付款票据列表(未做)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<UserReceivingBillViewModel>>> GetUserReceivingBillTopicList(UserCalendarReceivingQuery query, UserTicket user);

        /// <summary>
        /// 获取付款题列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<PaymentTopicInfoViewModel>>> GetPaymentTopicInfoList(ReceivePaymentTopicInfoQuery query, UserTicket user);

        /// <summary>
        /// 获取收款题列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<ReceiveTopicInfoViewModel>>> GetReceiveTopicInfoList(ReceivePaymentTopicInfoQuery query, UserTicket user);

        /// <summary>
        /// 付款操作
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<UserPaymentTopicViewModel>> PaymentOperate(UserPaymentTopicDto dto, UserTicket user);

        /// <summary>
        /// 收款操作
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<UserReceiveTopicViewModel>> ReceiveOperate(UserReceiveTopicDto dto, UserTicket user);

        /// <summary>
        /// 查询用户收付款列表(已做)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<UserReceivingBillViewModel>>> QueryUserHasDoneReceivingList(UserHasDoneReceivingQuery query, UserTicket user);
    }
}
