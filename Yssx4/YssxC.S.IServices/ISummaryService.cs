﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    public interface ISummaryService
    {
        /// <summary>
        /// 新增摘要
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
      //  Task<ResponseContext<SummaryDto>> AddSummary(SummaryDto model);

        /// <summary>
        /// 获取摘要列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SummaryDto>>> GetSummaryList(long id);

        /// <summary>
        /// 删除摘要
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
       // Task<ResponseContext<bool>> RemoveSummary(long id);
    }
}
