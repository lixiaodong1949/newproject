using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using YssxC.S.Dto;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 案例财务数据接口服务类
    /// </summary>
    public interface ICaseFinancialDataService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> SaveCaseFinancialData(CaseFinancialDataDto model, long currentUserId);

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> DeleteCaseFinancialData(long currentUserId, params long[] id);

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<CaseFinancialDataListResponseDto>> GetCaseFinancialDataList(long caseId);
    }
}