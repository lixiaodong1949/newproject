﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    public interface IDeclarationTemplateParentService
    {
        /// <summary>
        /// 获取所有申报表主表模板
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<List<DeclarationTemplateParentDto>>> GetAll();


    }
}
