using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 入职
    /// </summary>
    public interface IOnboardingService
    {
        #region 选择行业
        /// <summary>
        /// 获取所有行业 - 地图数据
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<GetIndustryInMapDto>>> GetIndustryInMap(int userType);

        /// <summary>
        /// 获取所有行业 - 下拉框数据
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<GetIndustryAllDataDto>>> GetIndustryAllData(int userType);

        /// <summary>
        /// 获取指定行业信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<IndustryDto>> GetIndustryById(long id, int userType);

        /// <summary>
        /// 根据行业id获取行业基本信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<IndustryBaseInfoDto>> GetIndustryBaseInfo(long id, int userType);
        #endregion

        #region 选择企业
        /// <summary>
        /// 获取行业下所有企业 - 地图数据
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<GetCaseByIndustryDto>>> GetCaseByIndustry(long id, int userType);

        /// <summary>
        /// 获取指定企业详细数据
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<GetCaseInfoOnboardingDto>> GetCaseInfoOnboarding(long id, int userType);

        #endregion

        #region 月度任务
        /// <summary>
        /// 根据企业查询月度任务
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<GetMonthTaskListDto>>> GetMonthTaskList(long id, long currentUserId, int userType, int platform = 0);

        /// <summary>
        /// 查询指定月度任务 - PC端
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<GetMonthTaskListDto>> GetMonthTaskById(long id, long currentUserId, int userType);

        /// <summary>
        /// 查询指定月度任务 - APP商城
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<GetMonthTaskListDto>> GetMonthTaskByIdApp(long id, long currentUserId);

        #endregion

        #region 部门介绍
        /// <summary>
        /// 根据企业查询部门介绍
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<GetCaseRegulationListDto>>> GetCaseRegulationList(long id);
        #endregion

        #region 生成试卷
        /// <summary>
        /// 生成试卷
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> GenerateTestPaper(long id, long currentUserId);

        /// <summary>
        /// 购买后自动生成试卷 - 云实习
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AutoGenerateTestPaper(AutoGenerateTestPaperDto model, long currentUserId = 0);

        #endregion

        #region 实习列表
        /// <summary>
        /// 实习列表 - 顶岗实习
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<GetCloudInternshipListDto>> GetCloudInternshipList(GetCloudInternshipListRequest model, long currentUserId, int userType);

        /// <summary>
        /// 查询通关记录
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<GetCloudInternshipRecordDto>> GetCloudInternshipRecord(GetCloudInternshipRecordRequest model, long currentUserId);
        #endregion


    }
}