﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 凭证题接口
    /// </summary>
    public interface ICertificateTopicService
    {
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model">请求数据</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveCertificateTopic(CertificateTopicRequest model);
        /// <summary>
        /// 根据Id获取凭证题信息
        /// </summary>
        /// <param name="id">凭证题Id</param>
        /// <returns></returns>
        Task<ResponseContext<CertificateTopicRequest>> GetCoreCertificateTopic(long id);
        /// <summary>
        /// 根据Id删除凭证题
        /// </summary>
        /// <param name="id">凭证题Id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCertificateTopicById(long id);
    }
}
