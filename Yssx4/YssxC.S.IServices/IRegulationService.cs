﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    /// <summary>
    /// 规章制度模板业务服务接口
    /// </summary>
    public interface IRegulationService
    {
        /// <summary>
        /// 删除规章制度信息
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteById(long id, UserTicket currentUser);
        /// <summary>
        /// 根据Id获取单条规章制度信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<RegulationDto>> GetInfoById(long id);
        /// <summary>
        /// 保存规章制度
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Save(RegulationDto model, UserTicket currentUser);
        /// <summary>
        /// 根据ParentId获取子类规章制度
        /// </summary>
        /// <param name="parentId">父类Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<RegulationListDto>>> GetListByParentId(long parentId);

        /// <summary>
        /// 获取父级规章制度列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<RegulationListDto>>> GetList(RegulationQueryDto query);

        /// <summary>
        /// 复制规章制度
        /// </summary>
        /// <param name="id">规章制度唯一id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Copy(long id, UserTicket currentUser);
        /// <summary>
        /// 根据规章制度id获取应用改规章制度的案例列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<GetCaseListByRegulationIdResponse>> GetCaseListPageByRegulationId(GetCaseListByRegulationIdDto query);
        /// <summary>
        /// 保存规章制度 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveCaseUseRegulation(CaseUseRegulationDto model, UserTicket currentUser);
        /// <summary>
        /// 获取规章制度模板名称列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<RegulationNamesListDto>>> GetNamesList(RegulationNamesListQueryDto query);
    }
}
