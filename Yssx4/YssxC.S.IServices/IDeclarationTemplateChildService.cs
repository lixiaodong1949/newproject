﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    public interface IDeclarationTemplateChildService
    {
        /// <summary>
        /// 根据条件查询申报表子表模板
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<List<DeclarationTemplateChildDto>>> GetList(GetDeclarationTemplateInput input);
    }
}
