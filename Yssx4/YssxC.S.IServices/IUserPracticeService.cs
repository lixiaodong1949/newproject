﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.Dto.ExamPaper;

namespace YssxC.S.IServices
{
    public interface IUserPracticeService
    {
        /// <summary>
        /// 填写分录
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<AccountingEntryTopicDto>>> GetAccountingEntryTopicList(AccountingEntryTopicRequest model);
        /// <summary>
        /// 查询分录
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="gradeId"></param>
        /// <param name="positionId"></param>
        /// <returns></returns>
        Task<PageResponse<AccountingEntryAnsweredDto>> GetAccountingEntryAnsweredList(AccountingEntryTopicRequest model, int auditType = -1);
        /// <summary>
        /// 出纳(会计主管)审核 auditType（0代表出纳  1代表会计主管）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<AccountingEntryTopicDto>>> GetAuditAccountingEntryTopicList(AccountingEntryTopicRequest model, int auditType);
        /// <summary>
        /// 获取月度任务题目集合(questionType：7--财务分析题   10--财务报表 17--纳税申报  18--财务报表申报题)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="taskId"></param>
        /// <param name="questionType"></param>
        /// <returns></returns>
        Task<ResponseContext<List<ExamOtherQuestionInfo>>> GetOtherTopicList(AccountingEntryTopicRequest model, long taskId, long declarationId, QuestionType questionType);
        /// <summary>
        /// 结转损溢
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="caseId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamProfitLossTopicDto>> GetProfitLossTopicInfo(long gradeId, long caseId, long taskId);
        /// <summary>
        /// 预览试卷作答信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperBasicInfo>> GetExamPaperBasicInfoView(long gradeId, UserTicket userTicket);
        /// <summary>
        /// 预览题目作答信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfoView(long gradeId, long questionId, long userId);
        /// <summary>
        /// 获取题目信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfo(long gradeId, long questionId, long userId);
        /// <summary>
        /// 提交答案--接口方法
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <returns>作答成功信息</returns>
        Task<ResponseContext<QuestionResult>> SubmitAnswer(QuestionAnswer model, long userId);
        /// <summary>
        /// 提交考卷---接口方法 
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        Task<ResponseContext<GradeInfo>> SubmitExam(long gradeId, long userId, bool isAutoSubmit = false);
        /// <summary>
        /// 新增拒绝记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddRefusalRecord(RefusalRecordDto model);

        /// <summary>
        /// 申报题目记录草稿
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeclareSaveDrafts(UserDeclareDto dto, UserTicket user);

        /// <summary>
        /// 用户申报
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UserDeclare(UserDeclareDto dto, UserTicket user);

        /// <summary>
        /// 获取其他实习工作列表
        /// </summary>
        /// <param name="currtTaskId">用户Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<GetOtherTasksResponseDto>>> GetOtherTasks(long currtTaskId);

    }
}
