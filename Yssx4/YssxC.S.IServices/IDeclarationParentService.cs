﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using YssxC.S.Dto;

namespace YssxC.S.IServices
{
    public interface IDeclarationParentService
    {
        /// <summary>
        /// 保存申报表主表
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Save(CreateDeclarationParentInput[] input,UserTicket currentUser);

        /// <summary>
        /// 删除申报表主表
        /// </summary>
        /// <param name="id">申报表主表id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Delete(long id, UserTicket currentUser);

        /// <summary>
        /// 根据id获取申报表主表
        /// </summary>
        /// <param name="id">申报表主表id</param>
        /// <returns></returns>
        Task<ResponseContext<DeclarationParentDto>> GetById(long id);

        /// <summary>
        /// 根据条件获取申报表主表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<DeclarationParentDto>>> GetList(GetDeclarationParentInput input);

        /// <summary>
        /// 修改主表信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Update(UpdateDeclarationParentInput input,UserTicket currentUser);
    }
}
