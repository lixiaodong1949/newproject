﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Sx.Course.QueryServices.FreeSqldb.Dto
{
    public class CourseSearchDto : PageRequest
    {
        /// <summary>
        /// 关键字搜索
        /// </summary>
        public string Keyword { get; set; }

        /// <summary>
        /// 0:查自己的，1：查所有的
        /// </summary>
        public int UseType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 学历
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 课程类型0:常规课
        /// </summary>
        public int CourseType { get; set; }
    }
}
