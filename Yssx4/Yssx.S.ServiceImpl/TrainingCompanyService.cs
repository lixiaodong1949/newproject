﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Linq;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.Dto.TrainingCompany;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.Pocos.BackManage;

namespace Yssx.S.ServiceImpl
{
    public class TrainingCompanyService : ITrainingCompanyService
    {
        /// <summary>
        /// 添加修改公司
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateSchool(TrainingCompanyDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            if (model.Id.HasValue)
            {
                var oldSchool = DbContext.FreeSql.Select<YssxTrainingCompany>().Where(m => m.Id == model.Id).First();
                if (oldSchool == null)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                }
                return await Task.Run(() =>
                {
                    return UpdateSchool(model, Id);
                });
            }

            if (DbContext.FreeSql.Select<YssxTrainingCompany>().Any(m => m.Name == model.Name))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "公司名称重复" };
            }
            var school = model.MapTo<YssxTrainingCompany>();
            school.Id = IdWorker.NextId();
            school.CreateBy = opreationId;
            school.CreateTime = DateTime.Now;
            return await Task.Run(() =>
            {
                DbContext.FreeSql.Insert(school).ExecuteAffrows();
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 修改公司信息
        /// </summary>
        /// <returns></returns>
        private ResponseContext<bool> UpdateSchool(TrainingCompanyDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            if (DbContext.FreeSql.Select<YssxTrainingCompany>().Any(m => (m.Name == model.Name) && m.Id != model.Id))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "公司名称重复" };
            }
            var school = model.MapTo<YssxTrainingCompany>();
            school.UpdateBy = opreationId;
            school.UpdateTime = DateTime.Now;
            DbContext.FreeSql.Update<YssxTrainingCompany>().SetSource(school).IgnoreColumns(m => new { m.IsDelete, m.CreateBy, m.CreateTime }).ExecuteAffrows();
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 删除公司信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteSchool(TrainingCompanyDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            if (!model.Id.HasValue)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };
            }
            var school = new YssxTrainingCompany
            {
                Id = model.Id.Value,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = opreationId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                DbContext.FreeSql.Update<YssxTrainingCompany>().SetSource(school).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 获取公司列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<TrainingCompanyDto>>> GetSchoolList(TrainingCompanyPageRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxTrainingCompany>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(m => m.CreateTime);
                if (model.Id.HasValue)
                    select = select.Where(m => m.Id == model.Id);
                if (!String.IsNullOrEmpty(model.Name))
                    select = select.Where(m => m.Name.Contains(model.Name));
                if (model.CompanySize!=0)
                {
                    select = select.Where(m => m.CompanySize==model.CompanySize);
                }
                if (model.Industry!=0)
                {
                    select = select.Where(m => m.Industry == model.Industry);
                }
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql();
                var items = DbContext.FreeSql.Ado.Query<TrainingCompanyDto>(sql);
                var res = new PageResponse<TrainingCompanyDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
                return new ResponseContext<PageResponse<TrainingCompanyDto>> { Code = CommonConstants.SuccessCode, Data = res };
            });
        }
    }
}
