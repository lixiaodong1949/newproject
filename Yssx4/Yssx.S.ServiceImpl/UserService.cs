﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Tas.Common.Utils;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using Yssx.Framework.Utils;
using Yssx.M.IServices.Extensions;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Topics;

namespace Yssx.S.ServiceImpl
{
    public class UserService : IUserService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> FindUserByName(string mb)
        {
            YssxUser user = await DbContext.FreeSql.Select<YssxUser>().Where(x => x.MobilePhone == mb).FirstAsync();

            if (user == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有找到" };
            }

            return new ResponseContext<bool> { Data = true };
        }

        /// <summary>
        /// 修改个人信息
        /// </summary>
        /// <param name="can">参数</param>
        /// <param name="type">类型（1.昵称 2.头像 3.真实姓名 4.工学号）</param>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ModifyUserInfo(string can, int type, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                var state = false;
                var user = DbContext.FreeSql.Select<YssxUser>().Where(u => u.Id == currentUserId && u.IsDelete == CommonConstants.IsNotDelete && u.Status == (int)Status.Enable).First();
                if (user == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有找到用户信息!" };
                switch (type)
                {
                    case 1:
                        state = DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.Set(x => new YssxUser { UpdateTime = dtNow, NikeName = System.Web.HttpUtility.UrlEncode(can, Encoding.UTF8) }).Where(x => x.Id == currentUserId).ExecuteAffrows() > 0;
                        if (state)
                        {
                            user.NikeName = System.Web.HttpUtility.UrlEncode(can, Encoding.UTF8);
                            SetUserCache(user);
                        }
                        break;
                    case 2:
                        state = DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.Set(x => new YssxUser { UpdateTime = dtNow, Photo = can }).Where(x => x.Id == currentUserId).ExecuteAffrows() > 0;
                        if (state)
                        {
                            user.Photo = can;
                            SetUserCache(user);
                        }
                        break;
                    case 3:
                        state = DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.Set(x => x.RealName, can).Where(x => x.Id == currentUserId).ExecuteAffrows() > 0;
                        if (user.UserType == (int)UserTypeEnums.Student)
                        {
                            state = DbContext.FreeSql.GetRepository<YssxStudent>().UpdateDiy.Set(x => x.Name, can).Where(x => x.UserId == currentUserId).ExecuteAffrows() > 0;
                        }
                        else if (user.UserType == (int)UserTypeEnums.Teacher)
                        {
                            state = DbContext.FreeSql.GetRepository<YssxTeacher>().UpdateDiy.Set(x => x.Name, can).Where(x => x.UserId == currentUserId).ExecuteAffrows() > 0;
                        }
                        if (state)
                        {
                            user.RealName = can;
                            SetUserCache(user);
                        }
                        break;
                    case 4:
                        if (user.UserType == (int)UserTypeEnums.Student)
                        {
                            var rStudent = DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                            if (rStudent == null)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有找到学生数据" };

                            rStudent.StudentNo = can;
                            state = DbContext.FreeSql.Update<YssxStudent>().SetSource(rStudent).UpdateColumns(x => new { x.StudentNo, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
                        }
                        if (user.UserType == (int)UserTypeEnums.Teacher)
                        {
                            var rTeacher = DbContext.FreeSql.Select<YssxTeacher>().Where(x => x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                            if (rTeacher == null)
                                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有找到教师数据" };

                            rTeacher.TeacherNo = can;
                            state = DbContext.FreeSql.Update<YssxTeacher>().SetSource(rTeacher).UpdateColumns(x => new { x.TeacherNo, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
                        }
                        break;
                    default:
                        break;
                }

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "操作成功!" : "操作失败!", Data = state };
            });
        }
        /// <summary>
        /// 老师修改学生姓名学号信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ModifyStudentInfoByUserId(StudentInfoRequest model,long currentUserId)
        {
            var dateNow = DateTime.Now;
            var student =await DbContext.FreeSql.GetRepository<YssxStudent>().Where(s => s.UserId == model.UserId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (student == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有找到学生数据" };
            var user= await DbContext.FreeSql.GetRepository<YssxUser>().Where(s => s.Id == model.UserId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if(user==null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "该学生对应用户信息不存在" };
            user.RealName = !string.IsNullOrEmpty(model.RealName) ? model.RealName : user.RealName;
            user.UpdateTime = dateNow;
            user.UpdateBy = currentUserId;
            student.Name = !string.IsNullOrEmpty(model.RealName) ? model.RealName : student.Name;
            student.StudentNo = !string.IsNullOrEmpty(model.StudentNo) ? model.StudentNo : student.StudentNo;
            user.UpdateTime = dateNow;
            user.UpdateBy = currentUserId;
            try
            {
                DbContext.FreeSql.Transaction(() =>
                {
                    if(!string.IsNullOrEmpty(model.RealName))
                        DbContext.FreeSql.Update<YssxUser>().SetSource(user).UpdateColumns(s => new { s.RealName, s.UpdateTime, s.UpdateBy }).ExecuteAffrows();

                    DbContext.FreeSql.Update<YssxStudent>().SetSource(student).UpdateColumns(s => new { s.Name, s.StudentNo, s.UpdateTime, s.UpdateBy }).ExecuteAffrows();
                });
            }
            catch (Exception)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作异常，请联系管理员" };
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "修改成功" };
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<YssxUser>> UserInformation(long userId)
        {
            var user = await DbContext.FreeSql.Select<YssxUser>().Where(u => u.Id == userId && u.IsDelete == CommonConstants.IsNotDelete && u.Status == (int)Status.Enable).FirstAsync();
            if (user == null)
                return new ResponseContext<YssxUser> { Code = CommonConstants.ErrorCode, Data = null, Msg = "没有找到用户信息!" };
            var school = DbContext.FreeSql.Select<YssxSchool>().Where(u => u.Id == user.TenantId).First();
            user.Email = school?.Name;
            user.NikeName = System.Web.HttpUtility.UrlDecode(user.NikeName, Encoding.UTF8);
            user.Password = null;
            return new ResponseContext<YssxUser> { Code = CommonConstants.SuccessCode, Data = user, Msg = "获取成功!" };
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="adt"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpdateUserPassword(UpdateUserPasswordDto model, long userId)
        {
            var state = false;
            if (model.NewPassword != model.AgainNewPassword)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "两次密码不一致!" };
            if (model.OriginalPassword == model.NewPassword)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "新旧密码不能一样!" };
            var user = await DbContext.FreeSql.Select<YssxUser>().Where(u => u.Id == userId && u.IsDelete == CommonConstants.IsNotDelete && u.Status == (int)Status.Enable).FirstAsync();
            if (user == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有找到用户信息!" };
            if (user.Password != model.OriginalPassword.Md5())
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "原密码错误!" };
            state = await DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.Set(x => new YssxUser { UpdateTime = DateTime.Now, Password = model.NewPassword.Md5() }).Where(x => x.Id == userId).ExecuteAffrowsAsync() > 0;
            if (state)
            {
                user.Password = model.NewPassword.Md5();
                SetUserCache(user);
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "操作成功!" : "操作失败!", Data = state };
        }

        /// <summary>
        /// 修改班级
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpdateClass(long userId, UpdateClassDto dto)
        {
            YssxClass y_class = await DbContext.FreeSql.GetRepository<YssxClass>().Where(x => x.Id == dto.NewId).FirstAsync();
            if (y_class == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级不存在！" };
            var ct = await DbContext.FreeSql.GetRepository<YssxStudent>().UpdateDiy.Set(x => x.ClassId, dto.NewId).Set(x => x.ProfessionId, y_class.ProfessionId).Set(x => x.CollegeId, y_class.CollegeId).Set(x => x.ClassName, y_class.Name).Set(x => x.Year, y_class.EntranceDateTime.Year).Where(x => x.UserId == userId).ExecuteAffrowsAsync();
            var state = ct > 0;
            return new ResponseContext<bool> { Data = state, Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "OK" : "换班级失败!" };
        }

        private void SetUserCache(YssxUser user)
        {
            //YssxUser user = RedisHelper.Get<YssxUser>($"{CommonConstants.Cache_GetUserByMobile}{moblie}");
            //if (user == null) return;
            var bytes = CompressHelper.BrotliCompress(user);
            if (bytes == null)
                return;
            RedisHelper.Set($"{CommonConstants.Cache_GetUserByMobile}{user.MobilePhone}", bytes, 30 * 60);
        }

        public async Task<ResponseContext<UserInfoDto>> GetUserInfo(UserTicket user)
        {
            UserInfoDto dto = new UserInfoDto();
            List<UserInfoDto> list = new List<UserInfoDto>();

            switch (user.Type)
            {
                case (int)UserTypeEnums.Student:
                    list = await DbContext.FreeSql.Select<YssxUser>().From<YssxStudent, YssxClass>((a, b, c) =>
                        a.LeftJoin(x => x.Id == b.UserId && b.IsDelete == CommonConstants.IsNotDelete)
                        .LeftJoin(x => b.ClassId == c.Id && c.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b, c) => a.Id == user.Id).ToListAsync((a, b, c) => new UserInfoDto
                        {
                            Id = a.Id,
                            TenantId = a.TenantId,
                            ClassName = c.Name,
                            CollegeName = b.CollegeName,
                            MobilePhone = a.MobilePhone,
                            NickName = a.NikeName,
                            Photo = a.Photo,
                            QQ = a.QQ,
                            ProfessionName = b.ProfessionName,
                            RealName = a.RealName,
                            SchoolName = b.SchoolName,
                            StudentNo = b.StudentNo,
                            ClassId = b.ClassId,
                            WeChat = a.Unionid,
                            TempExpiredDate = b.ExpiredDate
                        });
                    dto = list.Count > 0 ? list[0] : null;

                    if (dto.TempExpiredDate < DateTime.Now)
                        dto.RealExpiredDate = "";
                    else
                        dto.RealExpiredDate = dto.TempExpiredDate.ToString("yyyy-MM-dd HH:mm:ss");
                    break;
                case (int)UserTypeEnums.Teacher:
                case (int)UserTypeEnums.Educational:
                    list = await DbContext.FreeSql.Select<YssxUser>().From<YssxTeacher, YssxSchool>((a, b, c) =>
                        a.LeftJoin(x => x.Id == b.UserId && b.IsDelete == CommonConstants.IsNotDelete)
                        .LeftJoin(x => x.TenantId == c.Id && c.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b, c) => a.Id == user.Id).ToListAsync((a, b, c) => new UserInfoDto
                        {
                            Id = a.Id,
                            TenantId = a.TenantId,
                            MobilePhone = a.MobilePhone,
                            NickName = a.NikeName,
                            Photo = a.Photo,
                            QQ = a.QQ,
                            RealName = a.RealName,
                            SchoolName = c.Name,
                            WeChat = a.Unionid
                        });
                    dto = list.Count > 0 ? list[0] : null;
                    break;
                case (int)UserTypeEnums.OrdinaryUser:
                case (int)UserTypeEnums.Professional:
                    dto = await DbContext.FreeSql.Select<YssxUser>().Where(x => x.Id == user.Id).FirstAsync(a => new UserInfoDto
                    {
                        Id = a.Id,
                        TenantId = a.TenantId,
                        MobilePhone = a.MobilePhone,
                        NickName = a.NikeName,
                        Photo = a.Photo,
                        QQ = a.QQ,
                        RealName = a.RealName,
                        WeChat = a.Unionid
                    });
                    break;
                default: break;
            }
            dto.NickName = System.Web.HttpUtility.UrlDecode(dto.NickName, Encoding.UTF8);
            return new ResponseContext<UserInfoDto> { Data = dto };
        }

        public async Task<ResponseContext<bool>> IsVip(long uid)
        {
            var user = await DbContext.FreeSql.Select<YssxUser>().Where(s => s.Id == uid && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            //老师直接返回true
            if (user != null)
            { 
                if(user.UserType == 2)
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            }                
            
            YssxStudent student = await DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == uid && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (student == null) return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = false };

            bool state = student.ExpiredDate > DateTime.Now;

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state };

        }

        public async Task<ResponseContext<long>> GetUserIntegral(long userId)
        {
            YssxIntegral data = await DbContext.FreeSql.GetRepository<YssxIntegral>().Where(x => x.UserId == userId).FirstAsync();
            long number = data != null ? data.Number : 0;
            return new ResponseContext<long> { Data = number };
        }

        public async Task<ResponseContext<bool>> AddUserInfo(string name, string moblile)
        {
            bool isExist = await DbContext.FreeSql.GetRepository<YssxPartner>().Where(x => x.Mobile == moblile).AnyAsync();
            if (!isExist)
            {
                await DbContext.FreeSql.GetRepository<YssxPartner>().InsertAsync(new YssxPartner { Id = IdWorker.NextId(), Name = name, Mobile = moblile });
            }
            return new ResponseContext<bool> { Data = true };
        }

        public async Task<ResponseContext<List<PartnerDto>>> GetPartnerList()
        {
            List<PartnerDto> list = await DbContext.FreeSql.GetRepository<YssxPartner>().Where(x => x.Id > 0).ToListAsync<PartnerDto>();

            return new ResponseContext<List<PartnerDto>> { Data = list };
        }

        public async Task<ResponseContext<bool>> SetPartnerStatus(long id)
        {
            await DbContext.FreeSql.GetRepository<YssxPartner>().UpdateDiy.Set(x => x.Status, 1).Where(x => x.Id == id).ExecuteAffrowsAsync();
            return new ResponseContext<bool> { Data = true };
        }
    }
}
