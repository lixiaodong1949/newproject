﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class InformationService : IInformation
    {
        public async Task<ResponseContext<bool>> AddOrEditInfo(InfoDto dto)
        {
            bool state = false;
            YssxInformation data = new YssxInformation
            {
                Id = IdWorker.NextId(),
                Author = dto.Author,
                Content = dto.Content,
                CreateDate = DateTime.Now,
                MassgeType = dto.MassgeType,
                PublishDate = dto.PublishDate,
                TagJson = dto.TagJson,
                Title = dto.Title,
                TitleImg = dto.TitleImg
            };
            if (dto.Id == 0)
            {
                YssxInformation information = await DbContext.FreeSql.GetRepository<YssxInformation>().InsertAsync(data);

                state = information != null;
            }
            else
            {
                data.Id = dto.Id;
                state = await DbContext.FreeSql.GetRepository<YssxInformation>().UpdateDiy.SetSource(data).UpdateColumns(x => new { x.Author, x.Content, x.MassgeType, x.TagJson, x.Title, x.TitleImg }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Data = state, Msg = state ? "OK" : "失败了!", Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode };

        }

        public async Task<ResponseContext<List<InfoDto>>> GetInformationList()
        {
            List<YssxInformation> list = await DbContext.FreeSql.GetRepository<YssxInformation>().Where(x => x.IsDelete == 0).ToListAsync();
            List<InfoDto> dtos = list.Select(x => new InfoDto
            {
                Id = x.Id,
                Author = x.Author,
                Content = x.Content,
                MassgeType = x.MassgeType,
                PublishDate = x.PublishDate,
                TagJson = x.TagJson,
                Title = x.Title,
                TitleImg = x.TitleImg
            }).ToList();
            return new ResponseContext<List<InfoDto>> { Data = dtos };
        }

    }
}
