﻿using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework.Dal;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class ZhenShuOrderService : IZhenShuOrderService
    {
        public async Task<bool> AddCourseOrder()
        {
            List<YssxCourseJuniorOrder> list = await DbContext.FreeSql.GetRepository<YssxCourseJuniorOrder>().Where(x => x.UpdateBy == 0 && x.CourseName == "初级会计实务").ToListAsync();

            List<YssxCourseOrder> orders = new List<YssxCourseOrder>();
            foreach (var item in list)
            {
                YssxCourseOrder order = new YssxCourseOrder
                {
                    Id = IdWorker.NextId(),
                    CourseId = 1125652799064962,
                    CustomerId = item.UserId,

                };

                orders.Add(order);

                YssxCourseOrder order1 = new YssxCourseOrder
                {
                    Id = IdWorker.NextId(),
                    CourseId = 1125653069534718,
                    CustomerId = item.UserId,

                };

                orders.Add(order1);
            }

            await DbContext.FreeSql.GetRepository<YssxCourseOrder>().InsertAsync(orders);
            await DbContext.FreeSql.GetRepository<YssxCourseJuniorOrder>().UpdateDiy.SetSource(list).Set(x => x.UpdateBy, 1).ExecuteAffrowsAsync();

            return true;

        }

        public async Task<bool> AddCourseOrder(List<long> userIds)
        {
            List<YssxCourseJuniorOrder> orders = new List<YssxCourseJuniorOrder>();
            foreach (var item in userIds)
            {
                bool b = await DbContext.FreeSql.GetRepository<YssxCourseJuniorOrder>().Where(x => x.UserId == item).AnyAsync();
                if (!b)
                {
                    YssxCourseJuniorOrder order = new YssxCourseJuniorOrder();
                    order.UserId = item;
                    order.Id = IdWorker.NextId();
                    order.CourseId = 1125652799064962;
                    order.Status = 2;
                    order.Source = 1;
                    order.UpdateTime = DateTime.Now;
                    orders.Add(order);
                }
            }

            await DbContext.FreeSql.GetRepository<YssxCourseJuniorOrder>().InsertAsync(orders);

            return true;

        }
    }
}
