﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class CourceFilesService : ICourceFilesService
    {
        public async Task<ResponseContext<bool>> AddOrEditCourceFile(YssxCourceFilesDto dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxCourceFiles entity = new YssxCourceFiles
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                KnowledgePointId = dto.KnowledgePointId,
                File = dto.File,
                FileName = dto.FileName,
                FilesType = dto.FilesType,
                Sort = dto.Sort,
                KlgpGroupId = dto.KlgpGroupId,
                CreateByName = user.Name,
                TenantId = user.TenantId,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                YssxCourceFiles files = await DbContext.FreeSql.GetRepository<YssxCourceFiles>().InsertAsync(entity);
                state = files != null;
            }
            else
            {

                if (dto.UseType == 0)
                {
                    entity.AuditStatus = 0;
                }
                else
                {
                    entity.AuditStatus = 1;
                }
                state = DbContext.FreeSql.GetRepository<YssxCourceFiles>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.File, x.KnowledgePointId, x.AuditStatus, x.Sort, x.FileName, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<YssxCourceFilesDto>> GetCourceFileOne(long id)
        {
            ResponseContext<YssxCourceFilesDto> response = new ResponseContext<YssxCourceFilesDto>();
            YssxCourceFiles files = await DbContext.FreeSql.GetRepository<YssxCourceFiles>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            response.Data = new YssxCourceFilesDto
            {
                Id = files.Id,
                File = files.File,
                FileName = files.FileName,
                FilesType = files.FilesType,
                KnowledgePointId = files.KnowledgePointId,
                CreateByName = files.CreateByName,
                AuditStatus = files.AuditStatus,
                AuditStatusExplain = files.AuditStatusExplain,
                Uppershelf = files.Uppershelf,
                CreateTime = files.CreateTime.ToString("yyyy-MM-dd"),
                UpdateTime = files.UpdateTime.ToString("yyyy-MM-dd")
            };


            //var kpIds = list.SplitToArray<long>(',');
            //var kp = DbContext.FreeSql.Select<YssxKnowledgePoint>().Where(a => kpIds.Contains(a.Id) && a.IsDelete == CommonConstants.IsNotDelete).OrderBy(a => a.Sort).ToList();
            return response;
        }

        public async Task<ResponseContext<List<YssxCourceFilesDto>>> GetCourceFilesList(UserTicket user, int userType)
        {
            ResponseContext<List<YssxCourceFilesDto>> response = new ResponseContext<List<YssxCourceFilesDto>>();
            List<YssxCourceFiles> list = await DbContext.FreeSql.GetRepository<YssxCourceFiles>().Where(x => x.CreateBy == user.Id && x.AuditStatus == 1 && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            response.Data = list.Select(x => new YssxCourceFilesDto
            {
                Id = x.Id,
                File = x.File,
                FileName = x.FileName,
                FilesType = x.FilesType,
                KnowledgePointId = x.KnowledgePointId,
                AuditStatus = x.AuditStatus,
                CreateByName = x.CreateByName,
                AuditStatusExplain = x.AuditStatusExplain,
                Uppershelf = x.Uppershelf,
                CreateTime = x.CreateTime.ToString("yyyy-MM-dd"),
                UpdateTime = x.UpdateTime.ToString("yyyy-MM-dd")
            }).ToList();

            //var kpIds = list.SplitToArray<long>(',');
            //var kp = DbContext.FreeSql.Select<YssxKnowledgePoint>().Where(a => kpIds.Contains(a.Id) && a.IsDelete == CommonConstants.IsNotDelete).OrderBy(a => a.Sort).ToList();
            return response;
        }

        public async Task<PageResponse<YssxCourceFilesDto>> GetCourceFilesListByPage(UserTicket user, string keyword, int fileType, int status, int pageIndex, int pageSize)
        {
            var result = new PageResponse<YssxCourceFilesDto>();
            var select = DbContext.FreeSql.GetRepository<YssxCourceFiles>().Select.Where(s => s.CreateBy == user.Id && s.IsDelete == CommonConstants.IsNotDelete);
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                select.Where(x => x.FileName.Contains(keyword));
            }
            if (fileType > 0)
            {
                select.Where(x => x.FilesType == fileType);
            }

            switch (status)
            {
                case 0: select.Where(x => x.Uppershelf == status); break;
                case 1: select.Where(x => x.Uppershelf == 1 && x.AuditStatus == 0); break;
                case 2: select.Where(x => x.AuditStatus == 1); break;
                case 3: select.Where(x => x.AuditStatus == 2); break;
                default:
                    break;
            }

            var totalCount = await select.CountAsync();
            var sql = select.OrderByDescending(s => s.CreateTime)
              .Page(pageIndex, pageSize).ToSql("Id,FileName,File,FilesType,KnowledgePointId,AuditStatus,AuditStatusExplain,Uppershelf,UppershelfExplain,FileSize,ReadCount,CreateTime,UpdateTime,CreateByName");
            var items = await DbContext.FreeSql.Ado.QueryAsync<YssxCourceFilesDto>(sql);
            items.ForEach(x =>
            {
                if (x.AuditStatus == 0 && x.Uppershelf == 1)
                {
                    x.Status = 1;
                }
                else if (x.AuditStatus == 2)
                {
                    x.Status = 3;
                }
                else if (x.AuditStatus == 1)
                {
                    x.Status = 2;
                }
            });
            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = pageSize;
            result.PageIndex = pageIndex;
            result.RecordCount = totalCount;
            return result;
        }

        public async Task<ResponseContext<bool>> RemoveCourceFile(long uid, string ids)
        {
            string[] id_list = ids.Split(',');
            YssxCourceFiles entity = await DbContext.FreeSql.GetRepository<YssxCourceFiles>().Where(x => id_list.Contains(x.Id.ToString()) && x.CreateBy == uid).FirstAsync();
            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxCourceFiles>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<PageResponse<YssxCourceFilesDto>> SearchFilesListByPage(UserTicket user, string fileName, int fileType, long klgId, int pageIndex, int pageSize)
        {
            var result = new PageResponse<YssxCourceFilesDto>();
            var select = DbContext.FreeSql.GetRepository<YssxCourceFiles>().Select.Where(s => s.CreateBy == user.Id && s.IsDelete == CommonConstants.IsNotDelete);
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                select.Where(x => x.FileName.Contains(fileName));
            }
            if (fileType > 0)
            {
                select.Where(x => x.FilesType == fileType);
            }
            List<YssxKnowledgePoint> klgpList = new List<YssxKnowledgePoint>();

            if (klgId > 0)
            {
                YssxKnowledgePoint entity = await DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.Id == klgId).FirstAsync();
                List<YssxKnowledgePoint> sourceList = await DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.GroupId == klgId).ToListAsync();

                FindChildNode(klgId, klgpList, sourceList);
                if (klgpList.Count == 0)
                {
                    select.Where(x => x.KnowledgePointId.Contains(klgId.ToString()));
                }
                else
                {
                    select.Where(x => x.KlgpGroupId == entity.GroupId);
                }
            }

            var totalCount = await select.CountAsync();
            var sql = select.OrderByDescending(s => s.CreateTime)
              .Page(pageIndex, pageSize).ToSql("Id,FileName,File,FilesType,KnowledgePointId,AuditStatus,AuditStatusExplain,Uppershelf,UppershelfExplain,FileSize,ReadCount,CreateTime,UpdateTime,CreateByName");
            var items = await DbContext.FreeSql.Ado.QueryAsync<YssxCourceFilesDto>(sql);

            if (klgpList.Count > 0)
            {
                List<YssxCourceFilesDto> temps = new List<YssxCourceFilesDto>();
                foreach (var item in klgpList)
                {
                    var datas = items.Where(x => x.KnowledgePointId.Contains(item.Id.ToString()));
                    foreach (var item2 in datas)
                    {
                        if (temps.Any(s => s.Id == item2.Id)) continue;
                        temps.Add(item2);
                    }
                }

                items = temps;
            }

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = pageSize;
            result.PageIndex = pageIndex;
            result.RecordCount = totalCount;
            return result;
        }

        /// <summary>
        /// 查找父节点
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="retList"></param>
        private void FindParentNode(long parentId, List<YssxKnowledgePoint> retList)
        {
            if (parentId == 0) return;
            YssxKnowledgePoint yssxCoreSubjects = DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.Id == parentId).First();
            if (null == yssxCoreSubjects) return;
            retList.Add(yssxCoreSubjects);
            FindParentNode(yssxCoreSubjects.ParentId, retList);
        }

        /// <summary>
        /// 查找子节点
        /// </summary>
        private void FindChildNode(long id, List<YssxKnowledgePoint> retList, List<YssxKnowledgePoint> klgList)
        {
            if (id == 0) return;

            //List<YssxCoreSubject> yssxCoreSubjects = DbContext.FreeSql.Select<YssxCoreSubject>().Where(x => x.ParentId == id).ToList();
            List<YssxKnowledgePoint> yssxCoreSubjects = klgList.Where(x => x.ParentId == id).ToList();
            if (null == yssxCoreSubjects) return;
            foreach (var item in yssxCoreSubjects)
            {
                retList.Add(item);
                FindChildNode(item.Id, retList, klgList);
            }
        }

        #region 获取课件列表(购买/上传)
        /// <summary>
        /// 获取课件列表(购买/上传)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<YssxCourseFilesViewModel>> GetCourseFilesByBuyOrOneself(YssxCourseFilesQuery query, UserTicket user)
        {
            var result = new PageResponse<YssxCourseFilesViewModel>();

            if (query == null)
                return result;

            List<YssxKnowledgePoint> klgpList = new List<YssxKnowledgePoint>();

            if (query.KnowledgePointId.HasValue)
            {
                YssxKnowledgePoint kpModel = await DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.Id == query.KnowledgePointId.Value).FirstAsync();
                if (kpModel != null)
                {
                    List<YssxKnowledgePoint> sourceKPList = await DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.GroupId == kpModel.GroupId
                        && x.Status == Status.Enable && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                    if (sourceKPList != null && sourceKPList.Count > 0)
                    {
                        //递归算法获取知识点所有子级节点（包括它自己）
                        klgpList = GetKnowledgePointChilds(query.KnowledgePointId.Value, sourceKPList);
                    }
                }
            }

            List<YssxCourseFilesViewModel> list = new List<YssxCourseFilesViewModel>();

            long totalCount = 0;

            //平台(购买)
            if (query.CourseSource == 0)
            {
                var selectBuy = DbContext.FreeSql.Select<YssxCourseFilesOrder>().From<YssxCourceFiles>((a, b) => a.InnerJoin(x => x.CourseFilesId == b.Id))
                    .Where((a, b) => a.CustomerId == user.Id && (a.OrderStatus == (int)OrderStatus.Presented || a.OrderStatus == (int)OrderStatus.PaymentSuccess));

                if (!string.IsNullOrWhiteSpace(query.FileName))
                    selectBuy.Where((a, b) => b.FileName.Contains(query.FileName));
                if (query.FilesType.HasValue)
                    selectBuy.Where((a, b) => b.FilesType == query.FilesType.Value);

                if (klgpList.Count > 0)
                {
                    klgpList.ForEach(x =>
                    {
                        selectBuy.Where((a, b) => b.KnowledgePointId.Contains(x.Id.ToString()));
                    });
                }

                totalCount = await selectBuy.Distinct().CountAsync();

                var sqlBuy = selectBuy.OrderByDescending((a, b) => a.UpdateTime)
                  .Page(query.PageIndex, query.PageSize).ToSql("a.Id AS OrderId,a.CourseFilesId,b.FileName,b.File,b.KnowledgePointId,b.KlgpGroupId,b.CreateByName,b.FilesType," +
                  "b.FileSize,b.ReadCount,b.CreateTime AS FilesCreateTime,b.UpdateTime AS FilesUpdateTime,a.PaymentTime,a.OrderSource AS CourseFilesSource,a.EffectiveDate");
                list = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseFilesViewModel>(sqlBuy);
            }
            //私有(上传)
            if (query.CourseSource == 1)
            {
                FreeSql.ISelect<YssxCourceFiles> selectPrivate = DbContext.FreeSql.Select<YssxCourceFiles>().Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.CreateBy == user.Id);

                if (!string.IsNullOrWhiteSpace(query.FileName))
                    selectPrivate.Where(x => x.FileName.Contains(query.FileName));
                if (query.FilesType.HasValue)
                    selectPrivate.Where(x => x.FilesType == query.FilesType.Value);

                if (klgpList.Count > 0)
                {
                    klgpList.ForEach(x =>
                    {
                        selectPrivate.Where(a => a.KnowledgePointId.Contains(x.Id.ToString()));
                    });
                }

                totalCount = await selectPrivate.Distinct().CountAsync();

                var sql = selectPrivate.OrderByDescending(x => x.UpdateTime)
                  .Page(query.PageIndex, query.PageSize).ToSql("Id AS CourseFilesId,FileName,File,KnowledgePointId,KlgpGroupId,CreateByName,FilesType,FileSize,ReadCount," +
                  "CreateTime AS FilesCreateTime,UpdateTime AS FilesUpdateTime");
                list = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseFilesViewModel>(sql);
                list.ForEach(x =>
                {
                    x.CourseFilesSource = 2;
                });
            }
            //所有(购买/私有)
            if (!query.CourseSource.HasValue)
            {
                var selectAll = DbContext.FreeSql.Select<YssxCourceFiles>().From<YssxCourseFilesOrder>((a, b) => a.LeftJoin(x => x.Id == b.CourseFilesId)).Where((a, b) =>
                    (a.CreateBy == user.Id && a.IsDelete == CommonConstants.IsNotDelete) ||
                    (b.CustomerId == user.Id && (b.OrderStatus == (int)OrderStatus.Presented || b.OrderStatus == (int)OrderStatus.PaymentSuccess)));

                if (!string.IsNullOrWhiteSpace(query.FileName))
                    selectAll.Where((a, b) => a.FileName.Contains(query.FileName));
                if (query.FilesType.HasValue)
                    selectAll.Where((a, b) => a.FilesType == query.FilesType.Value);

                if (klgpList.Count > 0)
                {
                    klgpList.ForEach(x =>
                    {
                        selectAll.Where((a, b) => a.KnowledgePointId.Contains(x.Id.ToString()));
                    });
                }

                totalCount = await selectAll.Distinct().CountAsync();

                var sqlBuy = selectAll.OrderByDescending((a, b) => a.UpdateTime)
                  .Page(query.PageIndex, query.PageSize).ToSql("b.Id AS OrderId,a.Id AS CourseFilesId,a.FileName,a.File,a.KnowledgePointId,a.KlgpGroupId,a.CreateByName," +
                  "a.FilesType,a.FileSize,a.ReadCount,a.CreateTime AS FilesCreateTime,a.UpdateTime AS FilesUpdateTime,b.PaymentTime,b.OrderSource AS CourseFilesSource,b.EffectiveDate");
                list = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseFilesViewModel>(sqlBuy);
                list.ForEach(x =>
                {
                    if (!x.CourseFilesSource.HasValue)
                        x.CourseFilesSource = 2;
                });
            }

            result.Data = list;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 递归算法获取知识点所有子级节点（包括它自己）
        /// <summary>
        /// 递归算法获取知识点所有子级节点（包括它自己）
        /// </summary>
        /// <param name="knowledgePointId"></param>
        /// <param name="sourceList"></param>
        /// <returns></returns>
        private List<YssxKnowledgePoint> GetKnowledgePointChilds(long knowledgePointId, List<YssxKnowledgePoint> sourceList)
        {
            List<YssxKnowledgePoint> knowledgePointList = new List<YssxKnowledgePoint>();
            if (sourceList == null || sourceList.Count == 0) return knowledgePointList;

            var selfKnowledgePoint = sourceList.SingleOrDefault(a => a.Id == knowledgePointId);
            knowledgePointList.Add(selfKnowledgePoint);

            //查询知识点的子节点
            var items = sourceList.Where(a => a.ParentId == knowledgePointId).ToList();
            if (items != null && items.Count > 0)
            {
                foreach (var item in items)
                {
                    var childKnowledgePoints = GetKnowledgePointChilds(item.Id, sourceList);
                    knowledgePointList.AddRange(childKnowledgePoints);
                }
            }

            return knowledgePointList;
        }
        #endregion

        #region 新增资源文件
        /// <summary>
        /// 新增资源文件
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> AddOrEditResourceFile(YssxCourceFilesDto dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "对象是空的!" };

            YssxCourceFiles entity = new YssxCourceFiles
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                KnowledgePointId = dto.KnowledgePointId,
                File = dto.File,
                FileName = dto.FileName,
                FilesType = dto.FilesType,
                Sort = dto.Sort,
                KlgpGroupId = dto.KlgpGroupId,
                CategoryType = dto.CategoryType,
                CreateByName = user.Name,
                TenantId = user.TenantId,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                YssxCourceFiles files = await DbContext.FreeSql.GetRepository<YssxCourceFiles>().InsertAsync(entity);
                state = files != null;
            }
            else
            {

                if (dto.UseType == 0)
                {
                    entity.AuditStatus = 0;
                }
                else
                {
                    entity.AuditStatus = 1;
                }
                state = DbContext.FreeSql.GetRepository<YssxCourceFiles>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.File, x.KnowledgePointId, x.AuditStatus, x.Sort, x.FileName, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<long> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = entity.Id, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

    }
}
