﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.Entity;
using Yssx.Framework.AutoMapper;
using Microsoft.AspNetCore.Http;
using Yssx.Framework.Utils;
using System.Text.RegularExpressions;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 教师班级管理服务
    /// </summary>
    public class LessonPlanningClassService : ILessonPlanningClassService
    {
        #region 教师绑定班级
        /// <summary>
        /// 教师绑定班级
        /// </summary>
        /// <param name="requestList">班级列表</param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BindingTeacherClassRelation(List<TeacherClassRequestModel> requestList, UserTicket user)
        {
            if (requestList == null || requestList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "请选择绑定班级!" };

            //获取教师信息
            YssxTeacher teacherInfo = await DbContext.FreeSql.GetRepository<YssxTeacher>().Where(x => x.UserId == user.Id).FirstAsync();
            if (teacherInfo == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "该用户不是教师!" };

            bool state = true;

            List<YssxTeacherPlanningRelation> alreadyList = await DbContext.FreeSql.GetRepository<YssxTeacherPlanningRelation>().Where(x => x.UserId == user.Id
                && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            var alreadyClassIds = alreadyList.Select(x => x.ClassId).ToList();

            var requestClassIds = requestList.Select(x => x.ClassId).ToList();

            //需要删除的班级Ids
            var deleteClassIds = alreadyClassIds.Except(requestClassIds).ToList();
            //需要添加的班级Ids
            var addClassIds = requestClassIds.Except(alreadyClassIds).ToList();

            //需要新增数据集
            List<TeacherClassRequestModel> needAdds = requestList.Where(x => addClassIds.Contains(x.ClassId)).ToList();
            //需要删除数据集
            List<YssxTeacherPlanningRelation> deleteList = alreadyList.Where(x => deleteClassIds.Contains(x.ClassId)).ToList();

            //新增数据集
            List<YssxTeacherPlanningRelation> addList = needAdds.MapTo<List<YssxTeacherPlanningRelation>>();
            if (addList != null && addList.Count > 0)
            {
                addList.ForEach(x =>
                {
                    x.Id = IdWorker.NextId();
                    x.TenantId = user.TenantId;
                    x.UserId = user.Id;

                    x.CreateBy = user.Id;
                    x.CreateTime = DateTime.Now;
                    x.UpdateBy = user.Id;
                    x.UpdateTime = DateTime.Now;
                });

                var resultList = await DbContext.FreeSql.GetRepository<YssxTeacherPlanningRelation>().InsertAsync(addList);
                state = resultList != null;
            }
            if (deleteList.Count > 0)
            {
                //删除数据集
                deleteList.ForEach(x =>
                {
                    x.IsDelete = CommonConstants.IsDelete;
                    x.UpdateTime = DateTime.Now;
                    x.UpdateBy = user.Id;
                });
                state = DbContext.FreeSql.GetRepository<YssxTeacherPlanningRelation>().UpdateDiy.SetSource(deleteList).UpdateColumns(x =>
                       new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 教师绑定班级(App)
        /// <summary>
        /// 教师绑定班级(App)
        /// </summary>
        /// <param name="requestList">班级列表</param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BindingTeacherClassRelationForApp(List<TeacherClassRequestModel> requestList, UserTicket user)
        {
            if (requestList == null || requestList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "请选择绑定班级!" };

            List<long> classIds = requestList.Select(x => x.ClassId).ToList();

            List<YssxTeacherPlanningRelation> alreadyList = await DbContext.FreeSql.GetRepository<YssxTeacherPlanningRelation>().Where(x => x.UserId == user.Id
                && x.IsDelete == CommonConstants.IsNotDelete && classIds.Contains(x.ClassId)).ToListAsync();
            if (alreadyList.Count > 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "班级已经绑定!" };

            //获取教师信息
            YssxTeacher teacherInfo = await DbContext.FreeSql.GetRepository<YssxTeacher>().Where(x => x.UserId == user.Id).FirstAsync();
            if (teacherInfo == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "该用户不是教师!" };

            bool state = true;

            //新增数据集
            List<YssxTeacherPlanningRelation> addList = requestList.MapTo<List<YssxTeacherPlanningRelation>>();
            if (addList != null && addList.Count > 0)
            {
                addList.ForEach(x =>
                {
                    x.Id = IdWorker.NextId();
                    x.TenantId = user.TenantId;
                    x.UserId = user.Id;

                    x.CreateBy = user.Id;
                    x.CreateTime = DateTime.Now;
                    x.UpdateBy = user.Id;
                    x.UpdateTime = DateTime.Now;
                });

                var resultList = await DbContext.FreeSql.GetRepository<YssxTeacherPlanningRelation>().InsertAsync(addList);
                state = resultList != null;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 解绑单一教师班级
        /// <summary>
        /// 解绑单一教师班级
        /// </summary>
        /// <param name="teacherClassId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UnlinkTeacherClass(long teacherClassId, UserTicket user)
        {
            if (teacherClassId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = $"teacherClassId值：{teacherClassId}" };

            YssxTeacherPlanningRelation entity = await DbContext.FreeSql.GetRepository<YssxTeacherPlanningRelation>().Where(x => x.Id == teacherClassId).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<YssxTeacherPlanningRelation>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取教师班级列表
        /// <summary>
        /// 获取教师班级列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxTeacherClassRelationViewModel>>> GetTeacherClassRelationList(YssxTeacherClassRequestModel query, UserTicket user)
        {
            ResponseContext<List<YssxTeacherClassRelationViewModel>> response = new ResponseContext<List<YssxTeacherClassRelationViewModel>>();

            if (query == null) return response;

            if (!query.QuerySourceType.HasValue)
            {
                response.Msg = "数据源参数必传!";
                return response;
            }

            var list = new List<YssxTeacherClassRelationViewModel>();

            var select = DbContext.FreeSql.Select<YssxClass>();


            //院系、专业下班级
            if (query.QuerySourceType.Value == 0)
            {
                list = await select.From<YssxCollege, YssxProfession, YssxTeacherCollegeProfession>((a, b, c, d) =>
                    a.InnerJoin(x => x.CollegeId == b.Id)
                    .InnerJoin(x => x.ProfessionId == c.Id)
                    .InnerJoin(x => b.Id == d.CollegeId && c.Id == d.ProfessionId))
                    .Where((a, b, c, d) => d.UserId == user.Id && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete
                        && c.IsDelete == CommonConstants.IsNotDelete && d.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b, c, d) => new YssxTeacherClassRelationViewModel
                    {
                        SchoolId = user.TenantId,
                        SchoolName = user.SchoolName,
                        CollegeId = b.Id,
                        SchoolYear = a.EntranceDateTime.Year,
                        CollegeName = b.Name,
                        ProfessionId = c.Id,
                        ProfessionName = c.Name,
                        ClassId = a.Id,
                        ClassName = a.Name,
                    });
            }
            //学校下班级
            if (query.QuerySourceType.Value == 1)
            {
                list = await select.From<YssxSchool>((a, b) =>
                    a.InnerJoin(aa => aa.TenantId == b.Id))
                    .Where((a, b) => b.Id == user.TenantId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b) => new YssxTeacherClassRelationViewModel
                    {
                        SchoolId = b.Id,
                        SchoolName = b.Name,
                        CollegeId = a.CollegeId,
                        CollegeName = "",
                        ProfessionId = a.ProfessionId,
                        SchoolYear = a.EntranceDateTime.Year,
                        ProfessionName = "",
                        ClassId = a.Id,
                        ClassName = a.Name,
                    });
            }
            //已绑定班级
            if (query.QuerySourceType.Value == 2)
            {
                list = await select.From<YssxTeacherPlanningRelation>((a, b) =>
                 a.InnerJoin(x => x.Id == b.ClassId))
                .Where((a, b) => b.TenantId == user.TenantId && b.UserId == user.Id && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new YssxTeacherClassRelationViewModel
                {
                    TeacherClassId = b.Id,
                    SchoolId = user.TenantId,
                    SchoolName = user.SchoolName,
                    CollegeId = b.CollegeId,
                    CollegeName = "",
                    ProfessionId = b.ProfessionId,
                    SchoolYear = a.EntranceDateTime.Year,
                    ProfessionName = "",
                    ClassId = a.Id,
                    ClassName = a.Name,
                });
            }

            if (list.Count > 0)
            {
                //班级Id列表
                List<long> classIds = list.Select(a => a.ClassId).ToList();

                //统计班级学生人数                
                var classStudentCount = await DbContext.FreeSql.Select<YssxStudent>().From<YssxUser>(
                    (a, b) => a.InnerJoin(x => x.UserId == b.Id))
                    .Where((a, b) => classIds.Contains(a.ClassId) && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                    .GroupBy((a, b) => a.ClassId).ToListAsync(a => new { a.Key, Count = a.Count() });

                list.ForEach(x =>
                {
                    x.StudentCount = classStudentCount.FirstOrDefault(s => s.Key == x.ClassId)?.Count ?? 0;
                });
            }

            response.Data = list;

            return response;
        }
        #endregion

        #region 根据班级获取班级下学生信息
        /// <summary>
        /// 根据班级获取班级下学生信息
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxTeacherClassStudentViewModel>>> GetStudentListByClassId(YssxTeacherClassStudentRequestModel query)
        {
            ResponseContext<List<YssxTeacherClassStudentViewModel>> response = new ResponseContext<List<YssxTeacherClassStudentViewModel>>();

            if (query == null) return response;

            #region 2020-04-10前 带院系、专业查询
            /*
            var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxCollege, YssxProfession, YssxClass, YssxUser>(
                (a, b, c, d, e) =>
                a.InnerJoin(x => x.CollegeId == b.Id)
                .InnerJoin(x => x.ProfessionId == c.Id)
                .InnerJoin(x => x.ClassId == d.Id)
                .InnerJoin(x => x.UserId == e.Id)
                .InnerJoin(x => d.CollegeId == b.Id && d.ProfessionId == c.Id)
                ).Where((a, b, c, d, e) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete
                      && d.IsDelete == CommonConstants.IsNotDelete && e.IsDelete == CommonConstants.IsNotDelete && a.ClassId == query.ClassId);

            if (!string.IsNullOrWhiteSpace(query.Keyword))
                select.Where((a, b, c, d, e) => a.Name.Contains(query.Keyword) || a.StudentNo.Contains(query.Keyword));

            response.Data = await select.OrderByDescending((a, b, c, d, e) => a.UpdateTime ?? a.CreateTime).Distinct().ToListAsync((a, b, c, d, e) => new YssxTeacherClassStudentViewModel
            {
                StudentUserId = a.UserId,
                Name = a.Name,
                WorkNumber = a.StudentNo,
                MobilePhone = e.MobilePhone,
                InClassTime = a.UpdateTime ?? a.CreateTime,
                CollegeId = b.Id,
                CollegeName = b.Name,
                ProfessionId = c.Id,
                ProfessionName = c.Name,
                ClassId = a.ClassId,
                ClassName = d.Name
            });
            */
            #endregion

            var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxClass, YssxUser>(
                (a, b, c) =>
                a.InnerJoin(x => x.ClassId == b.Id)
                .InnerJoin(x => x.UserId == c.Id)
                ).Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete
                      && a.ClassId == query.ClassId);

            if (!string.IsNullOrWhiteSpace(query.Keyword))
                select.Where((a, b, c) => a.Name.Contains(query.Keyword) || a.StudentNo.Contains(query.Keyword) || c.MobilePhone.Contains(query.Keyword));

            response.Data = await select.OrderByDescending((a, b, c) => a.UpdateTime ?? a.CreateTime).Distinct().ToListAsync((a, b, c) => new YssxTeacherClassStudentViewModel
            {
                StudentUserId = a.UserId,
                Name = a.Name,
                WorkNumber = a.StudentNo,
                MobilePhone = c.MobilePhone,
                InClassTime = a.UpdateTime ?? a.CreateTime,
                CollegeId = a.CollegeId,
                CollegeName = a.CollegeName,
                ProfessionId = a.ProfessionId,
                ProfessionName = a.ProfessionName,
                ClassId = b.Id,
                ClassName = b.Name
            });

            return response;
        }
        #endregion

        #region 获取教师下所有班级学生
        /// <summary>
        /// 获取教师下所有班级学生
        /// </summary>
        /// <param name="year">入学年份</param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<AllClassStudentByTeacherViewModel>>> GetTeacherAllStudentList(int year, UserTicket user)
        {
            ResponseContext<List<AllClassStudentByTeacherViewModel>> response = new ResponseContext<List<AllClassStudentByTeacherViewModel>>();

            if (user == null)
                return response;

            #region 2020-04-10前 带院系、专业查询
            /*
            //不应该加User表
            var sourceList = await DbContext.FreeSql.Select<YssxStudent>().From<YssxCollege, YssxProfession, YssxClass, YssxTeacherPlanningRelation, YssxUser>(
                (a, b, c, d, e, f) =>
                a.InnerJoin(x => x.CollegeId == b.Id)
                .InnerJoin(x => x.ProfessionId == c.Id)
                .InnerJoin(x => x.ClassId == d.Id)
                .InnerJoin(x => x.ClassId == e.ClassId)
                .InnerJoin(x => d.CollegeId == b.Id && d.ProfessionId == c.Id && d.Id == e.ClassId)
                .InnerJoin(x => x.UserId == f.Id)
                ).Where((a, b, c, d, e, f) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete
                      && d.IsDelete == CommonConstants.IsNotDelete && e.IsDelete == CommonConstants.IsNotDelete && f.IsDelete == CommonConstants.IsNotDelete && e.UserId == user.Id)
                .ToListAsync((a, b, c, d, e, f) => new YssxCourseClassStudentViewModel
                {
                    StudentUserId = a.UserId,
                    Name = a.Name,
                    CollegeId = b.Id,
                    CollegeName = b.Name,
                    ProfessionId = c.Id,
                    ProfessionName = c.Name,
                    ClassId = d.Id,
                    ClassName = d.Name
                });

            if (sourceList == null || sourceList.Count == 0)
                return response;

            //班级列表
            var classList = sourceList.Select(x => new { x.CollegeId, x.CollegeName, x.ProfessionId, x.ProfessionName, x.ClassId, x.ClassName }).Distinct().ToList();

            List<AllClassStudentByTeacherViewModel> list = new List<AllClassStudentByTeacherViewModel>();

            classList.ForEach(x =>
            {
                //某一个班级学生列表
                var studentList = sourceList.Where(a => a.ClassId == x.ClassId).ToList();
                AllClassStudentByTeacherViewModel model = new AllClassStudentByTeacherViewModel();
                model.CollegeId = x.CollegeId;
                model.CollegeName = x.CollegeName;
                model.ProfessionId = x.ProfessionId;
                model.ProfessionName = x.ProfessionName;
                model.ClassId = x.ClassId;
                model.ClassName = x.ClassName;
                model.StudentList = studentList.MapTo<List<AllStudentData>>();

                list.Add(model);
            });
            */
            #endregion

            //不应该加User表
            var sourceList = await DbContext.FreeSql.Select<YssxStudent>().From<YssxClass, YssxTeacherPlanningRelation, YssxUser>(
                (a, b, c, d) =>
                a.InnerJoin(x => x.ClassId == b.Id)
                .InnerJoin(x => x.ClassId == c.ClassId)
                .InnerJoin(x => b.Id == c.ClassId)
                .InnerJoin(x => x.UserId == d.Id)
                ).Where((a, b, c, d) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete
                      && d.IsDelete == CommonConstants.IsNotDelete && c.UserId == user.Id).WhereIf(year > 0, (a, b, c, d) => b.EntranceDateTime.Year == year)
                .ToListAsync((a, b, c, d) => new YssxCourseClassStudentViewModel
                {
                    StudentUserId = a.UserId,
                    Name = a.Name,
                    ClassId = b.Id,
                    ClassName = b.Name,
                });

            if (sourceList == null || sourceList.Count == 0)
                return response;

            //班级列表
            var classList = sourceList.Select(x => new { x.ClassId, x.ClassName }).Distinct().ToList();

            List<AllClassStudentByTeacherViewModel> list = new List<AllClassStudentByTeacherViewModel>();

            classList.ForEach(x =>
            {
                //某一个班级学生列表
                var studentList = sourceList.Where(a => a.ClassId == x.ClassId).ToList();
                AllClassStudentByTeacherViewModel model = new AllClassStudentByTeacherViewModel();
                model.ClassId = x.ClassId;
                model.ClassName = x.ClassName;
                model.StudentList = studentList.MapTo<List<AllStudentData>>();

                list.Add(model);
            });

            response.Data = list;

            return response;
        }
        #endregion

        #region 根据班级Ids获取学生Id、Name
        /// <summary>
        /// 根据班级Ids获取学生Id、Name
        /// </summary>
        /// <param name="classIds"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<StudentInfoByClassIdsViewModel>>> GetStudentNameByClassIds(List<long> classIds)
        {
            ResponseContext<List<StudentInfoByClassIdsViewModel>> response = new ResponseContext<List<StudentInfoByClassIdsViewModel>>();

            if (classIds == null || classIds.Count == 0) return response;

            var select = DbContext.FreeSql.Select<YssxStudent>().Where(x => x.IsDelete == CommonConstants.IsNotDelete && classIds.Contains(x.ClassId));

            response.Data = await select.ToListAsync(x => new StudentInfoByClassIdsViewModel
            {
                StudentUserId = x.UserId,
                Name = x.Name,
                ClassId = x.ClassId,
            });

            return response;
        }
        #endregion

        #region 学生退班(通过教师操作)
        /// <summary>
        /// 学生退班(通过教师操作)
        /// </summary>
        /// <param name="studentUserId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> StudentQuitClassByTeacher(long studentUserId, UserTicket user)
        {
            //一个UserId应对应一个学生,目前数据库有重复数据 所以取List列表
            List<YssxStudent> list = await DbContext.FreeSql.GetRepository<YssxStudent>().Where(x => x.UserId == studentUserId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该学生信息!" };

            list.ForEach(x =>
            {
                x.ClassId = 0;
                x.ClassName = null;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });

            bool state = DbContext.FreeSql.GetRepository<YssxStudent>().UpdateDiy.SetSource(list).UpdateColumns(x => new
            {
                x.ClassId,
                x.ClassName,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取教师是否有课程、班级(是否可以发布任务)
        /// <summary>
        /// 获取教师是否有课程、班级(是否可以发布任务)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<TeacherIsCanPublishTask>> GetTeacherIsCanPublishTask(UserTicket user)
        {
            ResponseContext<TeacherIsCanPublishTask> response = new ResponseContext<TeacherIsCanPublishTask>();

            TeacherIsCanPublishTask data = new TeacherIsCanPublishTask();

            ////获取教师课程
            //List<YssxCourse> courseList = await DbContext.FreeSql.Select<YssxCourse>().Where(x => x.CreateBy == user.Id &&
            //    x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            //if (courseList.Count > 0)
            //    data.IsHasCourse = true;

            ////获取教师绑定班级
            //List<YssxClass> classList = await DbContext.FreeSql.Select<YssxClass>().From<YssxTeacherPlanningRelation>((a, b) =>
            //      a.InnerJoin(x => x.Id == b.ClassId))
            //    .Where((a, b) => b.UserId == user.Id && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
            //    .ToListAsync();
            //if (classList.Count > 0)
            //    data.IsHasClass = true;

            data.IsHasCourse = true;
            data.IsHasClass = true;

            response.Data = data;

            return response;
        }
        #endregion

        #region 编辑学生信息
        /// <summary>
        /// 编辑学生信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EditStudentByTeacher(EditStudentByTeacherRequestModel request, UserTicket user)
        {
            if (request == null || request.StudentUserId == 0 || string.IsNullOrEmpty(request.Name) || string.IsNullOrEmpty(request.StudentNo))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "参数不能为空!" };

            //获取用户信息
            YssxUser userInfo = await DbContext.FreeSql.GetRepository<YssxUser>().Where(x => x.Id == request.StudentUserId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (userInfo == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "用户不存在!" };
            if (userInfo.UserType != 1)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "用户不是学生!" };

            //获取学生信息
            YssxStudent studentInfo = await DbContext.FreeSql.GetRepository<YssxStudent>().Where(x => x.UserId == request.StudentUserId && x.IsDelete ==
                CommonConstants.IsNotDelete).FirstAsync();
            if (studentInfo == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "学生不存在!" };

            userInfo.RealName = request.Name;
            userInfo.UpdateTime = DateTime.Now;
            userInfo.UpdateBy = user.Id;

            studentInfo.Name = request.Name;
            studentInfo.StudentNo = request.StudentNo;
            studentInfo.UpdateTime = DateTime.Now;
            studentInfo.UpdateBy = user.Id;

            bool state = false;

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    state = uow.GetRepository<YssxUser>().UpdateDiy.SetSource(userInfo).UpdateColumns(x => new
                    {
                        x.RealName,
                        x.UpdateTime,
                        x.UpdateBy
                    }).ExecuteAffrows() > 0;

                    state = uow.GetRepository<YssxStudent>().UpdateDiy.SetSource(studentInfo).UpdateColumns(x => new
                    {
                        x.Name,
                        x.StudentNo,
                        x.UpdateTime,
                        x.UpdateBy
                    }).ExecuteAffrows() > 0;

                    if (state)
                        uow.Commit();
                    else
                        uow.Rollback();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 教师导入学生名单
        /// <summary>
        /// 教师导入学生名单
        /// </summary>
        /// <param name="file"></param>
        /// <param name="classId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ImportStudentResultModel>> ImportStudentByTeacher(IFormFile file, long classId, UserTicket user)
        {
            ResponseContext<ImportStudentResultModel> response = new ResponseContext<ImportStudentResultModel>();

            ImportStudentResultModel data = new ImportStudentResultModel();

            List<ImportStudentRequestModel> studentData = new List<ImportStudentRequestModel>();

            //错误列表
            List<ImportStudentViewModel> errorList = new List<ImportStudentViewModel>();

            #region 文件解析

            if (file == null || classId == 0)
                return new ResponseContext<ImportStudentResultModel> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空!" };

            var classModel = await DbContext.FreeSql.Select<YssxClass>().Where(x => x.Id == classId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (classModel == null)
                return new ResponseContext<ImportStudentResultModel> { Code = CommonConstants.ErrorCode, Msg = "班级不存在!" };
            if (classModel.TenantId != user.TenantId)
                return new ResponseContext<ImportStudentResultModel> { Code = CommonConstants.ErrorCode, Msg = "教师不在班级学校下!" };

            var school = await DbContext.FreeSql.Select<YssxSchool>().Where(x => x.Id == classModel.TenantId).FirstAsync();
            if (school == null)
                return new ResponseContext<ImportStudentResultModel> { Code = CommonConstants.ErrorCode, Msg = "学校不存在!" };
            if (school.IsDelete == CommonConstants.IsDelete)
                return new ResponseContext<ImportStudentResultModel> { Code = CommonConstants.ErrorCode, Msg = "学校已被删除!" };

            if (!file.FileName.Contains(".xls") && !file.FileName.Contains(".xlsx"))
                return new ResponseContext<ImportStudentResultModel> { Code = CommonConstants.ErrorCode, Msg = "模板不正确,请上传正确模板!" };

            using (var ms = file.OpenReadStream())
            {
                ResponseContext<List<ImportStudentRequestModel>> checkData = NPOIHelper<ImportStudentRequestModel>.CheckImportExcelData(ms, file.FileName);

                if (checkData.Code != CommonConstants.SuccessCode)
                {
                    response.Code = CommonConstants.ErrorCode;
                    response.Msg = checkData.Msg;
                    return response;
                }

                if (checkData.Data == null || checkData.Data.Count == 0)
                {
                    response.Code = CommonConstants.ErrorCode;
                    response.Msg = "不允许为空,请上传有效数据!";
                    return response;
                }

                studentData = checkData.Data;
            }

            #endregion

            #region 数据验证

            #region 数据导入前验证逻辑
            //1、未注册用户   【添加用户表;添加学生表】
            //2、注册未激活用户,学校是默认学校【中国测试大学】  1017666019115035  【修改用户表;添加学生表;删除用户缓存】
            //3、在本校没有绑定班级,身份是学生 【添加或修改学生表;删除用户缓存】
            //4、在本校,身份不是学生  不导入,提示非学生身份
            //5、在本校已绑定了其他班级   强制换班
            //6、已经在本校本班  不导入,提示已在本校本班
            //7、属于其他学校学生  不导入,提示不在本校、需重新激活
            #endregion

            //添加用户列表
            List<YssxUser> addUserList = new List<YssxUser>();
            //修改用户列表
            List<YssxUser> updateUserList = new List<YssxUser>();
            //添加学生列表
            List<YssxStudent> addStudentList = new List<YssxStudent>();
            //修改用户列表
            List<YssxStudent> updateStudentList = new List<YssxStudent>();
            //需要清缓存的用户列表
            List<YssxUser> deleteCache = new List<YssxUser>();

            List<ImportStudentRequestModel> alreadyExistData = new List<ImportStudentRequestModel>();

            foreach (var item in studentData)
            {
                #region 验证

                var repeatData = alreadyExistData.Where(x => x.MobilePhone == item.MobilePhone).ToList();
                if (repeatData != null && repeatData.Count > 0)
                {
                    errorList.Add(new ImportStudentViewModel
                    {
                        StudentNo = item.StudentNo,
                        Name = item.Name,
                        MobilePhone = item.MobilePhone,
                        Remark = "已存在文件中,不能上传重复数据!"
                    });

                    continue;
                }

                string remark = "";
                if (string.IsNullOrEmpty(item.StudentNo))
                    remark = remark + "学号不能为空,";
                if (string.IsNullOrEmpty(item.Name))
                    remark = remark + "姓名不能为空,";
                if (string.IsNullOrEmpty(item.MobilePhone))
                    remark = remark + "手机号不能为空,";
                if (!string.IsNullOrEmpty(item.MobilePhone))
                {
                    //验证手机号合理性
                    if (!Regex.IsMatch(item.MobilePhone, @"^1[3456789]\d{9}$"))
                        remark = remark + "手机号码无效,";
                }

                if (!string.IsNullOrEmpty(remark))
                {
                    remark = remark.Substring(0, remark.Length - 1);

                    errorList.Add(new ImportStudentViewModel
                    {
                        StudentNo = item.StudentNo,
                        Name = item.Name,
                        MobilePhone = item.MobilePhone,
                        Remark = remark
                    });

                    continue;
                }

                //添加到已存在的列表
                alreadyExistData.Add(item);

                //查询用户表
                YssxUser userData = await DbContext.FreeSql.GetRepository<YssxUser>().Where(x => x.MobilePhone == item.MobilePhone &&
                     x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

                #endregion

                #region 1、未注册用户
                //1、未注册用户
                if (userData == null)
                {
                    YssxUser userModel = new YssxUser
                    {
                        Id = IdWorker.NextId(),
                        UserName = item.Name,
                        Password = CommonConstants.NewUserDefaultPwd.Md5(),
                        UserType = (int)UserTypeEnums.Student,
                        MobilePhone = item.MobilePhone,
                        NikeName = item.Name,
                        RealName = item.Name,
                        Status = (int)Status.Enable,
                        TenantId = user.TenantId,
                        CreateBy = user.Id,
                        CreateTime = DateTime.Now,
                        UpdateBy = user.Id,
                        UpdateTime = DateTime.Now,
                        ClientId = Guid.NewGuid(),
                        RegistrationType = 3
                    };

                    addUserList.Add(userModel);

                    addStudentList.Add(new YssxStudent
                    {
                        Id = IdWorker.NextId(),
                        UserId = userModel.Id,
                        Name = userModel.RealName,
                        StudentNo = item.StudentNo,
                        TenantId = school.Id,
                        SchoolName = school.Name,
                        CollegeId = classModel.CollegeId,
                        ProfessionId = classModel.ProfessionId,
                        ClassId = classModel.Id,
                        ClassName = classModel.Name,
                        CreateBy = user.Id,
                        UpdateBy = user.Id,
                        CreateTime = DateTime.Now,
                        UpdateTime = DateTime.Now,
                    });

                    continue;
                }
                #endregion

                #region 2、注册未激活用户,学校是默认学校【中国测试大学】 1017666019115035
                //2、注册未激活用户,学校是默认学校【中国测试大学】 1017666019115035
                if (userData != null && userData.UserType == (int)UserTypeEnums.OrdinaryUser && userData.TenantId == CommonConstants.DefaultTenantId)
                {
                    userData.UserType = (int)UserTypeEnums.Student;
                    userData.UserName = item.Name;
                    userData.RealName = item.Name;
                    userData.TenantId = user.TenantId;
                    userData.UpdateBy = user.Id;
                    userData.UpdateTime = DateTime.Now;

                    updateUserList.Add(userData);

                    addStudentList.Add(new YssxStudent
                    {
                        Id = IdWorker.NextId(),
                        UserId = userData.Id,
                        Name = userData.RealName,
                        StudentNo = item.StudentNo,
                        TenantId = school.Id,
                        SchoolName = school.Name,
                        CollegeId = classModel.CollegeId,
                        ProfessionId = classModel.ProfessionId,
                        ClassId = classModel.Id,
                        ClassName = classModel.Name,
                        CreateBy = user.Id,
                        UpdateBy = user.Id,
                        CreateTime = DateTime.Now,
                        UpdateTime = DateTime.Now,
                    });

                    continue;
                }
                #endregion

                #region 3、在本校的情况
                //3.1、在本校没有绑定班级,身份是学生
                //3.2、在本校其他班级
                //3.3、在本校本班
                if (userData != null && userData.TenantId == user.TenantId && userData.UserType == (int)UserTypeEnums.Student)
                {
                    var sStudentData = await DbContext.FreeSql.GetRepository<YssxStudent>().Where(x => x.UserId == userData.Id &&
                       x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

                    if (sStudentData != null)
                    {
                        //1、在本校本班
                        if (sStudentData.ClassId == classModel.Id)
                        {
                            errorList.Add(new ImportStudentViewModel
                            {
                                StudentNo = item.StudentNo,
                                Name = item.Name,
                                MobilePhone = item.MobilePhone,
                                Remark = "已在本校本班,不能重复导入!"
                            });

                            continue;
                        }

                        //2、没有绑定班级  
                        //3、已绑定了其他班级
                        if (sStudentData.ClassId == 0 || sStudentData.ClassId != classModel.Id)
                        {
                            sStudentData.Name = userData.RealName;
                            sStudentData.StudentNo = item.StudentNo;
                            sStudentData.TenantId = school.Id;
                            sStudentData.SchoolName = school.Name;
                            sStudentData.CollegeId = classModel.CollegeId;
                            sStudentData.ProfessionId = classModel.ProfessionId;
                            sStudentData.ClassId = classModel.Id;
                            sStudentData.ClassName = classModel.Name;
                            sStudentData.UpdateBy = user.Id;
                            sStudentData.UpdateTime = DateTime.Now;

                            updateStudentList.Add(sStudentData);
                        }

                        userData.UserName = item.Name;
                        userData.RealName = item.Name;
                        userData.UpdateBy = user.Id;
                        userData.UpdateTime = DateTime.Now;

                        updateUserList.Add(userData);
                    }
                    else
                    {
                        addStudentList.Add(new YssxStudent
                        {
                            Id = IdWorker.NextId(),
                            UserId = userData.Id,
                            Name = userData.RealName,
                            StudentNo = item.StudentNo,
                            TenantId = school.Id,
                            SchoolName = school.Name,
                            CollegeId = classModel.CollegeId,
                            ProfessionId = classModel.ProfessionId,
                            ClassId = classModel.Id,
                            ClassName = classModel.Name,
                            CreateBy = user.Id,
                            UpdateBy = user.Id,
                            CreateTime = DateTime.Now,
                            UpdateTime = DateTime.Now,
                        });

                        userData.UserName = item.Name;
                        userData.RealName = item.Name;
                        userData.UpdateBy = user.Id;
                        userData.UpdateTime = DateTime.Now;

                        updateUserList.Add(userData);

                    }

                    deleteCache.Add(new YssxUser
                    {
                        MobilePhone = item.MobilePhone
                    });

                    continue;
                }
                #endregion

                #region 4、在本校,身份不是学生
                //4、在本校,身份不是学生
                if (userData != null && userData.TenantId == user.TenantId && userData.UserType != (int)UserTypeEnums.Student)
                {
                    errorList.Add(new ImportStudentViewModel
                    {
                        StudentNo = item.StudentNo,
                        Name = item.Name,
                        MobilePhone = item.MobilePhone,
                        Remark = "非学生身份,不能导入!"
                    });

                    continue;
                }
                #endregion

                #region 5、属于其他学校学生 不导入, 提示不在本校、需重新激活
                //5、属于其他学校学生 不导入, 提示不在本校、需重新激活
                if (userData != null && userData.TenantId != user.TenantId && userData.TenantId != CommonConstants.DefaultTenantId)
                {
                    errorList.Add(new ImportStudentViewModel
                    {
                        StudentNo = item.StudentNo,
                        Name = item.Name,
                        MobilePhone = item.MobilePhone,
                        Remark = "非本校学生,不能导入!"
                    });

                    continue;
                }
                #endregion
            }

            #endregion

            #region 添加数据
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //1、添加用户列表
                    if (addUserList.Count > 0)
                        uow.GetRepository<YssxUser>().Insert(addUserList);
                    //2、修改用户列表
                    if (updateUserList.Count > 0)
                    {
                        updateUserList.ForEach(x =>
                        {
                            uow.GetRepository<YssxUser>().Update(x);
                        });
                    }
                    //3、添加学生列表
                    if (addStudentList.Count > 0)
                        uow.GetRepository<YssxStudent>().Insert(addStudentList);
                    //4、修改学生列表
                    if (updateStudentList.Count > 0)
                    {
                        updateStudentList.ForEach(x =>
                        {
                            uow.GetRepository<YssxStudent>().Update(x);
                        });
                    }

                    //删除缓存
                    updateUserList.ForEach(x =>
                    {
                        RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{x.MobilePhone}");
                    });
                    deleteCache.ForEach(x =>
                    {
                        RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{x.MobilePhone}");
                    });

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                }
            }
            #endregion

            data.RightCount = studentData.Count - errorList.Count;
            data.ErrorCount = errorList.Count;
            data.ErrorList = errorList;

            response.Data = data;

            return response;
        }
        #endregion

        #region 教师手动添加学生
        /// <summary>
        /// 教师手动添加学生
        /// </summary>
        /// <param name="request"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ManualAddStudentByTeacher(TeacherAddStudentRequestModel request, UserTicket user)
        {
            if (request == null || request.ClassId == 0 || string.IsNullOrEmpty(request.Name) || string.IsNullOrEmpty(request.StudentNo) || string.IsNullOrEmpty(request.MobilePhone))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "参数不能为空!" };

            //验证手机号合理性
            if (!Regex.IsMatch(request.MobilePhone, @"^1[3456789]\d{9}$"))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "手机号码无效!" };

            var classModel = await DbContext.FreeSql.Select<YssxClass>().Where(x => x.Id == request.ClassId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (classModel == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级不存在!" };
            if (classModel.TenantId != user.TenantId)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "教师不在班级学校下!" };

            var school = await DbContext.FreeSql.Select<YssxSchool>().Where(x => x.Id == classModel.TenantId).FirstAsync();
            if (school == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学校不存在!" };
            if (school.IsDelete == CommonConstants.IsDelete)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学校已被删除!" };

            //查询用户表
            YssxUser userData = await DbContext.FreeSql.GetRepository<YssxUser>().Where(x => x.MobilePhone == request.MobilePhone &&
                 x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            bool result = false;

            #region 1、属于其他学校学生 不导入, 提示不在本校、需重新激活
            //1、属于其他学校学生 不导入, 提示不在本校、需重新激活
            if (userData != null && userData.TenantId != user.TenantId && userData.TenantId != CommonConstants.DefaultTenantId)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "非本校学生,不能导入!" };
            #endregion

            #region 2、在本校,身份不是学生
            //2、在本校,身份不是学生
            if (userData != null && userData.TenantId == user.TenantId && userData.UserType != (int)UserTypeEnums.Student)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "非学生身份,不能导入!" };
            #endregion

            #region 3、在本校的情况
            //3、在本校的情况
            //3.1、在本校没有绑定班级,身份是学生
            //3.2、在本校其他班级
            //3.3、在本校本班
            if (userData != null && userData.TenantId == user.TenantId && userData.UserType == (int)UserTypeEnums.Student)
            {
                var studentData = await DbContext.FreeSql.GetRepository<YssxStudent>().Where(x => x.UserId == userData.Id &&
                         x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

                if (studentData != null)
                {
                    //1、在本校本班
                    if (studentData.ClassId == classModel.Id)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已在本校本班,不能重复导入!" };

                    //2、没有绑定班级  
                    //3、已绑定了其他班级
                    if (studentData.ClassId == 0 || studentData.ClassId != classModel.Id)
                    {
                        studentData.Name = userData.RealName;
                        studentData.StudentNo = request.StudentNo;
                        studentData.TenantId = school.Id;
                        studentData.SchoolName = school.Name;
                        studentData.CollegeId = classModel.CollegeId;
                        studentData.ProfessionId = classModel.ProfessionId;
                        studentData.ClassId = classModel.Id;
                        studentData.ClassName = classModel.Name;
                        studentData.UpdateBy = user.Id;
                        studentData.UpdateTime = DateTime.Now;

                        //更新学生表
                        result = await DbContext.FreeSql.GetRepository<YssxStudent>().UpdateAsync(studentData) > 0;
                    }
                }
                else
                {
                    studentData = new YssxStudent
                    {
                        Id = IdWorker.NextId(),
                        UserId = userData.Id,
                        Name = userData.RealName,
                        StudentNo = request.StudentNo,
                        TenantId = school.Id,
                        SchoolName = school.Name,
                        CollegeId = classModel.CollegeId,
                        ProfessionId = classModel.ProfessionId,
                        ClassId = classModel.Id,
                        ClassName = classModel.Name,
                        CreateBy = user.Id,
                        UpdateBy = user.Id,
                        CreateTime = DateTime.Now,
                        UpdateTime = DateTime.Now,
                    };

                    //添加学生表
                    YssxStudent resultData = await DbContext.FreeSql.GetRepository<YssxStudent>().InsertAsync(studentData);

                    result = resultData != null;
                }

            }
            #endregion

            #region 4、注册未激活用户,学校是默认学校【中国测试大学】 1017666019115035
            //4、注册未激活用户,学校是默认学校【中国测试大学】 1017666019115035
            if (userData != null && userData.UserType == (int)UserTypeEnums.OrdinaryUser && userData.TenantId == CommonConstants.DefaultTenantId)
            {
                userData.UserType = (int)UserTypeEnums.Student;
                userData.UserName = request.Name;
                userData.RealName = request.Name;
                userData.TenantId = user.TenantId;
                userData.UpdateBy = user.Id;
                userData.UpdateTime = DateTime.Now;

                YssxStudent studentModel = new YssxStudent
                {
                    Id = IdWorker.NextId(),
                    UserId = userData.Id,
                    Name = userData.RealName,
                    StudentNo = request.StudentNo,
                    TenantId = school.Id,
                    SchoolName = school.Name,
                    CollegeId = classModel.CollegeId,
                    ProfessionId = classModel.ProfessionId,
                    ClassId = classModel.Id,
                    ClassName = classModel.Name,
                    CreateBy = user.Id,
                    UpdateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                };

                #region 新增数据
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //1、更新用户表
                        result = await uow.GetRepository<YssxUser>().UpdateAsync(userData) > 0;
                        //2、添加学生表
                        YssxStudent resultData = await uow.GetRepository<YssxStudent>().InsertAsync(studentModel);

                        result = resultData != null;

                        if (result)
                            uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                    }
                }
                #endregion
            }
            #endregion

            #region 5、未注册用户
            //5、未注册用户
            if (userData == null)
            {
                YssxUser userModel = new YssxUser
                {
                    Id = IdWorker.NextId(),
                    UserName = request.Name,
                    Password = CommonConstants.NewUserDefaultPwd.Md5(),
                    UserType = (int)UserTypeEnums.Student,
                    MobilePhone = request.MobilePhone,
                    NikeName = request.Name,
                    RealName = request.Name,
                    Status = (int)Status.Enable,
                    TenantId = user.TenantId,
                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                    ClientId = Guid.NewGuid(),
                    RegistrationType = 3
                };

                YssxStudent studentModel = new YssxStudent
                {
                    Id = IdWorker.NextId(),
                    UserId = userModel.Id,
                    Name = userModel.RealName,
                    StudentNo = request.StudentNo,
                    TenantId = school.Id,
                    SchoolName = school.Name,
                    CollegeId = classModel.CollegeId,
                    ProfessionId = classModel.ProfessionId,
                    ClassId = classModel.Id,
                    ClassName = classModel.Name,
                    CreateBy = user.Id,
                    UpdateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateTime = DateTime.Now,
                };

                #region 新增数据
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //1、添加用户表
                        YssxUser resultUser = await uow.GetRepository<YssxUser>().InsertAsync(userModel);
                        //2、添加学生表
                        YssxStudent resultStudent = await uow.GetRepository<YssxStudent>().InsertAsync(studentModel);

                        if (resultUser != null && resultStudent != null)
                            result = true;

                        if (result)
                            uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                    }
                }
                #endregion
            }
            #endregion

            //删除缓存
            RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{request.MobilePhone}");

            return new ResponseContext<bool>
            {
                Code = result ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Msg = result ? "操作成功!" : "添加失败!",
                Data = result
            };

        }
        #endregion

    }
}
