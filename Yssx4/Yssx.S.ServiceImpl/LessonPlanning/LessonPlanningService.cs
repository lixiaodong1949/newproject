﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.Entity;
using Yssx.Framework.AutoMapper;
using Yssx.Repository.Extensions;
using Yssx.S.Poco;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 课程下备课服务 [Obsolete]
    /// </summary>
    public class LessonPlanningService : ILessonPlanningService
    {
        #region 新增/编辑课程备课
        /// <summary>
        /// 新增/编辑课程备课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> AddOrEditLessonPlanning(LessonPlanningRequestModel dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "对象是空的!" };

            YssxLessonPlanning entity = new YssxLessonPlanning
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                LessonPlanName = dto.LessonPlanName,
                CourseId = dto.CourseId,
                IsStudentLook = dto.IsStudentLook,
                SemesterId = dto.SemesterId,
                Sort = dto.Sort,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;

                YssxLessonPlanning lessonModel = await DbContext.FreeSql.GetRepository<YssxLessonPlanning>().InsertAsync(entity);
                state = lessonModel != null;
            }
            else
            {
                state = await DbContext.FreeSql.GetRepository<YssxLessonPlanning>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                {
                    x.LessonPlanName,
                    x.IsStudentLook,
                    x.SemesterId,
                    x.Sort,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<long> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = entity.Id, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取课程备课列表
        /// <summary>
        /// 获取课程备课列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxLessonPlanningViewModel>>> GetLessonPlanningList(YssxLessonPlanningQuery query)
        {
            ResponseContext<List<YssxLessonPlanningViewModel>> response = new ResponseContext<List<YssxLessonPlanningViewModel>>();
            if (query == null)
                return response;

            var select = DbContext.FreeSql.GetRepository<YssxLessonPlanning>()
                .Where(x => x.CourseId == query.CourseId && x.IsDelete == CommonConstants.IsNotDelete);

            if (!string.IsNullOrEmpty(query.LessonPlanName))
                select.Where(x => x.LessonPlanName.Contains(query.LessonPlanName));
            if (query.SemesterId.HasValue)
                select.Where(x => x.SemesterId == query.SemesterId.Value);

            List<YssxLessonPlanningViewModel> list = await select.ToListAsync(x => new YssxLessonPlanningViewModel
            {
                Id = x.Id,
                LessonPlanName = x.LessonPlanName,
                CourseId = x.CourseId,
                IsStudentLook = x.IsStudentLook,
                SemesterId = x.SemesterId,
                Sort = x.Sort,
                CreateTime = x.CreateTime,
            });

            List<long> lessonPlanIds = list.Select(x => x.Id).ToList();

            //试卷列表
            List<ExamPaperCourse> examPaperList = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Select.Where(x => lessonPlanIds.Contains(x.CourseId) &&
                x.Status == ExamStatus.Started && x.ExamType == CourseExamType.ClassTest && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            list.ForEach(x =>
            {
                //试卷Id
                if (examPaperList.Count > 0)
                {
                    x.ExamId = examPaperList.Where(a => a.CourseId == x.Id).OrderByDescending(a => a.CreateTime).FirstOrDefault()?.Id;
                    x.TaskId = examPaperList.Where(a => a.CourseId == x.Id).OrderByDescending(a => a.CreateTime).FirstOrDefault()?.TaskId;
                }
            });

            response.Data = list;

            return response;
        }
        #endregion

        #region 删除课程备课
        /// <summary>
        /// 删除课程备课
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteLessonPlanning(List<long> ids, UserTicket user)
        {
            var list = await DbContext.FreeSql.GetRepository<YssxLessonPlanning>().Where(x => ids.Contains(x.Id)).ToListAsync();

            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            list.ForEach(x =>
            {
                x.IsDelete = 1;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });

            bool state = await DbContext.FreeSql.GetRepository<YssxLessonPlanning>().UpdateDiy.SetSource(list).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 新增课程备课素材、教案
        /// <summary>
        /// 新增课程备课素材、教案
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddLessonPlanningFiles(LessonPlanningFilesRequestModel dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            bool state = false;

            YssxSectionFiles entity = new YssxSectionFiles();
            entity.Id = IdWorker.NextId();
            entity.CourseId = dto.CourseId;
            entity.SectionId = dto.LessonPlanId; //课程备课Id赋值到SectionId
            entity.CourseFileId = dto.CourseFileId;
            entity.File = dto.File;
            entity.FileName = dto.FileName;
            entity.FilesType = dto.FilesType;
            entity.SectionType = dto.SectionType;
            entity.Sort = dto.Sort;

            entity.CreateBy = user.Id;
            entity.CreateTime = DateTime.Now;
            entity.UpdateBy = user.Id;
            entity.UpdateTime = DateTime.Now;

            YssxSectionFiles model = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().InsertAsync(entity);
            state = model != null;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取课程备课素材、教案列表
        /// <summary>
        /// 获取课程备课素材、教案列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxLessonPlanningFilesViewModel>>> GetLessonPlanningFilesList(YssxLessonPlanningFilesQuery query)
        {
            ResponseContext<List<YssxLessonPlanningFilesViewModel>> response = new ResponseContext<List<YssxLessonPlanningFilesViewModel>>();

            if (query == null)
                return response;

            var select = DbContext.FreeSql.GetRepository<YssxSectionFiles>().Where(x => x.SectionId == query.LessonPlanId
                && x.IsDelete == CommonConstants.IsNotDelete);

            if (query.CourseId.HasValue)
                select.Where(x => x.CourseId == query.CourseId.Value);
            if (query.SectionType.HasValue)
                select.Where(x => x.SectionType == query.SectionType.Value);
            if (!string.IsNullOrEmpty(query.FileName))
                select.Where(x => x.FileName.Contains(query.FileName));

            if (!query.SectionType.HasValue)
                select.Where(x => x.SectionType == 2 || x.SectionType == 5);

            response.Data = await select.From<YssxCourceFiles>((a, b) =>
             a.InnerJoin(x => x.CourseFileId == b.Id))
                .ToListAsync((a, b) => new YssxLessonPlanningFilesViewModel
                {
                    Id = a.Id,
                    CourseId = a.CourseId,
                    LessonPlanId = a.SectionId,
                    CourseFileId = a.CourseFileId,
                    File = b.File,
                    FileName = b.FileName,
                    FilesType = b.FilesType,
                    SectionType = a.SectionType,
                    Sort = a.Sort,
                });

            return response;
        }
        #endregion

        #region 删除课程备课素材、教案
        /// <summary>
        /// 删除课程备课素材、教案
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveLessonPlanningFiles(long id, UserTicket user)
        {
            YssxSectionFiles entity = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 新增课程备课习题
        /// <summary>
        /// 新增课程备课习题
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddLessonPlanningTopic(LessonPlanningTopicRequestModel dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            bool state = false;

            YssxSectionTopic entity = new YssxSectionTopic();
            entity.Id = IdWorker.NextId();
            entity.CourseId = dto.CourseId;
            entity.SectionId = dto.LessonPlanId; //课程备课Id赋值到SectionId
            entity.QuestionId = dto.QuestionId;
            entity.QuestionName = dto.QuestionName;
            entity.Sort = dto.Sort;

            entity.CreateBy = user.Id;
            entity.CreateTime = DateTime.Now;
            entity.UpdateBy = user.Id;
            entity.UpdateTime = DateTime.Now;

            YssxSectionTopic model = await DbContext.FreeSql.GetRepository<YssxSectionTopic>().InsertAsync(entity);
            state = model != null;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取课程备课习题列表
        /// <summary>
        /// 获取课程备课习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxLessonPlanningTopicViewModel>>> GetLessonPlanningTopicList(YssxLessonPlanningInfoQuery query)
        {
            ResponseContext<List<YssxLessonPlanningTopicViewModel>> response = new ResponseContext<List<YssxLessonPlanningTopicViewModel>>();

            if (query == null || !query.LessonPlanId.HasValue)
                return response;

            FreeSql.ISelect<YssxSectionTopic> select = DbContext.FreeSql.GetRepository<YssxSectionTopic>()
                .Where(x => x.SectionId == query.LessonPlanId.Value && x.IsDelete == CommonConstants.IsNotDelete);

            if (query.CourseId.HasValue)
                select.Where(x => x.CourseId == query.CourseId.Value);

            response.Data = await select.From<YssxTopicPublic>((a, b) => a.InnerJoin(aa => aa.QuestionId == b.Id)).OrderBy((a, b) => a.Sort)
                .ToListAsync((a, b) => new YssxLessonPlanningTopicViewModel
                {
                    Id = a.Id,
                    CourseId = a.CourseId,
                    LessonPlanId = a.SectionId,
                    QuestionId = a.QuestionId,
                    QuestionName = b.Title,
                    QuestionType = b.QuestionType,
                    KnowledgePointId = b.KnowledgePointIds,
                    QuestionContent = b.Content,
                    Score = b.Score,
                    Sort = a.Sort,
                });

            return response;
        }
        #endregion

        #region 删除课程备课习题
        /// <summary>
        /// 删除课程备课习题
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveLessonPlanningTopic(long id, UserTicket user)
        {
            YssxSectionTopic entity = await DbContext.FreeSql.GetRepository<YssxSectionTopic>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<YssxSectionTopic>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取课程备课素材、习题列表
        /// <summary>
        /// 获取课程备课素材、习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxLessonPlanningCdrAndTopicViewModel>>> GetLessonPlanningCdrAndTopicList(YssxLessonPlanningInfoQuery query)
        {
            ResponseContext<List<YssxLessonPlanningCdrAndTopicViewModel>> response = new ResponseContext<List<YssxLessonPlanningCdrAndTopicViewModel>>();

            if (query == null || !query.LessonPlanId.HasValue)
                return response;

            List<YssxLessonPlanningCdrAndTopicViewModel> list = new List<YssxLessonPlanningCdrAndTopicViewModel>();

            #region 习题查询

            FreeSql.ISelect<YssxSectionTopic> topicSelect = DbContext.FreeSql.GetRepository<YssxSectionTopic>()
                .Where(x => x.SectionId == query.LessonPlanId && x.IsDelete == CommonConstants.IsNotDelete);

            if (query.CourseId.HasValue)
                topicSelect.Where(x => x.CourseId == query.CourseId.Value);

            List<YssxLessonPlanningCdrAndTopicViewModel> topicList = new List<YssxLessonPlanningCdrAndTopicViewModel>();

            var topicData = await topicSelect.From<YssxTopicPublic>((a, b) => a.InnerJoin(x => x.QuestionId == b.Id))
                .OrderBy((a, b) => a.Sort).ToListAsync((a, b) => new
                {
                    Id = a.Id,
                    CourseId = a.CourseId,
                    LessonPlanId = a.SectionId,
                    QuestionId = a.QuestionId,
                    QuestionName = b.Title,
                    QuestionType = b.QuestionType,
                    KnowledgePointId = b.KnowledgePointIds,
                    QuestionContent = b.Content,
                    Score = b.Score,
                    ResourceType = 0,
                    Sort = a.Sort,
                });

            topicData.ForEach(a =>
            {
                YssxLessonPlanningCdrAndTopicViewModel model = new YssxLessonPlanningCdrAndTopicViewModel();
                model.Id = a.Id;
                model.CourseId = a.CourseId;
                model.LessonPlanId = a.LessonPlanId;
                model.QuestionId = a.QuestionId;
                model.QuestionName = a.QuestionName;
                model.QuestionType = a.QuestionType;
                model.KnowledgePointId = a.KnowledgePointId;
                model.QuestionContent = a.QuestionContent;
                model.Score = a.Score;
                model.ResourceType = 0;
                model.Sort = a.Sort;

                topicList.Add(model);
            });

            list.AddRange(topicList);

            #endregion

            #region 素材查询

            FreeSql.ISelect<YssxSectionFiles> filesSelect = DbContext.FreeSql.GetRepository<YssxSectionFiles>().Where(x => x.SectionId == query.LessonPlanId
                && x.IsDelete == CommonConstants.IsNotDelete && x.SectionType == 5);

            if (query.CourseId.HasValue)
                filesSelect.Where(x => x.CourseId == query.CourseId.Value);

            List<YssxLessonPlanningCdrAndTopicViewModel> filesList = await filesSelect.From<YssxCourceFiles>((a, b) =>
             a.InnerJoin(x => x.CourseFileId == b.Id))
                .ToListAsync((a, b) => new YssxLessonPlanningCdrAndTopicViewModel
                {
                    Id = a.Id,
                    CourseId = a.CourseId,
                    LessonPlanId = a.SectionId,
                    CourseFileId = a.CourseFileId,
                    File = b.File,
                    FileName = b.FileName,
                    FilesType = b.FilesType,
                    ResourceType = 1,
                    Sort = a.Sort,
                });

            list.AddRange(filesList);

            #endregion

            response.Data = list.OrderBy(x => x.Sort).ToList();

            return response;
        }
        #endregion

        #region 更新课程备课素材、习题排序
        /// <summary>
        /// 更新课程备课素材、习题排序
        /// </summary>
        /// <param name="list"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpdateLessonPlanningCdrAndTopicSort(List<LessonPlanningCdrAndTopicRequestModel> list, UserTicket user)
        {
            if (list == null || list.Count == 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            bool state = false;

            //习题列表
            List<long> topicIds = list.Where(x => x.ResourceType == 0).Select(x => x.Id).ToList();
            List<YssxSectionTopic> topicList = await DbContext.FreeSql.GetRepository<YssxSectionTopic>()
                .Where(x => topicIds.Contains(x.Id) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            topicList.ForEach(x =>
            {
                x.Sort = list.Where(a => a.Id == x.Id).FirstOrDefault()?.Sort ?? 0;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });

            //素材列表
            List<long> cdr = list.Where(x => x.ResourceType == 1).Select(x => x.Id).ToList();
            List<YssxSectionFiles> filesList = await DbContext.FreeSql.GetRepository<YssxSectionFiles>()
                .Where(x => cdr.Contains(x.Id) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            filesList.ForEach(x =>
            {
                x.Sort = list.Where(a => a.Id == x.Id).FirstOrDefault()?.Sort ?? 0;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //习题列表
                    if (topicList.Count > 0)
                    {
                        state = await uow.GetRepository<YssxSectionTopic>().UpdateDiy.SetSource(topicList).UpdateColumns(x => new
                        {
                            x.Sort,
                            x.UpdateTime,
                            x.UpdateBy
                        }).ExecuteAffrowsAsync() > 0;
                    }

                    //素材列表
                    if (filesList.Count > 0)
                    {
                        state = await uow.GetRepository<YssxSectionFiles>().UpdateDiy.SetSource(filesList).UpdateColumns(x => new
                        {
                            x.Sort,
                            x.UpdateTime,
                            x.UpdateBy
                        }).ExecuteAffrowsAsync() > 0;
                    }

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

    }
}
