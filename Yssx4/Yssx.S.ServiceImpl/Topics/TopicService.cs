﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices.Topics;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Topics;
using Yssx.S.IServices.Examination;
using Yssx.Repository.Extensions;

namespace Yssx.S.ServiceImpl.Topics
{
    /// <summary>
    /// 题目管理
    /// </summary>
    public class TopicService : ITopicService
    {
        #region 添加题目
        /// <summary>
        /// 添加简单类型题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveSimpleQuestion(SimpleQuestionRequest model, UserTicket CurrentUser)
        {
            return await Task.Run(() =>
            {
                long opreationId = CurrentUser.Id;//操作人ID
                DateTime dtNow = DateTime.Now;

                #region 校验
                //TODO 校验
                if (!model.Options.Any())
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请添加选项" };
                }
                if (model.Id > 0)
                {
                    var rYssxTopic = DbContext.FreeSql.GetRepository<YssxTopic>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rYssxTopic.Any())
                    {
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                    }
                }

                #endregion
                var topic = model.MapTo<YssxTopic>();
                topic.IsGzip = CommonConstants.IsGzip;
                topic.CreateBy = opreationId;
                topic.CreateTime = dtNow;
                if (model.Id == 0) { topic.Id = IdWorker.NextId(); }

                #region 处理选项排序
                var loop = 0;
                //选项信息
                var options = new List<YssxAnswerOption>();
                foreach (var item in model.Options)
                {
                    loop++;
                    options.Add(new YssxAnswerOption()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        TopicId = topic.Id,
                        AnswerOption = item.Text.Trim(),
                        Sort = loop,
                        CreateBy = opreationId,
                        CreateTime = dtNow,

                        AnswerKeysContent = "",
                        AnswerFileUrl = ""
                    });
                }
                #endregion

                if (model.Id == 0)
                {
                    #region 添加题目，选项，题目类别关联信息
                    options.ForEach(a => a.Id = IdWorker.NextId());

                    //添加到数据库
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        //添加题目
                        DbContext.FreeSql.Insert(topic).ExecuteAffrows();
                        DbContext.FreeSql.Insert<YssxAnswerOption>().AppendData(options).ExecuteAffrows();
                        uow.Commit();
                    }
                    #endregion
                }
                else
                {
                    topic.UpdateTime = dtNow;
                    topic.UpdateBy = opreationId;

                    #region 新增选项--添加ID
                    var addOption = options.Where(a => a.Id == 0).ToList();
                    var updateOption = options.Where(a => a.Id > 0).ToList();
                    addOption.ForEach(a =>
                    {
                        a.Id = IdWorker.NextId();
                    });

                    //取删除的选项
                    var existOptionId = options.Select(a => a.Id).ToArray();
                    var delOption = DbContext.FreeSql.Select<YssxAnswerOption>().Where(m => m.TopicId == topic.Id && m.IsDelete == CommonConstants.IsNotDelete && !existOptionId.Contains(m.Id)).ToList();
                    if (delOption.Count > 0)
                    {
                        delOption.ForEach(a =>
                        {
                            a.IsDelete = CommonConstants.IsDelete;
                            a.UpdateBy = opreationId;
                            a.UpdateTime = dtNow;
                        });
                    }
                    #endregion

                    #region 更新题目,选项(更新，添加)。题目类别关联信息不允许修改
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        //更新题目
                        DbContext.FreeSql.Update<YssxTopic>().SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrows();

                        #region 选项(更新，添加)
                        //添加
                        if (addOption.Count > 0)
                        {
                            DbContext.FreeSql.Insert<YssxAnswerOption>().AppendData(addOption).ExecuteAffrows();
                        }
                        //删除的选项
                        if (delOption.Count > 0)
                        {
                            DbContext.FreeSql.Update<YssxAnswerOption>().SetSource(delOption).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                        }
                        //更新选项
                        if (updateOption.Count > 0)
                        {
                            foreach (var item in updateOption)
                            {
                                DbContext.FreeSql.Update<YssxAnswerOption>().SetSource(item).UpdateColumns(a => new
                                {
                                    a.AnswerOption,
                                    a.Name,
                                    a.AnswerFileUrl,
                                    a.Sort,
                                    a.UpdateBy,
                                    a.UpdateTime
                                }).ExecuteAffrows();
                            }
                        }
                        #endregion

                        uow.Commit();
                    }
                    #endregion

                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        ///  添加复杂类型题目（分录题、表格题、案例题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveComplexQuestion(ComplexQuestionRequest model, UserTicket CurrentUser)
        {
            return await Task.Run(() =>
            {
                long opreationId = CurrentUser.Id;//操作人ID
                DateTime dtNow = DateTime.Now;

                #region 校验
                //TODO 校验
                var oldTopic = new YssxTopic();
                if (model.Id > 0)
                {
                    var rYssxTopic = DbContext.FreeSql.GetRepository<YssxTopic>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rYssxTopic.Any())
                    {
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                    }
                    oldTopic = rYssxTopic.First();
                }
                if (model.PartialScore > 0 && model.PartialScore >= model.Score)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，非完整性得分不能大于等于完整性得分" };
                }
                #endregion

                var topic = model.MapTo<YssxTopic>();
                topic.IsGzip = CommonConstants.IsGzip;
                topic.CreateBy = opreationId;
                topic.CreateTime = dtNow;
                if (model.Id == 0)
                {
                    topic.Id = IdWorker.NextId();
                }

                #region 文件排序
                //添加附件--用于排序需要
                var files = new List<YssxTopicFile>();
                var loop = 0;
                if (model.QuestionFile != null)
                {
                    foreach (var item in model.QuestionFile)
                    {
                        loop++;
                        //TODO上传附件获取URL地址
                        files.Add(new YssxTopicFile()
                        {
                            Id = IdWorker.NextId(),
                            Name = item.Name,
                            Url = item.Url,
                            Sort = loop
                        });
                    }
                }
                #endregion

                if (model.Id == 0)
                {
                    //添加到数据库
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        topic.TopicFileIds = string.Join(",", files.Select(m => m.Id));
                        //添加题目
                        DbContext.FreeSql.Insert(topic).ExecuteAffrows();
                        //添加附件
                        DbContext.FreeSql.Insert<YssxTopicFile>().AppendData(files).ExecuteAffrows();
                        uow.Commit();
                    }
                }
                else
                {
                    topic.UpdateTime = DateTime.Now;
                    topic.UpdateBy = opreationId;

                    #region 取删除的文件
                    var delTopicFile = new List<YssxTopicFile>();
                    if (!String.IsNullOrEmpty(oldTopic.TopicFileIds))
                    {
                        var rOldfileIds = oldTopic.TopicFileIds.SplitToArray<long>(',');
                        var rNewFileIds = files.Select(m => m.Id);
                        var rDelFileIds = rOldfileIds.Where(m => !rNewFileIds.Contains(m));
                        if (rDelFileIds.Count() > 0)
                        {
                            delTopicFile = DbContext.FreeSql.Select<YssxTopicFile>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && rDelFileIds.Contains(m.Id)).ToList();
                            if (delTopicFile.Count > 0)
                            {
                                delTopicFile.ForEach(a =>
                                {
                                    a.IsDelete = CommonConstants.IsDelete;
                                    a.UpdateBy = opreationId;
                                    a.UpdateTime = dtNow;
                                });
                            }
                        }
                    }
                    #endregion

                    #region 更新题目，更新文件顺序字段，添加文件顺序
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        if (delTopicFile.Count > 0)
                        {
                            DbContext.FreeSql.Update<YssxTopicFile>().SetSource(delTopicFile).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                        }
                        //添加附件
                        DbContext.FreeSql.Insert<YssxTopicFile>().AppendData(files).ExecuteAffrows();

                        topic.TopicFileIds = string.Join(",", files.Select(m => m.Id));
                        //更新题目
                        DbContext.FreeSql.Update<YssxTopic>().SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrows();

                        uow.Commit();
                    }
                    #endregion

                }

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 添加多题型题目（综合题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveMainSubQuestion(MainSubQuestionRequest model, UserTicket CurrentUser)
        {
            return await Task.Run(() =>
            {
                long opreationId = CurrentUser.Id;//操作人ID
                DateTime dtNow = DateTime.Now;

                #region 校验
                //TODO 校验
                var rYssxCase = DbContext.FreeSql.GetRepository<YssxCase>().Where(m => m.Id == model.CaseId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rYssxCase == null)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到案例信息" };
                }
                var oldTopic = new YssxTopic();
                if (model.Id > 0)
                {
                    var rYssxTopic = DbContext.FreeSql.GetRepository<YssxTopic>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rYssxTopic.Any())
                    {
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                    }
                    oldTopic = rYssxTopic.First();
                }
                if (model.SubQuestion.Count == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，子题目为0" };
                //验证题目类型是否有效
                var rIsInvalidQuestionType = model.SubQuestion
                    .Where(m => m.QuestionType == QuestionType.SingleChoice
                            || m.QuestionType == QuestionType.MultiChoice
                            || m.QuestionType == QuestionType.Judge
                            || m.QuestionType == QuestionType.FillGrid
                            || m.QuestionType == QuestionType.AccountEntry
                            || m.QuestionType == QuestionType.FillBlank
                            || m.QuestionType == QuestionType.FillGraphGrid).Count();
                if (rIsInvalidQuestionType != model.SubQuestion.Count)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，子题目类型有误" };
                }
                #endregion

                //支持选择题-选项
                var options = new List<YssxAnswerOption>();
                var topic = model.MapTo<YssxTopic>();
                topic.IsGzip = CommonConstants.IsGzip;
                topic.CreateBy = opreationId;
                topic.CreateTime = dtNow;
                if (model.Id == 0) { topic.Id = IdWorker.NextId(); }

                #region 文件排序
                //添加附件--用于排序需要
                var files = new List<YssxTopicFile>();
                var loop = 0;
                if (model.QuestionFile != null)
                {
                    foreach (var item in model.QuestionFile)
                    {
                        loop++;
                        files.Add(new YssxTopicFile()
                        {
                            Id = IdWorker.NextId(),
                            Name = item.Name,
                            Url = item.Url,
                            Sort = loop
                        });
                    }
                }

                #endregion

                if (model.Id == 0)
                {
                    //子题目 排序 添加父节点ID
                    #region 处理数据
                    model.SubQuestion.ForEach(a =>
                    {
                        a.Id = IdWorker.NextId();
                        a.CalculationType = a.QuestionType == QuestionType.AccountEntry ? (rYssxCase.SetType == AccountEntryCalculationType.Cell ? CalculationType.FreeCalculation : CalculationType.AvgCellCount) : a.CalculationType;
                    });
                    var subQuestion = model.SubQuestion.Select(a => a.MapTo<YssxTopic>()).ToList();
                    subQuestion.ForEach(a =>
                    {
                        a.ParentId = topic.Id;
                        a.IsGzip = CommonConstants.IsGzip;
                        a.CaseId = model.CaseId;
                        a.PositionId = 0;
                    });
                    //分录题
                    #region 
                    var rAccountEntryData = model.SubQuestion.Where(m => m.QuestionType == QuestionType.AccountEntry).Select(a => a.MapTo<YssxCertificateTopic>()).ToList();
                    var rYssxCertificateData = new List<YssxCertificateDataRecord>();
                    if (rAccountEntryData.Count > 0)
                    {
                        rAccountEntryData.ForEach(a =>
                        {
                            a.TopicId = a.Id;
                            a.Id = IdWorker.NextId();
                        });
                        var rListSubQuestion = model.SubQuestion.Where(m => m.QuestionType == QuestionType.AccountEntry).ToList();
                        if (rListSubQuestion.Count > 0)
                        {
                            foreach (var itemSub in rListSubQuestion)
                            {
                                var rCertificateTopic = rAccountEntryData.Where(m => m.TopicId == itemSub.Id).First();
                                List<YssxCertificateDataRecord> rListRecord = itemSub.DataRecords.MapTo<List<YssxCertificateDataRecord>>();
                                foreach (var itemRecord in rListRecord)
                                {
                                    itemRecord.Id = IdWorker.NextId();
                                    itemRecord.CertificateNo = itemSub.CertificateNo;
                                    itemRecord.CertificateDate = itemSub.CertificateDate;
                                    itemRecord.CertificateTopicId = rCertificateTopic.Id;
                                    itemRecord.CreateTime = dtNow;
                                    itemRecord.EnterpriseId = model.CaseId;
                                    rYssxCertificateData.Add(itemRecord);
                                }
                            }
                        }
                    }
                    #endregion
                    foreach (var item in model.SubQuestion)
                    {
                        if (item.Options != null && item.Options.Any())
                        {
                            item.Options.ForEach(a =>
                            {
                                var option = new YssxAnswerOption()
                                {
                                    TopicId = item.Id,
                                    Name = a.Name,
                                    AnswerOption = a.Text,
                                    AnswerFileUrl = a.AttatchImgUrl,
                                    AnswerKeysContent = item.AnswerValue,
                                };
                                options.Add(option);
                            });
                        }

                    }
                    options.ForEach(a => { a.Id = IdWorker.NextId(); });

                    DbContext.FreeSql.Transaction(() =>
                    {
                        topic.TopicFileIds = string.Join(",", files.Select(m => m.Id));
                        //添加题目
                        DbContext.FreeSql.Insert(topic).ExecuteAffrows();
                        //添加附件
                        DbContext.FreeSql.Insert<YssxTopicFile>().AppendData(files).ExecuteAffrows();
                        //添加子题目
                        DbContext.FreeSql.Insert<YssxTopic>().AppendData(subQuestion).ExecuteAffrows();
                        //添加子题目-选项
                        if (options.Any())
                        {
                            DbContext.FreeSql.Insert<YssxAnswerOption>().AppendData(options).ExecuteAffrows();
                        }
                        //添加分录题内容
                        if (rAccountEntryData.Count > 0)
                        {
                            DbContext.FreeSql.Insert<YssxCertificateTopic>().AppendData(rAccountEntryData).ExecuteAffrows();
                            DbContext.FreeSql.Insert<YssxCertificateDataRecord>().AppendData(rYssxCertificateData).ExecuteAffrows();
                        }
                    });
                    #endregion
                }
                else
                {
                    //子题目 排序 添加父节点ID
                    #region 处理数据
                    var newSubQuestion = model.SubQuestion;
                    topic.UpdateTime = dtNow;
                    topic.UpdateBy = opreationId;
                    //子题目
                    var addSubQuestion = newSubQuestion.Where(a => a.Id == 0).ToList();
                    var updateSubQuestion = newSubQuestion.Where(a => a.Id > 0);
                    //先取出添加或修改的题目数据，再设置添加题目的ID
                    newSubQuestion.ForEach(a =>
                    {
                        if (a.Id == 0) { a.Id = IdWorker.NextId(); }
                        a.CalculationType = a.QuestionType == QuestionType.AccountEntry ? (rYssxCase.SetType == AccountEntryCalculationType.Cell ? CalculationType.FreeCalculation : CalculationType.AvgCellCount) : a.CalculationType;
                    });

                    var addSubTopic = addSubQuestion.Select(a => a.MapTo<YssxTopic>()).ToList();
                    var updateSubTopic = updateSubQuestion.Select(a => a.MapTo<YssxTopic>()).ToList();

                    addSubTopic.ForEach(a =>
                    {
                        a.ParentId = topic.Id;
                        a.IsGzip = CommonConstants.IsGzip;
                        a.CaseId = model.CaseId;
                        a.PositionId = 0;
                    });
                    updateSubTopic.ForEach(a =>
                    {
                        a.ParentId = topic.Id;
                        a.IsGzip = CommonConstants.IsGzip;
                        a.CaseId = model.CaseId;
                        a.PositionId = 0;
                    });

                    #region 分录题
                    List<long> rCertificateTopicIdData = new List<long>();
                    var rAddAccountEntryData = addSubQuestion.Where(m => m.QuestionType == QuestionType.AccountEntry).Select(a => a.MapTo<YssxCertificateTopic>()).ToList();
                    var rUpdAccountEntryData = updateSubQuestion.Where(m => m.QuestionType == QuestionType.AccountEntry).Select(a => a.MapTo<YssxCertificateTopic>()).ToList();

                    var rYssxCertificateData = new List<YssxCertificateDataRecord>();
                    var rIsHaveAccountEntry = newSubQuestion.Any(m => m.QuestionType == QuestionType.AccountEntry);
                    if (rIsHaveAccountEntry)
                    {
                        //新增题目ID设置
                        rAddAccountEntryData.ForEach(a =>
                        {
                            a.TopicId = a.Id;
                            a.Id = IdWorker.NextId();
                        });
                        //修改题目ID设置
                        if (rUpdAccountEntryData.Count > 0)
                        {
                            var rUpdCertificateTopicId = rUpdAccountEntryData.Select(a => a.Id).ToList();
                            var rUpdYssxCertificateData = DbContext.FreeSql.Select<YssxCertificateTopic>().Where(x => rUpdCertificateTopicId.Contains(x.TopicId)).ToList();
                            foreach (var itemCertificate in rUpdAccountEntryData)
                            {
                                var rUpdEntity = rUpdYssxCertificateData.Where(m => m.TopicId == itemCertificate.Id).FirstOrDefault();
                                if (rUpdEntity != null)
                                {
                                    itemCertificate.TopicId = rUpdEntity.TopicId;
                                    itemCertificate.Id = rUpdEntity.Id;
                                }
                            }
                        }
                        //录题凭证数据记录
                        var rListSubQuestion = newSubQuestion.Where(m => m.QuestionType == QuestionType.AccountEntry).ToList();
                        if (rListSubQuestion.Count > 0)
                        {
                            //取凭证题目ID
                            var rSubQuestionId = rListSubQuestion.Select(m => m.Id).ToList();
                            rCertificateTopicIdData = DbContext.FreeSql.Select<YssxCertificateTopic>().Where(x => rSubQuestionId.Contains(x.TopicId)).ToList(m => m.Id);
                            //取凭证记录数据
                            foreach (var itemSub in rListSubQuestion)
                            {
                                var rAddCertificateTopic = rAddAccountEntryData.Where(m => m.TopicId == itemSub.Id).FirstOrDefault();
                                var rUpdCertificateTopic = rUpdAccountEntryData.Where(m => m.TopicId == itemSub.Id).FirstOrDefault();
                                List<YssxCertificateDataRecord> rListRecord = itemSub.DataRecords.MapTo<List<YssxCertificateDataRecord>>();
                                foreach (var itemRecord in rListRecord)
                                {
                                    itemRecord.Id = IdWorker.NextId();
                                    itemRecord.CertificateNo = itemSub.CertificateNo;
                                    itemRecord.CertificateDate = itemSub.CertificateDate;
                                    itemRecord.CertificateTopicId = rUpdCertificateTopic == null ? rAddCertificateTopic.Id : rUpdCertificateTopic.Id;
                                    itemRecord.CreateTime = dtNow;
                                    itemRecord.EnterpriseId = model.CaseId;
                                    rYssxCertificateData.Add(itemRecord);
                                }
                            }
                        }
                    }
                    #endregion

                    #endregion

                    var existIds = updateSubQuestion.Select(a => a.Id);
                    //子题目-选择题-选项
                    foreach (var item in newSubQuestion)
                    {
                        if (item.Options != null && item.Options.Any())
                        {
                            item.Options.ForEach(a =>
                            {
                                var option = new YssxAnswerOption()
                                {
                                    Id = a.Id,
                                    TopicId = item.Id,
                                    Name = a.Name,
                                    AnswerOption = a.Text,
                                    AnswerFileUrl = a.AttatchImgUrl,
                                    AnswerKeysContent = item.AnswerValue,
                                };
                                options.Add(option);
                            });
                        }

                    }
                    var addOption = options.Where(a => a.Id == 0).ToList();
                    var updateOption = options.Where(a => a.Id > 0);
                    addOption.ForEach(a => a.Id = IdWorker.NextId());

                    #region 取待删除数据
                    //取子题目
                    var rDelSubQuestion = DbContext.FreeSql.Select<YssxTopic>().Where(m => m.ParentId == topic.Id && !existIds.Contains(m.Id)).ToList();
                    rDelSubQuestion.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                    //取子题目-选项
                    var topicIds = options.Select(a => a.TopicId).Distinct().ToArray();
                    var optionIds = options.Select(a => a.Id).ToArray();
                    var rDelSubOption = DbContext.FreeSql.Select<YssxAnswerOption>().Where(a => topicIds.Contains(a.TopicId) && !optionIds.Contains(a.Id)).ToList();
                    rDelSubOption.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });

                    ////取文件
                    //var delTopicFile = new List<YssxTopicFile>();
                    //if (!string.IsNullOrEmpty(oldTopic.TopicFileIds))
                    //{
                    //    var idArr = oldTopic.TopicFileIds.Split(',').Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                    //    if (idArr.Any())
                    //    {
                    //        delTopicFile = DbContext.FreeSql.Select<YssxTopicFile>().Where(m => idArr.Contains(m.Id)).ToList();
                    //    }
                    //}
                    #endregion

                    #region 更新题目，更新文件顺序字段，添加文件顺序
                    DbContext.FreeSql.Transaction(() =>
                    {
                        #region 附件 处理
                        if (!string.IsNullOrEmpty(oldTopic.TopicFileIds))
                        {
                            var fileIds = oldTopic.TopicFileIds.SplitToArray<long>(',');
                            DbContext.FreeSql.Delete<YssxTopicFile>().Where(a => fileIds.Contains(a.Id)).ExecuteAffrows();
                        }
                        //添加附件
                        DbContext.FreeSql.Insert<YssxTopicFile>().AppendData(files).ExecuteAffrows();
                        topic.TopicFileIds = string.Join(",", files.Select(m => m.Id));
                        #endregion

                        //更新主题目
                        DbContext.FreeSql.Update<YssxTopic>().SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.QuestionContentType }).ExecuteAffrows();

                        #region 子题目操作
                        //删除不存在的题目
                        if (rDelSubQuestion.Any())
                        {
                            DbContext.FreeSql.Update<YssxTopic>().SetSource(rDelSubQuestion).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                        }
                        //添加新题目
                        if (addSubTopic.Any())
                        {
                            DbContext.FreeSql.Insert<YssxTopic>().AppendData(addSubTopic).ExecuteAffrows();
                        }
                        //更新已存在题目
                        if (updateSubTopic.Any())
                        {
                            foreach (var item in updateSubTopic)
                            {
                                //更新题目内容
                                DbContext.FreeSql.Update<YssxTopic>().SetSource(item).UpdateColumns(a => new
                                {
                                    a.Sort,
                                    a.Content,
                                    a.Hint,
                                    a.FullContent,
                                    a.TopicContent,
                                    a.AnswerValue,
                                    a.Score,
                                    a.PartialScore,
                                    a.CalculationType,
                                    a.IsCopy,
                                    a.IsCopyBigData,
                                    a.IsDisorder
                                }).ExecuteAffrows();
                            }
                        }
                        #endregion

                        #region 子题目-选择题-选项
                        if (options.Any())
                        {
                            #region 选项(更新，添加)
                            //删除选项
                            if (rDelSubOption.Any())
                            {
                                DbContext.FreeSql.Update<YssxAnswerOption>().SetSource(rDelSubOption).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                            }
                            //添加
                            if (addOption.Any())
                            {
                                DbContext.FreeSql.Insert<YssxAnswerOption>().AppendData(addOption).ExecuteAffrows();
                            }
                            //更新选项
                            foreach (var item in updateOption)
                            {
                                DbContext.FreeSql.Update<YssxAnswerOption>().SetSource(item).UpdateColumns(a => new
                                {
                                    a.AnswerOption,
                                    a.Name,
                                    a.AnswerFileUrl,
                                    a.Sort,
                                    a.UpdateBy,
                                    a.UpdateTime
                                }).ExecuteAffrows();
                            }
                            #endregion
                        }
                        #endregion

                        //添加分录题内容
                        if (rIsHaveAccountEntry)
                        {
                            if (rCertificateTopicIdData.Count > 0)
                                DbContext.FreeSql.Delete<YssxCertificateDataRecord>().Where(x => rCertificateTopicIdData.Contains(x.CertificateTopicId)).ExecuteAffrows();
                            if (rAddAccountEntryData.Count > 0)
                                DbContext.FreeSql.Insert<YssxCertificateTopic>().AppendData(rAddAccountEntryData).ExecuteAffrows();
                            if (rUpdAccountEntryData.Count > 0)
                            {
                                foreach (var itemUpd in rUpdAccountEntryData)
                                {
                                    DbContext.FreeSql.Update<YssxCertificateTopic>().SetSource(itemUpd).UpdateColumns(a => new
                                    {
                                        a.IsDisableAM,
                                        a.IsDisableCashier,
                                        a.IsDisableAuditor,
                                        a.IsDisableCreator,
                                        a.AccountingManager,
                                        a.CalculationScoreType,
                                        a.InVal,
                                        a.OutVal,
                                        a.Auditor,
                                        a.Cashier,
                                        a.Creator
                                    }).ExecuteAffrows();
                                }
                            }
                            if (rYssxCertificateData.Count > 0)
                                DbContext.FreeSql.Insert<YssxCertificateDataRecord>().AppendData(rYssxCertificateData).ExecuteAffrows();
                        }
                    });
                    #endregion
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        #endregion

        #region 获取题目信息

        /// <summary>
        /// 获取指定题目
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TopicInfoDto>> GetQuestion(long id)
        {
            return await Task.Run(() =>
            {
                var question = DbContext.FreeSql.Select<YssxTopic>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (question == null)
                {
                    return new ResponseContext<TopicInfoDto> { Code = CommonConstants.ErrorCode, Msg = "未找到相关题目" };
                }

                var questionDTO = question.MapTo<TopicInfoDto>();

                if (questionDTO.QuestionType == QuestionType.SingleChoice
                    || questionDTO.QuestionType == QuestionType.MultiChoice
                    || questionDTO.QuestionType == QuestionType.Judge)
                {
                    //简单题型,设置答案
                    //questionDTO.AnswerValue = _optionRep.First(a => a.TopicId == id && a.IsAnswer)?.AnswerOption;
                    //questionDTO.AnswerValue = questionDTO.AnswerValue;
                    //添加选项信息
                    var options = DbContext.FreeSql.Select<YssxAnswerOption>().Where(m => m.TopicId == id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                    questionDTO.Options = new List<ChoiceOption>();
                    foreach (var item in options)
                    {
                        questionDTO.Options.Add(new ChoiceOption()
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Text = item.AnswerOption,
                            AttatchImgUrl = item.AnswerFileUrl
                        });
                    }
                }
                else
                {
                    //复杂题型
                    //questionDTO.AnswerValue = _optionRep.First(a => a.TopicId == id)?.AnswerKeysContent;
                    //添加附件信息
                    questionDTO.QuestionFile = new List<QuestionFile>();

                    if (!string.IsNullOrEmpty(question.TopicFileIds))
                    {
                        var fileIds = question.TopicFileIds.SplitToArray<long>(',');
                        var files = DbContext.FreeSql.Select<YssxTopicFile>().Where(a => fileIds.Contains(a.Id) && a.IsDelete == CommonConstants.IsNotDelete).OrderBy(a => a.Sort).ToList();
                        foreach (var item in files)
                        {
                            questionDTO.QuestionFile.Add(new QuestionFile()
                            {
                                Url = item.Url,
                                Name = item.Name
                                //Sort = item.Sort
                            });
                        }
                    }
                    if (questionDTO.QuestionType == QuestionType.MainSubQuestion)
                    {
                        var subQuestions = DbContext.FreeSql.Select<YssxTopic>().Where(m => m.ParentId == questionDTO.Id && m.IsDelete == CommonConstants.IsNotDelete).OrderBy(a => a.Sort)
                            .ToList(a => new SubQuestion(a.Id, a.QuestionType, a.QuestionContentType, "", a.Hint, a.Content, a.TopicContent, a.FullContent, a.AnswerValue, a.Sort, a.Score, a.PartialScore, a.CalculationType));
                        var choiceQuestionIds = subQuestions.Where(a => a.QuestionType == QuestionType.SingleChoice
                            || a.QuestionType == QuestionType.MultiChoice || a.QuestionType == QuestionType.Judge).Select(a => a.Id).ToArray();
                        if (choiceQuestionIds.Any())
                        {
                            var optionAll = DbContext.FreeSql.Select<YssxAnswerOption>().Where(a => choiceQuestionIds.Contains(a.TopicId) && a.IsDelete == CommonConstants.IsNotDelete).ToList();
                            subQuestions.ForEach(a =>
                            {
                                var options = optionAll.Where(c => c.TopicId == a.Id).Select(d => new ChoiceOption()
                                {
                                    Id = d.Id,
                                    Name = d.Name,
                                    Text = d.AnswerOption,
                                    AttatchImgUrl = d.AnswerFileUrl
                                });
                                if (options.Any())
                                {
                                    a.Options = new List<ChoiceOption>();
                                    a.Options.AddRange(options);
                                }
                            });
                        }
                        //查询分录题相关数据
                        if (subQuestions.Any(m => m.QuestionType == QuestionType.AccountEntry))
                        {
                            subQuestions.ForEach(a =>
                            {
                                if (a.QuestionType == QuestionType.AccountEntry)
                                {
                                    var rCertificateTopic = DbContext.FreeSql.Select<YssxCertificateTopic>().Where(m => m.TopicId == a.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                                    var rDataRecord = DbContext.FreeSql.Select<YssxCertificateDataRecord>().Where(m => m.CertificateTopicId == rCertificateTopic.Id && m.IsDelete == CommonConstants.IsNotDelete).ToList(m => new CertificateDataRecord
                                    {
                                        SummaryInfo = m.SummaryInfo,
                                        SubjectId = m.SubjectId,
                                        BorrowAmount = m.BorrowAmount,
                                        CreditorAmount = m.CreditorAmount
                                    });
                                    a.CertificateNo = rCertificateTopic.CertificateNo;
                                    a.CertificateDate = rCertificateTopic.CertificateDate;
                                    a.DataRecords = rDataRecord;
                                    //2019 12 27注释
                                    a.AccountingManager = rCertificateTopic.AccountingManager;
                                    a.Cashier = rCertificateTopic.Cashier;
                                    a.Auditor = rCertificateTopic.Auditor;
                                    a.Creator = rCertificateTopic.Creator;
                                    a.IsDisableAM = rCertificateTopic.IsDisableAM;
                                    a.IsDisableCashier = rCertificateTopic.IsDisableCashier;
                                    a.IsDisableAuditor = rCertificateTopic.IsDisableAuditor;
                                    a.IsDisableCreator = rCertificateTopic.IsDisableCreator;
                                    a.CalculationScoreType = rCertificateTopic.CalculationScoreType;
                                    a.OutVal = rCertificateTopic.OutVal;
                                    a.InVal = rCertificateTopic.InVal;
                                    // add 2019 12 27
                                    a.CertificateTopic = DbContext.FreeSql.GetRepository<YssxCertificateTopic>().Select.Where(t => t.TopicId == a.Id && t.IsDelete == CommonConstants.IsNotDelete)
                                    .Select(t => new CertificateTopicView())
                                    .First($"{CommonConstants.Cache_GetCertificateTopicByTopicId}{a.Id}", true, 10 * 60);
                                }
                            });
                        }

                        questionDTO.SubQuestion = new List<SubQuestion>();
                        questionDTO.SubQuestion.AddRange(subQuestions);
                    }
                    //add 2019 12 27
                    else if (question.QuestionType == QuestionType.AccountEntry)//分录题
                    {
                        questionDTO.CertificateTopic = DbContext.FreeSql.GetRepository<YssxCertificateTopic>().Select.Where(s => s.TopicId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                            .Select(s => new CertificateTopicView())
                            .First($"{CommonConstants.Cache_GetCertificateTopicByTopicId}{id}", true, 10 * 60);
                    }
                }
                return new ResponseContext<TopicInfoDto> { Code = CommonConstants.SuccessCode, Data = questionDTO };
            });
        }

        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TopicStatisticsView>> GetQuestionByCase(GetQuestionByCaseRequest model)
        {
            return await Task.Run(() =>
            {
                if (model.CaseId <= 0)
                {
                    return new ResponseContext<TopicStatisticsView> { Code = CommonConstants.ErrorCode, Msg = "案例ID为空" };
                }

                var selectData = DbContext.FreeSql.GetRepository<YssxTopic>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CaseId == model.CaseId && m.ParentId == 0).OrderBy(m => m.Sort);
                #region 搜索条件
                if (!string.IsNullOrEmpty(model.Title))
                {
                    selectData = selectData.Where(m => m.Title.Contains(model.Title));
                }
                if (model.PositionId.HasValue && model.PositionId > 0)
                {
                    selectData = selectData.Where(m => m.PositionId == model.PositionId.Value);
                }
                if (model.QuestionType.HasValue)
                {
                    selectData = selectData.Where(m => m.QuestionType == (QuestionType)model.QuestionType.Value);
                }
                if (model.Status.HasValue)
                {
                    selectData = selectData.Where(m => m.Status == (Status)model.Status.Value);
                }
                if (model.BeginDate.HasValue)
                {
                    selectData = selectData.Where(m => m.CreateTime >= model.BeginDate.Value);
                }
                if (model.EndDate.HasValue)
                {
                    selectData = selectData.Where(m => m.CreateTime <= model.EndDate.Value);
                }
                #endregion
                //查询题目数据
                var totalCount = selectData.Count();
                var sql = selectData.Page(model.PageIndex, model.PageSize).ToSql("Id, Title, PositionId, IsGzip, Content, QuestionType, Education, Score, PartialScore, Status, CreateTime, UpdateTime, Sort");
                var topicData = DbContext.FreeSql.Ado.Query<TopicListView>(sql).ToList();
                //查询岗位名称、凭证单据张数
                var rTopicIdArr = topicData.Select(m => m.Id).ToList();
                var rPositionIdArr = topicData.Select(m => m.PositionId).ToList();
                var rPositionNameData = DbContext.FreeSql.GetRepository<YssxPosition>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && rPositionIdArr.Contains(m.Id)).ToList();
                var rCertificateData = DbContext.FreeSql.GetRepository<YssxCertificateTopic>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && rTopicIdArr.Contains(m.Id)).ToList();
                foreach (var item in topicData)
                {
                    if (rPositionNameData.Count > 0 && item.PositionId > 0)
                    {
                        item.PositionName = rPositionNameData.Any(m => m.Id == item.PositionId) ? rPositionNameData.First(m => m.Id == item.PositionId).Name : "";
                    }
                    if (rCertificateData.Count > 0 && item.Id > 0)
                    {
                        item.Certificate = rCertificateData.Any(m => m.TopicId == item.Id) ? rCertificateData.First(m => m.TopicId == item.Id).InvoicesNum : 0;
                    }
                }
                //返回数据
                var rTopicData = DbContext.FreeSql.GetRepository<YssxTopic>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CaseId == model.CaseId && m.ParentId == 0);
                var rTopicPositionData = DbContext.FreeSql.GetRepository<YssxTopic>().Select.From<YssxPosition>((a, b) => a.InnerJoin(aa => aa.PositionId == b.Id))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && a.CaseId == model.CaseId && a.ParentId == 0)
                    .GroupBy((a, b) => new { a.PositionId, b.Name }).ToList(a => new TopicPositionView
                    {
                        Id = a.Key.PositionId,
                        Name = a.Key.Name,
                        Score = a.Sum(a.Value.Item1.Score)
                    });

                TopicStatisticsView res = new TopicStatisticsView();
                res.Detail = new PageResponse<TopicListView> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = topicData };
                res.TotalCount = rTopicData.Count();
                res.TotalScore = rTopicData.Sum(m => m.Score);
                res.PositionQuestionDetail = rTopicPositionData;

                return new ResponseContext<TopicStatisticsView> { Code = CommonConstants.SuccessCode, Data = res };
            });
        }

        /// <summary>
        /// 获取图表题导入的EXCEL数据
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<TopicFillGridDto>>> GetQuestionInFillGrid(long caseId)
        {
            return await Task.Run(() =>
            {
                List<TopicFillGridDto> res = new List<TopicFillGridDto>();
                var question = DbContext.FreeSql.Select<YssxTopicFillGrid>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CaseId == caseId)
                    .OrderByDescending(m => m.CreateTime).ToList(a => new TopicFillGridDto
                    {
                        Id = a.Id,
                        Name = a.Name,
                        Content = a.Content
                    });
                if (question.Count > 1)
                {
                    res.Add(question.First());
                }
                return new ResponseContext<List<TopicFillGridDto>> { Code = CommonConstants.SuccessCode, Data = res };
            });
        }

        /// <summary>
        /// 查询缓存题目列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<TopicListView>>> GetQuestionByCache(long caseId)
        {
            return await Task.Run(() =>
            {
                if (caseId <= 0)
                {
                    return new ResponseContext<List<TopicListView>> { Code = CommonConstants.ErrorCode, Msg = "案例ID为空" };
                }
                List<TopicListView> topicData = new List<TopicListView>();
                //查询题目数据
                var question = DbContext.FreeSql.Select<YssxTopic>().Where(m => m.ParentId == -1 && m.CaseId == caseId && m.IsDelete == CommonConstants.IsNotDelete).OrderBy(m => m.Sort).ToList();
                if (question.Count > 0)
                {
                    topicData = question.MapTo<List<TopicListView>>();
                    //查询岗位名称、凭证单据张数
                    var rTopicIdArr = topicData.Select(m => m.Id).ToList();
                    var rPositionIdArr = topicData.Select(m => m.PositionId).ToList();
                    var rPositionNameData = DbContext.FreeSql.GetRepository<YssxPosition>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && rPositionIdArr.Contains(m.Id)).ToList();
                    var rCertificateData = DbContext.FreeSql.GetRepository<YssxCertificateTopic>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && rTopicIdArr.Contains(m.Id)).ToList();
                    foreach (var item in topicData)
                    {
                        if (rPositionNameData.Count > 0 && item.PositionId > 0)
                        {
                            item.PositionName = rPositionNameData.Any(m => m.Id == item.PositionId) ? rPositionNameData.First(m => m.Id == item.PositionId).Name : "";
                        }
                        if (rCertificateData.Count > 0 && item.Id > 0)
                        {
                            item.Certificate = rCertificateData.Any(m => m.TopicId == item.Id) ? rCertificateData.First(m => m.TopicId == item.Id).InvoicesNum : 0;
                        }
                    }
                }
                return new ResponseContext<List<TopicListView>> { Code = CommonConstants.SuccessCode, Data = topicData };
            });
        }

        #endregion

        #region 删除题目、选项、文件
        /// <summary>
        /// 删除题目
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteQuestion(long id, UserTicket CurrentUser)
        {
            return await Task.Run(() =>
            {
                long opreationId = CurrentUser.Id;//操作人ID
                DateTime dtNow = DateTime.Now;
                //校验
                //TODO-题目删除限制，临时去掉！！！！
                var any = DbContext.FreeSql.Select<ExamStudentGradeDetail>().Any(a => a.QuestionId == id && a.IsDelete == CommonConstants.IsNotDelete);
                if (any)
                {
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "该题目已经被用户使用，无法删除！", false);
                }
                #region 取数据【单独的分录题调单独的删除接口】
                //取题目
                var question = DbContext.FreeSql.Select<YssxTopic>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (question == null) { return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到题目信息" }; }
                question.IsDelete = CommonConstants.IsDelete;
                question.UpdateBy = opreationId;
                question.UpdateTime = dtNow;
                //取选项
                var option = new List<YssxAnswerOption>();
                if (question.QuestionType == QuestionType.SingleChoice || question.QuestionType == QuestionType.MultiChoice || question.QuestionType == QuestionType.Judge)
                {
                    option = DbContext.FreeSql.Select<YssxAnswerOption>().Where(m => m.TopicId == id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                    if (option.Count == 0) { return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到选项信息" }; }
                    option.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                }
                //取子题目（题目列表、题目选项、题目附件）
                var subQuestion = new List<YssxTopic>();
                var subOptionList = new List<YssxAnswerOption>();
                var subTopicFileList = new List<YssxTopicFile>();
                var subCertificateTopicList = new List<YssxCertificateTopic>();
                var rCertificateTopic = new YssxCertificateTopic();
                //分录题
                if (question.QuestionType == QuestionType.AccountEntry)
                {
                    //凭证题目
                    rCertificateTopic = DbContext.FreeSql.Select<YssxCertificateTopic>().Where(m => m.TopicId == question.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (rCertificateTopic != null)
                    {
                        rCertificateTopic.IsDelete = CommonConstants.IsDelete;
                        rCertificateTopic.UpdateBy = opreationId;
                        rCertificateTopic.UpdateTime = dtNow;
                    }
                    //凭证数据记录
                }
                //综合题
                else if (question.QuestionType == QuestionType.MainSubQuestion)
                {
                    subQuestion = DbContext.FreeSql.Select<YssxTopic>().Where(m => m.ParentId == id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                    if (subQuestion.Count == 0) { return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到子题目信息" }; }
                    foreach (var itemSub in subQuestion)
                    {
                        itemSub.IsDelete = CommonConstants.IsDelete;
                        itemSub.UpdateBy = opreationId;
                        itemSub.UpdateTime = dtNow;

                        //取子题目 - 选项
                        if (itemSub.QuestionType == QuestionType.SingleChoice || itemSub.QuestionType == QuestionType.MultiChoice || itemSub.QuestionType == QuestionType.Judge)
                        {
                            //简单题
                            var subOption = DbContext.FreeSql.Select<YssxAnswerOption>().Where(m => m.TopicId == itemSub.Id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                            if (subOption.Count > 0) subOptionList.AddRange(subOption);
                        }
                        else if (itemSub.QuestionType == QuestionType.AccountEntry)
                        {
                            //分录题
                            //凭证题目
                            var subCertificateTopic = DbContext.FreeSql.Select<YssxCertificateTopic>().Where(m => m.TopicId == itemSub.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                            if (subCertificateTopic != null)
                            {
                                subCertificateTopic.IsDelete = CommonConstants.IsDelete;
                                subCertificateTopic.UpdateBy = opreationId;
                                subCertificateTopic.UpdateTime = dtNow;
                                subCertificateTopicList.Add(subCertificateTopic);
                            }
                            //凭证数据记录
                        }
                        //取文件
                        if (!string.IsNullOrEmpty(itemSub.TopicFileIds))
                        {
                            var idArr = itemSub.TopicFileIds.Split(',').Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                            if (idArr.Any())
                            {
                                var subTopicFile = DbContext.FreeSql.Select<YssxTopicFile>().Where(m => idArr.Contains(m.Id) && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                                if (subTopicFile.Count > 0) subTopicFileList.AddRange(subTopicFile);
                            }
                        }
                    }
                    //更新字段
                    if (subOptionList.Count > 0)
                    {
                        subOptionList.ForEach(a =>
                        {
                            a.IsDelete = CommonConstants.IsDelete;
                            a.UpdateBy = opreationId;
                            a.UpdateTime = dtNow;
                        });
                    }
                    if (subTopicFileList.Count > 0)
                    {
                        subTopicFileList.ForEach(a =>
                        {
                            a.IsDelete = CommonConstants.IsDelete;
                            a.UpdateBy = opreationId;
                            a.UpdateTime = dtNow;
                        });
                    }
                }
                //取文件
                var topicFile = new List<YssxTopicFile>();
                if (!string.IsNullOrEmpty(question.TopicFileIds))
                {
                    var idArr = question.TopicFileIds.Split(',').Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                    if (idArr.Any())
                    {
                        topicFile = DbContext.FreeSql.Select<YssxTopicFile>().Where(m => idArr.Contains(m.Id) && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                    }
                    if (topicFile.Count > 0)
                    {
                        topicFile.ForEach(a =>
                        {
                            a.IsDelete = CommonConstants.IsDelete;
                            a.UpdateBy = opreationId;
                            a.UpdateTime = dtNow;
                        });
                    }
                }
                #endregion
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //删除题目
                    DbContext.FreeSql.Update<YssxTopic>().SetSource(question).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    //删除选项
                    if (question.QuestionType == QuestionType.SingleChoice || question.QuestionType == QuestionType.MultiChoice || question.QuestionType == QuestionType.Judge)
                    {
                        DbContext.FreeSql.Update<YssxAnswerOption>().SetSource(option).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    }
                    //删除文件
                    if (topicFile.Count > 0)
                    {
                        DbContext.FreeSql.Update<YssxTopicFile>().SetSource(topicFile).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    }
                    //删除【分录题】
                    if (question.QuestionType == QuestionType.AccountEntry)
                    {
                        //删除分录题·凭证题目
                        if (rCertificateTopic != null)
                        {
                            DbContext.FreeSql.Update<YssxCertificateTopic>().SetSource(rCertificateTopic).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                            //删除分录题·凭证数据记录
                            DbContext.FreeSql.Delete<YssxCertificateDataRecord>().Where(x => x.CertificateTopicId == rCertificateTopic.Id).ExecuteAffrows();
                        }
                    }
                    //删除【综合题】
                    else if (question.QuestionType == QuestionType.MainSubQuestion)
                    {
                        //删除题目
                        DbContext.FreeSql.Update<YssxTopic>().SetSource(subQuestion).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                        //删除简单题·选项
                        if (subOptionList.Count > 0)
                            DbContext.FreeSql.Update<YssxAnswerOption>().SetSource(subOptionList).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                        //删除分录题·凭证题目
                        if (subCertificateTopicList.Count > 0)
                            DbContext.FreeSql.Update<YssxCertificateTopic>().SetSource(subCertificateTopicList).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                        //删除分录题·凭证数据记录
                        var rCertificateTopicIdData = subCertificateTopicList.Select(m => m.Id).ToList();
                        if (rCertificateTopicIdData.Count > 0)
                            DbContext.FreeSql.Delete<YssxCertificateDataRecord>().Where(x => rCertificateTopicIdData.Contains(x.CertificateTopicId)).ExecuteAffrows();
                        //删除文件
                        if (subTopicFileList.Count > 0)
                            DbContext.FreeSql.Update<YssxTopicFile>().SetSource(subTopicFileList).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    }
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 删除题目附件
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTopicFile(long id, UserTicket CurrentUser)
        {
            long opreationId = CurrentUser.Id;//操作人ID

            var entity = DbContext.FreeSql.Select<YssxTopicFile>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息" };

            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = opreationId;
            entity.UpdateTime = DateTime.Now;
            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<YssxTopicFile>().SetSource(entity).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
        /// <summary>
        /// 删除选项
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteOption(long topicId, long id, UserTicket CurrentUser)
        {
            long opreationId = CurrentUser.Id;//操作人ID

            var entity = DbContext.FreeSql.Select<YssxAnswerOption>().Where(m => m.TopicId == topicId && m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息" };

            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = opreationId;
            entity.UpdateTime = DateTime.Now;
            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<YssxAnswerOption>().SetSource(entity).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 设置题目状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetQuestionStatuts(long topicId, int status, UserTicket CurrentUser)
        {
            long opreationId = CurrentUser.Id;//操作人ID

            if (topicId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，入参信息有误" };
            var topic = DbContext.FreeSql.Select<YssxTopic>().Where(m => m.Id == topicId && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (topic == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息" };

            topic.Status = (Status)status;
            topic.UpdateBy = opreationId;
            topic.UpdateTime = DateTime.Now;
            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<YssxTopic>().SetSource(topic).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.Status }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        ///// <summary>
        ///// 模板导入
        ///// </summary>
        ///// <returns></returns>
        //public async Task<ResponseContext<DataTable>> ImportExcelData(List<HttpFormFile> form)
        //{
        //    //if (file == null)
        //    //{
        //    //    return new ResponseContext<DataTable> { Code = CommonConstants.ErrorCode, Msg = "上传失败" };
        //    //}
        //    //var coreSubjects = new DataTable();
        //    //using (var ms = file.OpenReadStream())
        //    //{
        //    //    coreSubjects = NPOIHelper<DataTable>.ExcelToDataTable(ms, true);
        //    //}

        //    return await Task.Run(() =>
        //    {
        //        var file = form?.FirstOrDefault();
        //        if (file == null)
        //        {
        //            return new ResponseContext<DataTable> { Code = CommonConstants.ErrorCode, Msg = "上传失败" };
        //        }
        //        DataTable coreSubjects = new DataTable();
        //        using (var ms = new MemoryStream(file.File))
        //        {
        //            coreSubjects = NPOIHelper<DataTable>.ExcelToDataTable(ms, true);
        //        }
        //        return new ResponseContext<DataTable> { Code = CommonConstants.SuccessCode, Data = coreSubjects };
        //    });
        //}

        #endregion

        #region 修改题目信息 - 案例操作
        /// <summary>
        /// 案例修改分值题目获取
        /// </summary>
        public async Task<ResponseContext<List<TopicInfoDto>>> GetQuestionByCaseId(long CaseId)
        {
            return await Task.Run(() =>
            {
                var questions = DbContext.FreeSql.Select<YssxTopic>().Where(m => m.CaseId == CaseId && m.CalculationType == CalculationType.FreeCalculation && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                if (questions == null)
                {
                    return new ResponseContext<List<TopicInfoDto>> { Code = CommonConstants.ErrorCode, Msg = "未找到相关题目" };
                }

                var questionDTO = questions.MapTo<List<TopicInfoDto>>();


                return new ResponseContext<List<TopicInfoDto>> { Code = CommonConstants.SuccessCode, Data = questionDTO };
            });
        }

        /// <summary>
        /// 案例修改分值批量修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpdateQuestionByCaseId(List<TopicInfoDto> model)
        {
            return await Task.Run(() =>
            {
                var topic = model.MapTo<List<YssxTopic>>();
                int state = DbContext.FreeSql.Update<YssxTopic>().SetSource(topic).UpdateColumns(m => new { m.AnswerValue, m.Score }).ExecuteAffrows();


                return new ResponseContext<bool> { Code = state > 0 ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state > 0 };
            });

        }

        /// <summary>
        /// 一键修改分数
        /// </summary>
        /// <param name="caseId"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetQuetionScore(long caseId, decimal x)
        {

            return await Task.Run(() =>
            {
                List<YssxTopic> tops = DbContext.FreeSql.Select<YssxTopic>().Where(n => n.CaseId == caseId && n.CalculationType != CalculationType.FreeCalculation).ToList();

                tops.ForEach(m =>
                {
                    m.Score = Math.Round(m.Score * x, 2, MidpointRounding.AwayFromZero);
                });
                int state = DbContext.FreeSql.Update<YssxTopic>().SetSource(tops).UpdateColumns(m => new { m.Score }).ExecuteAffrows();

                return new ResponseContext<bool> { Code = state > 0 ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state > 0 };
            });

        }
        #endregion

    }
}
