﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.Repository.Extensions;
using Yssx.S.Dto;
using Yssx.S.Dto.Task;
using Yssx.S.IServices.Topics;
using Yssx.S.Poco;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Exam;

namespace Yssx.S.ServiceImpl.Topics
{
    /// <summary>
    /// 课业任务管理
    /// </summary>
    public class TaskService : ITaskService
    {
        #region 课业任务

        #region old
        /// <summary>
        /// 查询课业任务列表 - 教师端
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<TaskView>> GetTaskByPage(GetTaskByPageRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                //搜索班级
                var rSearchTaskId = new List<long>();
                if (model.ClassId > 0)
                {
                    rSearchTaskId = DbContext.FreeSql.GetRepository<YssxTaskStudent>()
                        .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.ClassId == model.ClassId).GroupBy(a => a.TaskId).ToList(m => m.Key);
                }
                //查询数据
                //删除的备课课程下发布的任务还要可见 2020.03.19
                var selectData = DbContext.FreeSql.GetGuidRepository<YssxTask>().Select.From<YssxPrepareCourse>((a, b) => a.LeftJoin(aa => aa.CourseId == b.Id))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.CreateBy == currentUserId)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b) => a.Name.Contains(model.Name))
                    .WhereIf(model.CourseId > 0, (a, b) => a.CourseId == model.CourseId)
                    .WhereIf(rSearchTaskId.Count > 0, (a, b) => rSearchTaskId.Contains(a.Id))
                    .OrderByDescending((a, b) => a.Sort).ToList((a, b) => new TaskView
                    {
                        Id = a.Id,
                        Name = a.Name,
                        TaskType = a.TaskType,
                        CourseId = a.CourseId,
                        CourseName = b.CourseName,
                        OriginalCourseId = a.OriginalCourseId,
                        StartTime = a.StartTime,
                        CloseTime = a.CloseTime,
                        TotalMinutes = a.TotalMinutes,
                        Status = a.Status,
                        Content = a.Content,
                        Sort = a.Sort
                    });
                #region 搜索条件
                selectData = model.TaskType > -1 ? selectData.Where(a => a.TaskType == (TaskType)model.TaskType).ToList() : selectData.Where(a => a.TaskType != TaskType.ClassTest).ToList();
                if (model.Status != -2)
                    selectData = selectData.Where(a => a.ExamStatus == (LessonsTaskStatus)model.Status).ToList();
                if (model.IsBeginTime == CommonConstants.IsYes)
                {
                    if (model.BeginDate.HasValue)
                    {
                        var rBeginDate = Convert.ToDateTime(model.BeginDate.Value.ToString("yyyy-MM-dd 00:00:01"));
                        selectData = selectData.Where(a => a.StartTime >= rBeginDate).ToList();
                    }
                    if (model.EndDate.HasValue)
                    {
                        var rEndDate = Convert.ToDateTime(model.EndDate.Value.ToString("yyyy-MM-dd 23:59:59"));
                        selectData = selectData.Where(a => a.StartTime <= rEndDate).ToList();
                    }
                }
                else
                {
                    if (model.BeginDate.HasValue)
                    {
                        var rBeginDate = Convert.ToDateTime(model.BeginDate.Value.ToString("yyyy-MM-dd 00:00:01"));
                        selectData = selectData.Where(a => a.CloseTime >= rBeginDate).ToList();
                    }
                    if (model.EndDate.HasValue)
                    {
                        var rEndDate = Convert.ToDateTime(model.EndDate.Value.ToString("yyyy-MM-dd 23:59:59"));
                        selectData = selectData.Where(a => a.CloseTime <= rEndDate).ToList();
                    }
                }
                #endregion
                var totalCount = selectData.Count();
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                //查询任务的班级列表和学生数据
                var rTaskIdArr = selectData.Select(m => m.Id).ToList();
                var rTaskStudentData = DbContext.FreeSql.GetRepository<YssxTaskStudent>().Select.From<YssxClass>((a, b) => a.InnerJoin(aa => aa.ClassId == b.Id))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && rTaskIdArr.Contains(a.TaskId)).ToList((a, b) => new
                    {
                        TaskId = a.TaskId,
                        ClassId = a.ClassId,
                        ClassName = b.Name,
                        UserId = a.UserId
                    });
                foreach (var item in selectData)
                {
                    var rClassDetailData = new List<TaskClassView>();
                    if (rTaskStudentData.Count > 0 && item.Id > 0 && rTaskStudentData.Any(m => m.TaskId == item.Id))
                    {
                        rClassDetailData = rTaskStudentData.Where(m => m.TaskId == item.Id).GroupBy(x => new { x.ClassId, x.ClassName }).Select(m =>
                            new TaskClassView
                            {
                                ClassId = m.Key.ClassId,
                                ClassName = m.Key.ClassName,
                                StudentCount = m.Count()
                            }).ToList();
                    }
                    item.ClassDetail = rClassDetailData;
                    item.TotalStudentCount = rClassDetailData.Sum(m => m.StudentCount);
                    item.SubmitCount = 0;
                }

                return new PageResponse<TaskView> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 查询课业任务列表 - 学生端
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<PageResponse<StudentTaskView>> GetStudentTaskByPage(GetTaskByPageRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                //删除的备课课程下发布的任务还要可见 2020.03.19
                var selectData = DbContext.FreeSql.GetGuidRepository<YssxTask>().Select
                    .From<YssxPrepareCourse, YssxTaskStudent, YssxUser, ExamCourseUserGrade>((a, b, c, d, e) => 
                        a.LeftJoin(aa => aa.CourseId == b.Id)
                        .InnerJoin(aa => aa.Id == c.TaskId && c.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => c.UserId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => aa.Id == e.TaskId && e.UserId == currentUserId && e.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d, e) => a.IsDelete == CommonConstants.IsNotDelete && d.Id == currentUserId)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b, c, d, e) => a.Name.Contains(model.Name))
                    .WhereIf(model.CourseId > 0, (a, b, c, d, e) => a.CourseId == model.CourseId)
                    .OrderByDescending((a, b, c, d, e) => a.CreateTime).ToList((a, b, c, d, e) => new StudentTaskView
                    {
                        Id = a.Id,
                        Name = a.Name,
                        TaskType = a.TaskType,
                        CourseId = a.CourseId,
                        CourseName = b.CourseName,
                        OriginalCourseId = a.OriginalCourseId,
                        StartTime = a.StartTime,
                        CloseTime = a.CloseTime,
                        TotalMinutes = a.TotalMinutes,
                        Status = e.Status == StudentExamStatus.End ? LessonsTaskStatus.End : a.Status,
                        Content = a.Content,
                        Sort = a.Sort,
                        ExamId = a.ExamId,
                        GradeId = e.Id,
                        ErrorCount = e.ErrorCount,
                        ExamPaperScore = e.ExamPaperScore,
                        Score = e.Score
                    });
                selectData = model.TaskType > -1 ? selectData.Where(a => a.TaskType == (TaskType)model.TaskType).ToList() : selectData.Where(a => a.TaskType != TaskType.ClassTest).ToList();
                if (model.Status != -2)
                    selectData = selectData.Where(a => a.ExamStatus == (LessonsTaskStatus)model.Status).ToList();
                var totalCount = selectData.Count();
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                return new PageResponse<StudentTaskView> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 查询指定课业任务信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TaskInfoDto>> GetTaskById(long taskId, long currentUserId)
        {
            return await Task.Run(() =>
            {
                YssxTask taskEntity = DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == taskId && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (taskEntity == null)
                    return new ResponseContext<TaskInfoDto> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该数据!" };
                var rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == taskEntity.ExamId).First();
                if (rPaperEntity == null)
                    return new ResponseContext<TaskInfoDto> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };
                var rPrepareCourseEntity = DbContext.FreeSql.Select<YssxPrepareCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Id == taskEntity.CourseId).First();
                if (rPrepareCourseEntity == null)
                    return new ResponseContext<TaskInfoDto> { Code = CommonConstants.ErrorCode, Msg = "任务关联的备课课程找不到" };

                var taskInfoData = taskEntity.MapTo<TaskInfoDto>();
                taskInfoData.TotalScore = rPaperEntity.TotalScore;
                taskInfoData.CourseName = rPrepareCourseEntity.CourseName;
                //查询题目列表
                var rTopicDetail = DbContext.FreeSql.GetRepository<YssxTaskTopic>().Select
                    .From<YssxTopicPublic>((a, b) => a.InnerJoin(aa => aa.TopicId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.TaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete).OrderBy((a, b) => a.Sort).ToList((a, b) => new TaskTopicDto
                    {
                        Id = a.Id,
                        TopicId = a.TopicId,
                        QuestionType = a.QuestionType,
                        Score = a.Score,
                        Sort = a.Sort,
                        Title = b.Title
                    });
                taskInfoData.TopicDetail = rTopicDetail;
                //筛选题目分组信息
                var rTopicGroupDetail = rTopicDetail.GroupBy(m => m.QuestionType).Select(a => new TaskTopicGroupDto
                {
                    QuestionType = a.Key,
                    Count = a.Count(),
                    TotalScore = a.Sum(b => b.Score)
                }).ToList();
                taskInfoData.TopicGroupDetail = rTopicGroupDetail;
                //查询班级学生列表
                var rStudentData = DbContext.FreeSql.GetRepository<YssxTaskStudent>().Select
                    .From<YssxClass, YssxUser, YssxStudent>((a, b, c, d) => a.InnerJoin(aa => aa.ClassId == b.Id).InnerJoin(aa => aa.UserId == c.Id).LeftJoin(aa => aa.UserId == d.UserId))
                    .Where((a, b, c, d) => a.TaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete).ToList((a, b, c, d) => new TaskStudentDto
                    {
                        TaskId = a.TaskId,
                        ClassId = a.ClassId,
                        ClassName = b.Name,
                        UserId = a.UserId,
                        UserName = c.RealName,
                        StudentId = a.StudentId,
                        StudentNo = d.StudentNo
                    });
                var rClassDetail = rStudentData.GroupBy(m => new { m.ClassId, m.ClassName }).Select(a => new TaskInfoClassDto
                {
                    ClassId = a.Key.ClassId,
                    ClassName = a.Key.ClassName,
                    StudentCount = a.Count()
                }).ToList();
                foreach (var itemClass in rClassDetail)
                {
                    itemClass.ClassName = DbContext.FreeSql.GetRepository<YssxClass>().Where(x => x.Id == itemClass.ClassId && x.IsDelete == CommonConstants.IsNotDelete).First(m => m.Name);
                    itemClass.StudentDetail = rStudentData.Where(m => m.ClassId == itemClass.ClassId).Select(m => new TaskInfoStudentDto
                    {
                        UserId = m.UserId,
                        StudentId = m.StudentId,
                        Name = m.UserName,
                        StudentNo = m.StudentNo
                    }).ToList();
                }
                taskInfoData.ClassDetail = rClassDetail;

                return new ResponseContext<TaskInfoDto> { Code = CommonConstants.SuccessCode, Msg = "成功", Data = taskInfoData };
            });
        }

        /// <summary>
        /// 修改课业任务
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EditTask(TaskDto model, long currentUserId)
        {
            var dtNow = DateTime.Now;
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
            if (model.StartTime > model.CloseTime)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "开始时间不得大于结束时间" };
            if (model.CloseTime < dtNow)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "结束时间不得小于当前时间" };
            if (model.TotalMinutes <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "考试时长必须大于0" };
            //if (DbContext.FreeSql.Select<YssxTask>().Any(m => m.Name == model.Name && m.Id != model.Id && m.CreateBy == currentUserId && m.IsDelete == CommonConstants.IsNotDelete))
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "课业任务名称重复" };

            return await Task.Run(() =>
            {
                bool state = true;

                #region 处理数据
                // 处理数据
                ExamPaperCourse rPaperEntity = new ExamPaperCourse();
                var rTaskEntity = DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == model.Id && x.TaskType == model.TaskType && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (rTaskEntity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到该课业任务" };
                if (rTaskEntity.ExamId == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "此课业任务数据异常，试卷ID为0" };
                rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == rTaskEntity.ExamId).First();
                if (rPaperEntity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };
                //课业任务
                rTaskEntity.Name = model.Name;
                rTaskEntity.StartTime = model.StartTime;
                rTaskEntity.CloseTime = model.CloseTime;
                rTaskEntity.TotalMinutes = model.TotalMinutes;
                rTaskEntity.UpdateBy = currentUserId;
                rTaskEntity.UpdateTime = dtNow;
                //试卷
                rPaperEntity.Name = model.Name;
                rPaperEntity.BeginTime = model.StartTime;
                rPaperEntity.EndTime = model.CloseTime;
                rPaperEntity.TotalMinutes = model.TotalMinutes;
                rPaperEntity.UpdateBy = currentUserId;
                rPaperEntity.UpdateTime = dtNow;
                //学生 - 新增

                //1、参数学生列表
                var students = model.ClassDetail;
                var studentUserIds = model.ClassDetail?.Select(s => s.UserId).ToList();
                //2、任务已有学生列表
                var taskStudents = DbContext.FreeSql.GetRepository<YssxTaskStudent>().Where(s => s.TaskId == model.Id && s.IsDelete == CommonConstants.IsNotDelete).ToList();
                var taskStudentsUserIds = taskStudents.Select(s => s.UserId).ToList();
                //查出新增的学生列表
                var addStudents = students.Where(s => !taskStudentsUserIds.Contains(s.UserId)).ToList();
                var rAddClassStudent = addStudents.Select(m => new YssxTaskStudent
                {
                    Id = IdWorker.NextId(),
                    TaskId = rTaskEntity.Id,
                    ClassId = m.ClassId,
                    UserId = m.UserId,
                    StudentId = m.StudentId,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                }).ToList();
                //查出移除的学生列表
                var removeStudents = taskStudents.Where(s => !studentUserIds.Contains(s.UserId)).ToList();
                var removeUserIds = removeStudents.Select(s => s.UserId).ToList();
                //新增的作答记录
                var rAddExamGradeData = addStudents.Select(m => new ExamCourseUserGrade
                {
                    Id = IdWorker.NextId(),
                    ExamId = rPaperEntity.Id,
                    CourseId = rPaperEntity.CourseId,
                    SectionId = rPaperEntity.SectionId,
                    TaskId = rPaperEntity.TaskId,
                    ExamPaperScore = rPaperEntity.TotalScore,
                    //LeftSeconds = rPaperEntity.TotalMinutes * 60,
                    LeftSeconds = model.TotalMinutes * 60,
                    RecordTime = model.StartTime,
                    UserId = m.UserId,
                    TenantId = rTaskEntity.TenantId,
                    StudentId = m.StudentId,
                    Status = StudentExamStatus.Wait,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                }).ToList();
                //移除作答记录的userid集合
                
                
                //作答记录 - 更新
                var rUpdExamGradeData = DbContext.FreeSql.GetRepository<ExamCourseUserGrade>()
                    .Where(x => x.TaskId == model.Id && x.IsDelete == CommonConstants.IsNotDelete).ToList(m => new ExamCourseUserGrade
                    {
                        Id = m.Id,
                        LeftSeconds = model.TotalMinutes * 60,
                        RecordTime = model.StartTime,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    });
                #endregion

                #region 更新数据
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //课业任务
                    DbContext.FreeSql.Update<YssxTask>().SetSource(rTaskEntity).UpdateColumns(a => new
                    {
                        a.Name,
                        a.StartTime,
                        a.CloseTime,
                        a.TotalMinutes,
                        a.UpdateBy,
                        a.UpdateTime
                    }).ExecuteAffrows();
                    //新增学生
                    if (rAddClassStudent.Count > 0)
                        DbContext.FreeSql.Insert<YssxTaskStudent>().AppendData(rAddClassStudent).ExecuteAffrows();
                   
                    if (removeStudents.Any())
                    {
                        //移除学生
                        DbContext.FreeSql.Delete<YssxTaskStudent>().Where(a => removeUserIds.Contains(a.UserId) && a.TaskId==model.Id).ExecuteAffrows();
                        //删除的作答记录
                        //DbContext.FreeSql.Delete<ExamCourseUserGrade>().Where(a => removeUserIds.Contains(a.UserId) && a.TaskId == model.Id);

                        DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().UpdateDiy.Set(c => new ExamCourseUserGrade
                        {
                            IsDelete = CommonConstants.IsDelete,
                            UpdateTime = DateTime.Now
                        }).Where(c => removeUserIds.Contains(c.UserId) && c.TaskId == model.Id).ExecuteAffrows();
                    }

                    //作答记录
                    if (rAddExamGradeData.Count > 0)
                        DbContext.FreeSql.Insert<ExamCourseUserGrade>().AppendData(rAddExamGradeData).ExecuteAffrows();
                    //试卷
                    var resUpd = DbContext.FreeSql.Update<ExamPaperCourse>().SetSource(rPaperEntity).UpdateColumns(a => new
                    {
                        a.Name,
                        a.BeginTime,
                        a.EndTime,
                        a.TotalMinutes,
                        a.UpdateBy,
                        a.UpdateTime
                    }).ExecuteAffrows();
                    //作答记录 - 更新
                    if (rUpdExamGradeData.Count > 0)
                        DbContext.FreeSql.Update<ExamCourseUserGrade>().SetSource(rUpdExamGradeData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.LeftSeconds, a.RecordTime }).ExecuteAffrows();

                    state = resUpd > 0;
                    uow.Commit();
                }
                #endregion
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        #endregion

        #region new
        /// <summary>
        /// 查询课业任务列表 - 教师端 - new
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<TaskView>> GetTaskByPageAll(GetTaskByPageAllRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                //搜索班级
                var rSearchTaskId = new List<long>();
                if (model.ClassId > 0)
                {
                    rSearchTaskId = DbContext.FreeSql.GetRepository<YssxTaskStudent>()
                        .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.ClassId == model.ClassId).GroupBy(a => a.TaskId).ToList(m => m.Key);
                }
                //查询数据
                //删除的备课课程下发布的任务还要可见 2020.03.19
                var selectData = DbContext.FreeSql.GetGuidRepository<YssxTask>().Select.From<YssxCourse>((a, b) => a.LeftJoin(aa => aa.OriginalCourseId == b.Id))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.CreateBy == currentUserId)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b) => a.Name.Contains(model.Name))
                    .WhereIf(model.OriginalCourseId > 0, (a, b) => a.OriginalCourseId == model.OriginalCourseId)
                    .WhereIf(rSearchTaskId.Count > 0, (a, b) => rSearchTaskId.Contains(a.Id))
                    .OrderByDescending((a, b) => a.Sort).ToList((a, b) => new TaskView
                    {
                        Id = a.Id,
                        Name = a.Name,
                        TaskType = a.TaskType,
                        CourseId = a.CourseId,
                        CourseName = b.CourseName,
                        OriginalCourseId = a.OriginalCourseId,
                        StartTime = a.StartTime,
                        CloseTime = a.CloseTime,
                        TotalMinutes = a.TotalMinutes,
                        Status = a.Status,
                        Content = a.Content,
                        Sort = a.Sort,
                        IsCanCheckExam = a.IsCanCheckExam
                    });
                #region 搜索条件
                selectData = model.TaskType > -1 ? selectData.Where(a => a.TaskType == (TaskType)model.TaskType).ToList() : selectData.Where(a => a.TaskType != TaskType.ClassTest).ToList();
                if (model.Status != -2)
                    selectData = selectData.Where(a => a.ExamStatus == (LessonsTaskStatus)model.Status).ToList();
                if (model.IsBeginTime == CommonConstants.IsYes)
                {
                    if (model.BeginDate.HasValue)
                    {
                        var rBeginDate = Convert.ToDateTime(model.BeginDate.Value.ToString("yyyy-MM-dd 00:00:01"));
                        selectData = selectData.Where(a => a.StartTime >= rBeginDate).ToList();
                    }
                    if (model.EndDate.HasValue)
                    {
                        var rEndDate = Convert.ToDateTime(model.EndDate.Value.ToString("yyyy-MM-dd 23:59:59"));
                        selectData = selectData.Where(a => a.StartTime <= rEndDate).ToList();
                    }
                }
                else
                {
                    if (model.BeginDate.HasValue)
                    {
                        var rBeginDate = Convert.ToDateTime(model.BeginDate.Value.ToString("yyyy-MM-dd 00:00:01"));
                        selectData = selectData.Where(a => a.CloseTime >= rBeginDate).ToList();
                    }
                    if (model.EndDate.HasValue)
                    {
                        var rEndDate = Convert.ToDateTime(model.EndDate.Value.ToString("yyyy-MM-dd 23:59:59"));
                        selectData = selectData.Where(a => a.CloseTime <= rEndDate).ToList();
                    }
                }
                #endregion
                var totalCount = selectData.Count();
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                //查询任务的班级列表和学生数据
                var rTaskIdArr = selectData.Select(m => m.Id).ToList();
                var rTaskStudentData = DbContext.FreeSql.GetRepository<YssxTaskStudent>().Select.From<YssxClass, YssxUser>((a, b, c) => 
                    a.InnerJoin(aa => aa.ClassId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                    .InnerJoin(aa => aa.UserId == c.Id && c.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && rTaskIdArr.Contains(a.TaskId))
                    .ToList((a, b, c) => new
                    {
                        TaskId = a.TaskId,
                        ClassId = a.ClassId,
                        ClassName = b.Name,
                        UserId = a.UserId
                    });
                foreach (var item in selectData)
                {
                    var rClassDetailData = new List<TaskClassView>();
                    if (rTaskStudentData.Count > 0 && item.Id > 0 && rTaskStudentData.Any(m => m.TaskId == item.Id))
                    {
                        rClassDetailData = rTaskStudentData.Where(m => m.TaskId == item.Id).GroupBy(x => new { x.ClassId, x.ClassName }).Select(m =>
                            new TaskClassView
                            {
                                ClassId = m.Key.ClassId,
                                ClassName = m.Key.ClassName,
                                StudentCount = m.Count()
                            }).ToList();
                    }
                    item.ClassDetail = rClassDetailData;
                    item.TotalStudentCount = rClassDetailData.Sum(m => m.StudentCount);
                    item.SubmitCount = 0;
                }

                return new PageResponse<TaskView> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 查询课业任务列表 - 学生端 - new
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<PageResponse<StudentTaskView>> GetStudentTaskByPageAll(GetTaskByPageAllRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                //删除的备课课程下发布的任务还要可见 2020.03.19
                var selectData = DbContext.FreeSql.GetGuidRepository<YssxTask>().Select
                    .From<YssxCourse, YssxTaskStudent, YssxUser, ExamCourseUserGrade>((a, b, c, d, e) =>
                        a.LeftJoin(aa => aa.OriginalCourseId == b.Id)
                        .InnerJoin(aa => aa.Id == c.TaskId && c.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => c.UserId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => aa.Id == e.TaskId && e.UserId == currentUserId && e.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d, e) => a.IsDelete == CommonConstants.IsNotDelete && d.Id == currentUserId)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b, c, d, e) => a.Name.Contains(model.Name))
                    .WhereIf(model.OriginalCourseId > 0, (a, b, c, d, e) => a.OriginalCourseId == model.OriginalCourseId)
                    .OrderByDescending((a, b, c, d, e) => a.CreateTime).ToList((a, b, c, d, e) => new StudentTaskView
                    {
                        Id = a.Id,
                        Name = a.Name,
                        TaskType = a.TaskType,
                        CourseId = a.CourseId,
                        CourseName = b.CourseName,
                        OriginalCourseId = a.OriginalCourseId,
                        StartTime = a.StartTime,
                        CloseTime = a.CloseTime,
                        TotalMinutes = a.TotalMinutes,
                        Status = a.Status,
                        GradeStatus = e.Status,
                        Content = a.Content,
                        Sort = a.Sort,
                        ExamId = a.ExamId,
                        GradeId = e.Id,
                        ErrorCount = e.ErrorCount,
                        ExamPaperScore = e.ExamPaperScore,
                        Score = e.Score,
                        IsCanCheckExam = a.IsCanCheckExam
                    });
                selectData = model.TaskType > -1 ? selectData.Where(a => a.TaskType == (TaskType)model.TaskType).ToList() : selectData.Where(a => a.TaskType != TaskType.ClassTest).ToList();
                if (model.Status != -2)
                    selectData = selectData.Where(a => a.ExamStatus == (LessonsTaskStatus)model.Status).ToList();
                var totalCount = selectData.Count();
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                return new PageResponse<StudentTaskView> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 查询指定课业任务信息 - new
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TaskInfoDto>> GetTaskInfoById(long taskId, long currentUserId)
        {
            return await Task.Run(() =>
            {
                YssxTask taskEntity = DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == taskId && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (taskEntity == null)
                    return new ResponseContext<TaskInfoDto> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该数据!" };
                var rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == taskEntity.ExamId).First();
                if (rPaperEntity == null)
                    return new ResponseContext<TaskInfoDto> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };
                var rCourseEntity = DbContext.FreeSql.Select<YssxCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Id == taskEntity.OriginalCourseId).First();
                if (rCourseEntity == null)
                    return new ResponseContext<TaskInfoDto> { Code = CommonConstants.ErrorCode, Msg = "任务关联的课程找不到" };

                var taskInfoData = taskEntity.MapTo<TaskInfoDto>();
                taskInfoData.TotalScore = rPaperEntity.TotalScore;
                taskInfoData.CourseName = rCourseEntity.CourseName;
                //查询题目列表
                var rTopicDetail = DbContext.FreeSql.GetRepository<YssxTaskTopic>().Select
                    .From<YssxTopicPublic>((a, b) => a.InnerJoin(aa => aa.TopicId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.TaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete).OrderBy((a, b) => a.Sort).ToList((a, b) => new TaskTopicDto
                    {
                        Id = a.Id,
                        TopicId = a.TopicId,
                        QuestionType = a.QuestionType,
                        Score = a.Score,
                        Sort = a.Sort,
                        Title = b.Title
                    });
                taskInfoData.TopicDetail = rTopicDetail;
                //筛选题目分组信息
                var rTopicGroupDetail = rTopicDetail.GroupBy(m => m.QuestionType).Select(a => new TaskTopicGroupDto
                {
                    QuestionType = a.Key,
                    Count = a.Count(),
                    TotalScore = a.Sum(b => b.Score)
                }).ToList();
                taskInfoData.TopicGroupDetail = rTopicGroupDetail;
                //查询班级学生列表
                var rStudentData = DbContext.FreeSql.GetRepository<YssxTaskStudent>().Select
                    .From<YssxClass, YssxUser, YssxStudent>((a, b, c, d) => a.InnerJoin(aa => aa.ClassId == b.Id).InnerJoin(aa => aa.UserId == c.Id).LeftJoin(aa => aa.UserId == d.UserId))
                    .Where((a, b, c, d) => a.TaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete).ToList((a, b, c, d) => new TaskStudentDto
                    {
                        TaskId = a.TaskId,
                        ClassId = a.ClassId,
                        ClassName = b.Name,
                        UserId = a.UserId,
                        UserName = c.RealName,
                        StudentId = a.StudentId,
                        StudentNo = d.StudentNo
                    });
                var rClassDetail = rStudentData.GroupBy(m => new { m.ClassId, m.ClassName }).Select(a => new TaskInfoClassDto
                {
                    ClassId = a.Key.ClassId,
                    ClassName = a.Key.ClassName,
                    StudentCount = a.Count()
                }).ToList();
                foreach (var itemClass in rClassDetail)
                {
                    itemClass.ClassName = DbContext.FreeSql.GetRepository<YssxClass>().Where(x => x.Id == itemClass.ClassId && x.IsDelete == CommonConstants.IsNotDelete).First(m => m.Name);
                    itemClass.StudentDetail = rStudentData.Where(m => m.ClassId == itemClass.ClassId).Select(m => new TaskInfoStudentDto
                    {
                        UserId = m.UserId,
                        StudentId = m.StudentId,
                        Name = m.UserName,
                        StudentNo = m.StudentNo
                    }).ToList();
                }
                taskInfoData.ClassDetail = rClassDetail;

                return new ResponseContext<TaskInfoDto> { Code = CommonConstants.SuccessCode, Msg = "成功", Data = taskInfoData };
            });
        }

        /// <summary>
        /// 修改课业任务 - new
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EditTaskInfo(TaskDto model, long currentUserId)
        {
            var dtNow = DateTime.Now;
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
            if (model.StartTime > model.CloseTime)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "开始时间不得大于结束时间" };
            if (model.CloseTime < dtNow)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "结束时间不得小于当前时间" };
            if (model.TotalMinutes <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "考试时长必须大于0" };
            //if (DbContext.FreeSql.Select<YssxTask>().Any(m => m.Name == model.Name && m.Id != model.Id && m.CreateBy == currentUserId && m.IsDelete == CommonConstants.IsNotDelete))
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "课业任务名称重复" };

            return await Task.Run(() =>
            {
                bool state = true;

                #region 处理数据
                // 处理数据
                ExamPaperCourse rPaperEntity = new ExamPaperCourse();
                var rTaskEntity = DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == model.Id && x.TaskType == model.TaskType && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (rTaskEntity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到该课业任务" };
                if (rTaskEntity.ExamId == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "此课业任务数据异常，试卷ID为0" };
                rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == rTaskEntity.ExamId).First();
                if (rPaperEntity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };
                //课业任务
                rTaskEntity.Name = model.Name;
                rTaskEntity.StartTime = model.StartTime;
                rTaskEntity.CloseTime = model.CloseTime;
                rTaskEntity.TotalMinutes = model.TotalMinutes;
                rTaskEntity.UpdateBy = currentUserId;
                rTaskEntity.UpdateTime = dtNow;
                rTaskEntity.IsCanCheckExam = model.IsCanCheckExam;
                //试卷
                rPaperEntity.Name = model.Name;
                rPaperEntity.BeginTime = model.StartTime;
                rPaperEntity.EndTime = model.CloseTime;
                rPaperEntity.TotalMinutes = model.TotalMinutes;
                rPaperEntity.UpdateBy = currentUserId;
                rPaperEntity.UpdateTime = dtNow;
                //学生 - 新增
                var rClassStudent = new List<TaskClassDto>();
                foreach (var item in model.ClassDetail)
                {
                    if (!DbContext.FreeSql.Select<YssxTaskStudent>().Any(m => m.TaskId == model.Id && m.ClassId == item.ClassId && m.UserId == item.UserId && m.IsDelete == CommonConstants.IsNotDelete))
                        rClassStudent.Add(item);
                }
                var rAddClassStudent = rClassStudent.Select(m => new YssxTaskStudent
                {
                    Id = IdWorker.NextId(),
                    TaskId = rTaskEntity.Id,
                    ClassId = m.ClassId,
                    UserId = m.UserId,
                    StudentId = m.StudentId,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                }).ToList();
                //作答记录 - 新增
                var rAddExamGradeData = rClassStudent.Select(m => new ExamCourseUserGrade
                {
                    Id = IdWorker.NextId(),
                    ExamId = rPaperEntity.Id,
                    CourseId = 0,
                    OriginalCourseId = rPaperEntity.OriginalCourseId,
                    SectionId = rPaperEntity.SectionId,
                    TaskId = rPaperEntity.TaskId,
                    ExamPaperScore = rPaperEntity.TotalScore,
                    LeftSeconds = rPaperEntity.TotalMinutes * 60,
                    UserId = m.UserId,
                    TenantId = rTaskEntity.TenantId,
                    StudentId = m.StudentId,
                    Status = StudentExamStatus.Wait,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                }).ToList();
                //作答记录 - 更新
                var rUpdExamGradeData = DbContext.FreeSql.GetRepository<ExamCourseUserGrade>()
                    .Where(x => x.TaskId == model.Id && x.IsDelete == CommonConstants.IsNotDelete).ToList(m => new ExamCourseUserGrade
                    {
                        Id = m.Id,
                        LeftSeconds = model.TotalMinutes * 60,
                        RecordTime = model.StartTime,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    });
                #endregion

                #region 更新数据
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //课业任务
                    DbContext.FreeSql.Update<YssxTask>().SetSource(rTaskEntity).UpdateColumns(a => new
                    {
                        a.Name,
                        a.StartTime,
                        a.CloseTime,
                        a.TotalMinutes,
                        a.UpdateBy,
                        a.UpdateTime,
                        a.IsCanCheckExam
                    }).ExecuteAffrows();
                    //学生
                    if (rAddClassStudent.Count > 0)
                        DbContext.FreeSql.Insert<YssxTaskStudent>().AppendData(rAddClassStudent).ExecuteAffrows();
                    //作答记录
                    if (rAddExamGradeData.Count > 0)
                        DbContext.FreeSql.Insert<ExamCourseUserGrade>().AppendData(rAddExamGradeData).ExecuteAffrows();
                    //试卷
                    var resUpd = DbContext.FreeSql.Update<ExamPaperCourse>().SetSource(rPaperEntity).UpdateColumns(a => new
                    {
                        a.Name,
                        a.BeginTime,
                        a.EndTime,
                        a.TotalMinutes,
                        a.UpdateBy,
                        a.UpdateTime,
                    }).ExecuteAffrows();
                    //作答记录 - 更新
                    if (rUpdExamGradeData.Count > 0)
                        DbContext.FreeSql.Update<ExamCourseUserGrade>().SetSource(rUpdExamGradeData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.LeftSeconds, a.RecordTime }).ExecuteAffrows();

                    state = resUpd > 0;
                    uow.Commit();
                }
                #endregion
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        #endregion

        /// <summary>
        /// 查询指定课业任务状态
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<LessonsTaskStatus>> GetTaskStatusById(long taskId, long GradeId)
        {
            var dateNow = DateTime.Now;
            YssxTask taskEntity =await DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == taskId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (taskEntity == null)
                return new ResponseContext<LessonsTaskStatus> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该课业任务" };
            var rGradeEntity =await DbContext.FreeSql.Select<ExamCourseUserGrade>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.TaskId == taskId && m.Id == GradeId).FirstAsync();
            if (rGradeEntity == null)
                return new ResponseContext<LessonsTaskStatus> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷作答主记录" };

            LessonsTaskStatus examStatus = LessonsTaskStatus.Wait;

            if (taskEntity.Status == LessonsTaskStatus.End || rGradeEntity.Status == StudentExamStatus.End)
            {
                examStatus = LessonsTaskStatus.End;
            }
            else if (dateNow < taskEntity.StartTime)
            {
                examStatus = LessonsTaskStatus.Wait;
            }
            else if (taskEntity.StartTime <= dateNow && dateNow < taskEntity.CloseTime)
            {
                examStatus = LessonsTaskStatus.Started;
            }
            else if (taskEntity.CloseTime <= dateNow)
            {
                examStatus = LessonsTaskStatus.End;
            }

            return new ResponseContext<LessonsTaskStatus> { Code = CommonConstants.SuccessCode, Msg = "成功", Data = examStatus };
        }

        /// <summary>
        /// 删除课业任务
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveTask(long id, long currentUserId)
        {
            var dtNow = DateTime.Now;

            YssxTask taskEntity = await DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == id && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (taskEntity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该课业任务!" };
            taskEntity.IsDelete = CommonConstants.IsDelete;
            taskEntity.UpdateBy = currentUserId;
            taskEntity.UpdateTime = dtNow;

            var paperEntity = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (paperEntity != null)
            {
                paperEntity.IsDelete = CommonConstants.IsDelete;
                paperEntity.UpdateBy = currentUserId;
                paperEntity.UpdateTime = dtNow;
            }
            var listTaskStudent = await DbContext.FreeSql.GetRepository<YssxTaskStudent>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync(m => new YssxTaskStudent
            {
                Id = m.Id,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = currentUserId,
                UpdateTime = dtNow
            });
            var listTaskTopic = await DbContext.FreeSql.GetRepository<YssxTaskTopic>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync(m => new YssxTaskTopic
            {
                Id = m.Id,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = currentUserId,
                UpdateTime = dtNow
            });
            var listExamGrade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync(m => new ExamCourseUserGrade
            {
                Id = m.Id,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = currentUserId,
                UpdateTime = dtNow
            });

            return await Task.Run(() =>
            {
                bool state = true;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //课业任务
                    state = DbContext.FreeSql.GetRepository<YssxTask>().UpdateDiy.SetSource(taskEntity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
                    //试卷
                    if (paperEntity != null)
                        DbContext.FreeSql.GetRepository<ExamPaperCourse>().UpdateDiy.SetSource(paperEntity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                    //学生
                    if (listTaskStudent.Count > 0)
                        DbContext.FreeSql.Update<YssxTaskStudent>().SetSource(listTaskStudent).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                    //题目
                    if (listTaskTopic.Count > 0)
                        DbContext.FreeSql.Update<YssxTaskTopic>().SetSource(listTaskTopic).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                    //作答记录
                    if (listExamGrade.Count > 0)
                        DbContext.FreeSql.Update<ExamCourseUserGrade>().SetSource(listExamGrade).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();

                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
            });
        }

        /// <summary>
        /// 结束课业任务
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ClosedTask(long id, long currentUserId)
        {
            var dtNow = DateTime.Now;

            YssxTask taskEntity = await DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == id && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (taskEntity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该课业任务!" };
            taskEntity.Status = LessonsTaskStatus.End;
            taskEntity.UpdateBy = currentUserId;
            taskEntity.UpdateTime = dtNow;

            var paperEntity = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (paperEntity != null)
            {
                paperEntity.Status = ExamStatus.End;
                paperEntity.UpdateBy = currentUserId;
                paperEntity.UpdateTime = dtNow;
            }
            var listExamGrade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync(m => new ExamCourseUserGrade
            {
                Id = m.Id,
                Status = StudentExamStatus.End,
                UpdateBy = currentUserId,
                UpdateTime = dtNow
            });
            
            return await Task.Run(() =>
            {
                bool state = true;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //课业任务
                    state = DbContext.FreeSql.GetRepository<YssxTask>().UpdateDiy.SetSource(taskEntity).UpdateColumns(x => new { x.Status, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
                    //试卷
                    if (paperEntity != null)
                        DbContext.FreeSql.GetRepository<ExamPaperCourse>().UpdateDiy.SetSource(paperEntity).UpdateColumns(x => new { x.Status, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                    //作答记录
                    if (listExamGrade.Count > 0)
                        DbContext.FreeSql.Update<ExamCourseUserGrade>().SetSource(listExamGrade).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.Status }).ExecuteAffrows();

                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }
        /// <summary>
        /// 一键交卷
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BatchSubmitExam(long id, long currentUserId)
        {
            var result = new ResponseContext<bool>(true);
            var dtNow = DateTime.Now;
            YssxTask taskEntity = await DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == id && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (taskEntity == null)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = "操作失败，未找到该课业任务!";
            }
            taskEntity.Status = LessonsTaskStatus.End;
            taskEntity.UpdateBy = currentUserId;
            taskEntity.UpdateTime = dtNow;

            var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (exam != null)
            {
                exam.Status = ExamStatus.End;
                exam.UpdateBy = currentUserId;
                exam.UpdateTime = dtNow;
            }
            var listExamGrade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>()
                .Where(x => x.TaskId == id && x.Status!=StudentExamStatus.End && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            var updateGradeList = new List<ExamCourseUserGrade>();

            foreach (var grade in listExamGrade)
            {
                //提交考卷
                SubmitExamGrade(exam, grade, true);
                updateGradeList.Add(grade);
            }

            return await Task.Run(() =>
            {
                try
                {
                    DbContext.FreeSql.Transaction(() =>
                    {
                        //课业任务
                        DbContext.FreeSql.GetRepository<YssxTask>().UpdateDiy.SetSource(taskEntity).UpdateColumns(x => new { x.Status, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                        //试卷
                        if (exam != null)
                            DbContext.FreeSql.GetRepository<ExamPaperCourse>().UpdateDiy.SetSource(exam).UpdateColumns(x => new { x.Status, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                        //作答记录
                        if (updateGradeList.Any())
                            DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().UpdateDiy.SetSource(updateGradeList).ExecuteAffrows();
                    });
                }
                catch (Exception ex)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = "一件交卷数据异常，请联系管理员";
                }
                
                return result;
            });
           
        }
        /// <summary>
        /// 提交考试
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="isBatchSubmit">true时代表老师一件交卷</param>
        private void SubmitExamGrade(ExamPaperCourse exam, ExamCourseUserGrade grade, bool isBatchSubmit = false)
        {
            grade.Status = StudentExamStatus.End;
            grade.IsManualSubmit = !isBatchSubmit;
            grade.UpdateTime = DateTime.Now;
            grade.SubmitTime = DateTime.Now;

            switch (exam.ExamType)
            {
                case CourseExamType.CourceTest:
                    grade.UsedSeconds = (grade.SubmitTime.Value - grade.CreateTime).TotalSeconds;
                    break;
                case CourseExamType.ClassExam:
                case CourseExamType.ClassHomeWork:
                case CourseExamType.ClassTask:
                case CourseExamType.ClassTest:
                    grade.UsedSeconds = (grade.SubmitTime.Value - grade.RecordTime).TotalSeconds;
                    break;
            }

            var totalQuestionCount = exam.TotalQuestionCount;

            //Done:计算考卷分数，返回考试成绩信息
            var answerCount = DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.GradeId == grade.Id && a.QuestionId == a.ParentQuestionId)
               .Master().ToList(a => new { a.QuestionId, a.Status, a.Score });
            grade.CorrectCount = answerCount.Count(a => a.Status == AnswerResultStatus.Right);
            grade.ErrorCount = answerCount.Count(a => a.Status == AnswerResultStatus.Error);
            grade.PartRightCount = answerCount.Count(a => a.Status == AnswerResultStatus.PartRight);
            grade.BlankCount = totalQuestionCount - answerCount.Count;

            if (grade.BlankCount <= 0)
                grade.BlankCount = 0;
            grade.Score = answerCount.Sum(a => a.Score);

        }
        #endregion

        #region 组卷 - old
        /// <summary>
        /// 手动组卷
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ManualOrgPaper(TaskManualOrgPaperDto model, long currentUserId, long tenantId)
        {
            var dtNow = DateTime.Now;
            #region 校验
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
            if (model.CourseId <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择课程" };
            if (model.StartTime > model.CloseTime)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "开始时间不得大于结束时间" };
            if (model.CloseTime < dtNow)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "结束时间不得小于当前时间" };
            if (model.TotalMinutes <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "考试时长必须大于0" };
            if (model.ClassDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择班级学员" };
            if (model.TopicDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择题目" };
            if (model.ClassDetail.GroupBy(m => m.UserId).Count() != model.ClassDetail.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学生信息重复" };
            if (model.ClassDetail.Any(m => m.ClassId == 0))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级ID异常" };
            //if (DbContext.FreeSql.Select<YssxTask>().Any(m => m.Name == model.Name && m.Id != model.Id && m.CreateBy == currentUserId && m.IsDelete == CommonConstants.IsNotDelete))
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "课业任务名称重复" };

            if (model.TotalScore <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "总分必须大于0" };
            if (model.TopicDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择题目" };
            if (model.TopicDetail.GroupBy(m => m.TopicId).Count() != model.TopicDetail.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "题目信息重复" };
            var totalScore = model.TopicDetail.Sum(m => m.Score);
            if (model.TotalScore != totalScore)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = $"卷面分与考核分数不一致，考核总分为{ totalScore }分，卷面分为{ model.TotalScore }" };
            //课程信息相关
            var rPrepareCourseEntity = DbContext.FreeSql.Select<YssxPrepareCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Id == model.CourseId).First();
            if (rPrepareCourseEntity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到备课课程信息" };
            if (!rPrepareCourseEntity.OriginalCourseId.HasValue || rPrepareCourseEntity.OriginalCourseId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "备课课程的原始课程ID为0" };

            #endregion

            return await Task.Run(() =>
            {
                bool state = true;
                //处理数据
                if (model.Id == 0)
                {
                    #region 处理数据
                    var maxSort = DbContext.FreeSql.Select<YssxTask>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.TaskType != TaskType.ClassTest)
                        .OrderByDescending(m => m.Sort).First(m => m.Sort);
                    var rExamId = IdWorker.NextId();
                    //课业任务
                    var rTaskEntity = new YssxTask
                    {
                        Id = IdWorker.NextId(),
                        TaskType = model.TaskType,
                        TenantId = tenantId,
                        Name = model.Name,
                        StartTime = model.StartTime,
                        CloseTime = model.CloseTime,
                        TotalMinutes = model.TotalMinutes,
                        CourseId = model.CourseId,
                        OriginalCourseId = rPrepareCourseEntity.OriginalCourseId.Value,
                        Sort = maxSort + 1,
                        Content = model.Content,
                        ExamId = rExamId,
                        Status = LessonsTaskStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    };
                    //试卷
                    ExamPaperCourse rPaperEntity = new ExamPaperCourse
                    {
                        Id = rExamId,
                        Name = rTaskEntity.Name,
                        TaskId = rTaskEntity.Id,
                        TenantId = tenantId,
                        CourseId = rTaskEntity.CourseId,
                        ExamType = rTaskEntity.TaskType == TaskType.ClassExam ? CourseExamType.ClassExam : (rTaskEntity.TaskType == TaskType.ClassTask ? CourseExamType.ClassTask : CourseExamType.ClassHomeWork),
                        CanShowAnswerBeforeEnd = true,
                        TotalScore = totalScore,
                        TotalQuestionCount = model.TopicDetail.Count,
                        TotalMinutes = rTaskEntity.TotalMinutes,
                        BeginTime = rTaskEntity.StartTime,
                        EndTime = rTaskEntity.CloseTime,
                        ReleaseTime = dtNow,
                        Sort = rTaskEntity.Sort,
                        IsRelease = true,
                        Status = ExamStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    };
                    //学生
                    var rClassStudent = model.ClassDetail.Select(m => new YssxTaskStudent
                    {
                        Id = IdWorker.NextId(),
                        TaskId = rTaskEntity.Id,
                        ClassId = m.ClassId,
                        UserId = m.UserId,
                        StudentId = m.StudentId,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //题目
                    var rAddTopicData = model.TopicDetail.Select(m => new YssxTaskTopic
                    {
                        Id = IdWorker.NextId(),
                        TaskId = rTaskEntity.Id,
                        TopicId = m.TopicId,
                        QuestionType = m.QuestionType,
                        Score = m.Score,
                        Sort = m.Sort,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //作答记录
                    var rExamGradeData = model.ClassDetail.Select(m => new ExamCourseUserGrade
                    {
                        Id = IdWorker.NextId(),
                        ExamId = rPaperEntity.Id,
                        CourseId = rPaperEntity.CourseId,
                        SectionId = rPaperEntity.SectionId,
                        TaskId = rPaperEntity.TaskId,
                        ExamPaperScore = rPaperEntity.TotalScore,
                        LeftSeconds = rPaperEntity.TotalMinutes * 60,
                        UserId = m.UserId,
                        TenantId = tenantId,
                        StudentId = m.StudentId,
                        Status = StudentExamStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();

                    #endregion

                    #region 新增数据
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        //课业任务
                        var taskEntity = DbContext.FreeSql.GetRepository<YssxTask>().Insert(rTaskEntity);
                        //试卷
                        var paperEntity = DbContext.FreeSql.GetRepository<ExamPaperCourse>().Insert(rPaperEntity);
                        //学生
                        if (rClassStudent.Count > 0)
                            DbContext.FreeSql.Insert<YssxTaskStudent>().AppendData(rClassStudent).ExecuteAffrows();
                        //题目
                        if (rAddTopicData.Count > 0)
                            DbContext.FreeSql.Insert<YssxTaskTopic>().AppendData(rAddTopicData).ExecuteAffrows();
                        //作答记录
                        if (rExamGradeData.Count > 0)
                            DbContext.FreeSql.Insert<ExamCourseUserGrade>().AppendData(rExamGradeData).ExecuteAffrows();

                        state = paperEntity != null;
                        uow.Commit();
                    }
                    #endregion
                }
                else
                {
                    #region 处理数据
                    var rTaskEntity = DbContext.FreeSql.Select<YssxTask>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == model.Id).First();
                    if (rTaskEntity == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到课业任务" };

                    ExamPaperCourse rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == rTaskEntity.ExamId).First();
                    if (rPaperEntity == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };

                    //课业任务
                    //rTaskEntity.Status = LessonsTaskStatus.Wait;
                    rTaskEntity.Name = model.Name;
                    rTaskEntity.StartTime = model.StartTime;
                    rTaskEntity.CloseTime = model.CloseTime;
                    rTaskEntity.TotalMinutes = model.TotalMinutes;
                    rTaskEntity.CourseId = model.CourseId;
                    rTaskEntity.OriginalCourseId = rPrepareCourseEntity.OriginalCourseId.Value;
                    rTaskEntity.Content = model.Content;
                    rTaskEntity.UpdateBy = currentUserId;
                    rTaskEntity.UpdateTime = dtNow;
                    //试卷
                    //rPaperEntity.Status = ExamStatus.Wait;
                    rPaperEntity.BeginTime = rTaskEntity.StartTime;
                    rPaperEntity.EndTime = rTaskEntity.CloseTime;
                    rPaperEntity.CourseId = model.CourseId;
                    rPaperEntity.TotalScore = totalScore;
                    rPaperEntity.TotalQuestionCount = model.TopicDetail.Count;
                    rPaperEntity.TotalMinutes = model.TotalMinutes;
                    rPaperEntity.UpdateBy = currentUserId;
                    rPaperEntity.UpdateTime = dtNow;

                    #region 学生
                    var rOldStudent = DbContext.FreeSql.GetRepository<YssxTaskStudent>().Where(x => x.TaskId == model.Id && x.IsDelete == CommonConstants.IsNotDelete).ToList();
                    //取待删除的学生
                    var rDelExamGradeData = new List<ExamCourseUserGrade>();
                    var rClassStudentId = model.ClassDetail.Select(m => m.UserId).ToList();
                    var delStudentData = rOldStudent.Where(m => !rClassStudentId.Contains(m.UserId)).ToList();
                    if (delStudentData.Count > 0)
                    {
                        delStudentData.ForEach(a =>
                        {
                            a.IsDelete = CommonConstants.IsDelete;
                            a.UpdateBy = currentUserId;
                            a.UpdateTime = dtNow;
                        });
                        //取待删除作答记录
                        var rDelStudentId = delStudentData.Select(m => m.UserId).ToList();
                        rDelExamGradeData = DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(x => x.TaskId == model.Id && rDelStudentId.Contains(x.UserId) && x.IsDelete == CommonConstants.IsNotDelete).ToList(m => new ExamCourseUserGrade
                        {
                            Id = m.Id,
                            IsDelete = CommonConstants.IsDelete,
                            UpdateBy = currentUserId,
                            UpdateTime = dtNow
                        });
                    }
                    //取待新增的学生
                    var rAddExamGradeData = new List<ExamCourseUserGrade>();
                    var rOldClassStudentId = rOldStudent.Select(m => m.UserId).ToList();
                    var addStudentData = model.ClassDetail.Where(m => !rOldClassStudentId.Contains(m.UserId)).Select(m => new YssxTaskStudent
                    {
                        Id = IdWorker.NextId(),
                        TaskId = rTaskEntity.Id,
                        ClassId = m.ClassId,
                        UserId = m.UserId,
                        StudentId = m.StudentId,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    if (addStudentData.Count > 0 && rTaskEntity.ExamId > 0)
                    {
                        rAddExamGradeData = addStudentData.Select(m => new ExamCourseUserGrade
                        {
                            Id = IdWorker.NextId(),
                            ExamId = rPaperEntity.Id,
                            CourseId = rPaperEntity.CourseId,
                            SectionId = rPaperEntity.SectionId,
                            TaskId = rPaperEntity.TaskId,
                            ExamPaperScore = rPaperEntity.TotalScore,
                            LeftSeconds = rPaperEntity.TotalMinutes * 60,
                            UserId = m.UserId,
                            TenantId = tenantId,
                            StudentId = m.StudentId,
                            Status = StudentExamStatus.Wait,
                            CreateBy = currentUserId,
                            UpdateBy = currentUserId,
                            UpdateTime = dtNow
                        }).ToList();
                    }
                    #endregion

                    #region 题目
                    //取新增的题目
                    var rAddTopicData = model.TopicDetail.Where(m => m.Id == 0).Select(m => new YssxTaskTopic
                    {
                        Id = IdWorker.NextId(),
                        TaskId = rTaskEntity.Id,
                        TopicId = m.TopicId,
                        QuestionType = m.QuestionType,
                        Score = m.Score,
                        Sort = m.Sort,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //取修改的题目
                    var rUpdTopicData = model.TopicDetail.Where(m => m.Id > 0).Select(m => new YssxTaskTopic
                    {
                        Id = m.Id,
                        Sort = m.Sort,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //取删除的题目
                    var rNewTopicId = model.TopicDetail.Select(m => m.TopicId).ToList();
                    var rDelTopicData = DbContext.FreeSql.Select<YssxTaskTopic>()
                        .Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.TaskId == rTaskEntity.Id && !rNewTopicId.Contains(m.TopicId))
                        .ToList(m => new YssxTaskTopic
                        {
                            Id = m.Id,
                            IsDelete = CommonConstants.IsDelete,
                            UpdateBy = currentUserId,
                            UpdateTime = dtNow
                        });
                    #endregion

                    #endregion

                    #region 更新数据
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        //课业任务
                        DbContext.FreeSql.Update<YssxTask>().SetSource(rTaskEntity).UpdateColumns(a => new
                        {
                            a.Name,
                            a.StartTime,
                            a.CloseTime,
                            a.TotalMinutes,
                            a.CourseId,
                            a.Content,
                            a.UpdateBy,
                            a.UpdateTime
                        }).ExecuteAffrows();
                        //试卷信息
                        var resUpd = DbContext.FreeSql.Update<ExamPaperCourse>().SetSource(rPaperEntity).UpdateColumns(a => new
                        {
                            a.BeginTime,
                            a.EndTime,
                            a.CourseId,
                            a.TotalScore,
                            a.TotalQuestionCount,
                            a.TotalMinutes,
                            a.UpdateBy,
                            a.UpdateTime
                        }).ExecuteAffrows();
                        //删除的学生
                        if (delStudentData.Count > 0)
                            DbContext.FreeSql.Update<YssxTaskStudent>().SetSource(delStudentData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                        //删除的作答记录
                        if (rDelExamGradeData.Count > 0)
                            DbContext.FreeSql.Update<ExamCourseUserGrade>().SetSource(rDelExamGradeData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                        //新增的学生
                        if (addStudentData.Count > 0)
                            DbContext.FreeSql.Insert<YssxTaskStudent>().AppendData(addStudentData).ExecuteAffrows();
                        //新增的作答记录
                        if (rAddExamGradeData.Count > 0)
                            DbContext.FreeSql.Insert<ExamCourseUserGrade>().AppendData(rAddExamGradeData).ExecuteAffrows();
                        //新增的题目
                        if (rAddTopicData.Count > 0)
                            DbContext.FreeSql.Insert<YssxTaskTopic>().AppendData(rAddTopicData).ExecuteAffrows();
                        //修改的题目
                        if (rUpdTopicData.Count > 0)
                            DbContext.FreeSql.Update<YssxTaskTopic>().SetSource(rUpdTopicData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.Sort }).ExecuteAffrows();
                        //删除的题目
                        if (rDelTopicData.Count > 0)
                            DbContext.FreeSql.Update<YssxTaskTopic>().SetSource(rDelTopicData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();

                        state = resUpd > 0;
                        uow.Commit();
                    }
                    #endregion
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 查询题目类型数量统计（1.知识点 2.课程章节）
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<TaskTopicGroupDto>> GetTaskTopicById(GetTaskTopicByIdRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                if (model.SurveyScopeDetail.Count == 0)
                    return new PageResponse<TaskTopicGroupDto> { Code = CommonConstants.ErrorCode, Msg = "查询失败，请选择考核范围!" };

                var selectData = new List<TaskTopicGroupDto>();
                var rTopicYear = string.IsNullOrEmpty(model.TopicYear) ? new List<string>() : model.TopicYear.Split(',').ToList();
                if (model.DataType == 1)
                {
                    //知识点
                    //查询题目列表
                    var rYssxTopicPublicData = new List<YssxTopicPublic>();
                    foreach (var item in model.SurveyScopeDetail)
                    {
                        var rTopicDetail = DbContext.FreeSql.GetRepository<YssxTopicPublic>()
                            .Where(x => x.KnowledgePointIds.IndexOf(item.ToString()) >= 0 && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                            .WhereIf(!string.IsNullOrEmpty(model.TopicYear), x => (x.UpdateTime.HasValue && rTopicYear.Contains(x.UpdateTime.Value.Year.ToString()))
                                || (!x.UpdateTime.HasValue && rTopicYear.Contains(x.CreateTime.Year.ToString()))).ToList();
                        if (rTopicDetail.Count > 0) rYssxTopicPublicData.AddRange(rTopicDetail);
                    }
                    if (rYssxTopicPublicData.Count > 0)
                    {
                        //去重
                        rYssxTopicPublicData = rYssxTopicPublicData.GroupBy(m => new { m.Id, m.QuestionType }).Select(a => new YssxTopicPublic
                        {
                            Id = a.Key.Id,
                            QuestionType = a.Key.QuestionType
                        }).ToList();
                        //筛选题目分组数据
                        selectData = rYssxTopicPublicData.GroupBy(m => m.QuestionType).Select(a => new TaskTopicGroupDto
                        {
                            QuestionType = a.Key,
                            Count = a.Count()
                        }).ToList();
                    }
                }
                else
                {
                    //备课章节
                    selectData = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Select
                        .From<YssxPrepareSectionTopic>((a, b) => a.InnerJoin(aa => aa.Id == b.QuestionId && b.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => model.SurveyScopeDetail.Contains(b.SectionId) && a.IsDelete == CommonConstants.IsNotDelete)
                        .WhereIf(!string.IsNullOrEmpty(model.TopicYear), (a, b) => (a.UpdateTime.HasValue && rTopicYear.Contains(a.UpdateTime.Value.Year.ToString())) 
                            || (!a.UpdateTime.HasValue && rTopicYear.Contains(a.CreateTime.Year.ToString())))
                        .GroupBy((a, b) => a.QuestionType).ToList(a => new TaskTopicGroupDto
                        {
                            QuestionType = a.Key,
                            Count = a.Count()
                        });
                }
                return new PageResponse<TaskTopicGroupDto> { Code = CommonConstants.SuccessCode, RecordCount = selectData.Count, Data = selectData };
            });
        }

        /// <summary>
        /// 智能组卷
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RandomOrgPaper(TaskRandomOrgPaperDto model, long currentUserId, long tenantId)
        {
            var dtNow = DateTime.Now;

            #region 验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
            if (model.CourseId <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择备课课程" };
            if (model.StartTime > model.CloseTime)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "开始时间不得大于结束时间" };
            if (model.CloseTime < dtNow)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "结束时间不得小于当前时间" };
            if (model.TotalMinutes <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "考试时长必须大于0" };
            if (model.TaskExamSetting == TaskExamSetting.Multiple && (model.ExamCount <= 1 || model.ExamCount > model.ClassDetail.Count))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "选择多人多卷时，试卷数量需大于1小于等于学生数量" };

            if (model.ClassDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择班级学员" };
            if (model.TopicGroupDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请填写题目类型数量" };
            if (model.ClassDetail.GroupBy(m => m.UserId).Count() != model.ClassDetail.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学生信息重复" };
            if (model.ClassDetail.Any(m => m.ClassId == 0))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级ID异常" };
            //if (DbContext.FreeSql.Select<YssxTask>().Any(m => m.Name == model.Name && m.Id != model.Id && m.CreateBy == currentUserId && m.IsDelete == CommonConstants.IsNotDelete))
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "课业任务名称重复" };
            //课程信息相关
            var rPrepareCourseEntity = DbContext.FreeSql.Select<YssxPrepareCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Id == model.CourseId).First();
            if (rPrepareCourseEntity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到备课课程信息" };
            if (!rPrepareCourseEntity.OriginalCourseId.HasValue || rPrepareCourseEntity.OriginalCourseId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "备课课程的原始课程ID为0" };

            #endregion

            bool state = true;
            if (model.TaskExamSetting == TaskExamSetting.One)
            {
                #region 处理数据
                //处理数据
                var maxSort = DbContext.FreeSql.Select<YssxTask>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.TaskType != TaskType.ClassTest)
                    .OrderByDescending(m => m.Sort).First(m => m.Sort);
                var rExamId = IdWorker.NextId();
                //任务
                var rTaskEntity = new YssxTask
                {
                    Id = IdWorker.NextId(),
                    TaskType = model.TaskType,
                    TenantId = tenantId,
                    Name = model.Name,
                    StartTime = model.StartTime,
                    CloseTime = model.CloseTime,
                    TotalMinutes = model.TotalMinutes,
                    CourseId = model.CourseId,
                    OriginalCourseId = rPrepareCourseEntity.OriginalCourseId.Value,
                    Sort = maxSort + 1,
                    Status = LessonsTaskStatus.Wait,
                    Content = model.Content,
                    ExamId = rExamId,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };
                //试卷
                var rPaperEntity = new ExamPaperCourse
                {
                    Id = rExamId,
                    Name = rTaskEntity.Name,
                    TaskId = rTaskEntity.Id,
                    TenantId = tenantId,
                    CourseId = rTaskEntity.CourseId,
                    ExamType = rTaskEntity.TaskType == TaskType.ClassExam ? CourseExamType.ClassExam : (rTaskEntity.TaskType == TaskType.ClassTask ? CourseExamType.ClassTask : CourseExamType.ClassHomeWork),
                    CanShowAnswerBeforeEnd = true,
                    TotalScore = 0,
                    TotalQuestionCount = model.TopicGroupDetail.Sum(m => m.Count),
                    TotalMinutes = rTaskEntity.TotalMinutes,
                    BeginTime = rTaskEntity.StartTime,
                    EndTime = rTaskEntity.CloseTime,
                    ReleaseTime = dtNow,
                    Sort = rTaskEntity.Sort,
                    IsRelease = true,
                    Status = ExamStatus.Wait,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };
                //学生
                var rClassStudent = model.ClassDetail.Select(m => new YssxTaskStudent
                {
                    Id = IdWorker.NextId(),
                    TaskId = rTaskEntity.Id,
                    ClassId = m.ClassId,
                    UserId = m.UserId,
                    StudentId = m.StudentId,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                }).ToList();
                //题目
                #region 查找范围内所有题目
                var rTopicYear = string.IsNullOrEmpty(model.TopicYear) ? new List<string>() : model.TopicYear.Split(',').ToList();
                var selectTopicData = new List<YssxTopicPublic>();
                if (model.DataType == 1)
                {
                    //知识点
                    //查询题目列表
                    foreach (var item in model.SurveyScopeDetail)
                    {
                        var rTopicDetail = DbContext.FreeSql.GetRepository<YssxTopicPublic>()
                            .Where(x => x.KnowledgePointIds.IndexOf(item.ToString()) >= 0 && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                            .WhereIf(!string.IsNullOrEmpty(model.TopicYear), x => (x.UpdateTime.HasValue && rTopicYear.Contains(x.UpdateTime.Value.Year.ToString()))
                                || (!x.UpdateTime.HasValue && rTopicYear.Contains(x.CreateTime.Year.ToString()))).ToList();
                        if (rTopicDetail.Count > 0) selectTopicData.AddRange(rTopicDetail);
                    }
                    if (selectTopicData.Count > 0)
                    {
                        //去重
                        selectTopicData = selectTopicData.GroupBy(m => new { m.Id, m.QuestionType, m.Score, m.Sort }).Select(a => new YssxTopicPublic
                        {
                            Id = a.Key.Id,
                            QuestionType = a.Key.QuestionType,
                            Score = a.Key.Score,
                            Sort = a.Key.Sort
                        }).ToList();
                    }
                }
                else
                {
                    //章节
                    selectTopicData = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Select
                        .From<YssxPrepareSectionTopic>((a, b) => a.InnerJoin(aa => aa.Id == b.QuestionId && b.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => model.SurveyScopeDetail.Contains(b.SectionId) && a.IsDelete == CommonConstants.IsNotDelete)
                        .WhereIf(!string.IsNullOrEmpty(model.TopicYear), (a, b) => (a.UpdateTime.HasValue && rTopicYear.Contains(a.UpdateTime.Value.Year.ToString()))
                            || (!a.UpdateTime.HasValue && rTopicYear.Contains(a.CreateTime.Year.ToString())))
                        .GroupBy((a, b) => new { a.Id, a.QuestionType }).ToList(a => new YssxTopicPublic
                        {
                            Id = a.Key.Id,
                            QuestionType = a.Key.QuestionType,
                            Score = a.Value.Item1.Score,
                            Sort = a.Value.Item1.Sort
                        });
                }
                if (selectTopicData.Count == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "组卷范围内可选题目数量为0" };
                #endregion

                #region 取试卷题目
                var rTaskTopicData = new List<YssxTaskTopic>();
                foreach (var item in model.TopicGroupDetail)
                {
                    //随机排序  new Random().NextDouble()    Guid.NewGuid()
                    var rAddTopicData = selectTopicData.Where(m => m.QuestionType == item.QuestionType).OrderBy(m => new Random().NextDouble()).Take(item.Count).Select(m => new YssxTaskTopic
                    {

                        Id = IdWorker.NextId(),
                        TaskId = rTaskEntity.Id,
                        TopicId = m.Id,
                        QuestionType = m.QuestionType,
                        Score = m.Score,
                        Sort = m.Sort,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    rTaskTopicData.AddRange(rAddTopicData);
                }
                var totalScore = rTaskTopicData.Sum(m => m.Score);
                rPaperEntity.TotalScore = totalScore;
                #endregion
                //作答记录
                var rExamGradeData = model.ClassDetail.Select(m => new ExamCourseUserGrade
                {
                    Id = IdWorker.NextId(),
                    ExamId = rPaperEntity.Id,
                    CourseId = rPaperEntity.CourseId,
                    SectionId = rPaperEntity.SectionId,
                    TaskId = rPaperEntity.TaskId,
                    ExamPaperScore = rPaperEntity.TotalScore,
                    LeftSeconds = rPaperEntity.TotalMinutes * 60,
                    RecordTime = rPaperEntity.BeginTime,
                    UserId = m.UserId,
                    TenantId = tenantId,
                    StudentId = m.StudentId,
                    Status = StudentExamStatus.Wait,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                }).ToList();

                #endregion

                #region 新增数据
                return await Task.Run(() =>
                {
                    var taskEntity = DbContext.FreeSql.GetRepository<YssxTask>().Insert(rTaskEntity);
                    var paperEntity = DbContext.FreeSql.GetRepository<ExamPaperCourse>().Insert(rPaperEntity);
                    if (rClassStudent.Count > 0)
                        DbContext.FreeSql.Insert<YssxTaskStudent>().AppendData(rClassStudent).ExecuteAffrows();
                    if (rTaskTopicData.Count > 0)
                        DbContext.FreeSql.Insert<YssxTaskTopic>().AppendData(rTaskTopicData).ExecuteAffrows();
                    if (rExamGradeData.Count > 0)
                        DbContext.FreeSql.Insert<ExamCourseUserGrade>().AppendData(rExamGradeData).ExecuteAffrows();

                    state = taskEntity != null;
                    return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
                });
                #endregion
            }
            else
            {
                #region 处理数据
                //处理数据
                var maxSort = DbContext.FreeSql.Select<YssxTask>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.TaskType != TaskType.ClassTest)
                    .OrderByDescending(m => m.Sort).First(m => m.Sort);

                #region 查找范围内所有题目
                var rTopicYear = string.IsNullOrEmpty(model.TopicYear) ? new List<string>() : model.TopicYear.Split(',').ToList();
                var selectTopicData = new List<YssxTopicPublic>();
                if (model.DataType == 1)
                {
                    //知识点
                    //查询题目列表
                    foreach (var item in model.SurveyScopeDetail)
                    {
                        var rTopicDetail = DbContext.FreeSql.GetRepository<YssxTopicPublic>()
                            .Where(x => x.KnowledgePointIds.IndexOf(item.ToString()) >= 0 && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                            .WhereIf(!string.IsNullOrEmpty(model.TopicYear), x => (x.UpdateTime.HasValue && rTopicYear.Contains(x.UpdateTime.Value.Year.ToString())) || (!x.UpdateTime.HasValue && rTopicYear.Contains(x.CreateTime.Year.ToString()))).ToList();
                        if (rTopicDetail.Count > 0) selectTopicData.AddRange(rTopicDetail);
                    }
                    if (selectTopicData.Count > 0)
                    {
                        //去重
                        selectTopicData = selectTopicData.GroupBy(m => new { m.Id, m.QuestionType, m.Score, m.Sort }).Select(a => new YssxTopicPublic
                        {
                            Id = a.Key.Id,
                            QuestionType = a.Key.QuestionType,
                            Score = a.Key.Score,
                            Sort = a.Key.Sort
                        }).ToList();
                    }
                }
                else
                {
                    //章节
                    selectTopicData = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Select
                        .From<YssxPrepareSectionTopic>((a, b) => a.InnerJoin(aa => aa.Id == b.QuestionId && b.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => model.SurveyScopeDetail.Contains(b.SectionId) && a.IsDelete == CommonConstants.IsNotDelete)
                        .WhereIf(!string.IsNullOrEmpty(model.TopicYear), (a, b) => (a.UpdateTime.HasValue && rTopicYear.Contains(a.UpdateTime.Value.Year.ToString())) || (!a.UpdateTime.HasValue && rTopicYear.Contains(a.CreateTime.Year.ToString())))
                        .GroupBy((a, b) => new { a.Id, a.QuestionType }).ToList(a => new YssxTopicPublic
                        {
                            Id = a.Key.Id,
                            QuestionType = a.Key.QuestionType,
                            Score = a.Value.Item1.Score,
                            Sort = a.Value.Item1.Sort
                        });
                }
                if (selectTopicData.Count == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "组卷范围内可选题目数量为0" };
                #endregion

                //循环生成任务
                int rStudentQty = Convert.ToInt32(Math.Floor(model.ClassDetail.Count * 1.0 / model.ExamCount));
                int rTakeCount = rStudentQty;
                List<YssxTask> listTask = new List<YssxTask>();
                List<ExamPaperCourse> listPaperCourse = new List<ExamPaperCourse>();
                List<YssxTaskStudent> listTaskStudent = new List<YssxTaskStudent>();
                List<YssxTaskTopic> listTaskTopic = new List<YssxTaskTopic>();
                List<ExamCourseUserGrade> listExamGrade = new List<ExamCourseUserGrade>();
                for (int i = 1; i < model.ExamCount + 1; i++)
                {
                    var rExamId = IdWorker.NextId();
                    if (i == model.ExamCount)
                        rTakeCount = model.ClassDetail.Count - (i - 1) * rStudentQty;
                    //任务
                    var rTaskEntity = new YssxTask
                    {
                        Id = IdWorker.NextId(),
                        TaskType = model.TaskType,
                        TenantId = tenantId,
                        Name = model.Name + "-" + i,
                        StartTime = model.StartTime,
                        CloseTime = model.CloseTime,
                        TotalMinutes = model.TotalMinutes,
                        CourseId = model.CourseId,
                        OriginalCourseId = rPrepareCourseEntity.OriginalCourseId.Value,
                        Sort = maxSort + i,
                        Status = LessonsTaskStatus.Wait,
                        Content = model.Content,
                        ExamId = rExamId,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    };
                    //试卷
                    var rPaperEntity = new ExamPaperCourse
                    {
                        Id = rExamId,
                        Name = rTaskEntity.Name,
                        TaskId = rTaskEntity.Id,
                        TenantId = tenantId,
                        CourseId = rTaskEntity.CourseId,
                        ExamType = rTaskEntity.TaskType == TaskType.ClassExam ? CourseExamType.ClassExam : (rTaskEntity.TaskType == TaskType.ClassTask ? CourseExamType.ClassTask : CourseExamType.ClassHomeWork),
                        CanShowAnswerBeforeEnd = true,
                        TotalScore = 0,
                        TotalQuestionCount = model.TopicGroupDetail.Sum(m => m.Count),
                        TotalMinutes = rTaskEntity.TotalMinutes,
                        BeginTime = rTaskEntity.StartTime,
                        EndTime = rTaskEntity.CloseTime,
                        ReleaseTime = dtNow,
                        Sort = rTaskEntity.Sort,
                        IsRelease = true,
                        Status = ExamStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    };
                    //学生
                    var rClassStudent = model.ClassDetail.Skip((i - 1) * rStudentQty).Take(rTakeCount).Select(m => new YssxTaskStudent
                    {
                        Id = IdWorker.NextId(),
                        TaskId = rTaskEntity.Id,
                        UserId = m.UserId,
                        ClassId = m.ClassId,
                        StudentId = m.StudentId,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //题目
                    #region 取试卷题目
                    var rTaskTopicData = new List<YssxTaskTopic>();
                    foreach (var item in model.TopicGroupDetail)
                    {
                        //随机排序  new Random().NextDouble()    Guid.NewGuid()
                        var rAddTopicData = selectTopicData.Where(m => m.QuestionType == item.QuestionType).OrderBy(m => new Random().NextDouble()).Take(item.Count).Select(m => new YssxTaskTopic
                        {
                            Id = IdWorker.NextId(),
                            TaskId = rTaskEntity.Id,
                            TopicId = m.Id,
                            QuestionType = m.QuestionType,
                            Score = m.Score,
                            Sort = m.Sort,
                            CreateBy = currentUserId,
                            UpdateBy = currentUserId,
                            UpdateTime = dtNow
                        }).ToList();
                        rTaskTopicData.AddRange(rAddTopicData);
                    }
                    if (rTaskTopicData.Count == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = string.Format("任务【{0}】随机选择的题目数量为0", rTaskEntity.Name) };
                    var totalScore = rTaskTopicData.Sum(m => m.Score);
                    rPaperEntity.TotalScore = totalScore;
                    #endregion
                    //作答记录
                    var rExamGradeData = model.ClassDetail.Skip((i - 1) * rStudentQty).Take(rTakeCount).Select(m => new ExamCourseUserGrade
                    {
                        Id = IdWorker.NextId(),
                        ExamId = rPaperEntity.Id,
                        CourseId = rPaperEntity.CourseId,
                        SectionId = rPaperEntity.SectionId,
                        TaskId = rPaperEntity.TaskId,
                        ExamPaperScore = rPaperEntity.TotalScore,
                        LeftSeconds = rPaperEntity.TotalMinutes * 60,
                        RecordTime = rPaperEntity.BeginTime,
                        UserId = m.UserId,
                        TenantId = tenantId,
                        StudentId = m.StudentId,
                        Status = StudentExamStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();

                    #region 收集数据
                    listTask.Add(rTaskEntity);
                    listPaperCourse.Add(rPaperEntity);
                    listTaskStudent.AddRange(rClassStudent);
                    listTaskTopic.AddRange(rTaskTopicData);
                    listExamGrade.AddRange(rExamGradeData);
                    #endregion
                }
                #endregion

                #region 新增数据
                return await Task.Run(() =>
                {
                    var taskRes = DbContext.FreeSql.Insert<YssxTask>().AppendData(listTask).ExecuteAffrows();
                    var paperRes = DbContext.FreeSql.Insert<ExamPaperCourse>().AppendData(listPaperCourse).ExecuteAffrows();
                    if (listTaskStudent.Count > 0)
                        DbContext.FreeSql.Insert<YssxTaskStudent>().AppendData(listTaskStudent).ExecuteAffrows();
                    if (listTaskTopic.Count > 0)
                        DbContext.FreeSql.Insert<YssxTaskTopic>().AppendData(listTaskTopic).ExecuteAffrows();
                    if (listExamGrade.Count > 0)
                        DbContext.FreeSql.Insert<ExamCourseUserGrade>().AppendData(listExamGrade).ExecuteAffrows();

                    state = taskRes > 0;
                    return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
                });
                #endregion
            }
        }

        #endregion


        #region 组卷 - new
        /// <summary>
        /// 手动组卷 - new
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ManualCreatePaper(TaskManualCreatePaperDto model, long currentUserId, long tenantId)
        {
            var dtNow = DateTime.Now;
            #region 校验
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
            if (model.OriginalCourseId <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择课程" };
            if (model.StartTime > model.CloseTime)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "开始时间不得大于结束时间" };
            if (model.CloseTime < dtNow)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "结束时间不得小于当前时间" };
            if (model.TotalMinutes <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "考试时长必须大于0" };
            if (model.Id==0 && model.ClassDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择班级学员" };
            if (model.TopicDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择题目" };
            if (model.ClassDetail.GroupBy(m => m.UserId).Count() != model.ClassDetail.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学生信息重复" };
            if (model.ClassDetail.Any(m => m.ClassId == 0))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级ID异常" };
            //if (DbContext.FreeSql.Select<YssxTask>().Any(m => m.Name == model.Name && m.Id != model.Id && m.CreateBy == currentUserId && m.IsDelete == CommonConstants.IsNotDelete))
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "课业任务名称重复" };

            if (model.TotalScore <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "总分必须大于0" };
            if (model.TopicDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择题目" };
            if (model.TopicDetail.GroupBy(m => m.TopicId).Count() != model.TopicDetail.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "题目信息重复" };
            var totalScore = model.TopicDetail.Sum(m => m.Score);
            if (model.TotalScore != totalScore)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = $"卷面分与考核分数不一致，考核总分为{ totalScore }分，卷面分为{ model.TotalScore }" };
            //课程信息相关
            var rCourseEntity = DbContext.FreeSql.Select<YssxCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Id == model.OriginalCourseId).First();
            if (rCourseEntity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到课程信息" };

            #endregion

            return await Task.Run(() =>
            {
                bool state = true;
                //处理数据
                if (model.Id == 0)
                {
                    #region 处理数据
                    var maxSort = DbContext.FreeSql.Select<YssxTask>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.TaskType != TaskType.ClassTest)
                        .OrderByDescending(m => m.Sort).First(m => m.Sort);
                    var rExamId = IdWorker.NextId();
                    //课业任务
                    var rTaskEntity = new YssxTask
                    {
                        Id = IdWorker.NextId(),
                        TaskType = model.TaskType,
                        TenantId = tenantId,
                        Name = model.Name,
                        StartTime = model.StartTime,
                        CloseTime = model.CloseTime,
                        TotalMinutes = model.TotalMinutes,
                        CourseId = 0,
                        OriginalCourseId = model.OriginalCourseId,
                        Sort = maxSort + 1,
                        Content = model.Content,
                        ExamId = rExamId,
                        Status = LessonsTaskStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow,
                        IsCanCheckExam = model.IsCanCheckExam,
                    };
                    //试卷
                    ExamPaperCourse rPaperEntity = new ExamPaperCourse
                    {
                        Id = rExamId,
                        Name = rTaskEntity.Name,
                        TaskId = rTaskEntity.Id,
                        TenantId = tenantId,
                        CourseId = rTaskEntity.CourseId,
                        OriginalCourseId = rTaskEntity.OriginalCourseId,
                        ExamType = rTaskEntity.TaskType == TaskType.ClassExam ? CourseExamType.ClassExam : (rTaskEntity.TaskType == TaskType.ClassTask ? CourseExamType.ClassTask : CourseExamType.ClassHomeWork),
                        CanShowAnswerBeforeEnd = true,
                        TotalScore = totalScore,
                        TotalQuestionCount = model.TopicDetail.Count,
                        TotalMinutes = rTaskEntity.TotalMinutes,
                        BeginTime = rTaskEntity.StartTime,
                        EndTime = rTaskEntity.CloseTime,
                        ReleaseTime = dtNow,
                        Sort = rTaskEntity.Sort,
                        IsRelease = true,
                        Status = ExamStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    };
                    //学生
                    var rClassStudent = model.ClassDetail.Select(m => new YssxTaskStudent
                    {
                        Id = IdWorker.NextId(),
                        TaskId = rTaskEntity.Id,
                        ClassId = m.ClassId,
                        UserId = m.UserId,
                        StudentId = m.StudentId,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //题目
                    var rAddTopicData = model.TopicDetail.Select(m => new YssxTaskTopic
                    {
                        Id = IdWorker.NextId(),
                        TaskId = rTaskEntity.Id,
                        TopicId = m.TopicId,
                        QuestionType = m.QuestionType,
                        Score = m.Score,
                        Sort = m.Sort,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //作答记录
                    var rExamGradeData = model.ClassDetail.Select(m => new ExamCourseUserGrade
                    {
                        Id = IdWorker.NextId(),
                        ExamId = rPaperEntity.Id,
                        CourseId = rPaperEntity.CourseId,
                        OriginalCourseId = rPaperEntity.OriginalCourseId,
                        SectionId = rPaperEntity.SectionId,
                        TaskId = rPaperEntity.TaskId,
                        ExamPaperScore = rPaperEntity.TotalScore,
                        LeftSeconds = rPaperEntity.TotalMinutes * 60,
                        UserId = m.UserId,
                        TenantId = tenantId,
                        StudentId = m.StudentId,
                        Status = StudentExamStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();

                    #endregion

                    #region 新增数据
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        //课业任务
                        var taskEntity = DbContext.FreeSql.GetRepository<YssxTask>().Insert(rTaskEntity);
                        //试卷
                        var paperEntity = DbContext.FreeSql.GetRepository<ExamPaperCourse>().Insert(rPaperEntity);
                        //学生
                        if (rClassStudent.Count > 0)
                            DbContext.FreeSql.Insert<YssxTaskStudent>().AppendData(rClassStudent).ExecuteAffrows();
                        //题目
                        if (rAddTopicData.Count > 0)
                            DbContext.FreeSql.Insert<YssxTaskTopic>().AppendData(rAddTopicData).ExecuteAffrows();
                        //作答记录
                        if (rExamGradeData.Count > 0)
                            DbContext.FreeSql.Insert<ExamCourseUserGrade>().AppendData(rExamGradeData).ExecuteAffrows();

                        state = paperEntity != null;
                        uow.Commit();
                    }
                    #endregion
                }
                else
                {
                    #region 处理数据
                    var rTaskEntity = DbContext.FreeSql.Select<YssxTask>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == model.Id).First();
                    if (rTaskEntity == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到课业任务" };

                    ExamPaperCourse rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == rTaskEntity.ExamId).First();
                    if (rPaperEntity == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };

                    //课业任务
                    //rTaskEntity.Status = LessonsTaskStatus.Wait;
                    rTaskEntity.Name = model.Name;
                    rTaskEntity.StartTime = model.StartTime;
                    rTaskEntity.CloseTime = model.CloseTime;
                    rTaskEntity.TotalMinutes = model.TotalMinutes;
                    rTaskEntity.OriginalCourseId = model.OriginalCourseId;
                    rTaskEntity.Content = model.Content;
                    rTaskEntity.UpdateBy = currentUserId;
                    rTaskEntity.UpdateTime = dtNow;
                    //试卷
                    //rPaperEntity.Status = ExamStatus.Wait;
                    rPaperEntity.BeginTime = rTaskEntity.StartTime;
                    rPaperEntity.EndTime = rTaskEntity.CloseTime;
                    rPaperEntity.OriginalCourseId = model.OriginalCourseId;
                    rPaperEntity.TotalScore = totalScore;
                    rPaperEntity.TotalQuestionCount = model.TopicDetail.Count;
                    rPaperEntity.TotalMinutes = model.TotalMinutes;
                    rPaperEntity.UpdateBy = currentUserId;
                    rPaperEntity.UpdateTime = dtNow;

                    #region 学生
                    //var rOldStudent = DbContext.FreeSql.GetRepository<YssxTaskStudent>().Where(x => x.TaskId == model.Id && x.IsDelete == CommonConstants.IsNotDelete).ToList();
                    ////取待删除的学生
                    //var rDelExamGradeData = new List<ExamCourseUserGrade>();
                    //var rClassStudentId = model.ClassDetail.Select(m => m.UserId).ToList();
                    //var delStudentData = rOldStudent.Where(m => !rClassStudentId.Contains(m.UserId)).ToList();
                    //if (delStudentData.Count > 0)
                    //{
                    //    delStudentData.ForEach(a =>
                    //    {
                    //        a.IsDelete = CommonConstants.IsDelete;
                    //        a.UpdateBy = currentUserId;
                    //        a.UpdateTime = dtNow;
                    //    });
                    //    //取待删除作答记录
                    //    var rDelStudentId = delStudentData.Select(m => m.UserId).ToList();
                    //    rDelExamGradeData = DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(x => x.TaskId == model.Id && rDelStudentId.Contains(x.UserId) && x.IsDelete == CommonConstants.IsNotDelete).ToList(m => new ExamCourseUserGrade
                    //    {
                    //        Id = m.Id,
                    //        IsDelete = CommonConstants.IsDelete,
                    //        UpdateBy = currentUserId,
                    //        UpdateTime = dtNow
                    //    });
                    //}
                    ////取待新增的学生
                    //var rAddExamGradeData = new List<ExamCourseUserGrade>();
                    //var rOldClassStudentId = rOldStudent.Select(m => m.UserId).ToList();
                    //var addStudentData = model.ClassDetail.Where(m => !rOldClassStudentId.Contains(m.UserId)).Select(m => new YssxTaskStudent
                    //{
                    //    Id = IdWorker.NextId(),
                    //    TaskId = rTaskEntity.Id,
                    //    ClassId = m.ClassId,
                    //    UserId = m.UserId,
                    //    StudentId = m.StudentId,
                    //    CreateBy = currentUserId,
                    //    UpdateBy = currentUserId,
                    //    UpdateTime = dtNow
                    //}).ToList();
                    //if (addStudentData.Count > 0 && rTaskEntity.ExamId > 0)
                    //{
                    //    rAddExamGradeData = addStudentData.Select(m => new ExamCourseUserGrade
                    //    {
                    //        Id = IdWorker.NextId(),
                    //        ExamId = rPaperEntity.Id,
                    //        CourseId = 0,
                    //        OriginalCourseId = rPaperEntity.OriginalCourseId,
                    //        SectionId = rPaperEntity.SectionId,
                    //        TaskId = rPaperEntity.TaskId,
                    //        ExamPaperScore = rPaperEntity.TotalScore,
                    //        LeftSeconds = rPaperEntity.TotalMinutes * 60,
                    //        UserId = m.UserId,
                    //        TenantId = tenantId,
                    //        StudentId = m.StudentId,
                    //        Status = StudentExamStatus.Wait,
                    //        CreateBy = currentUserId,
                    //        UpdateBy = currentUserId,
                    //        UpdateTime = dtNow
                    //    }).ToList();
                    //}
                    #endregion

                    #region 题目
                    //取新增的题目
                    var rAddTopicData = model.TopicDetail.Where(m => m.Id == 0).Select(m => new YssxTaskTopic
                    {
                        Id = IdWorker.NextId(),
                        TaskId = rTaskEntity.Id,
                        TopicId = m.TopicId,
                        QuestionType = m.QuestionType,
                        Score = m.Score,
                        Sort = m.Sort,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //取修改的题目
                    var rUpdTopicData = model.TopicDetail.Where(m => m.Id > 0).Select(m => new YssxTaskTopic
                    {
                        Id = m.Id,
                        Sort = m.Sort,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //取删除的题目
                    var rNewTopicId = model.TopicDetail.Select(m => m.TopicId).ToList();
                    var rDelTopicData = DbContext.FreeSql.Select<YssxTaskTopic>()
                        .Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.TaskId == rTaskEntity.Id && !rNewTopicId.Contains(m.TopicId))
                        .ToList(m => new YssxTaskTopic
                        {
                            Id = m.Id,
                            IsDelete = CommonConstants.IsDelete,
                            UpdateBy = currentUserId,
                            UpdateTime = dtNow
                        });
                    #endregion

                    #endregion

                    #region 更新数据
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        //课业任务
                        DbContext.FreeSql.Update<YssxTask>().SetSource(rTaskEntity).UpdateColumns(a => new
                        {
                            a.Name,
                            a.StartTime,
                            a.CloseTime,
                            a.TotalMinutes,
                            a.OriginalCourseId,
                            a.Content,
                            a.UpdateBy,
                            a.UpdateTime
                        }).ExecuteAffrows();
                        //试卷信息
                        var resUpd = DbContext.FreeSql.Update<ExamPaperCourse>().SetSource(rPaperEntity).UpdateColumns(a => new
                        {
                            a.BeginTime,
                            a.EndTime,
                            a.OriginalCourseId,
                            a.TotalScore,
                            a.TotalQuestionCount,
                            a.TotalMinutes,
                            a.UpdateBy,
                            a.UpdateTime
                        }).ExecuteAffrows();
                        ////删除的学生
                        //if (delStudentData.Count > 0)
                        //    DbContext.FreeSql.Update<YssxTaskStudent>().SetSource(delStudentData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                        ////删除的作答记录
                        //if (rDelExamGradeData.Count > 0)
                        //    DbContext.FreeSql.Update<ExamCourseUserGrade>().SetSource(rDelExamGradeData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                        ////新增的学生
                        //if (addStudentData.Count > 0)
                        //    DbContext.FreeSql.Insert<YssxTaskStudent>().AppendData(addStudentData).ExecuteAffrows();
                        ////新增的作答记录
                        //if (rAddExamGradeData.Count > 0)
                        //    DbContext.FreeSql.Insert<ExamCourseUserGrade>().AppendData(rAddExamGradeData).ExecuteAffrows();
                        //新增的题目
                        if (rAddTopicData.Count > 0)
                            DbContext.FreeSql.Insert<YssxTaskTopic>().AppendData(rAddTopicData).ExecuteAffrows();
                        //修改的题目
                        if (rUpdTopicData.Count > 0)
                            DbContext.FreeSql.Update<YssxTaskTopic>().SetSource(rUpdTopicData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.Sort }).ExecuteAffrows();
                        //删除的题目
                        if (rDelTopicData.Count > 0)
                            DbContext.FreeSql.Update<YssxTaskTopic>().SetSource(rDelTopicData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();

                        state = resUpd > 0;
                        uow.Commit();
                    }
                    #endregion
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 查询题目类型数量统计（1.知识点 2.课程章节） - new
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<TaskTopicGroupDto>> GetTaskTopicGroup(GetTaskTopicGroupRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                if (model.SurveyScopeDetail.Count == 0)
                    return new PageResponse<TaskTopicGroupDto> { Code = CommonConstants.ErrorCode, Msg = "查询失败，请选择考核范围!" };

                var selectData = new List<TaskTopicGroupDto>();
                var rTopicYear = string.IsNullOrEmpty(model.TopicYear) ? new List<string>() : model.TopicYear.Split(',').ToList();
                if (model.DataType == 1)
                {
                    //知识点
                    //查询题目列表
                    var rYssxTopicPublicData = new List<YssxTopicPublic>();
                    foreach (var item in model.SurveyScopeDetail)
                    {
                        var rTopicDetail = DbContext.FreeSql.GetRepository<YssxTopicPublic>()
                            .Where(x => x.KnowledgePointIds.IndexOf(item.ToString()) >= 0 && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                            .WhereIf(!string.IsNullOrEmpty(model.TopicYear), x => (x.UpdateTime.HasValue && rTopicYear.Contains(x.UpdateTime.Value.Year.ToString()))
                                || (!x.UpdateTime.HasValue && rTopicYear.Contains(x.CreateTime.Year.ToString()))).ToList();
                        if (rTopicDetail.Count > 0) rYssxTopicPublicData.AddRange(rTopicDetail);
                    }
                    if (rYssxTopicPublicData.Count > 0)
                    {
                        //去重
                        rYssxTopicPublicData = rYssxTopicPublicData.GroupBy(m => new { m.Id, m.QuestionType }).Select(a => new YssxTopicPublic
                        {
                            Id = a.Key.Id,
                            QuestionType = a.Key.QuestionType
                        }).ToList();
                        //筛选题目分组数据
                        selectData = rYssxTopicPublicData.GroupBy(m => m.QuestionType).Select(a => new TaskTopicGroupDto
                        {
                            QuestionType = a.Key,
                            Count = a.Count()
                        }).ToList();
                    }
                }
                else
                {
                    //章节
                    selectData = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Select
                        .From<YssxSectionTopic>((a, b) => a.InnerJoin(aa => aa.Id == b.QuestionId && b.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => model.SurveyScopeDetail.Contains(b.SectionId) && a.IsDelete == CommonConstants.IsNotDelete)
                        .WhereIf(!string.IsNullOrEmpty(model.TopicYear), (a, b) => (a.UpdateTime.HasValue && rTopicYear.Contains(a.UpdateTime.Value.Year.ToString()))
                            || (!a.UpdateTime.HasValue && rTopicYear.Contains(a.CreateTime.Year.ToString())))
                        .GroupBy((a, b) => a.QuestionType).ToList(a => new TaskTopicGroupDto
                        {
                            QuestionType = a.Key,
                            Count = a.Count()
                        });
                }
                return new PageResponse<TaskTopicGroupDto> { Code = CommonConstants.SuccessCode, RecordCount = selectData.Count, Data = selectData };
            });
        }

        /// <summary>
        /// 智能组卷 - new
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RandomCreatePaper(TaskRandomCreatePaperDto model, long currentUserId, long tenantId)
        {
            var dtNow = DateTime.Now;

            #region 验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
            if (model.OriginalCourseId <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择课程" };
            if (model.StartTime > model.CloseTime)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "开始时间不得大于结束时间" };
            if (model.CloseTime < dtNow)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "结束时间不得小于当前时间" };
            if (model.TotalMinutes <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "考试时长必须大于0" };
            if (model.TaskExamSetting == TaskExamSetting.Multiple && (model.ExamCount <= 1 || model.ExamCount > model.ClassDetail.Count))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "选择多人多卷时，试卷数量需大于1小于等于学生数量" };

            if (model.ClassDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择班级学员" };
            if (model.TopicGroupDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请填写题目类型数量" };
            if (model.ClassDetail.GroupBy(m => m.UserId).Count() != model.ClassDetail.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学生信息重复" };
            if (model.ClassDetail.Any(m => m.ClassId == 0))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级ID异常" };
            //if (DbContext.FreeSql.Select<YssxTask>().Any(m => m.Name == model.Name && m.Id != model.Id && m.CreateBy == currentUserId && m.IsDelete == CommonConstants.IsNotDelete))
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "课业任务名称重复" };
            //课程信息相关
            var rCourseEntity = DbContext.FreeSql.Select<YssxCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Id == model.OriginalCourseId).First();
            if (rCourseEntity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到课程信息" };

            #endregion

            bool state = true;
            if (model.TaskExamSetting == TaskExamSetting.One)
            {
                #region 处理数据
                //处理数据
                var maxSort = DbContext.FreeSql.Select<YssxTask>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.TaskType != TaskType.ClassTest)
                    .OrderByDescending(m => m.Sort).First(m => m.Sort);
                var rExamId = IdWorker.NextId();
                //任务
                var rTaskEntity = new YssxTask
                {
                    Id = IdWorker.NextId(),
                    TaskType = model.TaskType,
                    TenantId = tenantId,
                    Name = model.Name,
                    StartTime = model.StartTime,
                    CloseTime = model.CloseTime,
                    TotalMinutes = model.TotalMinutes,
                    CourseId = 0,
                    OriginalCourseId = model.OriginalCourseId,
                    Sort = maxSort + 1,
                    Status = LessonsTaskStatus.Wait,
                    Content = model.Content,
                    ExamId = rExamId,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow,
                    IsCanCheckExam = model.IsCanCheckExam,
                };
                //试卷
                var rPaperEntity = new ExamPaperCourse
                {
                    Id = rExamId,
                    Name = rTaskEntity.Name,
                    TaskId = rTaskEntity.Id,
                    TenantId = tenantId,
                    CourseId = rTaskEntity.CourseId,
                    OriginalCourseId = rTaskEntity.OriginalCourseId,
                    ExamType = rTaskEntity.TaskType == TaskType.ClassExam ? CourseExamType.ClassExam : (rTaskEntity.TaskType == TaskType.ClassTask ? CourseExamType.ClassTask : CourseExamType.ClassHomeWork),
                    CanShowAnswerBeforeEnd = true,
                    TotalScore = 0,
                    TotalQuestionCount = model.TopicGroupDetail.Sum(m => m.Count),
                    TotalMinutes = rTaskEntity.TotalMinutes,
                    BeginTime = rTaskEntity.StartTime,
                    EndTime = rTaskEntity.CloseTime,
                    ReleaseTime = dtNow,
                    Sort = rTaskEntity.Sort,
                    IsRelease = true,
                    Status = ExamStatus.Wait,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };
                //学生
                var rClassStudent = model.ClassDetail.Select(m => new YssxTaskStudent
                {
                    Id = IdWorker.NextId(),
                    TaskId = rTaskEntity.Id,
                    ClassId = m.ClassId,
                    UserId = m.UserId,
                    StudentId = m.StudentId,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                }).ToList();
                //题目
                #region 查找范围内所有题目
                var rTopicYear = string.IsNullOrEmpty(model.TopicYear) ? new List<string>() : model.TopicYear.Split(',').ToList();
                var selectTopicData = new List<YssxTopicPublic>();
                if (model.DataType == 1)
                {
                    //知识点
                    //查询题目列表
                    foreach (var item in model.SurveyScopeDetail)
                    {
                        var rTopicDetail = DbContext.FreeSql.GetRepository<YssxTopicPublic>()
                            .Where(x => x.KnowledgePointIds.IndexOf(item.ToString()) >= 0 && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                            .WhereIf(!string.IsNullOrEmpty(model.TopicYear), x => (x.UpdateTime.HasValue && rTopicYear.Contains(x.UpdateTime.Value.Year.ToString()))
                                || (!x.UpdateTime.HasValue && rTopicYear.Contains(x.CreateTime.Year.ToString()))).ToList();
                        if (rTopicDetail.Count > 0) selectTopicData.AddRange(rTopicDetail);
                    }
                    if (selectTopicData.Count > 0)
                    {
                        //去重
                        selectTopicData = selectTopicData.GroupBy(m => new { m.Id, m.QuestionType, m.Score, m.Sort }).Select(a => new YssxTopicPublic
                        {
                            Id = a.Key.Id,
                            QuestionType = a.Key.QuestionType,
                            Score = a.Key.Score,
                            Sort = a.Key.Sort
                        }).ToList();
                    }
                }
                else
                {
                    //章节
                    selectTopicData = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Select
                        .From<YssxSectionTopic>((a, b) => a.InnerJoin(aa => aa.Id == b.QuestionId && b.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => model.SurveyScopeDetail.Contains(b.SectionId) && a.IsDelete == CommonConstants.IsNotDelete)
                        .WhereIf(!string.IsNullOrEmpty(model.TopicYear), (a, b) => (a.UpdateTime.HasValue && rTopicYear.Contains(a.UpdateTime.Value.Year.ToString()))
                            || (!a.UpdateTime.HasValue && rTopicYear.Contains(a.CreateTime.Year.ToString())))
                        .GroupBy((a, b) => new { a.Id, a.QuestionType }).ToList(a => new YssxTopicPublic
                        {
                            Id = a.Key.Id,
                            QuestionType = a.Key.QuestionType,
                            Score = a.Value.Item1.Score,
                            Sort = a.Value.Item1.Sort
                        });
                }
                if (selectTopicData.Count == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "组卷范围内可选题目数量为0" };
                #endregion

                #region 取试卷题目
                var rTaskTopicData = new List<YssxTaskTopic>();
                foreach (var item in model.TopicGroupDetail)
                {
                    //随机排序  new Random().NextDouble()    Guid.NewGuid()
                    var rAddTopicData = selectTopicData.Where(m => m.QuestionType == item.QuestionType).OrderBy(m => new Random().NextDouble()).Take(item.Count).Select(m => new YssxTaskTopic
                    {

                        Id = IdWorker.NextId(),
                        TaskId = rTaskEntity.Id,
                        TopicId = m.Id,
                        QuestionType = m.QuestionType,
                        Score = m.Score,
                        Sort = m.Sort,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    rTaskTopicData.AddRange(rAddTopicData);
                }
                var totalScore = rTaskTopicData.Sum(m => m.Score);
                rPaperEntity.TotalScore = totalScore;
                #endregion
                //作答记录
                var rExamGradeData = model.ClassDetail.Select(m => new ExamCourseUserGrade
                {
                    Id = IdWorker.NextId(),
                    ExamId = rPaperEntity.Id,
                    CourseId = rPaperEntity.CourseId,
                    OriginalCourseId = rPaperEntity.OriginalCourseId,
                    SectionId = rPaperEntity.SectionId,
                    TaskId = rPaperEntity.TaskId,
                    ExamPaperScore = rPaperEntity.TotalScore,
                    LeftSeconds = rPaperEntity.TotalMinutes * 60,
                    RecordTime = rPaperEntity.BeginTime,
                    UserId = m.UserId,
                    TenantId = tenantId,
                    StudentId = m.StudentId,
                    Status = StudentExamStatus.Wait,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                }).ToList();

                #endregion

                #region 新增数据
                return await Task.Run(() =>
                {
                    var taskEntity = DbContext.FreeSql.GetRepository<YssxTask>().Insert(rTaskEntity);
                    var paperEntity = DbContext.FreeSql.GetRepository<ExamPaperCourse>().Insert(rPaperEntity);
                    if (rClassStudent.Count > 0)
                        DbContext.FreeSql.Insert<YssxTaskStudent>().AppendData(rClassStudent).ExecuteAffrows();
                    if (rTaskTopicData.Count > 0)
                        DbContext.FreeSql.Insert<YssxTaskTopic>().AppendData(rTaskTopicData).ExecuteAffrows();
                    if (rExamGradeData.Count > 0)
                        DbContext.FreeSql.Insert<ExamCourseUserGrade>().AppendData(rExamGradeData).ExecuteAffrows();

                    state = taskEntity != null;
                    return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
                });
                #endregion
            }
            else
            {
                #region 处理数据
                //处理数据
                var maxSort = DbContext.FreeSql.Select<YssxTask>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.TaskType != TaskType.ClassTest)
                    .OrderByDescending(m => m.Sort).First(m => m.Sort);

                #region 查找范围内所有题目
                var rTopicYear = string.IsNullOrEmpty(model.TopicYear) ? new List<string>() : model.TopicYear.Split(',').ToList();
                var selectTopicData = new List<YssxTopicPublic>();
                if (model.DataType == 1)
                {
                    //知识点
                    //查询题目列表
                    foreach (var item in model.SurveyScopeDetail)
                    {
                        var rTopicDetail = DbContext.FreeSql.GetRepository<YssxTopicPublic>()
                            .Where(x => x.KnowledgePointIds.IndexOf(item.ToString()) >= 0 && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                            .WhereIf(!string.IsNullOrEmpty(model.TopicYear), x => (x.UpdateTime.HasValue && rTopicYear.Contains(x.UpdateTime.Value.Year.ToString())) || (!x.UpdateTime.HasValue && rTopicYear.Contains(x.CreateTime.Year.ToString()))).ToList();
                        if (rTopicDetail.Count > 0) selectTopicData.AddRange(rTopicDetail);
                    }
                    if (selectTopicData.Count > 0)
                    {
                        //去重
                        selectTopicData = selectTopicData.GroupBy(m => new { m.Id, m.QuestionType, m.Score, m.Sort }).Select(a => new YssxTopicPublic
                        {
                            Id = a.Key.Id,
                            QuestionType = a.Key.QuestionType,
                            Score = a.Key.Score,
                            Sort = a.Key.Sort
                        }).ToList();
                    }
                }
                else
                {
                    //章节
                    selectTopicData = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Select
                        .From<YssxSectionTopic>((a, b) => a.InnerJoin(aa => aa.Id == b.QuestionId && b.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => model.SurveyScopeDetail.Contains(b.SectionId) && a.IsDelete == CommonConstants.IsNotDelete)
                        .WhereIf(!string.IsNullOrEmpty(model.TopicYear), (a, b) => (a.UpdateTime.HasValue && rTopicYear.Contains(a.UpdateTime.Value.Year.ToString())) || (!a.UpdateTime.HasValue && rTopicYear.Contains(a.CreateTime.Year.ToString())))
                        .GroupBy((a, b) => new { a.Id, a.QuestionType }).ToList(a => new YssxTopicPublic
                        {
                            Id = a.Key.Id,
                            QuestionType = a.Key.QuestionType,
                            Score = a.Value.Item1.Score,
                            Sort = a.Value.Item1.Sort
                        });
                }
                if (selectTopicData.Count == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "组卷范围内可选题目数量为0" };
                #endregion

                //循环生成任务
                int rStudentQty = Convert.ToInt32(Math.Floor(model.ClassDetail.Count * 1.0 / model.ExamCount));
                int rTakeCount = rStudentQty;
                List<YssxTask> listTask = new List<YssxTask>();
                List<ExamPaperCourse> listPaperCourse = new List<ExamPaperCourse>();
                List<YssxTaskStudent> listTaskStudent = new List<YssxTaskStudent>();
                List<YssxTaskTopic> listTaskTopic = new List<YssxTaskTopic>();
                List<ExamCourseUserGrade> listExamGrade = new List<ExamCourseUserGrade>();
                for (int i = 1; i < model.ExamCount + 1; i++)
                {
                    var rExamId = IdWorker.NextId();
                    if (i == model.ExamCount)
                        rTakeCount = model.ClassDetail.Count - (i - 1) * rStudentQty;
                    //任务
                    var rTaskEntity = new YssxTask
                    {
                        Id = IdWorker.NextId(),
                        TaskType = model.TaskType,
                        TenantId = tenantId,
                        Name = model.Name + "-" + i,
                        StartTime = model.StartTime,
                        CloseTime = model.CloseTime,
                        TotalMinutes = model.TotalMinutes,
                        CourseId = 0,
                        OriginalCourseId = model.OriginalCourseId,
                        Sort = maxSort + i,
                        Status = LessonsTaskStatus.Wait,
                        Content = model.Content,
                        ExamId = rExamId,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    };
                    //试卷
                    var rPaperEntity = new ExamPaperCourse
                    {
                        Id = rExamId,
                        Name = rTaskEntity.Name,
                        TaskId = rTaskEntity.Id,
                        TenantId = tenantId,
                        CourseId = rTaskEntity.CourseId,
                        OriginalCourseId = rTaskEntity.OriginalCourseId,
                        ExamType = rTaskEntity.TaskType == TaskType.ClassExam ? CourseExamType.ClassExam : (rTaskEntity.TaskType == TaskType.ClassTask ? CourseExamType.ClassTask : CourseExamType.ClassHomeWork),
                        CanShowAnswerBeforeEnd = true,
                        TotalScore = 0,
                        TotalQuestionCount = model.TopicGroupDetail.Sum(m => m.Count),
                        TotalMinutes = rTaskEntity.TotalMinutes,
                        BeginTime = rTaskEntity.StartTime,
                        EndTime = rTaskEntity.CloseTime,
                        ReleaseTime = dtNow,
                        Sort = rTaskEntity.Sort,
                        IsRelease = true,
                        Status = ExamStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    };
                    //学生
                    var rClassStudent = model.ClassDetail.Skip((i - 1) * rStudentQty).Take(rTakeCount).Select(m => new YssxTaskStudent
                    {
                        Id = IdWorker.NextId(),
                        TaskId = rTaskEntity.Id,
                        UserId = m.UserId,
                        ClassId = m.ClassId,
                        StudentId = m.StudentId,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //题目
                    #region 取试卷题目
                    var rTaskTopicData = new List<YssxTaskTopic>();
                    foreach (var item in model.TopicGroupDetail)
                    {
                        //随机排序  new Random().NextDouble()    Guid.NewGuid()
                        var rAddTopicData = selectTopicData.Where(m => m.QuestionType == item.QuestionType).OrderBy(m => new Random().NextDouble()).Take(item.Count).Select(m => new YssxTaskTopic
                        {
                            Id = IdWorker.NextId(),
                            TaskId = rTaskEntity.Id,
                            TopicId = m.Id,
                            QuestionType = m.QuestionType,
                            Score = m.Score,
                            Sort = m.Sort,
                            CreateBy = currentUserId,
                            UpdateBy = currentUserId,
                            UpdateTime = dtNow
                        }).ToList();
                        rTaskTopicData.AddRange(rAddTopicData);
                    }
                    if (rTaskTopicData.Count == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = string.Format("任务【{0}】随机选择的题目数量为0", rTaskEntity.Name) };
                    var totalScore = rTaskTopicData.Sum(m => m.Score);
                    rPaperEntity.TotalScore = totalScore;
                    #endregion
                    //作答记录
                    var rExamGradeData = model.ClassDetail.Skip((i - 1) * rStudentQty).Take(rTakeCount).Select(m => new ExamCourseUserGrade
                    {
                        Id = IdWorker.NextId(),
                        ExamId = rPaperEntity.Id,
                        CourseId = rPaperEntity.CourseId,
                        OriginalCourseId = rPaperEntity.OriginalCourseId,
                        SectionId = rPaperEntity.SectionId,
                        TaskId = rPaperEntity.TaskId,
                        ExamPaperScore = rPaperEntity.TotalScore,
                        LeftSeconds = rPaperEntity.TotalMinutes * 60,
                        RecordTime = rPaperEntity.BeginTime,
                        UserId = m.UserId,
                        TenantId = tenantId,
                        StudentId = m.StudentId,
                        Status = StudentExamStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();

                    #region 收集数据
                    listTask.Add(rTaskEntity);
                    listPaperCourse.Add(rPaperEntity);
                    listTaskStudent.AddRange(rClassStudent);
                    listTaskTopic.AddRange(rTaskTopicData);
                    listExamGrade.AddRange(rExamGradeData);
                    #endregion
                }
                #endregion

                #region 新增数据
                return await Task.Run(() =>
                {
                    var taskRes = DbContext.FreeSql.Insert<YssxTask>().AppendData(listTask).ExecuteAffrows();
                    var paperRes = DbContext.FreeSql.Insert<ExamPaperCourse>().AppendData(listPaperCourse).ExecuteAffrows();
                    if (listTaskStudent.Count > 0)
                        DbContext.FreeSql.Insert<YssxTaskStudent>().AppendData(listTaskStudent).ExecuteAffrows();
                    if (listTaskTopic.Count > 0)
                        DbContext.FreeSql.Insert<YssxTaskTopic>().AppendData(listTaskTopic).ExecuteAffrows();
                    if (listExamGrade.Count > 0)
                        DbContext.FreeSql.Insert<ExamCourseUserGrade>().AppendData(listExamGrade).ExecuteAffrows();

                    state = taskRes > 0;
                    return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
                });
                #endregion
            }
        }

        #endregion

        #region 课业统计

        /// <summary>
        /// 课业任务统计信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TaskGradeReportDto>> GetTaskGradeReport(long taskId, long currentUserId)
        {
            return await Task.Run(() =>
            {
                YssxTask taskEntity = DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == taskId && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First($"{CommonConstants.Cache_GetTaskById}{taskId}", true, 10 * 60);
                if (taskEntity == null)
                    return new ResponseContext<TaskGradeReportDto> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该数据!" };
                var rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == taskEntity.ExamId).First($"{CommonConstants.Cache_GetExamPaperCourseById}{taskEntity.ExamId}", true, 10 * 60);
                if (rPaperEntity == null)
                    return new ResponseContext<TaskGradeReportDto> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };

                var rTaskGradeEntity = new TaskGradeReportDto();
                //课业任务
                rTaskGradeEntity = taskEntity.MapTo<TaskGradeReportDto>();
                rTaskGradeEntity.TotalScore = rPaperEntity.TotalScore;
                //作答记录
                var rGradeData = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetTaskGradeByTaskId}{taskId}", () => DbContext.FreeSql.GetGuidRepository<YssxTask>().Select
                        .From<YssxTaskStudent, ExamCourseUserGrade, YssxUser, YssxClass, YssxStudent>((a, b, c, d, e, f) =>
                            a.InnerJoin(aa => aa.Id == b.TaskId && b.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => aa.Id == c.TaskId && b.UserId == c.UserId && c.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => b.UserId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => b.ClassId == e.Id && e.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => b.UserId == f.UserId && f.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b, c, d, e, f) => a.IsDelete == CommonConstants.IsNotDelete && a.Id == taskId)
                        .ToList((a, b, c, d, e, f) => new TaskGradeReportGradeDto
                        {
                            GradeId = c.Id,
                            UserName = d.RealName,
                            ClassId = b.ClassId,
                            ClassName = e.Name,
                            StudentNo = f.StudentNo,
                            Score = c.Score,
                            Status = c.Status,
                            UsedSeconds = c.UsedSeconds,
                            CorrectCount = c.CorrectCount,
                            ErrorCount = c.ErrorCount + c.PartRightCount,
                            PartRightCount = 0,
                            BlankCount = c.BlankCount
                        }), 10 * 60, true, false);
                rGradeData = rGradeData.OrderByDescending(m => m.Score).OrderByDescending(m => m.CorrectCount).ToList();
                int num = 0;
                foreach (var item in rGradeData)
                {
                    num++;
                    item.Ranking = num;
                }
                rTaskGradeEntity.GradeDetail = rGradeData.OrderBy(m => m.StudentNo).ToList();
                //计算数据
                rTaskGradeEntity.MaxScore = rGradeData.Count == 0 ? 0 : rGradeData.Max(m => m.Score);
                rTaskGradeEntity.MinScore = rGradeData.Count == 0 ? 0 : rGradeData.Min(m => m.Score);
                rTaskGradeEntity.AvgScore = rGradeData.Count == 0 ? 0 : rGradeData.Average(m => m.Score);
                
                rTaskGradeEntity.ExcellentScore = Convert.ToInt32(Math.Ceiling(rPaperEntity.TotalScore * (decimal)0.8));
                rTaskGradeEntity.GoodScore = Convert.ToInt32(Math.Ceiling(rPaperEntity.TotalScore * (decimal)0.7));
                rTaskGradeEntity.QualifiedScore = Convert.ToInt32(Math.Ceiling(rPaperEntity.TotalScore * (decimal)0.6));

                rTaskGradeEntity.ExcellentNumber = rGradeData.Count(m => m.Score >= rTaskGradeEntity.ExcellentScore);
                rTaskGradeEntity.GoodNumber = rGradeData.Count(m => m.Score >= rTaskGradeEntity.GoodScore && m.Score < rTaskGradeEntity.ExcellentScore);
                rTaskGradeEntity.QualifiedNumber = rGradeData.Count(m => m.Score >= rTaskGradeEntity.QualifiedScore && m.Score < rTaskGradeEntity.GoodScore);
                rTaskGradeEntity.UnqualifiedNumber = rGradeData.Count(m => m.Score < rTaskGradeEntity.QualifiedScore);
                //班级数据
                var rClassDetail = rGradeData.GroupBy(m => new { m.ClassId, m.ClassName }).Select(a => new TaskGradeReportClassDto
                {
                    ClassId = a.Key.ClassId,
                    ClassName = a.Key.ClassName,
                    StudentCount = a.Count(),
                    AbsentStudentCount = a.Count(x => x.IsAbsent)
                }).ToList();
                rTaskGradeEntity.ClassDetail = rClassDetail;
                //所有题目
                var rTopicDetail = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetTaskTopicByTaskId}{taskId}", () => DbContext.FreeSql.GetRepository<YssxTaskTopic>().Select
                    .From<YssxTopicPublic>((a, b) => a.InnerJoin(aa => aa.TopicId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.TaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete).OrderBy((a, b) => a.Sort).ToList((a, b) => new TaskTopicDto
                    {
                        Id = a.Id,
                        TopicId = a.TopicId,
                        QuestionType = a.QuestionType,
                        Score = a.Score,
                        Sort = a.Sort,
                        Title = b.Title
                    }), 10 * 60, true, false);
                rTaskGradeEntity.TopicDetail = rTopicDetail;
                //错题
                var rErrorTopicDetail = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetTaskErrorTopicByTaskId}{taskId}", () => DbContext.FreeSql.GetRepository<YssxTaskTopic>().Select.From<ExamCourseUserGrade, ExamCourseUserGradeDetail, YssxTopicPublic>((a, b, c, d) =>
                        a.InnerJoin(aa => aa.TaskId == b.TaskId && b.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => b.Id == c.GradeId && c.IsDelete == CommonConstants.IsNotDelete && (c.Status == AnswerResultStatus.Error || c.Status == AnswerResultStatus.PartRight))
                        .InnerJoin(aa => aa.TopicId == d.Id && d.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d) => a.TaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy((a, b, c, d) => a.Sort).GroupBy((a, b, c, d) => new { a.Id, a.TopicId }).ToList(m => new TaskTopicDto
                    {
                        Id = m.Key.Id,
                        TopicId = m.Key.TopicId,
                        QuestionType = m.Value.Item1.QuestionType,
                        Score = m.Value.Item1.Score,
                        Sort = m.Value.Item1.Sort,
                        Title = m.Value.Item4.Title
                    }), 10 * 60, true, false);
                rTaskGradeEntity.ErrorTopicDetail = rErrorTopicDetail;

                return new ResponseContext<TaskGradeReportDto> { Code = CommonConstants.SuccessCode, Msg = "成功", Data = rTaskGradeEntity };
            });
        }
        /// <summary>
        /// 课业任务统计信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TaskGradeReportDto>> GetCourseGradeDetailByTask(long taskId,string classIds, long currentUserId)
        {
            return await Task.Run(() =>
            {
                YssxTask taskEntity = DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == taskId && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (taskEntity == null)
                    return new ResponseContext<TaskGradeReportDto> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该数据!" };
                var rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == taskEntity.ExamId).First();
                if (rPaperEntity == null)
                    return new ResponseContext<TaskGradeReportDto> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };

                var rTaskGradeEntity = new TaskGradeReportDto();
                //课业任务
                rTaskGradeEntity = taskEntity.MapTo<TaskGradeReportDto>();
                rTaskGradeEntity.TotalScore = rPaperEntity.TotalScore;

                //查询课程名称
                rTaskGradeEntity.CourseName = DbContext.FreeSql.GetRepository<YssxCourse>().Where(x => x.Id == rTaskGradeEntity.OriginalCourseId).First()?.CourseName;

                //作答记录
                var gradeDetails = DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Select
                        .From<ExamCourseUserGradeDetail, YssxUser, YssxStudent, YssxClass>((a, b, c, d, e) =>
                            a.LeftJoin(aa => aa.Id == b.GradeId && b.IsDelete == CommonConstants.IsNotDelete&& b.QuestionId==b.ParentQuestionId)
                            .LeftJoin(aa => aa.UserId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
                            .LeftJoin(aa => aa.UserId == d.UserId && d.IsDelete == CommonConstants.IsNotDelete)
                            .LeftJoin(aa => d.ClassId == e.Id && e.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b, c, d, e) => a.TaskId == taskId&& a.IsDelete == CommonConstants.IsNotDelete)
                        .WhereIf(!string.IsNullOrEmpty(classIds), (a,b,c,d,e)=> classIds.Contains(e.Id.ToString()))
                        //.GroupBy((a,b,c,d,e)=>new {a.GradeId,c.RealName,d.StudentNo,e.Name,e.Id,b.Status,b.UsedSeconds})                        
                        .ToList((a, b, c, d, e) => new
                        {
                            GradeId = a.Id,
                            UserName = c.RealName,
                            ClassId = e.Id,
                            ClassName = e.Name,
                            StudentNo = d.StudentNo,
                            Score = b.Score,
                            Status = b.Status,
                            UsedSeconds = a.UsedSeconds,
                            ExamStatus=a.Status
                        });
                var resultData = gradeDetails.GroupBy(s => new { s.GradeId, s.UserName, s.ClassId, s.ClassName, s.StudentNo, s.ExamStatus, s.UsedSeconds })
                                .Select(s => new TaskGradeReportGradeDto
                                {
                                    GradeId = s.Key.GradeId,
                                    UserName = s.Key.UserName,
                                    ClassId = s.Key.ClassId,
                                    ClassName = s.Key.ClassName,
                                    StudentNo = s.Key.StudentNo,
                                    Score = s.Sum(c=>c.Score),
                                    Status = s.Key.ExamStatus,
                                    UsedSeconds = s.Key.UsedSeconds
                                }).ToList();
                var correctCount = 0;
                var errorCount = 0;

                resultData = resultData.OrderByDescending(m => m.Score).ToList();
                
                resultData.ForEach(s =>
                {
                    s.CorrectCount = gradeDetails.Where(d => d.Status == AnswerResultStatus.Right && d.GradeId == s.GradeId).Count();
                    s.ErrorCount = gradeDetails.Where(d => (d.Status == AnswerResultStatus.Error || d.Status == AnswerResultStatus.PartRight) && d.GradeId == s.GradeId).Count();
                    s.TotalQuestionCount = rPaperEntity.TotalQuestionCount;
                    s.DoQuestionCount = s.CorrectCount + s.ErrorCount;
                });
                int rank = 1;
                int doQuestionStudentCount = resultData.Where(s => (s.CorrectCount + s.ErrorCount) > 0).Count();
                resultData.ForEach(s =>
                {
                    if (s.DoQuestionCount > 0)
                    {
                        s.Ranking = rank;
                        rank++;
                    }
                    else
                    {
                        s.Ranking = doQuestionStudentCount + 1;
                        doQuestionStudentCount++;
                    }
                });
                
                resultData = resultData.OrderByDescending(m => m.StudentNo).ToList();
                
                rTaskGradeEntity.GradeDetail = resultData;
                rTaskGradeEntity.SubmitedCount = resultData.Where(d => d.Status == StudentExamStatus.End).Count();
                rTaskGradeEntity.NoSubmitedCount = resultData.Where(d => d.Status != StudentExamStatus.End).Count();
                rTaskGradeEntity.MissCount = resultData.Where(d => d.DoQuestionCount == 0).Count();
                //计算数据
                rTaskGradeEntity.MaxScore = resultData.Count == 0 ? 0 : Math.Round(resultData.Max(m => m.Score),4);
                rTaskGradeEntity.MinScore = resultData.Count == 0 ? 0 : Math.Round(resultData.Min(m => m.Score), 4);
                rTaskGradeEntity.AvgScore = resultData.Count == 0 ? 0 : Math.Round(resultData.Average(m => m.Score),4);

                rTaskGradeEntity.ExcellentScore = Convert.ToInt32(Math.Ceiling(rPaperEntity.TotalScore * (decimal)0.85));
                rTaskGradeEntity.GoodScore = Convert.ToInt32(Math.Ceiling(rPaperEntity.TotalScore * (decimal)0.75));
                rTaskGradeEntity.QualifiedScore = Convert.ToInt32(Math.Ceiling(rPaperEntity.TotalScore * (decimal)0.6));

                rTaskGradeEntity.ExcellentNumber = resultData.Count(m => m.Score >= rTaskGradeEntity.ExcellentScore);
                rTaskGradeEntity.GoodNumber = resultData.Count(m => m.Score >= rTaskGradeEntity.GoodScore && m.Score < rTaskGradeEntity.ExcellentScore);
                rTaskGradeEntity.QualifiedNumber = resultData.Count(m => m.Score >= rTaskGradeEntity.QualifiedScore && m.Score < rTaskGradeEntity.GoodScore);
                rTaskGradeEntity.UnqualifiedNumber = resultData.Count(m => m.Score < rTaskGradeEntity.QualifiedScore);
                //班级数据
                var rClassDetail = resultData.GroupBy(m => new { m.ClassId, m.ClassName }).Select(a => new TaskGradeReportClassDto
                {
                    ClassId = a.Key.ClassId,
                    ClassName = a.Key.ClassName,
                    StudentCount = a.Count(),
                    AbsentStudentCount = a.Count(x => x.IsAbsent)
                }).ToList();
                rTaskGradeEntity.ClassDetail = rClassDetail;
                return new ResponseContext<TaskGradeReportDto> { Code = CommonConstants.SuccessCode, Msg = "成功", Data = rTaskGradeEntity };
            });
        }
        /// <summary>
        /// 课业任务统计信息 - 移动端
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<TaskGradeReportGradeDto>> GetTaskGradeReportByApp(TaskGradeReportByAppDto model)
        {
            return await Task.Run(() =>
            {
                if (model.TaskId == 0)
                    return new PageResponse<TaskGradeReportGradeDto> { Code = CommonConstants.ErrorCode, Msg = "查询失败，课业任务ID异常" };
                //作答记录
                var selectData = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetGradeReportByTaskId}{model.TaskId}", () => DbContext.FreeSql.GetGuidRepository<YssxTask>().Select
                        .From<YssxTaskStudent, ExamCourseUserGrade, YssxUser, YssxClass, YssxStudent>((a, b, c, d, e, f) =>
                            a.InnerJoin(aa => aa.Id == b.TaskId && b.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => aa.Id == c.TaskId && b.UserId == c.UserId && c.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => b.UserId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => b.ClassId == e.Id && e.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => d.Id == f.UserId && f.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b, c, d, e, f) => a.IsDelete == CommonConstants.IsNotDelete && a.Id == model.TaskId)
                        .OrderByDescending((a, b, c, d, e, f) => c.Score).ToList((a, b, c, d, e, f) => new TaskGradeReportGradeDto
                        {
                            GradeId = c.Id,
                            UserName = d.RealName,
                            StudentNo = f.StudentNo,
                            ClassId = b.ClassId,
                            ClassName = e.Name,
                            Score = c.Score,
                            Status = c.Status,
                            UsedSeconds = c.UsedSeconds,
                            CorrectCount = c.CorrectCount,
                            ErrorCount = c.ErrorCount,
                            PartRightCount = c.PartRightCount,
                            BlankCount = c.BlankCount
                        }), 10 * 60, true, false);
                selectData = selectData.OrderByDescending(m => m.Score).OrderByDescending(m => m.CorrectRate).ToList();
                if (model.ClassId > 0)
                    selectData = selectData.Where(m => m.ClassId == model.ClassId).ToList();
                var totalCount = selectData.Count;
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                int num = 0;
                foreach (var item in selectData)
                {
                    num++;
                    item.Ranking = num;
                }
                return new PageResponse<TaskGradeReportGradeDto> { Code = CommonConstants.SuccessCode, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 导出课业任务统计数据
        /// </summary>
        /// <returns></returns>      
        public async Task<ResponseContext<TaskGradeReportDto>> ExportTaskGrade(long taskId)
        {
            return await Task.Run(() =>
            {
                YssxTask taskEntity = DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == taskId && x.IsDelete == CommonConstants.IsNotDelete).First($"{CommonConstants.Cache_GetTaskById}{taskId}", true, 10 * 60);
                if (taskEntity == null)
                    return new ResponseContext<TaskGradeReportDto> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该数据!" };
                var rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Id == taskEntity.ExamId).First($"{CommonConstants.Cache_GetExamPaperCourseById}{taskEntity.ExamId}", true, 10 * 60);
                if (rPaperEntity == null)
                    return new ResponseContext<TaskGradeReportDto> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };

                var rTaskGradeEntity = new TaskGradeReportDto();
                rTaskGradeEntity.Name = rPaperEntity.Name;
                //作答记录
                var rGradeData = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetTaskGradeByTaskId}{taskId}", () => DbContext.FreeSql.GetGuidRepository<YssxTask>().Select
                        .From<YssxTaskStudent, ExamCourseUserGrade, YssxUser, YssxClass, YssxStudent>((a, b, c, d, e, f) =>
                            a.InnerJoin(aa => aa.Id == b.TaskId && b.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => aa.Id == c.TaskId && b.UserId == c.UserId && c.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => b.UserId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => b.ClassId == e.Id && e.IsDelete == CommonConstants.IsNotDelete)
                            .InnerJoin(aa => b.UserId == f.UserId && f.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b, c, d, e, f) => a.IsDelete == CommonConstants.IsNotDelete && a.Id == taskId)
                        .ToList((a, b, c, d, e, f) => new TaskGradeReportGradeDto
                        {
                            GradeId = c.Id,
                            UserName = d.RealName,
                            ClassId = b.ClassId,
                            ClassName = e.Name,
                            StudentNo = f.StudentNo,
                            Score = c.Score,
                            UsedSeconds = c.UsedSeconds,
                            CorrectCount = c.CorrectCount,
                            ErrorCount = c.ErrorCount + c.PartRightCount,
                            PartRightCount = 0,
                            BlankCount = c.BlankCount
                        }), 10 * 60, true, false);
                rGradeData = rGradeData.OrderByDescending(m => m.Score).OrderByDescending(m => m.CorrectRate).ToList();
                int num = 0;
                foreach (var item in rGradeData)
                {
                    num++;
                    item.Ranking = num;
                }
                rTaskGradeEntity.GradeDetail = rGradeData.OrderBy(m => m.StudentNo).ToList();

                return new ResponseContext<TaskGradeReportDto> { Code = CommonConstants.SuccessCode, Msg = "成功", Data = rTaskGradeEntity };
            });
        }


        #endregion


        #region 课堂任务

        /// <summary>
        /// 查询课堂测验列表 - 教师端 - new
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<TestingTaskView>> GetTestingTaskByPage(GetTestingTaskByPageDto model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                //查询数据
                //删除的备课课程下发布的任务还要可见 2020.03.19
                var selectData = DbContext.FreeSql.GetGuidRepository<ExamPaperCourse>().Select.From<YssxCourse>((a, b) => a.LeftJoin(aa => aa.OriginalCourseId == b.Id))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.CreateBy == currentUserId && a.ExamType == CourseExamType.ClassTest)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b) => a.Name.Contains(model.Name))
                    .WhereIf(model.OriginalCourseId > 0, (a, b) => a.OriginalCourseId == model.OriginalCourseId)
                    .OrderByDescending((a, b) => a.CreateTime).ToList((a, b) => new TestingTaskView
                    {
                        Name = a.Name,
                        TaskType = TaskType.ClassTest,
                        ExamId = a.Id,
                        GroupId = a.TaskId,
                        CourseId = a.CourseId,
                        CourseName = b.CourseName,
                        OriginalCourseId = a.OriginalCourseId,
                        StartTime = a.BeginTime,
                        CloseTime = a.EndTime.Value,
                        TotalMinutes = a.TotalMinutes,
                        Status = a.Status
                    });
                #region 搜索条件
                if (model.Status != -2)
                    selectData = selectData.Where(a => a.ExamStatus == (ExamStatus)model.Status).ToList();
                if (model.IsBeginTime == CommonConstants.IsYes)
                {
                    if (model.BeginDate.HasValue)
                    {
                        var rBeginDate = Convert.ToDateTime(model.BeginDate.Value.ToString("yyyy-MM-dd 00:00:01"));
                        selectData = selectData.Where(a => a.StartTime >= rBeginDate).ToList();
                    }
                    if (model.EndDate.HasValue)
                    {
                        var rEndDate = Convert.ToDateTime(model.EndDate.Value.ToString("yyyy-MM-dd 23:59:59"));
                        selectData = selectData.Where(a => a.StartTime <= rEndDate).ToList();
                    }
                }
                else
                {
                    if (model.BeginDate.HasValue)
                    {
                        var rBeginDate = Convert.ToDateTime(model.BeginDate.Value.ToString("yyyy-MM-dd 00:00:01"));
                        selectData = selectData.Where(a => a.CloseTime >= rBeginDate).ToList();
                    }
                    if (model.EndDate.HasValue)
                    {
                        var rEndDate = Convert.ToDateTime(model.EndDate.Value.ToString("yyyy-MM-dd 23:59:59"));
                        selectData = selectData.Where(a => a.CloseTime <= rEndDate).ToList();
                    }
                }
                #endregion
                var totalCount = selectData.Count();
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                //查询任务的班级列表和学生数据
                var rGroupIdArr = selectData.Select(m => m.GroupId).ToList();
                var rTaskStudentData = DbContext.FreeSql.GetRepository<YssxTask>().Select.From<YssxClass, YssxStudent, YssxUser>((a, b, c, d) => 
                    a.InnerJoin(aa => aa.TenantId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                    .InnerJoin(aa => aa.TenantId == c.ClassId && c.IsDelete == CommonConstants.IsNotDelete)
                    .InnerJoin(aa => c.UserId == d.Id && d.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d) => a.IsDelete == CommonConstants.IsNotDelete && rGroupIdArr.Contains(a.GroupId)).ToList((a, b, c, d) => new
                    {
                        GroupId = a.GroupId,
                        ClassId = a.TenantId,
                        ClassName = b.Name,
                        UserId = c.UserId
                    });
                foreach (var item in selectData)
                {
                    var rClassDetailData = new List<TaskClassView>();
                    if (rTaskStudentData.Count > 0 && item.GroupId > 0 && rTaskStudentData.Any(m => m.GroupId == item.GroupId))
                    {
                        rClassDetailData = rTaskStudentData.Where(m => m.GroupId == item.GroupId).GroupBy(x => new { x.ClassId, x.ClassName }).Select(m =>
                            new TaskClassView
                            {
                                ClassId = m.Key.ClassId,
                                ClassName = m.Key.ClassName,
                                StudentCount = m.Count()
                            }).ToList();
                    }
                    item.ClassDetail = rClassDetailData;
                    item.TotalStudentCount = rClassDetailData.Sum(m => m.StudentCount);
                }

                return new PageResponse<TestingTaskView> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 查询课堂测验列表 - 学生端 - new
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<PageResponse<StudentTestingTaskView>> GetStudentTestingTaskByPage(GetTestingTaskByPageDto model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                //删除的备课课程下发布的任务还要可见 2020.03.19
                var selectData = DbContext.FreeSql.GetGuidRepository<YssxTask>().Select
                    .From<YssxCourse, YssxStudent, YssxUser, ExamPaperCourse, ExamCourseUserGrade>((a, b, c, d, e, f) =>
                        a.LeftJoin(aa => aa.OriginalCourseId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => aa.TenantId == c.ClassId && c.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => c.UserId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => aa.GroupId == e.TaskId && e.IsDelete == CommonConstants.IsNotDelete)
                        .LeftJoin(aa => e.Id == f.ExamId && f.UserId == currentUserId && f.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d, e, f) => a.IsDelete == CommonConstants.IsNotDelete && d.Id == currentUserId && a.TaskType == TaskType.ClassTest)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b, c, d, e, f) => a.Name.Contains(model.Name))
                    .WhereIf(model.OriginalCourseId > 0, (a, b, c, d, e, f) => a.OriginalCourseId == model.OriginalCourseId)
                    .OrderByDescending((a, b, c, d, e, f) => a.CreateTime).ToList((a, b, c, d, e, f) => new StudentTestingTaskView
                    {
                        Id = a.Id,
                        Name = a.Name,
                        TaskType = a.TaskType,
                        CourseId = a.CourseId,
                        CourseName = b.CourseName,
                        OriginalCourseId = a.OriginalCourseId,
                        StartTime = a.StartTime,
                        CloseTime = a.CloseTime,
                        TotalMinutes = a.TotalMinutes,
                        UsedSeconds = f.UsedSeconds,
                        Status = f.Status == StudentExamStatus.End ? LessonsTaskStatus.End : a.Status,
                        Content = a.Content,
                        ExamId = e.Id,
                        GroupId = a.GroupId,
                        GradeId = f.Id,
                        ErrorCount = f.ErrorCount,
                        ExamPaperScore = e.TotalScore,
                        Score = f.Score
                    });
                if (model.Status != -2)
                    selectData = selectData.Where(a => a.ExamStatus == (LessonsTaskStatus)model.Status).ToList();
                var totalCount = selectData.Count();
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                return new PageResponse<StudentTestingTaskView> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }


        #region old
        /// <summary>
        /// 课堂任务-开始做题
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<string>> StartCourseTestingTask(TaskDto model, long currentUserId)
        {
            var dtNow = DateTime.Now;
            return await Task.Run(() =>
            {
                //判断是否有未结束任务
                if (DbContext.FreeSql.Select<YssxTask>().Where(o => o.Status == LessonsTaskStatus.Started && o.CreateBy == currentUserId && o.IsDelete == CommonConstants.IsNotDelete).ToList()?.Count() > 0)
                {
                    return new ResponseContext<string> { Code = CommonConstants.ErrorCode, Data = "", Msg = "任务发布失败，您已有未结束任务！" };
                }

                var CourseName = DbContext.FreeSql.Select<YssxPrepareCourse>().Where(o => o.Id == model.CourseId).First()?.CourseName;

                //生成试卷
                ExamPaperCourse paperEntity = new ExamPaperCourse
                {
                    Id = IdWorker.NextId(),
                    Name = CourseName + "-课堂答题",
                    //TaskId = ,
                    CourseId = model.CourseId,
                    ExamType = CourseExamType.ClassTest,
                    CanShowAnswerBeforeEnd = true,
                    //TotalScore =,
                    TotalQuestionCount = model.TopicList.Count,
                    BeginTime = dtNow,
                    ReleaseTime = dtNow,
                    IsRelease = true,
                    Status = ExamStatus.Started,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow,
                };
                DbContext.FreeSql.GetRepository<ExamPaperCourse>().Insert(paperEntity);

                var TaskList = new List<YssxTask>();

                foreach (var ClassID in model.ClassDetail.Select(o => o.ClassId).ToList())
                {
                    //创建任务
                    var Task = new YssxTask
                    {
                        Id = IdWorker.NextId(),
                        TaskType = TaskType.ClassTest,
                        Name = CourseName + "-课堂答题",
                        StartTime = DateTime.Now,
                        CourseId = model.CourseId,
                        ExamId = paperEntity.Id,
                        Status = LessonsTaskStatus.Started,
                        TenantId = ClassID,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    };
                    TaskList.Add(Task);
                }
                DbContext.FreeSql.GetRepository<YssxTask>().Insert(TaskList);

                foreach (var item in TaskList)
                {
                    //创建任务试题
                    var TaskTopic = model.TopicList.Select(o => new YssxTaskTopic
                    {
                        Id = IdWorker.NextId(),
                        TaskId = item.Id,
                        TopicId = o.TopicId,
                        QuestionType = o.QuestionType,
                        Score = o.Score,
                        Sort = o.Sort,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    DbContext.FreeSql.GetRepository<YssxTaskTopic>().Insert(TaskTopic);
                }
                var TaskIDs = String.Join(',', TaskList.Select(o => o.Id));
                return new ResponseContext<string> { Code = CommonConstants.SuccessCode, Data = TaskIDs, Msg = "成功" };
            });
        }

        /// <summary>
        /// 课堂测试-收题
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<CourseTestingTaskAnalysis>> StopCourseTestingTask(List<long> TaskIDList, long currentUserId)
        {
            var dtNow = DateTime.Now;
            return await Task.Run(() =>
            {
                bool status = false;
                var Task = DbContext.FreeSql.Select<YssxTask>().Where(o => o.Id == TaskIDList[0] && o.IsDelete == CommonConstants.IsNotDelete).First();
                var taskList = new List<YssxTask>();
                foreach (var item in TaskIDList)
                {
                    taskList.Add(new YssxTask { Id = item, Status = LessonsTaskStatus.End, CloseTime = dtNow, UpdateBy = currentUserId, UpdateTime = dtNow });
                }
                status = DbContext.FreeSql.Update<YssxTask>().SetSource(taskList).UpdateColumns(o => new { o.Status, o.CloseTime, o.UpdateBy, o.UpdateTime }).ExecuteAffrows() > 0;

                var ExamID = Task.ExamId;

                var ExamPaper = DbContext.FreeSql.Select<ExamPaperCourse>().Where(o => o.Id == ExamID && o.IsDelete == CommonConstants.IsNotDelete).First();
                ExamPaper.Status = ExamStatus.End;
                ExamPaper.EndTime = dtNow;
                ExamPaper.UpdateBy = currentUserId;
                ExamPaper.UpdateTime = dtNow;

                if (status)
                {
                    status = DbContext.FreeSql.Update<ExamPaperCourse>().SetSource(ExamPaper).UpdateColumns(o => new { o.Status, o.EndTime, o.UpdateBy, o.UpdateTime }).ExecuteAffrows() > 0;
                }

                var Students = DbContext.FreeSql.GetRepository<YssxTask>().Select.From<YssxStudent>((a, b) => a.InnerJoin(o => o.TenantId == b.ClassId)).Where((a, b) => TaskIDList.Contains(a.Id)).ToList<YssxStudent>();

                var TopicList = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Select.From<YssxTaskTopic>((a, b) => a.InnerJoin(o => o.Id == b.TopicId)).Where((a, b) => b.TaskId == TaskIDList[0]).ToList();

                var ErrorStudentList = DbContext.FreeSql.GetRepository<ExamPaperCourse>().Select.From<ExamStudentGradeDetail, YssxStudent>((a, b, c) => a.InnerJoin(o => o.Id == b.ExamId).RightJoin(o => b.UserId == c.UserId)).Where((a, b, c) => TaskIDList.Contains(a.TaskId) && (b.Status == AnswerResultStatus.Error || b.Status == AnswerResultStatus.None)).ToList((a, b, c) => new
                {
                    CourseID = a.CourseId,
                    QuestionId = b.QuestionId,
                    StudentName = c.Name,
                });

                var TaskAnalysis = new CourseTestingTaskAnalysis { TaskName = Task.Name, TaskDate = Task.StartTime, NumberOfTask = Students.Count, SubmitTotal = Students.Count };

                TaskAnalysis.TopicAnalysisList = new List<AnswerTopicAnalysis>();

                foreach (var item in TopicList)
                {
                    var AnswerError = ErrorStudentList.Where(o => o.QuestionId == item.Id).Select(o => o.StudentName).ToList();
                    var TopicAnalysis = new AnswerTopicAnalysis { Title = item.Title, AnswerErrorTotal = AnswerError.Count, NumberOfTask = Students.Count, ErrorStudentList = AnswerError };
                    TaskAnalysis.TopicAnalysisList.Add(TopicAnalysis);
                }
                return new ResponseContext<CourseTestingTaskAnalysis> { Code = status ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = TaskAnalysis, Msg = status ? "成功" : "失败" };
            });
        }

        /// <summary>
        /// 刷新查看
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<CourseTestingTaskAnalysis>> RefreshCourseTestingTask(List<long> TaskIDList)
        {
            return await Task.Run(() =>
            {
                var Task = DbContext.FreeSql.Select<YssxTask>().Where(o => o.Id == TaskIDList[0] && o.IsDelete == CommonConstants.IsNotDelete).First();

                var TaskStudents = DbContext.FreeSql.GetRepository<YssxTask>().Select.From<YssxStudent, ExamStudentGrade>((a, b, c) => a.InnerJoin(o => o.TenantId == b.ClassId).InnerJoin(o => b.UserId == c.UserId)).Where((a, b, c) => TaskIDList.Contains(a.Id)).ToList((a, b, c) => new
                {
                    Name = b.Name,
                    ExamStatus = c.Status
                });

                var AnswerStudentList = TaskStudents.Where(o => o.ExamStatus == StudentExamStatus.Started).Select(o => o.Name).ToList();

                var TaskAnalysis = new CourseTestingTaskAnalysis { TaskName = Task.Name, TaskDate = Task.StartTime, NumberOfTask = TaskStudents.Count, SubmitTotal = TaskStudents.Count - AnswerStudentList.Count, NumberOfAnswer = AnswerStudentList.Count, StudentList = AnswerStudentList };

                return new ResponseContext<CourseTestingTaskAnalysis> { Code = CommonConstants.SuccessCode, Data = TaskAnalysis, Msg = "成功" };
            });
        }

        /// <summary>
        /// 获取课堂任务题目列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<object>> GetCourseTestingTaskTopicList(long TaskID)
        {
            return await Task.Run(() =>
            {
                var TopicList = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Select.From<YssxTaskTopic>((a, b) => a.InnerJoin(o => o.Id == b.TopicId)).Where((a, b) => b.TaskId == TaskID && a.IsDelete == CommonConstants.IsNotDelete).ToList().Select(o => new
                {
                    TopicID = o.Id,
                    QuestionType = o.QuestionType,
                    Title = o.Title
                }).ToList();

                return new ResponseContext<object> { Code = CommonConstants.SuccessCode, Data = TopicList, Msg = "成功" };
            });
        }



        ///// <summary>
        ///// 讲解习题
        ///// </summary>
        ///// <returns></returns>
        //public async Task<ResponseContext<object>> ExplainCourseTestingTask(long TopicID)
        //{
        //    return await Task.Run(() =>
        //    {
        //        var Topic = DbContext.FreeSql.Select<YssxTopicPublic>().Where(o =>o.Id==TopicID && o.IsDelete == CommonConstants.IsNotDelete).ToList();
        //        return new ResponseContext<object> { Code = CommonConstants.SuccessCode, Data = Topic, Msg = "成功" };
        //    });
        //}

        /// <summary>
        /// 获取我的任务课程
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<PageResponse<MyCourseTaskDto>> GetMyCourseTaskByPage(long currentUserId, int pageIndex, int pageSize)
        {
            return await Task.Run(() =>
            {
                var result = new PageResponse<MyCourseTaskDto>();
                long classId = DbContext.FreeSql.Select<YssxStudent>().Where(o => o.UserId == currentUserId).First(o => o.ClassId);
                var select = DbContext.FreeSql.GetRepository<YssxPrepareCourse>().Select.From<YssxTask, ExamCourseUserLastGrade>((a, b, c) => a.InnerJoin(o => o.Id == b.CourseId).InnerJoin(o => o.Id == c.TaskId)).Where((a, b, c) => b.TenantId == classId && a.IsDelete == CommonConstants.IsNotDelete);
                var CourseList = select.OrderByDescending((a, b, c) => a.Id).Page(pageIndex, pageSize).ToList((a, b, c) => new MyCourseTaskDto
                {
                    TaskID = b.Id,
                    TaskStatus = b.Status,
                    CourseTitle = a.CourseTitle,
                    Images = a.Images,
                    CourseID = a.Id,
                    CourseName = a.CourseName,
                    PublishingDate = a.PublishingDate,
                    PublishingHouse = a.PublishingHouse,
                    CreateByName = a.CreateByName,
                    UpdateTime = a.UpdateTime,
                    GradeId = c.Id,
                });
                var CourseIDs = CourseList.Select(m => m.CourseID).ToList();
                var Summary = DbContext.FreeSql.Select<YssxPrepareSectionSummary>().Where(o => CourseIDs.Contains(o.CourseId)).ToList();

                foreach (var item in CourseList)
                {
                    item.Summarys = Summary.Where(x => x.CourseId == item.CourseID).Select(o => new CourseSummary
                    {
                        Id = o.Id,
                        Count = o.Count,
                        SummaryType = o.SummaryType,

                    }).ToList();
                }

                result.Code = CommonConstants.SuccessCode;
                result.Data = CourseList;
                result.PageSize = pageSize;
                result.PageIndex = pageIndex;
                result.RecordCount = select.Count();
                return result;
            });
        }

        /// <summary>
        /// 获取课程任务试卷题目列表
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourseTaskExamTopicList(long TaskID)
        {
            return await Task.Run(() =>
            {
                var TopicList = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Select.From<YssxTaskTopic>((a, b) => a.InnerJoin(o => o.Id == b.TopicId)).Where((a, b) => b.TaskId == TaskID && a.IsDelete == CommonConstants.IsNotDelete).ToList();

                var ExamPaperID = DbContext.FreeSql.Select<ExamPaperCourse>().Where(o => o.TaskId == TaskID && o.IsDelete == CommonConstants.IsNotDelete).First(o => o.Id);

                var ExamPaper = new ExamPaperCourseBasicInfo { ExamId = ExamPaperID };
                ExamPaper.QuestionInfoList = new List<CourseQuestionInfo>();
                foreach (var item in TopicList)
                {
                    ExamPaper.QuestionInfoList.Add(new CourseQuestionInfo { QuestionId = item.Id, ParentQuestionId = item.ParentId, Title = item.Title, QuestionType = item.QuestionType, Score = item.Score, Sort = item.Sort });
                }

                return new ResponseContext<ExamPaperCourseBasicInfo> { Code = CommonConstants.SuccessCode, Data = ExamPaper, Msg = "成功" };
            });
        }
        #endregion

        #endregion


        public async Task<ResponseContext<bool>> SetTaskStatus(long taskId, DateTime endDate, bool isCanCheckExam)
        {
            if (endDate == null || taskId <= 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不合法" };
            if (endDate < DateTime.Now) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "不能小于当前时间" };

            List<ExamPaperCourse> papers = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(x => x.TaskId == taskId).ToListAsync();
            bool state = false;
            bool state2 = false;
            if (papers.Count > 0)
            {
                papers.ForEach(x =>
                {
                    x.Status = ExamStatus.Started;
                    x.EndTime = endDate;
                });
                state2 = await DbContext.FreeSql.GetRepository<YssxTask>().UpdateDiy
                    .Set(x => x.Status, LessonsTaskStatus.Started)
                    .Set(x => x.CloseTime, endDate)
                    .Set(x => x.IsCanCheckExam, isCanCheckExam)
                    .Where(x => x.Id == taskId).ExecuteAffrowsAsync() > 0;
                state = await DbContext.FreeSql.Update<ExamPaperCourse>().SetSource(papers).IgnoreColumns(a => new { a.Id }).ExecuteAffrowsAsync() > 0;
            }

            bool isOk = state && state2;
            return new ResponseContext<bool> { Code = isOk ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = isOk ? "ok" : "开启失败!", Data = state };
        }

        public async Task<ResponseContext<bool>> SetStudentStatus(long gradeId)
        {
            ExamCourseUserGrade exam = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(a => a.Id==gradeId &&a.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            bool state = false;
            if (exam!=null)
            {
                    exam.Status = StudentExamStatus.Started;

                state = await DbContext.FreeSql.Update<ExamCourseUserGrade>().SetSource(exam).Set(x => x.Status, StudentExamStatus.Started).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "ok" : "开启失败!", Data = state };
        }

        #region 打开或关闭课程任务学生查看试卷入口
        /// <summary>
        /// 打开或关闭课程任务学生查看试卷入口
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> OpenOrCloseCourseTaskCheckExam(CloseCheckExamRequestModel request, long currentUserId)
        {
            if (request == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "参数不能为空!" };

            YssxTask entity = await DbContext.FreeSql.GetRepository<YssxTask>().Where(x => x.Id == request.TaskId).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            bool state = await DbContext.FreeSql.GetRepository<YssxTask>().UpdateDiy
                .Set(x => x.IsCanCheckExam, request.IsCanCheckExam)
                .Set(x => x.UpdateBy, currentUserId)
                .Set(x => x.UpdateTime, DateTime.Now)
                .Where(x => x.Id == request.TaskId).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "操作成功!" : "失败!", Data = state };
        }
        #endregion

    }
}
