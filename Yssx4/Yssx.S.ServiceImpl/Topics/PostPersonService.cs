﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 岗位人员服务接口
    /// </summary>
    public class PostPersonService : IPostPersonService
    {
        /// <summary>
        /// 保存岗位人员
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SavePostPerson(PostPersonRequest model,long userId)
        {
            return await Task.Run(() =>
            {
                YssxPostPersonnel yssxPostPerson = model.MapTo<YssxPostPersonnel>();
                if (model.Id == 0)
                {
                    yssxPostPerson.Id = IdWorker.NextId();
                    yssxPostPerson.CreateBy = userId;
                    //查找当前最大sort，没有则默认为1
                    List<YssxPostPersonnel> postPersonnelList = DbContext.FreeSql.Select<YssxPostPersonnel>().Where(s => s.DepartmentId == model.DepartmentId).ToList();
                    int maxSort = 1;
                    if (postPersonnelList.Count > 0)
                        maxSort = postPersonnelList.Max(s => s.Sort);
                    yssxPostPerson.Sort = maxSort+1;
                }
                else
                {
                    yssxPostPerson.UpdateBy = userId;
                    yssxPostPerson.UpdateTime = DateTime.Now;
                }
                var result = model.Id > 0 ? UpdatePostPerson(yssxPostPerson) : AddPostPerson(yssxPostPerson);
                return result;
            });
        }
        /// <summary>
        /// 新增岗位人员
        /// </summary>
        /// <param name="yssxCase"></param>
        /// <param name="positions"></param>
        /// <returns></returns>
        private static ResponseContext<bool> AddPostPerson(YssxPostPersonnel yssxPostPerson)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            try
            {
                DbContext.FreeSql.Insert(yssxPostPerson).ExecuteAffrows();
            }
            catch (Exception)
            {
                result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            }
            return result;
            
        }
        private static ResponseContext<bool> UpdatePostPerson(YssxPostPersonnel yssxPostPerson)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            try
            {
                DbContext.FreeSql.Update<YssxPostPersonnel>(yssxPostPerson).SetSource(yssxPostPerson).ExecuteAffrows();
            }
            catch (Exception)
            {
                result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            }
            return result;
        }
        /// <summary>
        /// 删除岗位人员
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeletePostPerson(long id)
        {
            await DbContext.FreeSql.GetRepository<YssxPostPersonnel>().UpdateDiy.Set(c => new YssxPostPersonnel
            {
                IsDelete = CommonConstants.IsDelete,
                UpdateTime = DateTime.Now
            }).Where(c => c.Id == id).ExecuteAffrowsAsync();
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
        /// <summary>
        /// 获取岗位人员列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<PageResponse<PostPersonDto>> GetPostPersonList(PostPersonListRequest model)
        {
            var select = DbContext.FreeSql.Select<YssxPostPersonnel>()
                .Where(c => c.IsDelete == CommonConstants.IsNotDelete);

            if (model.DepartmentId > 0)
                select.Where(c => c.DepartmentId == model.DepartmentId);

            var totalCount = select.Count();
            var sql = select.OrderBy(s=>s.Sort).Page(model.PageIndex, model.PageSize).ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<PostPersonDto>(sql);

            var pageData = new PageResponse<PostPersonDto>() { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            return pageData;
        }
        /// <summary>
        /// 调整顺序  isAscending:true升序   false降序
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ChangeSort(long id, bool isAscending = true)
        {
            var postPersonnel = DbContext.FreeSql.Select<YssxPostPersonnel>().Where(s => s.Id == id).First();
            if (isAscending)
            {
                if (postPersonnel.Sort == 1)
                    return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
                else
                {
                    postPersonnel.Sort = postPersonnel.Sort - 1;
                    postPersonnel.UpdateTime = DateTime.Now;
                }
                DbContext.FreeSql.Transaction(() =>
                {
                    DbContext.FreeSql.Update<YssxPostPersonnel>(postPersonnel).SetSource(postPersonnel).ExecuteAffrows();
                    DbContext.FreeSql.GetRepository<YssxPostPersonnel>().UpdateDiy.Set(c => new YssxPostPersonnel
                    {
                        Sort = postPersonnel.Sort + 1,
                        UpdateTime = DateTime.Now
                    }).Where(c => c.Sort == postPersonnel.Sort && c.Id != postPersonnel.Id).ExecuteAffrowsAsync();

                });
            }
            else
            {
                DbContext.FreeSql.Transaction(() =>
                {
                    postPersonnel.Sort = postPersonnel.Sort + 1;
                    postPersonnel.UpdateTime = DateTime.Now;
                    DbContext.FreeSql.Update<YssxPostPersonnel>(postPersonnel).SetSource(postPersonnel).ExecuteAffrows();
                    DbContext.FreeSql.GetRepository<YssxPostPersonnel>().UpdateDiy.Set(c => new YssxPostPersonnel
                    {
                        Sort = postPersonnel.Sort - 1,
                        UpdateTime = DateTime.Now
                    }).Where(c => c.Sort == postPersonnel.Sort && c.Id != postPersonnel.Id).ExecuteAffrowsAsync();

                });
            }
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
    }
}
