﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 部门服务接口
    /// </summary>
    public class DepartmentService : IDepartmentService
    {
        /// <summary>
        /// 保存部门
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveDepartment(DepartmentRequest model,long userId)
        {
            return await Task.Run(() =>
            {
                YssxDepartment yssxDepartment = model.MapTo<YssxDepartment>();
                if (model.Id == 0)
                {
                    yssxDepartment.Id = IdWorker.NextId();
                    yssxDepartment.CreateBy = userId;
                    //查找当前最大sort，没有则默认为1
                    List<YssxDepartment> departmentList = DbContext.FreeSql.Select<YssxDepartment>().Where(s => s.CaseId == model.CaseId).ToList();
                    int maxSort = 1;
                    if (departmentList.Count>0)
                        maxSort = departmentList.Max(s => s.Sort);
                    yssxDepartment.Sort = maxSort+1;
                }
                else
                {
                    yssxDepartment.UpdateBy = userId;
                    yssxDepartment.UpdateTime = DateTime.Now;
                }
                var result = model.Id > 0 ? UpdateDepartment(yssxDepartment) : AddDepartment(yssxDepartment);
                return result;
            });
        }
        /// <summary>
        /// 新增部门
        /// </summary>
        /// <param name="yssxCase"></param>
        /// <param name="positions"></param>
        /// <returns></returns>
        private static ResponseContext<bool> AddDepartment(YssxDepartment yssxDepartment)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            try
            {
                DbContext.FreeSql.Insert(yssxDepartment).ExecuteAffrows();
            }
            catch (Exception)
            {
                result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            }
            return result;
            
        }
        private static ResponseContext<bool> UpdateDepartment(YssxDepartment yssxDepartment)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            try
            {
                DbContext.FreeSql.Update<YssxDepartment>(yssxDepartment).SetSource(yssxDepartment).ExecuteAffrows();
            }
            catch (Exception)
            {
                result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            }
            return result;
        }
        /// <summary>
        /// 删除部门
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteDepartment(long id)
        {
            await DbContext.FreeSql.GetRepository<YssxDepartment>().UpdateDiy.Set(c => new YssxDepartment
            {
                IsDelete = CommonConstants.IsDelete,
                UpdateTime = DateTime.Now
            }).Where(c => c.Id == id).ExecuteAffrowsAsync();
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<PageResponse<DepartmentDto>> GetDepartmentList(DepartmentListRequest model)
        {
            var select = DbContext.FreeSql.Select<YssxDepartment>()
                .Where(c => c.IsDelete == CommonConstants.IsNotDelete);

            if (model.CaseId > 0)
                select.Where(c => c.CaseId == model.CaseId);

            var totalCount = select.Count();
            var sql = select.OrderBy(s=>s.Sort).Page(model.PageIndex, model.PageSize).ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<DepartmentDto>(sql);

            var pageData = new PageResponse<DepartmentDto>() { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            return pageData;
        }
        /// <summary>
        /// 调整顺序  isAscending:true升序   false降序
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ChangeSort(long id,bool isAscending=true)
        {
            var department = DbContext.FreeSql.Select<YssxDepartment>().Where(s => s.Id == id).First();
            if (isAscending)
            {
                if (department.Sort == 1)
                    return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
                else
                {
                    department.Sort = department.Sort - 1;
                    department.UpdateTime = DateTime.Now;
                }
                DbContext.FreeSql.Transaction(() =>
                {
                    DbContext.FreeSql.Update<YssxDepartment>(department).SetSource(department).ExecuteAffrows();
                    DbContext.FreeSql.GetRepository<YssxDepartment>().UpdateDiy.Set(c => new YssxDepartment
                    {
                        Sort = department.Sort + 1,
                        UpdateTime = DateTime.Now
                    }).Where(c => c.Sort == department.Sort && c.Id != department.Id).ExecuteAffrowsAsync();

                });
            }
            else
            {
                DbContext.FreeSql.Transaction(() =>
                {
                    department.Sort = department.Sort + 1;
                    department.UpdateTime = DateTime.Now;
                    DbContext.FreeSql.Update<YssxDepartment>(department).SetSource(department).ExecuteAffrows();
                    DbContext.FreeSql.GetRepository<YssxDepartment>().UpdateDiy.Set(c => new YssxDepartment
                    {
                        Sort = department.Sort - 1,
                        UpdateTime = DateTime.Now
                    }).Where(c => c.Sort == department.Sort && c.Id != department.Id).ExecuteAffrowsAsync();

                });
            }
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
    }
}
