﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices.Topics;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Topics;

namespace Yssx.S.ServiceImpl.Topics
{
    /// <summary>
    /// 岗位管理
    /// </summary>
    public class PositionService : IPositionService
    {
        /// <summary>
        /// 获取岗位列表（不分页）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetPositionListResponse>>> GetPositionList()
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetRepository<YssxPosition>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).Select(m => new GetPositionListResponse
                {
                    Id = m.Id,
                    Name = m.Name,
                    ThumbnailText = m.ThumbnailText,
                    Remark = m.Remark,
                    FileUrl = m.FileUrl
                }).ToList();

                return new ResponseContext<List<GetPositionListResponse>> { Code = CommonConstants.SuccessCode, Data = selectData };
            });
        }

        /// <summary>
        /// 获取岗位列表（分页）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<GetPositionListResponse>>> GetPositionListByPage(GetPositionListRequest model)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetRepository<YssxPosition>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(m => m.CreateTime);
                if (!string.IsNullOrEmpty(model.SearchText))
                {
                    selectData = selectData.Where(m => m.Name.Contains(model.SearchText) || m.Remark.Contains(model.SearchText));
                }
                var totalCount = selectData.Count();
                var sql = selectData.Page(model.PageIndex, model.PageSize).ToSql("Id, Name, ThumbnailText, Remark, FileUrl, IsDelete, CreateBy, CreateTime, UpdateBy, UpdateTime");
                var items = DbContext.FreeSql.Ado.Query<YssxPosition>(sql).Select(m => new GetPositionListResponse
                {
                    Id = m.Id,
                    Name = m.Name,
                    ThumbnailText = m.ThumbnailText,
                    Remark = m.Remark,
                    FileUrl = m.FileUrl
                }).ToList();
                var res = new PageResponse<GetPositionListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
                return new ResponseContext<PageResponse<GetPositionListResponse>> { Code = CommonConstants.SuccessCode, Data = res };
            });
        }

        /// <summary>
        /// 保存岗位
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SavePosition(PositionRequest model,long userId)
        {
            long opreationId = userId;//操作人ID

            if (model.Id > 0)
            {
                return await Task.Run(() =>
                {
                    return UpdatePosition(model,userId);
                });
            }
            if (string.IsNullOrEmpty(model.Name))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
            }
            if (string.IsNullOrEmpty(model.FileUrl))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "岗位图标不能为空" };
            }
            if (DbContext.FreeSql.Select<YssxPosition>().Any(m => m.Name == model.Name))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能重复" };
            }

            var dateNow = DateTime.Now;
            YssxPosition position = new YssxPosition
            {
                Id = IdWorker.NextId(),
                Name = model.Name,
                ThumbnailText = model.ThumbnailText,
                Remark = model.Remark,
                FileUrl = model.FileUrl,
                IsDelete = CommonConstants.IsNotDelete,
                CreateBy = opreationId,
                CreateTime = dateNow
            };

            return await Task.Run(() =>
            {
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Insert(position).ExecuteAffrows();
                    tran.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 修改岗位
        /// </summary>
        /// <returns></returns>
        private ResponseContext<bool> UpdatePosition(PositionRequest model,long userId)
        {
            long opreationId = userId;//操作人ID

            var position = DbContext.FreeSql.Select<YssxPosition>().Where(m => m.Id == model.Id).First();
            if (position == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息" };

            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };

            if (string.IsNullOrEmpty(model.FileUrl))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "岗位图标不能为空" };

            if (DbContext.FreeSql.Select<YssxPosition>().Any(m => m.Name == model.Name && m.Id != model.Id))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能重复" };

            //该岗位是否已被使用
            bool isUsedPosition = DbContext.FreeSql.Select<YssxCasePosition>().Any(m => m.PostionId == model.Id);

            var dateNow = DateTime.Now;
            if (!isUsedPosition)
            {
                position.Name = model.Name;
                position.Remark = model.Remark;
            }
            position.ThumbnailText = model.ThumbnailText;
            position.FileUrl = model.FileUrl;
            position.UpdateBy = opreationId;
            position.UpdateTime = dateNow;

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<YssxPosition>().SetSource(position).UpdateColumns(m => new { m.Name, m.Remark, m.ThumbnailText, m.FileUrl, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                tran.Commit();
            }

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 删除岗位
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeletePosition(long id,long userId)
        {
            long opreationId = userId;//操作人ID

            if (id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，入参信息有误" };
            var student = DbContext.FreeSql.Select<YssxPosition>().Where(m => m.Id == id).First();
            if (student == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息" };

            if (DbContext.FreeSql.Select<YssxCasePosition>().Any(m => m.PostionId == id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "该岗位已被使用，不允许删除" };

            student.IsDelete = CommonConstants.IsDelete;
            student.UpdateBy = opreationId;
            student.UpdateTime = DateTime.Now;

            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<YssxPosition>().SetSource(student).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }


    }
}
