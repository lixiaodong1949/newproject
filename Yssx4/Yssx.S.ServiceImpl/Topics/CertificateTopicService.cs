﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices.Topics;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl.Topics
{
    /// <summary>
    /// 凭证题
    /// </summary>
    public class CertificateTopicService : ICertificateTopicService
    {
        /// <summary>
        /// 新增/修改分录题
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveCertificateTopic(CertificateTopicRequest model)
        {
            return await Task.Run(() =>
            {
                var yssxCase = DbContext.FreeSql.Select<YssxCase>()
                .Where(c => c.Id == model.CaseId && c.IsDelete == CommonConstants.IsNotDelete).First();

                if (yssxCase == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没找到该分录题隶属的案例" };
                else
                    model.CalculationScoreType = yssxCase.SetType;
                //类型从案例中读取
                if (model.CalculationScoreType == AccountEntryCalculationType.SetSingle)
                {
                    //查找案例  如果案例是统一设置 计分方式为权重 则设置权重占比默认1:1  ？
                    if (model.InVal <= 0 || model.OutVal <= 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "分录题计分方式为单独设置,外围里围值必须大于0" };
                }
                ResponseContext<bool> response = new ResponseContext<bool>();
                YssxTopic yssxTopic = model.MapTo<YssxTopic>();
                yssxTopic.IsGzip = CommonConstants.IsGzip;
                yssxTopic.QuestionType = QuestionType.AccountEntry;
                //分录题对应topic题目计分方式根据分录题的计分方式设置
                yssxTopic.CalculationType = model.CalculationScoreType == AccountEntryCalculationType.Cell ? CalculationType.FreeCalculation : CalculationType.AvgCellCount;

                YssxCertificateTopic yssxCore = model.MapTo<YssxCertificateTopic>();
                List<YssxCertificateDataRecord> yssxCertificateData = model.DataRecords.MapTo<List<YssxCertificateDataRecord>>();
                List<YssxTopicFile> yssxTopicFiles = new List<YssxTopicFile>();
                StringBuilder stringBuilder = new StringBuilder();

                if (model.QuestionFile != null)
                {
                    foreach (var item in model.QuestionFile)
                    {
                        YssxTopicFile yssxTopicFile = new YssxTopicFile();
                        yssxTopicFile.Id = IdWorker.NextId();
                        yssxTopicFile.Name = item.Name;
                        yssxTopicFile.Url = item.Url;
                        yssxTopicFile.CreateTime = DateTime.Now;
                        stringBuilder.Append(yssxTopicFile.Id).Append(",");
                        yssxTopicFiles.Add(yssxTopicFile);
                    }
                }
                if (stringBuilder.Length > 0)
                {
                    yssxCore.ImageIds = stringBuilder.ToString().Remove(stringBuilder.Length - 1, 1);
                    yssxTopic.TopicFileIds = stringBuilder.ToString().Remove(stringBuilder.Length - 1, 1);
                    yssxCore.InvoicesNum = model.QuestionFile.Count;
                }

                if (model.Id == 0)
                {
                    yssxCore.Id = IdWorker.NextId();
                }
                else
                {
                    YssxCertificateTopic yssxCoreCertificate = DbContext.FreeSql.Select<YssxCertificateTopic>().Where(x => x.TopicId == yssxTopic.Id).First();
                    yssxCore.Id = yssxCoreCertificate.Id;
                }
                if (yssxCertificateData != null)
                {
                    yssxCertificateData.ForEach(x =>
                    {
                        x.Id = IdWorker.NextId();
                        x.CertificateNo = yssxCore.CertificateNo;
                        x.CertificateDate = yssxCore.CertificateDate;
                        x.CertificateTopicId = yssxCore.Id;
                        //增加所属案例，借方贷方金额除100
                        x.EnterpriseId = model.CaseId;
                        x.CreateTime = DateTime.Now;
                    });
                }
                response = model.Id == 0 ? AddCertificateTopic(yssxTopic, yssxCore, yssxCertificateData, yssxTopicFiles) : UpdateCertificateTopic(yssxTopic, yssxCore, yssxCertificateData, yssxTopicFiles);
                return response;
            });
        }
        /// <summary>
        /// 新增凭证
        /// </summary>
        /// <param name="yssxTopic"></param>
        /// <param name="yssxCore"></param>
        /// <param name="yssxCertificateData"></param>
        /// <param name="yssxTopicFiles"></param>
        /// <returns></returns>
        private ResponseContext<bool> AddCertificateTopic(YssxTopic yssxTopic, YssxCertificateTopic yssxCore, List<YssxCertificateDataRecord> yssxCertificateData, List<YssxTopicFile> yssxTopicFiles)
        {
            ResponseContext<bool> response = new ResponseContext<bool>();
            yssxTopic.Id = IdWorker.NextId();
            yssxTopic.CreateTime = DateTime.Now;
            yssxCore.TopicId = yssxTopic.Id;
            yssxCore.CreateTime = DateTime.Now;

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    DbContext.FreeSql.Insert<YssxCertificateDataRecord>().AppendData(yssxCertificateData).ExecuteAffrows();
                    DbContext.FreeSql.Insert(yssxTopic).ExecuteAffrows();
                    DbContext.FreeSql.Insert(yssxCore).ExecuteAffrows();
                    DbContext.FreeSql.Insert<YssxTopicFile>(yssxTopicFiles).ExecuteAffrows();
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = ex.Message };
                }
            }
            response.Code = CommonConstants.SuccessCode;
            response.Data = true;
            return response;
        }
        /// <summary>
        /// 修改凭证
        /// </summary>
        /// <param name="yssxTopic"></param>
        /// <param name="yssxCore"></param>
        /// <param name="yssxCertificateData"></param>
        /// <param name="yssxTopicFiles"></param>
        /// <returns></returns>
        private ResponseContext<bool> UpdateCertificateTopic(YssxTopic yssxTopic, YssxCertificateTopic yssxCore, List<YssxCertificateDataRecord> yssxCertificateData, List<YssxTopicFile> yssxTopicFiles)
        {
            ResponseContext<bool> response = new ResponseContext<bool>();
            YssxCertificateTopic yssxCoreCertificate = DbContext.FreeSql.Select<YssxCertificateTopic>().Where(x => x.TopicId == yssxTopic.Id).First();
            //yssxCore.Id = yssxCoreCertificate.Id;
            if (yssxCoreCertificate == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有找到数据" };
            }
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(yssxCoreCertificate.ImageIds))
                    {
                        string[] ids = yssxCoreCertificate.ImageIds.Split(',');

                        foreach (var item in ids)
                        {
                            DbContext.FreeSql.Delete<YssxTopicFile>().Where(x => x.Id == long.Parse(item)).ExecuteAffrows();
                        }
                    }
                    DbContext.FreeSql.Delete<YssxCertificateDataRecord>().Where(x => x.CertificateTopicId == yssxCoreCertificate.Id).ExecuteAffrows();
                    yssxCore.TopicId = yssxCoreCertificate.TopicId;
                    yssxTopic.Id = yssxCoreCertificate.TopicId;
                    //yssxTopic.UpdateBy
                    yssxTopic.UpdateTime = DateTime.Now;

                    DbContext.FreeSql.Insert<YssxCertificateDataRecord>().AppendData(yssxCertificateData).ExecuteAffrows();
                    DbContext.FreeSql.Update<YssxTopic>().SetSource(yssxTopic).ExecuteAffrows();
                    DbContext.FreeSql.Update<YssxCertificateTopic>().SetSource(yssxCore).ExecuteAffrows();
                    DbContext.FreeSql.Insert<YssxTopicFile>(yssxTopicFiles).ExecuteAffrows();
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = ex.Message };
                }
            }
            response.Code = CommonConstants.SuccessCode;
            response.Data = true;
            return response;
        }

        /// <summary>
        /// 获取单个凭证题
        /// </summary>
        /// <param name="id">题目id</param>
        /// <returns></returns>
        public async Task<ResponseContext<CertificateTopicRequest>> GetCoreCertificateTopic(long id)
        {
            ResponseContext<CertificateTopicRequest> response = new ResponseContext<CertificateTopicRequest>();
            YssxCertificateTopic yssxCoreCertificate = await DbContext.FreeSql.Select<YssxCertificateTopic>().Where(x => x.TopicId == id).FirstAsync();
            if (yssxCoreCertificate != null)
            {
                YssxTopic yssxTopic = await DbContext.FreeSql.Select<YssxTopic>().Where(x => x.Id == yssxCoreCertificate.TopicId).FirstAsync();

                if (yssxTopic != null)
                {
                    response.Data = yssxTopic.MapTo<CertificateTopicRequest>();
                    if (!string.IsNullOrWhiteSpace(yssxCoreCertificate.ImageIds))
                    {
                        string[] ids = yssxCoreCertificate.ImageIds.Split(',');
                        List<QuestionFile> files = new List<QuestionFile>();
                        foreach (var item in ids)
                        {
                            if (string.IsNullOrWhiteSpace(item)) continue;
                            YssxTopicFile yssxTopicFile = DbContext.FreeSql.Select<YssxTopicFile>().Where(x => x.Id == long.Parse(item)).First();
                            if (yssxTopicFile == null) { continue; }
                            files.Add(new QuestionFile { Name = yssxTopicFile.Name, Url = yssxTopicFile.Url });
                        }
                        response.Data.QuestionFile = files;
                    }

                    response.Data.Id = yssxCoreCertificate.Id;
                    response.Data.CertificateDate = yssxCoreCertificate.CertificateDate.ToString();
                    response.Data.CertificateNo = yssxCoreCertificate.CertificateNo;
                    response.Data.EnterpriseId = yssxCoreCertificate.EnterpriseId;
                    response.Data.AccountingManager = yssxCoreCertificate.AccountingManager;
                    response.Data.Auditor = yssxCoreCertificate.Auditor;
                    response.Data.Cashier = yssxCoreCertificate.Cashier;
                    response.Data.Creator = yssxCoreCertificate.Creator;
                    response.Data.IsDisableAM = yssxCoreCertificate.IsDisableAM;
                    response.Data.IsDisableAuditor = yssxCoreCertificate.IsDisableAuditor;
                    response.Data.IsDisableCashier = yssxCoreCertificate.IsDisableCashier;
                    response.Data.IsDisableCreator = yssxCoreCertificate.IsDisableCreator;
                    response.Data.CalculationScoreType = yssxCoreCertificate.CalculationScoreType;
                    response.Data.InVal = yssxCoreCertificate.InVal;
                    response.Data.OutVal = yssxCoreCertificate.OutVal;
                    response.Data.DataRecords = DbContext.FreeSql.Select<YssxCertificateDataRecord>()
                        .Where(x => x.CertificateTopicId == yssxCoreCertificate.Id).ToList().MapTo<List<CertificateDataRecord>>();

                    response.Code = CommonConstants.SuccessCode;
                }
                else
                {
                    response.Code = CommonConstants.ErrorCode;
                    response.Msg = "没有找到数据!";
                }
            }
            else
            {
                response.Code = CommonConstants.ErrorCode;
                response.Msg = "没有找到数据!";
            }
            return response;
        }
        /// <summary>
        /// 根据凭证id删除凭证(分录题)
        /// </summary>
        /// <param name="id">凭证id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCertificateTopicById(long id)
        {
            YssxCertificateTopic certificateTopic = await DbContext.FreeSql
                .Select<YssxCertificateTopic>().Where(x => x.Id == id).FirstAsync();

            return DeleteCertificateTopic(certificateTopic);
        }
        /// <summary>
        /// 根据题库id删除凭证(分录题)
        /// </summary>
        /// <param name="id">题库id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCertificateTopicByTopicId(long id)
        {
            YssxCertificateTopic certificateTopic = await DbContext.FreeSql.Select<YssxCertificateTopic>().Where(x => x.TopicId == id).FirstAsync();
            return DeleteCertificateTopic(certificateTopic);
        }
        private static ResponseContext<bool> DeleteCertificateTopic(YssxCertificateTopic certificateTopic)
        {
            if (certificateTopic == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不存在,是否已经删除，请刷新列表" };

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    YssxTopic yssxTopic = DbContext.FreeSql.Select<YssxTopic>().Where(c => c.Id == certificateTopic.TopicId).First();
                    if (yssxTopic == null)
                        return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到对应题目记录！", false);
                    DbContext.FreeSql.GetRepository<YssxTopic>().UpdateDiy.Set(c => new YssxTopic
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now
                    }).Where(c => c.Id == certificateTopic.TopicId).ExecuteAffrows();

                    if (!string.IsNullOrWhiteSpace(certificateTopic.ImageIds))
                    {
                        string[] ids = certificateTopic.ImageIds.Split(",");
                        foreach (var item in ids)
                        {
                            DbContext.FreeSql.Delete<YssxTopicFile>().Where(x => x.Id == long.Parse(item)).ExecuteAffrows();
                        }
                    }
                    DbContext.FreeSql.Delete<YssxCertificateTopic>().Where(x => x.Id == certificateTopic.Id).ExecuteAffrows();
                    DbContext.FreeSql.Delete<YssxCertificateDataRecord>().Where(x => x.CertificateTopicId == certificateTopic.Id).ExecuteAffrows();
                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除数据异常" };
                }

            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }
    }
}
