﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using Yssx.S.Dto;
using Yssx.S.Dto.Topics;
using Yssx.S.IServices.Topics;
using Yssx.S.Poco;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Topics;

namespace Yssx.S.ServiceImpl.Topics
{
    /// <summary>
    /// 案例
    /// </summary>
    public class CaseService : ICaseService
    {
        #region 
        /// <summary>
        /// 根据id删除
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCase(long caseId)
        {
            YssxCase yssxCase = await DbContext.FreeSql.Select<YssxCase>().Where(c => c.Id == caseId).FirstAsync();
            if (yssxCase == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到源数据！", false);
            DbContext.FreeSql.Transaction(() =>
            {
                //删除案例主表  逻辑删除
                DbContext.FreeSql.GetRepository<YssxCase>().UpdateDiy.Set(c => new YssxCase
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now
                }).Where(c => c.Id == caseId).ExecuteAffrows();
                //删除案例--岗位中间表
                DbContext.FreeSql.GetRepository<YssxCasePosition>().UpdateDiy
                    .Set(c => c.IsDelete, CommonConstants.IsDelete)
                    .Set(c => c.UpdateTime, DateTime.Now)
                    .Where(c => c.CaseId == caseId).ExecuteAffrows();
            });
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
        /// <summary>
        /// 查找单个对象
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<CaseDto>> GetCase(long caseId)
        {
            var select = DbContext.FreeSql.Select<YssxCase>().Where(c => c.Id == caseId && c.IsDelete == CommonConstants.IsNotDelete);
            var ls = await DbContext.FreeSql.Ado.QueryAsync<CaseDto>(select.ToSql("*"));
            ls.ForEach(async o =>
            {
                var postionSql = DbContext.FreeSql.Select<YssxCasePosition>().Where(p => p.CaseId == o.Id).ToSql("PostionId,PostionName,TimeMinutes");
                o.PostionInfos = await DbContext.FreeSql.Ado.QueryAsync<PostionInfo>(postionSql);
                o.OptionType = o.CreateBy != null ? Convert.ToInt32(o.CreateBy) : 0;
            });
            return ls.Count > 0 ? new ResponseContext<CaseDto>(CommonConstants.SuccessCode, "", ls[0]) :
                new ResponseContext<CaseDto>(CommonConstants.SuccessCode, "没查找到对象！", null);
        }
        /// <summary>
        /// 查找案例列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PageResponse<CaseDto>> GetCaseList(CaseListRequest model)
        {
            var select = DbContext.FreeSql.Select<YssxCase>()
                .Where(c => c.IsDelete == CommonConstants.IsNotDelete);
            if (model.OptionType != -1)
                select.Where(c => c.CreateBy == model.OptionType);
            if (!string.IsNullOrEmpty(model.CaseName))
                select.Where(c => c.Name.Contains(model.CaseName));
            if (model.Education.HasValue)
                select.Where(c => c.Education == model.Education);
            if (model.CompetitionType.HasValue)
                select.Where(c => c.CompetitionType == (CompetitionType)model.CompetitionType);
            if (model.TestStatus.HasValue)
                select.Where(c => c.TestStatus == model.TestStatus);
            if (model.CreateBy.HasValue)
                select.Where(c => c.CreateBy == model.CreateBy);
            if (model.StartTime != null && model.EndTime != null)
                select.Where(c => c.CreateTime >= model.StartTime && c.CreateTime <= model.EndTime);
            if (!string.IsNullOrEmpty(model.Region))
                select.Where(c => c.Region.Contains(model.Region));
            if (model.RollUpType.HasValue)
                select.Where(c => c.RollUpType == model.RollUpType);

            var totalCount = select.Count();
            var fields = "Id,Name,Code,Education,RollUpType,IsBigData,CompetitionType,TotalTime,TestStatus,OfficialStatus,Sort,CreateBy,CreateTime,UpdateTime,Region,BGFileUrl,Industry,SxType";
            var sql = select.Page(model.PageIndex, model.PageSize).OrderBy(a => a.Sort).ToSql(fields);
            var items = await DbContext.FreeSql.Ado.QueryAsync<CaseDto>(sql);

            items.ForEach(async o =>
            {
                var postionSql = DbContext.FreeSql.Select<YssxCasePosition>().Where(p => p.CaseId == o.Id).ToSql("PostionId,PostionName,TimeMinutes");
                o.PostionInfos = await DbContext.FreeSql.Ado.QueryAsync<PostionInfo>(postionSql);
                o.QuestionCount = DbContext.FreeSql.Select<YssxTopic>().Where(t => t.CaseId == o.Id && t.ParentId == 0 && t.IsDelete == CommonConstants.IsNotDelete).Count();
                o.OptionType = model.OptionType;
            });

            var pageData = new PageResponse<CaseDto>() { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            return pageData;
        }

        /// <summary>
        /// 查询案例列表+区域
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<PageResponse<CaseDto>> GetCaseAreaList(CaseListRequest model)
        {
            var select = DbContext.FreeSql.Select<YssxCase>()
                .Where(c => c.IsDelete == CommonConstants.IsNotDelete);
            if (model.OptionType != -1)
                select.Where(c => c.CreateBy == model.OptionType);
            if (!string.IsNullOrEmpty(model.CaseName))
                select.Where(c => c.Name.Contains(model.CaseName));
            if (model.Education.HasValue)
                select.Where(c => c.Education == model.Education);
            if (model.CompetitionType.HasValue)
                select.Where(c => c.CompetitionType == (CompetitionType)model.CompetitionType);
            if (model.TestStatus.HasValue)
                select.Where(c => c.TestStatus == model.TestStatus);
            if (model.CreateBy.HasValue)
                select.Where(c => c.CreateBy == model.CreateBy);
            if (model.StartTime != null && model.EndTime != null)
                select.Where(c => c.CreateTime >= model.StartTime && c.CreateTime <= model.EndTime);
            if (!string.IsNullOrEmpty(model.Region))
                select.Where(c => c.Region.Contains(model.Region));
            if (model.RollUpType.HasValue)
                select.Where(c => c.RollUpType == model.RollUpType);

            var totalCount = select.Count();
            var fields = "Id,Name,Code,Education,RollUpType,IsBigData,CompetitionType,TotalTime,TestStatus,OfficialStatus,Sort,CreateBy,CreateTime,UpdateTime,Region,BGFileUrl";
            var sql = select.Page(model.PageIndex, model.PageSize).OrderBy(a => a.Sort).ToSql(fields);
            var items = await DbContext.FreeSql.Ado.QueryAsync<CaseDto>(sql);

            items.ForEach(async o =>
            {
                o.Name = o.Name + (string.IsNullOrEmpty(o.Region) ? "" : (o.Region));
                var postionSql = DbContext.FreeSql.Select<YssxCasePosition>().Where(p => p.CaseId == o.Id).ToSql("PostionId,PostionName,TimeMinutes");
                o.PostionInfos = await DbContext.FreeSql.Ado.QueryAsync<PostionInfo>(postionSql);
                o.QuestionCount = DbContext.FreeSql.Select<YssxTopic>().Where(t => t.CaseId == o.Id && t.ParentId == 0 && t.IsDelete == CommonConstants.IsNotDelete).Count();
                o.OptionType = model.OptionType;
            });

            var pageData = new PageResponse<CaseDto>() { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            return pageData;
        }
        /// <summary>
        /// 查找案例列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CaseNameDto>>> GetCaseNameList()
        {
            var sql = DbContext.FreeSql.Select<YssxCase>()
                .Where(c => c.IsDelete == CommonConstants.IsNotDelete)
                .ToSql("Id,name as CaseName");
            var items = await DbContext.FreeSql.Ado.QueryAsync<CaseNameDto>(sql);
            return new ResponseContext<List<CaseNameDto>>(CommonConstants.SuccessCode, null, items);
        }
        /// <summary>
        /// 保存案例
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveCase(CaseRequest model)
        {
            return await Task.Run(() =>
            {
                if (model.SetType == 0)
                {
                    if (model.OutVal <= 0 || model.InVal <= 0)
                    {
                        return new ResponseContext<bool>(CommonConstants.ErrorCode, "分录题分数占比类型为统一设置，里围外围值必须大于0！", false);
                    }
                }
                if (model.AccountEntryScale < 0 || model.AccountEntryScale >= 100)
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "分录题分数占比必须>=0且<100！", false);

                YssxCase yssxCase = model.MapTo<YssxCase>();
                if (model.Id == 0)
                    yssxCase.Id = IdWorker.NextId();
                //上线前-临时设置创建人（1.测试/开发 2.专业组）
                yssxCase.CreateBy = model.OptionType;
                //yssxCase.OfficialStatus=model.Status;
                //岗位集合
                List<YssxCasePosition> positions = new List<YssxCasePosition>();
                //来源岗位信息集合
                List<PostionInfo> positionInfos = model.PostionInfos;
                if (positionInfos.Count > 0)
                {
                    positionInfos.ForEach(p =>
                    {
                        YssxCasePosition casePosition = new YssxCasePosition();
                        casePosition.Id = IdWorker.NextId();
                        casePosition.PostionId = p.PostionId;
                        casePosition.PostionName = p.PostionName;
                        casePosition.CaseId = yssxCase.Id;
                        casePosition.TimeMinutes = p.TimeMinutes;
                        positions.Add(casePosition);
                    });
                }
                var result = model.Id > 0 ? UpdateCaseInfo(yssxCase, positions) : AddCaseInfo(yssxCase, positions);
                return result;
            });

        }
        /// <summary>
        /// 新增案例
        /// </summary>
        /// <param name="yssxCase"></param>
        /// <param name="positions"></param>
        /// <returns></returns>
        private static ResponseContext<bool> AddCaseInfo(YssxCase yssxCase, List<YssxCasePosition> positions)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            //List<YssxSubjectTemp> list = DbContext.FreeSql.GetRepository<YssxSubjectTemp, long>().Select.ToList();
            //获取科目信息集合并关联案例-caseid
            List<YssxSubject> subjects = GetSubjects(yssxCase.Id);
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    DbContext.FreeSql.Insert<YssxSubject>(subjects).ExecuteAffrows();
                    DbContext.FreeSql.Insert(yssxCase).ExecuteAffrows();
                    //DbContext.FreeSql.Insert(positions).ExecuteAffrows();
                    positions.ForEach(p =>
                    {
                        DbContext.FreeSql.Insert(p).ExecuteAffrows();
                    });
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            return result;
        }
        /// <summary>
        /// 修改案例
        /// </summary>
        /// <param name="yssxCase"></param>
        /// <param name="positions"></param>
        /// <returns></returns>
        private static ResponseContext<bool> UpdateCaseInfo(YssxCase yssxCase, List<YssxCasePosition> positions)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    if (!DbContext.FreeSql.Select<YssxCase>().Any(c => c.Id == yssxCase.Id))
                        return new ResponseContext<bool>(CommonConstants.ErrorCode, "未找到数据源！", false);
                    //修改案例主表
                    DbContext.FreeSql.Update<YssxCase>(yssxCase).SetSource(yssxCase).ExecuteAffrows();
                    //删除案例--岗位中间表
                    DbContext.FreeSql.Delete<YssxCasePosition>().Where(cp => cp.CaseId == yssxCase.Id).ExecuteAffrows();
                    //添加案例--岗位中间表
                    positions.ForEach(p =>
                    {
                        DbContext.FreeSql.Insert(p).ExecuteAffrows();
                    });
                    //修改关联试卷总时长
                    DbContext.FreeSql.GetRepository<ExamPaper>().UpdateDiy
                               .Set(c => c.TotalMinutes, yssxCase.TotalTime)
                               .Set(c => c.UpdateTime, DateTime.Now)
                               .Where(c => c.CaseId == yssxCase.Id).ExecuteAffrows();

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            return result;
        }

        /// <summary>
        /// 设置案例的区域
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetCaseRegion(long id, string region, long userId)
        {
            long opreationId = userId;//操作人ID

            if (id <= 0 || string.IsNullOrEmpty(region))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "设置失败，入参信息有误" };
            var entity = DbContext.FreeSql.Select<YssxCase>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "设置失败，找不到原始信息" };

            entity.Region = region;
            entity.UpdateBy = opreationId;
            entity.UpdateTime = DateTime.Now;

            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<YssxCase>().SetSource(entity).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.Region }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        #endregion
        /// <summary>
        /// 修改案例场景实训封面图
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileUrl"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetCaseSceneImg(long id, string fileUrl)
        {
            return await Task.Run(() =>
            {
                //修改案例场景实训封面图
                DbContext.FreeSql.GetRepository<YssxCase>().UpdateDiy
                           .Set(c => c.BGFileUrl, fileUrl)
                           .Where(c => c.Id == id).ExecuteAffrows();
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
        #region 
        /// <summary>
        /// 复制案例
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CloneCase(CloneCaseRequest model, long userId)
        {
            var result = true;
            var freeSql = DbContext.FreeSql;
            ResponseContext<bool> response = new ResponseContext<bool>();
            try
            {
                if (model.ClonePostionInfos == null || model.ClonePostionInfos.Count == 0)
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "岗位信息不能为空！", false);
                //查找案例主表
                var sourceCase = freeSql.Select<YssxCase>().Where(c => c.Id == model.SourceId && c.IsDelete == CommonConstants.IsNotDelete).First();
                if (sourceCase == null)
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "源数据不存在！", false);
                YssxCase targetCase = sourceCase;
                targetCase.Id = IdWorker.NextId();
                targetCase.Name = model.TargetName;
                int maxSort = freeSql.Select<YssxCase>().Where(c => c.IsDelete == CommonConstants.IsNotDelete).Max(c => c.Sort);
                targetCase.Sort = maxSort + 1;
                targetCase.CreateBy = userId;
                targetCase.CreateTime = DateTime.Now;
                // 增加案例岗位中间表
                List<YssxCasePosition> yssxCasePositions = freeSql.Select<YssxCasePosition>()
                    .Where(c => c.CaseId == model.SourceId && c.IsDelete == CommonConstants.IsNotDelete).ToList();
                //查找该案例下所有题目  （分录题和综合需特殊处理）
                var sourceTopics = freeSql.Select<YssxTopic>()
                    .Where(t => t.CaseId == model.SourceId && t.IsDelete == CommonConstants.IsNotDelete && t.ParentId == 0).ToList();

                #region 增加案例关联科目信息及摘要
                //获取科目信息集合并关联案例-caseid  
                List<YssxSubject> subjects = freeSql.Select<YssxSubject>().Where(s => s.EnterpriseId == model.SourceId).ToList();//源案例关联科目信息
                var targetSubjects = GetTargetSubjects(subjects, targetCase.Id, userId);
                List<YssxSummary> summaries = freeSql.Select<YssxSummary>().Where(s => s.EnterpriseId == model.SourceId).ToList();//源案例关联摘要信息
                summaries.ForEach(s => { s.Id = IdWorker.NextId(); s.EnterpriseId = targetCase.Id; });
                #endregion
                #region 增加案例岗位中间表数据
                List<YssxCasePosition> casePositions = new List<YssxCasePosition>();
                yssxCasePositions.ForEach(c =>
                {
                    c.Id = IdWorker.NextId();
                    c.CreateBy = userId;
                    c.CreateTime = DateTime.Now;
                    c.CaseId = targetCase.Id;
                    //找目标岗位id和名称
                    c.PostionId = model.ClonePostionInfos.Where(cp => cp.SourcePostionId == c.PostionId).Select(cp => cp.TargetPostionId).FirstOrDefault();
                    c.PostionName = model.ClonePostionInfos.Where(cp => cp.SourcePostionName == c.PostionName).Select(cp => cp.TargetPostionName).FirstOrDefault();
                    casePositions.Add(c);
                });
                #endregion
                freeSql.GetRepository<YssxCase>().Insert(targetCase);
                freeSql.GetRepository<YssxSubject>().Insert(targetSubjects);
                freeSql.GetRepository<YssxSummary>().Insert(summaries);

                #region 增加案例岗位中间表数据
                freeSql.GetRepository<YssxCasePosition>().Insert(casePositions);
                //casePositions.ForEach(c =>
                //{
                //    uow.GetRepository<YssxCasePosition>().Insert(c);
                //});
                #endregion
                #region 题目复制
                sourceTopics.ForEach(s =>
                {
                    var sourceTopicId = s.Id;//记录源id
                    s.Id = IdWorker.NextId();//重新给id赋值,标记为新对象
                    s.CaseId = targetCase.Id;
                    s.CreateBy = userId;
                    s.CreateTime = DateTime.Now;
                    s.IsGzip = CommonConstants.IsGzip;
                    var sourcePostionId = s.PositionId;
                    if (s.PositionId > 0)
                        s.PositionId = model.ClonePostionInfos.Where(cp => cp.SourcePostionId == s.PositionId).Select(cp => cp.TargetPostionId).FirstOrDefault();
                    //题目复制
                    CloneQuestion(freeSql, s, sourceTopicId, targetSubjects, userId);
                });
                #endregion
            }
            catch (Exception ex)
            {
                result = false;
                CommonLogger.Error($"{model.SourceId}{model.TargetName}复制过程中异常" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, ex);
            }
            if (result)
            {
                response.Code = CommonConstants.SuccessCode;
                response.Data = true;
            }
            else
            {
                response.Code = CommonConstants.ErrorCode;
                response.Msg = "复制过程中异常！";
                response.Data = false;
            }
            return response;
        }
        /// <summary>
        /// 一键更新案例下分录题的AnswerValue
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ReplaceAnswerValue(long caseId, long userId)
        {
            ResponseContext<bool> response = new ResponseContext<bool>();
            //所有分录题列表（包括子题目）
            var yssxTopics = await DbContext.FreeSql.Select<YssxTopic>()
                .Where(t => t.CaseId == caseId && t.QuestionType == QuestionType.AccountEntry).ToListAsync();

            var yssxCase = await DbContext.FreeSql.Select<YssxCase>().Where(t => t.Id == caseId).FirstAsync();
            var result = true;
            List<YssxTopic> topics = new List<YssxTopic>();
            List<YssxCertificateTopic> certificateTopics = new List<YssxCertificateTopic>();
            try
            {
                foreach (YssxTopic item in yssxTopics)
                {
                    //解码
                    var newAnswerValue = HttpUtility.UrlDecode(item.AnswerValue);
                    //反序列化answervalue
                    var obj = JsonConvert.DeserializeObject<AnswerObj>(newAnswerValue);
                    //var DynamicObject = JsonConvert.DeserializeObject<dynamic>(json);
                    //JToken jToken = JToken.Parse(json);
                    // 更改摘要
                    UpdateSummary(ref obj, caseId);
                    // 更改科目
                    UpdateSubject(ref obj, caseId);
                    // 更改制单人
                    var yssxCertificate = DbContext.FreeSql.Select<YssxCertificateTopic>().Where(c => c.TopicId == item.Id).First();
                    if (yssxCertificate != null)
                    {
                        if (yssxCertificate.IsDisableCreator == Status.Enable)
                        {
                            UpdateCreator(ref obj, item, yssxCase, ref yssxCertificate, userId);
                        }
                    }
                    var targetAnswerValue = JsonConvert.SerializeObject(obj);
                    //url编码
                    item.AnswerValue = HttpUtility.UrlEncode(targetAnswerValue);
                    item.UpdateBy = userId;
                    item.UpdateTime = DateTime.Now;
                    topics.Add(item);
                    certificateTopics.Add(yssxCertificate);
                }

                foreach (var item in topics)
                {
                    DbContext.FreeSql.GetRepository<YssxTopic>().UpdateDiy.SetSource(item)
                        .UpdateColumns(t => new { t.AnswerValue, t.UpdateBy, t.UpdateTime }).ExecuteAffrows();
                }
                foreach (var yssxCertificate in certificateTopics)
                {
                    DbContext.FreeSql.GetRepository<YssxCertificateTopic>().UpdateDiy.SetSource(yssxCertificate)
                        .UpdateColumns(t => new { t.Creator, t.UpdateBy, t.UpdateTime }).ExecuteAffrows();
                }
            }
            catch (Exception ex)
            {
                CommonLogger.Error($"{caseId}案例更新AnswerValue过程中出错" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, ex);
                result = false;

            }
            if (result)
            {
                response.Code = CommonConstants.SuccessCode;
                response.Data = true;
            }
            else
            {
                response.Code = CommonConstants.ErrorCode;
                response.Msg = "更新过程中出错！";
                response.Data = false;
            }
            return response;
        }
        private void UpdateSummary(ref AnswerObj obj, long caseId)
        {
            var summaryInfo = obj?.ItemCtrlRow;
            if (summaryInfo != null && summaryInfo.Items != null && summaryInfo.Items.Count > 0)
            {
                var summaryIdValue = summaryInfo.Items[0].Value;

                if (!string.IsNullOrEmpty(summaryIdValue))
                {
                    //源摘要id
                    var sourceSummaryId = long.Parse(summaryIdValue);
                    var sourceSummary = DbContext.FreeSql.Select<YssxSummary>()
                        .Where(s => s.Id == sourceSummaryId).First(s => s.Summary);
                    //目标摘要id
                    var targetSummaryId = DbContext.FreeSql.Select<YssxSummary>()
                        .Where(s => s.Summary == sourceSummary && s.EnterpriseId == caseId).First(s => s.Id);
                    summaryInfo.Items[0].Value = targetSummaryId.ToString();
                }

            }
        }
        private void UpdateSubject(ref AnswerObj obj, long caseId)
        {
            var bodyInfos = obj?.BodyCtrlRows;
            if (bodyInfos != null && bodyInfos.Count > 0)
            {
                foreach (BodyCtrlRows bRow in bodyInfos)
                {
                    if (bRow.Items != null && bRow.Items.Count > 0)
                    {
                        var subjectIdValue = bRow.Items[0].Value;
                        if (!string.IsNullOrEmpty(subjectIdValue))
                        {
                            //源科目id
                            var sourceSubjectId = long.Parse(subjectIdValue);
                            var sourceSubjectCode = DbContext.FreeSql.Select<YssxSubject>()
                                .Where(s => s.Id == sourceSubjectId).First(s => s.SubjectCode);

                            //目标科目id
                            var targetSubjectId = DbContext.FreeSql.Select<YssxSubject>()
                                .Where(s => s.SubjectCode == sourceSubjectCode && s.EnterpriseId == caseId).First(s => s.Id);

                            bRow.Items[0].Value = targetSubjectId.ToString();
                        }
                    }
                }
            }
        }
        private void UpdateCreator(ref AnswerObj obj, YssxTopic item, YssxCase yssxCase, ref YssxCertificateTopic yssxCertificate, long userId)
        {
            var footInfos = obj?.FooterCtrlRow;
            if (footInfos != null && footInfos.Items != null && footInfos.Items.Count > 0)
            {
                var footCount = footInfos.Items.Count();
                //制单人姓名
                var creatorName = footInfos.Items[footCount - 1].Value;

                if (!string.IsNullOrEmpty(creatorName))
                {
                    var targetCreatorName = creatorName;
                    //查找该题目所属岗位对应的制单人姓名
                    YssxPosition yssxPosition = null;
                    //岗位id
                    var positionId = item.PositionId;
                    if (item.ParentId > 0)//多题型下的分录题，岗位id从父级找
                        positionId = DbContext.FreeSql.Select<YssxTopic>().Where(t => t.Id == item.ParentId).First(s => s.PositionId);
                    if (positionId > 0)
                    {
                        //    yssxPosition = DbContext.FreeSql.Select<YssxPosition>().Where(p => p.Id == positionId).First();
                        //if (yssxPosition != null)
                        //{
                        switch (positionId)
                        {
                            case 931254105587740://税务会计
                                targetCreatorName = yssxCase.TaxAccountant;
                                break;
                            case 931254235611165://成本会计
                                targetCreatorName = yssxCase.CostAccountant;
                                break;
                            case 931254285942814://业务会计
                                targetCreatorName = yssxCase.BusinessAccountant;
                                break;
                            case 931254034284571://出纳
                                targetCreatorName = yssxCase.Cashier;
                                break;
                            case var q when (q == 931254353051679 || q == 935994308264025)://会计主管或者财务主管
                                targetCreatorName = yssxCase.AccountingManager;
                                break;
                            default:
                                break;
                        }
                    }

                    //更改yssxCertificate制单人
                    yssxCertificate.Creator = targetCreatorName;
                    yssxCertificate.UpdateBy = userId;
                    yssxCertificate.UpdateTime = DateTime.Now;
                    //更改answervalue制单人
                    footInfos.Items[footCount - 1].Value = targetCreatorName;
                }
            }
        }
        //题目复制
        private void CloneQuestion(IFreeSql freeSql, YssxTopic s, long sourceTopicId, List<YssxSubject> subjects, long userId)
        {
            switch (s.QuestionType)
            {
                case var q when (q == QuestionType.SingleChoice || q == QuestionType.MultiChoice || q == QuestionType.Judge):
                    CloneSimpleQuestion(freeSql, s, sourceTopicId, userId);
                    break;
                case var q when (q == QuestionType.FillGrid || q == QuestionType.FillBlank || q == QuestionType.FillGraphGrid || q == QuestionType.SettleAccounts || q == QuestionType.GridFillBank || q == QuestionType.FinancialStatements):
                    CloneComplexQuestion(freeSql, s, userId);
                    break;
                case QuestionType.AccountEntry:
                    CloneAccountEntry(freeSql, s, sourceTopicId, subjects, userId);
                    break;
                case QuestionType.MainSubQuestion:
                    CloneMainSubQuestion(freeSql, s, sourceTopicId, subjects, userId);
                    break;
                default:
                    throw new Exception($"{s.QuestionType}类型的题目不存在");
            }

        }
        //复制简单类型的题目
        private void CloneSimpleQuestion(IFreeSql freeSql, YssxTopic s, long sourceTopicId, long userId)
        {
            //增加题目
            freeSql.GetRepository<YssxTopic>().Insert(s);
            //增加yssx_answer_option  根据sourcetiopicid查找选项内容
            List<YssxAnswerOption> sourceAnswerOptions = freeSql.Select<YssxAnswerOption>()
                .Where(a => a.TopicId == sourceTopicId && a.IsDelete == CommonConstants.IsNotDelete).ToList();

            sourceAnswerOptions.ForEach(a =>
            {
                a.Id = IdWorker.NextId();
                a.TopicId = s.Id;
                a.CreateBy = userId;
                a.CreateTime = DateTime.Now;
            });
            freeSql.GetRepository<YssxAnswerOption>().Insert(sourceAnswerOptions);
        }
        //复制复杂类型的题目
        private void CloneComplexQuestion(IFreeSql freeSql, YssxTopic s, long userId)
        {
            //增加YssxTopicFile
            List<YssxTopicFile> yssxTopicFiles = freeSql.Select<YssxTopicFile>()
                .Where(t => s.TopicFileIds.Contains(t.Id.ToString()) && t.IsDelete == CommonConstants.IsNotDelete).ToList();

            yssxTopicFiles.ForEach(t =>
            {
                t.Id = IdWorker.NextId();
                t.CreateBy = userId;
                t.CreateTime = DateTime.Now;
            });
            freeSql.GetRepository<YssxTopicFile>().Insert(yssxTopicFiles);
            s.TopicFileIds = string.Join(",", yssxTopicFiles.Select(f => f.Id).ToArray());
            //增加题目
            freeSql.GetRepository<YssxTopic>().Insert(s);
        }
        //复制分录题
        private void CloneAccountEntry(IFreeSql freeSql, YssxTopic s, long sourceTopicId, List<YssxSubject> subjects, long userId)
        {
            //YssxCertificateTopic certificateTopic = freeSql.Select<YssxCertificateTopic>()
            //    .Where(c => c.TopicId == sourceTopicId && c.IsDelete == CommonConstants.IsNotDelete).First();
            YssxCertificateTopic certificateTopic = freeSql.GetRepository<YssxCertificateTopic>().Select
                .Where(c => c.TopicId == sourceTopicId && c.IsDelete == CommonConstants.IsNotDelete).First();

            var sourceCertificateTopicId = certificateTopic.Id;
            certificateTopic.Id = IdWorker.NextId();
            certificateTopic.TopicId = s.Id;

            List<YssxCertificateDataRecord> yssxCertificateDataRecords = freeSql.GetRepository<YssxCertificateDataRecord>().Select
                .Where(d => d.CertificateTopicId == sourceCertificateTopicId && d.IsDelete == CommonConstants.IsNotDelete).ToList();

            var subjectIds = yssxCertificateDataRecords.Select(t => t.SubjectId).ToList();
            var sourceSubjects = DbContext.FreeSql.Select<YssxSubject>().Where(ys => subjectIds.Contains(ys.Id)).ToList();

            yssxCertificateDataRecords.ForEach(t =>
            {
                if (t.SubjectId == 0)
                    throw new Exception("凭证数据记录中科目id不能为0，数据异常");
                var sourceSubject = sourceSubjects.FirstOrDefault(ys => ys.Id == t.SubjectId);
                if (sourceSubject == null)
                    throw new Exception($"题目{s.Title}未找到来源科目{t.SubjectId}");
                //根据目标案例id和科目编码查找最新科目并关联 
                //YssxSubject yssxSubject = freeSql.Select<YssxSubject>()
                //.Where(ys => ys.EnterpriseId == s.CaseId && ys.SubjectCode == sourceSubject.SubjectCode).First();
                YssxSubject yssxSubject = subjects.Where(ys => ys.EnterpriseId == s.CaseId && ys.SubjectCode == sourceSubject.SubjectCode).FirstOrDefault();
                //t.SubjectId 要取对应的subjectId
                t.SubjectId = yssxSubject != null ? yssxSubject.Id : 0;
                t.EnterpriseId = s.CaseId;
                t.Id = IdWorker.NextId();
                t.CreateBy = userId;
                t.CertificateTopicId = certificateTopic.Id;
                t.CreateTime = DateTime.Now;
                // uow.GetRepository<YssxCertificateDataRecord>().Insert(t);
            });
            freeSql.GetRepository<YssxCertificateDataRecord>().Insert(yssxCertificateDataRecords);
            //新增YssxTopicFile  考虑是否提取出来
            //List<YssxTopicFile> yssxTopicFiles = freeSql.Select<YssxTopicFile>()
            //    .Where(t => s.TopicFileIds.Contains(t.Id.ToString()) && t.IsDelete == CommonConstants.IsNotDelete).ToList();
            List<YssxTopicFile> yssxTopicFiles = DbContext.FreeSql.GetRepository<YssxTopicFile>().Select
                .Where(t => s.TopicFileIds.Contains(t.Id.ToString()) && t.IsDelete == CommonConstants.IsNotDelete).ToList();

            yssxTopicFiles.ForEach(t =>
            {
                t.Id = IdWorker.NextId();
                t.CreateBy = userId;
                t.CreateTime = DateTime.Now;
                //uow.GetRepository<YssxTopicFile>().Insert(t);
            });
            freeSql.GetRepository<YssxTopicFile>().Insert(yssxTopicFiles);
            s.TopicFileIds = string.Join(",", yssxTopicFiles.Select(f => f.Id).ToArray());
            //增加题目
            freeSql.GetRepository<YssxTopic>().Insert(s);
            certificateTopic.ImageIds = string.Join(",", yssxTopicFiles.Select(f => f.Id).ToArray());
            freeSql.GetRepository<YssxCertificateTopic>().Insert(certificateTopic);
        }
        //复制综合题
        private void CloneMainSubQuestion(IFreeSql freeSql, YssxTopic s, long sourceTopicId, List<YssxSubject> subjects, long userId)
        {
            //增加YssxTopicFile
            List<YssxTopicFile> yssxTopicFiles = freeSql.Select<YssxTopicFile>()
                .Where(t => s.TopicFileIds.Contains(t.Id.ToString()) && t.IsDelete == CommonConstants.IsNotDelete).ToList();

            yssxTopicFiles.ForEach(t =>
            {
                t.Id = IdWorker.NextId();
                t.CreateBy = userId;
                t.CreateTime = DateTime.Now;
            });
            freeSql.GetRepository<YssxTopicFile>().Insert(yssxTopicFiles);
            s.TopicFileIds = string.Join(",", yssxTopicFiles.Select(f => f.Id).ToArray());
            //增加题目
            freeSql.GetRepository<YssxTopic>().Insert(s);
            //查找该综合题下的子题目
            List<YssxTopic> yssxTopics = freeSql.Select<YssxTopic>()
                .Where(t => t.ParentId == sourceTopicId && t.IsDelete == CommonConstants.IsNotDelete).ToList();

            yssxTopics.ForEach(t =>
            {
                var subSourceTopicId = t.Id;//记录源id
                t.Id = IdWorker.NextId();//重新给id赋值,标记为新对象
                t.ParentId = s.Id; //新的题目id
                t.CaseId = s.CaseId; //新的caseid
                t.CreateBy = userId;
                t.CreateTime = DateTime.Now;
                t.IsGzip = CommonConstants.IsGzip;
                CloneQuestion(freeSql, t, subSourceTopicId, subjects, userId);
            });
        }
        private static List<YssxSubject> GetSubjects(long caseId)
        {
            List<YssxSubjectTemp> list = DbContext.FreeSql.GetRepository<YssxSubjectTemp, long>().Select.ToList();
            List<YssxSubject> subjects = list.Select(x => new YssxSubject
            {
                Id = IdWorker.NextId(),
                AssistCode = x.AssistCode,
                SubjectCode = x.SubjectCode,
                SubjectName = x.SubjectName,
                SubjectType = x.SubjectType,
                Code1 = x.Code1,
                Direction = x.Direction,
                EnterpriseId = caseId,
                Code2 = "",
                Code3 = "",
                Code4 = ""
            }).ToList();
            return subjects;
        }
        /// <summary>
        /// 根据源科目列表，获得当前科目下的列表
        /// </summary>
        /// <param name="sourceSubjects"></param>
        /// <returns></returns>
        private List<YssxSubject> GetTargetSubjects(List<YssxSubject> sourceSubjects, long caseId, long userId)
        {
            var targetSubjects = new List<YssxSubject>();
            //查找parent为0的记录数
            var Subjects0 = sourceSubjects.Where(s => s.ParentId == 0).ToList();
            SetTargetSubjects(sourceSubjects, Subjects0, ref targetSubjects, 0, caseId, userId);
            return targetSubjects;
        }
        /// <summary>
        /// 递归得到新的科目列表
        /// </summary>
        /// <param name="sourceSubjects">来源科目集合</param>
        /// <param name="childSourceSubjects">子科目集合</param>
        /// <param name="targetSubjects">目标科目集合</param>
        /// <param name="pid">父id</param>
        /// <param name="caseId">所属案例id</param>
        private void SetTargetSubjects(List<YssxSubject> sourceSubjects, List<YssxSubject> childSourceSubjects, ref List<YssxSubject> targetSubjects, long pid, long caseId, long userId)
        {
            foreach (var item in childSourceSubjects)
            {
                var sourceSid = item.Id;
                var targetSid = IdWorker.NextId();//获取新的id
                item.Id = targetSid;
                item.ParentId = pid;
                item.EnterpriseId = caseId;
                item.CreateTime = DateTime.Now;
                item.CreateBy = userId;
                targetSubjects.Add(item);
                //查找parentid为该科目ID的子集
                var childSubjects = sourceSubjects.Where(s => s.ParentId == sourceSid).ToList();
                SetTargetSubjects(sourceSubjects, childSubjects, ref targetSubjects, targetSid, caseId, userId);
            }
        }
        #endregion

        #region 获取教师购买/赠送的实训套题列表(只获取有行业的套题)
        /// <summary>
        /// 获取教师购买/赠送的实训套题列表(只获取有行业的套题)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxJobTrainingViewModel>>> GetTeacherJobTrainingList(UserTicket user)
        {
            ResponseContext<List<YssxJobTrainingViewModel>> response = new ResponseContext<List<YssxJobTrainingViewModel>>();

            //查询教师购买/赠送的案例列表
            var list = await DbContext.FreeSql.Select<YssxSchoolCase>().From<YssxCase, YssxIndustry>(
                (a, b, c) =>
                a.InnerJoin(x => x.CaseId == b.Id)
                .InnerJoin(x => b.Industry == c.Id)
                ).Where((a, b, c) => a.SchoolId == user.TenantId && a.OpenStatus == Status.Enable && a.IsDelete == CommonConstants.IsNotDelete
                    && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete)
                .GroupBy((a, b, c) => new { b.Id, b.Name, IndustryId = c.Id, IndustryName = c.Name })
                .ToListAsync(x => new YssxJobTrainingViewModel
                {
                    JobTrainingId = x.Key.Id,
                    JobTrainingName = x.Key.Name,
                    IndustryId = x.Key.IndustryId,
                    IndustryName = x.Key.IndustryName,
                });

            //案例Ids
            List<long> caseIds = list.Select(x => x.JobTrainingId).ToList();

            //案例岗位列表
            List<YssxCasePosition> positionList = DbContext.FreeSql.Select<YssxCasePosition>().Where(x => caseIds.Contains(x.CaseId)
                && x.IsDelete == CommonConstants.IsNotDelete).ToList();

            //案例题目列表
            List<YssxTopic> topicList = DbContext.FreeSql.Select<YssxTopic>().Where(x => caseIds.Contains(x.CaseId) && x.ParentId == 0
                && x.IsDelete == CommonConstants.IsNotDelete).ToList();

            list.ForEach(x =>
            {
                //岗位列表
                List<YssxCasePosition> positions = positionList.Where(a => a.CaseId == x.JobTrainingId).ToList();
                if (positions != null && positions.Count > 0)
                {
                    List<YssxPositionViewModel> positionVw = new List<YssxPositionViewModel>();
                    positions.ForEach(a => positionVw.Add(new YssxPositionViewModel
                    {
                        PositionId = a.PostionId,
                        PositionName = a.PostionName,
                    }));

                    x.PositionList = positionVw;
                }

                //分数
                x.Score = topicList.Where(a => a.CaseId == x.JobTrainingId).Sum(a => a.Score);
            });

            response.Data = list;

            return response;
        }

        public async Task<ResponseContext<List<YssxJobTrainingViewModel>>> GetTeacherJobTrainingList(UserTicket user, int year)
        {
            ResponseContext<List<YssxJobTrainingViewModel>> response = new ResponseContext<List<YssxJobTrainingViewModel>>();

            //查询教师购买/赠送的案例列表
            var list = await DbContext.FreeSql.Select<YssxSchoolResource>().From<YssxResourcePeckDetails, YssxCase, YssxIndustry>(
                (a, b, c, d) =>
                a.InnerJoin(x => x.ResourceId == b.ResourceId)
                .InnerJoin(x => b.CaseId == c.Id)
                  .InnerJoin(x => c.Industry == d.Id)
                ).Where((a, b, c, d) => a.SchoolId == user.TenantId && a.Year == year && a.IsDelete == CommonConstants.IsNotDelete
                    && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete)
                .GroupBy((a, b, c, d) => new { b.CaseId, c.Name, IndustryId = d.Id, IndustryName = d.Name })
                .ToListAsync(x => new YssxJobTrainingViewModel
                {
                    JobTrainingId = x.Key.CaseId,
                    JobTrainingName = x.Key.Name,
                    IndustryId = x.Key.IndustryId,
                    IndustryName = x.Key.IndustryName,
                });

            //案例Ids
            List<long> caseIds = list.Select(x => x.JobTrainingId).ToList();

            //案例岗位列表
            List<YssxCasePosition> positionList = DbContext.FreeSql.Select<YssxCasePosition>().Where(x => caseIds.Contains(x.CaseId)
                && x.IsDelete == CommonConstants.IsNotDelete).ToList();

            //案例题目列表
            List<YssxTopic> topicList = DbContext.FreeSql.Select<YssxTopic>().Where(x => caseIds.Contains(x.CaseId) && x.ParentId == 0
                && x.IsDelete == CommonConstants.IsNotDelete).ToList();

            list.ForEach(x =>
            {
                //岗位列表
                List<YssxCasePosition> positions = positionList.Where(a => a.CaseId == x.JobTrainingId).ToList();
                if (positions != null && positions.Count > 0)
                {
                    List<YssxPositionViewModel> positionVw = new List<YssxPositionViewModel>();
                    positions.ForEach(a => positionVw.Add(new YssxPositionViewModel
                    {
                        PositionId = a.PostionId,
                        PositionName = a.PostionName,
                    }));

                    x.PositionList = positionVw;
                }

                //分数
                x.Score = topicList.Where(a => a.CaseId == x.JobTrainingId).Sum(a => a.Score);
            });

            response.Data = list;

            return response;
        }
        #endregion

        #region  获取教师购买/赠送的实训套题列表(App使用)(只获取有行业的套题)
        /// <summary>
        /// 获取教师购买/赠送的实训套题列表(App使用)(只获取有行业的套题)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxJobTrainingViewModelForApp>>> GetTeacherJobTrainingListForApp(UserTicket user, int year)
        {
            ResponseContext<List<YssxJobTrainingViewModelForApp>> response = new ResponseContext<List<YssxJobTrainingViewModelForApp>>();

            //获取教师购买/赠送的实训套题列表
            ResponseContext<List<YssxJobTrainingViewModel>> list = await GetTeacherJobTrainingList(user);
            if (list == null || list.Data == null || list.Data.Count == 0)
                return response;

            var industreyList = list.Data.Select(x => new { x.IndustryId, x.IndustryName }).Distinct().ToList();

            List<YssxJobTrainingViewModelForApp> dataList = new List<YssxJobTrainingViewModelForApp>();

            industreyList.ForEach(x =>
            {
                YssxJobTrainingViewModelForApp model = new YssxJobTrainingViewModelForApp();
                model.IndustryId = x.IndustryId;
                model.IndustryName = x.IndustryName;

                //行业下的岗位实训列表
                List<YssxJobTrainingViewModel> sourceList = list.Data.Where(a => a.IndustryId == x.IndustryId).ToList();

                List<YssxJobTrainingDetaillViewModel> jobTrainingList = new List<YssxJobTrainingDetaillViewModel>();

                sourceList.ForEach(a =>
                {
                    YssxJobTrainingDetaillViewModel jobModel = new YssxJobTrainingDetaillViewModel();
                    jobModel.JobTrainingId = a.JobTrainingId;
                    jobModel.JobTrainingName = a.JobTrainingName;
                    jobModel.Score = a.Score;
                    jobModel.PositionList = a.PositionList;

                    jobTrainingList.Add(jobModel);
                });

                model.JobTrainingList = jobTrainingList;

                dataList.Add(model);
            });

            response.Data = dataList;

            return response;
        }
        #endregion

        #region AnswerValue反序列化相关类
        public class AnswerObj
        {
            public HeaderCtrlRow HeaderCtrlRow { get; set; }
            public FooterCtrlRow FooterCtrlRow { get; set; }

            public ItemCtrlRow ItemCtrlRow { get; set; }
            public List<BodyCtrlRows> BodyCtrlRows { get; set; }
        }
        public class HeaderCtrlRow
        {
            public List<ItemObj> Items { get; set; }
        }
        public class FooterCtrlRow
        {
            public List<ItemObj> Items { get; set; }
        }
        public class ItemCtrlRow
        {
            public List<ItemObj> Items { get; set; }
        }
        public class BodyCtrlRows
        {
            public List<ItemObj> Items;
        }
        public class ItemList
        {
            public List<ItemObj> Items { get; set; }
        }
        public class ItemObj
        {
            public string Name { get; set; }
            public string Value { get; set; }
            public string Score { get; set; }
        }
        #endregion
    }
}