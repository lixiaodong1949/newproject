﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.Configurations;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.M.IServices;
using Yssx.M.ServiceImpl;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 注册验证码
    /// </summary>
    public class RegisteredService : IRegisteredService
    {
        private IbkUserService _service = new bkUserService();
        /// <summary>
        /// 校验注册验证码
        /// </summary>
        /// <param name="Mb"></param>
        /// <param name="VerificationCode"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CheckVerificationCode(string mb, string verificationCode)
        {
            var code = DbContext.FreeSql.Select<YssxVerifyCode>().Where(m => m.MobilePhone == mb).OrderByDescending(x => x.CreateTime).First();
            if (code == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "没有找到验证码，请确认手机号码是否正确!", Data = false };
            }

            else if (code.Code != verificationCode)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "验证码错误!", Data = false };
            }
            else if (DateTime.Now > code.ExpirTime)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "验证码过期!", Data = false };
            }
            return await Task.Run(() =>
            {
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "验证成功!", Data = true };
            });
        }
        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="password">密码</param>
        /// <param name="againPassword">再次输入的密码</param>
        /// <param name="nikename">昵称</param>
        /// <returns></returns>
        public async Task<ResponseContext<YssxUser>> RegisteredUsers(string mb, string password, string againPassword, string nikename)
        {
            var state = false;
            if (password != againPassword)
            {
                return new ResponseContext<YssxUser> { Code = CommonConstants.ErrorCode, Msg = "密码不一致!", Data = null };
            }

            var user = DbContext.FreeSql.Select<YssxUser>().Where(x => x.MobilePhone == mb).First();
            if (user == null)
            {
                user = new YssxUser
                {
                    Id = IdWorker.NextId(),
                    UserName = "",
                    Password = password.Md5(),
                    UserType = 7,
                    IsDelete = 0,
                    CreateBy = 0,
                    UpdateBy = 0,
                    Email = "",
                    MobilePhone = mb,
                    Photo = "",
                    QQ = "",
                    RealName = "",
                    Sex = "",
                    Status = 1,
                    TenantId = 1017666019115035,
                    WeChat = "",
                    IdNumber = "",
                    UpdateTime = DateTime.Now,
                    CreateTime = DateTime.Now,
                    ClientId = Guid.NewGuid(),
                    RegistrationType = 1

                };
                user.NikeName = string.IsNullOrEmpty(nikename) ? user.Id.ToString() : System.Web.HttpUtility.UrlEncode(nikename, Encoding.UTF8);
                state = DbContext.FreeSql.Insert(user).ExecuteAffrows() > 0;
            }
            else
            {
                state = DbContext.FreeSql.Update<YssxUser>(user).Set(x => new YssxUser { Password = password.Md5(), NikeName = string.IsNullOrEmpty(nikename) ? user.Id.ToString() : System.Web.HttpUtility.UrlEncode(nikename, Encoding.UTF8) }).ExecuteAffrows() > 0;
            }

            //var user = yssxUser.MapTo<YssxUser>();
            return await Task.Run(() =>
            {

                user.NikeName = string.IsNullOrEmpty(nikename) ? user.Id.ToString() : System.Web.HttpUtility.UrlDecode(nikename, Encoding.UTF8);
                return new ResponseContext<YssxUser> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state ? user : null, Msg = state ? "注册成功!" : "注册失败!" };
            });
        }

        //public Task<ResponseContext<bool>> SendVerificationCode(string Mb)
        //{
        //    throw new NotImplementedException();
        //}
        /// <summary>
        /// 验证手机号码
        /// </summary>
        /// <param name="mb">手机号码</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> VerifyPhoneNumber(string mb)
        {
            if (DbContext.FreeSql.Select<YssxUser>().Any(m => m.MobilePhone == mb))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码已被注册!", Data = false };
            }

            return await Task.Run(() =>
            {
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        #region 用户注册(学生身份) 通过教师邀请
        /// <summary>
        /// 用户注册(学生身份) 通过教师邀请
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<YssxUser>> RegisterStudentByInvite(RegisterUserRequestModel dto)
        {
            if (dto.ClassId == 0)
                return new ResponseContext<YssxUser> { Code = CommonConstants.ErrorCode, Msg = "班级不能为空!", Data = null };

            if (dto.Password.TrimEnd() != dto.AgainPassword.TrimEnd())
                return new ResponseContext<YssxUser> { Code = CommonConstants.ErrorCode, Msg = "密码不一致!", Data = null };

            var classModel = await DbContext.FreeSql.Select<YssxClass>().Where(x => x.Id == dto.ClassId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (classModel == null)
                return new ResponseContext<YssxUser> { Code = CommonConstants.ErrorCode, Msg = "班级不存在!", Data = null };

            var user = await DbContext.FreeSql.Select<YssxUser>().Where(x => x.MobilePhone == dto.MobilePhone).FirstAsync();

            if (user != null)
                return new ResponseContext<YssxUser> { Code = CommonConstants.ErrorCode, Msg = "该手机号已注册，请直接登录!", Data = null };

            var state = false;

            #region 初始化数据
            //用户信息
            user = new YssxUser
            {
                Id = IdWorker.NextId(),
                Password = dto.Password.TrimEnd().Md5(),
                UserType = (int)UserTypeEnums.Student,
                MobilePhone = dto.MobilePhone,
                Status = (int)Status.Enable,
                TenantId = classModel.TenantId,
                UpdateTime = DateTime.Now,
                CreateTime = DateTime.Now,
                ClientId = Guid.NewGuid(),
                RegistrationType = 1,
                RealName = dto.NikeName
            };
            user.NikeName = string.IsNullOrEmpty(dto.NikeName) ? dto.MobilePhone : System.Web.HttpUtility.UrlEncode(dto.NikeName, Encoding.UTF8);

            YssxStudent student = new YssxStudent
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                Name = user.RealName,
                TenantId = user.TenantId,
                CollegeId = classModel.CollegeId,
                ProfessionId = classModel.ProfessionId,
                ClassId = classModel.Id,
                ClassName = classModel.Name,
                CreateBy = user.Id,
                UpdateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now,
            };
            #endregion

            #region 新增数据
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //用户表
                    var userBack = await uow.GetRepository<YssxUser>().InsertAsync(user);
                    //学生表
                    var studentBack = await uow.GetRepository<YssxStudent>().InsertAsync(student);

                    if (userBack != null && studentBack != null)
                    {
                        state = true;
                        uow.Commit();
                    }
                    else
                    {
                        uow.Rollback();
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }
            #endregion

            user.NikeName = dto.NikeName;

            return new ResponseContext<YssxUser>
            {
                Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode,
                Data = state ? user : null,
                Msg = state ? "注册成功!" : "注册失败!"
            };
        }
        #endregion

    }
}
