﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto.Statistics;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Statistics;

namespace Yssx.S.ServiceImpl
{
    public class DataStatisticsService : IDataStatistics
    {
        public async Task<ResponseContext<List<string>>> GetDates()
        {
            string sql = "select date from yssx_region_statistics group by date";

            List<string> list = await DbContext.FreeSql.Ado.QueryAsync<string>(sql);
            return new ResponseContext<List<string>> { Data = list };
        }

        public async Task<ResponseContext<List<RegionDataDto>>> GetRegionStatistics(string date)
        {
            //List<RegionDataDto> list = await DbContext.FreeSql.GetRepository<YssxRegionStatistics>().Where(x => x.Date == date).OrderByDescending(x=> x.Count).ToListAsync(x => new RegionDataDto
            //{
            //    Id = x.Id,
            //    Count = x.Count,
            //    Date = x.Date,
            //    Name = x.Name
            //});
            string sql = "SELECT a.Name,sum(a.Count) Count FROM yssx_region_statistics a GROUP BY a.Name";
            List<RegionDataDto> list = await DbContext.FreeSql.Ado.QueryAsync<RegionDataDto>(sql);
            list = list.OrderByDescending(x => x.Count).ToList();
            return new ResponseContext<List<RegionDataDto>> { Data = list };
        }

        public async Task<ResponseContext<List<ResourceStatisticsDto>>> GetResourceStatistics()
        {
            ResponseContext<List<ResourceStatisticsDto>> response = new ResponseContext<List<ResourceStatisticsDto>>();
            List<ResourceStatisticsDto> list = new List<ResourceStatisticsDto>();

            //合作院校
            ResourceStatisticsDto dto1 = new ResourceStatisticsDto();
            //long ct1 = await DbContext.FreeSql.GetRepository<YssxSchool>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
            dto1.Number = 2395;
            dto1.ResourseType = 0;
            list.Add(dto1);

            //用户数
            ResourceStatisticsDto dto2 = new ResourceStatisticsDto();
            //long u_ct1 = await DbContext.FreeSql.GetRepository<YssxUser>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
            //long u_ct2 = await DbContext.JSFreeSql.GetRepository<SkillUser>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
            //long u_ct3 = await DbContext.JNFreeSql.GetRepository<YssxJNUser>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
            //dto2.Number = (u_ct1 + u_ct2 + u_ct3 + 45000) * 10;
            dto2.Number = 2256337;
            dto2.ResourseType = 1;
            list.Add(dto2);

            //答题次数
            ResourceStatisticsDto dt3 = new ResourceStatisticsDto();
            long d_ct2 = await DbContext.FreeSql.GetRepository<ExamStudentGradeDetail>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
            long d_ct3 = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
            dt3.Number = (d_ct2 + d_ct3) * 3;
            dt3.ResourseType = 4;
            list.Add(dt3);
            response.Data = list;

            //课程数
            ResourceStatisticsDto dt4 = new ResourceStatisticsDto();
            dt4.Number = 25;
            dt4.ResourseType = 2;
            list.Add(dt4);

            ResourceStatisticsDto dt6 = new ResourceStatisticsDto();
            dt6.Number = 733840;
            dt6.ResourseType = 3;
            list.Add(dt6);

            ResourceStatisticsDto dt5 = new ResourceStatisticsDto();
            dt5.Number = 1898543;
            dt5.ResourseType = 5;
            list.Add(dt5);

            response.Data = list;
            return response;
        }

        public async Task<ResponseContext<List<StudentTopicDetailDto>>> GetStudentTopicDetails()
        {
            string sql = "select DATE_FORMAT(createtime,'%Y-%m-%d') date ,count(*) Number from exam_student_grade_detail where  createtime > date_sub(curdate(), interval 6 day) GROUP BY DATE_FORMAT(createtime,'%Y-%m-%d') order by date;";

            List<StudentTopicDetailDto> list = await DbContext.FreeSql.Ado.QueryAsync<StudentTopicDetailDto>(sql);

            List<StudentTopicDetailDto> list2 = await DbContext.JNFreeSql.Ado.QueryAsync<StudentTopicDetailDto>(sql);

            ResponseContext<List<StudentTopicDetailDto>> response = new ResponseContext<List<StudentTopicDetailDto>>();

            List<StudentTopicDetailDto> list3 = new List<StudentTopicDetailDto>();
            foreach (var item in list)
            {
                var i = list2.Find(x => x.Date == item.Date);
                StudentTopicDetailDto k = new StudentTopicDetailDto();
                if (i != null)
                {
                    k.Number = (i.Number + item.Number);
                   // k.Number = (i.Number + item.Number) * 10;
                    k.Date = item.Date;
                    list3.Add(k);
                }
            }

            response.Data = list3;
            return response;
            // DbContext.FreeSql.Select<YssxUserLoginLog>().AsTable((a, b) => sql).ToList();
        }

        public async Task<ResponseContext<List<TaskStatisticsDto>>> GetTaskStatistics()
        {
            string sql = "select DATE_FORMAT(createtime,'%Y-%m-%d') date ,count(*) Number from exam_paper where  createtime > date_sub(curdate(), interval 6 day) GROUP BY DATE_FORMAT(createtime,'%Y-%m-%d') order by date;";

            List<TaskStatisticsDto> list = await DbContext.FreeSql.Ado.QueryAsync<TaskStatisticsDto>(sql);

            List<TaskStatisticsDto> list2 = await DbContext.JNFreeSql.Ado.QueryAsync<TaskStatisticsDto>(sql);

            ResponseContext<List<TaskStatisticsDto>> response = new ResponseContext<List<TaskStatisticsDto>>();

            List<TaskStatisticsDto> list3 = new List<TaskStatisticsDto>();
            list3.AddRange(list);
            list3.AddRange(list2);

            var groupData = list3.GroupBy(x => new { x.Date }).Select(y => new
            {
                Date = y.Key.Date,
                Number = y.Sum(x => x.Number),
            });

            List<TaskStatisticsDto> list4 = groupData.Select(x => new TaskStatisticsDto
            {
                Date = x.Date,
               // Number = x.Number * 5
                Number = x.Number
            }).ToList();
            //foreach (var item in list)
            //{
            //    var i = list2.Find(x => x.Date == item.Date);
            //    TaskStatisticsDto k = new TaskStatisticsDto();
            //    if (i != null)
            //    {

            //        k.Number = (i.Number + item.Number) * 10;
            //        k.Date = item.Date;
            //        list3.Add(k);
            //    }
            //    else
            //    {
            //        k.Number = (i.Number + item.Number) * 10;
            //        k.Date = item.Date;
            //        list3.Add(k);
            //    }
            //}

            response.Data = list4;
            return response;
        }

        public async Task<ResponseContext<List<SchoolUseStatis>>> GetSchoolUseDataOrderBy()
        {
            string sql = @"select ul.name 'SchoolName',ul.logintime 'Date',count(ul.logintime) 'Number'  from 
                        (SELECT yul.userid, DATE_FORMAT(yul.logintime, '%Y-%m') logintime, ysl.name from yssx_userloginlog yul, yssx_user yur, yssx_school ysl where yul.userid = yur.id and yur.TenantId = ysl.id and ysl.IsDelete = 0 and ysl.IsDisplay = 0) ul
                                    where ul.logintime >= '2020-02'
                        GROUP BY ul.logintime,ul.`name`
                        order by ul.logintime";

            string sql2 = @"select ul.name 'SchoolName',ul.logintime 'Date',count(ul.logintime) 'Number'  from 
                        (SELECT yul.userid, DATE_FORMAT(yul.logintime, '%Y-%m') logintime, ysl.name from yssx_userloginlog yul, yssx_user yur, yssx_school ysl where yul.userid = yur.id and yur.TenantId = ysl.id and ysl.IsDelete = 0) ul
                                    where ul.logintime >= '2020-02'
                        GROUP BY ul.logintime,ul.`name`
                        order by ul.logintime";

            List<SchoolUseStatis> list = await DbContext.FreeSql.Ado.QueryAsync<SchoolUseStatis>(sql);
            List<SchoolUseStatis> list2 = await DbContext.JNFreeSql.Ado.QueryAsync<SchoolUseStatis>(sql2);

            ResponseContext<List<SchoolUseStatis>> response = new ResponseContext<List<SchoolUseStatis>>();

            List<SchoolUseStatis> list3 = new List<SchoolUseStatis>();
            list3.AddRange(list);
            list3.AddRange(list2);

            var groupData = list3.GroupBy(x => new { x.SchoolName }).Select(y => new
            {
                SchoolName = y.Key.SchoolName,
                Number = y.Sum(x => x.Number),
            });

            //var orderList=groupData.OrderBy(x => x.Number);
            //var test = orderList.Take(10);

            List<SchoolUseStatis> list4 = groupData.OrderByDescending(x => x.Number).Take(10).Select(x => new SchoolUseStatis
            {
                SchoolName = x.SchoolName,
                Number = x.Number
                // Number = x.Number * 10
            }).ToList();
            response.Data = list4;
            return response;
        }

        public async Task<ResponseContext<List<UserStatisticsDto>>> GetSchoolUseStatistics()
        {

            string sql = $"select ul.name 'SchoolName',ul.logintime 'Date',count(ul.name) 'Number'  from (SELECT yul.userid, DATE_FORMAT(yul.logintime, '%Y-%m-%d') logintime, ysl.name from yssx_userloginlog yul, yssx_user yur,yssx_school ysl where yul.userid = yur.id and yur.TenantId = ysl.id and ysl.IsDelete = 0 and ysl.IsDisplay = 0) ul where ul.logintime > date_sub(curdate(), interval 9 day) GROUP BY ul.logintime,ul.`name`";

            string sql2 = @"select ul.name 'SchoolName',ul.logintime 'Date',count(ul.name) 'Number'  from (SELECT yul.userid, DATE_FORMAT(yul.logintime, '%Y-%m-%d') logintime, ysl.name from yssx_userloginlog yul, yssx_user yur,yssx_school ysl where yul.userid = yur.id and yur.TenantId = ysl.id and ysl.IsDelete = 0) ul where ul.logintime > date_sub(curdate(), interval 9 day) GROUP BY ul.logintime,ul.`name`";

            List<UserStatisticsDto> list = await DbContext.FreeSql.Ado.QueryAsync<UserStatisticsDto>(sql);
            List<UserStatisticsDto> list2 = await DbContext.JNFreeSql.Ado.QueryAsync<UserStatisticsDto>(sql2);

            ResponseContext<List<UserStatisticsDto>> response = new ResponseContext<List<UserStatisticsDto>>();

            List<UserStatisticsDto> list3 = new List<UserStatisticsDto>();

            list3.AddRange(list);
            list3.AddRange(list2);

            var groupData = list3.GroupBy(x => new { x.Date }).Select(y => new
            {
                Date = y.Key.Date,
                Number = y.Count()
            });

            List<UserStatisticsDto> list4 = groupData.Select(x => new UserStatisticsDto
            {
                Date = x.Date,
                Number = x.Number
                //Number = x.Number * 2
            }).ToList();
            response.Data = list4;
            return response;
        }

        public async Task<ResponseContext<List<UserStatisticsDto>>> GetSchoolUseMonthStat()
        {
            string sql = @"select ul.name 'SchoolName',ul.logintime 'Date',count(ul.name) 'Number'  from 
                        (SELECT yul.userid, DATE_FORMAT(yul.logintime, '%Y-%m') logintime, ysl.name from yssx_userloginlog yul, yssx_user yur, trainingcloud.yssx_school ysl where yul.userid = yur.id and yur.TenantId = ysl.id and ysl.IsDelete = 0 and ysl.IsDisplay = 0) ul
                                    where ul.logintime >= '2020-02'
                        GROUP BY ul.logintime,ul.`name`
                        order by ul.logintime";

            string sql2 = @"select ul.name 'SchoolName',ul.logintime 'Date',count(ul.name) 'Number'  from 
                        (SELECT yul.userid, DATE_FORMAT(yul.logintime, '%Y-%m') logintime, ysl.name from yssx_userloginlog yul, yssx_user yur, yssx_school ysl where yul.userid = yur.id and yur.TenantId = ysl.id and ysl.IsDelete = 0) ul
                                    where ul.logintime >= '2020-02'
                        GROUP BY ul.logintime,ul.`name`
                        order by ul.logintime";

            List<UserStatisticsDto> list = await DbContext.FreeSql.Ado.QueryAsync<UserStatisticsDto>(sql);
            List<UserStatisticsDto> list2 = await DbContext.JNFreeSql.Ado.QueryAsync<UserStatisticsDto>(sql2);

            ResponseContext<List<UserStatisticsDto>> response = new ResponseContext<List<UserStatisticsDto>>();

            List<UserStatisticsDto> list3 = new List<UserStatisticsDto>();
            list3.AddRange(list);
            list3.AddRange(list2);

            var groupData = list3.GroupBy(x => new { x.Date }).Select(y => new
            {
                Date = y.Key.Date,
                Number = y.Count(),
            });

            List<UserStatisticsDto> list4 = groupData.Select(x => new UserStatisticsDto
            {
                Date = x.Date,
                Number = x.Number
                //Number = x.Number * 2
            }).ToList();
            response.Data = list4;
            return response;
        }

        public async Task<ResponseContext<List<UserStatisticsDto>>> GetUserStatistics()
        {
            // string sql = "select DATE_FORMAT(logintime, '%Y-%u') Date ,count(LoginTime) Number  from yssx_userloginlog GROUP BY DATE_FORMAT(logintime, '%Y-%u') order by date desc";

            string sql = "select DATE_FORMAT(logintime,'%Y-%m-%d') date ,count(LoginTime) Number from yssx_userloginlog where  logintime > date_sub(curdate(), interval 6 day) GROUP BY DATE_FORMAT(logintime,'%Y-%m-%d') order by date;";

            List<UserStatisticsDto> list = await DbContext.FreeSql.Ado.QueryAsync<UserStatisticsDto>(sql);
            List<UserStatisticsDto> list2 = await DbContext.JNFreeSql.Ado.QueryAsync<UserStatisticsDto>(sql);

            ResponseContext<List<UserStatisticsDto>> response = new ResponseContext<List<UserStatisticsDto>>();

            List<UserStatisticsDto> list3 = new List<UserStatisticsDto>();
            foreach (var item in list)
            {
                var i = list2.Find(x => x.Date == item.Date);
                UserStatisticsDto k = new UserStatisticsDto();
                if (i != null)
                {
                    // k.Number = (i.Number + item.Number) * 10;
                    k.Number = (i.Number + item.Number);
                    k.Date = item.Date;
                    list3.Add(k);
                }
            }

            response.Data = list3;
            return response;
        }

        public async Task<ResponseContext<List<UserStatisticsDto>>> GetUserByMonthStatistics()
        {
            string sql = "select DATE_FORMAT(logintime,'%Y-%m') date ,count(LoginTime) Number from yssx_userloginlog where  logintime > date_sub(curdate(), interval 8 MONTH) GROUP BY DATE_FORMAT(logintime,'%Y-%m') order by date";

            List<UserStatisticsDto> list = await DbContext.FreeSql.Ado.QueryAsync<UserStatisticsDto>(sql);
            List<UserStatisticsDto> list2 = await DbContext.JNFreeSql.Ado.QueryAsync<UserStatisticsDto>(sql);

            ResponseContext<List<UserStatisticsDto>> response = new ResponseContext<List<UserStatisticsDto>>();

            List<UserStatisticsDto> list3 = new List<UserStatisticsDto>();
            foreach (var item in list)
            {
                var i = list2.Find(x => x.Date == item.Date);
                UserStatisticsDto k = new UserStatisticsDto();
                if (i != null)
                {
                    //k.Number = (i.Number + item.Number) * 10;
                    k.Number = (i.Number + item.Number);
                    k.Date = item.Date;
                    list3.Add(k);
                }
            }

            response.Data = list3;
            return response;
        }

        public async Task<ResponseContext<List<SchoolStatisticsDto>>> GetSchoolStatisticsList(string date)
        {
            string sql = $"select * from stat_countinfo a where a.Date='{date}'";

            List<SchoolStatisticsListDto> list = await DbContext.FreeSql.Ado.QueryAsync<SchoolStatisticsListDto>(sql);

            //
            List<SchoolStatisticsListDto> l1 = list.Where(x => x.Teacher == 0 && x.Student == 0 && x.Class == 0).ToList();
            List<SchoolStatisticsListDto> l2 = list.Where(x => x.Teacher >= 1 && x.Student == 0 && x.Class == 0).ToList();
            List<SchoolStatisticsListDto> l3 = list.Where(x => x.Teacher >= 1 && x.Student == 0 && x.Class == 1).ToList();
            List<SchoolStatisticsListDto> l4 = list.Where(x => x.Teacher >= 1 && x.Student < 40 && x.Class == 1).ToList();
            List<SchoolStatisticsListDto> l5 = list.Where(x => x.Teacher >= 1 && x.Student < 40 && x.Class > 1).ToList();
            List<SchoolStatisticsListDto> l6 = list.Where(x => x.Teacher >= 1 && (x.Student >= 40 && x.Student <= 200) && x.Class >= 1).ToList();
            List<SchoolStatisticsListDto> l7 = list.Where(x => x.Teacher >= 1 && x.Student > 200 && x.Class > 1).ToList();
            List<SchoolStatisticsListDto> l8 = list.Where(x => x.Teacher >= 1 && x.Student >= 500 && x.Class > 1).ToList();
            List<SchoolStatisticsListDto> l9 = list.Where(x => x.Teacher >= 1 && x.Student >= 1000 && x.Class > 1).ToList();

            List<SchoolStatisticsDto> schoolStatistics = new List<SchoolStatisticsDto>();

            SchoolStatisticsDto ss1 = new SchoolStatisticsDto { StudentCount = l1.Count };



            return new ResponseContext<List<SchoolStatisticsDto>> { };

        }


    }
}



