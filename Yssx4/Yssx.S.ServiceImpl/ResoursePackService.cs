﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto.ResourcePackge;
using Yssx.S.IServices;
using Yssx.S.Poco;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class ResoursePackService : IResoursePackDetailsService
    {
        public ResoursePackService()
        {

        }

        public async Task<ResponseContext<bool>> AddResourceDetails(AddResourceDetailsDto dto)
        {
            List<YssxResourcePeckDetails> peckDetails = new List<YssxResourcePeckDetails>();
            foreach (var item in dto.CaseList)
            {
                YssxResourcePeckDetails details = new YssxResourcePeckDetails
                {
                    Id = IdWorker.NextId(),
                    CaseId = item.CaseId,
                    CaseName = item.CaseName,
                    ModuleName = dto.ModuleName,
                    ModuleType = dto.ModuleType,
                    ResourceId = dto.ResourceId,
                    Sort = dto.Sort,
                };
                peckDetails.Add(details);
            }
            await DbContext.FreeSql.GetRepository<YssxResourcePeckDetails>().InsertAsync(peckDetails);

            return new ResponseContext<bool> { };
        }

        public async Task<ResponseContext<bool>> AddResoursePeck(ResoursePeckDto dto)
        {
            bool isExist = await DbContext.FreeSql.GetRepository<YssxResoursePeck>().Where(x => x.PackgeName == dto.PackgeName.Trim()).AnyAsync();
            if (isExist) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名字已经存在" };
            await DbContext.FreeSql.GetRepository<YssxResoursePeck>().InsertAsync(new YssxResoursePeck { Id = IdWorker.NextId(), CreateTime = DateTime.Now, PackgeName = dto.PackgeName, Sort = dto.Sort, Education = dto.Education });
            return new ResponseContext<bool> { Data = true };
        }

        public async Task<ResponseContext<bool>> DeleteResourceDetails(long id)
        {
            int count = await DbContext.FreeSql.GetRepository<YssxResourcePeckDetails>().DeleteAsync(x => x.Id == id);
            return new ResponseContext<bool> { Data = true };
        }

        public async Task<ResponseContext<bool>> DeleteResoursePeck(long id)
        {
            int count = await DbContext.FreeSql.GetRepository<YssxResoursePeck>().UpdateDiy.Set(x => x.IsDelete, 1).Where(x => x.Id == id).ExecuteAffrowsAsync();
            return new ResponseContext<bool> { Data = true };
        }

        public async Task<ResponseContext<List<ResourcePackDetailDto>>> GetResourcePackDetailsList(long id)
        {
            List<ResourcePackDetailDto> list = await DbContext.FreeSql.GetRepository<YssxResourcePeckDetails>().Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.ResourceId == id).ToListAsync<ResourcePackDetailDto>();
            return new ResponseContext<List<ResourcePackDetailDto>> { Data = list };
        }

        public async Task<ResponseContext<List<ResoursePeckDto>>> GetResoursePeckList(int eType)
        {
            List<ResoursePeckDto> list = await DbContext.FreeSql.GetRepository<YssxResoursePeck>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).WhereIf(eType > -1, x => x.Education == eType).ToListAsync<ResoursePeckDto>();
            return new ResponseContext<List<ResoursePeckDto>> { Data = list };
        }
    }

    public class BindingResourseToSchool : IBindingResourseToSchool
    {
        public BindingResourseToSchool()
        {

        }

        //new ModuleDto{Id=1,Name="岗位实训"},
        //new ModuleDto{Id=2,Name="课程实训"},
        //new ModuleDto { Id = 3, Name = "财务大数据" },
        //new ModuleDto { Id = 4, Name = "业税财实训" },
        //new ModuleDto { Id = 5, Name = "管理会计" },
        //new ModuleDto { Id = 6, Name = "云实习" },
        //new ModuleDto { Id = 7, Name = "同步课程实习" },
        //new ModuleDto { Id = 8, Name = "业财税实习" },
        //new ModuleDto { Id = 9, Name = "经典案例" },

        public async Task<ResponseContext<bool>> BindingResourse(BindingResourseDto dto)
        {
            // YssxCourseOrder


            YssxResourcePeckDetails peck = await DbContext.FreeSql.GetRepository<YssxResourcePeckDetails>().Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.ResourceId == dto.ResourceId).FirstAsync();
            int moduleType = peck.ModuleType;

            YssxSchoolResource schoolCase = new YssxSchoolResource();
            schoolCase.Id = IdWorker.NextId();
            schoolCase.SchoolId = dto.SchoolId;
            schoolCase.ResourceId = dto.ResourceId;
            schoolCase.ResourceName = dto.ResourceName;
            schoolCase.Sort = dto.Sort;
            schoolCase.Year = dto.Year;
            schoolCase.ModuleId = moduleType;
            if (moduleType == 2)
            {
                List<YssxResourcePeckDetails> details = await DbContext.FreeSql.GetRepository<YssxResourcePeckDetails>().Where(x => x.ResourceId == dto.ResourceId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                List<YssxCourseOrder> courseOrders = details.Select(x => new YssxCourseOrder
                {
                    Id = IdWorker.NextId(),
                    CourseId = x.CaseId,
                    TenantId = dto.SchoolId,
                    Year = dto.Year
                }).ToList();

                await DbContext.FreeSql.GetRepository<YssxCourseOrder>().InsertAsync(courseOrders);
                await DbContext.FreeSql.GetRepository<YssxSchoolResource>().InsertAsync(schoolCase);

            }
            else if (moduleType == 1 || moduleType == 3 || moduleType == 4)
            {

                await DbContext.FreeSql.GetRepository<YssxSchoolResource>().InsertAsync(schoolCase);

                var schoolCaseList = await DbContext.FreeSql.GetRepository<YssxResourcePeckDetails>().Select.From<YssxCase, YssxTopic>((a, b, c) => a
                               .LeftJoin(aa => aa.CaseId == b.Id).LeftJoin(aa => b.Id == c.CaseId && c.ParentId == 0 && c.IsDelete == CommonConstants.IsNotDelete))
                               .Where((a, b, c) => a.ResourceId == dto.ResourceId && a.IsDelete == CommonConstants.IsNotDelete)
                               .GroupBy((a, b, c) => new { a.Id, a.CaseId, a.CaseName, a.Sort, b.CompetitionType, b.TotalTime })
                               .ToListAsync(a =>
                               new
                               {
                                   a.Key.Id,
                                   a.Key.CaseId,
                                   a.Key.CaseName,
                                   a.Key.Sort,
                                   a.Value.Item2.CompetitionType,
                                   a.Value.Item2.TotalTime,
                                   TotalQuestionCount = "Count(c.id)",
                                   TotalScore = a.Sum(a.Value.Item3.Score)
                               });
                List<ExamPaper> papers = schoolCaseList.Select(x => new ExamPaper
                {
                    Id = IdWorker.NextId(),
                    CaseId = x.CaseId,
                    CanShowAnswerBeforeEnd = true,
                    ExamType = ExamType.PracticeTest,
                    ExamSourceType = ExamSourceType.Order,
                    IsRelease = true,
                    Name = x.CaseName,
                    CompetitionType = x.CompetitionType,
                    TenantId = dto.SchoolId,
                    TotalScore = x.TotalScore,
                    Year = dto.Year,
                    Sort = x.Sort,
                    TotalMinutes = x.TotalTime,
                    TotalQuestionCount = Convert.ToInt32(x.TotalQuestionCount),


                }).ToList();

                List<ExamPaper> examPapers = schoolCaseList.Where(x => x.CompetitionType == CompetitionType.TeamCompetition).Select(x => new ExamPaper
                {
                    Id = IdWorker.NextId(),
                    CaseId = x.Id,
                    CanShowAnswerBeforeEnd = false,
                    ExamType = ExamType.EmulationTest,
                    ExamSourceType = ExamSourceType.Order,
                    IsRelease = true,
                    Name = x.CaseName,
                    CompetitionType = x.CompetitionType,
                    TenantId = dto.SchoolId,
                    TotalScore = x.TotalScore,
                    Year = dto.Year,
                    Sort = x.Sort,
                    TotalMinutes = x.TotalTime,
                    TotalQuestionCount = Convert.ToInt32(x.TotalQuestionCount),
                }).ToList();

                papers.AddRange(examPapers);

                await DbContext.FreeSql.GetRepository<ExamPaper>().InsertAsync(papers);
            }

            return new ResponseContext<bool> { Data = true };
        }

        public async Task<ResponseContext<List<BindingResourseDto>>> GetSchoolResourseList(long schoolId)
        {
            //var sql = select.OrderBy((a, b) => a.Sort).OrderByDescending((a, b) => a.CreateTime).Page(model.PageIndex, model.PageSize).ToSql("a.*,b.Name as ExamPaperName");
            List<BindingResourseDto> list = new List<BindingResourseDto>();
            list = await DbContext.FreeSql.GetRepository<YssxSchoolResource>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).WhereIf(schoolId > 0, x => x.SchoolId == schoolId).ToListAsync<BindingResourseDto>();
            //if (schoolId == 0)
            //{
            //    select.OrderByDescending(x => x.CreateTime).Page(0, 10);
            //}
            //var sql = select.ToSql();
            //list = await DbContext.FreeSql.Ado.QueryAsync<BindingResourseDto>(sql);

            return new ResponseContext<List<BindingResourseDto>> { Data = list };
        }
    }
}
