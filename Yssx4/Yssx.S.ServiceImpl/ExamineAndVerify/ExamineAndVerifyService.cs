﻿using Microsoft.Extensions.DependencyModel.Resolution;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Repository.Extensions;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.IServices;
using Yssx.S.IServices.ExamineAndVerify;
using Yssx.S.IServices.Means;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Mall;
using Yssx.S.Pocos.SceneTraining;

namespace Yssx.S.ServiceImpl.ExamineAndVerify
{
    /// <summary>
    /// 审核和上下架
    /// </summary>
    public class ExamineAndVerifyService : IExamineAndVerifyService
    {
        /// <summary>
        /// 审核
        /// </summary>
        /// <param name="dto">审核dto</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ExamineAndVerify(ExamineAndVerifyDto dto, long oId)
        {
            return await Task.Run(() =>
            {
                var state = false;
                var content = string.Empty;
                var Userinfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == oId).First();
                if (Userinfo != null) content = string.IsNullOrEmpty(Userinfo.RealName) ? string.IsNullOrEmpty(Userinfo.NikeName) ? Userinfo.Id.ToString() : Userinfo.NikeName : Userinfo.RealName;
                switch (dto.Type)
                {
                    case 1:
                        List<YssxCourse> list = DbContext.FreeSql.Select<YssxCourse>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                        state = DbContext.FreeSql.Update<YssxCourse>().SetSource(list).Set(x => x.AuditStatus,dto.Examinestate).Set(x=>x.AuditStatusExplain,dto.Auditstatusexplain).Set(x=>x.Uppershelf,dto.Examinestate ).ExecuteAffrows() > 0;
                        break;
                    case 2:
                        List<YssxCourceFiles> listc = DbContext.FreeSql.Select<YssxCourceFiles>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                        state = DbContext.FreeSql.Update<YssxCourceFiles>().SetSource(listc).Set(x => x.AuditStatus, dto.Examinestate).Set(x => x.AuditStatusExplain, dto.Auditstatusexplain).Set(x => x.Uppershelf, dto.Examinestate).ExecuteAffrows() > 0;
                        break;
                    case 3:// 案例状态 CaseStatus 0待上架 1上架待审 2已上架 3已拒绝
                        List<YssxUploadCase> listu = DbContext.FreeSql.Select<YssxUploadCase>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                        if (dto.Examinestate == 2)//拒绝
                        {
                            //var Rejected = (int)CaseStatus.Rejected;                                                                                                                               // dto.Examinestate
                            state = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(listu).Set(x => x.AuditStatus, dto.Examinestate).Set(x => x.AuditStatusExplain, dto.Auditstatusexplain).Set(x => x.Uppershelf,3).Set(x => x.CaseStatus, 3).Set(x => x.RejectionTime, DateTime.Now).Set(x => x.Remarks, dto.Auditstatusexplain).ExecuteAffrows() > 0;//.Set(x => x.ValuePoint, dto.ValuePoint)
                            //List<YssxUploadCase> cases = DbContext.FreeSql.Select<YssxUploadCase>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                            foreach (var item in listu)
                            {
                                DbContext.FreeSql.GetRepository<OperationLog>().Insert(new OperationLog { Id = IdWorker.NextId(), ModuleId = item.Id, Module = 5, Content = Userinfo.UserType == 6 ? "平台审核拒绝，拒绝理由：" + dto.Auditstatusexplain : content + " 审核拒绝，拒绝理由：" + dto.Auditstatusexplain, CreateBy = oId });
                                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{item.Id}", new UploadCaseResponse(), 1, true);//预览
                                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{item.Id}", new NoUserUploadCaseResponse(), 1, true);//无用户登陆预览
                            }

                        }
                        if (dto.Examinestate == 1)//成功
                        {
                            //上架前检查是否设置价格
                            foreach (var p in dto.IdList)
                            {
                                var oldUserinfo = DbContext.FreeSql.Select<YssxMallProductItem>().Where(m => m.TargetId == p && m.IsDelete == CommonConstants.IsNotDelete).First();
                                if (oldUserinfo==null)
                                {
                                    DbContext.FreeSql.Update<YssxUploadCase>().SetSource(listu).Set(x => x.CaseGrade, dto.CaseGrade).ExecuteAffrows();
                                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未设置价格，请先设置产品价格！", Data = state };
                                }
                            }
                            //设置已上架
                            //var OnShelves = (int)CaseStatus.OnShelves;
                            state = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(listu).Set(x => x.AuditStatus, dto.Examinestate).Set(x => x.AuditStatusExplain, dto.Auditstatusexplain).Set(x => x.Uppershelf,2).Set(x => x.CaseStatus, 2).Set(x => x.ShelfTime, DateTime.Now).Set(x => x.Remarks, dto.Auditstatusexplain).Set(x => x.CaseGrade, dto.CaseGrade).ExecuteAffrows() > 0;//.Set(x => x.ValuePoint, dto.ValuePoint)
                            if (state == true) { 
                                //var cases = DbContext.FreeSql.Select<YssxUploadCase>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                                foreach (var item in listu)
                                {
                                    DbContext.FreeSql.GetRepository<OperationLog>().Insert(new OperationLog { Id = IdWorker.NextId(), ModuleId = item.Id, Module = 5, Content = Userinfo.UserType == 6 ? "平台 审核通过,已上架" : content + " 审核通过,已上架", CreateBy = oId });
                                    //同步标签库
                                    if (!string.IsNullOrEmpty(item.CaseLabel))
                                    {
                                        item.CaseLabel = item.CaseLabel.Replace("{\"list\":", ""); 
                                        item.CaseLabel = item.CaseLabel.Remove(item.CaseLabel.Length - 1, 1);
                                        var casetag = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CaseTagDto>>(item.CaseLabel);
                                        foreach (var items in casetag)
                                        {
                                            var oldUserinfo = DbContext.FreeSql.Select<YssxTag>().Where(m => m.Name == items.Name && m.IsDelete == CommonConstants.IsNotDelete).First();
                                            if (oldUserinfo == null) { 
                                                var yssxTag = new YssxTag
                                                {
                                                    Id = IdWorker.NextId(),
                                                    Name = items.Name,
                                                    TagSourceType = TagSourceType.Case,
                                                    Status = Status.Enable,
                                                    CaseId = item.Id,
                                                    CreateBy = item.CreateBy,
                                                    UpdateBy = item.UpdateBy
                                                };
                                                DbContext.FreeSql.GetRepository<YssxTag>().Insert(yssxTag);
                                            }
                                        }
                                    }
                                    FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{item.Id}", new UploadCaseResponse(), 1, true);//预览
                                    FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{item.Id}", new NoUserUploadCaseResponse(), 1, true);//无用户登陆预览
                                }
                            }
                        }
                        break;
                    case 4:
                        List<YssxTopic> listt = DbContext.FreeSql.Select<YssxTopic>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                        state = DbContext.FreeSql.Update<YssxTopic>().SetSource(listt).Set(x => x.AuditStatus, dto.Examinestate).Set(x => x.AuditStatusExplain, dto.Auditstatusexplain).Set(x => x.Uppershelf, dto.Examinestate).ExecuteAffrows() > 0;
                        break;
                    case 5:
                        var lists = DbContext.FreeSql.Select<YssxSceneTraining>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                        state = DbContext.FreeSql.Update<YssxSceneTraining>().SetSource(lists).Set(x => x.AuditStatus, dto.Examinestate).Set(x => x.AuditStatusExplain, dto.Auditstatusexplain).Set(x => x.Uppershelf, dto.Examinestate).ExecuteAffrows() > 0;
                        break;
                    default:
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未传类型!", Data = state };
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "操作成功!" : "操作失败!", Data = state };
            });
        }

        /// <summary>
        /// 上下架
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="type">类型（1，课程，2，课件，3，案例，4，题目，5，场景实训）</param>
        /// <param name="status">2，删除1，上架，0，下架(违规处理)</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpperAndLowerFrames(UpperAndLowerFramesDto dto, long oId)
        {
            return await Task.Run(() =>
            {
                var state = false;
                var content = string.Empty;
                var Userinfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == oId).First();
                if (Userinfo != null) content = string.IsNullOrEmpty(Userinfo.RealName) ? string.IsNullOrEmpty(Userinfo.NikeName) ? Userinfo.Id.ToString() : Userinfo.NikeName : Userinfo.RealName;
                switch (dto.Type)
                {
                    case 1:
                        List<YssxCourse> listc = DbContext.FreeSql.Select<YssxCourse>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                        state = DbContext.FreeSql.Update<YssxCourse>().SetSource(listc).Set(x => x.AuditStatus, 0).Set(x=>x.Uppershelf,dto.Status).Set(x=>x.UpdateTime,DateTime.Now).Set(x=>x.UppershelfExplain,dto.UppershelfExplain ).ExecuteAffrows() > 0;
                        break;
                    case 2:
                        List<YssxCourceFiles> listco = DbContext.FreeSql.Select<YssxCourceFiles>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                    if (dto.Status == 2)
                    {
                          state = DbContext.FreeSql.Update<YssxCourceFiles>().SetSource(listco).Set(x=> x.UpdateTime,DateTime.Now).Set(x=>x.IsDelete,CommonConstants.IsDelete).ExecuteAffrows()>0;
                    }
                    else
                        state = DbContext.FreeSql.Update<YssxCourceFiles>().SetSource(listco).Set(x => x.AuditStatus, 0).Set(x => x.Uppershelf, dto.Status).Set(x => x.UpdateTime, DateTime.Now).Set(x => x.UppershelfExplain, dto.UppershelfExplain).ExecuteAffrows() > 0;
                        break;
                    case 3:
                        List<YssxUploadCase> listu = DbContext.FreeSql.Select<YssxUploadCase>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                        if (dto.Status == 2)
                            state = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(listu).Set(x => x.IsDelete, CommonConstants.IsDelete).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows() > 0;
                        if (dto.Status == 0)
                        {
                            //var StayOnTheShelf = (int)CaseStatus.StayOnTheShelf;
                            state = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(listu).Set(x => x.Uppershelf, 0).Set(x => x.CaseStatus, 0).Set(x => x.UpdateTime, DateTime.Now).Set(x => x.UppershelfExplain, dto.UppershelfExplain).Set(x => x.Remarks, dto.UppershelfExplain).ExecuteAffrows() > 0;

                            foreach (var item in listu)
                            {
                                DbContext.FreeSql.GetRepository<OperationLog>().Insert(new OperationLog { Id = IdWorker.NextId(), ModuleId = item.Id, Module = 5, Content = Userinfo.UserType == 6 ? "平台 下架" : content + " 下架", CreateBy = oId });
                                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{item.Id}", new UploadCaseResponse(), 1, true);//预览
                                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{item.Id}", new NoUserUploadCaseResponse(), 1, true);//无用户登陆预览
                            }

                        }

                        if (dto.Status == 1)
                        {
                            //var WaitingForTrial = (int)CaseStatus.WaitingForTrial;
                            state = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(listu).Set(x => x.Uppershelf, 1).Set(x => x.CaseStatus, 1).Set(x => x.UpdateTime, DateTime.Now).Set(x => x.UppershelfExplain, dto.UppershelfExplain).Set(x => x.Remarks, dto.UppershelfExplain).ExecuteAffrows() > 0;//.Set(x => x.CaseLabel, dto.CaseLabel)

                            foreach (var item in listu)
                            {
                                DbContext.FreeSql.GetRepository<OperationLog>().Insert(new OperationLog { Id = IdWorker.NextId(), ModuleId = item.Id, Module = 5, Content = Userinfo.UserType == 6 ? "平台 上架" : content + " 上架", CreateBy = oId });
                                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{item.Id}", new UploadCaseResponse(), 1, true);//预览
                                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{item.Id}", new NoUserUploadCaseResponse(), 1, true);//无用户登陆预览
                            }

                        }
                        break;
                    case 4:
                        List<YssxTopicPublic> listp = DbContext.FreeSql.Select<YssxTopicPublic>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                        state = DbContext.FreeSql.Update<YssxTopicPublic>().SetSource(listp).Set(x => x.Status,dto.Status == 1 ? Status.Enable : Status.Disable).Set(x=>x.UpdateTime,DateTime.Now ).ExecuteAffrows() > 0;
                        break;
                    case 5:
                        var lists= DbContext.FreeSql.Select<YssxSceneTraining>().Where(x => dto.IdList.Contains(x.Id)).ToList();
                        if (dto.Status == 2)
                        {
                            state = DbContext.FreeSql.Update<YssxSceneTraining>().SetSource(lists).Set(x => x.UpdateTime, DateTime.Now).Set(x => x.IsDelete, CommonConstants.IsDelete).ExecuteAffrows() > 0;
                        }
                        else
                        {
                            state = DbContext.FreeSql.Update<YssxSceneTraining>().SetSource(lists).Set(x => x.AuditStatus, 0).Set(x => x.Uppershelf, dto.Status).Set(x => x.UpdateTime, DateTime.Now).Set(x => x.UppershelfExplain, dto.UppershelfExplain).ExecuteAffrows() > 0;
                        }
                        break;
                    default:
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未传类型!", Data = state };
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "操作成功!" : "操作失败!", Data = state };
            });
        }
    }
}
