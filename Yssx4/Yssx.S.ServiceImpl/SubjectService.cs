﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class SubjectService : ISubjectService
    {
        public async Task<ResponseContext<bool>> AddSubject(SubjectDto model)
        {
            long subjectCode = 0;
            if (long.TryParse($"{model.Code1}{model.Code2}{model.Code3}{model.Code4}", out subjectCode))
            {
                YssxSubject subject = DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == model.EnterpriseId && x.SubjectType == model.SubjectType && x.SubjectCode == subjectCode).First();
                if (null == subject)
                {
                    subject = new YssxSubject();

                    subject.SubjectCode = subjectCode;
                    subject.Id = IdWorker.NextId();
                    subject.SubjectName = model.SubjectName;
                    if (model.ParentId > 0)
                    {
                        subject.SubjectName = GetSubjectName(model.ParentId, model.SubjectName);
                        model.Direction = DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.Id == model.ParentId).First()?.Direction;
                    }
                    subject.Code1 = model.Code1;
                    subject.Code2 = model.Code2;
                    subject.Code3 = model.Code3;
                    subject.Code4 = model.Code4;
                    subject.SubjectType = model.SubjectType;
                    subject.Direction = model.Direction;
                    subject.EnterpriseId = model.EnterpriseId;
                    subject.ParentId = model.ParentId;
                    subject.AssistCode = PinYinHelper.GetFirstPinyin(model.SubjectName);
                    subject.CreateTime = DateTime.Now;
                    var result = await DbContext.FreeSql.GetRepository<YssxSubject>().InsertAsync(subject);
                    return new ResponseContext<bool> { Data = result != null };
                }
                else
                {
                    return new ResponseContext<bool> { Data = true };
                }

            }
            else
            {
                return new ResponseContext<bool> { Data = false, Code = CommonConstants.ErrorCode, Msg = "科目编码有问题!" };
            }
        }

        public async Task<ResponseContext<string>> GetNextNum(long id)
        {
            string num = "01";
            int n = 0;
            List<YssxSubject> yssxCoreSubjects = await DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.ParentId == id).OrderBy(x => x.SubjectCode).ToListAsync();
            if (yssxCoreSubjects == null || yssxCoreSubjects.Count == 0)
            {
                num = "01";
            }
            else
            {
                YssxSubject yssx = yssxCoreSubjects.LastOrDefault();
                if (!string.IsNullOrEmpty(yssx.Code2) && string.IsNullOrEmpty(yssx.Code3))
                {
                    if (int.TryParse(yssx.Code2, out n) && n > 0)
                    {
                        n += 1;
                        if (n <= 9)
                        {
                            num = $"0{n}";
                        }
                        else
                        {
                            num = $"{n}";
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(yssx.Code3) && string.IsNullOrEmpty(yssx.Code4))
                {
                    if (int.TryParse(yssx.Code3, out n) && n > 0)
                    {
                        n += 1;
                        if (n <= 9)
                        {
                            num = $"0{n}";
                        }
                        else
                        {
                            num = $"{n}";
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(yssx.Code4))
                {
                    if (int.TryParse(yssx.Code4, out n) && n > 0)
                    {
                        n += 1;
                        if (n <= 9)
                        {
                            num = $"0{n}";
                        }
                        else
                        {
                            num = $"{n}";
                        }
                    }
                }

            }

            return new ResponseContext<string> { Data = num };
        }

        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectLastNodeList(long id, int type, string keyword)
        {
            List<YssxSubject> subjects = await DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == id).WhereIf(true, a => a.SubjectType == type).ToListAsync();
            List<YssxSubject> p_subjects = new List<YssxSubject>();
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                long code = 0;
                if (long.TryParse(keyword, out code))
                {

                    p_subjects = subjects.Where(x => x.ParentId == 0 && x.SubjectCode == code).OrderBy(x => x.SubjectCode).ToList();
                }
                else
                {
                    p_subjects = subjects.Where(x => x.ParentId == 0 && (x.SubjectName.Contains(keyword) || x.AssistCode.Contains(keyword))).OrderBy(x => x.SubjectCode).ToList();
                }
            }
            else
            {
                p_subjects = subjects.Where(x => x.ParentId == 0).OrderBy(x => x.SubjectCode).ToList();
            }

            if (p_subjects == null) return null;

            List<YssxSubject> retLis = new List<YssxSubject>();
            foreach (var item in p_subjects)
            {
                retLis.Add(item);
                FindChildNode(item.Id, retLis, subjects);
            }

            if (retLis == null) return null;

            List<SubjectDto> coreSubjects = DataToModel(retLis);

            List<SubjectDto> coreSubjects2 = new List<SubjectDto>();
            coreSubjects.ForEach(x =>
            {
                if (!retLis.Any(o => o.ParentId == x.Id))
                {
                    coreSubjects2.Add(x);
                }
            });

            return new ResponseContext<List<SubjectDto>> { Data = coreSubjects2 };
        }

        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectsList(long id)
        {
            List<YssxSubject> list = await DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == id).ToListAsync();
            return new ResponseContext<List<SubjectDto>> { Data = DataToModel(list) };
        }

        public async Task<ResponseContext<bool>> RemoveSubject(long id)
        {
            bool retVal = await DbContext.FreeSql.GetRepository<YssxSubject>().DeleteAsync(x => x.Id == id) > 0;
            return new ResponseContext<bool> { Data = retVal, Code = retVal ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = retVal ? "OK" : "删除失败！" };
        }

        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectByType(long id, int type)
        {
            List<YssxSubject> subjects = new List<YssxSubject>();
            if (type > 0)
            {
                subjects = await DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == id && x.SubjectType == type).ToListAsync();
            }
            else
            {
                subjects = await DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == id).ToListAsync();
            }

            List<YssxSubject> p_subjects = new List<YssxSubject>();

            p_subjects = subjects.Where(x => x.ParentId == 0).OrderBy(x => x.SubjectCode).ToList();

            if (p_subjects == null) return null;

            List<YssxSubject> retLis = new List<YssxSubject>();
            foreach (var item in p_subjects)
            {
                retLis.Add(item);
                FindChildNode(item.Id, retLis, subjects);
            }

            if (retLis == null) return null;

            List<SubjectDto> coreSubjects = DataToModel(retLis);

            //List<SubjectDto> coreSubjects2 = new List<SubjectDto>();
            //coreSubjects.ForEach(x =>
            //{
            //    if (!retLis.Any(o => o.ParentId == x.Id))
            //    {
            //        coreSubjects2.Add(x);
            //    }
            //});
            coreSubjects.ForEach(x =>
            {
                if (!retLis.Any(o => o.ParentId == x.Id))
                    x.IsLastNode = 1;
            });

            return new ResponseContext<List<SubjectDto>> { Data = coreSubjects };
        }


        public async Task<ResponseContext<bool>> AddValues(SubjectDataDto model)
        {
            ResponseContext<bool> result = new ResponseContext<bool>();

            List<YssxSubject> yssxBeginningData = model.BeginningDatas.Select(x => new YssxSubject
            {
                Id = x.Id,
                BorrowAmount = x.BorrowAmount,
                UpdateTime = DateTime.Now,
                CreditorAmount = x.CreditorAmount
            }).ToList();

            //foreach (var item in yssxBeginningData)
            //{
            //    DbContext.FreeSql.GetRepository<YssxSubject>().UpdateDiy.SetSource(item).UpdateColumns(x => new { x.BorrowAmount, x.CreditorAmount, x.UpdateTime }).ExecuteAffrows();
            //}

            DbContext.FreeSql.GetRepository<YssxSubject>().UpdateDiy.SetSource(yssxBeginningData).UpdateColumns(x => new { x.BorrowAmount, x.CreditorAmount, x.UpdateTime }).ExecuteAffrows();

            result.Code = CommonConstants.SuccessCode;
            result.Data = true;
            return result;
        }

        public async Task<ResponseContext<TrialBalancingDto>> TrialBalancing(long caseId)
        {
            var enterpriseSubjects = await DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.EnterpriseId == caseId).OrderBy(x => x.SubjectCode).ToListAsync();
            var list = enterpriseSubjects.Where(m => m.ParentId == 0).ToList();
            List<YssxSubject> retLis = new List<YssxSubject>();
            foreach (var item in list)
            {
                retLis.Add(item);
                FindChildNode(item.Id, retLis, enterpriseSubjects);
            }
            List<YssxSubject> temps = retLis.ToList();
            decimal c = 0;
            decimal b = 0;
            temps.ForEach(x =>
            {
                if (x.ParentId == 0)
                {
                    c += x.CreditorAmount;
                    b += x.BorrowAmount;
                }
            });
            string msg = b == c ? "数据平衡" : "数据不平衡";
            return new ResponseContext<TrialBalancingDto> { Data = new TrialBalancingDto { BorrowAmountSummer = b, CreditorAmountSummer = c }, Msg = msg };
        }

        #region 私有方法
        private string GetSubjectName(long parentId, string subjectName)
        {
            List<YssxSubject> retList = new List<YssxSubject>();
            FindParentNode(parentId, retList);
            StringBuilder sb = new StringBuilder();
            retList = retList.OrderBy(x => x.SubjectCode).ToList();
            foreach (var i in retList)
            {
                if (string.IsNullOrEmpty(i.SubjectName)) continue;
                if (!i.SubjectName.Contains("-"))
                {
                    sb.Append(i.SubjectName).Append("-");
                }
                else
                {
                    string[] s = i.SubjectName.Split('-');
                    sb.Append(s[s.Length - 1]).Append("-");
                }
            }

            sb.Append(subjectName);

            return sb.ToString();
        }

        /// <summary>
        /// 查找父节点
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="retList"></param>
        private void FindParentNode(long parentId, List<YssxSubject> retList)
        {
            if (parentId == 0) return;
            YssxSubject yssxCoreSubjects = DbContext.FreeSql.GetRepository<YssxSubject>().Where(x => x.Id == parentId).First();
            if (null == yssxCoreSubjects) return;
            retList.Add(yssxCoreSubjects);
            FindParentNode(yssxCoreSubjects.ParentId, retList);
        }

        /// <summary>
        /// 查找子节点
        /// </summary>
        private void FindChildNode(long id, List<YssxSubject> retList, List<YssxSubject> enterpriseSubjects)
        {
            if (id == 0) return;

            //List<YssxCoreSubject> yssxCoreSubjects = DbContext.FreeSql.Select<YssxCoreSubject>().Where(x => x.ParentId == id).ToList();
            List<YssxSubject> yssxCoreSubjects = enterpriseSubjects.Where(x => x.ParentId == id).OrderBy(x => x.SubjectCode).ToList();
            if (null == yssxCoreSubjects) return;
            foreach (var item in yssxCoreSubjects)
            {
                retList.Add(item);
                FindChildNode(item.Id, retList, enterpriseSubjects);
            }
        }

        private List<SubjectDto> DataToModel(List<YssxSubject> sources)
        {
            if (null == sources) return null;
            return sources.Select(x => new SubjectDto
            {
                Id = x.Id,
                AssistCode = x.AssistCode,
                BorrowAmount = x.BorrowAmount,
                Code1 = x.Code1,
                Code2 = x.Code2,
                Code3 = x.Code3,
                Code4 = x.Code4,
                CreditorAmount = x.CreditorAmount,
                Direction = x.Direction,
                EnterpriseId = x.EnterpriseId,
                ParentId = x.ParentId,
                SubjectCode = x.SubjectCode,
                SubjectName = x.SubjectName,
                SubjectType = x.SubjectType
            }).ToList();
        }




        #endregion
    }
}
