﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.Entity;
using Yssx.Framework.AutoMapper;
using Yssx.S.Poco;
using Yssx.S.Pocos.Exam;
using Yssx.Framework.Logger;
using Tas.Common.Utils;
using Yssx.Repository.Extensions;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 用户首页服务
    /// </summary>
    public class UserHomeService : IUserHomeService
    {
        #region 获取用户自主训练
        /// <summary>
        /// 获取用户自主训练
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<YssxUserAutoTrainingViewModel>> GetUserAutoTrainingList(UserTicket user)
        {
            ResponseContext<YssxUserAutoTrainingViewModel> response = new ResponseContext<YssxUserAutoTrainingViewModel>();
            if (user == null)
                return response;

            YssxUserAutoTrainingViewModel data = new YssxUserAutoTrainingViewModel();

            List<YssxUserTrainingViewModel> list = new List<YssxUserTrainingViewModel>();

            //int year = 0;
            //if (user.Type == 1)
            //{
            //    YssxClass yssx = await DbContext.FreeSql.GetRepository<YssxClass>().Select.From<YssxStudent>((a, b) => a.LeftJoin(x => x.Id == b.ClassId)).Where((a, b) => b.UserId == user.Id && b.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            //    if (yssx != null)
            //    {
            //        year = yssx.EntranceDateTime.Year;
            //    }
            //}

            //课程实训
            //var courseSelect = DbContext.FreeSql.GetRepository<YssxCourse>().Where(s => s.IsDelete == CommonConstants.IsNotDelete && s.AuditStatus == 1)
            //    .OrderByDescending(a => a.UpdateTime).Take(3);
            List<YssxCourse> courseSelect =await DbContext.FreeSql.GetRepository<YssxCourse>().Select
                              .From<YssxCourseOrder>((a, b) => a.InnerJoin(aa => aa.Id == b.CourseId))
                              .Where((a, b) => b.TenantId==user.TenantId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete==CommonConstants.IsNotDelete)
                              .Take(3).ToListAsync();
            //学生
            //if (user.Type == (int)UserTypeEnums.Student)
            //    courseSelect = courseSelect.Where(x => x.Education == user.Education);
            //普通用户
            if (user.Type == (int)UserTypeEnums.OrdinaryUser)
                user.TenantId = CommonConstants.DefaultTenantId;   //默认测试大学

            #region 实训
            //实训列表
            var trainData = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetHomeUserTrainingList}{user.TenantId}",
                () => DbContext.FreeSql.Select<ExamPaper>().From<YssxCase, YssxIndustry>((a, b, c) =>
                a.InnerJoin(x => x.CaseId == b.Id)
                 .InnerJoin(x => b.Industry == c.Id))
                 .Where((a, b, c) => a.IsRelease && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete
                    && a.TenantId == user.TenantId && a.TaskId == 0 && a.ExamType == ExamType.PracticeTest)
                 .ToList((a, b, c) => new
                 {
                     ExamId = a.Id,
                     TrainingId = b.Id,
                     TrainingName = b.Name,
                     RollUpType = b.RollUpType,
                     CaseYear = b.CaseYear,
                     SxType = b.SxType,
                     IndustryName = c.Name,
                     UpdateTime = b.UpdateTime
                 }), 720 * 60, true, false);

            #region 会计实训
            //会计实训
            var accountantList = trainData.Where(a => a.SxType == 0).OrderByDescending(a => a.UpdateTime).ToList();
            accountantList.Take(3).ToList().ForEach(x =>
            {
                YssxUserTrainingViewModel model = new YssxUserTrainingViewModel();
                model.TrainingId = x.TrainingId;
                model.TrainingName = x.TrainingName;
                model.RollUpType = x.RollUpType;
                model.CaseYear = x.CaseYear;
                model.SxType = x.SxType;
                model.IndustryName = x.IndustryName;
                model.ExamId = x.ExamId;

                list.Add(model);
            });
            #endregion

            #region 出纳实训
            //出纳实训
            var casherList = trainData.Where(a => a.SxType == 1).OrderByDescending(a => a.UpdateTime).ToList();
            casherList.Take(3).ToList().ForEach(x =>
            {
                YssxUserTrainingViewModel model = new YssxUserTrainingViewModel();
                model.TrainingId = x.TrainingId;
                model.TrainingName = x.TrainingName;
                model.RollUpType = x.RollUpType;
                model.CaseYear = x.CaseYear;
                model.SxType = x.SxType;
                model.IndustryName = x.IndustryName;
                model.ExamId = x.ExamId;

                list.Add(model);
            });
            #endregion

            #region 大数据
            //大数据
            var bigList = trainData.Where(a => a.SxType == 2).OrderByDescending(a => a.UpdateTime).ToList();
            bigList.Take(3).ToList().ForEach(x =>
            {
                YssxUserTrainingViewModel model = new YssxUserTrainingViewModel();
                model.TrainingId = x.TrainingId;
                model.TrainingName = x.TrainingName;
                model.RollUpType = x.RollUpType;
                model.CaseYear = x.CaseYear;
                model.SxType = x.SxType;
                model.IndustryName = x.IndustryName;
                model.ExamId = x.ExamId;

                list.Add(model);
            });
            #endregion

            #region 业财税
            //业财税
            var taxList = trainData.Where(a => a.SxType == 3).OrderByDescending(a => a.UpdateTime).ToList();
            taxList.Take(3).ToList().ForEach(x =>
            {
                YssxUserTrainingViewModel model = new YssxUserTrainingViewModel();
                model.TrainingId = x.TrainingId;
                model.TrainingName = x.TrainingName;
                model.RollUpType = x.RollUpType;
                model.CaseYear = x.CaseYear;
                model.SxType = x.SxType;
                model.IndustryName = x.IndustryName;
                model.ExamId = x.ExamId;

                list.Add(model);
            });
            #endregion

            //试卷Ids
            List<long> examIdList = list.Select(x => x.ExamId).ToList();

            //主作答记录列表
            var gradeList = await DbContext.FreeSql.Select<ExamStudentGrade>().Where(x => examIdList.Contains(x.ExamId) && x.IsDelete == CommonConstants.IsNotDelete
               && x.Status != StudentExamStatus.End && x.UserId == user.Id).ToListAsync();

            if (gradeList.Count > 0)
            {
                list.ForEach(x =>
                {
                    x.GradeId = gradeList.Where(a => a.ExamId == x.ExamId).OrderByDescending(a => a.CreateTime).FirstOrDefault()?.Id;
                });
            }
            #endregion

            #region 业务场景实训
            var sceneData = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetHomeUserSecneTrainingList}{CommonConstants.DefaultTenantId}",
                 () => DbContext.FreeSql.Select<YssxSceneTraining>().From<YssxCase>((a, b) =>
                 a.InnerJoin(x => x.SelectCompanyId == b.Id))
                 .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && a.AuditStatus == 1)
                 .OrderByDescending((a, b) => a.UpdateTime)
                 .Take(3)
                 .ToList((a, b) => new
                 {
                     TrainingId = a.Id,
                     TrainingName = a.Name,
                     RollUpType = b.RollUpType,
                     CaseYear = b.CaseYear,
                     IndustryId = b.Id,
                     IndustryName = b.Name,
                     UpdateTime = a.UpdateTime
                 }), 720 * 60, true, false);

            sceneData.ForEach(x =>
            {
                YssxUserTrainingViewModel model = new YssxUserTrainingViewModel();
                model.TrainingId = x.TrainingId;
                model.TrainingName = x.TrainingName;
                model.RollUpType = x.RollUpType;
                model.CaseYear = x.CaseYear;
                model.SxType = 4;
                model.IndustryId = x.IndustryId;
                model.IndustryName = x.IndustryName;

                list.Add(model);
            });
            #endregion

            #region 课程实训
            var courseData =courseSelect.Select(x => new
                {
                    TrainingId = x.Id,
                    TrainingName = x.CourseName,
                    Education = x.Education,
                    UpdateTime = x.UpdateTime
                }).ToList();

            courseData.ForEach(x =>
            {
                YssxUserTrainingViewModel model = new YssxUserTrainingViewModel();
                if (x.Education == 0)
                    model.IndustryName = "中职";
                if (x.Education == 1)
                    model.IndustryName = "高职";
                if (x.Education == 2)
                    model.IndustryName = "本科";
                model.TrainingId = x.TrainingId;
                model.TrainingName = x.TrainingName;
                model.SxType = 5;

                list.Add(model);
            });
            #endregion

            //实操数(主作答记录列表)
            var operateCount = await DbContext.FreeSql.Select<ExamStudentGrade>().From<ExamPaper>(
                (a, b) =>
                a.InnerJoin(x => x.ExamId == b.Id))
                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete
                && a.Status == StudentExamStatus.End && a.UserId == user.Id && b.TaskId == 0).CountAsync();

            data.OperateCount = (int)operateCount;
            data.TrainingList = list;

            response.Data = data;
            return response;
        }
        #endregion

        #region 获取学生任务列表 Obsolete
        /// <summary>
        /// 获取学生任务列表
        /// 进行中、结束时间离当前时间最近的任务
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        /*public async Task<ResponseContext<List<YssxStudentTaskInfoViewModel>>> GetStudentTaskList(UserTicket user)
        {
            ResponseContext<List<YssxStudentTaskInfoViewModel>> response = new ResponseContext<List<YssxStudentTaskInfoViewModel>>();
            if (user == null)
                return response;

            List<YssxStudentTaskInfoViewModel> list = new List<YssxStudentTaskInfoViewModel>();

            #region 课堂测验、课程任务
            var courseData = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetHomeUserTestTaskList}{user.Id}",
                () => DbContext.FreeSql.Select<ExamPaperCourse>().From<YssxTaskStudent>((a, b) =>
              a.InnerJoin(x => x.TaskId == b.TaskId))
                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && b.UserId == user.Id &&
                    (a.ExamType == CourseExamType.ClassTask || a.ExamType == CourseExamType.ClassTest) && a.BeginTime <= DateTime.Now &&
                    DateTime.Now < a.EndTime && a.Status != ExamStatus.End)
                .ToList((a, b) => new YssxTaskInfoViewModel
                {
                    TaskId = a.TaskId,
                    TaskName = a.Name,
                    CourseId = a.CourseId,
                    TaskType = (int)a.ExamType,
                    TaskStartTime = a.BeginTime,
                    TaskEndTime = a.EndTime,
                    TaskStatus = (int)a.Status,
                    ExamId = a.Id,
                    TotalQuestionCount = a.TotalQuestionCount,
                    UpdateTime = a.UpdateTime,
                }), 10 * 60, true, false);

            //课堂测验
            var courseTest = courseData.Where(a => a.TaskType == (int)CourseExamType.ClassTest).OrderBy(a => a.TaskEndTime).ToList();
            if (courseTest.Count > 0)
            {
                var testModel = courseTest.Take(1).FirstOrDefault();

                YssxStudentTaskInfoViewModel model = await InitializeStudentTask(testModel, 0, user);
                list.Add(model);
            }

            //课程任务
            var courseTask = courseData.Where(a => a.TaskType == (int)CourseExamType.ClassTask).OrderBy(a => a.TaskEndTime).ToList();
            if (courseTask.Count > 0)
            {
                var taskModel = courseTask.Take(1).FirstOrDefault();

                YssxStudentTaskInfoViewModel model = await InitializeStudentTask(taskModel, 1, user);
                list.Add(model);
            }
            #endregion

            #region 岗位实训任务
            var jobData = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetHomeUserJobTrainingTaskList}{user.Id}",
                () => DbContext.FreeSql.Select<YssxJobTrainingTask>().From<YssxJobTrainingTaskClass, YssxStudent>(
                (a, b, c) =>
                a.InnerJoin(x => x.Id == b.JobTrainingTaskId)
                .InnerJoin(x => b.ClassId == c.ClassId))
                .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete
                    && c.UserId == user.Id && a.TrainStartTime <= DateTime.Now && DateTime.Now < a.TrainEndTime && (a.TaskStatus != (int)TrainType.End || a.TaskStatus == null))
                .ToList((a, b, c) => new
                {
                    TaskId = a.Id,
                    TaskName = a.TaskName,
                    TaskStartTime = a.TrainStartTime,
                    TaskEndTime = a.TrainEndTime,
                    TaskStatus = a.TaskStatus,
                    UpdateTime = a.UpdateTime
                }), 10 * 60, true, false);

            if (jobData.Count > 0)
            {
                var jobModel = jobData.OrderBy(x => x.TaskEndTime).Take(1).FirstOrDefault();

                YssxStudentTaskInfoViewModel model = new YssxStudentTaskInfoViewModel();
                model.TaskId = jobModel.TaskId;
                model.TaskName = jobModel.TaskName;
                model.TaskType = 2;
                model.TaskStartTime = jobModel.TaskStartTime;
                model.TaskEndTime = jobModel.TaskEndTime;
                model.TaskStatus = jobModel.TaskStatus;

                //试卷列表
                List<ExamPaper> examPaperList = await DbContext.FreeSql.Select<ExamPaper>().Where(x => jobModel.TaskId == x.TaskId
                       && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                //总题数
                model.TotalQuestionCount = examPaperList.Sum(x => x.TotalQuestionCount);

                //试卷Ids
                List<long> examIds = examPaperList.Select(q => q.Id).ToList();

                //学生作答主信息列表
                List<ExamStudentGrade> studentGradeList = await DbContext.FreeSql.Select<ExamStudentGrade>()
                    .Where(x => x.UserId == user.Id && examIds.Contains(x.ExamId) && x.IsDelete == CommonConstants.IsNotDelete && x.GroupId == 0).ToListAsync();

                //岗位实训任务套题列表
                var jobTrainingList = await DbContext.FreeSql.Select<YssxJobTrainingTaskTopic>().From<YssxCase>(
                    (a, b) => a.InnerJoin(x => x.JobTrainingId == b.Id))
                    .Where((a, b) => jobModel.TaskId == a.JobTrainingTaskId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b) => new
                    {
                        JobTrainingTaskId = a.JobTrainingTaskId,
                        JobTrainingId = b.Id,
                        JobTrainingName = b.Name,
                        RollUpType = b.RollUpType,
                        CaseYear = b.CaseYear,
                    });

                //已作答题数
                int hasAnsweredCount = 0;
                //进行中的作答记录
                List<long> gradeIds = new List<long>();
                //实训套题列表
                List<JobTrainingTaskViewModel> topicList = new List<JobTrainingTaskViewModel>();

                jobTrainingList.ForEach(x =>
                {
                    JobTrainingTaskViewModel jobInfo = new JobTrainingTaskViewModel();
                    jobInfo.JobTrainingId = x.JobTrainingId;
                    jobInfo.JobTrainingName = x.JobTrainingName;
                    jobInfo.RollUpType = x.RollUpType;
                    jobInfo.CaseYear = x.CaseYear;

                    if (examPaperList.Where(b => b.TaskId == x.JobTrainingTaskId && b.CaseId == x.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault() != null)
                    {
                        jobInfo.ExamId = examPaperList.Where(b => b.TaskId == x.JobTrainingTaskId && b.CaseId == x.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault()?.Id;

                        //主作答记录
                        ExamStudentGrade gradeModel = studentGradeList.Where(b => b.ExamId == jobInfo.ExamId).FirstOrDefault();
                        if (gradeModel != null)
                        {
                            jobInfo.GradeId = gradeModel.Id;
                            jobInfo.StudentExamStatus = (int)gradeModel.Status;

                            if (gradeModel.Status == StudentExamStatus.End)
                                hasAnsweredCount = model.TotalQuestionCount - gradeModel.BlankCount + hasAnsweredCount;
                            if (gradeModel.Status == StudentExamStatus.Started)
                                gradeIds.Add(gradeModel.Id);
                        }
                    }

                    topicList.Add(jobInfo);
                });

                //学生作答题数
                if (gradeIds.Count > 0)
                    hasAnsweredCount = (int)(await DbContext.FreeSql.Select<ExamStudentGradeDetail>().Where(a => a.UserId == user.Id && gradeIds.Contains(a.GradeId) &&
                        a.IsDelete == CommonConstants.IsNotDelete && a.QuestionId == a.ParentQuestionId).CountAsync()) + hasAnsweredCount;

                model.HasAnsweredCount = hasAnsweredCount;
                model.TopicList = topicList;

                list.Add(model);
            }
            #endregion

            response.Data = list;

            return response;
        }*/
        #endregion

        #region 获取学生任务列表
        /// <summary>
        /// 获取学生任务列表
        /// 进行中、结束时间离当前时间最近的任务
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxStudentTaskInfoViewModel>>> GetStudentTaskList(UserTicket user)
        {
            ResponseContext<List<YssxStudentTaskInfoViewModel>> response = new ResponseContext<List<YssxStudentTaskInfoViewModel>>();
            if (user == null)
                return response;

            List<YssxStudent> stuList = await DbContext.FreeSql.GetRepository<YssxStudent>().Where(x => x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (stuList == null || stuList.Count == 0)
                return response;

            List<YssxStudentTaskInfoViewModel> list = new List<YssxStudentTaskInfoViewModel>();

            #region 课堂测验
            var testData = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Select.From<YssxTask>((a, b) =>
                 a.InnerJoin(x => x.TaskId == b.GroupId))
                .Where((a, b) => b.TenantId == stuList[0].ClassId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete
                    && b.Status == LessonsTaskStatus.Started && a.Status == ExamStatus.Started && a.ExamType == CourseExamType.ClassTest)
                .GroupBy((a, b) => new { a.Id, a.TaskId, a.Name, a.BeginTime, a.Status, b.OriginalCourseId })
                .OrderByDescending(x => x.Key.BeginTime)
                .Take(1)
                .ToListAsync(a => new YssxTaskInfoViewModel
                {
                    TaskId = a.Key.TaskId,
                    TaskName = a.Key.Name,
                    TaskType = 0,
                    TaskStartTime = a.Key.BeginTime,
                    TaskStatus = (int)a.Key.Status,
                    CourseId = a.Key.OriginalCourseId,
                    ExamId = a.Key.Id,
                });

            YssxTaskInfoViewModel testTemp = testData.FirstOrDefault();
            if (testTemp != null)
            {
                YssxStudentTaskInfoViewModel testModel = await InitialCourseTask(testTemp, user);

                list.Add(testModel);
            }
            #endregion

            #region 课程作业、课程考试 【任务】            
            var courseData = await DbContext.FreeSql.Select<ExamPaperCourse>().From<YssxTaskStudent>((a, b) =>
              a.InnerJoin(x => x.TaskId == b.TaskId))
                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && b.UserId == user.Id &&
                    (a.ExamType == CourseExamType.ClassExam || a.ExamType == CourseExamType.ClassHomeWork) && a.BeginTime <= DateTime.Now &&
                    DateTime.Now < a.EndTime && a.Status != ExamStatus.End)
                .OrderByDescending((a, b) => a.BeginTime)
                .Take(1)
                .ToListAsync((a, b) => new YssxTaskInfoViewModel
                {
                    TaskId = a.TaskId,
                    TaskName = a.Name,
                    TaskType = 1,
                    TaskStartTime = a.BeginTime,
                    TaskEndTime = a.EndTime,
                    TaskStatus = (int)a.Status,
                    CourseId = a.OriginalCourseId,
                    ExamId = a.Id,
                });

            YssxTaskInfoViewModel courseTest = courseData.FirstOrDefault();
            if (courseTest != null)
            {
                YssxStudentTaskInfoViewModel courseModel = await InitialCourseTask(courseTest, user);

                list.Add(courseModel);
            }
            #endregion

            #region 岗位实训任务
            var jobData = await DbContext.FreeSql.Select<YssxJobTrainingTask>().From<YssxJobTrainingTaskClass, YssxStudent>(
                (a, b, c) =>
                a.InnerJoin(x => x.Id == b.JobTrainingTaskId)
                .InnerJoin(x => b.ClassId == c.ClassId))
                .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete
                    && c.UserId == user.Id && a.TrainStartTime <= DateTime.Now && DateTime.Now < a.TrainEndTime && (a.TaskStatus != (int)TrainType.End || a.TaskStatus == null))
                .OrderByDescending((a, b, c) => a.TrainStartTime)
                .Take(1)
                .ToListAsync((a, b, c) => new YssxStudentTaskInfoViewModel
                {
                    TaskId = a.Id,
                    TaskName = a.TaskName,
                    TaskType = 2,
                    TaskStartTime = a.TrainStartTime,
                    TaskEndTime = a.TrainEndTime,
                    TaskStatus = a.TaskStatus,
                    AnswerShowType = a.AnswerShowType,
                });

            YssxStudentTaskInfoViewModel jobModel = jobData.FirstOrDefault();
            if (jobModel != null)
            {
                //试卷列表
                List<ExamPaper> examPaperList = await DbContext.FreeSql.Select<ExamPaper>().Where(x => jobModel.TaskId == x.TaskId
                       && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                //试卷Ids
                List<long> examIds = examPaperList.Select(q => q.Id).ToList();

                //学生作答主信息列表
                List<ExamStudentGrade> studentGradeList = await DbContext.FreeSql.Select<ExamStudentGrade>()
                    .Where(x => x.UserId == user.Id && examIds.Contains(x.ExamId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                //岗位实训任务套题列表
                var jobTrainingList = await DbContext.FreeSql.Select<YssxJobTrainingTaskTopic>().From<YssxCase>(
                    (a, b) => a.InnerJoin(x => x.JobTrainingId == b.Id))
                    .Where((a, b) => jobModel.TaskId == a.JobTrainingTaskId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b) => new
                    {
                        JobTrainingTaskId = a.JobTrainingTaskId,
                        JobTrainingId = b.Id,
                        JobTrainingName = b.Name,
                        RollUpType = b.RollUpType,
                        CaseYear = b.CaseYear,
                    });

                //实训套题列表
                List<JobTrainingTaskViewModel> topicList = new List<JobTrainingTaskViewModel>();

                jobTrainingList.ForEach(x =>
                {
                    JobTrainingTaskViewModel jobInfo = new JobTrainingTaskViewModel();
                    jobInfo.JobTrainingId = x.JobTrainingId;
                    jobInfo.JobTrainingName = x.JobTrainingName;
                    jobInfo.RollUpType = x.RollUpType;
                    jobInfo.CaseYear = x.CaseYear;

                    if (examPaperList.Where(b => b.TaskId == x.JobTrainingTaskId && b.CaseId == x.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault() != null)
                    {
                        jobInfo.ExamId = examPaperList.Where(b => b.TaskId == x.JobTrainingTaskId && b.CaseId == x.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault()?.Id;

                        //主作答记录
                        ExamStudentGrade jobGrade = studentGradeList.Where(b => b.ExamId == jobInfo.ExamId).FirstOrDefault();
                        if (jobGrade != null)
                        {
                            jobInfo.GradeId = jobGrade.Id;
                            jobInfo.StudentExamStatus = (int)jobGrade.Status;
                        }
                    }

                    topicList.Add(jobInfo);
                });

                jobModel.TopicList = topicList;

                list.Add(jobModel);
            }
            #endregion

            response.Data = list;

            return response;
        }
        #endregion

        #region 初始化课程任务
        /// <summary>
        /// 初始化课程任务
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<YssxStudentTaskInfoViewModel> InitialCourseTask(YssxTaskInfoViewModel model, UserTicket user)
        {
            YssxStudentTaskInfoViewModel data = new YssxStudentTaskInfoViewModel();
            data.TaskId = model.TaskId;
            data.TaskName = model.TaskName;
            data.TaskStartTime = model.TaskStartTime;
            data.TaskEndTime = model.TaskEndTime;
            data.TaskType = model.TaskType;
            data.TaskStatus = model.TaskStatus;
            data.CourseId = model.CourseId;
            data.ExamId = model.ExamId;

            //课堂测验 主作答记录
            ExamCourseUserGrade gradeModel = await DbContext.FreeSql.Select<ExamCourseUserGrade>().Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.ExamId ==
                data.ExamId && x.UserId == user.Id).FirstAsync();
            if (gradeModel != null)
            {
                data.GradeId = gradeModel.Id;
                data.StudentExamStatus = (int)gradeModel.Status;
            }

            return data;
        }
        #endregion

        #region 初始化课程任务信息
        /// <summary>
        /// 初始化课程任务信息
        /// </summary>
        /// <param name="model">任务信息</param>
        /// <param name="examType">任务类型 0:课堂测验 1:课程任务</param>
        /// <returns></returns>
        private async Task<YssxStudentTaskInfoViewModel> InitializeStudentTask(YssxTaskInfoViewModel model, int taskType, UserTicket user)
        {
            YssxStudentTaskInfoViewModel data = new YssxStudentTaskInfoViewModel();

            data.TaskId = model.TaskId;
            data.TaskName = model.TaskName;
            data.TaskType = taskType;
            data.TaskStartTime = model.TaskStartTime;
            data.TaskEndTime = model.TaskEndTime;
            data.TaskStatus = model.TaskStatus;
            data.ExamId = model.ExamId;
            data.TotalQuestionCount = model.TotalQuestionCount;
            data.CourseId = model.CourseId;

            //主作答记录
            ExamCourseUserGrade gradeModel = await DbContext.FreeSql.Select<ExamCourseUserGrade>().Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.ExamId ==
                model.ExamId && x.UserId == user.Id).FirstAsync();
            if (gradeModel != null)
            {
                data.GradeId = gradeModel.Id;
                data.StudentExamStatus = (int)gradeModel.Status;

                if (gradeModel.Status == StudentExamStatus.End)
                    data.HasAnsweredCount = model.TotalQuestionCount - gradeModel.BlankCount;
                if (gradeModel.Status == StudentExamStatus.Started)
                    data.HasAnsweredCount = (int)(await DbContext.FreeSql.Select<ExamCourseUserGradeDetail>().Where(x => x.IsDelete == CommonConstants.IsNotDelete
                    && x.GradeId == gradeModel.Id && x.UserId == user.Id && x.QuestionId == x.ParentQuestionId).CountAsync());
            }

            return data;
        }
        #endregion

        #region 获取教师任务列表
        /// <summary>
        /// 获取教师任务列表
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxTeacherTaskInfoViewModel>>> GetTeacherTaskList(UserTicket user)
        {
            ResponseContext<List<YssxTeacherTaskInfoViewModel>> response = new ResponseContext<List<YssxTeacherTaskInfoViewModel>>();
            if (user == null)
                return response;

            List<YssxTeacherTaskInfoViewModel> list = new List<YssxTeacherTaskInfoViewModel>();

            List<YssxTaskInfoViewModel> tempList = new List<YssxTaskInfoViewModel>();

            //课程作业、课程考试 【任务】
            var courseData = await DbContext.FreeSql.Select<YssxTask>().Where(x => x.CreateBy == user.Id && x.IsDelete == CommonConstants.IsNotDelete &&
                     (x.TaskType == TaskType.ClassHomeWork || x.TaskType == TaskType.ClassExam) && x.StartTime <= DateTime.Now &&
                     DateTime.Now < x.CloseTime && x.Status != LessonsTaskStatus.End)
                .OrderByDescending(a => a.StartTime)
                .Take(1)
                .ToListAsync(x => new YssxTaskInfoViewModel
                {
                    TaskId = x.Id,
                    TaskName = x.Name,
                    TaskType = 1,  //课程任务
                    TaskStartTime = x.StartTime,
                    TaskEndTime = x.CloseTime,
                    TaskStatus = (int)x.Status,
                });
            tempList.AddRange(courseData);

            //课堂测验
            var testData = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Select.From<YssxTask>((a, b) =>
                 a.InnerJoin(x => x.TaskId == b.GroupId))
                .Where((a, b) => a.CreateBy == user.Id && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete
                    && b.Status == LessonsTaskStatus.Started && a.Status == ExamStatus.Started)
                .GroupBy((a, b) => new { a.TaskId, a.Name, a.BeginTime, a.Status })
                .OrderByDescending(x => x.Key.BeginTime)
                .Take(1)
                .ToListAsync(a => new YssxTaskInfoViewModel
                {
                    TaskId = a.Key.TaskId,
                    TaskName = a.Key.Name,
                    TaskType = 0,  //课堂测验
                    TaskStartTime = a.Key.BeginTime,
                    TaskStatus = (int)a.Key.Status,
                });
            tempList.AddRange(testData);

            //岗位实训任务
            var jobData = await DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().Where(a => a.CreateBy == user.Id && a.IsDelete == CommonConstants.IsNotDelete &&
                  a.TrainStartTime <= DateTime.Now && DateTime.Now < a.TrainEndTime && (a.TaskStatus != (int)TrainType.End || a.TaskStatus == null))
                  .OrderByDescending(x => x.TrainStartTime)
                  .Take(1)
                  .ToListAsync(x => new YssxTaskInfoViewModel
                  {
                      TaskId = x.Id,
                      TaskName = x.TaskName,
                      TaskType = 2,  //岗位实训任务
                      TaskStartTime = x.TrainStartTime,
                      TaskEndTime = x.TrainEndTime,
                      TaskStatus = (int)x.TaskStatus,
                  });
            tempList.AddRange(jobData);

            if (tempList.Count == 0)
                return response;

            YssxTaskInfoViewModel tempModel = tempList.OrderByDescending(x => x.TaskStartTime).Take(1).FirstOrDefault();

            YssxTeacherTaskInfoViewModel dataModel = new YssxTeacherTaskInfoViewModel();
            dataModel.TaskId = tempModel.TaskId;
            dataModel.TaskName = tempModel.TaskName;
            dataModel.TaskType = tempModel.TaskType;
            dataModel.TaskStartTime = tempModel.TaskStartTime;
            dataModel.TaskEndTime = tempModel.TaskEndTime;
            dataModel.TaskStatus = tempModel.TaskStatus;

            //课程作业、课程考试 【任务】
            if (tempModel.TaskType == 1)
            {
                //学生数量
                dataModel.StudentCount = (int)await DbContext.FreeSql.GetRepository<YssxTaskStudent>().Where(x => x.TaskId == tempModel.TaskId && x.IsDelete ==
                    CommonConstants.IsNotDelete).CountAsync();
                //班级Ids
                List<long> classIds = DbContext.FreeSql.Select<YssxTaskStudent>().Where(x => x.TaskId == tempModel.TaskId && x.IsDelete ==
                       CommonConstants.IsNotDelete).ToList().Select(x => x.ClassId).Distinct().ToList();
                //班级列表
                var classList = await DbContext.FreeSql.Select<YssxClass>().Where(x => classIds.Contains(x.Id) && x.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync(x => new TaskClassViewModel
                    {
                        ClassId = x.Id,
                        ClassName = x.Name,
                    });

                dataModel.ClassList = classList;
            }

            //课堂测验
            if (tempModel.TaskType == 0)
            {
                //班级列表
                //课堂测验在使用YssxTask表时  ClassId值存储在TenantId字段上
                var classList = await DbContext.FreeSql.Select<YssxClass>().From<YssxTask>((a, b) =>
                 a.InnerJoin(x => x.Id == b.TenantId))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && b.GroupId == tempModel.TaskId)
                    .ToListAsync((a, b) => new TaskClassViewModel
                    {
                        ClassId = a.Id,
                        ClassName = a.Name,
                    });

                dataModel.ClassList = classList;

                //学生数量
                dataModel.StudentCount = await GetClassStudentCountByClass(classList);
            }

            //岗位实训任务
            if (tempModel.TaskType == 2)
            {
                //班级列表
                var classList = await DbContext.FreeSql.Select<YssxClass>().From<YssxJobTrainingTaskClass>((a, b) =>
                 a.InnerJoin(x => x.Id == b.ClassId))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && b.JobTrainingTaskId == tempModel.TaskId)
                    .ToListAsync((a, b) => new TaskClassViewModel
                    {
                        ClassId = a.Id,
                        ClassName = a.Name,
                    });

                dataModel.ClassList = classList;

                //学生数量
                dataModel.StudentCount = await GetClassStudentCountByClass(classList);
            }

            list.Add(dataModel);

            response.Data = list;

            return response;
        }
        #endregion

        #region 根据班级获取学生数量
        /// <summary>
        /// 根据班级获取学生数量
        /// </summary>
        /// <param name="classList"></param>
        /// <returns></returns>
        private async Task<int> GetClassStudentCountByClass(List<TaskClassViewModel> classList)
        {
            if (classList == null || classList.Count == 0)
                return 0;

            //班级Ids
            List<long> classIds = classList.Select(a => a.ClassId).ToList();
            //学生数量
            int studentCount = (int)await DbContext.FreeSql.Select<YssxStudent>().Where(x => classIds.Contains(x.ClassId)
             && x.IsDelete == CommonConstants.IsNotDelete).CountAsync();

            return studentCount;
        }
        #endregion

        #region 获取教师资源详情
        /// <summary>
        /// 获取教师资源详情
        /// </summary> 
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxTeacherResourceViewModel>>> GetTeacherResourceList(UserTicket user)
        {
            ResponseContext<List<YssxTeacherResourceViewModel>> response = new ResponseContext<List<YssxTeacherResourceViewModel>>();
            if (user == null)
                return response;

            //todo:应加UserId条件
            response.Data = await DbContext.FreeSql.Select<YssxTeacherResource>().Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new YssxTeacherResourceViewModel
                {
                    ResourceType = x.ResourceType,
                    TotalCount = x.TotalCount,
                    HasWonCount = x.HasWonCount,
                });

            return response;
        }
        #endregion

        #region 获取教师实训资源详情
        /// <summary>
        /// 获取教师实训资源详情
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxTeacherTrainResourceViewModel>>> GetTeacherTrainResourceList(UserTicket user)
        {
            ResponseContext<List<YssxTeacherTrainResourceViewModel>> response = new ResponseContext<List<YssxTeacherTrainResourceViewModel>>();
            if (user == null)
                return response;

            List<YssxTeacherTrainResourceViewModel> list = new List<YssxTeacherTrainResourceViewModel>();

            //案例列表
            List<YssxCase> caseList = await DbContext.FreeSql.Select<YssxCase>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //0:会计实训 1:出纳实训 2:大数据 3:业税财            
            int accCount = caseList.Where(x => x.SxType == 0).Count();
            int cashCount = caseList.Where(x => x.SxType == 1).Count();
            int bigCount = caseList.Where(x => x.SxType == 2).Count();
            int itmCount = caseList.Where(x => x.SxType == 3).Count();

            //购买案例列表（实训）
            var orderList = await DbContext.FreeSql.Select<YssxSchoolCase>().From<YssxCase>(
               (a, b) =>
               a.InnerJoin(x => x.CaseId == b.Id)
               ).Where((a, b) => a.SchoolId == user.TenantId && a.OpenStatus == Status.Enable && a.IsDelete == CommonConstants.IsNotDelete
                   && b.IsDelete == CommonConstants.IsNotDelete)
               .GroupBy((a, b) => new { b.Id, b.Name, b.SxType })
               .ToListAsync(x => new
               {
                   Id = x.Key.Id,
                   Name = x.Key.Name,
                   SxType = x.Key.SxType,
               });
            //0:会计实训 1:出纳实训 2:大数据 3:业税财            
            int accOrderCount = orderList.Where(x => x.SxType == 0).Count();
            int cashOrderCount = orderList.Where(x => x.SxType == 1).Count();
            int bigOrderCount = orderList.Where(x => x.SxType == 2).Count();
            int itmOrderCount = orderList.Where(x => x.SxType == 3).Count();

            //业务场景实训开放给用户  不需要购买
            //总数展示列表数据(包括未上架)
            //购买数取已上架的数据

            //业务场景实训列表
            List<YssxSceneTraining> sceneTrainingList = await DbContext.FreeSql.Select<YssxSceneTraining>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            int sceneCount = sceneTrainingList.Count();

            //购买业务场景实训列表
            int sceneOrderCount = sceneTrainingList.Where(x => x.AuditStatus == 1).Count();

            list.Add(new YssxTeacherTrainResourceViewModel
            {
                TrainType = 0,
                TotalCount = bigCount,
                HasWonCount = bigOrderCount,
            });
            list.Add(new YssxTeacherTrainResourceViewModel
            {
                TrainType = 1,
                TotalCount = itmCount,
                HasWonCount = itmOrderCount,
            });
            list.Add(new YssxTeacherTrainResourceViewModel
            {
                TrainType = 2,
                TotalCount = accCount,
                HasWonCount = accOrderCount,
            });
            list.Add(new YssxTeacherTrainResourceViewModel
            {
                TrainType = 3,
                TotalCount = cashCount,
                HasWonCount = cashOrderCount,
            });
            list.Add(new YssxTeacherTrainResourceViewModel
            {
                TrainType = 4,
                TotalCount = sceneCount,
                HasWonCount = sceneOrderCount,
            });

            response.Data = list;

            return response;
        }
        #endregion

        #region 获取首页案例列表
        /// <summary>
        /// 获取首页案例列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<UserCaseViewModel>>> GetUserCaseList()
        {
            ResponseContext<List<UserCaseViewModel>> response = new ResponseContext<List<UserCaseViewModel>>();

            //案例列表
            response.Data = await DbContext.FreeSql.Select<YssxUploadCase>().Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.CaseStatus == (int)CaseStatus.OnShelves)
                .OrderByDescending(x => x.CreateTime)
                .Take(6)
                .ToListAsync(x => new UserCaseViewModel
                {
                    CaseId = x.Id,
                    CaseName = x.Title,
                });

            response.Data.ForEach(x =>
            {
                x.CaseName = System.Web.HttpUtility.UrlDecode(x.CaseName, Encoding.UTF8);
            });

            return response;
        }
        #endregion

    }
}
