﻿using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tas.Common.Configurations;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Logger;
using Yssx.Framework.WeiDian;
using Yssx.S.Dto.Mall;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Mall;

namespace Yssx.S.ServiceImpl
{
    public class WeiDianHostedService : BackgroundService
    {
        private Timer timer;
        private readonly string appKey = "692107";
        private readonly string secret = "1a5f588e9a77af2cec9a2cd92112d5f0";
        private string token = string.Empty;
        WeiDianHttpClient client = new WeiDianHttpClient();

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //if ("true".Equals(AppSettingConfig.GetSection("enable_weidian"), StringComparison.OrdinalIgnoreCase))
            //{
            //    token = RedisHelper.Get(CommonConstants.Cache_WeiDianAccessToken);
            //    if (string.IsNullOrEmpty(token))
            //    {
            //        token = GetToken();
            //        if (!string.IsNullOrEmpty(token))
            //        {
            //            RedisHelper.Set(CommonConstants.Cache_WeiDianAccessToken, token);
            //            DoWork("test");
            //            // 定时任务，每十分钟运行一次
            //            timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromMinutes(10));
            //        }
            //    }
            //    else
            //    {
            //        DoWork("test");
            //        // 定时任务，每十分钟运行一次
            //        timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromMinutes(10));
            //    }
            //}
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            if (!string.IsNullOrEmpty(token))
            {
                //已完成
                GetOrderListRequest getOrderListRequest = new GetOrderListRequest() { OrderType = OrderType.finish };
                GetOrderListResponse getOrderListResponse = client.GetOrderList(getOrderListRequest, token);
                // token 失效
                if (getOrderListResponse.Status.StatusCode.Equals("10013"))
                {
                    RedisHelper.Set(CommonConstants.Cache_WeiDianAccessToken, "");
                    token = GetToken();
                    if (!string.IsNullOrEmpty(token))
                        RedisHelper.Set(CommonConstants.Cache_WeiDianAccessToken, token);
                    return;
                }
                var pages = Math.Ceiling((decimal)getOrderListResponse.Result.TotalNum / getOrderListRequest.PageSize);
                for (int i = 1; i <= pages; i++)
                {
                    if (i != 1)
                    {
                        getOrderListRequest = new GetOrderListRequest() { OrderType = OrderType.finish, PageNum = i };
                        getOrderListResponse = client.GetOrderList(getOrderListRequest, token);
                    }
                    HandlerAsync(getOrderListResponse);
                }
                //已发货
                GetOrderListRequest getShipedOrderListRequest = new GetOrderListRequest() { OrderType = OrderType.shiped };
                GetOrderListResponse getShipedOrderListResponse = client.GetOrderList(getShipedOrderListRequest, token);
                // token 失效
                if (getShipedOrderListResponse.Status.StatusCode.Equals("10013"))
                {
                    RedisHelper.Set(CommonConstants.Cache_WeiDianAccessToken, "");
                    token = GetToken();
                    if (!string.IsNullOrEmpty(token))
                        RedisHelper.Set(CommonConstants.Cache_WeiDianAccessToken, token);
                    return;
                }
                var pagesShiped = Math.Ceiling((decimal)getShipedOrderListResponse.Result.TotalNum / getShipedOrderListRequest.PageSize);
                for (int m = 1; m <= pagesShiped; m++)
                {
                    if (m != 1)
                    {
                        getShipedOrderListRequest = new GetOrderListRequest() { OrderType = OrderType.shiped, PageNum = m };
                        getShipedOrderListResponse = client.GetOrderList(getShipedOrderListRequest, token);
                    }
                    HandlerAsync(getShipedOrderListResponse);
                }

            }
        }

        private async Task HandlerAsync(GetOrderListResponse getOrderListResponse)
        {
            if (getOrderListResponse.Status.StatusCode.Equals("0"))
            {
                foreach (var order in getOrderListResponse.Result.Orders)
                {
                    GetOrderRequest getOrderRequest = new GetOrderRequest();
                    getOrderRequest.OrderId = order.OrderId;
                    GetOrderResponse getOrderResponse = client.GetOrder(getOrderRequest, token);
                    if (getOrderResponse.Status.StatusCode.Equals("0"))
                    {
                        var yssxUser = DbContext.FreeSql.Select<YssxUser>().Where(u => u.MobilePhone == getOrderResponse.Result.UserPhone).First();
                        if (yssxUser == null)
                            continue;

                        // 同一用户，同一时间只能有一笔订单。该订单已同步过，不再处理
                        if (DbContext.FreeSql.Select<YssxMallOrder>().Where(o => o.UserId == yssxUser.Id).Where(o => o.PaymentTime == getOrderResponse.Result.PayTime).Any())
                            continue;

                        using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                        {
                            try
                            {
                                // 新增订单
                                YssxMallOrder yssxMallOrder = new YssxMallOrder();
                                yssxMallOrder.Id = long.Parse(getOrderResponse.Result.OrderId);
                                yssxMallOrder.UserId = yssxUser.Id;
                                if (getOrderResponse.Result.StatusOri == "30" || getOrderResponse.Result.StatusOri == "31" || getOrderResponse.Result.StatusOri == "40" || getOrderResponse.Result.StatusOri == "50")
                                {
                                    yssxMallOrder.Status = 2;
                                }
                                else
                                {
                                    continue;
                                }
                                yssxMallOrder.PaymentType = 1;
                                yssxMallOrder.PaymentTime = getOrderResponse.Result.PayTime;
                                yssxMallOrder.Source = 2;
                                yssxMallOrder.OrderTotal = decimal.Parse(getOrderResponse.Result.Total);
                                yssxMallOrder.Remark = getOrderResponse.Result.StatusDesc;
                                DbContext.FreeSql.Insert<YssxMallOrder>(yssxMallOrder).ExecuteAffrows();

                                foreach (var item in getOrderResponse.Result.Items)
                                {
                                    var yssxMallProduct = DbContext.FreeSql.Select<YssxMallProduct>().Where(p => p.Id == long.Parse(item.MerchantCode)).First();
                                    if (yssxMallProduct == null)
                                        continue;
                                    var yssxMallProductItems = DbContext.FreeSql.Select<YssxMallProductItem>().Where(p => p.ProductId == long.Parse(item.MerchantCode)).ToList();
                                    if (yssxMallProductItems == null)
                                        continue;

                                    YssxMallOrderItem yssxMallOrderItem = new YssxMallOrderItem();
                                    yssxMallOrderItem.Id = IdWorker.NextId();
                                    yssxMallOrderItem.OrderId = yssxMallOrder.Id;
                                    yssxMallOrderItem.ProductId = yssxMallProduct.Id;
                                    yssxMallOrderItem.Quantity = int.Parse(item.Quantity);
                                    yssxMallOrderItem.UnitPrice = decimal.Parse(item.Price);
                                    yssxMallOrderItem.DiscountAmount = yssxMallProduct.OldPrice * yssxMallOrderItem.Quantity - decimal.Parse(item.TotalPrice);
                                    yssxMallOrder.OrderDiscount += yssxMallOrderItem.DiscountAmount;
                                    DbContext.FreeSql.Insert<YssxMallOrderItem>(yssxMallOrderItem).ExecuteAffrows();

                                    foreach (var yssxMallProductItem in yssxMallProductItems)
                                    {
                                        AutoGenerateTestPaperDto autoGenerateTestPaperDto = new AutoGenerateTestPaperDto()
                                        {
                                            Category = yssxMallProductItem.Category,
                                            TargetId = yssxMallProductItem.TargetId,
                                            UserId = yssxMallOrder.UserId
                                        };
                                        if (yssxMallProductItem.Source == 1)
                                        {
                                            //云实习
                                            var result = await AutoGenerateTestPaper(autoGenerateTestPaperDto);
                                            CommonLogger.Error("云实习生成试卷：" + result);
                                        }
                                        else if (yssxMallProductItem.Source == 2)
                                        {
                                            //同步实训
                                            try
                                            {
                                                autoGenerateTestPaperDto.Category = 1;
                                                Service.CaseServiceNew caseService = new Service.CaseServiceNew();
                                                var result2 = caseService.AutoGenerateTestPaper(autoGenerateTestPaperDto).Result;
                                            }
                                            catch (Exception ex)
                                            {
                                                CommonLogger.Error("同步实训生成试卷：" + ex.Message);
                                            }
                                        }
                                        else if (yssxMallProductItem.Source == 3)
                                        {
                                            //业税财
                                        }
                                        else if (yssxMallProductItem.Source == 4)
                                        {
                                            //岗位实训
                                            try
                                            {
                                                autoGenerateTestPaperDto.Category = 1;
                                                InternshipProveService ipService = new InternshipProveService();
                                                var result4 = ipService.AutoGenerateTestPaper(autoGenerateTestPaperDto).Result;
                                            }
                                            catch (Exception ex)
                                            {
                                                CommonLogger.Error("岗位实训生成试卷：" + ex.Message);
                                            }
                                        }
                                        else if (yssxMallProductItem.Source == 5)
                                        {
                                            //课程实训
                                            try
                                            {
                                                autoGenerateTestPaperDto.Category = 1;
                                            }
                                            catch (Exception ex)
                                            {
                                                CommonLogger.Error("课程实训生成试卷：" + ex.Message);
                                            }

                                        }
                                        else if (yssxMallProductItem.Source == 6)
                                        {
                                            //经验案例
                                            try
                                            {
                                                autoGenerateTestPaperDto.Category = 1;
                                                UploadCaseService ucService = new UploadCaseService();
                                                var result6 = ucService.AutoGenerateTestPaper(autoGenerateTestPaperDto).Result;
                                            }
                                            catch (Exception ex)
                                            {
                                                CommonLogger.Error("经验案例生成试卷：" + ex.Message);
                                            }
                                        }
                                        else if (yssxMallProductItem.Source == 7)
                                        {
                                            //初级会计考证
                                            try
                                            {
                                                autoGenerateTestPaperDto.Category = 1;
                                            }
                                            catch (Exception ex)
                                            {
                                                CommonLogger.Error("初级会计考证生成试卷：" + ex.Message);
                                            }
                                        }
                                    }
                                }
                                DbContext.FreeSql.Update<YssxMallOrder>(yssxMallOrder).SetSource(yssxMallOrder).ExecuteAffrows();

                                uow.Commit();
                            }
                            catch (Exception ex)
                            {
                                CommonLogger.Error(ex.Message);
                                uow.Rollback();
                            }
                        }
                    }
                }
            }
        }

        public async Task<string> AutoGenerateTestPaper(AutoGenerateTestPaperDto input)
        {
            var api = AppSettingConfig.GetSection("yssc_api");
            HttpClient client = new HttpClient();
            var requestContent = new StringContent(JsonConvert.SerializeObject(input));
            requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var response = await client.PostAsync($"{api}yssxc/Onboarding/AutoGenerateTestPaper", requestContent);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }

        private string GetToken()
        {
            var token = string.Empty;
            var tokenResponse = client.GetAccessToken(appKey, secret);
            if ("0".Equals(tokenResponse.Status.StatusCode))
            {
                token = tokenResponse.Result.AccessToken;
            }
            else
            {
                CommonLogger.Error($"获取微店API的access_token失败，{tokenResponse.Status.StatusCode} - {tokenResponse.Status.StatusReason}");
            }
            return token;
        }

        private string GenerateOrderNo(int orderType)
        {
            string prefix = string.Empty;
            if (orderType == 1)
            {
                prefix = "ZZSC";
            }
            else if (orderType == 2)
            {
                prefix = "KCTB";
            }
            else if (orderType == 3)
            {
                prefix = "YESC";
            }

            return prefix + DateTime.Now.ToString("yyyyMMdd") + new Random().Next(100000, 999999);
        }
    }
}
