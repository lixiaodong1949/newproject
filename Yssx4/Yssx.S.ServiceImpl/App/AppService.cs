﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.Entity;
using Yssx.Framework.AutoMapper;
using Yssx.Repository.Extensions;
using Yssx.S.Poco;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// App服务实现
    /// </summary>
    public class AppService : IAppService
    {
        #region 用户是否已存在预览课程行为
        /// <summary>
        /// 用户是否已存在预览课程行为
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> IsExistUserPreviewCourseAction(UserTicket user)
        {
            YssxUserCoursePreviewRecord data = await DbContext.FreeSql.GetRepository<YssxUserCoursePreviewRecord>().Where(x => x.UserId == user.Id).FirstAsync();

            bool state = false;

            if (data != null)
                state = true;

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state, Msg = state ? "已存在!" : "未存在!" };
        }
        #endregion

        #region 获取用户预览课程信息
        /// <summary>
        /// 获取用户预览课程信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserPreviewCourseInfoViewModel>> GetUserPreviewCourseInfoForApp(UserTicket user)
        {
            ResponseContext<UserPreviewCourseInfoViewModel> result = new ResponseContext<UserPreviewCourseInfoViewModel>();

            UserPreviewCourseInfoViewModel data = new UserPreviewCourseInfoViewModel();

            YssxUserCoursePreviewRecord previewData = await DbContext.FreeSql.GetRepository<YssxUserCoursePreviewRecord>().Where(x => x.UserId == user.Id && x.IsDelete
                == CommonConstants.IsNotDelete).FirstAsync();
            if (previewData == null)
                return new ResponseContext<UserPreviewCourseInfoViewModel> { Code = CommonConstants.SuccessCode, Data = null };

            YssxCourse courseData = await DbContext.FreeSql.GetRepository<YssxCourse>().Where(x => x.Id == previewData.CourseId).FirstAsync();
            if (courseData == null)
                return new ResponseContext<UserPreviewCourseInfoViewModel> { Code = CommonConstants.SuccessCode, Data = null };

            data.CourseId = courseData.Id;
            data.CourseName = courseData.CourseName;

            result.Data = data;
            result.Code = CommonConstants.SuccessCode;
            return result;
        }
        #endregion

        #region 获取学校课程订单数据(App)
        /// <summary>
        /// 获取学校课程订单数据(App)
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<OrderCourseDto>>> GetCourseListBySchoolPlatForApp(CourseSearchDto dto, UserTicket user)
        {
            var userId = user.Id;
            var tenantid = user.TenantId;
            var result = new ResponseContext<List<OrderCourseDto>>();
            List<YssxCourse> list = new List<YssxCourse>();

            list = await DbContext.FreeSql.GetRepository<YssxCourse>().Select
                                .From<YssxCourseOrder>((a, b) => a.InnerJoin(aa => aa.Id == b.CourseId))
                                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete).Where((a, b) => b.TenantId == tenantid)
                                .OrderByDescending((a, b) => a.CreateTime)
                                .ToListAsync();

            List<YssxCourseListDto> retList = list.Select(x => new YssxCourseListDto
            {
                Author = x.Author,
                CourseName = x.CourseName,
                Education = x.Education,
                Id = x.Id,
                PublishingDate = x.PublishingDate,
                PublishingHouse = x.PublishingHouse,
                Images = x.Images,
                IsShow = x.IsShow,
                CloneStatus = x.CloneStatus,
                IsEdit = x.IsEdit
            }).ToList();

            //当前用户移除的课程集合
            var removeCourseIds = await DbContext.FreeSql.GetRepository<YssxRemoveCourse>()
                .Where(s => s.TenantId == tenantid && s.UserId == userId && s.IsDelete == CommonConstants.IsNotDelete).Select(s => s.CourseId.ToString()).ToListAsync();

            //查询当前预览课程
            YssxUserCoursePreviewRecord data = await DbContext.FreeSql.GetRepository<YssxUserCoursePreviewRecord>().Where(x => x.UserId == user.Id).FirstAsync();

            foreach (var item in retList)
            {
                if (removeCourseIds.Contains(item.Id.ToString()))
                    item.IsRemoved = true;
                else
                    item.IsRemoved = false;

                if (data != null)
                    if (item.Id == data.CourseId)
                        item.IsPreviewNow = true;
            }

            //学历集合(去除-1学历)
            var educations = retList.Where(x => x.Education != -1).GroupBy(s => s.Education).Distinct().Select(t => t.Key).ToList();
            var courses = new List<OrderCourseDto>();
            foreach (var education in educations)
            {
                var educationCourse = new OrderCourseDto();
                educationCourse.Education = education;
                educationCourse.CourseList = retList.Where(s => s.Education == education).ToList();
                courses.Add(educationCourse);
            }

            result.Data = courses;
            result.Code = CommonConstants.SuccessCode;
            return result;
        }
        #endregion

        #region 获取学校课程任务下课程数据(App)
        /// <summary>
        /// 获取学校课程任务下课程数据(App)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<OrderCourseDto>>> GetCourseListBySchoolCourseTaskForApp(UserTicket user)
        {
            var userId = user.Id;
            var tenantid = user.TenantId;
            var result = new ResponseContext<List<OrderCourseDto>>();
            List<YssxCourse> list = new List<YssxCourse>();

            list = await DbContext.FreeSql.GetRepository<YssxCourse>().Select
                                .From<YssxCourseOrder>((a, b) => a.InnerJoin(aa => aa.Id == b.CourseId))
                                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete).Where((a, b) => b.TenantId == tenantid)
                                .OrderByDescending((a, b) => a.CreateTime)
                                .ToListAsync();

            List<YssxCourseListDto> retList = list.Select(x => new YssxCourseListDto
            {
                Author = x.Author,
                CourseName = x.CourseName,
                Education = x.Education,
                Id = x.Id,
                PublishingDate = x.PublishingDate,
                PublishingHouse = x.PublishingHouse,
                Images = x.Images,
                IsShow = x.IsShow,
                CloneStatus = x.CloneStatus,
                IsEdit = x.IsEdit
            }).ToList();

            //当前用户移除的课程集合
            var removeCourseIds = await DbContext.FreeSql.GetRepository<YssxRemoveCourse>()
                .Where(s => s.TenantId == tenantid && s.UserId == userId && s.IsDelete == CommonConstants.IsNotDelete).Select(s => s.CourseId.ToString()).ToListAsync();

            //查询当前发布课程任务课程
            YssxUserPublishCourseTaskRecord data = await DbContext.FreeSql.GetRepository<YssxUserPublishCourseTaskRecord>().Where(x => x.UserId == user.Id).FirstAsync();

            foreach (var item in retList)
            {
                if (removeCourseIds.Contains(item.Id.ToString()))
                    item.IsRemoved = true;
                else
                    item.IsRemoved = false;

                if (data != null)
                    if (item.Id == data.CourseId)
                        item.IsPreviewNow = true;
            }

            //学历集合(去除-1学历)
            var educations = retList.Where(x => x.Education != -1).GroupBy(s => s.Education).Distinct().Select(t => t.Key).ToList();
            var courses = new List<OrderCourseDto>();
            foreach (var education in educations)
            {
                var educationCourse = new OrderCourseDto();
                educationCourse.Education = education;
                educationCourse.CourseList = retList.Where(s => s.Education == education).ToList();
                courses.Add(educationCourse);
            }

            result.Data = courses;
            result.Code = CommonConstants.SuccessCode;
            return result;
        }
        #endregion

        #region 获取学校课程订单数据(不分学历层级)
        /// <summary>
        /// 获取学校课程订单数据(不分学历层级)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxCourseListDto>>> GetSchoolBuyCourseListForApp(UserTicket user)
        {
            var userId = user.Id;
            var tenantid = user.TenantId;
            var result = new ResponseContext<List<YssxCourseListDto>>();
            List<YssxCourse> list = new List<YssxCourse>();

            list = await DbContext.FreeSql.GetRepository<YssxCourse>().Select
                                .From<YssxCourseOrder>((a, b) => a.InnerJoin(aa => aa.Id == b.CourseId))
                                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete).Where((a, b) => b.TenantId == tenantid)
                                .OrderByDescending((a, b) => a.CreateTime)
                                .ToListAsync();

            List<YssxCourseListDto> retList = list.Select(x => new YssxCourseListDto
            {
                Author = x.Author,
                CourseName = x.CourseName,
                Education = x.Education,
                Id = x.Id,
                PublishingDate = x.PublishingDate,
                PublishingHouse = x.PublishingHouse,
                Images = x.Images,
                IsShow = x.IsShow,
                CloneStatus = x.CloneStatus,
                IsEdit = x.IsEdit
            }).ToList();

            //当前用户移除的课程集合
            var removeCourseIds = await DbContext.FreeSql.GetRepository<YssxRemoveCourse>()
                .Where(s => s.TenantId == tenantid && s.UserId == userId && s.IsDelete == CommonConstants.IsNotDelete).Select(s => s.CourseId.ToString()).ToListAsync();

            //查询当前预览课程
            YssxUserCoursePreviewRecord data = await DbContext.FreeSql.GetRepository<YssxUserCoursePreviewRecord>().Where(x => x.UserId == user.Id).FirstAsync();

            foreach (var item in retList)
            {
                if (removeCourseIds.Contains(item.Id.ToString()))
                    item.IsRemoved = true;
                else
                    item.IsRemoved = false;

                if (data != null)
                    if (item.Id == data.CourseId)
                        item.IsPreviewNow = true;
            }

            result.Data = retList;
            result.Code = CommonConstants.SuccessCode;
            return result;
        }
        #endregion

        #region 记录用户预览课程记录
        /// <summary>
        /// 记录用户预览课程记录
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RecordUserCoursePreviewAction(UserCoursePreviewRequestModel dto, UserTicket user)
        {
            if (dto == null || dto.CourseId == 0 || dto.UserId == 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不允许为空!" };

            YssxUserCoursePreviewRecord data = await DbContext.FreeSql.GetRepository<YssxUserCoursePreviewRecord>().Where(x => x.UserId == user.Id && x.IsDelete ==
                CommonConstants.IsNotDelete).FirstAsync();

            if (data == null)
            {
                data = new YssxUserCoursePreviewRecord();

                data.UserId = user.Id;
                data.TenantId = user.TenantId;
                data.CreateBy = user.Id;
                data.CreateTime = DateTime.Now;
            }
            data.CourseId = dto.CourseId;
            data.UpdateBy = user.Id;
            data.UpdateTime = DateTime.Now;

            bool state = false;

            if (data.Id <= 0)
            {
                data.Id = IdWorker.NextId();

                YssxUserCoursePreviewRecord model = await DbContext.FreeSql.GetRepository<YssxUserCoursePreviewRecord>().InsertAsync(data);
                state = model != null;
            }
            else
            {
                state = await DbContext.FreeSql.GetRepository<YssxUserCoursePreviewRecord>().UpdateDiy.SetSource(data).UpdateColumns(x => new
                {
                    x.CourseId,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 记录用户发布课程任务记录
        /// <summary>
        /// 记录用户发布课程任务记录
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RecordUserPublishCourseTaskAction(UserPublishCourseTaskRequestModel dto, UserTicket user)
        {
            if (dto == null || dto.CourseId == 0 || dto.UserId == 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不允许为空!" };

            YssxUserPublishCourseTaskRecord data = await DbContext.FreeSql.GetRepository<YssxUserPublishCourseTaskRecord>().Where(x => x.UserId == user.Id && x.IsDelete ==
                CommonConstants.IsNotDelete).FirstAsync();

            if (data == null)
            {
                data = new YssxUserPublishCourseTaskRecord();

                data.UserId = user.Id;
                data.TenantId = user.TenantId;
                data.CreateBy = user.Id;
                data.CreateTime = DateTime.Now;
            }
            data.CourseId = dto.CourseId;
            data.UpdateBy = user.Id;
            data.UpdateTime = DateTime.Now;

            bool state = false;

            if (data.Id <= 0)
            {
                data.Id = IdWorker.NextId();

                YssxUserPublishCourseTaskRecord model = await DbContext.FreeSql.GetRepository<YssxUserPublishCourseTaskRecord>().InsertAsync(data);
                state = model != null;
            }
            else
            {
                state = await DbContext.FreeSql.GetRepository<YssxUserPublishCourseTaskRecord>().UpdateDiy.SetSource(data).UpdateColumns(x => new
                {
                    x.CourseId,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

    }
}
