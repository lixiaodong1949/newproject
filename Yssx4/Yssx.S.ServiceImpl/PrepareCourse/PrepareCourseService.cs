﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Entity;
using Yssx.S.Poco;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 课程备课 [Obsolete]
    /// </summary>
    public class PrepareCourseService : IPrepareCourseService
    {
        #region 选中备课
        /// <summary>
        /// 选中备课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> SelectedLessonPrepare(SelectedLessonPrepareDto dto, UserTicket user)
        {
            if (dto == null || !dto.CourseId.HasValue || user == null) return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "对象是空的!" };

            YssxCourse courseModel = await DbContext.FreeSql.GetRepository<YssxCourse>().Where(x => x.Id == dto.CourseId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (courseModel == null)
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "没有课程数据源!" };

            //验证课程不能重复备课
            List<YssxPrepareCourse> originalCourseList = await DbContext.FreeSql.GetRepository<YssxPrepareCourse>().Where(x => x.OriginalCourseId == dto.CourseId
                && x.CreateBy == user.Id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (originalCourseList != null && originalCourseList.Count > 0)
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "该门课程已被选中,不能重复备课!" };

            bool state = false;

            YssxPrepareCourse preCourseEntity = new YssxPrepareCourse
            {
                Id = IdWorker.NextId(),
                CourseName = courseModel.CourseName,
                CourseType = courseModel.CourseType,
                CourseTitle = courseModel.CourseTitle,
                CourseTypeId = courseModel.CourseTypeId,
                KnowledgePointId = courseModel.KnowledgePointId.ToString(),
                Education = courseModel.Education,
                Author = courseModel.Author,
                PublishingHouse = courseModel.PublishingHouse,
                PublishingDate = courseModel.PublishingDate,
                Images = courseModel.Images,
                Intro = courseModel.Intro,
                Description = courseModel.Description,
                ReadCount = courseModel.ReadCount,
                TenantId = user.TenantId,
                CreateBy = user.Id,
                CreateByName = user.Name,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now,
                OriginalCourseId = dto.CourseId,

                CourseSource = dto.CourseSource,
                OrderId = dto.OrderId,
                EffectiveDate = dto.EffectiveDate,
            };

            //记录原始章节Id、新章节Id
            Dictionary<long, long> keyValuePairs = new Dictionary<long, long>();
            //原始课程章节
            var oldSectionList = DbContext.FreeSql.GetRepository<YssxSection>().Where(x => x.CourseId == dto.CourseId.Value && x.IsDelete == CommonConstants.IsNotDelete).ToList();            
            var resultList = CreateSectionParentId(oldSectionList, keyValuePairs);
            //原始章节Ids
            List<long> oldSectionIds = keyValuePairs.Keys.ToList();

            //复制章节信息
            var preSecList = ReplicaSection(preCourseEntity.Id, resultList, user);
            //复制章节习题信息
            var preTopicList = ReplicaSectionTopic(dto.CourseId.Value, preCourseEntity.Id, keyValuePairs, user);
            //复制章节场景实训信息
            var preSceneList = ReplicaSectionSceneTraining(dto.CourseId.Value, preCourseEntity.Id, dto.CourseSource, dto.OrderId, dto.EffectiveDate, keyValuePairs, user);
            //复制章节教材
            var preTextList = ReplicaSectionTextBook(dto.CourseId.Value, preCourseEntity.Id, keyValuePairs, user);
            //复制课程课件、视频、教案、授课计划
            var preFilesList = ReplicaSectionFiles(dto.CourseId.Value, preCourseEntity.Id, dto.CourseSource, dto.OrderId, dto.EffectiveDate, keyValuePairs, user);
            //复制章节统计
            var preSummaryList = ReplicaSectionSummary(dto.CourseId.Value, preCourseEntity.Id, keyValuePairs, user);

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //课程
                    YssxPrepareCourse preCourse = uow.GetRepository<YssxPrepareCourse>().Insert(preCourseEntity);

                    //章节
                    if (preSecList.Count > 0)
                        uow.GetRepository<YssxPrepareSection>().Insert(preSecList);
                    //章节习题
                    if (preTopicList.Count > 0)
                        uow.GetRepository<YssxPrepareSectionTopic>().Insert(preTopicList);
                    //章节场景实训
                    if (preSceneList.Count > 0)
                        uow.GetRepository<YssxPrepareSectionSceneTraining>().Insert(preSceneList);
                    //章节教材
                    if (preTextList.Count > 0)
                        uow.GetRepository<YssxPrepareSectionTextBook>().Insert(preTextList);
                    //课程课件、视频、教案、授课计划
                    if (preFilesList.Count > 0)
                        uow.GetRepository<YssxPrepareSectionFiles>().Insert(preFilesList);
                    //章节统计
                    if (preSummaryList.Count > 0)
                        uow.GetRepository<YssxPrepareSectionSummary>().Insert(preSummaryList);

                    state = preCourse != null;

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            return new ResponseContext<long> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = preCourseEntity.Id, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 复制课程信息

        #region 复制章节信息
        /// <summary>
        /// 复制章节信息
        /// </summary>
        /// <param name="newCourseId">新生成课程Id</param>
        /// <param name="sectionList">原始课程章节</param>
        /// <param name="user"></param>
        /// <returns></returns>
        private List<YssxPrepareSection> ReplicaSection(long newCourseId, List<YssxSection> sectionList, UserTicket user)
        {
            List<YssxPrepareSection> list = new List<YssxPrepareSection>();

            if (sectionList == null || sectionList.Count <= 0)
                return list;

            var preSecList = sectionList.MapTo<List<YssxPrepareSection>>();

            preSecList.ForEach(x =>
            {
                x.CourseId = newCourseId;
                x.CreateBy = user.Id;
                x.UpdateBy = user.Id;
                x.CreateTime = DateTime.Now;
                x.UpdateTime = DateTime.Now;
            });

            return preSecList;
        }
        #endregion

        #region 复制章节习题信息
        /// <summary>
        /// 复制章节习题信息
        /// </summary>
        /// <param name="oldCourseId">选中课程Id</param>
        /// <param name="newCourseId">新生成课程Id</param>
        /// <param name="keyValuePairs">原始和新生成章节Ids</param>
        /// <param name="user"></param>
        /// <returns></returns>
        private List<YssxPrepareSectionTopic> ReplicaSectionTopic(long oldCourseId, long newCourseId, Dictionary<long, long> keyValuePairs, UserTicket user)
        {
            List<YssxPrepareSectionTopic> list = new List<YssxPrepareSectionTopic>();

            var topicList = DbContext.FreeSql.GetRepository<YssxSectionTopic>().Where(x => x.CourseId == oldCourseId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            if (topicList != null && topicList.Count > 0)
            {
                //原始章节Ids
                List<long> oldSectionIds = keyValuePairs.Keys.ToList();

                list = topicList.MapTo<List<YssxPrepareSectionTopic>>();

                list = list.Where(x => oldSectionIds.Contains(x.SectionId)).ToList();

                list.ForEach(x =>
                {
                    x.Id = IdWorker.NextId();
                    x.CourseId = newCourseId;
                    x.SectionId = x.SectionId == 0 ? 0 : keyValuePairs[x.SectionId];
                    x.CreateBy = user.Id;
                    x.UpdateBy = user.Id;
                    x.CreateTime = DateTime.Now;
                    x.UpdateTime = DateTime.Now;
                });
            }

            return list;
        }
        #endregion

        #region 复制章节场景实训信息
        /// <summary>
        /// 复制章节场景实训信息
        /// </summary>
        /// <param name="oldCourseId">选中课程Id</param>
        /// <param name="newCourseId">新生成课程Id</param>
        /// <param name="courseSource">选中课程来源 0:系统赠送 1:客户创建(购买) 2:上传私有</param>
        /// <param name="orderId">订单Id</param>
        /// <param name="effectiveDate">有效期</param>
        /// <param name="keyValuePairs">原始和新生成章节Ids</param>
        /// <param name="user"></param>
        /// <returns></returns>
        private List<YssxPrepareSectionSceneTraining> ReplicaSectionSceneTraining(long oldCourseId, long newCourseId, Nullable<int> courseSource,
            Nullable<long> orderId, Nullable<DateTime> effectiveDate, Dictionary<long, long> keyValuePairs, UserTicket user)
        {
            List<YssxPrepareSectionSceneTraining> list = new List<YssxPrepareSectionSceneTraining>();

            var secSceneList = DbContext.FreeSql.GetRepository<YssxSectionSceneTraining>().Where(x => x.CourseId == oldCourseId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            if (secSceneList != null && secSceneList.Count > 0)
            {
                //原始章节Ids
                List<long> oldSectionIds = keyValuePairs.Keys.ToList();

                list = secSceneList.MapTo<List<YssxPrepareSectionSceneTraining>>();

                list = list.Where(x => oldSectionIds.Contains(x.SectionId)).ToList();

                list.ForEach(x =>
                {
                    x.Id = IdWorker.NextId();
                    x.CourseId = newCourseId;
                    x.SectionId = x.SectionId == 0 ? 0 : keyValuePairs[x.SectionId];
                    x.CreateBy = user.Id;
                    x.UpdateBy = user.Id;
                    x.CreateTime = DateTime.Now;
                    x.UpdateTime = DateTime.Now;

                    x.OrderId = orderId;
                    x.EffectiveDate = effectiveDate;

                    if (courseSource.HasValue && (courseSource.Value == (int)CourseDataSource.SystemPresent || courseSource.Value == (int)CourseDataSource.IndividualBuy))
                        x.DataSourceType = (int)CourseResourcesType.UnderCourse;
                    if (courseSource.HasValue && courseSource.Value == (int)CourseDataSource.UploadPrivate)
                        x.DataSourceType = (int)CourseResourcesType.UploadPrivate;
                });
            }

            return list;
        }
        #endregion

        #region 复制章节教材
        /// <summary>
        /// 复制章节教材
        /// </summary>
        /// <param name="oldCourseId">选中课程Id</param>
        /// <param name="newCourseId">新生成课程Id</param>
        /// <param name="keyValuePairs">原始和新生成章节Ids</param>
        /// <param name="user"></param>
        /// <returns></returns>
        private List<YssxPrepareSectionTextBook> ReplicaSectionTextBook(long oldCourseId, long newCourseId, Dictionary<long, long> keyValuePairs, UserTicket user)
        {
            List<YssxPrepareSectionTextBook> list = new List<YssxPrepareSectionTextBook>();
            
            var secTextList = DbContext.FreeSql.GetRepository<YssxSectionTextBook>().Where(x => x.CourseId == oldCourseId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            if (secTextList != null && secTextList.Count > 0)
            {
                //原始章节Ids
                List<long> oldSectionIds = keyValuePairs.Keys.ToList();

                list = secTextList.MapTo<List<YssxPrepareSectionTextBook>>();

                list = list.Where(x => oldSectionIds.Contains(x.SectionId)).ToList();

                list.ForEach(x =>
                {
                    x.Id = IdWorker.NextId();
                    x.CourseId = newCourseId;
                    x.SectionId = x.SectionId == 0 ? 0 : keyValuePairs[x.SectionId];
                    x.CreateBy = user.Id;
                    x.UpdateBy = user.Id;
                    x.CreateTime = DateTime.Now;
                    x.UpdateTime = DateTime.Now;
                });
            }

            return list;
        }
        #endregion

        #region 复制课程课件、视频、教案、授课计划
        /// <summary>
        /// 复制课程课件、视频、教案、授课计划
        /// </summary>
        /// <param name="oldCourseId">选中课程Id</param>
        /// <param name="newCourseId">新生成课程Id</param>
        /// <param name="courseSource">选中课程来源 0:系统赠送 1:客户创建(购买) 2:上传私有</param>
        /// <param name="orderId">订单Id</param>
        /// <param name="effectiveDate">有效期</param>
        /// <param name="keyValuePairs">原始和新生成章节Ids</param>
        /// <param name="user"></param>
        /// <returns></returns>
        private List<YssxPrepareSectionFiles> ReplicaSectionFiles(long oldCourseId, long newCourseId, Nullable<int> courseSource,
            Nullable<long> orderId, Nullable<DateTime> effectiveDate, Dictionary<long, long> keyValuePairs, UserTicket user)
        {
            List<YssxPrepareSectionFiles> list = new List<YssxPrepareSectionFiles>();

            var couFilesList = DbContext.FreeSql.GetRepository<YssxSectionFiles>().Where(x => x.CourseId == oldCourseId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            if (couFilesList != null && couFilesList.Count > 0)
            {
                //原始章节Ids
                List<long> oldSectionIds = keyValuePairs.Keys.ToList();

                list = couFilesList.MapTo<List<YssxPrepareSectionFiles>>();

                list = list.Where(x => oldSectionIds.Contains(x.SectionId) || x.SectionId == 0).ToList();

                list.ForEach(x =>
                {
                    x.Id = IdWorker.NextId();
                    x.CourseId = newCourseId;
                    x.SectionId = x.SectionId == 0 ? 0 : keyValuePairs[x.SectionId];
                    x.CreateBy = user.Id;
                    x.UpdateBy = user.Id;
                    x.CreateTime = DateTime.Now;
                    x.UpdateTime = DateTime.Now;

                    x.OrderId = orderId;
                    x.EffectiveDate = effectiveDate;

                    if (courseSource.HasValue && (courseSource.Value == (int)CourseDataSource.SystemPresent || courseSource.Value == (int)CourseDataSource.IndividualBuy))
                        x.DataSourceType = (int)CourseResourcesType.UnderCourse;
                    if (courseSource.HasValue && courseSource.Value == (int)CourseDataSource.UploadPrivate)
                        x.DataSourceType = (int)CourseResourcesType.UploadPrivate;
                });
            }

            return list;
        }
        #endregion

        #region 复制章节统计
        /// <summary>
        /// 复制章节统计
        /// </summary>
        /// <param name="oldCourseId">选中课程Id</param>
        /// <param name="newCourseId">新生成课程Id</param>
        /// <param name="keyValuePairs">原始和新生成章节Ids</param>
        /// <param name="user"></param>
        /// <returns></returns>
        private List<YssxPrepareSectionSummary> ReplicaSectionSummary(long oldCourseId, long newCourseId, Dictionary<long, long> keyValuePairs, UserTicket user)
        {
            List<YssxPrepareSectionSummary> list = new List<YssxPrepareSectionSummary>();

            var couSummaryList = DbContext.FreeSql.GetRepository<YssxSectionSummary>().Where(x => x.CourseId == oldCourseId).ToList();
            if (couSummaryList != null && couSummaryList.Count > 0)
            {
                //原始章节Ids
                List<long> oldSectionIds = keyValuePairs.Keys.ToList();

                list = couSummaryList.MapTo<List<YssxPrepareSectionSummary>>();

                list = list.Where(x => oldSectionIds.Contains(x.SectionId) || x.SectionId == 0).ToList();

                list.ForEach(x =>
                {
                    x.Id = IdWorker.NextId();
                    x.CourseId = newCourseId;
                    x.SectionId = x.SectionId == 0 ? 0 : keyValuePairs[x.SectionId];
                    x.UpdateTime = DateTime.Now;
                });
            }

            return list;
        }
        #endregion

        #endregion

        #region 替换章节父节点
        /// <summary>
        /// 替换章节父节点
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private List<YssxSection> CreateSectionParentId(List<YssxSection> list, Dictionary<long, long> keyValuePairs)
        {
            List<YssxSection> resultList = new List<YssxSection>();

            if (list == null || list.Count <= 0)
                return resultList;

            //章列表
            var chaList = list.Where(x => x.SectionType == (int)SectionType.Chapter).ToList();

            resultList.AddRange(chaList);

            foreach (var item in chaList)
            {
                var secList = list.Where(x => x.ParentId == item.Id).ToList();
                long chapterId = IdWorker.NextId();
                //记录原始章Id、新章Id
                keyValuePairs.Add(item.Id, chapterId);
                item.Id = chapterId;
                secList.ForEach(x =>
                {
                    long sectionId = IdWorker.NextId();
                    //记录原始节Id、新节Id
                    keyValuePairs.Add(x.Id, sectionId);
                    x.Id = sectionId;
                    x.ParentId = item.Id;
                });

                resultList.AddRange(secList);
            }

            return resultList;
        }
        #endregion

        #region 取消选中备课
        /// <summary>
        /// 取消选中备课
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UncheckLessonPrepare(List<long> ids, UserTicket user)
        {
            var list = await DbContext.FreeSql.GetRepository<YssxPrepareCourse>().Where(x => ids.Contains(x.Id)).ToListAsync();

            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            list.ForEach(x =>
            {
                x.IsDelete = 1;
                x.UpdateTime = DateTime.Now;
                x.UpdateById = user.Id;
                x.UpdateByName = user.Name;
            });

            bool state = DbContext.FreeSql.GetRepository<YssxPrepareCourse>().UpdateDiy.SetSource(list).UpdateColumns(x => new
            { x.IsDelete, x.UpdateTime, x.UpdateById, x.UpdateByName }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 获取备课课程列表
        /// <summary>
        /// 获取备课课程列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<YssxPrepareCourseViewModel>> GetPrepareCourseList(YssxPrepareCourseQuery query, UserTicket user)
        {
            var result = new PageResponse<YssxPrepareCourseViewModel>();
            if (query == null)
                return result;
            if (!query.DataSourceType.HasValue)
                return new PageResponse<YssxPrepareCourseViewModel> { Code = CommonConstants.ErrorCode, Data = null, Msg = "需指定数据来源!" };

            FreeSql.ISelect<YssxPrepareCourse> select = DbContext.FreeSql.GetRepository<YssxPrepareCourse>().Select.Where(x => x.IsDelete == CommonConstants.IsNotDelete);

            if (query.DataSourceType.HasValue && query.DataSourceType.Value == (int)DataSourceType.Oneself)
                select.Where(x => x.CreateBy == user.Id);
            if (!string.IsNullOrWhiteSpace(query.Keyword))
                select.Where(x => x.CourseName.Contains(query.Keyword) || x.Author.Contains(query.Keyword));
            if (query.Education.HasValue)
                select.Where(x => x.Education == query.Education);
            if (query.SemesterId.HasValue)
                select.Where(x => x.SemesterId == query.SemesterId);

            var totalCount = await select.CountAsync();

            var sql = select.From<YssxSemester>((a, b) => a.LeftJoin(aa => aa.SemesterId == b.Id)).OrderByDescending((a, b) => a.CreateTime)
              .Page(query.PageIndex, query.PageSize).ToSql("a.Id,a.CourseName,a.CourseType,a.CourseTitle,a.CourseTypeId,a.KnowledgePointId,a.Education,a.Author,a.PublishingHouse," +
              "a.PublishingDate,a.Images,a.Intro,a.Description,a.ReadCount,a.CreateTime,a.CreateByName,a.UpdateTime,a.EffectiveDate,a.OriginalCourseId,b.Id AS SemesterId," +
              "b.Name AS SemesterName");

            var items = await DbContext.FreeSql.Ado.QueryAsync<YssxPrepareCourseViewModel>(sql);

            List<long> courseIds = items.Select(x => x.Id).ToList();
            List<YssxPrepareSectionSummary> summaries = await DbContext.FreeSql.GetRepository<YssxPrepareSectionSummary>().Select.Where(x => courseIds.Contains(x.CourseId)).ToListAsync();

            List<YssxTeacherClassViewModel> courseClassList = await DbContext.FreeSql.Select<YssxTeacherCourseClass>()
                     .Where(x => courseIds.Contains(x.CourseId) && x.IsDelete == CommonConstants.IsNotDelete)
                     .ToListAsync(x => new YssxTeacherClassViewModel
                     {
                         CourseId = x.CourseId,
                         CourseClassId = x.Id,
                         SchoolId = x.SchoolId,
                         SchoolName = x.SchoolName,
                         CollegeId = x.CollegeId,
                         CollegeName = x.CollegeName,
                         ProfessionId = x.ProfessionId,
                         ProfessionName = x.ProfessionName,
                         ClassId = x.ClassId,
                         ClassName = x.ClassName,
                     });

            //试卷列表
            List<ExamPaperCourse> examPaperList = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Select.Where(x => courseIds.Contains(x.CourseId) &&
                x.Status == ExamStatus.Started && x.ExamType == CourseExamType.ClassTest && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            items.ForEach(x =>
            {
                //课程汇总信息
                x.SummaryListViewModel = summaries.Where(o => o.CourseId == x.Id).Select(i => new YssxSectionSummaryDto
                {
                    Id = i.Id,
                    Count = i.Count,
                    CourseId = i.CourseId,
                    SummaryType = i.SummaryType
                }).ToList();

                //绑定班级列表
                x.CourseClassList = courseClassList.Where(a => a.CourseId == x.Id).ToList();

                //试卷Id
                if (examPaperList.Count > 0)
                {
                    x.ExamId = examPaperList.Where(a => a.CourseId == x.Id).OrderByDescending(a => a.CreateTime).FirstOrDefault()?.Id;
                    x.TaskId = examPaperList.Where(a => a.CourseId == x.Id).OrderByDescending(a => a.CreateTime).FirstOrDefault()?.TaskId;
                }
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 编辑备课课程
        /// <summary>
        /// 编辑备课课程
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EditPrepareCourse(PrepareCourseDto dto, UserTicket user)
        {
            bool state = false;
            if (dto == null || user == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "没有数据源" };

            YssxPrepareCourse entity = await DbContext.FreeSql.GetRepository<YssxPrepareCourse>().Where(x => x.Id == dto.Id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "没有该数据源!" };

            var preCourse = dto.MapTo<YssxPrepareCourse>();

            if (preCourse != null)
            {
                preCourse.UpdateById = user.Id;
                preCourse.UpdateByName = user.Name;
                preCourse.UpdateTime = DateTime.Now;

                state = DbContext.FreeSql.GetRepository<YssxPrepareCourse>().UpdateDiy.SetSource(preCourse).UpdateColumns(x => new
                {
                    x.CourseName,
                    x.CourseType,
                    x.CourseTitle,
                    x.CourseTypeId,
                    x.KnowledgePointId,
                    x.Education,
                    x.Author,
                    x.PublishingHouse,
                    x.PublishingDate,
                    x.Images,
                    x.Intro,
                    x.Description,
                    x.ReadCount,
                    x.SemesterId,
                    x.UpdateById,
                    x.UpdateByName,
                    x.UpdateTime
                }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 编辑备课课程学期
        /// <summary>
        /// 编辑备课课程学期
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EditPrepareCourseSemester(PrepareCourseSemesterDto dto, UserTicket user)
        {
            bool state = false;
            if (dto == null || user == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "没有数据源" };

            YssxPrepareCourse entity = await DbContext.FreeSql.GetRepository<YssxPrepareCourse>().Where(x => x.Id == dto.CourseId).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "没有该数据源!" };

            entity.SemesterId = dto.SemesterId;
            entity.UpdateById = user.Id;
            entity.UpdateByName = user.Name;
            entity.UpdateTime = DateTime.Now;

            state = DbContext.FreeSql.GetRepository<YssxPrepareCourse>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.SemesterId,
                x.UpdateById,
                x.UpdateByName,
                x.UpdateTime
            }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        # endregion

    }
}
