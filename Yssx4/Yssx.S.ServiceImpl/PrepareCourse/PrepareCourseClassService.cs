﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.Entity;
using Yssx.Framework.AutoMapper;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 课程班级服务 [Obsolete]
    /// </summary>
    public class PrepareCourseClassService : IPrepareCourseClassService
    {
        #region 获取教师班级
        /// <summary>
        /// 获取教师班级
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxTeacherClassViewModel>>> GetTeacherClassList(YssxTeacherClassQuery query, UserTicket user)
        {
            ResponseContext<List<YssxTeacherClassViewModel>> response = new ResponseContext<List<YssxTeacherClassViewModel>>();

            if (query == null) return response;

            if (!query.QuerySourceType.HasValue)
            {
                response.Msg = "数据源参数必传!";
                return response;
            }

            if (query.QuerySourceType.Value == 2 && !query.CourseId.HasValue)
            {
                response.Msg = "课程参数必传!";
                return response;
            }

            var list = new List<YssxTeacherClassViewModel>();

            var select = DbContext.FreeSql.GetRepository<YssxClass>().Where(x => x.IsDelete == CommonConstants.IsNotDelete);

            //院系、专业下班级
            if (query.QuerySourceType.Value == 0)
            {
                list = await select.From<YssxTeacherCollegeProfession>((a, b) =>
                    a.InnerJoin(x => x.TenantId == b.SchoolId && x.CollegeId == b.CollegeId && x.ProfessionId == b.ProfessionId))
                    .Where((a, b) => b.UserId == user.Id && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b) => new YssxTeacherClassViewModel
                    {
                        SchoolId = b.SchoolId,
                        SchoolName = b.SchoolName,
                        CollegeId = b.CollegeId,
                        CollegeName = b.CollegeName,
                        ProfessionId = b.ProfessionId,
                        ProfessionName = b.ProfessionName,
                        ClassId = a.Id,
                        ClassName = a.Name,
                    });
            }
            //学校下班级
            if (query.QuerySourceType.Value == 1)
            {
                list = await select.From<YssxSchool, YssxCollege, YssxProfession>((a, b, c, d) =>
                    a.InnerJoin(aa => aa.TenantId == b.Id)
                    .InnerJoin(aa => aa.TenantId == c.TenantId && aa.CollegeId == c.Id)
                    .InnerJoin(aa => aa.TenantId == d.TenantId && aa.ProfessionId == d.Id))
                    .Where((a, b, c, d) => b.Id == user.TenantId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete
                        && c.IsDelete == CommonConstants.IsNotDelete && d.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b, c, d) => new YssxTeacherClassViewModel
                    {
                        SchoolId = b.Id,
                        SchoolName = b.Name,
                        CollegeId = c.Id,
                        CollegeName = c.Name,
                        ProfessionId = d.Id,
                        ProfessionName = d.Name,
                        ClassId = a.Id,
                        ClassName = a.Name,
                    });
            }
            //已绑定课程班级
            if (query.QuerySourceType.Value == 2)
            {
                list = await DbContext.FreeSql.Select<YssxTeacherCourseClass>()
                     .Where(x => x.CourseId == query.CourseId && x.IsDelete == CommonConstants.IsNotDelete)
                     .ToListAsync(a => new YssxTeacherClassViewModel
                     {
                         CourseId = query.CourseId,
                         CourseClassId = a.Id,
                         SchoolId = a.SchoolId,
                         SchoolName = a.SchoolName,
                         CollegeId = a.CollegeId,
                         CollegeName = a.CollegeName,
                         ProfessionId = a.ProfessionId,
                         ProfessionName = a.ProfessionName,
                         ClassId = a.ClassId,
                         ClassName = a.ClassName,
                     });
            }

            if (list.Count > 0)
            {
                //班级Id列表
                List<long> classIds = list.Select(a => a.ClassId).ToList();

                //统计班级学生人数
                var classStudentCount = await DbContext.FreeSql.Select<YssxStudent>().Where(x => classIds.Contains(x.ClassId)
                    && x.IsDelete == CommonConstants.IsNotDelete).GroupBy(a => a.ClassId).ToListAsync(a => new { a.Key, Count = a.Count() });

                list.ForEach(x =>
                {
                    x.StudentCount = classStudentCount.FirstOrDefault(s => s.Key == x.ClassId)?.Count ?? 0;
                });
            }

            response.Data = list;

            return response;
        }
        #endregion

        #region 绑定课程班级(PC)
        /// <summary>
        /// 绑定课程班级(PC)
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BindingCourseClassForPC(PrepareCourseClassDto dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };
            if (!dto.CourseId.HasValue) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "课程不能为空!" };

            //获取教师信息
            YssxTeacher teacherInfo = await DbContext.FreeSql.GetRepository<YssxTeacher>().Where(x => x.UserId == user.Id).FirstAsync();
            if (teacherInfo == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "该用户不是教师!" };

            bool state = true;

            List<YssxTeacherCourseClass> alreadyList = await DbContext.FreeSql.GetRepository<YssxTeacherCourseClass>().Where(x => x.CourseId == dto.CourseId.Value
                && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //课程解绑所有班级
            if (dto.ClassList == null || dto.ClassList.Count <= 0)
            {
                if (alreadyList != null && alreadyList.Count > 0)
                {
                    alreadyList.ForEach(x =>
                    {
                        x.IsDelete = CommonConstants.IsDelete;
                        x.UpdateTime = DateTime.Now;
                        x.UpdateBy = user.Id;
                    });

                    state = DbContext.FreeSql.GetRepository<YssxTeacherCourseClass>().UpdateDiy.SetSource(alreadyList).UpdateColumns(x =>
                        new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;

                    return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
                }
                else
                {
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = "成功" };
                }
            }

            //课程绑定、解绑班级
            if (dto.ClassList != null && dto.ClassList.Count > 0)
            {
                //已存在数据集
                List<long> oldList = dto.ClassList.Where(x => x.CourseClassId.HasValue).Select(x => x.CourseClassId.Value).ToList();
                //需要新增数据集
                List<ClassInfo> needAdds = dto.ClassList.Where(x => !x.CourseClassId.HasValue).ToList();
                //需要删除数据集
                List<YssxTeacherCourseClass> deleteList = alreadyList.Where(x => !oldList.Contains(x.Id)).ToList();

                //新增数据集
                List<YssxTeacherCourseClass> addList = needAdds.MapTo<List<YssxTeacherCourseClass>>();
                if (addList != null && addList.Count > 0)
                {
                    addList.ForEach(x =>
                    {
                        x.Id = IdWorker.NextId();
                        x.SchoolId = user.TenantId;
                        x.TeacherId = user.Id;
                        x.CourseId = dto.CourseId.Value;
                        x.CreateBy = user.Id;
                        x.CreateTime = DateTime.Now;
                        x.UpdateBy = user.Id;
                        x.UpdateTime = DateTime.Now;
                    });

                    var resultList = await DbContext.FreeSql.GetRepository<YssxTeacherCourseClass>().InsertAsync(addList);
                    state = resultList != null;
                }
                if (deleteList.Count > 0)
                {
                    //删除数据集
                    deleteList.ForEach(x =>
                    {
                        x.IsDelete = CommonConstants.IsDelete;
                        x.UpdateTime = DateTime.Now;
                        x.UpdateBy = user.Id;
                    });
                    state = DbContext.FreeSql.GetRepository<YssxTeacherCourseClass>().UpdateDiy.SetSource(deleteList).UpdateColumns(x =>
                           new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
                }
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 绑定课程班级(App)
        /// <summary>
        /// 绑定课程班级(App)
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> BindingCourseClassForApp(PrepareCourseClassForAppDto dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "对象是空的!" };

            YssxTeacherCourseClass alreadyModel = await DbContext.FreeSql.GetRepository<YssxTeacherCourseClass>().
                Where(x => x.ClassId == dto.ClassId && x.CourseId == dto.CourseId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (alreadyModel != null)
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "该班级已绑定该门课程!" };

            YssxTeacherCourseClass entity = new YssxTeacherCourseClass
            {
                Id = IdWorker.NextId(),
                SchoolId = user.TenantId,
                SchoolName = dto.SchoolName,
                CollegeId = dto.CollegeId,
                CollegeName = dto.CollegeName,
                ProfessionId = dto.ProfessionId,
                ProfessionName = dto.ProfessionName,
                TeacherId = user.Id,
                CourseId = dto.CourseId.Value,
                ClassId = dto.ClassId,
                ClassName = dto.ClassName,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            YssxTeacherCourseClass courseClass = await DbContext.FreeSql.GetRepository<YssxTeacherCourseClass>().InsertAsync(entity);
            bool state = courseClass != null;

            return new ResponseContext<long> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = entity.Id, Msg = state ? "成功" : "操作失败!" };

        }
        #endregion

        #region 解绑单一课程班级
        /// <summary>
        /// 解绑单一课程班级
        /// </summary>
        /// <param name="courseClassId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UnlinkCourseClass(long courseClassId, UserTicket user)
        {
            YssxTeacherCourseClass entity = await DbContext.FreeSql.GetRepository<YssxTeacherCourseClass>().Where(x => x.Id == courseClassId).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<YssxTeacherCourseClass>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 获取学期班级列表(班级管理)
        /// <summary>
        /// 获取学期班级列表(班级管理)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxSemesterClassViewModel>>> GetSemesterClassList(UserTicket user)
        {
            ResponseContext<List<YssxSemesterClassViewModel>> response = new ResponseContext<List<YssxSemesterClassViewModel>>();

            //一个学期有多门课程 多门课程存在同一个班级
            var sourceList = await DbContext.FreeSql.Select<YssxTeacherCourseClass>().From<YssxPrepareCourse, YssxSemester>
                 ((a, b, c) =>
                 a.InnerJoin(x => x.CourseId == b.Id)
                 .InnerJoin(x => b.SemesterId == c.Id)
                 ).Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && b.CreateBy == user.Id)
                 .GroupBy((a, b, c) => new { c.Id, c.Name, a.ProfessionId, a.ProfessionName, a.ClassId, a.ClassName })
                 .ToListAsync(x => new
                 {
                     SemesterId = x.Key.Id,
                     SemesterName = x.Key.Name,
                     ProfessionId = x.Key.ProfessionId,
                     ProfessionName = x.Key.ProfessionName,
                     ClassId = x.Key.ClassId,
                     ClassName = x.Key.ClassName,
                 });

            //学期
            var semesterList = sourceList.Select(x => new { x.SemesterId, x.SemesterName }).Distinct().ToList();

            //班级
            var classList = sourceList.Select(x => x.ClassId).Distinct().ToList();
            //统计班级学生人数
            var classStudentCount = await DbContext.FreeSql.Select<YssxStudent>().Where(x => classList.Contains(x.ClassId)
                && x.IsDelete == CommonConstants.IsNotDelete).GroupBy(a => a.ClassId).ToListAsync(a => new { a.Key, Count = a.Count() });

            List<YssxSemesterClassViewModel> result = new List<YssxSemesterClassViewModel>();

            semesterList.ForEach(x =>
            {
                //班级个数
                int countClass = 0;

                List<StudentProfession> studentProfessionList = new List<StudentProfession>();
                //专业                
                sourceList.Where(a => a.SemesterId == x.SemesterId).Select(q => new { q.ProfessionId, q.ProfessionName }).Distinct().ToList().ForEach(w =>
                {
                    StudentProfession studentProfession = new StudentProfession();
                    studentProfession.ProfessionId = w.ProfessionId;
                    studentProfession.ProfessionName = w.ProfessionName;

                    List<StudentClass> studentClassList = new List<StudentClass>();
                    //班级
                    sourceList.Where(b => b.SemesterId == x.SemesterId && b.ProfessionId == w.ProfessionId).Select(d => new { d.ClassId, d.ClassName }).Distinct().ToList()
                    .ForEach(e =>
                    {
                        StudentClass studentClass = new StudentClass();
                        studentClass.ClassId = e.ClassId;
                        studentClass.ClassName = e.ClassName;
                        studentClass.StudentCount = classStudentCount.FirstOrDefault(s => s.Key == e.ClassId)?.Count ?? 0;

                        studentClassList.Add(studentClass);
                    });

                    studentProfession.StudentClassList = studentClassList;

                    countClass += studentClassList.Count;

                    studentProfessionList.Add(studentProfession);
                });

                YssxSemesterClassViewModel model = new YssxSemesterClassViewModel
                {
                    SemesterId = x.SemesterId,
                    SemesterName = x.SemesterName,
                    ClassCount = countClass,
                    StudentProfessionList = studentProfessionList,
                };

                result.Add(model);
            });

            response.Data = result;

            return response;
        }
        #endregion

        #region 根据班级获取学生信息
        /// <summary>
        /// 根据班级获取学生信息
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxCourseClassStudentViewModel>>> GetStudentListByClass(YssxClassStudentQuery query)
        {
            ResponseContext<List<YssxCourseClassStudentViewModel>> response = new ResponseContext<List<YssxCourseClassStudentViewModel>>();

            if (query == null) return response;

            if (!query.ClassId.HasValue)
            {
                response.Msg = "班级参数必传!";
                return response;
            }

            var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxCollege, YssxProfession, YssxClass>((a, b, c, d) =>
                a.InnerJoin(x => x.CollegeId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                .InnerJoin(x => x.ProfessionId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
                .InnerJoin(x => x.ClassId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
                ).Where((a, b, c, d) => a.IsDelete == CommonConstants.IsNotDelete && a.ClassId == query.ClassId);

            if (!string.IsNullOrWhiteSpace(query.Keyword))
                select.Where((a, b, c, d) => a.Name.Contains(query.Keyword) || a.StudentNo.Contains(query.Keyword));

            response.Data = await select.ToListAsync((a, b, c, d) => new YssxCourseClassStudentViewModel
            {
                StudentUserId = a.UserId,
                Name = a.Name,
                WorkNumber = a.StudentNo,
                CollegeId = b.Id,
                CollegeName = b.Name,
                ProfessionId = c.Id,
                ProfessionName = c.Name,
                ClassId = d.Id,
                ClassName = d.Name
            });

            return response;
        }
        #endregion

        #region 根据教师课程绑定班级获取学生信息
        /// <summary>
        /// 根据教师课程绑定班级获取学生信息
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxCourseClassStudentViewModel>>> GetStudentListByTeacherCourseClass(YssxCourseClassStudentQuery query, UserTicket user)
        {
            ResponseContext<List<YssxCourseClassStudentViewModel>> response = new ResponseContext<List<YssxCourseClassStudentViewModel>>();

            var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxTeacherCourseClass, YssxPrepareCourse>(
                (a, b, c) =>
                a.InnerJoin(x => x.ClassId == b.ClassId)
                .InnerJoin(x => b.CourseId == c.Id)
                ).Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete
                    && c.CreateBy == user.Id && c.IsDelete == CommonConstants.IsNotDelete);

            if (query != null && !string.IsNullOrWhiteSpace(query.Keyword))
                select.Where((a, b, c) => a.Name.Contains(query.Keyword) || a.StudentNo.Contains(query.Keyword) || b.ClassName.Contains(query.Keyword));

            response.Data = await select.OrderByDescending((a, b, c) => a.ClassId).Distinct().ToListAsync((a, b, c) => new YssxCourseClassStudentViewModel
            {
                StudentUserId = a.UserId,
                Name = a.Name,
                WorkNumber = a.StudentNo,
                CollegeId = a.CollegeId,
                CollegeName = a.CollegeName,
                ProfessionId = a.ProfessionId,
                ProfessionName = a.ProfessionName,
                ClassId = a.ClassId,
                ClassName = b.ClassName
            });

            return response;
        }
        #endregion

        #region 根据某一个课程获取绑定班级的学生实体
        /// <summary>
        /// 根据某一个课程获取绑定班级的学生实体
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxClassStudentByCourseViewModel>>> GetStudentListByCourse(YssxCourseClassStudentByCourseQuery query, UserTicket user)
        {
            ResponseContext<List<YssxClassStudentByCourseViewModel>> response = new ResponseContext<List<YssxClassStudentByCourseViewModel>>();

            if (query == null || !query.CourseId.HasValue)
                return response;

            var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxTeacherCourseClass, YssxPrepareCourse>(
                (a, b, c) =>
                a.InnerJoin(x => x.ClassId == b.ClassId)
                .InnerJoin(x => b.CourseId == c.Id)
                ).Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete
                    && c.Id == query.CourseId && c.IsDelete == CommonConstants.IsNotDelete);

            var sourceList = await select.OrderByDescending((a, b, c) => a.ClassId).Distinct().ToListAsync((a, b, c) => new YssxCourseClassStudentViewModel
            {
                StudentUserId = a.UserId,
                Name = a.Name,
                WorkNumber = a.StudentNo,
                CollegeId = a.CollegeId,
                CollegeName = a.CollegeName,
                ProfessionId = a.ProfessionId,
                ProfessionName = a.ProfessionName,
                ClassId = a.ClassId,
                ClassName = a.ClassName
            });

            if (sourceList == null || sourceList.Count == 0)
                return response;

            //班级列表
            var classList = sourceList.Select(x => new { x.CollegeId, x.CollegeName, x.ProfessionId, x.ProfessionName, x.ClassId, x.ClassName }).Distinct().ToList();

            List<YssxClassStudentByCourseViewModel> list = new List<YssxClassStudentByCourseViewModel>();

            classList.ForEach(x =>
            {
                //某一个班级学生列表
                var studentList = sourceList.Where(a => a.ClassId == x.ClassId).ToList();
                YssxClassStudentByCourseViewModel model = new YssxClassStudentByCourseViewModel();
                model.CollegeId = x.CollegeId;
                model.CollegeName = x.CollegeName;
                model.ProfessionId = x.ProfessionId;
                model.ProfessionName = x.ProfessionName;
                model.ClassId = x.ClassId;
                model.ClassName = x.ClassName;
                model.StudentList = studentList.MapTo<List<StudentData>>();

                list.Add(model);
            });

            response.Data = list;

            return response;
        }
        #endregion

        #region 获取教师绑定课程班级列表
        /// <summary>
        /// 获取教师绑定课程班级列表
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxTeacherClassViewModel>>> GetTeacherBindingCourseClassList(UserTicket user)
        {
            ResponseContext<List<YssxTeacherClassViewModel>> response = new ResponseContext<List<YssxTeacherClassViewModel>>();

            var list = await DbContext.FreeSql.Select<YssxTeacherCourseClass>().From<YssxPrepareCourse>
                 ((a, b) =>
                 a.InnerJoin(x => x.CourseId == b.Id)
                 ).Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && b.CreateBy == user.Id)
                 .GroupBy((a, b) => new { a.SchoolId, a.SchoolName, a.CollegeId, a.CollegeName, a.ProfessionId, a.ProfessionName, a.ClassId, a.ClassName })
                 .ToListAsync(x => new YssxTeacherClassViewModel
                 {
                     SchoolId = x.Key.SchoolId,
                     SchoolName = x.Key.SchoolName,
                     CollegeId = x.Key.CollegeId,
                     CollegeName = x.Key.CollegeName,
                     ProfessionId = x.Key.ProfessionId,
                     ProfessionName = x.Key.ProfessionName,
                     ClassId = x.Key.ClassId,
                     ClassName = x.Key.ClassName,
                 });

            response.Data = list;

            return response;
        }
        #endregion

    }
}
