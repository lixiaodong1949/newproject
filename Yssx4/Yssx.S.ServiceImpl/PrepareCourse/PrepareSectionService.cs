﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.Entity;
using Yssx.Framework.AutoMapper;
using Yssx.Repository.Extensions;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 备课课程章节 [Obsolete]
    /// </summary>
    public class PrepareSectionService : IPrepareSectionService
    {
        #region 新增/编辑备课课程章节
        /// <summary>
        /// 新增/编辑备课课程章节
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditPrepareSection(PrepareSectionDto dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxPrepareSection entity = new YssxPrepareSection
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                SectionName = dto.SectionName,
                SectionTitle = dto.SectionTitle,
                CourseId = dto.CourseId,
                ParentId = dto.ParentId,
                SectionType = dto.SectionType,
                KnowledgePointId = dto.KnowledgePointId,
                Sort = dto.Sort,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            //取缓存中课程章节
            List<YssxPrepareSection> cacheList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetPrepareCourseSection}{dto.CourseId}", () =>
                new List<YssxPrepareSection>(), 10 * 60, true, false);

            if (dto.Id <= 0)
            {
                YssxPrepareSection sectionModel = await DbContext.FreeSql.GetRepository<YssxPrepareSection>().InsertAsync(entity);
                state = sectionModel != null;
                if ((dto.SectionType == (int)SectionType.Chapter) && state)
                {
                    //添加课程章节信息统计
                    await AddPrepareSectionSummary(sectionModel.CourseId, (int)SectionSummaryType.Section);
                }
                if (state)
                    //将新增数据添加到缓存列表
                    cacheList.Add(sectionModel);
            }
            else
            {
                state = DbContext.FreeSql.GetRepository<YssxPrepareSection>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                { x.SectionName, x.SectionTitle, x.Sort, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;

                if (state)
                {
                    //取缓存中 要更新的数据
                    YssxPrepareSection cacheModel = cacheList.SingleOrDefault(x => x.Id == entity.Id);
                    if (cacheModel != null)
                    {
                        cacheList.SingleOrDefault(x => x.Id == entity.Id).SectionName = entity.SectionName;
                        cacheList.SingleOrDefault(x => x.Id == entity.Id).SectionTitle = entity.SectionTitle;
                        cacheList.SingleOrDefault(x => x.Id == entity.Id).Sort = entity.Sort;
                        cacheList.SingleOrDefault(x => x.Id == entity.Id).UpdateBy = entity.UpdateBy;
                        cacheList.SingleOrDefault(x => x.Id == entity.Id).UpdateTime = entity.UpdateTime;
                    }
                }
            }

            //将列表更新到缓存
            FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetPrepareCourseSection}{dto.CourseId}", cacheList, 10 * 60, true);

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 获取备课课程章节列表
        /// <summary>
        /// 获取备课课程章节列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxPrepareSectionViewModel>>> GetPrepareSectionList(YssxPrepareSectionQuery query)
        {
            ResponseContext<List<YssxPrepareSectionViewModel>> response = new ResponseContext<List<YssxPrepareSectionViewModel>>();

            if (query == null || !query.CourseId.HasValue)
                return response;

            List<YssxPrepareSection> list = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetPrepareCourseSection}{query.CourseId}", () =>
                DbContext.FreeSql.GetRepository<YssxPrepareSection>()
                .Where(x => x.CourseId == query.CourseId.Value && x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.Sort).ToList(), 10 * 60, true, false);

            if (list == null || list.Count == 0)
                return response;

            if (query.ParentId.HasValue)
                list = list.Where(x => x.ParentId == query.ParentId.Value).ToList();
            if (query.SectionType.HasValue)
                list = list.Where(x => x.SectionType == query.SectionType.Value).ToList();

            if (list != null && list.Count > 0)
            {
                var items = list.MapTo<List<YssxPrepareSectionViewModel>>();

                var topicList = await DbContext.FreeSql.GetRepository<YssxPrepareSectionTopic>()
                        .Where(x => x.CourseId == query.CourseId.Value && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                var sceneList = await DbContext.FreeSql.GetRepository<YssxPrepareSectionSceneTraining>()
                        .Where(x => x.CourseId == query.CourseId.Value && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                var textList = await DbContext.FreeSql.GetRepository<YssxPrepareSectionTextBook>()
                        .Where(x => x.CourseId == query.CourseId.Value && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                items.ForEach(x =>
                {
                    if (x.SectionType == (int)SectionType.Section)
                    {
                        //章节习题
                        int topicCount = topicList.Where(a => a.SectionId == x.Id).Count();
                        if (topicCount > 0)
                        {
                            YssxSectionSummaryDto tCountModel = new YssxSectionSummaryDto
                            {
                                Count = topicCount,
                                CourseId = x.CourseId,
                                SummaryType = (int)SectionSummaryType.Exercises,
                            };
                            x.SummaryListViewModel.Add(tCountModel);
                        }

                        //场景实训
                        int sceneCount = sceneList.Where(a => a.SectionId == x.Id).Count();
                        if (sceneCount > 0)
                        {
                            YssxSectionSummaryDto sCountModel = new YssxSectionSummaryDto
                            {
                                Count = sceneCount,
                                CourseId = x.CourseId,
                                SummaryType = (int)SectionSummaryType.SectionSceneTraining,
                            };
                            x.SummaryListViewModel.Add(sCountModel);
                        }

                        //教材
                        int textCount = textList.Where(a => a.SectionId == x.Id).Count();
                        if (textCount > 0)
                        {
                            YssxSectionSummaryDto tCountModel = new YssxSectionSummaryDto
                            {
                                Count = textCount,
                                CourseId = x.CourseId,
                                SummaryType = (int)SectionSummaryType.TextBook,
                            };
                            x.SummaryListViewModel.Add(tCountModel);
                        }
                    }
                });

                response.Data = items;
            }

            return response;
        }
        #endregion

        #region 删除备课课程章节
        /// <summary>
        /// 删除备课课程章节
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemovePrepareSection(long id, UserTicket user)
        {
            //如果删除章 则同时删除章下面的课件/视频、章下面的节、节下面的教材/习题/场景实训

            YssxPrepareSection entity = await DbContext.FreeSql.GetRepository<YssxPrepareSection>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<YssxPrepareSection>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;

            if (state)
            {
                //取缓存中课程章节
                List<YssxPrepareSection> cacheList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetPrepareCourseSection}{entity.CourseId}", () =>
                    new List<YssxPrepareSection>(), 10 * 60, true, false);

                //删除缓存列表中数据
                YssxPrepareSection cacheModel = cacheList.SingleOrDefault(x => x.Id == id);
                if (cacheModel != null)                
                    cacheList.Remove(cacheModel);

                //将列表更新到缓存
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetPrepareCourseSection}{entity.CourseId}", cacheList, 10 * 60, true);

                if (entity.SectionType == (int)SectionType.Chapter)
                {
                    //删除章节信息统计
                    await RemovPrepareSectionSummary(entity.CourseId, (int)SectionSummaryType.Section);
                    //删除章节下的课件、视频
                    await RemovePrepareSectionFilesBySection(entity.CourseId, entity.Id, user);

                    //删除章下的节列表
                    await RemoveSectionByRemoveChapter(entity.CourseId, entity.Id, user);
                }
                else
                {
                    //删除备课课程章节习题
                    await RemovePrepareSectionTopicBySection(entity.CourseId, entity.Id, user);

                    //删除备课课程章节场景实训
                    await RemovePrepareSectionSceneTrainingBySection(entity.CourseId, entity.Id, user);

                    //删除备课课程章节教材
                    await RemovePrepareSectionTextBookBySection(entity.CourseId, entity.Id, user);
                }
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 删除备课课程本章所有小节
        /// <summary>
        /// 删除备课课程本章所有小节
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemovePrepareAllSectionOfChapter(long id, UserTicket user)
        {
            YssxPrepareSection entity = await DbContext.FreeSql.GetRepository<YssxPrepareSection>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            List<YssxPrepareSection> list = await DbContext.FreeSql.GetRepository<YssxPrepareSection>()
                .Where(x => x.CourseId == entity.CourseId && x.ParentId == id && x.SectionType == (int)SectionType.Section && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "本章没有小节!" };

            //删除章下的节列表
            bool state = await RemoveSectionByRemoveChapter(entity.CourseId, entity.Id, user);

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 删除节列表(根据删除某一章)
        /// <summary>
        /// 删除节列表(根据删除某一章)
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<bool> RemoveSectionByRemoveChapter(long courseId, long sectionId, UserTicket user)
        {
            List<YssxPrepareSection> list = await DbContext.FreeSql.GetRepository<YssxPrepareSection>()
                .Where(x => x.CourseId == courseId && x.ParentId == sectionId && x.SectionType == (int)SectionType.Section && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            bool state = false;

            if (list != null && list.Count > 0)
            {
                list.ForEach(x =>
                {
                    x.IsDelete = 1;
                    x.UpdateTime = DateTime.Now;
                    x.UpdateBy = user.Id;
                });

                //批量删除节列表
                state = DbContext.FreeSql.GetRepository<YssxPrepareSection>().UpdateDiy.SetSource(list).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
                if (state)
                {
                    //取缓存中课程章节
                    List<YssxPrepareSection> cacheList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetPrepareCourseSection}{courseId}", () =>
                        new List<YssxPrepareSection>(), 10 * 60, true, false);

                    List<long> cacheIds = list.Select(x => x.Id).ToList();
                    var removeList = cacheList.Where(x => cacheIds.Contains(x.Id)).ToList();

                    //删除缓存中数据
                    cacheList.RemoveAll(it => removeList.Contains(it));

                    //将列表更新到缓存
                    FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetPrepareCourseSection}{courseId}", cacheList, 10 * 60, true);

                    foreach (var item in list)
                    {
                        //删除备课课程章节习题(删除章节时触发)
                        await RemovePrepareSectionTopicBySection(courseId, item.Id, user);

                        //删除备课课程章节场景实训(删除章节时触发)
                        await RemovePrepareSectionSceneTrainingBySection(courseId, item.Id, user);

                        //删除备课课程章节教材(删除章节时触发)
                        await RemovePrepareSectionTextBookBySection(courseId, item.Id, user);
                    }
                }
            }

            return state;
        }
        #endregion

        #region 新增备课课程节习题
        /// <summary>
        /// 新增备课课程节习题
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddPrepareSectionTopic(PrepareSectionTopicDto dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            bool state = false;

            YssxPrepareSectionTopic entity = dto.MapTo<YssxPrepareSectionTopic>();
            if (entity != null)
            {
                entity.Id = IdWorker.NextId();
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;
                entity.UpdateBy = user.Id;
                entity.UpdateTime = DateTime.Now;

                YssxPrepareSectionTopic sectionTopic = await DbContext.FreeSql.GetRepository<YssxPrepareSectionTopic>().InsertAsync(entity);
                state = sectionTopic != null;

                //添加课程章节信息统计
                await AddPrepareSectionSummary(sectionTopic.CourseId, (int)SectionSummaryType.Exercises);
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 获取备课课程节习题列表
        /// <summary>
        /// 获取备课课程节习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxPrepareSectionTopicViewModel>>> GetPrepareSectionTopicList(YssxPrepareInfoQuery query)
        {
            ResponseContext<List<YssxPrepareSectionTopicViewModel>> response = new ResponseContext<List<YssxPrepareSectionTopicViewModel>>();

            if (query == null || !query.CourseId.HasValue)
                return response;

            FreeSql.ISelect<YssxPrepareSectionTopic> select = DbContext.FreeSql.GetRepository<YssxPrepareSectionTopic>()
                .Where(x => x.IsDelete == CommonConstants.IsNotDelete);

            if (query.CourseId.HasValue)
                select.Where(x => x.CourseId == query.CourseId.Value);
            if (query.SectionId.HasValue)
                select.Where(x => x.SectionId == query.SectionId.Value);

            response.Data = await select.From<YssxTopicPublic>((a, b) => a.InnerJoin(aa => aa.QuestionId == b.Id)).OrderBy((a, b) => b.Sort).ToListAsync((a, b) => new YssxPrepareSectionTopicViewModel
            {
                Id = a.Id,
                CourseId = a.CourseId,
                SectionId = a.SectionId,
                QuestionId = a.QuestionId,
                QuestionName = b.Title,
                QuestionType = b.QuestionType,
                KnowledgePointId = b.KnowledgePointIds,
                Sort = a.Sort,
                QuestionContent = b.Content,
                Score = b.Score,
                QuestionSort = b.Sort,
            });
            return response;
        }
        #endregion

        #region 删除备课课程节习题
        /// <summary>
        /// 删除备课课程节习题
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemovePrepareSectionTopic(long id, UserTicket user)
        {
            YssxPrepareSectionTopic entity = await DbContext.FreeSql.GetRepository<YssxPrepareSectionTopic>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<YssxPrepareSectionTopic>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
            await RemovPrepareSectionSummary(entity.CourseId, (int)SectionSummaryType.Exercises);
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 删除备课课程章节习题(删除章节时触发)
        /// <summary>
        /// 删除备课课程章节习题(删除章节时触发)
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<bool> RemovePrepareSectionTopicBySection(long courseId, long sectionId, UserTicket user)
        {
            List<YssxPrepareSectionTopic> list = await DbContext.FreeSql.GetRepository<YssxPrepareSectionTopic>()
                .Where(x => x.CourseId == courseId && x.SectionId == sectionId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            bool state = false;

            if (list != null && list.Count > 0)
            {
                list.ForEach(x =>
                {
                    x.IsDelete = 1;
                    x.UpdateTime = DateTime.Now;
                    x.UpdateBy = user.Id;
                });
                //批量删除章节习题
                state = DbContext.FreeSql.GetRepository<YssxPrepareSectionTopic>().UpdateDiy.SetSource(list).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
                await RemovPrepareSectionSummary(courseId, (int)SectionSummaryType.Exercises, list.Count);
            }

            return state;
        }
        #endregion

        #region 新增备课课程节场景实训
        /// <summary>
        /// 新增备课课程节场景实训
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddPrepareSectionSceneTraining(PrepareSectionSceneTrainingDto dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            bool state = false;

            YssxPrepareSectionSceneTraining entity = dto.MapTo<YssxPrepareSectionSceneTraining>();
            if (entity != null)
            {
                entity.Id = IdWorker.NextId();
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;
                entity.UpdateBy = user.Id;
                entity.UpdateTime = DateTime.Now;

                YssxPrepareSectionSceneTraining sectionSTraining = await DbContext.FreeSql.GetRepository<YssxPrepareSectionSceneTraining>().InsertAsync(entity);
                state = sectionSTraining != null;

                //添加课程章节信息统计
                await AddPrepareSectionSummary(sectionSTraining.CourseId, (int)SectionSummaryType.SectionSceneTraining);
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 获取备课课程节场景实训列表
        /// <summary>
        /// 获取备课课程节场景实训列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxPrepareSectionSceneTrainingViewModel>>> GetPrepareSectionSceneTrainingList(YssxPrepareInfoQuery query)
        {
            ResponseContext<List<YssxPrepareSectionSceneTrainingViewModel>> response = new ResponseContext<List<YssxPrepareSectionSceneTrainingViewModel>>();

            if (query == null || !query.CourseId.HasValue)
                return response;

            var select = DbContext.FreeSql.GetRepository<YssxPrepareSectionSceneTraining>()
                .Where(x => x.IsDelete == CommonConstants.IsNotDelete);

            if (query.CourseId.HasValue)
                select.Where(x => x.CourseId == query.CourseId.Value);
            if (query.SectionId.HasValue)
                select.Where(x => x.SectionId == query.SectionId.Value);

            response.Data = await select.From<YssxSceneTraining>((a, b) => a.InnerJoin(aa => aa.SceneTrainingId == b.Id)).ToListAsync((a, b) => new YssxPrepareSectionSceneTrainingViewModel
            {
                Id = a.Id,
                CourseId = a.CourseId,
                SectionId = a.SectionId,
                SceneTrainingId = a.SceneTrainingId,
                SceneTrainingName = b.Name,
                KnowledgePointId = a.KnowledgePointId,
                Sort = a.Sort,
                DataSourceType = a.DataSourceType,
                EffectiveDate = a.EffectiveDate,
            });

            return response;
        }
        #endregion

        #region 删除备课课程节场景实训
        /// <summary>
        /// 删除备课课程节场景实训
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemovePrepareSectionSceneTraining(long id, UserTicket user)
        {
            YssxPrepareSectionSceneTraining entity = await DbContext.FreeSql.GetRepository<YssxPrepareSectionSceneTraining>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<YssxPrepareSectionSceneTraining>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
            await RemovPrepareSectionSummary(entity.CourseId, (int)SectionSummaryType.SectionSceneTraining);
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 删除备课课程章节场景实训(删除章节时触发)
        /// <summary>
        /// 删除备课课程章节场景实训(删除章节时触发)
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<bool> RemovePrepareSectionSceneTrainingBySection(long courseId, long sectionId, UserTicket user)
        {

            List<YssxPrepareSectionSceneTraining> list = await DbContext.FreeSql.GetRepository<YssxPrepareSectionSceneTraining>()
                .Where(x => x.CourseId == courseId && x.SectionId == sectionId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            bool state = false;

            if (list != null && list.Count > 0)
            {
                list.ForEach(x =>
                {
                    x.IsDelete = 1;
                    x.UpdateTime = DateTime.Now;
                    x.UpdateBy = user.Id;
                });
                //批量删除场景实训
                state = DbContext.FreeSql.GetRepository<YssxPrepareSectionSceneTraining>().UpdateDiy.SetSource(list).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
                await RemovPrepareSectionSummary(courseId, (int)SectionSummaryType.SectionSceneTraining, list.Count);
            }

            return state;
        }
        #endregion

        #region 新增/编辑备课课程节教材
        /// <summary>
        /// 新增/编辑备课课程节教材
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditPrepareSectionTextBook(PrepareSectionTextBookDto dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            bool state = false;

            YssxPrepareSectionTextBook entity = dto.MapTo<YssxPrepareSectionTextBook>();
            if (entity != null)
            {
                entity.Id = dto.Id > 0 ? dto.Id : IdWorker.NextId();
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;
                entity.UpdateBy = user.Id;
                entity.UpdateTime = DateTime.Now;

                //取缓存中课程教材详情
                List<YssxPrepareSectionTextBook> cacheList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetPrepareCourseSectionTextBook}{dto.CourseId}", () =>
                    new List<YssxPrepareSectionTextBook>(), 10 * 60, true, false);

                if (dto.Id <= 0)
                {
                    YssxPrepareSectionTextBook sectionTextBook = await DbContext.FreeSql.GetRepository<YssxPrepareSectionTextBook>().InsertAsync(entity);
                    state = sectionTextBook != null;

                    if (state)
                    {
                        //添加课程章节信息统计
                        await AddPrepareSectionSummary(sectionTextBook.CourseId, (int)SectionSummaryType.TextBook);

                        //将新增数据添加到缓存列表
                        cacheList.Add(sectionTextBook);
                    }
                }
                else
                {
                    state = DbContext.FreeSql.GetRepository<YssxPrepareSectionTextBook>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                    { x.Content, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
                    
                    if (state)
                    {
                        //取缓存中 要更新的数据
                        YssxPrepareSectionTextBook cacheModel = cacheList.SingleOrDefault(x => x.Id == entity.Id);
                        if (cacheModel != null)
                        {
                            cacheList.SingleOrDefault(x => x.Id == entity.Id).Content = entity.Content;
                            cacheList.SingleOrDefault(x => x.Id == entity.Id).UpdateBy = entity.UpdateBy;
                            cacheList.SingleOrDefault(x => x.Id == entity.Id).UpdateTime = entity.UpdateTime;
                        }
                    }
                }

                //将列表更新到缓存
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetPrepareCourseSectionTextBook}{dto.CourseId}", cacheList, 10 * 60, true);
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 获取备课课程某一节教材详情
        /// <summary>
        /// 获取备课课程某一节教材详情
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxPrepareSectionTextBookViewModel>>> GetPrepareSectionTextBookDetail(YssxPrepareInfoQuery query)
        {
            ResponseContext<List<YssxPrepareSectionTextBookViewModel>> response = new ResponseContext<List<YssxPrepareSectionTextBookViewModel>>();
            if (query == null)
                return response;
            if (!query.CourseId.HasValue)
            {
                response.Msg = "课程参数必传!";
                return response;
            }
            if (!query.SectionId.HasValue)
            {
                response.Msg = "章节参数必传!";
                return response;
            }

            var sourceData = await DbContext.FreeSql.GetRepository<YssxPrepareSectionTextBook>()
               .Where(x => x.CourseId == query.CourseId.Value && x.IsDelete == CommonConstants.IsNotDelete)
               .ToListAsync($"{CommonConstants.Cache_GetPrepareCourseSectionTextBook}{query.CourseId}", true, 10 * 60, true, false);

            List<YssxPrepareSectionTextBook> textBook = sourceData.Where(x => x.SectionId == query.SectionId.Value).ToList();

            if (textBook != null && textBook.Count > 0)
                response.Data = textBook.MapTo<List<YssxPrepareSectionTextBookViewModel>>();

            return response;
        }
        #endregion

        #region 删除备课课程节教材
        /// <summary>
        /// 删除备课课程节教材
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemovePrepareSectionTextBook(long id, UserTicket user)
        {
            YssxPrepareSectionTextBook entity = await DbContext.FreeSql.GetRepository<YssxPrepareSectionTextBook>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<YssxPrepareSectionTextBook>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
            if (state)
            {
                await RemovPrepareSectionSummary(entity.CourseId, (int)SectionSummaryType.TextBook);
                //取缓存中课程教材详情
                List<YssxPrepareSectionTextBook> cacheList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetPrepareCourseSectionTextBook}{entity.CourseId}", () =>
                 new List<YssxPrepareSectionTextBook>(), 10 * 60, true, false);

                //删除缓存列表中数据
                YssxPrepareSectionTextBook cacheModel = cacheList.SingleOrDefault(x => x.Id == id);
                if (cacheModel != null)                
                    cacheList.Remove(cacheModel);                

                //将列表更新到缓存
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetPrepareCourseSectionTextBook}{entity.CourseId}", cacheList, 10 * 60, true);
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 删除备课课程章节教材(删除章节时触发)
        /// <summary>
        /// 删除备课课程章节教材(删除章节时触发)
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<bool> RemovePrepareSectionTextBookBySection(long courseId, long sectionId, UserTicket user)
        {
            List<YssxPrepareSectionTextBook> list = await DbContext.FreeSql.GetRepository<YssxPrepareSectionTextBook>()
                .Where(x => x.CourseId == courseId && x.SectionId == sectionId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            bool state = false;

            if (list != null && list.Count > 0)
            {
                list.ForEach(x =>
                {
                    x.IsDelete = 1;
                    x.UpdateTime = DateTime.Now;
                    x.UpdateBy = user.Id;
                });

                //批量删除教材
                state = DbContext.FreeSql.GetRepository<YssxPrepareSectionTextBook>().UpdateDiy.SetSource(list).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
                if (state)
                {
                    await RemovPrepareSectionSummary(courseId, (int)SectionSummaryType.TextBook, list.Count);
                    //取缓存中课程教材详情
                    List<YssxPrepareSectionTextBook> cacheList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetPrepareCourseSectionTextBook}{courseId}", () =>
                     new List<YssxPrepareSectionTextBook>(), 10 * 60, true, false);

                    List<long> cacheIds = list.Select(x => x.Id).ToList();
                    var removeList = cacheList.Where(x => cacheIds.Contains(x.Id)).ToList();

                    //删除缓存中数据
                    cacheList.RemoveAll(it => removeList.Contains(it));

                    //将列表更新到缓存
                    FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetPrepareCourseSectionTextBook}{courseId}", cacheList, 10 * 60, true);
                }
            }

            return state;
        }
        #endregion

        #region 新增备课课程课件、视频、教案、授课计划
        /// <summary>
        /// 新增备课课程课件、视频、教案、授课计划
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddPrepareSectionFiles(PrepareSectionFilesDto dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            bool state = false;

            YssxPrepareSectionFiles entity = dto.MapTo<YssxPrepareSectionFiles>();
            if (entity != null)
            {
                entity.Id = IdWorker.NextId();
                entity.CreateBy = user.Id;
                entity.CreateTime = DateTime.Now;
                entity.UpdateBy = user.Id;
                entity.UpdateTime = DateTime.Now;

                YssxPrepareSectionFiles sectionFiles = await DbContext.FreeSql.GetRepository<YssxPrepareSectionFiles>().InsertAsync(entity);
                state = sectionFiles != null;

                //添加课程章节信息统计
                if (dto.SectionType == 1)
                    await AddPrepareSectionSummary(sectionFiles.CourseId, (int)SectionSummaryType.Files);
                if (dto.SectionType == 2)
                    await AddPrepareSectionSummary(sectionFiles.CourseId, (int)SectionSummaryType.LessonPlan);
                if (dto.SectionType == 3)
                    await AddPrepareSectionSummary(sectionFiles.CourseId, (int)SectionSummaryType.Video);
                if (dto.SectionType == 4)
                    await AddPrepareSectionSummary(sectionFiles.CourseId, (int)SectionSummaryType.TeachingPlan);
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 获取备课课程课件、视频、教案、授课计划列表(App)
        /// <summary>
        /// 获取备课课程课件、视频、教案、授课计划列表(App)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxPrepareSectionFilesViewModel>>> GetPrepareSectionFilesListForApp(YssxPrepareSectionFilesQuery query)
        {
            ResponseContext<List<YssxPrepareSectionFilesViewModel>> response = new ResponseContext<List<YssxPrepareSectionFilesViewModel>>();

            if (query == null)
                return response;
            if (!query.CourseId.HasValue)
            {
                response.Msg = "课程参数必传!";
                return response;
            }

            if (!query.QuerySourceType.HasValue)
            {
                response.Msg = "数据源参数必传!";
                return response;
            }

            if (query.QuerySourceType.Value == 2 && !query.SectionId.HasValue)
            {
                response.Msg = "章节参数必传!";
                return response;
            }

            var select = DbContext.FreeSql.GetRepository<YssxPrepareSectionFiles>().Where(x => x.CourseId == query.CourseId.Value
                && x.IsDelete == CommonConstants.IsNotDelete);

            if (!string.IsNullOrEmpty(query.FileName))
                select.Where(x => x.FileName.Contains(query.FileName));

            //课程下所有
            if (query.QuerySourceType.Value == 0)
                query.SectionId = null;

            //课程本身所有
            if (query.QuerySourceType.Value == 1)
                query.SectionId = 0;

            if (query.SectionId.HasValue)
                select.Where(x => x.SectionId == query.SectionId.Value);
            if (query.SectionType.HasValue)
                select.Where(x => x.SectionType == query.SectionType.Value);

            response.Data = await select.From<YssxCourceFiles, YssxPrepareSection>((a, b, c) =>
             a.InnerJoin(aa => aa.CourseFileId == b.Id)
             .LeftJoin(aa => aa.SectionId == c.Id))
                .ToListAsync((a, b, c) => new YssxPrepareSectionFilesViewModel
                {
                    Id = a.Id,
                    CourseId = a.CourseId,
                    SectionId = a.SectionId,
                    SectionName = c.SectionName,
                    SectionTitle = c.SectionTitle,
                    CourseFileId = a.CourseFileId,
                    File = b.File,
                    FileName = b.FileName,
                    KnowledgePointId = b.KnowledgePointId,
                    FilesType = b.FilesType,
                    SectionType = a.SectionType,
                    Sort = a.Sort,
                    DataSourceType = a.DataSourceType,
                    EffectiveDate = a.EffectiveDate,
                });

            return response;
        }
        #endregion

        #region 获取备课课程课件、视频、教案、授课计划列表(PC)
        /// <summary>
        /// 获取备课课程课件、视频、教案、授课计划列表(PC)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<YssxPrepareSectionFilesForPCViewModel>> GetPrepareSectionFilesListForPC(YssxPrepareSectionFilesQuery query)
        {
            ResponseContext<YssxPrepareSectionFilesForPCViewModel> response = new ResponseContext<YssxPrepareSectionFilesForPCViewModel>();

            if (query == null)
                return response;
            if (!query.CourseId.HasValue)
            {
                response.Msg = "课程参数必传!";
                return response;
            }

            if (!query.QuerySourceType.HasValue)
            {
                response.Msg = "数据源参数必传!";
                return response;
            }

            if (query.QuerySourceType.Value == 2 && !query.SectionId.HasValue)
            {
                response.Msg = "章节参数必传!";
                return response;
            }

            var select = DbContext.FreeSql.GetRepository<YssxPrepareSectionFiles>().Where(x => x.CourseId == query.CourseId.Value
                && x.IsDelete == CommonConstants.IsNotDelete);

            if (!string.IsNullOrEmpty(query.FileName))
                select.Where(x => x.FileName.Contains(query.FileName));

            //课程下所有
            if (query.QuerySourceType.Value == 0)
                query.SectionId = null;

            //课程本身所有
            if (query.QuerySourceType.Value == 1)
                query.SectionId = 0;

            if (query.SectionId.HasValue)
                select.Where(x => x.SectionId == query.SectionId.Value);
            if (query.SectionType.HasValue)
                select.Where(x => x.SectionType == query.SectionType.Value);

            var list = await select.From<YssxCourceFiles, YssxPrepareSection>((a, b, c) =>
              a.InnerJoin(aa => aa.CourseFileId == b.Id)
              .LeftJoin(aa => aa.SectionId == c.Id))
                .ToListAsync((a, b, c) => new YssxPrepareSectionFilesViewModel
                {
                    Id = a.Id,
                    CourseId = a.CourseId,
                    SectionId = a.SectionId,
                    SectionName = c.SectionName,
                    SectionTitle = c.SectionTitle,
                    CourseFileId = a.CourseFileId,
                    File = b.File,
                    FileName = b.FileName,
                    KnowledgePointId = b.KnowledgePointId,
                    FilesType = b.FilesType,
                    SectionType = a.SectionType,
                    Sort = a.Sort,
                    DataSourceType = a.DataSourceType,
                    EffectiveDate = a.EffectiveDate,
                });

            YssxPrepareSectionFilesForPCViewModel model = new YssxPrepareSectionFilesForPCViewModel();
            //课程资源
            model.CourseFilesList = list.Where(a => a.SectionId == 0 && string.IsNullOrEmpty(a.SectionName)).ToList();
            //章节资源
            model.SectionFilesList = list.Where(a => a.SectionId != 0 && !string.IsNullOrEmpty(a.SectionName)).ToList();

            response.Data = model;

            return response;
        }
        #endregion

        #region 删除备课课程课件、视频、教案、授课计划
        /// <summary>
        /// 删除备课课程课件、视频、教案、授课计划
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemovePrepareSectionFiles(long id, UserTicket user)
        {
            YssxPrepareSectionFiles entity = await DbContext.FreeSql.GetRepository<YssxPrepareSectionFiles>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<YssxPrepareSectionFiles>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;

            if (entity.SectionType == 1)
                await RemovPrepareSectionSummary(entity.CourseId, (int)SectionSummaryType.Files);
            if (entity.SectionType == 2)
                await RemovPrepareSectionSummary(entity.CourseId, (int)SectionSummaryType.LessonPlan);
            if (entity.SectionType == 3)
                await RemovPrepareSectionSummary(entity.CourseId, (int)SectionSummaryType.Video);
            if (entity.SectionType == 4)
                await RemovPrepareSectionSummary(entity.CourseId, (int)SectionSummaryType.TeachingPlan);

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 删除备课课程章节课件、视频、教案、授课计划(删除章节时触发)
        /// <summary>
        /// 删除备课课程章节课件、视频、教案、授课计划(删除章节时触发)
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<bool> RemovePrepareSectionFilesBySection(long courseId, long sectionId, UserTicket user)
        {
            List<YssxPrepareSectionFiles> list = await DbContext.FreeSql.GetRepository<YssxPrepareSectionFiles>()
                .Where(x => x.CourseId == courseId && x.SectionId == sectionId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            bool state = false;

            if (list != null && list.Count > 0)
            {
                list.ForEach(x =>
                {
                    x.IsDelete = 1;
                    x.UpdateTime = DateTime.Now;
                    x.UpdateBy = user.Id;
                });

                //批量删除教材
                state = DbContext.FreeSql.GetRepository<YssxPrepareSectionFiles>().UpdateDiy.SetSource(list).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;

                //课件
                int filesCount = list.Where(x => x.SectionType == 1).Count();
                if (filesCount > 0)
                    await RemovPrepareSectionSummary(courseId, (int)SectionSummaryType.Files, filesCount);

                //教案
                int lessonPlanCount = list.Where(x => x.SectionType == 2).Count();
                if (lessonPlanCount > 0)
                    await RemovPrepareSectionSummary(courseId, (int)SectionSummaryType.LessonPlan, filesCount);

                //视频
                int videoCount = list.Where(x => x.SectionType == 3).Count();
                if (videoCount > 0)
                    await RemovPrepareSectionSummary(courseId, (int)SectionSummaryType.Video, filesCount);

                //授课计划
                int teachingPlanCount = list.Where(x => x.SectionType == 4).Count();
                if (teachingPlanCount > 0)
                    await RemovPrepareSectionSummary(courseId, (int)SectionSummaryType.TeachingPlan, filesCount);
            }

            return state;
        }
        #endregion

        #region 添加课程章节信息统计汇总
        /// <summary>
        /// 添加课程章节信息统计汇总
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private async Task<bool> AddPrepareSectionSummary(long courseId, int summaryType)
        {
            YssxPrepareSectionSummary entity = await DbContext.FreeSql.GetRepository<YssxPrepareSectionSummary>().Where(x => x.CourseId == courseId && x.SummaryType == summaryType).FirstAsync();
            bool state = false;
            if (null == entity)
            {
                entity = await DbContext.FreeSql.GetRepository<YssxPrepareSectionSummary>().InsertAsync(new YssxPrepareSectionSummary
                { Id = IdWorker.NextId(), CourseId = courseId, SummaryType = summaryType, Count = 1, UpdateTime = DateTime.Now });
                state = entity != null;
            }
            else
            {
                entity.Count += 1;
                entity.UpdateTime = DateTime.Now;
                state = DbContext.FreeSql.GetRepository<YssxPrepareSectionSummary>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.Count, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return state;
        }
        #endregion

        #region 移除课程章节信息统计汇总
        /// <summary>
        /// 移除课程章节信息统计汇总
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sectionId"></param>
        /// <param name="summaryType"></param>
        /// <returns></returns>
        private async Task<bool> RemovPrepareSectionSummary(long courseId, int summaryType, int count = 1)
        {
            YssxPrepareSectionSummary entity = await DbContext.FreeSql.GetRepository<YssxPrepareSectionSummary>()
                .Where(x => x.CourseId == courseId && x.SummaryType == summaryType).FirstAsync();

            bool state = false;
            if (null != entity)
            {
                entity.Count -= count;
                entity.UpdateTime = DateTime.Now;
                state = DbContext.FreeSql.GetRepository<YssxPrepareSectionSummary>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.Count, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return state;
        }
        #endregion

    }
}
