﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.Pocos.BackManage;
using System.Linq;
using Yssx.Repository.Extensions;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 首页相关
    /// </summary>
    public class HomePageService : IHomePageService
    {
        public async Task<ResponseContext<List<BannerInfo>>> GetBannerInfo()
        {
            List<YssxBanner> bannerList = await DbContext.FreeSql.Select<YssxBanner>()
                .Where(c => c.IsDelete == CommonConstants.IsNotDelete).ToListAsync(CommonConstants.Cache_GetBanner, true, 30 * 60, false, false);
            List<BannerInfo> list = bannerList.Select(x => new BannerInfo
            {
                Id = x.Id,
                BannerUrl = x.BannerUrl,
                ImageUrl = x.ImageUrl,
                ProductId=x.ProductId
            }).ToList();
            return new ResponseContext<List<BannerInfo>> { Data = list };
        }

        /// <summary>
        /// 获取移动端首页信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<McHomePageDto>> GetMcHomePageInfo()
        {
            McHomePageDto model = new McHomePageDto();
            var bannerSelect = DbContext.FreeSql.Select<YssxBanner>()
                 .Where(c => c.IsDelete == CommonConstants.IsNotDelete);
            var bannerSql = bannerSelect.ToSql("*");
            var bannerList = await DbContext.FreeSql.Ado.QueryAsync<BannerInfo>(bannerSql);

            var courseSelect = DbContext.FreeSql.Select<YssxCourse>()
                 .Where(c => c.AuditStatus == 1 && c.IsDelete == CommonConstants.IsNotDelete);
            var courseSql = courseSelect.OrderByDescending(c => c.ReadCount).Page(1, 5).ToSql("Id,CourseName,Intro,Images");
            var courseList = await DbContext.FreeSql.Ado.QueryAsync<CourseInfo>(courseSql);

            var courseFilesSelect = DbContext.FreeSql.Select<YssxCourceFiles>()
                 .Where(c => c.AuditStatus == 1 && c.IsDelete == CommonConstants.IsNotDelete);
            var courseFilesSql = courseFilesSelect.OrderByDescending(c => c.ReadCount).Page(1, 5).ToSql("Id,FileName,KnowledgePointId,FilesType");
            var courseFilesList = await DbContext.FreeSql.Ado.QueryAsync<CourseFilesInfo>(courseFilesSql);

            foreach (var item in courseFilesList)
            {
                var knowledgePoints = DbContext.FreeSql.Select<YssxKnowledgePoint>()
                    .Where(s => item.KnowledgePointId.Contains(s.Id.ToString())).ToList();
                item.KnowledgePoints = string.Join("、", knowledgePoints.Select(s => s.Name).ToArray());
            }
            model.BannerInfos = bannerList;
            model.CourseInfos = courseList;
            model.CourseFilesInfos = courseFilesList;

            return new ResponseContext<McHomePageDto>(CommonConstants.SuccessCode, "", model);

        }

        #region 获取IOS发布信息
        /// <summary>
        /// 获取IOS发布信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<ReleaseInfoIOSViewModel>> GetReleaseInfoIOS()
        {
            ResponseContext<ReleaseInfoIOSViewModel> response = new ResponseContext<ReleaseInfoIOSViewModel>();

            ReleaseInfoIOSViewModel data = new ReleaseInfoIOSViewModel();

            YssxReleaseInfoIOS sourceData = await DbContext.FreeSql.Select<YssxReleaseInfoIOS>().Where(x =>
                 x.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(x => x.UpdateTime).FirstAsync();

            if (sourceData == null)
                return response;

            data.VersionNumber = sourceData.VersionNumber;
            data.UpdateContent = sourceData.UpdateContent;
            data.IsForceUpdate = sourceData.IsForceUpdate;

            response.Data = data;

            return response;
        }
        #endregion

    }
}
