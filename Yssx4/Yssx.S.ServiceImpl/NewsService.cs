﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class NewsService : INewsService
    {
        public async Task<ResponseContext<bool>> AddOrEditNews(YssxNewsDto dto)
        {
            YssxNews entity = new YssxNews
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                Title = dto.Title,
                ContentType = dto.ContentType,
                ImgUrl = dto.ImgUrl,
                NewsUrl = dto.NewsUrl,
                Platform = dto.Platform,
                Sort = dto.Sort,
                Status = dto.Status,
                CreateTime = DateTime.Now,

            };

            bool state = false;

            if (dto.Id <= 0)
            {
                await DbContext.FreeSql.GetRepository<YssxNews>().InsertAsync(entity);
            }
            else
            {
                await DbContext.FreeSql.GetRepository<YssxNews>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.Title, x.ContentType, x.ImgUrl, x.NewsUrl, x.Platform, x.Sort, x.Status }).ExecuteAffrowsAsync();
            }
            return new ResponseContext<bool> { Data = state };
        }

        public async Task<ResponseContext<List<YssxNewsDto>>> GetNewsList()
        {
            List<YssxNewsDto> list = await DbContext.FreeSql.GetRepository<YssxNews>().Where(x => x.IsDelete == 0).ToListAsync<YssxNewsDto>();
            return new ResponseContext<List<YssxNewsDto>> { Data = list };

        }

        public async Task<ResponseContext<List<YssxNewsDto>>> GetNewsListByPlatform(int platform)
        {
            List<YssxNewsDto> list = await DbContext.FreeSql.GetRepository<YssxNews>().Where(x => x.IsDelete == 0 && x.Platform == platform && x.Status == 1).ToListAsync<YssxNewsDto>();
            return new ResponseContext<List<YssxNewsDto>> { Data = list };
        }

        public async Task<ResponseContext<bool>> SetStatus(long id, int status)
        {
            int x = await DbContext.FreeSql.GetRepository<YssxNews>().UpdateDiy.Set(a => a.Status, status).Where(a => a.Id == id).ExecuteAffrowsAsync();
            return new ResponseContext<bool> { Data = x > 0 };
        }
    }
}
