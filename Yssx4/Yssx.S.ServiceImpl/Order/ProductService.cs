﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Mall;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 产品
    /// </summary>
    public class ProductService : IProductService
    {
        #region 抢购商品
        /// <summary>
        /// 抢购商品
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<ProductDto>>> GetCasProducts()
        {
            int date = DateTime.Now.Day;
            List<YssxProduct> list = await DbContext.FreeSql.GetRepository<YssxProduct>().Where(x => x.Date == date).ToListAsync();
            List<ProductDto> dtos = list.Select(x => new ProductDto
            {
                Id = x.Id,
                CaseId = x.CaseId,
                CaseName = x.CaseName,
                DiscountPrice = x.DiscountPrice,
                OrderType = x.OrderType,
                OriginalPrice = x.OriginalPrice,
                EndDate = $"2020-12-{date} 23:59:59",
            }).ToList();
            return new ResponseContext<List<ProductDto>> { Data = dtos };
        }
        #endregion

        #region 产品

        /// <summary>
        /// 保存产品
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveProduct(SaveProductDto model, long currentUserId)
        {
            DateTime dtNow = DateTime.Now;
            return await Task.Run(() =>
            {
                #region 校验
                //TODO 校验
                if (model.Id > 0)
                {
                    var rAnyEntity = DbContext.FreeSql.GetRepository<YssxMallProduct>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rAnyEntity.Any())
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到产品信息" };
                }
                if (string.IsNullOrEmpty(model.Name))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "产品名称不能为空" };
                if (string.IsNullOrEmpty(model.ShortDesc))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "产品简介不能为空" };
                if (string.IsNullOrEmpty(model.FullDesc))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "产品介绍不能为空" };
                if (model.OldPrice <= 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "产品原价不能小于等于0" };
                #endregion
                var product = model.MapTo<YssxMallProduct>();
                product.CreateBy = currentUserId;
                product.CreateTime = dtNow;
                product.UpdateBy = currentUserId;
                product.UpdateTime = dtNow;
                if (model.Id == 0) { product.Id = IdWorker.NextId(); }

                #region 处理明细排序
                //var loop = 0;
                ////明细信息
                //var yssxMallProductItems = new List<YssxMallProductItem>();
                //foreach (var item in model.ItemDetail)
                //{
                //    loop++;
                //    yssxMallProductItems.Add(new YssxMallProductItem()
                //    {
                //        Id = item.Id,
                //        ProductId = product.Id,
                //        Category = item.Category,
                //        TargetId = item.TargetId,
                //        Source = item.Source,
                //        CreateBy = currentUserId,
                //        CreateTime = dtNow
                //    });
                //}
                #endregion

                if (model.Id == 0)
                {
                    //yssxMallProductItems.ForEach(a => a.Id = IdWorker.NextId());
                    //添加数据
                    var state = false;
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        state = DbContext.FreeSql.Insert(product).ExecuteAffrows() > 0;
                        //if (state)
                        //    DbContext.FreeSql.Insert<YssxMallProductItem>().AppendData(yssxMallProductItems).ExecuteAffrows();
                        uow.Commit();
                    }
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state };
                }
                else
                {
                    #region 明细数据
                    //var addItem = yssxMallProductItems.Where(a => a.Id == 0).ToList();
                    //var editItem = yssxMallProductItems.Where(a => a.Id > 0).ToList();
                    //addItem.ForEach(a => { a.Id = IdWorker.NextId(); });

                    ////取删除的明细数据
                    //var existDetailId = yssxMallProductItems.Select(a => a.Id).ToArray();
                    //var delItem = DbContext.FreeSql.Select<YssxMallProductItem>().Where(m => m.ProductId == product.Id && m.IsDelete == CommonConstants.IsNotDelete && !existDetailId.Contains(m.Id)).ToList();
                    //if (delItem.Count > 0)
                    //{
                    //    delItem.ForEach(a =>
                    //    {
                    //        a.IsDelete = CommonConstants.IsDelete;
                    //        a.UpdateBy = currentUserId;
                    //        a.UpdateTime = dtNow;
                    //    });
                    //}
                    #endregion

                    //更新数据
                    var state = false;
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        //更新产品
                        state = DbContext.FreeSql.Update<YssxMallProduct>().SetSource(product).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrows() > 0;

                        ////添加明细
                        //if (state && addItem.Count > 0)
                        //    DbContext.FreeSql.Insert<YssxMallProductItem>().AppendData(addItem).ExecuteAffrows();
                        ////删除明细
                        //if (state && delItem.Count > 0)
                        //    DbContext.FreeSql.Update<YssxMallProductItem>().SetSource(delItem).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                        ////更新明细
                        //if (state && editItem.Count > 0)
                        //    DbContext.FreeSql.Update<YssxMallProductItem>().SetSource(editItem).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime }).ExecuteAffrows();

                        uow.Commit();
                    }

                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state };
                }
            });
        }

        /// <summary>
        /// 删除产品
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteProduct(long id, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;

                var entity = DbContext.FreeSql.GetRepository<YssxMallProduct>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到原始信息!" };

                entity.IsDelete = CommonConstants.IsDelete;
                entity.UpdateTime = dtNow;
                entity.UpdateBy = currentUserId;

                var listDetail = DbContext.FreeSql.GetRepository<YssxMallProductItem>().Where(x => x.ProductId == id && x.IsDelete == CommonConstants.IsNotDelete).ToList(m => new YssxMallProductItem
                {
                    Id = m.Id,
                    IsDelete = CommonConstants.IsDelete,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                });

                bool state = true;
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //产品
                    state = DbContext.FreeSql.Update<YssxMallProduct>().SetSource(entity).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                    //明细
                    if (state && listDetail.Count > 0)
                        DbContext.FreeSql.Update<YssxMallProductItem>().SetSource(listDetail).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();

                    tran.Commit();
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 获取产品列表
        /// </summary>
        public async Task<PageResponse<GetProductByPageDto>> GetProductByPage(GetProductByPageRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.GetRepository<YssxMallProduct>().Select.From<YssxUser>((a, b) =>
                    a.LeftJoin(aa => aa.UpdateBy == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b) => a.Name.Contains(model.Name));

                var totalCount = select.Count();
                var selectData = select.Page(model.PageIndex, model.PageSize).ToList((a, b) => new GetProductByPageDto
                {
                    Id = a.Id,
                    Img = a.Img,
                    Name = a.Name,
                    ShortDesc = a.ShortDesc,
                    Price = a.Price,
                    OldPrice = a.OldPrice,
                    UpdateByName = b.RealName,
                    UpdateTime = a.UpdateTime
                });
                return new PageResponse<GetProductByPageDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 获取指定产品数据
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<SaveProductDto>> GetProductInfoById(long id)
        {
            return await Task.Run(() =>
            {
                var entity = DbContext.FreeSql.GetGuidRepository<YssxMallProduct>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<SaveProductDto> { Code = CommonConstants.ErrorCode, Msg = "找不到产品数据" };
                var resData = entity.MapTo<SaveProductDto>();
                //产品明细
                var detailData = DbContext.FreeSql.GetGuidRepository<YssxMallProductItem>().Where(x => x.ProductId == id && x.IsDelete == CommonConstants.IsNotDelete)
                    .ToList(x => new SaveProductDetailDto
                    {
                        Id = x.Id,
                        ProductId = x.ProductId,
                        Category = x.Category,
                        TargetId = x.TargetId,
                        Source = x.Source
                    });
                resData.ItemDetail = detailData;

                return new ResponseContext<SaveProductDto> { Code = CommonConstants.SuccessCode, Data = resData };
            });
        }



        /// <summary>
        /// 保存产品明细
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveProductDetail(SaveProductDetailDto model, long currentUserId)
        {
            DateTime dtNow = DateTime.Now;
            return await Task.Run(() =>
            {
                #region 校验
                //TODO 校验
                var rAnyEntity = DbContext.FreeSql.GetRepository<YssxMallProduct>().Where(m => m.Id == model.ProductId && m.IsDelete == CommonConstants.IsNotDelete);
                if (!rAnyEntity.Any())
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到产品信息" };
                if (model.TargetId == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，关联案例ID错误" };
                if (string.IsNullOrEmpty(model.TargetName))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，关联案例名称不能为空" };
                if (model.Category <= 0 || model.Category > 2)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，分类错误" };
                var rAnyTargetId = DbContext.FreeSql.GetRepository<YssxMallProductItem>().Where(m => m.TargetId == model.TargetId && m.IsDelete == CommonConstants.IsNotDelete);
                if (rAnyTargetId.Any())
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，关联的案例或月度任务已存在" };
                #endregion

                var rProductItems = new YssxMallProductItem()
                {
                    Id = IdWorker.NextId(),
                    ProductId = model.ProductId,
                    Category = model.Category,
                    TargetId = model.TargetId,
                    TargetName = model.TargetName,
                    Source = model.Source,
                    CreateBy = currentUserId,
                    CreateTime = dtNow,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };
                //添加数据
                var state = DbContext.FreeSql.Insert(rProductItems).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state };
            });
        }

        /// <summary>
        /// 删除产品明细
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteProductDetail(long id, long productId, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;

                var entity = DbContext.FreeSql.GetRepository<YssxMallProductItem>().Where(m => m.Id == id && m.ProductId == productId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到原始信息!" };

                entity.IsDelete = CommonConstants.IsDelete;
                entity.UpdateTime = dtNow;
                entity.UpdateBy = currentUserId;

                //产品明细
                bool state = DbContext.FreeSql.Update<YssxMallProductItem>().SetSource(entity).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }
        #endregion


    }
}
