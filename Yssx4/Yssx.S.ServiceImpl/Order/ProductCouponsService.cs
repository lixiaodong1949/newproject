﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Order;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 产品兑换券
    /// </summary>
    public class ProductCouponsService : IProductCouponsService
    {
        public string GenerateOrderNo(int orderType)
        {
            string prefix = string.Empty;
            if (orderType == 1)
            {
                prefix = "ZZSC";
            }
            else if (orderType == 2)
            {
                prefix = "KCTB";
            }
            else if (orderType == 3)
            {
                prefix = "YESC";
            }

            return prefix + DateTime.Now.ToString("yyyyMMdd") + new Random().Next(100000, 999999);
        }

        /// <summary>
        /// 验证产品卡券
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ExchangeCouponsCode(string code, string mobilePhone, long currentUserId)
        {
            DateTime dtNow = DateTime.Now;
            //验证
            if (string.IsNullOrEmpty(code))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请填写卡券码" };
            if (string.IsNullOrEmpty(mobilePhone))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请填写购买时备注的手机号" };

            var rCode = DbContext.FreeSql.Select<YssxProductCoupons>().Where(x => x.CouponsNo == code && x.MobilePhone == mobilePhone && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (rCode == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "卡券不存在" };
            if (rCode.IsExchange)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "卡券已兑换" };
            //if (rCode.State == 0)
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "激活码禁用!" };
            //if (rCode.EndTime < DateTime.Now)
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "激活码失效!" };

            //兑换券信息
            rCode.IsExchange = true;
            rCode.UpdateBy = currentUserId;
            rCode.UpdateTime = dtNow;
            //产品信息
            var rCaseId = 0;
            var rOrderType = 1;
            var rCaseName = "";
            decimal rOriginalPrice = 0;
            if (rCode.CaseName.Contains("麓山煤矿") && rCode.CaseName.Contains("圆梦造纸厂") && rCode.CaseName.Contains("南邦水泥厂"))
            {
                rCaseId = 4;
                rOrderType = 1;
                rCaseName = "抢购搭配价";
                rOriginalPrice = 2394;
            }
            else if (rCode.CaseName.Contains("麓山煤矿"))
            {
                rCaseId = 2;
                rOrderType = 1;
                rCaseName = "麓山煤矿";
                rOriginalPrice = 698;
            }
            else if (rCode.CaseName.Contains("圆梦造纸厂"))
            {
                rCaseId = 1;
                rOrderType = 2;
                rCaseName = "圆梦造纸厂";
                rOriginalPrice = 498;
            }
            else if (rCode.CaseName.Contains("南邦水泥厂"))
            {
                rCaseId = 3;
                rOrderType = 3;
                rCaseName = "南邦水泥厂";
                rOriginalPrice = 1198;
            }

            //订单信息
            var rSxRtpOrder = new SxRtpOrder
            {
                Id = IdWorker.NextId(),
                UserId = currentUserId,
                CaseId = rCaseId,
                CaseName = rCaseName,
                OrderNo = GenerateOrderNo(rOrderType),
                TransactionNo = rCode.OrderId.ToString(),
                Description = string.Format("{0}的订单", rCaseName),
                Status = 2,
                PaymentType = 3,
                LoginType = 0,
                OriginalPrice = rOriginalPrice,
                DiscountPrice = rCode.ActualPayment,
                PaymentTime = rCode.PaymentTime,
                CreateBy = currentUserId,
                CreateTime = dtNow
            };

            return await Task.Run(() =>
            {
                var state = false;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    state = DbContext.FreeSql.Insert(rSxRtpOrder).ExecuteAffrows() > 0;
                    if (state)
                        DbContext.FreeSql.Update<YssxProductCoupons>().SetSource(rCode).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsExchange }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "兑换成功", Data = state };
            });
        }


        /// <summary>
        /// 自动验证产品卡券
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AutoExchangeCouponsCode(string ignoreMobilePhone, long currentUserId)
        {
            DateTime dtNow = DateTime.Now;
            var rIgnoreMobilePhone = ignoreMobilePhone.Split(",");
            var rOrderList = DbContext.FreeSql.Select<YssxProductCoupons>().From<YssxUser>((a, b) =>
                a.InnerJoin(aa => aa.MobilePhone == b.MobilePhone && b.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b) => !rIgnoreMobilePhone.Contains(a.MobilePhone) && !a.IsExchange && a.IsDelete == CommonConstants.IsNotDelete)
                .ToList((a, b) => new ProductCouponsDto
                {
                    Id = a.Id,
                    MobilePhone = a.MobilePhone,
                    CaseName = a.CaseName,
                    OrderId = a.OrderId,
                    ActualPayment = a.ActualPayment,
                    PaymentTime = a.PaymentTime,
                    UserId = b.Id
                });

            if (rOrderList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "无可同步卡券" };

            var rUpdCodeList = new List<YssxProductCoupons>();
            var rAddOrderList = new List<SxRtpOrder>();
            foreach (var item in rOrderList)
            {
                //兑换券信息
                var rCode = new YssxProductCoupons
                {
                    Id = item.Id,
                    IsExchange = true,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };
                rUpdCodeList.Add(rCode);
                //产品信息
                var rCaseId = 0;
                var rOrderType = 1;
                var rCaseName = "";
                decimal rOriginalPrice = 0;
                if (item.CaseName.Contains("麓山煤矿") && item.CaseName.Contains("圆梦造纸厂") && item.CaseName.Contains("南邦水泥厂"))
                {
                    rCaseId = 4;
                    rOrderType = 1;
                    rCaseName = "抢购搭配价";
                    rOriginalPrice = 2394;
                }
                else if (item.CaseName.Contains("麓山煤矿"))
                {
                    rCaseId = 2;
                    rOrderType = 1;
                    rCaseName = "麓山煤矿";
                    rOriginalPrice = 698;
                }
                else if (item.CaseName.Contains("圆梦造纸厂"))
                {
                    rCaseId = 1;
                    rOrderType = 2;
                    rCaseName = "圆梦造纸厂";
                    rOriginalPrice = 498;
                }
                else if (item.CaseName.Contains("南邦水泥厂"))
                {
                    rCaseId = 3;
                    rOrderType = 3;
                    rCaseName = "南邦水泥厂";
                    rOriginalPrice = 1198;
                }

                //订单信息
                var rSxRtpOrder = new SxRtpOrder
                {
                    Id = IdWorker.NextId(),
                    UserId = item.UserId,
                    CaseId = rCaseId,
                    CaseName = rCaseName,
                    OrderNo = GenerateOrderNo(rOrderType),
                    TransactionNo = item.OrderId.ToString(),
                    Description = string.Format("{0}的订单", rCaseName),
                    Status = 2,
                    PaymentType = 3,
                    LoginType = 0,
                    OriginalPrice = rOriginalPrice,
                    DiscountPrice = item.ActualPayment,
                    PaymentTime = item.PaymentTime,
                    CreateBy = item.UserId,
                    CreateTime = dtNow
                };
                rAddOrderList.Add(rSxRtpOrder);
            }

            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    var state = false;
                    try
                    {
                        state = DbContext.FreeSql.Insert<SxRtpOrder>().AppendData(rAddOrderList).ExecuteAffrows() > 0;
                        if (state)
                            DbContext.FreeSql.Update<YssxProductCoupons>().SetSource(rUpdCodeList).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsExchange }).ExecuteAffrows();
                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        return new ResponseContext<bool>(CommonConstants.ErrorCode, "自动兑换异常！", state);
                    }
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "自动兑换成功", Data = state };
                }

            });
        }

    }
}
