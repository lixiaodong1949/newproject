﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto.Order;
using Yssx.S.IServices.Order;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Order;

namespace Yssx.S.ServiceImpl.Order
{
    public class UserOrderService : IUserOrderService
    {
        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResponse<UserOrderDto>> GetOrderList(GetUserOrdersDto input)
        {
            var select = DbContext.FreeSql.Select<YssxUserOrder, YssxUser>()
                .LeftJoin((o, u) => o.UserId == u.Id)
                .WhereIf(input.UserId.HasValue && input.UserId > 0, (o, u) => o.UserId == input.UserId)
                .WhereIf(!string.IsNullOrEmpty(input.OrderNo), (o, u) => o.OrderNo.Contains(input.OrderNo))
                .WhereIf(!string.IsNullOrEmpty(input.MobilePhone), (o, u) => u.MobilePhone.Contains(input.MobilePhone))
                .WhereIf(!string.IsNullOrEmpty(input.RealName), (o, u) => u.RealName.Contains(input.RealName));

            var totalCount = select.Count();
            var sql = select.Page(input.PageIndex, input.PageSize).OrderByDescending((o,u) => o.PaymentTime).ToSql("a.*,b.MobilePhone,b.RealName,b.WeChat");
            var items = await DbContext.FreeSql.Ado.QueryAsync<UserOrderDto>(sql);

            return new PageResponse<UserOrderDto>() { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items };
        }

        /// <summary>
        /// 获取单个用户的订单信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<SingleUserOrder>> GetSingleUserOrders(GetSingleUserOrderDto input)
        {
            SingleUserOrder singleUserOrder = new SingleUserOrder();
            var yssxUser = await DbContext.FreeSql.Select<YssxUser>().Where(u => u.Id == input.UserId && u.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (yssxUser == null)
                return new ResponseContext<SingleUserOrder>(CommonConstants.ErrorCode, "没有找到该用户！", null);
            var school = await DbContext.FreeSql.Select<YssxSchool>().Where(s => s.Id == yssxUser.TenantId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (school == null)
                return new ResponseContext<SingleUserOrder>(CommonConstants.ErrorCode, "没有找到该用户所属的学校！", null);
            else if (school.Id == CommonConstants.DefaultTenantId)
                singleUserOrder.SchoolName = "";
            singleUserOrder.UserName = yssxUser.UserName;
            singleUserOrder.MobilePhone = yssxUser.MobilePhone;
            singleUserOrder.RealName = yssxUser.RealName;
            singleUserOrder.UserType = yssxUser.UserType;
            singleUserOrder.Wechat = yssxUser.WeChat;
            singleUserOrder.RegisterTime = yssxUser.CreateTime;

            var select = DbContext.FreeSql.Select<YssxUserOrder>().Where(o => o.UserId == input.UserId);
            var totalCount = select.Count();
            var sql = select.Page(input.PageIndex,input.PageSize).OrderByDescending(o => o.PaymentTime).ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<UserOrderDto>(sql);
            singleUserOrder.Orders = new PageResponse<UserOrderDto> { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items };
            return new ResponseContext<SingleUserOrder>(CommonConstants.SuccessCode, "", singleUserOrder);
        }

        /// <summary>
        /// 订单统计
        /// </summary>
        /// <returns>今日成交金额、今日成交量、成交总金额</returns>
        public async Task<ResponseContext<UserOrderStatsDto>> GetStats()
        {
            UserOrderStatsDto userOrderStatsDto = new UserOrderStatsDto();
            var yssxUserOrder = DbContext.FreeSql.Select<YssxUserOrder>().Where(o => o.PaymentTime >= DateTime.Now.Date).Where(o => o.PaymentTime <= DateTime.Now).Where(o => o.Status ==2);
            userOrderStatsDto.TodayAmount = await yssxUserOrder.SumAsync(o => o.OrderTotal);
            userOrderStatsDto.TodayCount = await yssxUserOrder.CountAsync();
            userOrderStatsDto.TotalAmount = await DbContext.FreeSql.Select<YssxUserOrder>().Where(o => o.Status == 2).SumAsync(o => o.OrderTotal);
            return new ResponseContext<UserOrderStatsDto>(CommonConstants.SuccessCode, "", userOrderStatsDto);
        }

        /// <summary>
        /// 迁移抢购订单到用户订单表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Migrate()
        {
            var sxRtpOrders = await DbContext.FreeSql.Select<SxRtpOrder>().ToListAsync();
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    foreach (var sxRtpOrder in sxRtpOrders)
                    {
                        if (DbContext.FreeSql.Select<YssxUserOrder>().Where(o => o.OrderNo == sxRtpOrder.OrderNo).Any())
                            continue;

                        YssxUserOrder yssxUserOrder = new YssxUserOrder();
                        yssxUserOrder.Id = IdWorker.NextId();
                        yssxUserOrder.OrderNo = sxRtpOrder.OrderNo;
                        yssxUserOrder.UserId = sxRtpOrder.UserId;
                        yssxUserOrder.Description = sxRtpOrder.Description;
                        yssxUserOrder.Status = sxRtpOrder.Status;
                        yssxUserOrder.PaymentType = sxRtpOrder.PaymentType;
                        yssxUserOrder.PaymentTime = sxRtpOrder.PaymentTime;
                        yssxUserOrder.LoginType = sxRtpOrder.LoginType;
                        yssxUserOrder.OrderTotal = sxRtpOrder.DiscountPrice;
                        yssxUserOrder.Qty = 1;
                        DbContext.FreeSql.Insert<YssxUserOrder>(yssxUserOrder).ExecuteAffrows();

                        if (sxRtpOrder.CaseId == 1)
                        {
                            YssxUserOrderDetail yssxUserOrderDetail = new YssxUserOrderDetail();
                            yssxUserOrderDetail.Id = IdWorker.NextId();
                            yssxUserOrderDetail.OrderId = yssxUserOrder.Id;
                            yssxUserOrderDetail.ProductId = 1060383171608577;
                            yssxUserOrderDetail.CaseType = 1;
                            DbContext.FreeSql.Insert<YssxUserOrderDetail>(yssxUserOrderDetail).ExecuteAffrows();
                        }
                        else if (sxRtpOrder.CaseId == 2)
                        {
                            YssxUserOrderDetail yssxUserOrderDetail = new YssxUserOrderDetail();
                            yssxUserOrderDetail.Id = IdWorker.NextId();
                            yssxUserOrderDetail.OrderId = yssxUserOrder.Id;
                            yssxUserOrderDetail.ProductId = 1060383200968707;
                            yssxUserOrderDetail.CaseType = 2;
                            DbContext.FreeSql.Insert<YssxUserOrderDetail>(yssxUserOrderDetail).ExecuteAffrows();
                        }
                        else if (sxRtpOrder.CaseId == 3)
                        {
                            YssxUserOrderDetail yssxUserOrderDetail = new YssxUserOrderDetail();
                            yssxUserOrderDetail.Id = IdWorker.NextId();
                            yssxUserOrderDetail.OrderId = yssxUserOrder.Id;
                            yssxUserOrderDetail.ProductId = 1060383230328837;
                            yssxUserOrderDetail.CaseType = 3;
                            DbContext.FreeSql.Insert<YssxUserOrderDetail>(yssxUserOrderDetail).ExecuteAffrows();
                        }
                        else if (sxRtpOrder.CaseId == 4)
                        {
                            YssxUserOrderDetail yssxUserOrderDetail = new YssxUserOrderDetail();
                            yssxUserOrderDetail.Id = IdWorker.NextId();
                            yssxUserOrderDetail.OrderId = yssxUserOrder.Id;
                            yssxUserOrderDetail.ProductId = 1060383259688967;
                            yssxUserOrderDetail.CaseType = 1;
                            DbContext.FreeSql.Insert<YssxUserOrderDetail>(yssxUserOrderDetail).ExecuteAffrows();

                            yssxUserOrderDetail = new YssxUserOrderDetail();
                            yssxUserOrderDetail.Id = IdWorker.NextId();
                            yssxUserOrderDetail.OrderId = yssxUserOrder.Id;
                            yssxUserOrderDetail.ProductId = 1060383259688967;
                            yssxUserOrderDetail.CaseType = 2;
                            DbContext.FreeSql.Insert<YssxUserOrderDetail>(yssxUserOrderDetail).ExecuteAffrows();

                            yssxUserOrderDetail = new YssxUserOrderDetail();
                            yssxUserOrderDetail.Id = IdWorker.NextId();
                            yssxUserOrderDetail.OrderId = yssxUserOrder.Id;
                            yssxUserOrderDetail.ProductId = 1060383259688967;
                            yssxUserOrderDetail.CaseType = 3;
                            DbContext.FreeSql.Insert<YssxUserOrderDetail>(yssxUserOrderDetail).ExecuteAffrows();
                        }
                    }
                    uow.Commit();
                    return new ResponseContext<bool>(CommonConstants.SuccessCode,"",true);
                }
                catch (Exception)
                {
                    uow.Rollback();
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);
                }
            }
           
        }
    }
}
