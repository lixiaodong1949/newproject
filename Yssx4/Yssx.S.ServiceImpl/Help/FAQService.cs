﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Repository.Extensions;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 常见问题
    /// </summary>
    public class FAQService : IFAQService
    {
        #region 问题分类
        /// <summary>
        /// 添加/编辑 问题分类
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditFAQClass(FAQClassDto model, long currentUserId)
        {
            var dtNow = DateTime.Now;
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
            if (DbContext.FreeSql.Select<YssxFAQClass>().Any(m => m.Name == model.Name && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称重复" };

            return await Task.Run(() =>
            {
                bool state = true;
                if (model.Id > 0)
                {
                    //修改
                    var oldEntity = DbContext.FreeSql.GetRepository<YssxFAQClass>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldEntity == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

                    oldEntity.Name = model.Name;
                    oldEntity.Sort = model.Sort;
                    oldEntity.UpdateTime = dtNow;
                    oldEntity.UpdateBy = currentUserId;

                    using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        state = DbContext.FreeSql.Update<YssxFAQClass>().SetSource(oldEntity).UpdateColumns(m => new { m.Name, m.Sort, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                        tran.Commit();
                    }
                }
                else
                {
                    //新增
                    var entity = new YssxFAQClass
                    {
                        Id = IdWorker.NextId(),
                        Name = model.Name,
                        Sort = model.Sort,
                        CreateBy = currentUserId,
                        CreateTime = dtNow
                    };

                    using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        state = DbContext.FreeSql.GetRepository<YssxFAQClass>().Insert(entity) != null;
                        tran.Commit();
                    }
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 删除 问题分类
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteFAQClass(long id, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var entity = DbContext.FreeSql.GetRepository<YssxFAQClass>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到原始信息!" };

                entity.IsDelete = CommonConstants.IsDelete;
                entity.UpdateTime = DateTime.Now;
                entity.UpdateBy = currentUserId;

                bool state = true;
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    state = DbContext.FreeSql.Update<YssxFAQClass>().SetSource(entity).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                    tran.Commit();
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 获取问题分类列表
        /// </summary>
        public async Task<PageResponse<FAQClassDto>> GetFAQClassByPage(GetFAQClassByPageRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.GetRepository<YssxFAQClass>().Where(m => m.IsDelete == CommonConstants.IsNotDelete)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), m => m.Name.Contains(model.Name));
                var totalCount = select.Count();
                var selectData = select.OrderBy(m => m.Sort).Page(model.PageIndex, model.PageSize).ToList(m => new FAQClassDto
                {
                    Id = m.Id,
                    Name = m.Name,
                    Sort = m.Sort
                });
                return new PageResponse<FAQClassDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        #endregion

        #region 常见问题

        /// <summary>
        /// 添加/编辑 常见问题
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditFAQInfo(FAQInfoDto model, long currentUserId)
        {
            var dtNow = DateTime.Now;
            //验证
            if (string.IsNullOrEmpty(model.Title) || string.IsNullOrEmpty(model.Content))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "标题和内容不能为空" };
            if (DbContext.FreeSql.Select<YssxFAQInfo>().Any(m => m.Title == model.Title && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "标题重复" };
            if (!DbContext.FreeSql.Select<YssxFAQClass>().Any(m => m.Id == model.FaqClassId && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "问题分类无效" };

            return await Task.Run(() =>
            {
                bool state = true;
                var maxSort = DbContext.FreeSql.Select<YssxFAQInfo>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(m => m.Sort).First(m => m.Sort);
                if (model.Id > 0)
                {
                    //修改
                    var oldEntity = DbContext.FreeSql.GetRepository<YssxFAQInfo>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldEntity == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

                    oldEntity.Title = model.Title;
                    oldEntity.Content = model.Content;
                    oldEntity.FaqClassId = model.FaqClassId;
                    oldEntity.RoleType = model.RoleType;
                    oldEntity.Sort = model.Sort == 0 ? maxSort + 1 : model.Sort;
                    oldEntity.UpdateTime = dtNow;
                    oldEntity.UpdateBy = currentUserId;

                    using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        state = DbContext.FreeSql.Update<YssxFAQInfo>().SetSource(oldEntity).UpdateColumns(m => new { m.Title, m.Content, m.FaqClassId, m.RoleType, m.Sort, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                        tran.Commit();
                    }
                }
                else
                {
                    //新增
                    var newEntity = new YssxFAQInfo
                    {
                        Id = IdWorker.NextId(),
                        Title = model.Title,
                        Content = model.Content,
                        FaqClassId = model.FaqClassId,
                        RoleType = model.RoleType,
                        Sort = model.Sort == 0 ? maxSort + 1 : model.Sort,
                        ReadNumber = 0,
                        CreateBy = currentUserId,
                        CreateTime = dtNow,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    };

                    using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        state = DbContext.FreeSql.GetRepository<YssxFAQInfo>().Insert(newEntity) != null;
                        tran.Commit();
                    }
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 删除 常见问题
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteFAQInfo(long id, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var entity = DbContext.FreeSql.GetRepository<YssxFAQInfo>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到原始信息!" };

                entity.IsDelete = CommonConstants.IsDelete;
                entity.UpdateTime = DateTime.Now;
                entity.UpdateBy = currentUserId;

                bool state = true;
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    state = DbContext.FreeSql.Update<YssxFAQInfo>().SetSource(entity).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                    tran.Commit();
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 获取常见问题列表
        /// </summary>
        public async Task<PageResponse<FAQInfoDto>> GetFAQInfoByPage(GetFAQInfoByPageRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.GetRepository<YssxFAQInfo>().Select
                .From<YssxFAQClass>((a, b) => a.InnerJoin(aa => aa.FaqClassId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Title), (a, b) => a.Title.Contains(model.Title))
                .WhereIf(model.FaqClassId > 0, (a, b) => a.FaqClassId == model.FaqClassId);
                if (model.SortType == 0)
                    select = select.OrderBy((a, b) => a.Sort);
                if (model.SortType == 1)
                    select = select.OrderByDescending((a, b) => a.CreateTime);
                if (model.SortType == 2)
                    select = select.OrderByDescending((a, b) => a.ReadNumber);

                var totalCount = select.Count();
                var selectData = select.Page(model.PageIndex, model.PageSize).ToList((a, b) => new FAQInfoDto
                {
                    Id = a.Id,
                    Title = a.Title,
                    Content = a.Content,
                    FaqClassId = a.FaqClassId,
                    FaqClassName = b.Name,
                    RoleType = a.RoleType,
                    Sort = a.Sort,
                    ReadNumber = a.ReadNumber,
                    UpdateTime = a.UpdateTime
                });
                return new PageResponse<FAQInfoDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 获取指定常见问题
        /// </summary>
        public async Task<ResponseContext<FAQInfoOneDto>> GetFAQInfoById(long id)
        {
            return await Task.Run(() =>
            {
                var entity = DbContext.FreeSql.GetRepository<YssxFAQInfo>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<FAQInfoOneDto> { Code = CommonConstants.ErrorCode, Msg = "找不到题目信息!" };

                var rFAQInfoEntity = new FAQInfoOneDto();
                rFAQInfoEntity.Id = entity.Id;
                rFAQInfoEntity.Title = entity.Title;
                rFAQInfoEntity.Content = entity.Content;
                rFAQInfoEntity.FaqClassId = entity.FaqClassId;
                rFAQInfoEntity.RoleType = entity.RoleType;
                rFAQInfoEntity.Sort = entity.Sort;
                rFAQInfoEntity.ReadNumber = entity.ReadNumber;
                rFAQInfoEntity.UpdateTime = entity.UpdateTime;
                //分类名称
                var rFAQClass = DbContext.FreeSql.GetRepository<YssxFAQClass>().Select.Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Id == rFAQInfoEntity.FaqClassId).First();
                rFAQInfoEntity.FaqClassName = rFAQClass == null ? "" : rFAQClass.Name;
                //上一篇
                var previous = DbContext.FreeSql.GetRepository<YssxFAQInfo>().Select.Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Sort <= entity.Sort && m.CreateTime < entity.CreateTime)
                    .OrderByDescending(m => m.Sort).OrderByDescending(m => m.CreateTime).First();
                rFAQInfoEntity.Previous = previous == null ? 0 : previous.Id;
                rFAQInfoEntity.PreviousTitle = previous == null ? "" : previous.Title;
                //下一篇
                var next = DbContext.FreeSql.GetRepository<YssxFAQInfo>().Select.Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Sort >= entity.Sort && m.CreateTime > entity.CreateTime)
                    .OrderBy(m => m.Sort).OrderBy(m => m.CreateTime).First();
                rFAQInfoEntity.Next = next == null ? 0 : next.Id;
                rFAQInfoEntity.NextTitle = next == null ? "" : next.Title;

                //更新数据
                entity.ReadNumber = entity.ReadNumber + 1;
                DbContext.FreeSql.Update<YssxFAQInfo>().SetSource(entity).UpdateColumns(m => new { m.ReadNumber }).ExecuteAffrows();

                return new ResponseContext<FAQInfoOneDto> { Code = CommonConstants.SuccessCode, Data = rFAQInfoEntity, Msg = "成功" };
            });
        }

        /// <summary>
        /// 获取角色分类问题列表
        /// </summary>
        public async Task<ResponseContext<FAQRoleInfoDto>> GetFAQRoleList()
        {
            return await Task.Run(() =>
            {
                var entity = new FAQRoleInfoDto();
                //var rFAQRoleData = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetFaqRoleData}", () => DbContext.FreeSql.GetRepository<YssxFAQInfo>().Select
                //    .Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.RoleType > 0)
                //    .OrderBy(m => m.Sort).OrderBy(m => m.CreateTime).ToList(m => new FAQClassDto
                //    {
                //        Id = m.Id,
                //        Name = m.Title,
                //        Sort = m.Sort
                //    }), 10 * 60, true, false);
                //普通用户
                var userList = DbContext.FreeSql.GetRepository<YssxFAQInfo>().Select.Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.RoleType == 1)
                    .OrderBy(m => m.Sort).OrderBy(m => m.CreateTime).ToList(m => new FAQClassDto
                    {
                        Id = m.Id,
                        Name = m.Title,
                        Sort = m.Sort
                    });
                entity.UserDetail = userList;
                //教师
                var teacherList = DbContext.FreeSql.GetRepository<YssxFAQInfo>().Select.Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.RoleType == 2)
                    .OrderBy(m => m.Sort).OrderBy(m => m.CreateTime).ToList(m => new FAQClassDto
                    {
                        Id = m.Id,
                        Name = m.Title,
                        Sort = m.Sort
                    });
                entity.TeacherDetail = teacherList;
                //学生
                var studentList = DbContext.FreeSql.GetRepository<YssxFAQInfo>().Select.Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.RoleType == 3)
                    .OrderBy(m => m.Sort).OrderBy(m => m.CreateTime).ToList(m => new FAQClassDto
                    {
                        Id = m.Id,
                        Name = m.Title,
                        Sort = m.Sort
                    });
                entity.StudentDetail = studentList;

                return new ResponseContext<FAQRoleInfoDto> { Code = CommonConstants.SuccessCode, Data = entity, Msg = "成功" };
            });
        }





        #endregion

    }
}
