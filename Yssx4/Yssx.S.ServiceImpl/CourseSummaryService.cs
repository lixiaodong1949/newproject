﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class CourseSummaryService : ICourseSummaryService
    {
        public async Task<ResponseContext<SummaryDto>> AddSummary(SummaryDto model)
        {
            if (model == null) return new ResponseContext<SummaryDto> { Code = CommonConstants.ErrorCode };
            YssxCourseSummary data = new YssxCourseSummary();
            if (model.Id == 0)
            {
                data = await DbContext.FreeSql.GetRepository<YssxCourseSummary>().InsertAsync(new YssxCourseSummary { Id = IdWorker.NextId(), CourseId = model.CourseId, Summary = model.Name, CreateBy = model.CreateBy, Type = 1, Sort = model.Sort });
                if (data == null) return new ResponseContext<SummaryDto> { Code = CommonConstants.ErrorCode };
            }
            else
            {
                data.Id = model.Id;
                data.Summary = model.Name;
                data.Sort = model.Sort;
                data.UpdateTime = DateTime.Now;
                int count = await DbContext.FreeSql.GetRepository<YssxCourseSummary>().UpdateDiy.SetSource(data).UpdateColumns(x=> new {x.Summary,x.Sort,x.CreateTime }).Where(x => x.Id == model.Id).ExecuteAffrowsAsync();
            }
            return new ResponseContext<SummaryDto> { Data = new SummaryDto { Id = data.Id, Name = data.Summary, CreateBy = data.CreateBy } };
        }

        public async Task<ResponseContext<List<SummaryDto>>> GetSummaryList(long cid)
        {
            var data = await DbContext.FreeSql.GetRepository<YssxCourseSummary>().Where(x => x.CourseId == cid).ToListAsync();
            return new ResponseContext<List<SummaryDto>> { Data = DataToModel(data) };
        }

        public async Task<ResponseContext<bool>> RemoveSummary(long id)
        {
            bool b = await DbContext.FreeSql.GetRepository<YssxCourseSummary>().DeleteAsync(x => x.Id == id) > 0;
            return new ResponseContext<bool> { Data = b };
        }

        #region 私有方法
        private List<SummaryDto> DataToModel(List<YssxCourseSummary> sources)
        {
            if (sources == null) return null;
            return sources.Select(x => new SummaryDto { Id = x.Id, CreateBy = x.CreateBy, CourseId = x.CourseId, Name = x.Summary, Sort = x.Sort }).OrderBy(x => x.Sort).ToList();
        }
        #endregion
    }
}
