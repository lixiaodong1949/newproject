﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 印章服务接口
    /// </summary>
    public class StampService: IStampService
    {
        /// <summary>
        /// 保存印章
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveStamp(StampRequest model,long userId)
        {
            return await Task.Run(() =>
            {
                YssxStamp yssxStamp = model.MapTo<YssxStamp>();
                if (model.Id == 0)
                {
                    yssxStamp.Id = IdWorker.NextId();
                    yssxStamp.CreateBy = userId;
                }
                else
                {
                    yssxStamp.UpdateBy = userId;
                    yssxStamp.UpdateTime = DateTime.Now;
                }
                var result = model.Id > 0 ? UpdateStamp(yssxStamp) : AddStamp(yssxStamp);
                return result;
            });
        }
        /// <summary>
        /// 新增印章
        /// </summary>
        /// <param name="yssxCase"></param>
        /// <param name="positions"></param>
        /// <returns></returns>
        private static ResponseContext<bool> AddStamp(YssxStamp yssxStamp)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            try
            {
                DbContext.FreeSql.Insert(yssxStamp).ExecuteAffrows();
            }
            catch (Exception)
            {
                result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            }
            return result;
            
        }
        private static ResponseContext<bool> UpdateStamp(YssxStamp yssxStamp)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            try
            {
                DbContext.FreeSql.Update<YssxStamp>(yssxStamp).SetSource(yssxStamp).ExecuteAffrows();
            }
            catch (Exception)
            {
                result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            }
            return result;
        }
        /// <summary>
        /// 删除印章
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteStamp(long id)
        {
            //删除案例主表  逻辑删除
            await DbContext.FreeSql.GetRepository<YssxStamp>().UpdateDiy.Set(c => new YssxStamp
            {
                IsDelete = CommonConstants.IsDelete,
                UpdateTime = DateTime.Now
            }).Where(c => c.Id == id).ExecuteAffrowsAsync();
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
        /// <summary>
        /// 获取印章列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<PageResponse<StampDto>> GetStampList(StampListRequest model,long userId)
        {
            var select = DbContext.FreeSql.Select<YssxStamp>()
                .Where(c => c.CreateBy==userId && c.IsDelete == CommonConstants.IsNotDelete);

            if (model.CaseId > 0)
                select.Where(c => c.CaseId == model.CaseId);

            var totalCount = select.Count();
            var sql = select.Page(model.PageIndex, model.PageSize).ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<StampDto>(sql);

            var pageData = new PageResponse<StampDto>() { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            return pageData;
        }
    }
}
