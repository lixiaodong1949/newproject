﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 同步实训 案例部门 岗位
    /// </summary>
    public class CaseDepartmentServiceNew : ICaseDepartmentServiceNew
    {
        #region 部门管理（同步实训）
        /// <summary>
        /// 获取部门管理列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<DepartmentDtoNew>> GetDepartmentList(DepartmentRequestNew model)
        {
            long totalCount = 0;

            var list = await DbContext.FreeSql.GetGuidRepository<YssxDepartmentNew>()
                 .Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                 .WhereIf(!string.IsNullOrEmpty(model.DepartmentName), x => x.DepartmentName.Contains(model.DepartmentName))
                 .WhereIf(model.CaseId > 0, x => x.CaseId == model.CaseId)
                 .Count(out totalCount)
                 .Page(model.PageIndex, model.PageSize)
                 .OrderByDescending(x => x.CreateTime).ToListAsync<DepartmentDtoNew>();

            return new PageResponse<DepartmentDtoNew> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = list };
        }

        /// <summary>
        /// 获取部门管理列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<DepartmentDtoNew>>> GetDepartmentAll(DepartmentRequestNew model)
        {
            var list = await DbContext.FreeSql.GetGuidRepository<YssxDepartmentNew>()
                 .Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                 .WhereIf(!string.IsNullOrEmpty(model.DepartmentName), x => x.DepartmentName.Contains(model.DepartmentName))
                 .WhereIf(model.CaseId > 0, x => x.CaseId == model.CaseId)
                 .OrderBy(x => x.Sort)
                 .OrderByDescending(x => x.Id).ToListAsync<DepartmentDtoNew>();

            return new ResponseContext<List<DepartmentDtoNew>> { Code = CommonConstants.SuccessCode, Data = list };
        }

        /// <summary>
        /// 新增或修改部门管理
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditDepartment(DepartmentDtoNew model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.DepartmentName))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "部门名称不能为空" };
            if (DbContext.FreeSql.Select<YssxDepartmentNew>().Any(m => m.DepartmentName == model.DepartmentName && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "部门名称重复" };

            bool state = true;
            if (model.Id == 0)
            {
                var depart = model.MapTo<YssxDepartmentNew>();

                depart.Id = IdWorker.NextId();
                depart.CreateBy = currentUserId;

                var entity = await DbContext.FreeSql.GetRepository<YssxDepartmentNew>().InsertAsync(depart);
                state = entity != null;
            }
            else
            {
                YssxDepartmentNew entity = await DbContext.FreeSql.GetRepository<YssxDepartmentNew>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该部门!" };

                var depart = model.MapTo<YssxDepartmentNew>();

                depart.UpdateBy = currentUserId;
                depart.UpdateTime = DateTime.Now;

                var resUpd = await DbContext.FreeSql.Update<YssxDepartmentNew>().SetSource(depart).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// 删除部门管理
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveDepartment(long id, long currentUserId)
        {
            YssxDepartmentNew entity = await DbContext.FreeSql.GetRepository<YssxDepartmentNew>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该部门!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<YssxDepartmentNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }
        #endregion

        #region 岗位管理（同步实训）
        /// <summary>
        /// 获取岗位管理列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<PositionDtoNew>> GetPositionList(PositionRequestNew model)
        {
            List<YssxPositionNew> list = await DbContext.FreeSql.GetGuidRepository<YssxPositionNew>().Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.PositionName), x => x.Name.Contains(model.PositionName)).OrderByDescending(x => x.CreateTime).ToListAsync();
            var totalCount = list.Count();
            var selectData = list.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).Select(x => new PositionDtoNew
            {
                Id = x.Id,
                Describe = x.Describe,
                PositionName = x.Name

            }).ToList();
            return new PageResponse<PositionDtoNew> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
        }
        /// <summary>
        /// 根据Id获取岗位管理详情
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PositionDtoNew>> GetPositionById(long id)
        {
            var entity = await DbContext.FreeSql.GetGuidRepository<YssxPositionNew>(x => x.IsDelete == CommonConstants.IsNotDelete)
                .Where(x => x.Id == id)
                .FirstAsync(x => new PositionDtoNew
                {
                    Id = x.Id,
                    Describe = x.Describe,
                    PositionName = x.Name

                });

            return new ResponseContext<PositionDtoNew> { Code = CommonConstants.SuccessCode, Data = entity };
        }

        /// <summary>
        /// 新增或修改岗位管理
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditPosition(PositionDtoNew model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.PositionName))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "岗位名称不能为空" };
            if (DbContext.FreeSql.Select<YssxPositionNew>().Any(m => m.Name == model.PositionName && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "岗位名称重复" };

            bool state = true;
            if (model.Id == 0)
            {
                var entity = await DbContext.FreeSql.GetRepository<YssxPositionNew>().InsertAsync(new YssxPositionNew
                {
                    Id = IdWorker.NextId(),
                    Describe = model.Describe,
                    Name = model.PositionName,
                    CreateBy = currentUserId
                });
                state = entity != null;
            }
            else
            {
                YssxPositionNew entity = await DbContext.FreeSql.GetRepository<YssxPositionNew>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该岗位!" };
                entity.Describe = model.Describe;
                entity.Name = model.PositionName;
                entity.UpdateBy = currentUserId;
                entity.UpdateTime = DateTime.Now;
                var resUpd = await DbContext.FreeSql.Update<YssxPositionNew>().SetSource(entity).UpdateColumns(a => new { a.Describe, a.Name, a.UpdateBy, a.UpdateTime }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// 删除岗位管理
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemovePosition(long id, long currentUserId)
        {
            YssxPositionNew entity = await DbContext.FreeSql.GetRepository<YssxPositionNew>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该岗位!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<YssxPositionNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }
        #endregion

    }
}
