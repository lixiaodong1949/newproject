﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.Repository.Extensions;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.Service
{
    /// <summary>
    /// 实训类型业务服务
    /// </summary>
    public class DrillTypeService : IDrillTypeService
    {
        /// <summary>
        /// 保存实训类型 新增和修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Save(DrillTypeDto model, bkUserTicket currentUser)
        {
            if (model.Id <= 0)//新增
            {
                return await Add(model, currentUser);
            }
            else //修改
            {
                return await Update(model, currentUser);
            }
        }

        /// <summary>
        /// 保存实训类型 新增
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Add(DrillTypeDto model, bkUserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<YssxDrillType>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasName = await repo
             .Where(e => e.Name == model.Name && e.CaseId == model.CaseId)
             .AnyAsync();

            if (hasName)
            {
                result.Msg = $"实训类型名称重复。";
                return result;
            }
            #endregion

            YssxDrillType DrillType = CreateDrillType(model, currentUser);

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo2 = uow.GetRepository<YssxDrillType>();

                    await repo2.InsertAsync(DrillType);

                    uow.Commit();
                    result.Msg = DrillType.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            result.Code = CommonConstants.SuccessCode;
            result.Data = true;
            return result;
        }
        /// <summary>
        /// 保存实训类型 修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Update(DrillTypeDto model, bkUserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);
            var repo = DbContext.FreeSql.GetRepository<YssxDrillType>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var drillType = await repo
                   .Where(e => e.Id == model.Id)
                   .FirstAsync();

            if (drillType == null)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"实训类型不存在，Id:{model.Id}", false);

            var hasName = await repo
             .Where(e => e.Name == model.Name && e.Id != model.Id && e.CaseId == model.CaseId)
             .AnyAsync();

            if (hasName)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"实训类型名称重复。", false);

            #endregion

            #region 保存到数据库中

            drillType.Name = model.Name;
            drillType.Sort = model.Sort;
            drillType.Status = model.Status;

            await repo.UpdateAsync(drillType);

            result.Msg = drillType.Id.ToString();
            #endregion

            result.Code = CommonConstants.SuccessCode;
            result.Data = true;

            return result;
        }

        /// <summary>
        /// 构建 实训类型对象 新增和修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private static YssxDrillType CreateDrillType(DrillTypeDto model, bkUserTicket currentUser)
        {
            var DrillType = model.MapTo<YssxDrillType>();

            if (model.Id <= 0)
            {
                DrillType.Id = IdWorker.NextId();//获取唯一Id
                DrillType.CreateBy = currentUser.Id;
            }
            else
            {
                DrillType.UpdateBy = currentUser.Id;
                DrillType.UpdateTime = DateTime.Now;
            }

            return DrillType;
        }

        /// <summary>
        /// 根据Id删除案例实训类型
        /// </summary>
        /// <param name="id">案例实训类型Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteById(long id, bkUserTicket currentUser)
        {
            //删除
            await DbContext.FreeSql.GetRepository<YssxDrillType>().UpdateDiy.Set(c => new YssxDrillType
            {
                IsDelete = CommonConstants.IsDelete,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(c => c.Id == id).ExecuteAffrowsAsync();

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }
        /// <summary>
        /// 根据id获取实训类型信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<DrillTypeDto>> GetInfoById(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<YssxDrillType>().Select
               .Where(e => e.Id == id)
               .FirstAsync<DrillTypeDto>();

            return new ResponseContext<DrillTypeDto> { Code = CommonConstants.SuccessCode, Data = entity };
        }
        /// <summary>
        /// 获取父级实训类型列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<DrillTypeListDto>>> GetList(DrillTypeQueryDto query)
        {
            long count = 0;

            var entity = await DbContext.FreeSql.GetRepository<YssxDrillType>(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .Where(e => e.CaseId == query.CaseId)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderBy(e => e.CreateTime)
                   .Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .ToListAsync<DrillTypeListDto>();

            var data = new PageResponse<DrillTypeListDto>
            {
                Data = entity,
                RecordCount = count,
                PageIndex = query.PageIndex,
                PageSize = query.PageSize
            };
            return new ResponseContext<PageResponse<DrillTypeListDto>> { Code = CommonConstants.SuccessCode, Data = data };
        }
        /// <summary>
        /// 获取实训类型名称列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<DrillTypeNameListDto>>> GetNameList(DrillTypeNameListRequestDto query)
        {
            var data = await DbContext.FreeSql.GetRepository<YssxDrillType>(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .Where(e => e.CaseId == query.CaseId)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .WhereIf(query.Status.HasValue, e => e.Status == query.Status.Value)
                   .OrderBy(e => e.Sort)
                   .OrderBy(e => e.Id)
                   .ToListAsync<DrillTypeNameListDto>();

            return new ResponseContext<List<DrillTypeNameListDto>> { Code = CommonConstants.SuccessCode, Data = data };
        }

        /// <summary>
        /// 指导编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetGuideInfo(SetGuideInfoDto model, bkUserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);
            var repo = DbContext.FreeSql.GetRepository<YssxDrillType>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var drillType = await repo
                   .Where(e => e.Id == model.Id)
                   .FirstAsync();

            if (drillType == null)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"实训类型不存在，Id:{model.Id}", false);

            #endregion

            drillType.FlowDescription = model.FlowDescription;
            drillType.TaskDescription = model.TaskDescription;

            await repo.UpdateAsync(drillType);

            result.Code = CommonConstants.SuccessCode;
            result.Data = true;
            result.Msg = drillType.Id.ToString();

            return result;
        }


        /// <summary>
        /// 获取指导编辑信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<SetGuideInfoDto>> GetGuideInfoById(long dillTypeId)
        {
            var entity = await DbContext.FreeSql.GetRepository<YssxDrillType>(e => e.IsDelete == e.IsDelete)
            .Where(e => e.Id == dillTypeId)
            .FirstAsync<SetGuideInfoDto>();

            return new ResponseContext<SetGuideInfoDto> { Code = CommonConstants.SuccessCode, Data = entity };
        }
    }
}
