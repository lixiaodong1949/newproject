﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 流程业务服务
    /// </summary>
    public class FlowService : IFlowService
    {
        /// <summary>
        /// 保存流程 新增和修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Save(FlowDto model, bkUserTicket currentUser)
        {
            if (model.Id <= 0)//新增
            {
                return await Add(model, currentUser);
            }
            else //修改
            {
                return await Update(model, currentUser);
            }
        }

        /// <summary>
        /// 保存流程 新增
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Add(FlowDto model, bkUserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<YssxFlow>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasName = await repo
             .Where(e => e.Name == model.Name && e.DrillTypeId == model.DrillTypeId)
             .AnyAsync();

            if (hasName)
            {
                result.Msg = $"流程名称重复。";
                return result;
            }
            #endregion

            YssxFlow Flow = CreateFlow(model, currentUser);

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo2 = uow.GetRepository<YssxFlow>();

                    await repo2.InsertAsync(Flow);

                    uow.Commit();
                    result.Msg = Flow.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            result.Code = CommonConstants.SuccessCode;
            result.Data = true;
            return result;
        }

        /// <summary>
        /// 保存流程 修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Update(FlowDto model, bkUserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);
            var repo = DbContext.FreeSql.GetRepository<YssxFlow>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasFlow = await repo
                   .Where(e => e.Id == model.Id)
                   .AnyAsync();

            if (!hasFlow)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"流程不存在，Id:{model.Id}", false);

            var hasName = await repo
             .Where(e => e.Name == model.Name && e.Id != model.Id && e.DrillTypeId == model.DrillTypeId)
             .AnyAsync();

            if (hasName)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"流程名称重复。", false);

            #endregion

            YssxFlow Flow = CreateFlow(model, currentUser);

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo2 = uow.GetRepository<YssxFlow>();

                    await repo2.UpdateAsync(Flow);

                    uow.Commit();
                    result.Msg = Flow.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            result.Code = CommonConstants.SuccessCode;
            result.Data = true;

            return result;
        }

        /// <summary>
        /// 构建 流程对象 新增和修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private static YssxFlow CreateFlow(FlowDto model, bkUserTicket currentUser)
        {
            var Flow = model.MapTo<YssxFlow>();

            if (model.Id <= 0)
            {
                Flow.Id = IdWorker.NextId();//获取唯一Id
                Flow.CreateBy = currentUser.Id;
            }
            else
            {
                Flow.UpdateBy = currentUser.Id;
                Flow.UpdateTime = DateTime.Now;
            }

            return Flow;
        }

        /// <summary>
        /// 根据Id删除案例流程
        /// </summary>
        /// <param name="id">案例流程Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteById(long id, bkUserTicket currentUser)
        {
            //删除
            await DbContext.FreeSql.GetRepository<YssxFlow>().UpdateDiy.Set(c => new YssxFlow
            {
                IsDelete = CommonConstants.IsDelete,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(c => c.Id == id).ExecuteAffrowsAsync();

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 根据id获取流程信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<FlowDto>> GetInfoById(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<YssxFlow>().Select
               .Where(e => e.Id == id)
               .FirstAsync<FlowDto>();

            return new ResponseContext<FlowDto> { Code = CommonConstants.SuccessCode, Data = entity };
        }

        /// <summary>
        /// 获取流程列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<FlowListDto>>> GetList(FlowQueryDto query)
        {
            long count = 0;

            var entity = await DbContext.FreeSql.GetRepository<YssxFlow>(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .Where(e => e.DrillTypeId == query.DrillTypeId)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderBy(e => e.CreateTime)
                   .OrderBy(e => e.Id)
                   .Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .ToListAsync<FlowListDto>();

            var data = new PageResponse<FlowListDto>
            {
                Data = entity,
                RecordCount = count,
                PageIndex = query.PageIndex,
                PageSize = query.PageSize
            };
            return new ResponseContext<PageResponse<FlowListDto>> { Code = CommonConstants.SuccessCode, Data = data };
        }

        /// <summary>
        /// 获取流程名称列表 前台
        /// </summary>
        /// <returns></returns> 
        public async Task<ResponseContext<List<FlowNameListDto>>> GetNameList(FlowNameQueryRequestDto query)
        {
            var entity = await DbContext.FreeSql.GetRepository<YssxFlow>(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .Where(e => e.Status == Status.Enable && e.DrillTypeId == query.DrillTypeId)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderBy(e => e.Sort)
                   .OrderBy(e => e.Id)
                   .ToListAsync<FlowNameListDto>();

            return new ResponseContext<List<FlowNameListDto>> { Code = CommonConstants.SuccessCode, Data = entity };
        }
    }
}
