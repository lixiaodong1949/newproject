﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 同步实训 摘要
    /// </summary>
    public class SummaryServiceNew:ISummaryServiceNew
    {
        public async Task<ResponseContext<SummaryDtoNew>> AddSummary(SummaryDtoNew model)
        {
            if (model == null) return new ResponseContext<SummaryDtoNew> { Code = CommonConstants.ErrorCode };
            YssxSummaryNew data = new YssxSummaryNew();
            if (model.Id == 0)
            {
                data = await DbContext.FreeSql.GetRepository<YssxSummaryNew>().InsertAsync(new YssxSummaryNew { Id = IdWorker.NextId(), Summary = model.Name, EnterpriseId = model.EnterpriseId, Type = 1, Sort = model.Sort });
                if (data == null) return new ResponseContext<SummaryDtoNew> { Code = CommonConstants.ErrorCode };
            }
            else
            {
                data.Id = model.Id;
                data.Summary = model.Name;
                data.EnterpriseId = model.EnterpriseId;
                data.Sort = model.Sort;
                int count = await DbContext.FreeSql.GetRepository<YssxSummaryNew>().UpdateAsync(data);
            }
            return new ResponseContext<SummaryDtoNew> { Data = new SummaryDtoNew { Id = data.Id, Name = data.Summary, EnterpriseId = data.EnterpriseId } };
        }

        public async Task<ResponseContext<List<SummaryDtoNew>>> GetSummaryList(long id)
        {
            var data = await DbContext.FreeSql.GetRepository<YssxSummaryNew>().Where(x => x.EnterpriseId == id).ToListAsync();
            return new ResponseContext<List<SummaryDtoNew>> { Data = DataToModel(data) };
        }

        public async Task<ResponseContext<bool>> RemoveSummary(long id)
        {
            bool b = await DbContext.FreeSql.GetRepository<YssxSummaryNew>().DeleteAsync(x => x.Id == id) > 0;
            return new ResponseContext<bool> { Data = b };
        }

        #region 私有方法
        private List<SummaryDtoNew> DataToModel(List<YssxSummaryNew> sources)
        {
            if (sources == null) return null;
            return sources.Select(x => new SummaryDtoNew { Id = x.Id, EnterpriseId = x.EnterpriseId, Name = x.Summary, Sort = x.Sort }).OrderBy(x => x.Sort).ToList();
        }
        #endregion
    }
}
