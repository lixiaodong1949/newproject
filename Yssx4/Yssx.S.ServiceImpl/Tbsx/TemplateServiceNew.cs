﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 同步实训 模板管理
    /// </summary>
    public class TemplateServiceNew: ITemplateServiceNew
    {
        /// <summary>
        /// 获取模板类型树列表
        /// </summary>
        public async Task<PageResponse<TemplateTreeItemDtoNew>> GetTemplateTree()
        {
            var types = DbContext.FreeSql.GetRepository<YssxTemplateTypeNew>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).ToList(m => new TemplateTreeItemDtoNew
            {
                Id = m.Id,
                Name = m.Name,
                Pid = m.ParentId
            });
            return new PageResponse<TemplateTreeItemDtoNew> { PageSize = types.Count, PageIndex = 1, RecordCount = types.Count, Data = types };
        }

        /// <summary>
        /// 获取指定模板类型
        /// </summary>
        /// <param name="id"></param> 
        /// <returns></returns>
        public async Task<ResponseContext<TemplateTreeItemDtoNew>> GetTemplateTypeById(long id)
        {
            var rTemplateTypeData = DbContext.FreeSql.GetRepository<YssxTemplateTypeNew>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete);
            var res = rTemplateTypeData.Any() ? rTemplateTypeData.First(m => new TemplateTreeItemDtoNew { Id = m.Id, Name = m.Name, Pid = m.ParentId }) : new TemplateTreeItemDtoNew();
            return new ResponseContext<TemplateTreeItemDtoNew> { Code = CommonConstants.SuccessCode, Data = res };
        }
        /// <summary>
        /// 修改模板类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ResponseContext<bool> UpdateTemplateType(TemplateTreeItemDtoNew model, long currentUserId)
        {
            var rep = DbContext.FreeSql.GetRepository<YssxTemplateTypeNew>();
            var oldTemplateType = rep.Where(m => m.Id == model.Id).First();
            if (oldTemplateType == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

            if (rep.Select.Any(m => (m.Name == model.Name) && m.Id != oldTemplateType.Id))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称重复" };


            var now = DateTime.Now;

            oldTemplateType.Name = model.Name;
            oldTemplateType.UpdateTime = DateTime.Now;
            oldTemplateType.UpdateBy = currentUserId;

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<YssxTemplateTypeNew>().SetSource(oldTemplateType).UpdateColumns(m => new { m.Name, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }
        /// <summary>
        /// 添加/编辑模板类型信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateTemplateType(TemplateTreeItemDtoNew model, long currentUserId)
        {
            if (model.Id != 0)
            {
                return UpdateTemplateType(model, currentUserId);
            }
            var templateType = new YssxTemplateTypeNew { Name = model.Name, ParentId = model.Pid };
            var now = DateTime.Now;
            templateType.Id = IdWorker.NextId();
            templateType.CreateBy = currentUserId;
            templateType.CreateTime = now;

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Insert(templateType).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 删除模板类型
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTemplateType(long tId, long currentUserId)
        {
            var delType = new YssxTemplateTypeNew
            {
                Id = tId,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = currentUserId,
                UpdateTime = DateTime.Now
            };
            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<YssxTemplateTypeNew>().SetSource(delType).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }



        /// <summary>
        /// 获取模板列表
        /// </summary>
        public async Task<PageResponse<TemplateDtoNew>> GetTemplates(GetTemplatesRequestNew model)
        {
            var select = DbContext.FreeSql.GetRepository<YssxTemplateNew>().Select.From<YssxTemplateTypeNew>((a, b) => a.InnerJoin(c => c.TemplateTypeId == b.Id)).
                Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.Id == model.TypeId);
            if (!string.IsNullOrEmpty(model.Name))
            {
                select = select.Where((a, b) => a.Name.Contains(model.Name));
            }
            var count = select.Count();
            var templateDtos = select.Page(model.PageIndex, model.PageSize).ToList((a, b) => new TemplateDtoNew
            {
                Id = a.Id,
                Name = a.Name,
                TemplateTypeName = b.Name
            });
            return new PageResponse<TemplateDtoNew> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = count, Data = templateDtos };
        }

        /// <summary>
        /// 获取指定模板
        /// </summary>
        /// <param name="id"></param> 
        /// <returns></returns>
        public async Task<ResponseContext<TemplateDtoNew>> GetTemplateById(long id)
        {
            var res = DbContext.FreeSql.GetRepository<YssxTemplateNew>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First(m => new TemplateDtoNew
            {
                Id = m.Id,
                Name = m.Name,
                FullHtml = m.FullHtml,
                ClearStyleHtml = m.ClearStyleHtml,
                AnswerJson = m.AnswerJson,
                TemplateTypeId = m.TemplateTypeId
            });
            return new ResponseContext<TemplateDtoNew> { Code = CommonConstants.SuccessCode, Data = res };
        }
        /// <summary>
        /// 修改模板
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ResponseContext<bool> UpdateTemplate(TemplateDtoNew model, long currentUserId)
        {
            var rep = DbContext.FreeSql.GetRepository<YssxTemplateNew>();
            var oldTemplate = rep.Where(m => m.Id == model.Id).First();
            if (oldTemplate == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

            if (rep.Select.Any(m => (m.Name == model.Name) && m.Id != oldTemplate.Id))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称重复" };


            var now = DateTime.Now;

            oldTemplate.Name = model.Name;
            oldTemplate.FullHtml = model.FullHtml;
            oldTemplate.ClearStyleHtml = model.ClearStyleHtml;
            oldTemplate.AnswerJson = model.AnswerJson;
            oldTemplate.UpdateTime = DateTime.Now;
            oldTemplate.UpdateBy = currentUserId;

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<YssxTemplateNew>().SetSource(oldTemplate).UpdateColumns(m => new { m.Name, m.FullHtml, m.ClearStyleHtml, m.AnswerJson, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }
        /// <summary>
        /// 添加模板信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateTemplate(TemplateDtoNew model, long currentUserId)
        {
            if (model.Id != 0)
            {
                return UpdateTemplate(model, currentUserId);
            }
            var template = new YssxTemplateNew
            {
                Name = model.Name,
                TemplateTypeId = model.TemplateTypeId,
                FullHtml = model.FullHtml,
                ClearStyleHtml = model.ClearStyleHtml,
                AnswerJson = model.AnswerJson
            };
            var now = DateTime.Now;
            template.Id = IdWorker.NextId();
            template.CreateBy = currentUserId;
            template.CreateTime = now;

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Insert(template).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }


        /// <summary>
        /// 删除指定模板
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTemplate(long tId, long currentUserId)
        {
            var delType = new YssxTemplateNew { Id = tId, IsDelete = CommonConstants.IsDelete, UpdateBy = currentUserId, UpdateTime = DateTime.Now };
            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<YssxTemplateNew>().SetSource(delType).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }


        /// <summary>
        /// 获取模板树（下拉框）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<LayuiTreeDtoNew>>> GetTemplateSelectTreeDatas()
        {
            var allTypes = DbContext.FreeSql.GetRepository<YssxTemplateTypeNew>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).ToList();
            var rootTypes = allTypes.Where(m => m.ParentId == 0).ToList();
            var tree = new List<LayuiTreeDtoNew>();
            foreach (var rootType in rootTypes)
            {
                var dto = new LayuiTreeDtoNew { id = rootType.Id, name = rootType.Name, children = new List<LayuiTreeDtoNew>() };
                tree.Add(dto);
                LoadTemplateSelectTree(allTypes, dto);
            }
            return new ResponseContext<List<LayuiTreeDtoNew>> { Code = CommonConstants.SuccessCode, Data = tree };
        }
        /// <summary>
        /// 递归获取子模板
        /// </summary>
        /// <param name="allTypes"></param>
        /// <param name="item"></param>
        private static void LoadTemplateSelectTree(List<YssxTemplateTypeNew> allTypes, LayuiTreeDtoNew item)
        {
            var temTypes = allTypes.Where(m => m.ParentId == item.id).ToList();
            if (temTypes.Count == 0)
            {
                var templates = DbContext.FreeSql.GetRepository<YssxTemplateNew>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.TemplateTypeId == item.id).ToList();
                item.children = templates.Select(m => new LayuiTreeDtoNew { id = m.Id, name = m.Name }).ToList();
            }
            else
            {
                foreach (var temType in temTypes)
                {
                    var dto = new LayuiTreeDtoNew { id = temType.Id, name = temType.Name, children = new List<LayuiTreeDtoNew>() };
                    item.children.Add(dto);
                    LoadTemplateSelectTree(allTypes, dto);
                }
            }
        }
    }
}
