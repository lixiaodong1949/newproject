﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices.Topics;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl.Topics
{
    /// <summary>
    /// 公共 - 凭证题
    /// </summary>
    public class CertificateTopicPublicServiceNew : ICertificateTopicPublicServiceNew
    {
        /// <summary>
        /// 新增/修改分录题
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> SaveCertificateTopic(CertificateTopicPubRequestNew model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                //类型从前端传过来
                if (model.CalculationScoreType == AccountEntryCalculationType.SetSingle)
                {
                    if (model.InVal <= 0 || model.OutVal <= 0)
                        return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "分录题计分方式为单独设置,外围里围值必须大于0" };
                }
                ResponseContext<long> response = new ResponseContext<long>();
                YssxTopicPublicNew yssxTopic = model.MapTo<YssxTopicPublicNew>();
                yssxTopic.IsGzip = CommonConstants.IsGzip;
                yssxTopic.QuestionType = QuestionType.AccountEntry;
                //分录题对应topic题目计分方式根据分录题的计分方式设置
                yssxTopic.CalculationType = model.CalculationScoreType == AccountEntryCalculationType.Cell ? CalculationType.FreeCalculation : CalculationType.AvgCellCount;
                //知识点
                List<YssxKnowledgePointNew> rAddKnowledgePointData = new List<YssxKnowledgePointNew>();
                yssxTopic.KnowledgePointIds = "";
                if (model.KnowledgePointDetail.Count > 0)
                {
                    var maxSort = DbContext.FreeSql.GetRepository<YssxKnowledgePointNew>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(x => x.Sort).First(x => x.Sort);
                    foreach (var itemKp in model.KnowledgePointDetail)
                    {
                        if (itemKp.Id == 0)
                        {
                            var entityKp = DbContext.FreeSql.GetRepository<YssxKnowledgePointNew>().Where(x => x.Name == itemKp.Name && x.IsDelete == CommonConstants.IsNotDelete).First();
                            if (entityKp != null)
                            {
                                itemKp.Id = entityKp.Id;
                            }
                            else
                            {
                                itemKp.Id = IdWorker.NextId();
                                rAddKnowledgePointData.Add(new YssxKnowledgePointNew()
                                {
                                    Id = itemKp.Id,
                                    GroupId = itemKp.Id,
                                    Name = itemKp.Name,
                                    ParentId = 0,
                                    Sort = maxSort + rAddKnowledgePointData.Count + 1,
                                    CreateBy = currentUserId
                                });
                            }
                        }
                        yssxTopic.KnowledgePointIds = string.IsNullOrEmpty(yssxTopic.KnowledgePointIds) ? itemKp.Id.ToString() : yssxTopic.KnowledgePointIds + "," + itemKp.Id.ToString();
                    }
                }

                YssxCertificateTopicPublicNew yssxCore = model.MapTo<YssxCertificateTopicPublicNew>();
                List<YssxPubCertificateDataRecordNew> yssxCertificateData = model.DataRecords.MapTo<List<YssxPubCertificateDataRecordNew>>();
                List<YssxTopicPublicFileNew> yssxTopicFiles = new List<YssxTopicPublicFileNew>();
                StringBuilder stringBuilder = new StringBuilder();

                if (model.QuestionFile != null)
                {
                    foreach (var item in model.QuestionFile)
                    {
                        YssxTopicPublicFileNew YssxTopicPublicFileNew = new YssxTopicPublicFileNew();
                        YssxTopicPublicFileNew.Id = IdWorker.NextId();
                        YssxTopicPublicFileNew.Name = item.Name;
                        YssxTopicPublicFileNew.Url = item.Url;
                        YssxTopicPublicFileNew.CreateTime = DateTime.Now;
                        stringBuilder.Append(YssxTopicPublicFileNew.Id).Append(",");
                        yssxTopicFiles.Add(YssxTopicPublicFileNew);
                    }
                }
                if (stringBuilder.Length > 0)
                {
                    yssxCore.ImageIds = stringBuilder.ToString().Remove(stringBuilder.Length - 1, 1);
                    yssxTopic.TopicFileIds = stringBuilder.ToString().Remove(stringBuilder.Length - 1, 1);
                    yssxCore.InvoicesNum = model.QuestionFile.Count;
                }

                if (model.Id == 0)
                {
                    yssxCore.Id = IdWorker.NextId();
                    yssxCore.CreateBy = currentUserId;
                    yssxTopic.CreateBy = currentUserId;
                }
                else
                {
                    YssxCertificateTopicPublicNew yssxCoreCertificate = DbContext.FreeSql.Select<YssxCertificateTopicPublicNew>().Where(x => x.TopicId == yssxTopic.Id).First();
                    yssxCore.Id = yssxCoreCertificate.Id;
                    yssxCore.UpdateBy = currentUserId;
                    yssxTopic.UpdateBy = currentUserId;
                }
                if (yssxCertificateData != null)
                {
                    yssxCertificateData.ForEach(x =>
                    {
                        x.Id = IdWorker.NextId();
                        x.CertificateNo = yssxCore.CertificateNo;
                        x.CertificateDate = yssxCore.CertificateDate;
                        x.CertificateTopicId = yssxCore.Id;
                        x.CreateTime = DateTime.Now;
                    });
                }
                response = model.Id == 0 ? AddCertificateTopic(yssxTopic, yssxCore, yssxCertificateData, yssxTopicFiles, rAddKnowledgePointData) : UpdateCertificateTopic(yssxTopic, yssxCore, yssxCertificateData, yssxTopicFiles, rAddKnowledgePointData);
                return response;
            });
        }
        /// <summary>
        /// 新增凭证
        /// </summary>
        /// <param name="yssxTopic"></param>
        /// <param name="yssxCore"></param>
        /// <param name="yssxCertificateData"></param>
        /// <param name="yssxTopicFiles"></param>
        /// <returns></returns>
        private ResponseContext<long> AddCertificateTopic(YssxTopicPublicNew yssxTopic, YssxCertificateTopicPublicNew yssxCore, List<YssxPubCertificateDataRecordNew> yssxCertificateData, List<YssxTopicPublicFileNew> yssxTopicFiles, List<YssxKnowledgePointNew> yssxKnowledgePoint)
        {
            ResponseContext<long> response = new ResponseContext<long>();
            yssxTopic.Id = IdWorker.NextId();
            yssxTopic.CreateTime = DateTime.Now;
            yssxCore.TopicId = yssxTopic.Id;
            yssxCore.CreateTime = DateTime.Now;

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    DbContext.FreeSql.Insert<YssxPubCertificateDataRecordNew>().AppendData(yssxCertificateData).ExecuteAffrows();
                    DbContext.FreeSql.Insert(yssxTopic).ExecuteAffrows();
                    DbContext.FreeSql.Insert(yssxCore).ExecuteAffrows();
                    DbContext.FreeSql.Insert<YssxTopicPublicFileNew>().AppendData(yssxTopicFiles).ExecuteAffrows();
                    //添加新的知识点
                    if (yssxKnowledgePoint.Count > 0)
                    {
                        DbContext.FreeSql.Insert<YssxKnowledgePointNew>().AppendData(yssxKnowledgePoint).ExecuteAffrows();
                    }
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = ex.Message };
                }
            }
            response.Code = CommonConstants.SuccessCode;
            response.Data = yssxTopic.Id;
            return response;
        }
        /// <summary>
        /// 修改凭证
        /// </summary>
        /// <param name="yssxTopic"></param>
        /// <param name="yssxCore"></param>
        /// <param name="yssxCertificateData"></param>
        /// <param name="yssxTopicFiles"></param>
        /// <returns></returns>
        private ResponseContext<long> UpdateCertificateTopic(YssxTopicPublicNew yssxTopic, YssxCertificateTopicPublicNew yssxCore, List<YssxPubCertificateDataRecordNew> yssxCertificateData, List<YssxTopicPublicFileNew> yssxTopicFiles, List<YssxKnowledgePointNew> yssxKnowledgePoint)
        {
            ResponseContext<long> response = new ResponseContext<long>();
            YssxCertificateTopicPublicNew yssxCoreCertificate = DbContext.FreeSql.Select<YssxCertificateTopicPublicNew>().Where(x => x.TopicId == yssxTopic.Id).First();
            //yssxCore.Id = yssxCoreCertificate.Id;
            if (yssxCoreCertificate == null)
            {
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "没有找到数据" };
            }
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(yssxCoreCertificate.ImageIds))
                    {
                        string[] ids = yssxCoreCertificate.ImageIds.Split(',');

                        foreach (var item in ids)
                        {
                            DbContext.FreeSql.Delete<YssxTopicPublicFileNew>().Where(x => x.Id == long.Parse(item)).ExecuteAffrows();
                        }
                    }
                    DbContext.FreeSql.Delete<YssxPubCertificateDataRecordNew>().Where(x => x.CertificateTopicId == yssxCoreCertificate.Id).ExecuteAffrows();
                    yssxCore.TopicId = yssxCoreCertificate.TopicId;
                    yssxTopic.Id = yssxCoreCertificate.TopicId;
                    yssxTopic.UpdateTime = DateTime.Now;

                    DbContext.FreeSql.Insert<YssxPubCertificateDataRecordNew>().AppendData(yssxCertificateData).ExecuteAffrows();
                    DbContext.FreeSql.Update<YssxTopicPublicNew>().SetSource(yssxTopic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.IsDelete }).ExecuteAffrows();
                    DbContext.FreeSql.Update<YssxCertificateTopicPublicNew>().SetSource(yssxCore).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.IsDelete }).ExecuteAffrows();
                    DbContext.FreeSql.Insert<YssxTopicPublicFileNew>().AppendData(yssxTopicFiles).ExecuteAffrows();
                    //添加新的知识点
                    if (yssxKnowledgePoint.Count > 0)
                    {
                        DbContext.FreeSql.Insert<YssxKnowledgePointNew>().AppendData(yssxKnowledgePoint).ExecuteAffrows();
                    }
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = ex.Message };
                }
            }
            response.Code = CommonConstants.SuccessCode;
            response.Data = yssxTopic.Id;
            return response;
        }

        /// <summary>
        /// 获取单个凭证题
        /// </summary>
        /// <param name="id">题目id</param>
        /// <returns></returns>
        public async Task<ResponseContext<CertificateTopicPubRequestNew>> GetCoreCertificateTopic(long id)
        {
            ResponseContext<CertificateTopicPubRequestNew> response = new ResponseContext<CertificateTopicPubRequestNew>();
            YssxCertificateTopicPublicNew yssxCoreCertificate = await DbContext.FreeSql.Select<YssxCertificateTopicPublicNew>().Where(x => x.TopicId == id).FirstAsync();
            if (yssxCoreCertificate != null)
            {
                YssxTopicPublicNew yssxTopic = await DbContext.FreeSql.Select<YssxTopicPublicNew>().Where(x => x.Id == yssxCoreCertificate.TopicId).FirstAsync();

                if (yssxTopic != null)
                {
                    response.Data = yssxTopic.MapTo<CertificateTopicPubRequestNew>();
                    if (!string.IsNullOrWhiteSpace(yssxCoreCertificate.ImageIds))
                    {
                        string[] ids = yssxCoreCertificate.ImageIds.Split(',');
                        List<QuestionPubFileNew> files = new List<QuestionPubFileNew>();
                        foreach (var item in ids)
                        {
                            if (string.IsNullOrWhiteSpace(item)) continue;
                            YssxTopicPublicFileNew YssxTopicPublicFileNew = DbContext.FreeSql.Select<YssxTopicPublicFileNew>().Where(x => x.Id == long.Parse(item)).First();
                            if (YssxTopicPublicFileNew == null) { continue; }
                            files.Add(new QuestionPubFileNew { Name = YssxTopicPublicFileNew.Name, Url = YssxTopicPublicFileNew.Url });
                        }
                        response.Data.QuestionFile = files;
                    }
                    if (!string.IsNullOrEmpty(yssxTopic.KnowledgePointIds))
                    {
                        List<TopicPubKnowledgePointDtoNew> rTopickpList = new List<TopicPubKnowledgePointDtoNew>();
                        var rKnowledgePointIds = yssxTopic.KnowledgePointIds.SplitToArray<long>(',');
                        var rKpData = DbContext.FreeSql.Select<YssxKnowledgePointNew>().Where(a => rKnowledgePointIds.Contains(a.Id) && a.IsDelete == CommonConstants.IsNotDelete).OrderBy(a => a.Sort).ToList();
                        if (rKnowledgePointIds.Count() > 0 && rKpData.Count > 0)
                        {
                            foreach (var item in rKnowledgePointIds)
                            {
                                if (rKpData.Any(m => m.Id == item))
                                    rTopickpList.Add(new TopicPubKnowledgePointDtoNew
                                    {
                                        Id = item,
                                        Name = rKpData.First(m => m.Id == item).Name
                                    });
                            }
                        }
                        response.Data.KnowledgePointDetail = rTopickpList;
                    }

                    response.Data.Id = yssxCoreCertificate.Id;
                    response.Data.CertificateDate = yssxCoreCertificate.CertificateDate.ToString();
                    response.Data.CertificateNo = yssxCoreCertificate.CertificateNo;
                    response.Data.EnterpriseId = yssxCoreCertificate.EnterpriseId;
                    response.Data.AccountingManager = yssxCoreCertificate.AccountingManager;
                    response.Data.Auditor = yssxCoreCertificate.Auditor;
                    response.Data.Cashier = yssxCoreCertificate.Cashier;
                    response.Data.Creator = yssxCoreCertificate.Creator;
                    response.Data.IsDisableAM = yssxCoreCertificate.IsDisableAM;
                    response.Data.IsDisableAuditor = yssxCoreCertificate.IsDisableAuditor;
                    response.Data.IsDisableCashier = yssxCoreCertificate.IsDisableCashier;
                    response.Data.IsDisableCreator = yssxCoreCertificate.IsDisableCreator;
                    response.Data.CalculationScoreType = yssxCoreCertificate.CalculationScoreType;
                    response.Data.InVal = yssxCoreCertificate.InVal;
                    response.Data.OutVal = yssxCoreCertificate.OutVal;
                    response.Data.DataRecords = DbContext.FreeSql.Select<YssxPubCertificateDataRecordNew>()
                        .Where(x => x.CertificateTopicId == yssxCoreCertificate.Id).ToList().MapTo<List<PubCertificateDataRecordNew>>();

                    response.Code = CommonConstants.SuccessCode;
                }
                else
                {
                    response.Code = CommonConstants.ErrorCode;
                    response.Msg = "没有找到数据!";
                }
            }
            else
            {
                response.Code = CommonConstants.ErrorCode;
                response.Msg = "没有找到数据!";
            }
            return response;
        }
        /// <summary>
        /// 根据凭证id删除凭证(分录题)
        /// </summary>
        /// <param name="id">凭证id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCertificateTopicById(long id, long currentUserId)
        {
            YssxCertificateTopicPublicNew certificateTopic = await DbContext.FreeSql.Select<YssxCertificateTopicPublicNew>().Where(x => x.Id == id).FirstAsync();

            return DeleteCertificateTopic(certificateTopic, currentUserId);
        }
        /// <summary>
        /// 根据题库id删除凭证(分录题)
        /// </summary>
        /// <param name="id">题库id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCertificateTopicByTopicId(long id, long currentUserId)
        {
            YssxCertificateTopicPublicNew certificateTopic = await DbContext.FreeSql.Select<YssxCertificateTopicPublicNew>().Where(x => x.TopicId == id).FirstAsync();
            return DeleteCertificateTopic(certificateTopic, currentUserId);
        }
        private static ResponseContext<bool> DeleteCertificateTopic(YssxCertificateTopicPublicNew certificateTopic, long currentUserId)
        {
            if (certificateTopic == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "数据不存在,是否已经删除，请刷新列表" };

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    YssxTopicPublicNew yssxTopic = DbContext.FreeSql.Select<YssxTopicPublicNew>().Where(c => c.Id == certificateTopic.TopicId).First();
                    if (yssxTopic == null)
                        return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到对应题目记录！", false);
                    DbContext.FreeSql.GetRepository<YssxTopicPublicNew>().UpdateDiy.Set(c => new YssxTopicPublicNew
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateBy = currentUserId,
                        UpdateTime = DateTime.Now
                    }).Where(c => c.Id == certificateTopic.TopicId).ExecuteAffrows();

                    if (!string.IsNullOrWhiteSpace(certificateTopic.ImageIds))
                    {
                        string[] ids = certificateTopic.ImageIds.Split(",");
                        foreach (var item in ids)
                        {
                            DbContext.FreeSql.Delete<YssxTopicPublicFileNew>().Where(x => x.Id == long.Parse(item)).ExecuteAffrows();
                        }
                    }
                    DbContext.FreeSql.Delete<YssxCertificateTopicPublicNew>().Where(x => x.Id == certificateTopic.Id).ExecuteAffrows();
                    DbContext.FreeSql.Delete<YssxPubCertificateDataRecordNew>().Where(x => x.CertificateTopicId == certificateTopic.Id).ExecuteAffrows();
                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除数据异常" };
                }

            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }
    }
}
