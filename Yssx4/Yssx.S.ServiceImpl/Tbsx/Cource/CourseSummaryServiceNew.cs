﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 同步实训 课程摘要
    /// </summary>
    public class CourseSummaryServiceNew : ICourseSummaryServiceNew
    {
        public async Task<ResponseContext<SummaryDtoNew>> AddSummary(SummaryDtoNew model)
        {
            if (model == null) return new ResponseContext<SummaryDtoNew> { Code = CommonConstants.ErrorCode };
            YssxCourseSummaryNew data = new YssxCourseSummaryNew();
            if (model.Id == 0)
            {
                data = await DbContext.FreeSql.GetRepository<YssxCourseSummaryNew>().InsertAsync(new YssxCourseSummaryNew { Id = IdWorker.NextId(), CourseId = model.CourseId, Summary = model.Name, CreateBy = model.CreateBy, Type = 1, Sort = model.Sort });
                if (data == null) return new ResponseContext<SummaryDtoNew> { Code = CommonConstants.ErrorCode };
            }
            else
            {
                data.Id = model.Id;
                data.Summary = model.Name;
                data.Sort = model.Sort;
                data.UpdateTime = DateTime.Now;
                int count = await DbContext.FreeSql.GetRepository<YssxCourseSummaryNew>().UpdateDiy.SetSource(data).UpdateColumns(x=> new {x.Summary,x.Sort,x.CreateTime }).Where(x => x.Id == model.Id).ExecuteAffrowsAsync();
            }
            return new ResponseContext<SummaryDtoNew> { Data = new SummaryDtoNew { Id = data.Id, Name = data.Summary, CreateBy = data.CreateBy } };
        }

        public async Task<ResponseContext<List<SummaryDtoNew>>> GetSummaryList(long cid)
        {
            var data = await DbContext.FreeSql.GetRepository<YssxCourseSummaryNew>().Where(x => x.CourseId == cid).ToListAsync();
            return new ResponseContext<List<SummaryDtoNew>> { Data = DataToModel(data) };
        }

        public async Task<ResponseContext<bool>> RemoveSummary(long id)
        {
            bool b = await DbContext.FreeSql.GetRepository<YssxCourseSummaryNew>().DeleteAsync(x => x.Id == id) > 0;
            return new ResponseContext<bool> { Data = b };
        }

        #region 私有方法
        private List<SummaryDtoNew> DataToModel(List<YssxCourseSummaryNew> sources)
        {
            if (sources == null) return null;
            return sources.Select(x => new SummaryDtoNew { Id = x.Id, CreateBy = x.CreateBy, CourseId = x.CourseId, Name = x.Summary, Sort = x.Sort }).OrderBy(x => x.Sort).ToList();
        }
        #endregion
    }
}
