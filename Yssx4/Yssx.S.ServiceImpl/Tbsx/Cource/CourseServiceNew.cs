﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Yssx.S.Pocos.Topics;
using static Yssx.S.ServiceImpl.Topics.CaseService;
using Newtonsoft.Json;
using System.Web;
using Yssx.Repository.Extensions;
using Microsoft.Extensions.DependencyModel.Resolution;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 同步实训课程
    /// </summary>
    public class CourseServiceNew : ICourseServiceNew
    {
        public async Task<ResponseContext<long>> AddOrEditCourse(YssxCourseDtoNew dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "对象是空的!" };

            var courseEntity = new YssxCourseNew
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseName = dto.CourseName,
                CourseTitle = dto.CourseTitle,
                KnowledgePointId = dto.KnowledgePointId,
                CourseTypeId = dto.CourseTypeId,
                Author = dto.Author,
                Education = dto.Education,
                CourseType = dto.CourseType,
                Intro = dto.Intro,
                Description = dto.Description,
                Images = dto.Images,
                PublishingHouse = dto.PublishingHouse,
                PublishingDate = dto.PublishingDate,
                CreateTime = DateTime.Now,
                //TenantId = user.TenantId,
                CreateBy = user.Id,
                CreateByName = user.Name,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                var course = await DbContext.FreeSql.GetRepository<YssxCourseNew>().InsertAsync(courseEntity);
                state = course != null;
                await AddSubjectToCourse(courseEntity.Id);
                await DbContext.FreeSql.GetRepository<YssxCourseCountNew>().InsertAsync(new YssxCourseCountNew { Id = IdWorker.NextId(), CourseId = courseEntity.Id, ReadCount = 0, CreateTime = DateTime.Now, DianZanCount = 0, OrderCount = 0 });
            }
            else
            {
                state = UpdateCourse(courseEntity, dto.UseType);
            }

            return new ResponseContext<long> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = courseEntity.Id, Msg = state ? "成功" : "操作失败!" };
        }
        private bool UpdateCourse(YssxCourseNew entity, int useType)
        {
            if (useType == 0)
            {
                entity.AuditStatus = 0;
                return DbContext.FreeSql.GetRepository<YssxCourseNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.CourseName, x.Description, x.Intro, x.AuditStatus, x.CourseTitle, x.CourseTypeId, x.Author, x.Education, x.Images, x.PublishingHouse, x.PublishingDate, x.UpdateTime }).ExecuteAffrows() > 0;
            }
            else
            {
                return DbContext.FreeSql.GetRepository<YssxCourseNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.CourseName, x.Description, x.Intro, x.CourseTitle, x.CourseTypeId, x.Author, x.Education, x.Images, x.PublishingHouse, x.PublishingDate, x.UpdateTime }).ExecuteAffrows() > 0;
            }

        }
        /// <summary>
        /// 添加课程默认的科目，从课程目模板表中导入
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        private async Task<bool> AddSubjectToCourse(long cid)
        {
            List<YssxCourseSubjectTemp> list = DbContext.FreeSql.GetRepository<YssxCourseSubjectTemp>().Where(x => x.Id > 0).ToList();
            List<YssxCourseSubjectNew> list1 = list.Select(x => new YssxCourseSubjectNew
            {
                Id = IdWorker.NextId(),
                CourseId = cid,
                Code1 = x.Code1,
                Direction = x.Direction,
                SubjectName = x.SubjectName,
                SubjectCode = x.SubjectCode,
                SubjectType = x.SubjectType,
                ParentId = 0,
                CreateTime = DateTime.Now
            }).ToList();
            await DbContext.FreeSql.GetRepository<YssxCourseSubjectNew>().InsertAsync(list1);

            return list.Count > 0;
        }

        public async Task<PageResponse<YssxCourseDtoNew>> GetCourseListByPage(CourseSearchDtoNew dto, UserTicket user)
        {
            var result = new PageResponse<YssxCourseDtoNew>();
            FreeSql.ISelect<YssxCourseNew> select = DbContext.FreeSql.GetRepository<YssxCourseNew>().Select.Where(s => s.IsDelete == CommonConstants.IsNotDelete);
            long tenantid = 981239287939076;
            if (user.Type == (int)UserTypeEnums.Professional)
            {
                //select.Where(s => s.TenantId == user.TenantId);
                select.WhereIf(dto.Education >= 0, s => s.Education == dto.Education);
                select.WhereIf(!string.IsNullOrWhiteSpace(dto.Keyword), s => s.CourseName.Contains(dto.Keyword));
            }
            else
            {

                
                if (!string.IsNullOrWhiteSpace(dto.Keyword))
                {
                    select.Where(x => x.CourseName.Contains(dto.Keyword));
                }
                else if (dto.UseType == 0 && user.Type == (int)UserTypeEnums.Teacher)
                {
                    select.Where(s => s.CreateBy == user.Id);
                }
                else if (dto.UseType == 2 && user.Type == (int)UserTypeEnums.Teacher)
                {
                    select.Where(s => s.AuditStatus == 1 && s.TenantId == tenantid && s.Education == dto.Education);
                }
                else
                {
                    select.Where(s => s.CreateBy == user.Id);
                }
            }
           
            var totalCount = await select.CountAsync();
            var sql = select.OrderByDescending(s => s.Education == 3 ? -1 : s.Education).OrderByDescending(s => s.ReadCount)
              .Page(dto.PageIndex, dto.PageSize).ToSql("Id,CourseName,CourseType,CourseTitle,CourseTypeId,Author,Education,Images,PublishingHouse,PublishingDate,Intro,Description,CreateByName,AuditStatus,Uppershelf,ReadCount,AuditStatusExplain,UppershelfExplain,CreateTime,UpdateTime");
            var items = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseDtoNew>(sql);


            List<long> ids = items.Select(x => x.Id).ToList();
            // List<YssxSectionSummary> yssxes = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetCourseSummary}sectionSummary", () => DbContext.FreeSql.GetRepository<YssxSectionSummary>().Select.Where(x => ids.Contains(x.CourseId)).ToList(), 10 * 60, true, false);

            List<YssxSectionSummaryNew> yssxes = await DbContext.FreeSql.GetRepository<YssxSectionSummaryNew>().Select.Where(x => ids.Contains(x.CourseId)).ToListAsync();

            items.ForEach(x =>
            {
                if (x.AuditStatus == 0 && x.Uppershelf == 1)
                {
                    x.Status = 1;
                }
                else if (x.AuditStatus == 2)
                {
                    x.Status = 3;
                }
                else if (x.AuditStatus == 1)
                {
                    x.Status = 2;
                }
                x.SummaryListDto = yssxes.Where(o => o.CourseId == x.Id).Select(i => new YssxSectionSummaryDtoNew
                {
                    Id = i.Id,
                    Count = i.Count,
                    CourseId = i.CourseId,
                    SectionId = i.SectionId,
                    SummaryType = i.SummaryType
                }).ToList();
            });
            //获取课程绑定案例
            if (items != null && items.Count > 0)
            {
                var cids = items.Select(e => e.Id).ToList();
                var cases = await DbContext.FreeSql.GetRepository<YssxCourseBindCase>(s => s.IsDelete == CommonConstants.IsNotDelete)
                     .Where(e => cids.Contains(e.CourseId))
                     .From<YssxCaseNew>((a, b) => a.LeftJoin(aa => aa.CaseId == b.Id))
                     .ToListAsync((a, b) => new BindCaseDto
                     {
                         CaseId = b.Id,
                         CourseId = a.CourseId,
                         CaseName = b.Name
                     });
                if (cases != null && cases.Count > 0)
                {
                    items.ForEach(e =>
                    {
                        e.Cases = cases.Where(o => o.CourseId == e.Id).ToList();
                    });
                }
            }

            result.Data = items.OrderByDescending(s => s.Education == 3 ? -1 : s.Education).OrderByDescending(s => s.ReadCount).ToList();
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = dto.PageSize;
            result.PageIndex = dto.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }

        public async Task<PageResponse<YssxCourseDtoNew>> GetCourseListPage(int pageIndex, int pageSize, int sortType)
        {
            var result = new PageResponse<YssxCourseDtoNew>();
            var select = DbContext.FreeSql.GetRepository<YssxCourseNew>().Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.AuditStatus == 1);
            var totalCount = await select.CountAsync();
            var sql = select.OrderByDescending(a => a.CreateTime)
              .Page(pageIndex, pageSize).ToSql("a.Id,a.CourseName,a.CourseType,a.CourseTitle,a.CourseTypeId,a.Author,a.Education,a.Images,a.PublishingHouse,a.PublishingDate,a.Intro,a.Description,a.CreateTime,a.UpdateTime");
            var items = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseDtoNew>(sql);

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = pageSize;
            result.PageIndex = pageIndex;
            result.RecordCount = totalCount;
            return result;
        }

        public async Task<ResponseContext<bool>> RemoveCourse(long id)
        {
            YssxCourseNew entity = await DbContext.FreeSql.GetRepository<YssxCourseNew>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxCourseNew>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        #region 购买课程(测试接口)
        /// <summary>
        /// 购买课程(测试接口)
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BuyClassTest(long courseId, UserTicket user)
        {
            bool state = false;

            YssxCourseOrderNew entity = new YssxCourseOrderNew
            {
                Id = IdWorker.NextId(),
                CourseId = courseId,
                CustomerId = user.Id,
                CustomerByName = user.Name,
                //TenantId = user.TenantId,
                PaymentTime = DateTime.Now,
                OrderStatus = (int)OrderStatus.PaymentSuccess,
                OrderSource = (int)CourseDataSource.IndividualBuy,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now,
            };

            var resultList = await DbContext.FreeSql.GetRepository<YssxCourseOrderNew>().InsertAsync(entity);
            state = resultList != null;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 获取购买课程列表
        /// <summary>
        /// 获取购买课程列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<YssxCourseOrderViewModelNew>> GetCourseOrderList(YssxCourseOrderQueryNew query, UserTicket user)
        {
            var result = new PageResponse<YssxCourseOrderViewModelNew>();

            if (query == null)
                return result;

            var select = DbContext.FreeSql.Select<YssxCourseOrderNew>().From<YssxCourseNew>((a, b) => a.InnerJoin(x => x.CourseId == b.Id)).Where((a, b) => a.CustomerId == user.Id);

            if (!string.IsNullOrEmpty(query.OrderNumber))
                select.Where((a, b) => a.OrderNumber == query.OrderNumber);
            if (query.CourseId.HasValue)
                select.Where((a, b) => a.CourseId == query.CourseId.Value);
            if (!string.IsNullOrEmpty(query.Keyword))
                select.Where((a, b) => b.CourseName.Contains(query.Keyword) || b.Author.Contains(query.Keyword));
            if (query.Education.HasValue)
                select.Where((a, b) => b.Education == query.Education.Value);
            if (query.CourseType.HasValue)
                select.Where((a, b) => b.CourseType == query.CourseType.Value);

            if (query.OrderSource.HasValue)
                select.Where((a, b) => a.OrderSource == query.OrderSource.Value);
            else
                select.Where((a, b) => a.OrderStatus == (int)OrderStatus.Presented || a.OrderStatus == (int)OrderStatus.PaymentSuccess);

            var totalCount = await select.CountAsync();

            var sql = select.OrderByDescending((a, b) => a.CreateTime)
              .Page(query.PageIndex, query.PageSize).ToSql("a.Id AS OrderId,a.CourseId,b.CourseName,b.CourseType,b.CourseTitle,b.Education,b.Author,b.PublishingHouse,b.PublishingDate,b.Images," +
              "b.CreateByName,b.CreateTime AS CourseCreateTime,b.UpdateTime AS CourseUpdateTime,b.Intro,b.Description,b.ReadCount,a.PaymentTime,a.OrderSource,a.EffectiveDate");
            var items = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseOrderViewModelNew>(sql);

            List<long> courseIds = items.Select(x => x.CourseId).ToList();
            List<YssxSectionSummaryNew> summaries = await DbContext.FreeSql.GetRepository<YssxSectionSummaryNew>().Select.Where(x => courseIds.Contains(x.CourseId)).ToListAsync();

            items.ForEach(x =>
            {
                x.SummaryListViewModel = summaries.Where(o => o.CourseId == x.CourseId).Select(i => new YssxSectionSummaryDtoNew
                {
                    Id = i.Id,
                    Count = i.Count,
                    CourseId = i.CourseId,
                    SectionId = i.SectionId,
                    SummaryType = i.SummaryType
                }).ToList();
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 获取课程列表(App)
        /// <summary>
        /// 获取课程列表(App)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<YssxCourseForAppViewModelNew>> GetCourseForAppList(YssxCourseForAppQueryNew query, UserTicket user)
        {
            var result = new PageResponse<YssxCourseForAppViewModelNew>();

            if (query == null)
                return result;

            List<YssxCourseForAppViewModelNew> list = new List<YssxCourseForAppViewModelNew>();

            long totalCount = 0;

            //平台(购买)
            if (query.CourseSource == 0)
            {
                var selectBuy = DbContext.FreeSql.Select<YssxCourseOrderNew>().From<YssxCourseNew>((a, b) => a.InnerJoin(x => x.CourseId == b.Id))
                    .Where((a, b) => a.CustomerId == user.Id && (a.OrderStatus == (int)OrderStatus.Presented || a.OrderStatus == (int)OrderStatus.PaymentSuccess));

                if (query.Education.HasValue)
                    selectBuy.Where((a, b) => b.Education == query.Education.Value);

                totalCount = await selectBuy.CountAsync();

                var sqlBuy = selectBuy.OrderByDescending((a, b) => a.UpdateTime)
                  .Page(query.PageIndex, query.PageSize).ToSql("a.Id AS OrderId,a.CourseId,b.CourseName,b.CourseType,b.CourseTitle,b.Education,b.Author,b.PublishingHouse," +
                    "b.PublishingDate,b.Images,b.CreateByName,b.CreateTime AS CourseCreateTime,b.UpdateTime AS CourseUpdateTime,b.Intro,b.Description,b.ReadCount,a.PaymentTime," +
                    "a.OrderSource AS CourseSource,a.EffectiveDate");
                list = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseForAppViewModelNew>(sqlBuy);
            }
            //私有(上传)
            if (query.CourseSource == 1)
            {
                FreeSql.ISelect<YssxCourseNew> selectPrivate = DbContext.FreeSql.Select<YssxCourseNew>().Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.CreateBy == user.Id);

                if (query.Education.HasValue)
                    selectPrivate.Where(x => x.Education == query.Education.Value);

                totalCount = await selectPrivate.CountAsync();

                var sql = selectPrivate.OrderByDescending(x => x.UpdateTime)
                  .Page(query.PageIndex, query.PageSize).ToSql("Id AS CourseId,CourseName,CourseType,CourseTitle,Education,Author,PublishingHouse,PublishingDate,Images," +
                  "CreateByName,CreateTime AS CourseCreateTime,UpdateTime AS CourseUpdateTime,Intro,Description,ReadCount");
                list = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseForAppViewModelNew>(sql);
                list.ForEach(x =>
                {
                    x.CourseSource = 2;
                });
            }
            //所有(购买/私有)
            if (!query.CourseSource.HasValue)
            {
                var selectAll = DbContext.FreeSql.Select<YssxCourseNew>().From<YssxCourseOrderNew>((a, b) => a.LeftJoin(x => x.Id == b.CourseId)).Where((a, b) =>
                    (a.CreateBy == user.Id && a.IsDelete == CommonConstants.IsNotDelete) ||
                    (b.CustomerId == user.Id && (b.OrderStatus == (int)OrderStatus.Presented || b.OrderStatus == (int)OrderStatus.PaymentSuccess)));

                if (query.Education.HasValue)
                    selectAll.Where((a, b) => a.Education == query.Education.Value);

                totalCount = await selectAll.CountAsync();

                var sqlBuy = selectAll.OrderByDescending((a, b) => a.UpdateTime)
                  .Page(query.PageIndex, query.PageSize).ToSql("b.Id AS OrderId,a.Id AS CourseId,a.CourseName,a.CourseType,a.CourseTitle,a.Education,a.Author,a.PublishingHouse," +
                    "a.PublishingDate,a.Images,a.CreateByName,a.CreateTime AS CourseCreateTime,a.UpdateTime AS CourseUpdateTime,a.Intro,a.Description,a.ReadCount,b.PaymentTime," +
                    "b.OrderSource AS CourseSource,b.EffectiveDate");
                list = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseForAppViewModelNew>(sqlBuy);
                list.ForEach(x =>
                {
                    if (!x.CourseSource.HasValue)
                        x.CourseSource = 2;
                });
            }

            List<long> courseIds = list.Select(x => x.CourseId).ToList();
            var summaries = await DbContext.FreeSql.GetRepository<YssxSectionSummaryNew>().Select.Where(x => courseIds.Contains(x.CourseId)).ToListAsync();

            list.ForEach(x =>
            {
                x.SummaryListViewModel = summaries.Where(o => o.CourseId == x.CourseId).Select(i => new YssxSectionSummaryDtoNew
                {
                    Id = i.Id,
                    Count = i.Count,
                    CourseId = i.CourseId,
                    SectionId = i.SectionId,
                    SummaryType = i.SummaryType
                }).ToList();
            });

            result.Data = list;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }


        #endregion


        /// <summary>
        /// 根据源科目列表，获得当前科目下的列表
        /// </summary>
        /// <param name="sourceSubjects"></param>
        /// <returns></returns>
        private List<YssxCourseSubjectNew> GetTargetSubjects(List<YssxCourseSubjectNew> sourceSubjects, long caseId)
        {
            var targetSubjects = new List<YssxCourseSubjectNew>();
            //查找parent为0的记录数
            var Subjects0 = sourceSubjects.Where(s => s.ParentId == 0).ToList();
            SetTargetSubjects(sourceSubjects, Subjects0, ref targetSubjects, 0, caseId);
            return targetSubjects;
        }
        /// <summary>
        /// 递归得到新的科目列表
        /// </summary>
        /// <param name="sourceSubjects">来源科目集合</param>
        /// <param name="childSourceSubjects">子科目集合</param>
        /// <param name="targetSubjects">目标科目集合</param>
        /// <param name="pid">父id</param>
        /// <param name="caseId">所属案例id</param>
        private void SetTargetSubjects(List<YssxCourseSubjectNew> sourceSubjects, List<YssxCourseSubjectNew> childSourceSubjects, ref List<YssxCourseSubjectNew> targetSubjects, long pid, long cid)
        {
            foreach (var item in childSourceSubjects)
            {
                var sourceSid = item.Id;
                var targetSid = IdWorker.NextId();//获取新的id
                item.Id = targetSid;
                item.ParentId = pid;
                item.CourseId = cid;
                item.CreateTime = DateTime.Now;
                targetSubjects.Add(item);
                //查找parentid为该科目ID的子集
                var childSubjects = sourceSubjects.Where(s => s.ParentId == sourceSid).ToList();
                SetTargetSubjects(sourceSubjects, childSubjects, ref targetSubjects, targetSid, cid);
            }
        }

        public async Task<ResponseContext<long>> CloneCourse(YssxCourseDtoNew dto, UserTicket user)
        {
            //1.克隆课程
            long cid = dto.Id;

            //检查是否重复引用平台数据
            if (dto.DataCreateWay == (int)DataCreateWay.Quotation)
            {
                bool b = await DbContext.FreeSql.GetRepository<YssxCourseNew>().Where(x => x.OriginalDataId == dto.Id && x.CreateBy == user.Id && x.IsDelete == CommonConstants.IsNotDelete).AnyAsync();
                if (b)
                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "已引用该门课程,不能重复引用!" };
            }

            YssxCourseNew course = await DbContext.FreeSql.GetRepository<YssxCourseNew>().Where(x => x.Id == cid && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (course == null) return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "课程不存在!" };

            course.Id = IdWorker.NextId();
            course.CourseName = dto.CourseName;
            course.CloneStatus = 1;
            course.IsShow = dto.DataCreateWay == (int)DataCreateWay.Quotation ? 1 : 0;
            //course.TenantId = user.TenantId;
            course.CreateBy = user.Id;
            course.CreateTime = DateTime.Now;
            course.UpdateTime = DateTime.Now;

            course.DataCreateWay = dto.DataCreateWay;
            course.OriginalDataId = dto.Id;
            course.AuditStatus = 0;
            course.Uppershelf = 0;



            await DbContext.FreeSql.GetRepository<YssxCourseNew>().InsertAsync(course);
            await DbContext.FreeSql.GetRepository<YssxCourseTaskJobNew>().InsertAsync(new YssxCourseTaskJobNew
            {
                Id = IdWorker.NextId(),
                CourseId = dto.Id,
                NewCourseId = course.Id,
                CourseName = dto.CourseName,
                TaskType = dto.DataCreateWay,
                UserId = user.Id,
                //TenantId = user.TenantId,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            });

            return new ResponseContext<long> { Data = course.Id };
        }


        /// <summary>
        /// 克隆章节
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        private async Task<List<YssxSectionCloneNew>> CloneSection(long cid, long newId)
        {
            var sections = await DbContext.FreeSql.GetRepository<YssxSectionNew>().Where(x => x.CourseId == cid).ToListAsync();
            var newSections = new List<YssxSectionNew>();
            var cloneSections = new List<YssxSectionCloneNew>();
            var childSections = sections.Where(x => x.ParentId == 0).ToList();
            SetTargetSection(sections, childSections, newSections, cloneSections, 0, newId);
            var results = await DbContext.FreeSql.GetRepository<YssxSectionNew>().InsertAsync(newSections);
            return cloneSections;
        }

        private async Task<bool> CloneSubject(long cid, long newId)
        {
            List<YssxCourseSubjectNew> subjects = await DbContext.FreeSql.GetRepository<YssxCourseSubjectNew>().Where(x => x.CourseId == cid).ToListAsync();
            List<YssxCourseSubjectNew> list = GetTargetSubjects(subjects, newId);
            await DbContext.FreeSql.GetRepository<YssxCourseSubjectNew>().InsertAsync(list);
            return true;
        }

        /// <summary>
        /// 克隆题目
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="newId"></param>
        /// <returns></returns>
        private async Task<bool> CloneTopic(long cid, long newId, List<YssxSectionCloneNew> cloneSections, List<YssxTopicPublicNew> topicPublics)
        {
            List<long> sectionIds = cloneSections.Select(x => x.Id).ToList();
            var topics = await DbContext.FreeSql.GetRepository<YssxSectionTopicNew>().Where(x => x.CourseId == cid && sectionIds.Contains(x.SectionId)
                && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (topics.Count == 0) return true;
            topics.ForEach(x =>
            {
                x.Id = IdWorker.NextId();
                x.SectionId = cloneSections.Where(o => o.Id == x.SectionId).FirstOrDefault().NewId;
                x.QuestionId = topicPublics.Where(o => o.UpdateBy == x.QuestionId).FirstOrDefault().Id;
                x.CourseId = newId;
            });

            var list = await DbContext.FreeSql.GetRepository<YssxSectionTopicNew>().InsertAsync(topics);
            return list.Count > 0;
        }

        /// <summary>
        /// 克隆题库
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="dataCreateWay">数据新增方式 0:自建数据 1:拷贝自有数据(深度拷贝) 2:引用平台数据(通过购买行为、深度拷贝)</param>
        /// <returns></returns>
        private async Task<List<YssxTopicPublicNew>> ClonePublicTopic(long cid, int dataCreateWay)
        {
            var list = await DbContext.FreeSql.Select<YssxTopicPublicNew>().From<YssxSectionTopicNew>(
                  (a, b) => a.InnerJoin(x => x.Id == b.QuestionId))
                  .Where((a, b) => b.CourseId == cid && b.IsDelete == CommonConstants.IsNotDelete)
                  .Distinct()
                  .ToListAsync();

            list.ForEach(x =>
            {
                x.DataCreateWay = dataCreateWay;
                x.OriginalDataId = x.Id;
                x.CourseId = cid;

                x.UpdateBy = x.Id;
                x.UpdateTime = DateTime.Now;
                x.CreateTime = DateTime.Now;

                x.Id = IdWorker.NextId();
            });

            //综合题列表
            var mainList = list.Where(x => x.QuestionType == QuestionType.MainSubQuestion).ToList();
            //综合题原始Id
            List<long> mainIds = mainList.Select(x => x.OriginalDataId).ToList();
            //综合题子题列表
            var synthesisList = await DbContext.FreeSql.GetRepository<YssxTopicPublicNew>().Where(x => mainIds.Contains(x.ParentId)
                && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            synthesisList.ForEach(x =>
            {
                x.DataCreateWay = dataCreateWay;
                x.OriginalDataId = x.Id;

                x.Id = IdWorker.NextId();
                long parentId = mainList.SingleOrDefault(a => a.OriginalDataId == x.ParentId).Id;
                x.ParentId = parentId;
                x.CourseId = cid;

                x.CreateTime = DateTime.Now;
                x.UpdateTime = DateTime.Now;
            });
            list.AddRange(synthesisList);

            //综合题子题中 分录题列表
            List<YssxTopicPublicNew> entryList = synthesisList.Where(x => x.QuestionType == QuestionType.AccountEntry).ToList();
            //综合题子题中 分录题原始Id
            List<long> entryIds = entryList.Select(x => x.OriginalDataId).ToList();
            //分录题公共凭证列表
            var cerList = await DbContext.FreeSql.GetRepository<YssxCertificateTopicPublicNew>().Where(x => entryIds.Contains(x.TopicId)
                && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            cerList.ForEach(x =>
            {
                x.Id = IdWorker.NextId();
                long topicId = entryList.SingleOrDefault(a => a.OriginalDataId == x.TopicId).Id;
                x.TopicId = topicId;
                x.CourseId = cid;
                x.CreateTime = DateTime.Now;
                x.UpdateTime = DateTime.Now;
            });

            //克隆分录题
            var list2 = await DbContext.FreeSql.Select<YssxCertificateTopicPublicNew>().From<YssxSectionTopicNew>(
                (a, b) => a.InnerJoin(x => x.TopicId == b.QuestionId))
                .Where((a, b) => b.CourseId == cid && b.IsDelete == CommonConstants.IsNotDelete)
                .Distinct()
                .ToListAsync();

            list2.ForEach(x =>
            {
                x.UpdateBy = x.TopicId;
                x.Id = IdWorker.NextId();
                x.TopicId = list.Where(o => o.UpdateBy == x.UpdateBy).FirstOrDefault().Id;

                x.UpdateTime = DateTime.Now;
                x.CreateTime = DateTime.Now;
            });

            list2.AddRange(cerList);

            await CloneAnswerOption(list);

            await DbContext.FreeSql.GetRepository<YssxCertificateTopicPublicNew>().InsertAsync(list2);

            await DbContext.FreeSql.GetRepository<YssxTopicPublicNew>().InsertAsync(list);

            await CloneTopicFile(list);
            return list;
        }

        private async Task<bool> CloneAnswerOption(List<YssxTopicPublicNew> list)
        {
            var simples = list.Where(x => x.QuestionType == QuestionType.SingleChoice || x.QuestionType == QuestionType.MultiChoice || x.QuestionType == QuestionType.Judge).ToList();
            var answerOptions = new List<YssxPublicAnswerOptionNew>();
            simples.ForEach(x =>
            {
                var ao = DbContext.FreeSql.GetRepository<YssxPublicAnswerOptionNew>().Where(a => a.TopicId == x.UpdateBy && a.IsDelete == CommonConstants.IsNotDelete).ToList();
                if (ao != null)
                {
                    ao.ForEach(o =>
                    {
                        o.Id = IdWorker.NextId();
                        o.TopicId = x.Id;
                        answerOptions.Add(o);
                    });
                }
            });
            if (answerOptions.Count > 0)
            {
                var ass = await DbContext.FreeSql.GetRepository<YssxPublicAnswerOptionNew>().InsertAsync(answerOptions);
            }
            return true;
        }

        private async Task<bool> CloneTopicFile(List<YssxTopicPublicNew> list)
        {
            var topics = list.Where(x => x.QuestionType != QuestionType.SingleChoice && x.QuestionType != QuestionType.MultiChoice).ToList();
            var yssxTopicFiles = new List<YssxTopicPublicFileNew>();
            topics.ForEach(x =>
            {
                var files = DbContext.FreeSql.GetRepository<YssxTopicPublicFileNew>().Select
              .Where(t => x.TopicFileIds.Contains(t.Id.ToString()) && t.IsDelete == CommonConstants.IsNotDelete).ToList();
                if (files != null && files.Count > 0)
                {
                    yssxTopicFiles.AddRange(files);
                }
            });

            if (yssxTopicFiles.Count > 0)
            {
                yssxTopicFiles.ForEach(x =>
                {
                    x.Id = IdWorker.NextId();
                    x.CreateTime = DateTime.Now;
                });
                await DbContext.FreeSql.GetRepository<YssxTopicPublicFileNew>().InsertAsync(yssxTopicFiles);
            }
            return true;
        }

        /// <summary>
        /// 克隆课程文件
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="newId"></param>
        /// <returns></returns>

        private async Task<bool> CloneFiles(long cid, long newId, List<YssxSectionCloneNew> cloneSections)
        {
            List<long> sectionIds = cloneSections.Select(x => x.Id).ToList();
            var files = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().Where(x => x.CourseId == cid
                && (sectionIds.Contains(x.SectionId) || x.SectionId == 0) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (files.Count == 0) return true;
            files.ForEach(x =>
            {
                x.Id = IdWorker.NextId();
                if (x.SectionId > 0)
                {
                    x.SectionId = cloneSections.Where(o => o.Id == x.SectionId).FirstOrDefault().NewId;
                }
                x.CourseId = newId;
            });

            var list = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().InsertAsync(files);
            return list.Count > 0;
        }

        /// <summary>
        /// 克隆课程案例
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="newId"></param>
        /// <param name="cloneSections"></param>
        /// <returns></returns>
        private async Task<bool> CloneCase(long cid, long newId, List<YssxSectionCloneNew> cloneSections)
        {
            var cases = await DbContext.FreeSql.GetRepository<YssxSectionCaseNew>().Where(x => x.CourseId == cid && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (cases.Count == 0) return true;
            cases.ForEach(x =>
            {
                x.Id = IdWorker.NextId();
                x.SectionId = cloneSections.Where(o => o.Id == x.SectionId).FirstOrDefault().NewId;
                x.CourseId = newId;
            });

            var list = await DbContext.FreeSql.GetRepository<YssxSectionCaseNew>().InsertAsync(cases);
            return list.Count > 0;
        }

        /// <summary>
        /// 克隆教材
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="newId"></param>
        /// <param name="cloneSections"></param>
        /// <returns></returns>
        private async Task<bool> CloneTextBook(long cid, long newId, List<YssxSectionCloneNew> cloneSections)
        {
            var textBooks = await DbContext.FreeSql.GetRepository<YssxSectionTextBookNew>().Where(x => x.CourseId == cid && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (textBooks.Count == 0) return true;
            textBooks.ForEach(x =>
            {
                x.Id = IdWorker.NextId();
                if (x.SectionId > 0)
                {
                    x.SectionId = cloneSections.Where(o => o.Id == x.SectionId).FirstOrDefault().NewId;
                }
                x.CourseId = newId;
            });

            var list = await DbContext.FreeSql.GetRepository<YssxSectionTextBookNew>().InsertAsync(textBooks);
            return list.Count > 0;

        }

        /// <summary>
        /// 克隆场景实训
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="newId"></param>
        /// <param name="cloneSections"></param>
        /// <returns></returns>
        private async Task<bool> CloneSceneTraining(long cid, long newId, List<YssxSectionCloneNew> cloneSections)
        {
            var sceneTrainings = await DbContext.FreeSql.GetRepository<YssxSectionSceneTrainingNew>().Where(x => x.CourseId == cid && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (sceneTrainings.Count == 0) return true;
            sceneTrainings.ForEach(x =>
            {
                x.Id = IdWorker.NextId();
                if (x.SectionId > 0)
                {
                    x.SectionId = cloneSections.Where(o => o.Id == x.SectionId).FirstOrDefault().NewId;
                }
                x.CourseId = newId;
            });

            var list = await DbContext.FreeSql.GetRepository<YssxSectionSceneTrainingNew>().InsertAsync(sceneTrainings);
            return list.Count > 0;
        }

        /// <summary>
        /// 克隆课程汇总
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="newId"></param>
        /// <param name="cloneSections"></param>
        /// <returns></returns>
        private async Task<bool> CloneSectionSummery(long cid, long newId, List<YssxSectionCloneNew> cloneSections)
        {
            var summaries = await DbContext.FreeSql.GetRepository<YssxSectionSummaryNew>().Where(x => x.CourseId == cid).ToListAsync();
            if (summaries.Count == 0) return true;
            summaries.ForEach(x =>
            {
                x.Id = IdWorker.NextId();
                if (x.SectionId > 0)
                    x.SectionId = cloneSections.Where(o => o.Id == x.SectionId).FirstOrDefault().NewId;
                x.CourseId = newId;
            });

            var list = await DbContext.FreeSql.GetRepository<YssxSectionSummaryNew>().InsertAsync(summaries);
            return list.Count > 0;
        }

        /// <summary>
        /// 克隆课程摘要
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="newId"></param>
        /// <returns></returns>
        private async Task<bool> CloneCourseSummery(long cid, long newId)
        {
            var summaries = await DbContext.FreeSql.GetRepository<YssxCourseSummaryNew>().Where(x => x.CourseId == cid).ToListAsync();
            if (summaries.Count == 0) return true;
            summaries.ForEach(x =>
            {
                x.Id = IdWorker.NextId();
                x.CourseId = newId;
            });

            var list = await DbContext.FreeSql.GetRepository<YssxCourseSummaryNew>().InsertAsync(summaries);
            return list.Count > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceSections"></param>
        /// <param name="childSections"></param>
        /// <param name="newSections"></param>
        /// <param name="cloneSections"></param>
        /// <param name="pid"></param>
        /// <param name="cid"></param>

        private void SetTargetSection(List<YssxSectionNew> sourceSections, List<YssxSectionNew> childSections, List<YssxSectionNew> newSections, List<YssxSectionCloneNew> cloneSections, long pid, long cid)
        {
            foreach (var item in childSections)
            {
                var sourceSid = item.Id;
                var targetSid = IdWorker.NextId();//获取新的id
                item.Id = targetSid;
                item.ParentId = pid;
                item.CourseId = cid;
                item.CreateTime = DateTime.Now;
                newSections.Add(item);
                cloneSections.Add(new YssxSectionCloneNew { Id = sourceSid, NewId = targetSid, ParentId = pid, CourseId = cid });
                //查找parentid为该科目ID的子集
                var childSubjects = sourceSections.Where(s => s.ParentId == sourceSid).ToList();
                SetTargetSection(sourceSections, childSubjects, newSections, cloneSections, targetSid, cid);
            }
        }

        /// <summary>
        /// 一键更新案例下分录题的AnswerValue
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        private async Task<bool> ReplaceAnswerValue(long caseId, List<YssxTopicPublicNew> topicPublics)
        {
            //所有分录题列表（包括子题目）
            var yssxTopics = topicPublics.Where(x => x.QuestionType == QuestionType.AccountEntry).ToList();

            if (yssxTopics.Count == 0)
                return true;

            var result = true;
            var topics = new List<YssxTopicPublicNew>();
            var certificateTopics = new List<YssxCertificateTopicPublicNew>();
            try
            {
                foreach (YssxTopicPublicNew item in yssxTopics)
                {
                    //解码
                    var newAnswerValue = HttpUtility.UrlDecode(item.AnswerValue);
                    //反序列化answervalue
                    var obj = JsonConvert.DeserializeObject<AnswerObj>(newAnswerValue);
                    //var DynamicObject = JsonConvert.DeserializeObject<dynamic>(json);
                    //JToken jToken = JToken.Parse(json);
                    // 更改摘要
                    UpdateSummary(ref obj, caseId);
                    // 更改科目
                    UpdateSubject(ref obj, caseId);
                    // 更改制单人
                    var yssxCertificate = DbContext.FreeSql.Select<YssxCertificateTopicPublicNew>().Where(c => c.TopicId == item.Id).First();
                    //if (yssxCertificate != null)
                    //{
                    //    if (yssxCertificate.IsDisableCreator == Status.Enable)
                    //    {
                    //        UpdateCreator(ref obj, item, yssxCase, ref yssxCertificate);
                    //    }
                    //}
                    var targetAnswerValue = JsonConvert.SerializeObject(obj);
                    //url编码
                    item.AnswerValue = HttpUtility.UrlEncode(targetAnswerValue);
                    // item.UpdateBy = CurrentbkUserTicket.Id;
                    item.UpdateTime = DateTime.Now;
                    topics.Add(item);
                    certificateTopics.Add(yssxCertificate);
                }

                foreach (var item in topics)
                {
                    DbContext.FreeSql.GetRepository<YssxTopicPublicNew>().UpdateDiy.SetSource(item)
                        .UpdateColumns(t => new { t.AnswerValue, t.UpdateBy, t.UpdateTime }).ExecuteAffrows();
                }
                foreach (var yssxCertificate in certificateTopics)
                {
                    DbContext.FreeSql.GetRepository<YssxCertificateTopicPublicNew>().UpdateDiy.SetSource(yssxCertificate)
                        .UpdateColumns(t => new { t.Creator, t.UpdateBy, t.UpdateTime }).ExecuteAffrows();
                }
            }
            catch (Exception ex)
            {
                //   CommonLogger.Error($"{caseId}案例更新AnswerValue过程中出错" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, ex);
                result = false;

            }
            return result;
        }
        private void UpdateSummary(ref AnswerObj obj, long caseId)
        {
            var summaryInfo = obj?.ItemCtrlRow;
            if (summaryInfo != null && summaryInfo.Items != null && summaryInfo.Items.Count > 0)
            {
                var summaryIdValue = summaryInfo.Items[0].Value;

                if (!string.IsNullOrEmpty(summaryIdValue))
                {
                    //源摘要id
                    var sourceSummaryId = long.Parse(summaryIdValue);
                    var sourceSummary = DbContext.FreeSql.Select<YssxCourseSummaryNew>()
                        .Where(s => s.Id == sourceSummaryId).First(s => s.Summary);
                    //目标摘要id
                    var targetSummaryId = DbContext.FreeSql.Select<YssxCourseSummaryNew>()
                        .Where(s => s.Summary == sourceSummary && s.CourseId == caseId).First(s => s.Id);
                    summaryInfo.Items[0].Value = targetSummaryId.ToString();
                }

            }
        }
        private void UpdateSubject(ref AnswerObj obj, long caseId)
        {
            var bodyInfos = obj?.BodyCtrlRows;
            if (bodyInfos != null && bodyInfos.Count > 0)
            {
                foreach (BodyCtrlRows bRow in bodyInfos)
                {
                    if (bRow.Items != null && bRow.Items.Count > 0)
                    {
                        var subjectIdValue = bRow.Items[0].Value;
                        if (!string.IsNullOrEmpty(subjectIdValue))
                        {
                            //源科目id
                            var sourceSubjectId = long.Parse(subjectIdValue);
                            var sourceSubjectCode = DbContext.FreeSql.Select<YssxCourseSubjectNew>()
                                .Where(s => s.Id == sourceSubjectId).First(s => s.SubjectCode);

                            //目标科目id
                            var targetSubjectId = DbContext.FreeSql.Select<YssxCourseSubjectNew>()
                                .Where(s => s.SubjectCode == sourceSubjectCode && s.CourseId == caseId).First(s => s.Id);

                            bRow.Items[0].Value = targetSubjectId.ToString();
                        }
                    }
                }
            }
        }


        /// <summary>
        /// 复制题目
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="s"></param>
        /// <param name="sourceTopicId"></param>
        /// <param name="subjects"></param>
        private void CloneQuestion(IFreeSql freeSql, YssxTopicPublicNew s, long sourceTopicId, List<YssxCourseSubjectNew> subjects)
        {
            switch (s.QuestionType)
            {
                case var q when (q == QuestionType.SingleChoice || q == QuestionType.MultiChoice || q == QuestionType.Judge):
                    CloneSimpleQuestion(freeSql, s, sourceTopicId);
                    break;
                case var q when (q == QuestionType.FillGrid || q == QuestionType.FillBlank || q == QuestionType.FillGraphGrid || q == QuestionType.SettleAccounts || q == QuestionType.GridFillBank || q == QuestionType.FinancialStatements):
                    CloneComplexQuestion(freeSql, s);
                    break;
                case QuestionType.AccountEntry:
                    CloneAccountEntry(freeSql, s, sourceTopicId, subjects, 0);
                    break;
                case QuestionType.MainSubQuestion:
                    CloneMainSubQuestion(freeSql, s, sourceTopicId, subjects);
                    break;
                default:
                    throw new Exception($"{s.QuestionType}类型的题目不存在");
            }

        }

        /// <summary>
        /// 复制简单题
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="s"></param>
        /// <param name="sourceTopicId"></param>
        private void CloneSimpleQuestion(IFreeSql freeSql, YssxTopicPublicNew s, long sourceTopicId)
        {
            //增加题目
            freeSql.GetRepository<YssxTopicPublicNew>().Insert(s);
            //增加yssx_answer_option  根据sourcetiopicid查找选项内容
            var sourceAnswerOptions = freeSql.Select<YssxPublicAnswerOptionNew>()
                 .Where(a => a.TopicId == sourceTopicId && a.IsDelete == CommonConstants.IsNotDelete).ToList();

            sourceAnswerOptions.ForEach(a =>
            {
                a.Id = IdWorker.NextId();
                a.TopicId = s.Id;
                //a.CreateBy = CurrentbkUserTicket.Id;
                a.CreateTime = DateTime.Now;
            });
            freeSql.GetRepository<YssxPublicAnswerOptionNew>().Insert(sourceAnswerOptions);
        }
        /// <summary>
        /// 复制复杂类型的题目
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="s"></param>
        private void CloneComplexQuestion(IFreeSql freeSql, YssxTopicPublicNew s)
        {
            //增加YssxTopicFile
            var yssxTopicFiles = freeSql.Select<YssxTopicPublicFileNew>()
                .Where(t => s.TopicFileIds.Contains(t.Id.ToString()) && t.IsDelete == CommonConstants.IsNotDelete).ToList();

            yssxTopicFiles.ForEach(t =>
            {
                t.Id = IdWorker.NextId();
                //t.CreateBy = CurrentbkUserTicket.Id;
                t.CreateTime = DateTime.Now;
            });
            freeSql.GetRepository<YssxTopicPublicFileNew>().Insert(yssxTopicFiles);
            s.TopicFileIds = string.Join(",", yssxTopicFiles.Select(f => f.Id).ToArray());
            //增加题目
            freeSql.GetRepository<YssxTopicPublicNew>().Insert(s);
        }
        /// <summary>
        /// 复制分录题
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="s"></param>
        /// <param name="sourceTopicId"></param>
        /// <param name="subjects"></param>
        private void CloneAccountEntry(IFreeSql freeSql, YssxTopicPublicNew s, long sourceTopicId, List<YssxCourseSubjectNew> subjects, long cid)
        {
            //YssxCertificateTopic certificateTopic = freeSql.Select<YssxCertificateTopic>()
            //    .Where(c => c.TopicId == sourceTopicId && c.IsDelete == CommonConstants.IsNotDelete).First();
            var certificateTopic = freeSql.GetRepository<YssxCertificateTopicPublicNew>().Select
                .Where(c => c.TopicId == sourceTopicId && c.IsDelete == CommonConstants.IsNotDelete).First();

            var sourceCertificateTopicId = certificateTopic.Id;
            certificateTopic.Id = IdWorker.NextId();
            certificateTopic.TopicId = s.Id;

            var yssxCertificateDataRecords = freeSql.GetRepository<YssxCertificateDataRecordNew>().Select
                .Where(d => d.CertificateTopicId == sourceCertificateTopicId && d.IsDelete == CommonConstants.IsNotDelete).ToList();

            var subjectIds = yssxCertificateDataRecords.Select(t => t.SubjectId).ToList();
            var sourceSubjects = freeSql.Select<YssxCourseSubjectNew>().Where(ys => subjectIds.Contains(ys.Id)).ToList();

            yssxCertificateDataRecords.ForEach(t =>
            {
                if (t.SubjectId == 0)
                    throw new Exception("凭证数据记录中科目id不能为0，数据异常");
                var sourceSubject = sourceSubjects.FirstOrDefault(ys => ys.Id == t.SubjectId);
                if (sourceSubject == null)
                    throw new Exception($"题目{s.Title}未找到来源科目{t.SubjectId}");
                //根据目标案例id和科目编码查找最新科目并关联 
                //YssxSubject yssxSubject = freeSql.Select<YssxSubject>()
                //.Where(ys => ys.EnterpriseId == s.CaseId && ys.SubjectCode == sourceSubject.SubjectCode).First();
                YssxCourseSubjectNew yssxSubject = subjects.Where(ys => ys.CourseId == cid && ys.SubjectCode == sourceSubject.SubjectCode).FirstOrDefault();
                //t.SubjectId 要取对应的subjectId
                t.SubjectId = yssxSubject != null ? yssxSubject.Id : 0;
                //t.EnterpriseId = cid;
                t.Id = IdWorker.NextId();
                //t.CreateBy = CurrentbkUserTicket.Id;
                t.CertificateTopicId = certificateTopic.Id;
                t.CreateTime = DateTime.Now;
                // uow.GetRepository<YssxCertificateDataRecord>().Insert(t);
            });
            freeSql.GetRepository<YssxCertificateDataRecordNew>().Insert(yssxCertificateDataRecords);
            //新增YssxTopicFile  考虑是否提取出来
            //List<YssxTopicFile> yssxTopicFiles = freeSql.Select<YssxTopicFile>()
            //    .Where(t => s.TopicFileIds.Contains(t.Id.ToString()) && t.IsDelete == CommonConstants.IsNotDelete).ToList();
            var yssxTopicFiles = freeSql.GetRepository<YssxTopicPublicFileNew>().Select
                .Where(t => s.TopicFileIds.Contains(t.Id.ToString()) && t.IsDelete == CommonConstants.IsNotDelete).ToList();

            yssxTopicFiles.ForEach(t =>
            {
                t.Id = IdWorker.NextId();
                //t.CreateBy = CurrentbkUserTicket.Id;
                t.CreateTime = DateTime.Now;
                //uow.GetRepository<YssxTopicFile>().Insert(t);
            });
            freeSql.GetRepository<YssxTopicPublicFileNew>().Insert(yssxTopicFiles);
            s.TopicFileIds = string.Join(",", yssxTopicFiles.Select(f => f.Id).ToArray());
            //增加题目
            freeSql.GetRepository<YssxTopicPublicNew>().Insert(s);
            certificateTopic.ImageIds = string.Join(",", yssxTopicFiles.Select(f => f.Id).ToArray());
            freeSql.GetRepository<YssxCertificateTopicPublicNew>().Insert(certificateTopic);
        }


        /// <summary>
        /// 复制综合题
        /// </summary>
        /// <param name="freeSql"></param>
        /// <param name="s"></param>
        /// <param name="sourceTopicId"></param>
        /// <param name="subjects"></param>
        private void CloneMainSubQuestion(IFreeSql freeSql, YssxTopicPublicNew s, long sourceTopicId, List<YssxCourseSubjectNew> subjects)
        {
            //增加YssxTopicFile
            var yssxTopicFiles = freeSql.Select<YssxTopicPublicFileNew>()
                .Where(t => s.TopicFileIds.Contains(t.Id.ToString()) && t.IsDelete == CommonConstants.IsNotDelete).ToList();

            yssxTopicFiles.ForEach(t =>
            {
                t.Id = IdWorker.NextId();
                //t.CreateBy = CurrentbkUserTicket.Id;
                t.CreateTime = DateTime.Now;
            });
            freeSql.GetRepository<YssxTopicPublicFileNew>().Insert(yssxTopicFiles);
            s.TopicFileIds = string.Join(",", yssxTopicFiles.Select(f => f.Id).ToArray());
            //增加题目
            freeSql.GetRepository<YssxTopicPublicNew>().Insert(s);
            //查找该综合题下的子题目
            var yssxTopics = freeSql.Select<YssxTopicPublicNew>()
                .Where(t => t.ParentId == sourceTopicId && t.IsDelete == CommonConstants.IsNotDelete).ToList();

            yssxTopics.ForEach(t =>
            {
                var subSourceTopicId = t.Id;//记录源id
                t.Id = IdWorker.NextId();//重新给id赋值,标记为新对象
                t.ParentId = s.Id; //新的题目id
                //t.CaseId = s.CaseId; //新的caseid
                //t.CreateBy = CurrentbkUserTicket.Id;
                t.CreateTime = DateTime.Now;
                t.IsGzip = CommonConstants.IsGzip;
                CloneQuestion(freeSql, t, subSourceTopicId, subjects);
            });
        }


        #region 克隆课程备课列表
        /// <summary>
        /// 克隆课程备课列表
        /// </summary>
        /// <param name="oldCourseId">原始课程Id</param>
        /// <param name="newCourseId">新生成课程Id</param>
        /// <param name="dataCreateWay">数据新增方式 0:自建数据 1:拷贝自有数据(深度拷贝) 2:引用平台数据(通过购买行为、深度拷贝)</param>
        /// <param name="keyValuePairs">记录原始备课Id、新备课Id</param>
        /// <param name="user"></param>
        private async Task<bool> CloneLessonPlanning(long oldCourseId, long newCourseId, int dataCreateWay, Dictionary<long, long> keyValuePairs, UserTicket user)
        {
            var planList = await DbContext.FreeSql.Select<YssxLessonPlanningNew>().Where(x => x.CourseId == oldCourseId && x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new YssxLessonPlanningNew
                {
                    CourseId = newCourseId,

                    //TenantId = user.TenantId,
                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,

                    DataCreateWay = dataCreateWay,
                    OriginalDataId = x.Id,
                });

            planList.ForEach(x =>
            {
                x.Id = IdWorker.NextId();
                keyValuePairs.Add(x.OriginalDataId, x.Id);
            });

            var modelList = await DbContext.FreeSql.GetRepository<YssxLessonPlanningNew>().InsertAsync(planList);

            return modelList.Count > 0;
        }
        #endregion

        #region 克隆课程备课题目列表
        /// <summary>
        /// 克隆课程备课题目列表
        /// </summary>
        /// <param name="oldCourseId">原始课程Id</param>
        /// <param name="newCourseId">新生成课程Id</param>
        /// <param name="keyValuePairs">记录原始备课Id、新备课Id</param>
        /// <param name="topicPublics">题库</param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<bool> CloneLessonPlanningTopic(long oldCourseId, long newCourseId, Dictionary<long, long> keyValuePairs, List<YssxTopicPublicNew> topicPublics, UserTicket user)
        {
            //课程备课Ids
            List<long> sectionIds = keyValuePairs.Keys.ToList();
            var topics = await DbContext.FreeSql.GetRepository<YssxSectionTopicNew>().Where(x => x.CourseId == oldCourseId && sectionIds.Contains(x.SectionId)
                && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (topics.Count == 0) return true;

            topics.ForEach(x =>
            {
                x.Id = IdWorker.NextId();
                x.CourseId = newCourseId;
                x.SectionId = keyValuePairs[x.SectionId];
                x.QuestionId = topicPublics.Where(o => o.UpdateBy == x.QuestionId).FirstOrDefault()?.Id ?? 0;

                x.CreateBy = user.Id;
                x.CreateTime = DateTime.Now;
                x.UpdateBy = user.Id;
                x.UpdateTime = DateTime.Now;
            });

            var list = await DbContext.FreeSql.GetRepository<YssxSectionTopicNew>().InsertAsync(topics);
            return list.Count > 0;
        }
        #endregion

        #region 克隆课程备课文件列表
        /// <summary>
        /// 克隆课程备课文件列表
        /// </summary>
        /// <param name="oldCourseId">原始课程Id</param>
        /// <param name="newCourseId">新生成课程Id</param>
        /// <param name="keyValuePairs">记录原始备课Id、新备课Id</param>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<bool> CloneLessonPlanningFiles(long oldCourseId, long newCourseId, Dictionary<long, long> keyValuePairs, UserTicket user)
        {
            //课程备课Ids
            List<long> sectionIds = keyValuePairs.Keys.ToList();
            var files = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().Where(x => x.CourseId == oldCourseId && sectionIds.Contains(x.SectionId)
                 && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            if (files.Count == 0) return true;

            files.ForEach(x =>
            {
                x.Id = IdWorker.NextId();
                if (x.SectionId > 0)
                {
                    x.SectionId = keyValuePairs[x.SectionId];
                }
                x.CourseId = newCourseId;

                x.CreateBy = user.Id;
                x.CreateTime = DateTime.Now;
                x.UpdateBy = user.Id;
                x.UpdateTime = DateTime.Now;
            });

            var list = await DbContext.FreeSql.GetRepository<YssxSectionFilesNew>().InsertAsync(files);
            return list.Count > 0;
        }
        #endregion

        #region 引用平台习题(深度拷贝购买课程下的某一道习题)
        /// <summary>
        /// 引用平台习题(深度拷贝购买课程下的某一道习题)
        /// </summary>
        /// <param name="courseId">习题即将存放的课程Id</param>
        /// <param name="topicId">需要引用的习题Id</param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> QuotationPlatformsTopic(long courseId, long topicId, UserTicket user)
        {
            var topicModel = await DbContext.FreeSql.GetRepository<YssxTopicPublicNew>().Where(x => x.Id == topicId).FirstAsync();
            if (topicModel == null)
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "没有该数据源!" };

            //不支持引用分录题
            if (topicModel.QuestionType == QuestionType.AccountEntry)
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "暂不支持引用分录题!" };

            var topicList = new List<YssxTopicPublicNew>();

            var synthesisList = new List<YssxTopicPublicNew>();
            //检查综合题下是否有分录题
            if (topicModel.QuestionType == QuestionType.MainSubQuestion)
            {
                synthesisList = await DbContext.FreeSql.GetRepository<YssxTopicPublicNew>().Where(x => x.ParentId == topicId).ToListAsync();

                if (synthesisList.Where(x => x.QuestionType == QuestionType.AccountEntry).Count() > 0)
                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "暂不支持引用分录题!" };
            }

            topicModel.Id = IdWorker.NextId();
            topicModel.DataCreateWay = (int)DataCreateWay.Quotation;
            topicModel.OriginalDataId = topicId;
            topicModel.CourseId = courseId;

            topicModel.CreateBy = user.Id;
            topicModel.CreateTime = DateTime.Now;
            topicModel.UpdateBy = user.Id;
            topicModel.UpdateTime = DateTime.Now;

            topicList.Add(topicModel);

            var answerOptions = new List<YssxPublicAnswerOptionNew>();

            if (topicModel.QuestionType == QuestionType.SingleChoice || topicModel.QuestionType == QuestionType.MultiChoice || topicModel.QuestionType == QuestionType.Judge)
            {
                var options = DbContext.FreeSql.GetRepository<YssxPublicAnswerOptionNew>().Where(a => a.TopicId == topicId && a.IsDelete == CommonConstants.IsNotDelete).ToList();
                if (options != null)
                {
                    options.ForEach(o =>
                    {
                        o.Id = IdWorker.NextId();
                        o.TopicId = topicModel.Id;

                        topicModel.CreateBy = user.Id;
                        topicModel.CreateTime = DateTime.Now;
                        topicModel.UpdateBy = user.Id;
                        topicModel.UpdateTime = DateTime.Now;

                        answerOptions.Add(o);
                    });
                }
            }

            if (topicModel.QuestionType == QuestionType.MainSubQuestion)
            {
                //初始化综合题
                var mainQuestion = InitializeMainSubQuestion(synthesisList, courseId, user);
                topicList.AddRange(mainQuestion.Item1);
                answerOptions.AddRange(mainQuestion.Item2);
            }

            bool state = false;
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //习题
                    if (topicList.Count > 0)
                    {
                        var addList = await uow.GetRepository<YssxTopicPublicNew>().InsertAsync(topicList);

                        state = addList.Count > 0;
                    }
                    //选项
                    if (answerOptions.Count > 0)
                    {
                        await uow.GetRepository<YssxPublicAnswerOptionNew>().InsertAsync(answerOptions);
                    }

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            return new ResponseContext<long> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = topicModel.Id, Msg = state ? "成功!" : "操作失败!" };
        }

        /// <summary>
        /// 引用平台习题时初始化综合题
        /// </summary>
        /// <param name="list"></param>
        /// <param name="courseId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private (List<YssxTopicPublicNew>, List<YssxPublicAnswerOptionNew>) InitializeMainSubQuestion(List<YssxTopicPublicNew> list, long courseId, UserTicket user)
        {
            var topicList = new List<YssxTopicPublicNew>();
            List<YssxPublicAnswerOptionNew> answerOptions = new List<YssxPublicAnswerOptionNew>();

            if (list == null || list.Count == 0)
                return (topicList, answerOptions);

            list.ForEach(x =>
            {
                var topicId = x.Id;

                x.Id = IdWorker.NextId();
                x.DataCreateWay = (int)DataCreateWay.Quotation;
                x.OriginalDataId = topicId;
                x.CourseId = courseId;

                x.CreateBy = user.Id;
                x.CreateTime = DateTime.Now;
                x.UpdateBy = user.Id;
                x.UpdateTime = DateTime.Now;

                topicList.Add(x);

                if (x.QuestionType == QuestionType.SingleChoice || x.QuestionType == QuestionType.MultiChoice || x.QuestionType == QuestionType.Judge)
                {
                    var options = DbContext.FreeSql.GetRepository<YssxPublicAnswerOptionNew>().Where(a => a.TopicId == topicId && a.IsDelete == CommonConstants.IsNotDelete).ToList();
                    if (options != null)
                    {
                        options.ForEach(o =>
                        {
                            o.Id = IdWorker.NextId();
                            o.TopicId = x.Id;

                            x.CreateBy = user.Id;
                            x.CreateTime = DateTime.Now;
                            x.UpdateBy = user.Id;
                            x.UpdateTime = DateTime.Now;

                            answerOptions.Add(o);
                        });
                    }
                }
            });

            return (topicList, answerOptions);
        }
        #endregion

        public async Task<PageResponse<YssxCourseListDtoNew>> GetCourseList(CourseSearchDtoNew dto, UserTicket user)
        {
            long tenantid = 981239287939076;
            PageResponse<YssxCourseListDtoNew> result = new PageResponse<YssxCourseListDtoNew>();

            FreeSql.ISelect<YssxCourseNew> sql = DbContext.FreeSql.GetRepository<YssxCourseNew>().Where(s => s.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(dto.Education == -1 && dto.UseType == 0, s => s.CreateBy == user.Id)
                .WhereIf(dto.UseType == 2 && dto.Education == -1, s => s.TenantId == tenantid && s.AuditStatus == 1)
                .WhereIf(dto.Education > -1 && dto.UseType == 2, s => s.TenantId == tenantid && s.Education == dto.Education && s.AuditStatus == 1)
                .WhereIf(dto.Education > -1 && dto.UseType == 0, s => s.CreateBy == user.Id && s.Education == dto.Education)
                .WhereIf(!string.IsNullOrEmpty(dto.Keyword) && dto.UseType == 0, s => (s.CourseName.Contains(dto.Keyword) || s.Author.Contains(dto.Keyword)) && s.CreateBy == user.Id)
                .WhereIf(!string.IsNullOrEmpty(dto.Keyword) && dto.UseType == 2, s => (s.CourseName.Contains(dto.Keyword) || s.Author.Contains(dto.Keyword)));

            var totalCount = sql.CountAsync().ConfigureAwait(false);//记录总条数

            List<YssxCourseNew> list = await sql
                .OrderByDescending(s => s.CreateTime)
                .Page(dto.PageIndex, dto.PageSize)
                .ToListAsync();

            List<YssxCourseListDtoNew> retList = list.Select(x => new YssxCourseListDtoNew
            {
                Author = x.Author,
                CourseName = x.CourseName,
                Education = x.Education,
                Id = x.Id,
                PublishingDate = x.PublishingDate,
                PublishingHouse = x.PublishingHouse,
                Images = x.Images,
                IsShow = x.IsShow,
                CloneStatus = x.CloneStatus,
            }).ToList();
            result.PageIndex = dto.PageIndex;
            result.PageSize = dto.PageSize;
            result.Data = retList;
            result.Code = CommonConstants.SuccessCode;
            result.RecordCount = await totalCount;
            return result;
        }

        public async Task<PageResponse<YssxCourseListDtoNew>> GetShowList(PageRequest page, UserTicket user)
        {
            PageResponse<YssxCourseListDtoNew> result = new PageResponse<YssxCourseListDtoNew>();
            FreeSql.ISelect<YssxCourseNew> sql = DbContext.FreeSql.GetRepository<YssxCourseNew>().Where(s => s.IsDelete == CommonConstants.IsNotDelete && s.IsShow == 1 && s.CreateBy == user.Id);
            var totalCount = sql.CountAsync().ConfigureAwait(false);
            List<YssxCourseListDtoNew> list = await sql.Page(page.PageIndex, page.PageSize).ToListAsync(x => new YssxCourseListDtoNew
            {
                Author = x.Author,
                CourseName = x.CourseName,
                Education = x.Education,
                Id = x.Id,
                Images = x.Images,
                IsShow = x.IsShow,
            });
            result.PageIndex = page.PageIndex;
            result.PageSize = page.PageSize;
            result.Data = list;
            result.Code = CommonConstants.SuccessCode;
            result.RecordCount = await totalCount;
            return result;
        }

        public async Task<ResponseContext<YssxCourseDtoNew>> GetCourseById(long id)
        {
            FreeSql.ISelect<YssxCourseNew> select = DbContext.FreeSql.GetRepository<YssxCourseNew>().Select.Where(s => s.IsDelete == CommonConstants.IsNotDelete && s.Id == id);
            var sql = select.ToSql("Id,CourseName,CourseType,CourseTitle,CourseTypeId,Author,Education,Images,PublishingHouse,PublishingDate,Intro,Description,CreateByName,AuditStatus,Uppershelf,ReadCount,AuditStatusExplain,UppershelfExplain,CreateTime,UpdateTime");
            var items = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseDtoNew>(sql);
            return new ResponseContext<YssxCourseDtoNew> { Data = items.Count > 0 ? items[0] : null };
        }

        public async Task<ResponseContext<bool>> SetIsShow(long id, int isShow)
        {
            bool state = await DbContext.FreeSql.GetRepository<YssxCourseNew>().UpdateDiy.Set(x => x.IsShow, isShow).Where(x => x.Id == id).ExecuteAffrowsAsync() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "OK" : "设置失败!" };
        }

        public async Task<bool> CloneCourseTaskJob()
        {

            YssxCourseTaskJobNew dto = await DbContext.FreeSql.GetRepository<YssxCourseTaskJobNew>().Where(x => x.Status == 0).OrderBy(x => x.CreateTime).FirstAsync();
            if (dto == null) return true;
            await DbContext.FreeSql.GetRepository<YssxCourseTaskJobNew>().UpdateDiy.Set(x => x.Status, 1).Set(x => x.UpdateTime, DateTime.Now).Where(x => x.Id == dto.Id).ExecuteAffrowsAsync();
            UserTicket user = new UserTicket
            {
                Id = dto.UserId,
                //TenantId = dto.TenantId
            };
            long cid = dto.CourseId;

            YssxCourseNew course = new YssxCourseNew { Id = dto.NewCourseId };

            //2.克隆章节
            var cloneSections = await CloneSection(cid, course.Id);
            //3.克隆题库
            var yssxTopicPublics = await ClonePublicTopic(cid, dto.TaskType);
            //3.克隆题目
            await CloneTopic(cid, course.Id, cloneSections, yssxTopicPublics);
            //4.克隆文件
            await CloneFiles(cid, course.Id, cloneSections);
            //克隆教材
            await CloneTextBook(cid, course.Id, cloneSections);
            //克隆案例
            await CloneCase(cid, course.Id, cloneSections);
            //克隆场景实训
            await CloneSceneTraining(cid, course.Id, cloneSections);
            //克隆汇总
            await CloneSectionSummery(cid, course.Id, cloneSections);
            //克隆摘要
            await CloneCourseSummery(cid, course.Id);
            //克隆科目
            await CloneSubject(cid, course.Id);

            //一键更新案例下分录题的AnswerValue
            await ReplaceAnswerValue(course.Id, yssxTopicPublics);

            //记录原始备课Id、新备课Id
            Dictionary<long, long> keyValuePairs = new Dictionary<long, long>();
            //克隆课程备课列表
            await CloneLessonPlanning(dto.Id, course.Id, dto.TaskType, keyValuePairs, user);
            //克隆备课题目关系表
            await CloneLessonPlanningTopic(dto.Id, course.Id, keyValuePairs, yssxTopicPublics, user);
            //克隆课程备课文件关系表
            await CloneLessonPlanningFiles(dto.Id, course.Id, keyValuePairs, user);


            await DbContext.FreeSql.GetRepository<YssxCourseCountNew>().InsertAsync(new YssxCourseCountNew { Id = IdWorker.NextId(), CourseId = course.Id, ReadCount = 0, CreateTime = DateTime.Now, DianZanCount = 0, OrderCount = 0 });

            await DbContext.FreeSql.GetRepository<YssxCourseTaskJobNew>().UpdateDiy.Set(x => x.Status, 2).Set(x => x.UpdateTime, DateTime.Now).Where(x => x.Id == dto.Id).ExecuteAffrowsAsync();
            await DbContext.FreeSql.GetRepository<YssxCourseNew>().UpdateDiy.Set(x => x.CloneStatus, 0).Where(x => x.Id == dto.NewCourseId).ExecuteAffrowsAsync();
            return true;
        }

        /// <summary>
        /// 绑定案例到课程
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BandCases(BandCasesDto input, UserTicket user)
        {
            var datas = new List<YssxCourseBindCase>();
            foreach (var item in input.CaseIds)
            {
                datas.Add(new YssxCourseBindCase
                {
                    Id = IdWorker.NextId(),
                    CaseId = item,
                    CourseId = input.CourseId,
                    CreateBy = user.Id,
                });
            }
            if (datas.Count > 0)
                await DbContext.FreeSql.GetRepository<YssxCourseBindCase>().InsertAsync(datas);

            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
        /// <summary>
        /// 绑定案例到课程
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BandCase(long courseId, long caseId, UserTicket user)
        {
            var casebind = new YssxCourseBindCase
            {
                Id = IdWorker.NextId(),
                CaseId = caseId,
                CourseId = courseId,
                CreateBy = user.Id,
            };

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    await uow.GetRepository<YssxCourseBindCase>(e => e.IsDelete == CommonConstants.IsNotDelete)
                             .UpdateDiy.Set(c => new YssxCourseBindCase
                             {
                                 IsDelete = CommonConstants.IsDelete,
                                 UpdateTime = DateTime.Now,
                             }).Where(c => c.CourseId == courseId).ExecuteAffrowsAsync();

                    await uow.GetRepository<YssxCourseBindCase>().InsertAsync(casebind);

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }


            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
        /// <summary>
        /// 删除课程绑定的案例
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCases(long CourseId, long CaseId)
        {
            await DbContext.FreeSql.GetRepository<YssxCourseBindCase>().UpdateDiy.Set(e => new YssxCourseBindCase { IsDelete = CommonConstants.IsDelete, UpdateTime = DateTime.Now })
                .Where(e => e.CaseId == CaseId && e.CourseId == CourseId)
                .ExecuteAffrowsAsync();

            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
    }
}
