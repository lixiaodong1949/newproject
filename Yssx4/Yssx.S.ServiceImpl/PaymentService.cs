﻿using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using Yssx.Framework.Payment;
using Yssx.Framework.Payment.Webchat;
using Yssx.S.Dto.Order;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Order;
using Yssx.S.ServiceImpl.Web;

namespace Yssx.S.ServiceImpl
{
    public class PaymentService : IPaymentService
    {
        private string GenerateOrderNo(int orderType)
        {
            string prefix = string.Empty;
            if (orderType == 1)
            {
                prefix = "ZZSC";
            }
            else if (orderType == 2)
            {
                prefix = "KCTB";
            }
            else if (orderType == 3)
            {
                prefix = "YESC";
            }

            return prefix + DateTime.Now.ToString("yyyyMMdd") + new Random().Next(100000, 999999);
        }

        public async Task<ResponseContext<AlipayResponse>> AlipayWap(CreateRtpOrderDto input, UserTicket currentUser)
        {
            SxRtpOrder sxRtpOrder = input.MapTo<SxRtpOrder>();
            sxRtpOrder.Id = IdWorker.NextId();
            sxRtpOrder.OrderNo = GenerateOrderNo(input.OrderType);
            sxRtpOrder.UserId = currentUser.Id;
            sxRtpOrder.Status = 0;
            sxRtpOrder.PaymentType = 2;
            sxRtpOrder.LoginType = 3;
            sxRtpOrder.PaymentTime = DateTime.Now;
            DbContext.FreeSql.Insert<SxRtpOrder>(sxRtpOrder).ExecuteAffrows();

            Yssx.Framework.Payment.Order order = new Yssx.Framework.Payment.Order();
            order.OrderNo = sxRtpOrder.OrderNo;
            order.Name = input.CaseName;
            order.Amount = input.DiscountPrice.ToString();
            order.Body = input.Description;
            order.ProductCode = input.CaseId.ToString();

            var result = Payment.AlipayWap(order);
            AlipayResponse response = new AlipayResponse();
            if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
            {
                // 支付请求成功
                response.Form = result.Content;
                response.OrderNo = order.OrderNo;
                return new ResponseContext<AlipayResponse>(CommonConstants.SuccessCode, result.Msg, response);
            }
            return new ResponseContext<AlipayResponse>(CommonConstants.ErrorCode, result.Msg, null);
        }

        public async Task<ResponseContext<AlipayResponse>> Alipay(CreateRtpOrderDto input,UserTicket currentUser)
        {
            SxRtpOrder sxRtpOrder = input.MapTo<SxRtpOrder>();
            sxRtpOrder.Id = IdWorker.NextId();
            sxRtpOrder.OrderNo = GenerateOrderNo(input.OrderType);
            sxRtpOrder.UserId = currentUser.Id;
            sxRtpOrder.Status = 0;
            sxRtpOrder.PaymentType = 2;
            sxRtpOrder.LoginType = 0;
            sxRtpOrder.PaymentTime = DateTime.Now;
            DbContext.FreeSql.Insert<SxRtpOrder>(sxRtpOrder).ExecuteAffrows();

            Yssx.Framework.Payment.Order order = new Yssx.Framework.Payment.Order();
            order.OrderNo = sxRtpOrder.OrderNo;
            order.Name = input.CaseName;
            order.Amount = input.DiscountPrice.ToString();
            order.Body = input.Description;
            order.ProductCode = input.CaseId.ToString();

            var result = Payment.Alipay(order);
            AlipayResponse response = new AlipayResponse();
            if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
            {
                // 支付请求成功
                response.Form = result.Content;
                response.OrderNo = order.OrderNo;
                return new ResponseContext<AlipayResponse>(CommonConstants.SuccessCode, result.Msg, response);
            }
            return new ResponseContext<AlipayResponse>(CommonConstants.ErrorCode, result.Msg, null);
        }

        public async void AlipayNotify(Dictionary<string, string> input)
        {
            bool flag = Payment.CheckAlipay(input);
            if (flag)
            {
                //TODO: 交易成功处理
                var orderNo = input["out_trade_no"];
                SxRtpOrder sxRtpOrder = await DbContext.FreeSql.Select<SxRtpOrder>().Where(o => o.OrderNo == orderNo).FirstAsync();
                if ("TRADE_SUCCESS".Equals(input["trade_status"], StringComparison.OrdinalIgnoreCase))
                {
                    sxRtpOrder.Status = 2;
                    sxRtpOrder.TransactionNo = input["trade_no"];
                    sxRtpOrder.PaymentTime = DateTime.Parse(input["gmt_payment"]);
                    DbContext.FreeSql.Update<SxRtpOrder>(sxRtpOrder).SetSource(sxRtpOrder).ExecuteAffrows();

                    // 短信通知
                    var user = await DbContext.FreeSql.Select<YssxUser>().Where(u => u.Id == sxRtpOrder.UserId).FirstAsync();
                    if (!string.IsNullOrWhiteSpace(user.MobilePhone) && sxRtpOrder.Status != 2)
                    {
                        SendSms(sxRtpOrder, user);
                    }
                }
            }
            else
            {
                //TODO: 交易失败处理
            }
        }

        private async void SendSms(SxRtpOrder sxRtpOrder, YssxUser user)
        {
            OrderSms orderSms = new OrderSms();
            if (sxRtpOrder.CaseName.Equals("麓山煤矿"))
            {
                orderSms.cname = "顶岗实习麓山煤矿";
                orderSms.cdate = "5月";
                orderSms.copen = "31日";
            }
            else if (sxRtpOrder.CaseName.Equals("圆梦造纸厂"))
            {
                orderSms.cname = "课程实训圆梦造纸厂";
                orderSms.cdate = "6月";
                orderSms.copen = "12日";
            }
            else if (sxRtpOrder.CaseName.Equals("南邦水泥厂"))
            {
                orderSms.cname = "业税财融合南邦水泥厂";
                orderSms.cdate = "7月";
                orderSms.copen = "31日";
            }
            else if (sxRtpOrder.CaseName.Equals("抢购搭配价"))
            {
                orderSms.cname = "企业搭配套餐";
                orderSms.cdate = "5月";
                orderSms.copen = "31日起";
            }
            string sms = JsonConvert.SerializeObject(orderSms);
            CommonLogger.Error(sms);
            var result = DysmsService.SendSms(user.MobilePhone, sms);
            CommonLogger.Error(JsonConvert.SerializeObject(result));
        }

        public async Task<ResponseContext<WebchatResponse>> Webchat(CreateRtpOrderDto input,UserTicket currentUser)
        {
            SxRtpOrder sxRtpOrder = input.MapTo<SxRtpOrder>();
            sxRtpOrder.Id = IdWorker.NextId();
            sxRtpOrder.OrderNo = GenerateOrderNo(input.OrderType);
            sxRtpOrder.UserId = currentUser.Id;
            sxRtpOrder.Status = 0;
            sxRtpOrder.PaymentType = 1;
            sxRtpOrder.LoginType = 0;
            sxRtpOrder.PaymentTime = DateTime.Now;
            DbContext.FreeSql.Insert<SxRtpOrder>(sxRtpOrder).ExecuteAffrows();

            Yssx.Framework.Payment.Order order = new Yssx.Framework.Payment.Order();
            order.OrderNo = sxRtpOrder.OrderNo;
            order.Name = input.CaseName;
            order.Amount = input.DiscountPrice.ToString();
            order.Body = input.Description;
            order.ProductCode = input.CaseId.ToString();
            try
            {
                var result = Payment.Webchat(order);
                WebchatResponse response = new WebchatResponse();
                if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
                {
                    // 支付请求成功
                    response.Url = result.Content;
                    response.OrderNo = order.OrderNo;
                    return new ResponseContext<WebchatResponse>(CommonConstants.SuccessCode, result.Msg, response);
                }
                return new ResponseContext<WebchatResponse>(CommonConstants.ErrorCode, result.Msg, null);
            }
            catch (Exception ex)
            {
                return new ResponseContext<WebchatResponse>(CommonConstants.ErrorCode, ex.Message, null);
                throw;
            }
        }

        public async Task<ResponseContext<WebchatResponse>> WebchatWap(CreateRtpOrderDto input, UserTicket currentUser)
        {
            SxRtpOrder sxRtpOrder = input.MapTo<SxRtpOrder>();
            sxRtpOrder.Id = IdWorker.NextId();
            sxRtpOrder.OrderNo = GenerateOrderNo(input.OrderType);
            sxRtpOrder.UserId = currentUser.Id;
            sxRtpOrder.Status = 0;
            sxRtpOrder.PaymentType = 1;
            sxRtpOrder.PaymentTime = DateTime.Now;
            sxRtpOrder.LoginType = 3;
            DbContext.FreeSql.Insert<SxRtpOrder>(sxRtpOrder).ExecuteAffrows();

            Yssx.Framework.Payment.Order order = new Yssx.Framework.Payment.Order();
            order.OrderNo = sxRtpOrder.OrderNo;
            order.Name = input.CaseName;
            order.Amount = input.DiscountPrice.ToString();
            order.Body = input.Description;
            order.ProductCode = input.CaseId.ToString();
            try
            {
                var result = Payment.WebchatWap(order);
                WebchatResponse response = new WebchatResponse();
                if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
                {
                    // 支付请求成功
                    response.Url = result.Content;
                    response.OrderNo = order.OrderNo;
                    return new ResponseContext<WebchatResponse>(CommonConstants.SuccessCode, result.Msg, response);
                }
                return new ResponseContext<WebchatResponse>(CommonConstants.ErrorCode, result.Msg, null);
            }
            catch (Exception ex)
            {
                return new ResponseContext<WebchatResponse>(CommonConstants.ErrorCode, ex.Message, null);
                throw;
            }
        }

        public async void WebchatNotify(string xmlString)
        {
            bool flag = Payment.CheckWebchat(xmlString);
            CommonLogger.Info(xmlString);
            if (flag)
            {
                //TODO: 交易成功处理
                WxPayData data = new WxPayData();
                data.FromXml(xmlString);
                string orderNo = data.GetValue("out_trade_no").ToString();
                SxRtpOrder sxRtpOrder = await DbContext.FreeSql.Select<SxRtpOrder>().Where(o => o.OrderNo == orderNo).FirstAsync();

                sxRtpOrder.Status = 2;
                sxRtpOrder.TransactionNo = data.GetValue("transaction_id").ToString();
                sxRtpOrder.PaymentTime = DateTime.ParseExact(data.GetValue("time_end").ToString(), "yyyyMMddHHmmss", CultureInfo.CurrentCulture);
                DbContext.FreeSql.Update<SxRtpOrder>(sxRtpOrder).SetSource(sxRtpOrder).ExecuteAffrows();

                // 短信通知
                var user = await DbContext.FreeSql.Select<YssxUser>().Where(u => u.Id == sxRtpOrder.UserId).FirstAsync();
                if (!string.IsNullOrWhiteSpace(user.MobilePhone) && sxRtpOrder.Status != 2)
                {
                    SendSms(sxRtpOrder, user);
                }

                // 分账
                var amount = (int)(sxRtpOrder.DiscountPrice * 0.3M);
                var account = user.WeChat;
                Receiver[] receivers = new Receiver[1] { new Receiver() {  Type = "PERSONAL_OPENID", Account = account, Amount = amount, Description = "云上实训分账"} };
                ProfitSharingOrder profitSharingOrder = new ProfitSharingOrder();
                profitSharingOrder.TransactionId = sxRtpOrder.TransactionNo;
                profitSharingOrder.OrderNo = sxRtpOrder.OrderNo;
                profitSharingOrder.Receivers = JsonConvert.SerializeObject(receivers);
                WechatWebPay webchat = new WechatWebPay();
                webchat.ProfitSharing(profitSharingOrder);
            }
            else
            {
                //TODO: 交易失败处理
            }
        }

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResponse<RtpOrderDto>> GetOrderList(GetRtpOrderDto input)
        {
            var select = DbContext.FreeSql.Select<SxRtpOrder,YssxUser>()
                .LeftJoin((o,u) => o.UserId == u.Id)
                .WhereIf(input.UserId.HasValue && input.UserId > 0, (o, u) => o.UserId == input.UserId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Name), (o, u) => o.CaseName.Contains(input.Name))
                .WhereIf(!string.IsNullOrWhiteSpace(input.OrderNo), (o, u) => o.OrderNo.Contains(input.OrderNo))
                .WhereIf(!string.IsNullOrWhiteSpace(input.MobilePhone),(o,u) => u.MobilePhone.Contains(input.MobilePhone))
                .WhereIf(!string.IsNullOrWhiteSpace(input.RealName),(o,u) => u.RealName.Contains(input.RealName));
            var totalCount = select.Count();
            var sql = select.Page(input.PageIndex, input.PageSize).OrderByDescending((o, u) => o.PaymentTime).ToSql("a.*,b.MobilePhone,b.RealName,b.WeChat");
            var items = await DbContext.FreeSql.Ado.QueryAsync<RtpOrderDto>(sql);

            return new PageResponse<RtpOrderDto>() { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items };
        }

        /// <summary>
        /// 获取用户所有订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserRtpOrder>> GetUserOrders(GetUserRtpOrderDto input)
        {
            UserRtpOrder userRtpOrder = new UserRtpOrder();
            var user = await DbContext.FreeSql.Select<YssxUser>().Where(u => u.Id == input.UserId && u.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (user == null)
                return new ResponseContext<UserRtpOrder>(CommonConstants.ErrorCode, "没有找到该用户！", null);
            var school = await DbContext.FreeSql.Select<YssxSchool>().Where(s => s.Id == user.TenantId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (school == null)
                return new ResponseContext<UserRtpOrder>(CommonConstants.ErrorCode, "没有找到该用户的所属学校！", null);
            else if (school.Id == CommonConstants.DefaultTenantId)
                userRtpOrder.SchoolName = "";
            var select = DbContext.FreeSql.Select<SxRtpOrder>().Where(o => o.UserId == input.UserId);
            var totalCount = select.Count();
            var sql = select.Page(input.PageIndex, input.PageSize).OrderByDescending(o => o.PaymentTime).ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<RtpOrderDto>(sql);
            userRtpOrder.Orders = new PageResponse<RtpOrderDto>() { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items};
            return new ResponseContext<UserRtpOrder>(CommonConstants.SuccessCode,"",userRtpOrder);
        }

        /// <summary>
        /// 获取是否已下过订单
        /// </summary>
        /// <param name="caseId">案例id</param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> GetCaseOrderStatus(long caseId,UserTicket currentUser)
        {
            var order = await DbContext.FreeSql.Select<SxRtpOrder>()
                .Where(o => o.CaseId == caseId)
                .Where(o => o.UserId == currentUser.Id)
                .Where(o => o.Status == 2).FirstAsync();
            if (order == null)
                return new ResponseContext<bool>(CommonConstants.SuccessCode, "此商品未有支付过的订单", false);
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "此商品已有支付过的订单", true);
        }

        /// <summary>
        /// 统计
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<OrderStatsDto>> GetStats()
        {
            OrderStatsDto statsDto = new OrderStatsDto();
            var sxRtpOrders =  DbContext.FreeSql.Select<SxRtpOrder>().Where(o => o.PaymentTime >= DateTime.Now.Date).Where(o => o.PaymentTime <= DateTime.Now).Where(o => o.Status == 2);
            statsDto.TodayAmount = await sxRtpOrders.SumAsync(o => o.DiscountPrice);
            statsDto.TodayCount = await sxRtpOrders.CountAsync();
            statsDto.TotalAmount = await DbContext.FreeSql.Select<SxRtpOrder>().Where(o => o.Status == 2).SumAsync(o => o.DiscountPrice);
            return new ResponseContext<OrderStatsDto>(CommonConstants.SuccessCode, "",statsDto);
        }

        /// <summary>
        /// 取消订单
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CancelOrder(CancelOrderInputDto input, bkUserTicket currentUser)
        {
            var order = await DbContext.FreeSql.Select<SxRtpOrder>().Where(o => o.Id == input.OrderId).FirstAsync();
            if (order == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode,"该订单不存在！",false);
            order.Status = 1;
            order.Remark = input.Remark;
            order.UpdateBy = currentUser.Id;
            order.UpdateTime = DateTime.Now;
            DbContext.FreeSql.Update<SxRtpOrder>(order).SetSource(order).ExecuteAffrows();
            return new ResponseContext<bool>(CommonConstants.SuccessCode,"成功取消！",true);
        }
    }
}
