﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Repository.Extensions;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.IServices.Examination;
using Yssx.S.Poco;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl.Examination
{
    /// <summary>
    /// 教师/教务 试卷相关服务
    /// </summary>
    public class ExamPaperService : IExamPaperService
    {
        /// <summary>
        /// 无参构造
        /// </summary>
        public ExamPaperService()
        {

        }

        /// <summary>
        /// 获取考试岗位信息
        /// </summary>
        /// <param name="examId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<ExamPaperPostionView>>> GetExamPaperPostions(long examId)
        {
            var result = new ResponseContext<List<ExamPaperPostionView>>();

            if (examId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = $"ExamId不能为0 ";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<ExamPaper>().Where(s => s.Id == examId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamById}{examId}", true, 10 * 60);
            if (exam == null)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"考试信息不存在 ";
                return result;
            }

            var list = await DbContext.FreeSql.GetRepository<YssxCasePosition>().Where(s => s.CaseId == exam.CaseId && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync($"{CommonConstants.Cache_GetCasePositionsByCaseId}{exam.CaseId}", true, 10 * 60, true);

            result.Data = list.Select(s => new ExamPaperPostionView()
            {
                ExamId = examId,
                PostionId = s.PostionId,
                PostionName = s.PostionName
            }).ToList();

            return result;
        }
    }
}
