﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Tas.Common.IdGenerate;
using Tas.Common.Utils;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Redis;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.Poco;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Exam;
using Yssx.Repository.Extensions;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Logger;
using Yssx.Framework.AutoMapper;
using Yssx.S.Dto;
using Yssx.Framework.Utils;
using Yssx.Framework.AnswerCompare.EntitysV2;
using Yssx.Framework.AnswerCompare;
using Yssx.S.IServices.Examination;
using Yssx.S.Pocos.Topics;
using Yssx.S.Pocos.SceneTraining;
using Yssx.S.Dto.CourseExam;
using System.IO;
using Aop.Api.Domain;

namespace Yssx.S.ServiceImpl.Examination
{
    /// <summary>
    /// 课程练习做题相关服务 
    /// </summary>
    public class CourseExercisesService : ICourseExercisesService
    {
        /// <summary>
        /// 无参构造函数
        /// </summary>
        public CourseExercisesService()
        {

        }

        #region 课程练习
        /// <summary>
        /// 获取当前用户课程（自己的课程和购买的课程）
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CourseInfoDto>>> GetCourseInfoByUser(UserTicket currentUser)
        {
            var userId = currentUser.Id;
            var userType = currentUser.Type;
            var tenantId = currentUser.TenantId;
            List<CourseInfoDto> resultData = new List<CourseInfoDto>();
            long tenantid2 = 981239287939076;
            if (userType == 6)
            {
                //专业组
                var sql = DbContext.FreeSql.GetRepository<YssxCourseOrder>().Select
                                .From<YssxCourse>((a, b) =>
                                 a.LeftJoin(aa => aa.CourseId == b.Id))
                                .Where((a, b) => b.IsDelete == CommonConstants.IsNotDelete && a.TenantId == tenantId)
                                .ToSql("b.Id as CourseId,b.CourseName,b.Images,b.Education,b.CourseType");
                resultData = await DbContext.FreeSql.Ado.QueryAsync<CourseInfoDto>(sql);
            }
            else if (currentUser.Education == -1)
            {
                var sql = DbContext.FreeSql.GetRepository<YssxCourseOrder>().Select
                               .From<YssxCourse>((a, b) =>
                                a.LeftJoin(aa => aa.CourseId == b.Id))
                               .Where((a, b) => b.IsDelete == CommonConstants.IsNotDelete && a.TenantId == tenantid2 && b.AuditStatus == 1)
                               .ToSql("b.Id as CourseId,b.CourseName,b.Images,b.Education,b.CourseType");
                resultData = await DbContext.FreeSql.Ado.QueryAsync<CourseInfoDto>(sql);
            }
            else
            {
                //查找自己的课程
                //var sql = DbContext.FreeSql.GetRepository<YssxCourse>().Select
                //                .Where(a => a.CreateBy == userId && a.IsDelete == CommonConstants.IsNotDelete)
                //                .ToSql("Id as CourseId,CourseName,Images,Education,CourseType");

                //var items = await DbContext.FreeSql.Ado.QueryAsync<CourseInfoDto>(sql);
                ////查找购买的课程
                //var sql1 = DbContext.FreeSql.GetRepository<YssxCourseOrder>().Select
                //                .From<YssxCourse>((a, b) =>
                //                 a.LeftJoin(aa => aa.CourseId == b.Id))
                //                .Where((a, b) => b.IsDelete == CommonConstants.IsNotDelete && a.CustomerId == userId)
                //                .ToSql("b.Id as CourseId,b.CourseName,b.Images,b.Education,b.CourseType");
                //var items1 = await DbContext.FreeSql.Ado.QueryAsync<CourseInfoDto>(sql1);
                //resultData = items.Union(items1).ToList();
                var sql = DbContext.FreeSql.GetRepository<YssxCourse>().Select
                              .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.TenantId == tenantid2 && a.AuditStatus == 1 && a.Education == currentUser.Education)
                              .ToSql("a.Id as CourseId,a.CourseName,a.Images,a.Education,a.CourseType");
                resultData = await DbContext.FreeSql.Ado.QueryAsync<CourseInfoDto>(sql);
            }
            //resultData.ForEach(s =>
            //{
            //    var questionList = DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Select
            //                    .From<ExamCourseUserGrade>((a, b) =>
            //                     a.LeftJoin(aa => aa.GradeId == b.Id))
            //                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && b.CourseId == s.CourseId && b.UserId == userId)
            //                    .GroupBy((a, b) => new { a.QuestionId, a.Status })
            //                    .ToList(a => new {
            //                        a.Key.QuestionId,
            //                        a.Key.Status
            //                    });
            //    s.DoQuestionCount = questionList.Select(x=>x.QuestionId).Distinct().Count();
            //    s.ErrorCount = questionList.Where(x => x.Status == AnswerResultStatus.Error || x.Status == AnswerResultStatus.PartRight).Select(x=>x.QuestionId).Distinct().Count();
            //    s.QuestionCount = DbContext.FreeSql.GetRepository<YssxSectionTopic>().Select.Where(a => a.CourseId == s.CourseId && a.IsDelete == CommonConstants.IsNotDelete)
            //                    .ToList($"{CommonConstants.Cache_GetQuestionCountByCourseId}{s.CourseId}", true, 10 * 60).Count();
            //});
            return new ResponseContext<List<CourseInfoDto>>(CommonConstants.SuccessCode, null, resultData);
        }
        /// <summary>
        /// 根据课程id获取章信息
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        public async Task<PageResponse<SectionInfoDto>> GetSectionListByCourse(long courseId, int pageSize, int pageIndex, UserTicket user)
        {
            var userId = user.Id;
            //章列表
            var oldstudent = DbContext.FreeSql.Select<YssxStudent>().Where(m => m.UserId == userId && m.IsDelete == CommonConstants.IsNotDelete).First();
            long classId = 0;
            if (oldstudent != null)
                classId = oldstudent.ClassId;

            var select = DbContext.FreeSql.GetRepository<YssxSection>().Select.From<YssxPracticeDetails>((a, b) =>a.LeftJoin(aa=>aa.Id==b.TenantId && b.ClassId== classId && b.IsDelete== CommonConstants.IsNotDelete))
                .Where((a, b) => a.CourseId == courseId && a.ParentId == 0 && a.IsDelete == CommonConstants.IsNotDelete);
            var totalCount = select.Count();
            var sql = select.Page(pageIndex, pageSize).OrderBy((a, b) => a.Sort).ToSql("a.Id as SectionId,SectionName,SectionTitle,b.State Close,b.AnswerState AnswerClose");
            var sectionList = await DbContext.FreeSql.Ado.QueryAsync<SectionInfoDto>(sql);

            return new PageResponse<SectionInfoDto>() { PageSize = pageSize, PageIndex = pageIndex, RecordCount = totalCount, Data = sectionList };
        }

        /// <summary>
        /// 根据章Id获取节信息
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<SubSectionInfoDto>>> GetSubSectionList(long sectionId, long userId)
        {
            List<SubSectionInfoDto> subSectionInfoList = new List<SubSectionInfoDto>();
            subSectionInfoList = await DbContext.FreeSql.GetRepository<YssxSection>().Select
            .From<YssxSectionTopic, ExamCourseUserLastGrade, ExamCourseUserGradeDetail>((a, b, c, d) =>
                    a.LeftJoin(aa => aa.Id == b.SectionId && b.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.Id == c.SectionId && c.UserId == userId)
                    .LeftJoin(aa => c.GradeId == d.GradeId && d.QuestionId == d.ParentQuestionId)
            )
            .Where((a, b, c, d) => a.ParentId == sectionId && a.IsDelete == CommonConstants.IsNotDelete)
            .OrderBy((a, b, c, d) => a.Sort)
            .GroupBy((a, b, c, d) => new { a.Id, a.SectionName, a.SectionTitle, c.GradeId })
            .ToListAsync(a => new SubSectionInfoDto()
            {
                SectionId = a.Key.Id,
                SectionName = a.Key.SectionName,
                SectionTitle = a.Key.SectionTitle,
                TotalScore = 0, //TODO  YssxSectionTopic 增加题目分数字段
                GradeId = a.Key.GradeId,
                //Status = a.Key.Status,  //TODO mysql字符集bug
                TotalQuestionCount = Convert.ToInt32("count(distinct b.id)"),
                DoQuestionCount = Convert.ToInt32("count(distinct d.id)"),
            });



            List<ExamCourseUserGrade> userGrades = await DbContext.FreeSql.GetRepository<YssxSection>().Select.From<ExamCourseUserGrade>((a, b) => a.InnerJoin(x => x.Id == b.SectionId)).Where((a, b) => a.ParentId == sectionId && b.UserId == userId && a.IsDelete == CommonConstants.IsNotDelete).ToListAsync((a, b) => new ExamCourseUserGrade
            {
                Id = b.Id,
                Status = b.Status
            });

            if (subSectionInfoList.Any())
            {
                subSectionInfoList.AsParallel().ForAll(a =>
                {
                    if (a.GradeId > 0)
                    {
                        var grade = userGrades.Where(e => e.Id == a.GradeId).FirstOrDefault();
                        if (grade != null)
                        {
                            a.Status = grade.Status;
                            //状态为已结束则设置gradeId=0

                            if (a.Status == StudentExamStatus.End)
                            {
                                a.GradeId = 0;
                                a.DoQuestionCount = 0;
                            }
                        }
                        else
                        {
                            a.Status = StudentExamStatus.Wait;
                        }

                    }
                });
            }
            return new ResponseContext<List<SubSectionInfoDto>>(CommonConstants.SuccessCode, null, subSectionInfoList);
        }

        #endregion

        #region 备课课程练习
        /// <summary>
        /// 获取当前用户所在备课的课程
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<PrepareCourseInfoDto>>> GetPrepareCourseInfoByUser(UserTicket currentUser)
        {
            var userId = currentUser.Id;
            //查找当前用户所在班级
            var classId = await DbContext.FreeSql.GetRepository<YssxStudent>()
                .Where(s => s.UserId == userId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync(s => s.ClassId);
            if (classId > 0)
            {
                //所在备课课程id集合
                var courseIds = await DbContext.FreeSql.GetRepository<YssxTeacherCourseClass>()
                .Where(s => s.ClassId == classId && s.IsDelete == CommonConstants.IsNotDelete).ToListAsync(s => s.CourseId);
                //查找当前用户备课课程
                var sql = DbContext.FreeSql.GetRepository<YssxPrepareCourse>().Select
                                .Where(a => courseIds.Contains(a.Id) && a.IsDelete == CommonConstants.IsNotDelete)
                                .ToSql("Id as CourseId,CourseName,Images,Education,CourseType");
                var items = await DbContext.FreeSql.Ado.QueryAsync<PrepareCourseInfoDto>(sql);
                return new ResponseContext<List<PrepareCourseInfoDto>>(CommonConstants.SuccessCode, null, items);
            }
            else
            {
                return new ResponseContext<List<PrepareCourseInfoDto>>(CommonConstants.SuccessCode, null, null);
            }
        }
        /// <summary>
        /// 根据备课课程id获取章节信息
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        public async Task<PageResponse<SectionInfoDto>> GetPrepareSectionListByCourse(long courseId, int pageSize, int pageIndex, UserTicket user)
        {
            var userId = user.Id;
            //章列表
            var select = DbContext.FreeSql.GetRepository<YssxPrepareSection>()
                .Where(s => s.CourseId == courseId && s.ParentId == 0 && s.IsDelete == CommonConstants.IsNotDelete);
            var totalCount = select.Count();
            var sql = select.Page(pageIndex, pageSize).OrderBy(a => a.Sort).ToSql("Id as SectionId,SectionName,SectionTitle");
            var sectionList = await DbContext.FreeSql.Ado.QueryAsync<SectionInfoDto>(sql);

            return new PageResponse<SectionInfoDto>() { PageSize = pageSize, PageIndex = pageIndex, RecordCount = totalCount, Data = sectionList };
        }
        /// <summary>
        /// 根据章Id获取节信息
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<SubSectionInfoDto>>> GetPrepareSubSectionList(long sectionId, long userId)
        {
            List<SubSectionInfoDto> subSectionInfoList = new List<SubSectionInfoDto>();
            subSectionInfoList = await DbContext.FreeSql.GetRepository<YssxPrepareSection>().Select
            .From<YssxPrepareSectionTopic, ExamCourseUserLastGrade, ExamCourseUserGradeDetail>((a, b, c, d) =>
                    a.LeftJoin(aa => aa.Id == b.SectionId && b.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.Id == c.SectionId && c.UserId == userId)
                    //.LeftJoin(aa=>c.GradeId==d.Id)
                    .LeftJoin(aa => c.GradeId == d.GradeId && d.QuestionId == d.ParentQuestionId)
            )
            .Where((a, b, c, d) => a.ParentId == sectionId && a.IsDelete == CommonConstants.IsNotDelete)
            .GroupBy((a, b, c, d) => new { a.Id, a.SectionName, a.SectionTitle, c.GradeId })
            .ToListAsync(a => new SubSectionInfoDto()
            {
                SectionId = a.Key.Id,
                SectionName = a.Key.SectionName,
                SectionTitle = a.Key.SectionTitle,
                TotalScore = 0, //TODO  YssxSectionTopic 增加题目分数字段
                GradeId = a.Key.GradeId,
                //Status = a.Key.Status,  //TODO mysql字符集bug
                TotalQuestionCount = Convert.ToInt32("count(distinct b.id)"),
                DoQuestionCount = Convert.ToInt32("count(distinct d.id)"),
            });

            if (subSectionInfoList.Any())
            {
                subSectionInfoList.AsParallel().ForAll(async a =>
                {
                    if (a.GradeId > 0)
                    {
                        var grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(e => e.Id == a.GradeId).FirstAsync();
                        if (grade != null)
                        {
                            a.Status = grade.Status;
                            //状态为已结束则设置gradeId=0
                            a.GradeId = a.Status == StudentExamStatus.End ? 0 : a.GradeId;
                        }
                        else
                        {
                            a.Status = StudentExamStatus.Wait;
                        }

                    }
                });
            }
            return new ResponseContext<List<SubSectionInfoDto>>(CommonConstants.SuccessCode, null, subSectionInfoList);
        }
        /// <summary>
        /// 获取试卷信息--接口方法（包括试卷基本信息和试卷包含的题目信息）备课课程练习
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetPrepareCourseExamPaperBasicInfo(long courseId, long sectionId, UserTicket user, long gradeId = 0)
        {
            var result = new ResponseContext<ExamPaperCourseBasicInfo>();
            //课程信息
            var course = await DbContext.FreeSql.GetRepository<YssxPrepareCourse>().Where(s => s.Id == courseId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            var sectionList = await DbContext.FreeSql.GetRepository<YssxPrepareSection>().Select
                .From<YssxPrepareSectionTopic, YssxTopicPublic>((a, b, c) =>
                a.LeftJoin(aa => aa.Id == b.SectionId)
                .LeftJoin(aa => b.QuestionId == c.Id)
                )
                .Where((a, b, c) => a.Id == sectionId && a.IsDelete == CommonConstants.IsNotDelete)
                .GroupBy((a, b, c) => new { a.Id, a.SectionName, a.SectionTitle })
                .ToListAsync(a => new
                {
                    a.Key.Id,
                    a.Key.SectionName,
                    a.Key.SectionTitle,
                    TotalScore = Convert.ToDecimal("sum(c.Score)"),
                    TotalQuestionCount = Convert.ToInt32("count(c.id)")
                });
            if (course == null || sectionList.Count <= 0)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"课程或章节不存在 ";
                return result;
            }
            var section = sectionList[0];
            //判断试卷是否存在，不存在则创建试卷
            var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(s => s.CourseId == courseId && s.SectionId == sectionId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamCourseByCourseSectionId}{courseId}:{sectionId}", true, 10 * 60);
            if (exam == null)
            {
                exam = new ExamPaperCourse
                {
                    Id = IdWorker.NextId(),
                    Name = $"{course.CourseName}:{section.SectionTitle}",
                    CourseId = courseId,
                    SectionId = sectionId,
                    ExamType = CourseExamType.CourceTest,
                    TotalScore = section.TotalScore,
                    TotalQuestionCount = section.TotalQuestionCount,
                    //TenantId = user.TenantId,
                    //BeginTime = p.StartTime,
                    //EndTime = p.EndTime,
                    CanShowAnswerBeforeEnd = true,
                    Status = ExamStatus.Wait
                };
                DbContext.FreeSql.GetRepository<ExamPaperCourse>().Insert(exam);
            }

            switch (exam.ExamType)
            {
                case CourseExamType.CourceTest:

                    return await GetCourceTestExamPaperBasicInfo(exam, gradeId, user);

                default:
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"不存在的考试类型 ";

                    return result;
            }

        }
        #endregion

        #region 答题相关接口

        #region 获取试卷所有题目信息
        /// <summary>
        /// 获取试卷信息--接口方法（包括试卷基本信息和试卷包含的题目信息）课程练习
        /// </summary>
        /// 
        /// <returns></returns>
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourseExamPaperBasicInfo(long courseId, long sectionId, UserTicket user, long gradeId = 0)
        {
            var result = new ResponseContext<ExamPaperCourseBasicInfo>();
            //课程信息
            var course = await DbContext.FreeSql.GetRepository<YssxCourse>().Where(s => s.Id == courseId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            var sectionList = await DbContext.FreeSql.GetRepository<YssxSection>().Select
                .From<YssxSectionTopic, YssxTopicPublic>((a, b, c) =>
                a.LeftJoin(aa => aa.Id == b.SectionId)
                .LeftJoin(aa => b.QuestionId == c.Id)
                )
                .Where((a, b, c) => a.Id == sectionId && a.IsDelete == CommonConstants.IsNotDelete)
                .GroupBy((a, b, c) => new { a.Id, a.SectionName, a.SectionTitle })
                .ToListAsync(a => new
                {
                    a.Key.Id,
                    a.Key.SectionName,
                    a.Key.SectionTitle,
                    TotalScore = Convert.ToDecimal("sum(c.Score)"),
                    TotalQuestionCount = Convert.ToInt32("count(c.id)")
                });
            if (course == null || sectionList.Count <= 0)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"课程或章节不存在 ";
                return result;
            }
            var section = sectionList[0];
            //判断试卷是否存在，不存在则创建试卷
            var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(s => s.CourseId == courseId && s.SectionId == sectionId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamCourseByCourseSectionId}{courseId}:{sectionId}", true, 10 * 60);
            if (exam == null)
            {
                exam = new ExamPaperCourse
                {
                    Id = IdWorker.NextId(),
                    Name = $"{course.CourseName}:{section.SectionTitle}",
                    CourseId = courseId,
                    SectionId = sectionId,
                    ExamType = CourseExamType.CourceTest,
                    TotalScore = section.TotalScore,
                    TotalQuestionCount = section.TotalQuestionCount,
                    //TenantId = user.TenantId,
                    //BeginTime = p.StartTime,
                    //EndTime = p.EndTime,
                    CanShowAnswerBeforeEnd = true,
                    Status = ExamStatus.Wait
                };
                DbContext.FreeSql.GetRepository<ExamPaperCourse>().Insert(exam);
            }

            switch (exam.ExamType)
            {
                case CourseExamType.CourceTest:

                    return await GetCourceTestExamPaperBasicInfo(exam, gradeId, user);

                default:
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"不存在的考试类型 ";

                    return result;
            }

        }
        /// <summary>
        /// 获取任务试卷信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetTaskExamPaperBasicInfo(long taskId, UserTicket user, long gradeId = 0)
        {
            var result = new ResponseContext<ExamPaperCourseBasicInfo>();
            //任务信息
            //var task = await DbContext.FreeSql.GetRepository<YssxTask>().Where(s => s.Id == taskId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(s => s.TaskId == taskId && s.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync($"{CommonConstants.Cache_GetExamCourseByTaskId}{taskId}", true, 10 * 60);
            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"任务试卷不存在！";
                return result;
            }
            
            var grade = new ExamCourseUserGrade();
            //grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(s => s.Id == gradeId && s.UserId == user.Id && s.IsDelete==CommonConstants.IsNotDelete).FirstAsync();
            if (gradeId > 0)
            {
                grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(s => s.Id == gradeId && s.UserId == user.Id && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

                if (grade == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"答题记录不存在 ";
                    return result;
                }

                if (grade.Status == StudentExamStatus.End)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"本次测试已经结束了！";
                    return result;
                }
            }
            else
            {
                grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(s => s.TaskId == taskId && s.UserId == user.Id && s.IsDelete == CommonConstants.IsNotDelete).Master().FirstAsync();
                if (grade != null)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"答题记录已存在，不能重复创建,请重试 ";
                    return result;
                }
                //加用户锁
                var gradeResult = RedisLock.WaitLock(user.Id.ToString(), () =>
                {
                    return InsertGrade(exam, user);
                });

                if (gradeResult.Code == CommonConstants.SuccessCode)
                {
                    grade = gradeResult.Data;
                }
                else
                {
                    result.Code = gradeResult.Code;
                    result.Msg = gradeResult.Msg;
                    return result;
                }
                
            }
            if (grade.IsFirstTime)
            {
                if (exam.ExamType != CourseExamType.ClassExam)
                {
                    grade.IsFirstTime = false;
                    grade.RecordTime = DateTime.Now;
                    await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().UpdateDiy.SetSource(grade)
                        .UpdateColumns(a => new { a.IsFirstTime, a.RecordTime }).ExecuteAffrowsAsync();
                }
            }

            //if (grade.Status == StudentExamStatus.End)
            //{
            //    result.Code = CommonConstants.ErrorCode;
            //    result.Msg = $"本次考试任务已经结束了！";
            //    return result;
            //}
            return await GetExamPaperBasicInfoBase(exam, grade);

        }
        #endregion


        #region 获取题目信息

        /// <summary>
        /// 获取题目信息--接口方法（课程练习，作业，任务，考试等）
        /// </summary>
        /// <param name="progressId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamCourseQuestionInfo>> GetCourseQuestionInfo(long gradeId, CourseExamType examType, long questionId, UserTicket userTicket)
        {
            var result = new ResponseContext<ExamCourseQuestionInfo>(CommonConstants.BadRequest, $"");
            try
            {
                var user = userTicket;
                ExamCourseUserGrade grade = null;
                if (gradeId > 0)
                {
                    grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                    if (grade == null)
                    {
                        result.Code = CommonConstants.NotFund;
                        result.Msg = $"主作答记录不存在";
                        return result;
                    }
                }
                var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync($"{CommonConstants.Cache_GetExamCourseById}{grade.ExamId}", true, 10 * 60);

                if (exam == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"考试信息不存在";
                    return result;
                }
                #region 校验
                var question = await DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.Id == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync($"{CommonConstants.Cache_GetPublicTopicById}{questionId}", true, 10 * 60, true);

                if (question == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"题目不存在";
                    return result;
                }

                #endregion

                switch (examType)
                {
                    case CourseExamType.CourceTest:

                        return await GetCourceTestQuestionInfo(exam, grade, question, user);

                    case CourseExamType.ClassHomeWork:
                    case CourseExamType.ClassTask:
                    case CourseExamType.ClassExam:
                    case CourseExamType.ClassTest:
                    case CourseExamType.NewCourseTask:
                        return await GetTaskQuestionInfo(exam, grade, question, user);
                    default:

                        throw new NotImplementedException($"{examType}类型不存在");
                }
            }
            catch (Exception ex)
            {
                result.Msg = "系统异常";
                result.Code = CommonConstants.ErrorCode;
                CommonLogger.Error(JsonHelper.SerializeObject(ex));
            }

            return result;
        }
        /// <summary>
        /// 预览题目作答信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamCourseQuestionInfo>> GetCourseQuestionInfoView(long gradeId, long questionId, UserTicket userTicket)
        {
            var result = new ResponseContext<ExamCourseQuestionInfo>(CommonConstants.BadRequest, $"");
            try
            {
                var user = userTicket;

                ExamCourseUserGrade grade = null;
                if (gradeId > 0)
                {
                    grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                    if (grade == null)
                    {
                        result.Code = CommonConstants.NotFund;
                        result.Msg = $"主作答记录不存在";
                        return result;
                    }
                }
                var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync($"{CommonConstants.Cache_GetExamCourseById}{grade.ExamId}", true, 10 * 60);

                if (exam == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"考试信息不存在";
                    return result;
                }
                #region 校验
                var question = await DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.Id == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync($"{CommonConstants.Cache_GetPublicTopicById}{questionId}", true, 10 * 60, true);

                if (question == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"题目不存在";
                    return result;
                }

                #endregion

                result = await GetQuestionInfoBase(exam, grade, question, isView: true);
            }
            catch (Exception ex)
            {
                result.Msg = "系统异常";
                result.Code = CommonConstants.ErrorCode;
                CommonLogger.Error(JsonHelper.SerializeObject(ex));
            }

            return result;

            #endregion

        }
        /// <summary>
        /// 获取题目基本信息--课程练习
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="progress"></param>
        /// <param name="question"></param>
        /// <returns></returns>
        private async Task<ResponseContext<ExamCourseQuestionInfo>> GetCourceTestQuestionInfo(ExamPaperCourse exam, ExamCourseUserGrade grade, YssxTopicPublic question, UserTicket user)
        {
            return await GetQuestionInfoBase(exam, grade, question);
        }
        /// <summary>
        /// 获取题目基本信息--任务
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="progress"></param>
        /// <param name="question"></param>
        /// <returns></returns>
        private async Task<ResponseContext<ExamCourseQuestionInfo>> GetTaskQuestionInfo(ExamPaperCourse exam, ExamCourseUserGrade grade, YssxTopicPublic question, UserTicket user)
        {
            return await GetQuestionInfoBase(exam, grade, question);
        }
        /// <summary>
        /// 获取题目信息基础函数
        /// </summary>
        /// <returns></returns>
        private async Task<ResponseContext<ExamCourseQuestionInfo>> GetQuestionInfoBase(ExamPaperCourse exam, ExamCourseUserGrade grade, YssxTopicPublic question, bool isView = false)
        {
            var result = new ResponseContext<ExamCourseQuestionInfo>();

            var gradeId = grade != null ? grade.Id : 0;
            var questionId = question.Id;

            var questionDTO = question.MapTo<ExamCourseQuestionInfo>();
            questionDTO.QuestionId = questionId;

            //加载题目信息
            if (questionDTO.QuestionType == QuestionType.SingleChoice
                || questionDTO.QuestionType == QuestionType.MultiChoice
                || questionDTO.QuestionType == QuestionType.Judge
                || questionDTO.QuestionType == QuestionType.NewMultiChoice)
            {
                //添加选项信息
                var options = await DbContext.FreeSql.GetRepository<YssxPublicAnswerOption>().Where(a => a.TopicId == questionId && a.IsDelete == CommonConstants.IsNotDelete).OrderBy(a => a.Sort).ToListAsync($"{CommonConstants.Cache_GetPublicOptionsByTopicId}{questionId}", true, 10 * 60);
                var optionList = new List<ChoiceOption>();
                options.ForEach(a =>
                {
                    optionList.Add(new ChoiceOption()
                    {
                        Id = a.Id,
                        Name = a.Name,
                        Text = a.AnswerOption,
                        AttatchImgUrl = a.AnswerFileUrl
                    });
                });
                questionDTO.Options = optionList;
            }
            else
            {
                //复杂题型
                //1.添加附件信息
                var fileList = new List<QuestionFile>();
                var fileIds = question.TopicFileIds;
                if (!string.IsNullOrEmpty(fileIds))
                {
                    var fileIdArr = fileIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                    if (fileIdArr.Any())
                    {
                        var files = await DbContext.FreeSql.GetRepository<YssxTopicPublicFile>().Where(a => fileIdArr.Contains(a.Id) && a.IsDelete == CommonConstants.IsNotDelete).Select(a => new QuestionFile() { Name = a.Name, Url = a.Url, Sort = a.Sort }).ToListAsync($"{CommonConstants.Cache_GetPublicTopicFilesByTopicId}{questionId}", true, 10 * 60);
                        questionDTO.QuestionFile = files;
                    }
                }
                //2.添加子题目
                if (questionDTO.QuestionType == QuestionType.MainSubQuestion)
                {
                    var subQuestions = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.ParentId == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                        .OrderBy(s => s.Sort)
                        .ToListAsync($"{CommonConstants.Cache_GetSubPublicTopicByParentId}{questionId}", true, 10 * 60, true, false).Result
                        .Select(s =>
                        new ExamCourseQuestionInfo()
                        {
                            QuestionId = s.Id,
                            QuestionType = s.QuestionType,
                            QuestionContentType = s.QuestionContentType,
                            CalculationType = s.CalculationType,
                            Hint = s.Hint,
                            Content = s.Content,
                            TopicContent = s.TopicContent,
                            FullContent = s.FullContent,
                            AnswerValue = s.AnswerValue,
                            Score = s.Score,
                            Sort = s.Sort,
                            IsCopy = s.IsCopy,
                            IsCopyBigData = s.IsCopyBigData,
                            IsDisorder = s.IsDisorder
                        }).ToList();

                    var detailList = new List<ExamCourseUserGradeDetail>();
                    if (gradeId > 0)
                    {
                        detailList = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(s => s.GradeId == gradeId && s.ParentQuestionId == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                            .ToListAsync(s => new ExamCourseUserGradeDetail()
                            {
                                Answer = s.Answer,
                                AnswerCompareInfo = s.AnswerCompareInfo,
                                QuestionId = s.QuestionId
                            });
                    }

                    if (subQuestions.Any() && detailList.Any())
                    {
                        subQuestions.ForEach(s =>
                        {
                            var detail = detailList.FirstOrDefault(a => a.QuestionId == s.QuestionId);
                            if (detail != null && detail.QuestionId > 0)
                            {
                                s.StudentAnswer = detail.Answer;
                                s.AnswerCompareInfo = detail.AnswerCompareInfo;
                            }
                        });
                    }

                    var choiceQuestionIds = subQuestions.Where(a => a.QuestionType == QuestionType.SingleChoice
                    || a.QuestionType == QuestionType.MultiChoice || a.QuestionType == QuestionType.Judge || a.QuestionType == QuestionType.UndefinedTermChoice).Select(a => a.QuestionId).ToArray();

                    if (choiceQuestionIds.Any())
                    {
                        var optionAll = await DbContext.FreeSql.GetRepository<YssxPublicAnswerOption>().Where(a => choiceQuestionIds.Contains(a.TopicId) && a.IsDelete == CommonConstants.IsNotDelete).ToListAsync($"{CommonConstants.Cache_GetPublicOptionsByTopicId}{questionId}", true, 10 * 60);
                        subQuestions.ForEach(a =>
                        {
                            var options = optionAll.Where(c => c.TopicId == a.QuestionId).Select(d => new ChoiceOption()
                            {
                                Id = d.Id,
                                Name = d.Name,
                                Text = d.AnswerOption,
                                AttatchImgUrl = d.AnswerFileUrl
                            });
                            if (options.Any())
                            {
                                a.Options = options.ToList();
                            }
                        });
                    }

                    if (subQuestions.Any(s => s.QuestionType == QuestionType.AccountEntry))
                    {
                        subQuestions.ForEach(s =>
                        {
                            if (s.QuestionType == QuestionType.AccountEntry)
                            {
                                s.CertificateTopic = DbContext.FreeSql.GetRepository<YssxCertificateTopicPublic>().Select.Where(a => a.TopicId == s.QuestionId && a.IsDelete == CommonConstants.IsNotDelete)
                                .Select(a => new CertificateTopicView())
                                .First($"{CommonConstants.Cache_GetCertificateTopicPublicByTopicId}{s.QuestionId}", true, 10 * 60);
                            }
                        });
                    }

                    questionDTO.SubQuestion = subQuestions;
                }
                else if (question.QuestionType == QuestionType.AccountEntry)//分录题
                {
                    questionDTO.CertificateTopic = await DbContext.FreeSql.GetRepository<YssxCertificateTopicPublic>().Select.Where(s => s.TopicId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                        .Select(s => new CertificateTopicView())
                        .FirstAsync($"{CommonConstants.Cache_GetCertificateTopicPublicByTopicId}{questionId}", true, 10 * 60);
                }
            }

            #region 附上答题信息及答案显示

            var gradeDetail = new ExamCourseUserGradeDetail();
            if (gradeId > 0)
            {
                gradeDetail = DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.QuestionId == questionId && a.GradeId == gradeId)
                    .First(a => new ExamCourseUserGradeDetail { Status = a.Status, Answer = a.Answer, AnswerCompareInfo = a.AnswerCompareInfo,Score=a.Score,Comment=a.Comment });
            }
            //作答结束
            if (exam.CanShowAnswerBeforeEnd || (grade != null && grade.Status == StudentExamStatus.End))//&& isView
            {
                if (gradeDetail != null)
                {
                    questionDTO.AnswerCompareInfo = gradeDetail.AnswerCompareInfo;
                    questionDTO.AnswerResultStatus = gradeDetail.Status;
                }
            }
            else
            {
                questionDTO.Hint = null;
                questionDTO.FullContent = null;
                questionDTO.AnswerValue = null;
                questionDTO.AnswerCompareInfo = null;

                if (questionDTO.SubQuestion != null && questionDTO.SubQuestion.Count > 0)
                {
                    questionDTO.SubQuestion.ForEach(a =>
                    {
                        a.Hint = null;
                        a.FullContent = null;
                        a.AnswerValue = null;
                        a.AnswerCompareInfo = null;
                    });
                }

                questionDTO.AnswerResultStatus = gradeDetail == null ? AnswerResultStatus.None : AnswerResultStatus.Pending;
            }
            questionDTO.StudentAnswer = gradeDetail?.Answer;
            if (questionDTO.QuestionType == QuestionType.ShortAnswerQuestion)
            {
                questionDTO.TeacherScore = gradeDetail?.Score;
                questionDTO.Comment = gradeDetail?.Comment;
            }

            #endregion

            result.Data = questionDTO;
            result.Msg = "操作成功";
            result.Code = CommonConstants.SuccessCode;

            return result;
        }

        #endregion

        #region 提交答案

        /// <summary>
        /// 提交答案--接口方法
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <returns>作答成功信息</returns>
        public async Task<ResponseContext<QuestionResult>> SubmitAnswer(CourseQuestionAnswer model, CourseExamType examType, UserTicket userTicket)
        {
            var result = new ResponseContext<QuestionResult>(CommonConstants.BadRequest, "");
            var user = userTicket;

            #region 校验

            if (model.GradeId == 0 || model.QuestionId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "GradeId,QuestionId都不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "主作答记录不存在!";
                return result;
            }

            if (grade.Status == StudentExamStatus.End)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"练习已结束，不能提交答案！";
                return result;
            }
            var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamCourseById}{grade.ExamId}", true, 10 * 60);
            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }
            var question = await DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.Id == model.QuestionId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetPublicTopicById}{model.QuestionId}", true, 10 * 60, true);
            if (question == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "题目不存在";
                return result;
            }


            //var validateQuestion = await ValidateQuestion(exam, grade, user, question, model);

            #endregion

            switch (examType)
            {
                case CourseExamType.CourceTest:

                    return await SubmitCourseTestAnswer(model, exam, question, user);
                case CourseExamType.ClassHomeWork:
                case CourseExamType.ClassTask:
                case CourseExamType.ClassExam:
                case CourseExamType.ClassTest:
                case CourseExamType.NewCourseTask:
                    return await SubmitTaskAnswer(model, exam, question, user);
                default:

                    throw new NotImplementedException($"{examType}类型不存在");
            }
        }

        /// <summary>
        /// 提交答案--课程练习
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="question"></param>
        /// <param name="user"></param>
        /// <returns>作答成功信息</returns>
        private async Task<ResponseContext<QuestionResult>> SubmitCourseTestAnswer(CourseQuestionAnswer answer, ExamPaperCourse exam, YssxTopicPublic question, UserTicket user)
        {
            return await SubmitAnswerBase(answer, exam, question, user);
        }
        /// <summary>
        /// 提交答案--任务
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="question"></param>
        /// <param name="user"></param>
        /// <returns>作答成功信息</returns>
        private async Task<ResponseContext<QuestionResult>> SubmitTaskAnswer(CourseQuestionAnswer answer, ExamPaperCourse exam, YssxTopicPublic question, UserTicket user)
        {
            if (exam.EndTime <= DateTime.Now || exam.Status == ExamStatus.End)
            {
                var result = new ResponseContext<QuestionResult>(CommonConstants.BadRequest, "");
                result.Code = CommonConstants.BadRequest;
                result.Msg = "考试已结束!";
                return result;
            }
            return await SubmitAnswerBase(answer, exam, question, user);
        }
        /// <summary>
        /// 提交答案基础函数
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <param name="exam"></param>
        /// <param name="question"></param>
        /// <param name="user"></param>
        /// <returns>作答成功信息</returns>
        private async Task<ResponseContext<QuestionResult>> SubmitAnswerBase(CourseQuestionAnswer answer, ExamPaperCourse exam, YssxTopicPublic question, UserTicket user)
        {
            var result = new ResponseContext<QuestionResult>() { Data = new QuestionResult() { QuestionId = question.Id } };
            try
            {
                List<YssxExamCourseCertificateDataRecord> AddAccountEntryList(CourseQuestionAnswer ans, long detailId)
                {
                    var entryList = new List<YssxExamCourseCertificateDataRecord>();
                    if (ans.AccountEntryList != null && ans.AccountEntryList.Any())
                    {
                        entryList = ans.AccountEntryList.Select(a => a.MapTo<YssxExamCourseCertificateDataRecord>()).ToList();
                        entryList.ForEach(a =>
                        {
                            a.Id = IdWorker.NextId();
                            a.ExamId = exam.Id;
                            a.GradeId = answer.GradeId;
                            a.GradeDetailId = detailId;
                            a.QuestionId = ans.QuestionId;
                            a.CourseId = exam.CourseId;
                            a.TenantId = exam.TenantId;

                            a.BorrowAmount = a.BorrowAmount;
                            a.CreditorAmount = a.CreditorAmount;
                        });
                    }

                    return entryList;
                }

                #region 提交答案

                var accountEntryList = new List<YssxExamCourseCertificateDataRecord>();

                result = RedisLock.SkipLock(user.Id.ToString(), () =>
                {
                    var lockResult = new ResponseContext<QuestionResult>() { Data = new QuestionResult() };

                    try
                    {
                        var gradeDetailList = DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.GradeId == answer.GradeId && a.ParentQuestionId == answer.QuestionId).Master().ToList(a => new { a.Id, a.ExamId, a.QuestionId, a.Answer });
                        if (gradeDetailList == null || gradeDetailList.Count == 0)
                        {
                            #region 插入作答信息

                            var detailInsertList = new List<ExamCourseUserGradeDetail>();
                            var nowTime = DateTime.Now;
                            var mainDetail = new ExamCourseUserGradeDetail()
                            {
                                Id = IdWorker.NextId(),
                                GradeId = answer.GradeId,
                                ExamId = exam.Id,
                                TenantId = user.TenantId,
                                QuestionId = answer.QuestionId,
                                ParentQuestionId = answer.QuestionId,
                                UserId = user.Id,
                                StudentId = CommonConstants.StudentId,
                                Answer = answer.AnswerValue,
                                CreateTime = nowTime
                            };
                            answer.GradeDetailId = mainDetail.Id;

                            if (question.QuestionType == QuestionType.MainSubQuestion)//多题型 
                            {
                                var multiQuestions = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.ParentId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                                    .OrderBy(s => s.Sort)
                                    .ToListAsync($"{CommonConstants.Cache_GetSubPublicTopicByParentId}{question.Id}", true, 10 * 60, true).Result
                                    .Select(s =>
                                    new SubQuestionDto()
                                    {
                                        Id = s.Id,
                                        QuestionType = s.QuestionType
                                    });

                                foreach (var item in answer.MultiQuestionAnswers)
                                {
                                    var subQuestion = multiQuestions.FirstOrDefault(s => s.Id == item.QuestionId);
                                    var subDetail = new ExamCourseUserGradeDetail()
                                    {
                                        Id = IdWorker.NextId(),
                                        TenantId = user.TenantId,
                                        ExamId = exam.Id,
                                        GradeId = answer.GradeId,
                                        QuestionId = item.QuestionId,
                                        UserId = user.Id,
                                        StudentId = CommonConstants.StudentId,
                                        Answer = item.AnswerValue,
                                        ParentQuestionId = answer.QuestionId,
                                        CreateTime = nowTime
                                    };

                                    item.GradeDetailId = subDetail.Id;

                                    #region 分录题特殊处理

                                    if (subQuestion.QuestionType == QuestionType.AccountEntry)//分录题，插入凭证
                                    {
                                        //subDetail.AccountEntryAuditStatus = item.AccountEntryAuditStatus;
                                        subDetail.AccountEntryStatus = item.AccountEntryStatus;
                                        accountEntryList.AddRange(AddAccountEntryList(item, subDetail.Id));
                                    }

                                    #endregion

                                    detailInsertList.Add(subDetail);

                                }
                            }
                            else if (question.QuestionType == QuestionType.AccountEntry)//分录题特殊处理
                            {
                                //mainDetail.AccountEntryAuditStatus = answer.AccountEntryAuditStatus;
                                mainDetail.AccountEntryStatus = answer.AccountEntryStatus;
                                accountEntryList = AddAccountEntryList(answer, mainDetail.Id);
                            }

                            detailInsertList.Add(mainDetail);

                            DbContext.FreeSql.Transaction(() =>
                            {
                                detailInsertList.ForEach(s =>
                                {
                                    DbContext.FreeSql.Delete<ExamStudentGradeDetail>().Where(b => b.GradeId == s.GradeId && b.ParentQuestionId == s.ParentQuestionId && b.QuestionId == s.QuestionId).ExecuteAffrows();
                                });
                                DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Insert(detailInsertList);
                                if (accountEntryList.Any())
                                {
                                    DbContext.FreeSql.GetRepository<YssxExamCourseCertificateDataRecord>().Insert(accountEntryList);
                                }

                                //if (question.QuestionType == QuestionType.SettleAccounts)//结账题特殊处理
                                //{
                                //    DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().UpdateDiy
                                //    .UpdateColumns(a => new { a.UpdateTime, a.IsSettled })
                                //    .Set(a => a.UpdateTime, DateTime.Now)
                                //    .Set(a => a.IsSettled, true).Where(a => a.Id == answer.GradeId).ExecuteAffrows();
                                //}
                            });

                            #endregion
                        }
                        else
                        {
                            #region 更新作答信息

                            foreach (var detail in gradeDetailList)
                            {
                                if (detail.QuestionId == answer.QuestionId)
                                {
                                    answer.GradeDetailId = detail.Id;
                                    if (question.QuestionType == QuestionType.MainSubQuestion)//多题型
                                    {
                                        var questionIds = gradeDetailList.Select(s => s.QuestionId).Where(s => s != answer.QuestionId);

                                        foreach (var item in gradeDetailList)
                                        {
                                            if (item.QuestionId == question.Id)
                                            {
                                                continue;
                                            }

                                            var dtoDetail = answer.MultiQuestionAnswers.FirstOrDefault(a => a.QuestionId == item.QuestionId);
                                            if (dtoDetail == null || dtoDetail.QuestionId == 0)
                                            {
                                                continue;
                                            }
                                            if (dtoDetail.IsIgnore)
                                            {
                                                dtoDetail.AnswerValue = item.Answer;
                                            }
                                            dtoDetail.GradeDetailId = item.Id;

                                            #region 分录题特殊处理

                                            if (dtoDetail.QuestionType == QuestionType.AccountEntry)//分录题，插入凭证
                                            {
                                                accountEntryList.AddRange(AddAccountEntryList(dtoDetail, item.Id));
                                            }

                                            #endregion
                                        }

                                        //先删除之前录入凭证数据
                                        if (accountEntryList.Any())
                                        {
                                            DbContext.FreeSql.Transaction(() =>
                                            {
                                                DbContext.FreeSql.GetRepository<YssxExamCourseCertificateDataRecord>().Delete(a => a.GradeId == answer.GradeId && questionIds.Contains(a.QuestionId));
                                                DbContext.FreeSql.GetRepository<YssxExamCourseCertificateDataRecord>().Insert(accountEntryList);
                                            });
                                        }
                                    }
                                    else
                                    {
                                        #region 结账题特殊处理（有答题记录时取消结账--即删除答题记录）
                                        //TODO 暂不确定是否有结账题
                                        //if (question.QuestionType == QuestionType.SettleAccounts)
                                        //{
                                        //    //删除答案
                                        //    DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Delete(s => s.Id == detail.Id);
                                        //    DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().UpdateDiy
                                        //    .UpdateColumns(a => new { a.UpdateTime, a.IsSettled })
                                        //    .Set(a => a.UpdateTime, DateTime.Now)
                                        //    .Set(a => a.IsSettled, false).Where(a => a.Id == answer.GradeId).ExecuteAffrows();

                                        //    //zhcc  20190824
                                        //    var questionResult = new QuestionResult()
                                        //    {
                                        //        No = question.Sort,
                                        //        QuestionId = question.Id,
                                        //        AnswerResult = AnswerResultStatus.None
                                        //    };
                                        //    lockResult.Data = questionResult;
                                        //    return lockResult;
                                        //}

                                        #endregion

                                        #region 分录题：录入凭证存储更新（用于账簿查询）

                                        if (question.QuestionType == QuestionType.AccountEntry)
                                        {
                                            accountEntryList = AddAccountEntryList(answer, detail.Id);
                                        }

                                        #endregion

                                        if (accountEntryList.Any())
                                        {
                                            DbContext.FreeSql.Transaction(() =>
                                            {
                                                //先删除之前录入凭证数据
                                                DbContext.FreeSql.GetRepository<YssxExamCourseCertificateDataRecord>().Delete(a => a.GradeId == answer.GradeId && a.QuestionId == answer.QuestionId);
                                                DbContext.FreeSql.GetRepository<YssxExamCourseCertificateDataRecord>().Insert(accountEntryList);
                                            });
                                        }
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        lockResult.Code = CommonConstants.ErrorCode;
                        lockResult.Msg = $"提交答案失败";
                        CommonLogger.Error($"答题异常", ex);
                    }

                    return lockResult;

                }, timeoutSeconds: 5);

                if (result == null || result.Code != CommonConstants.SuccessCode)
                {
                    return result;
                }

                if (result.Data.QuestionId > 0)//取消结账，直接返回
                {
                    goto End;
                }

                #endregion

                //TODO:计算题目分数
                var task = Task.Run(() =>
                {
                    var compareRst = CompareTopicAnswer(question, answer);

                    return compareRst;
                }).ConfigureAwait(false);
                if (exam.CanShowAnswerBeforeEnd)
                {
                    result.Data = await task;
                }

                //if (exam.ExamType != ExamType.PracticeTest && exam.CompetitionType == CompetitionType.TeamCompetition)
                //{
                //    if (question.QuestionType == QuestionType.MainSubQuestion)
                //    {
                //        result.Data.AccountEntryStatusDic = answer.MultiQuestionAnswers.Where(s => s.QuestionType == QuestionType.AccountEntry).Select(s => new Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>(s.QuestionId, s.AccountEntryStatus, s.AccountEntryAuditStatus, s.Sort)).ToList();
                //    }
                //    else if (question.QuestionType == QuestionType.AccountEntry)
                //    {
                //        result.Data.AccountEntryStatusDic = new List<Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>>() { new Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>(answer.QuestionId, answer.AccountEntryStatus, answer.AccountEntryAuditStatus, answer.Sort) };
                //    }
                //}

                if (result.Data.AnswerResult == AnswerResultStatus.Right)
                {
                    var data = new { QuestionId = answer.QuestionId, UserId = user.Id, GradeId = answer.GradeId };
                    await RedisHelper.PublishAsync("SubmitAnswer", JsonHelper.SerializeObject(data));
                }

                result.Msg = "成功提交答案";

            }
            catch (Exception ex)
            {
                result.Msg = "系统异常";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"答题异常:GradeId-{answer.GradeId},QuestionId-{answer.QuestionId},UserId:{user.Id}";
                CommonLogger.Error(msg + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, ex);
            }

        End:
            return result;
        }

        #endregion
        #region 题目算分相关方法

        /// <summary>
        /// 比较答案
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="answer"></param>
        /// <param name="caseModel"></param>
        /// <returns></returns>
        private QuestionResult CompareTopicAnswer(YssxTopicPublic topic, CourseQuestionAnswer answer)
        {
            var examResultDto = new QuestionResult { AnswerResult = AnswerResultStatus.Error, No = answer.Sort, QuestionId = answer.QuestionId };
            if (topic.QuestionType != QuestionType.MainSubQuestion && string.IsNullOrEmpty(answer.AnswerValue))
            {
                UpdateScores(answer, examResultDto);
                return examResultDto;
            }
            if (topic.QuestionType == QuestionType.SettleAccounts)//结账题特殊处理，直接得分
            {
                examResultDto.AnswerResult = AnswerResultStatus.Right;
                examResultDto.AnswerScore = topic.Score;
                UpdateScores(answer, examResultDto);
                return examResultDto;
            }
            if (topic.QuestionType == QuestionType.ShortAnswerQuestion)//案例分析题
            {
                examResultDto.AnswerResult = AnswerResultStatus.Pending;
                examResultDto.AnswerScore = 0;
                UpdateScores(answer, examResultDto);
                return examResultDto;
            }
            var score = topic.Score;
            var standardAnswerVal = HttpUtility.UrlDecode(topic.AnswerValue);
            var answerTheValue = HttpUtility.UrlDecode(answer.AnswerValue);
            AnswerResultStatus status;
            decimal answerScore = 0;
            switch (topic.QuestionType)
            {
                case QuestionType.SingleChoice:
                case QuestionType.Judge:
                    status = answerTheValue == standardAnswerVal ? AnswerResultStatus.Right : AnswerResultStatus.Error;
                    answerScore = status == AnswerResultStatus.Right ? score : 0m;
                    examResultDto.AnswerResult = status;
                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.MultiChoice:
                    var standardVal = standardAnswerVal.ToLower().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    var answerVal = answerTheValue.ToLower().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    status = CommonMethod.CompareArrayElement(standardVal, answerVal) ? AnswerResultStatus.Right : AnswerResultStatus.Error;

                    answerScore = status == AnswerResultStatus.Right ? score : 0m;
                    examResultDto.AnswerResult = status;
                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.UndefinedTermChoice:
                case QuestionType.NewMultiChoice:
                    var standardVal2 = standardAnswerVal.ToLower().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    var answerVal2 = answerTheValue.ToLower().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    int count = CommonMethod.CompareArrayElement2(standardVal2, answerVal2);
                    answerScore = count == standardVal2.Length ? score : decimal.Round((score / standardVal2.Length * count), 3, MidpointRounding.AwayFromZero);
                    if (count == standardVal2.Length) status = AnswerResultStatus.Right;
                    else if (count == 0) status = AnswerResultStatus.Error;
                    else status = AnswerResultStatus.PartRight;

                    examResultDto.AnswerResult = status;
                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.AccountEntry:
                case QuestionType.GridFillBank:
                case QuestionType.FillBlank:
                case QuestionType.FillGrid:
                case QuestionType.FillGraphGrid:
                case QuestionType.FinancialStatements:
                    var standardJObj = JsonHelper.DeserializeObject<Answer>(standardAnswerVal);
                    var answerJObj = JsonHelper.DeserializeObject<Answer>(answerTheValue);
                    var answerCountInfo = AnswerCompareHelperV2.Compare(standardJObj, answerJObj, 500, 600);

                    //var caseSet = new YssxCase();
                    //if (topic.QuestionType == QuestionType.AccountEntry)//分录题获取非完整性得分及分录题占比设置
                    //{
                    //    //caseSet = DbContext.FreeSql.GetRepository<YssxCase>().Where(s => s.Id == topic.CaseId).FirstAsync($"{CommonConstants.Cache_GetCaseById}{topic.CaseId}", true, 10 * 60, true).Result;
                    //}
                    //TODO 分录题分数，之前是从案例里面取，现在没有案例，是否直接在题目中 ?
                    var scale = 0M;//topic.QuestionType == QuestionType.AccountEntry ? caseSet.AccountEntryScale : 0M;
                    switch (topic.CalculationType)
                    {
                        case CalculationType.None:
                        case CalculationType.AvgCellCount:
                            var aroundScored = 0m;
                            int aroundVaildCount = answerCountInfo.HeaderCountInfo.ValidCount + answerCountInfo.FooterCountInfo.ValidCount;
                            int aroundRightCount = answerCountInfo.HeaderCountInfo.RightCount + answerCountInfo.FooterCountInfo.RightCount;
                            if (aroundVaildCount != 0)//只有分录题才有外围
                            {
                                var aroundPercent = 0m;
                                //if (caseSet.SetType == AccountEntryCalculationType.SetAll)//案例设置为“统一设置权重”
                                //{
                                //    aroundPercent = Math.Round((decimal)caseSet.OutVal / (decimal)(caseSet.InVal + caseSet.OutVal), 4, MidpointRounding.AwayFromZero);
                                //}
                                //else//案例设置为“单独设置权重”
                                //{
                                var certificateTopic = DbContext.FreeSql.GetRepository<YssxCertificateTopicPublic>().Where(s => s.TopicId == topic.Id && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetCertificateTopicPublicByTopicIdAll}{topic.Id}", true, 10 * 60).Result;
                                aroundPercent = Math.Round((decimal)certificateTopic.OutVal / (decimal)(certificateTopic.InVal + certificateTopic.OutVal), 4, MidpointRounding.AwayFromZero);
                                //}
                                var aroundTotalScore = Math.Round(score * aroundPercent, 4, MidpointRounding.AwayFromZero);
                                score = score - aroundTotalScore;
                                aroundScored = aroundTotalScore / aroundVaildCount * aroundRightCount;
                            }
                            var otherValidCount = answerCountInfo.TotalCountInfo.ValidCount - aroundVaildCount;
                            var otherRightCount = answerCountInfo.TotalCountInfo.RightCount - aroundRightCount;
                            if (otherValidCount != 0)
                                answerScore = score / otherValidCount * otherRightCount;
                            answerScore = Math.Round(answerScore + aroundScored, 4, MidpointRounding.AwayFromZero);
                            break;
                        case CalculationType.FreeCalculation:
                            answerScore = answerCountInfo.TotalCountInfo.Scored;
                            break;
                        case CalculationType.CalculatedRow:
                            answerScore = CalcRowsScored(answerJObj.BodyCtrlRows, score);
                            break;
                        case CalculationType.CalculatedColumn:
                            answerScore = CalcRowsScored(answerJObj.BodyCtrlColumns, score);
                            break;
                    }
                    answerScore = GetNonholonomicScore(answerCountInfo, answerScore, scale);
                    status = answerCountInfo.TotalCountInfo.ValidCount == answerCountInfo.TotalCountInfo.RightCount ? AnswerResultStatus.Right : (answerCountInfo.TotalCountInfo.RightCount == 0 ? AnswerResultStatus.Error : AnswerResultStatus.PartRight);
                    examResultDto.AnswerCountInfo = answerCountInfo;
                    examResultDto.AnswerResultJson = HttpUtility.UrlEncode(JsonHelper.SerializeObject(answerJObj));
                    examResultDto.AnswerResult = status;
                    //if (examResultDto.AnswerResult == AnswerResultStatus.Right)
                    //{
                    //    examResultDto.AnswerScore = score;
                    //}
                    //else
                    //{
                    //    examResultDto.AnswerScore = answerScore;
                    //}

                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.MainSubQuestion:
                    examResultDto = MainSubQuestionCompare(answer, examResultDto);

                    break;

            }
            UpdateScores(answer, examResultDto);
            return examResultDto;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="score"></param>
        /// <returns></returns>
        private static decimal CalcRowsScored(List<AnswerRow> rows, decimal score)
        {
            if (rows == null)
                return 0;
            var columnTotalCount = (decimal)rows.Count;
            //2019-9-7
            //var rigthColumnCount = rows.Count(m => m.Items.All(i => i.IsRight && i.IsValid));
            var rigthColumnCount = (decimal)rows.Count(m => m.Items.All(i => i.IsRight || !i.IsValid) && m.Items.Any(i => i.IsRight));
            return Math.Round(rigthColumnCount / columnTotalCount * score, 4, MidpointRounding.AwayFromZero);
        }
        /// <summary>
        /// 获取非完整得分
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="scored"></param>
        /// <param name="accountEntryScale"></param>
        /// <param name="fillGridScale"></param>
        /// <returns></returns>
        private static decimal GetNonholonomicScore(AnswerCountInfo answerCountInfo, decimal scored, decimal scale)
        {
            if (scale == 0)
                return scored;
            if (answerCountInfo.TotalCountInfo.ValidCount != answerCountInfo.TotalCountInfo.RightCount)//非完整性得分判断
            {
                return Math.Round(scored * scale / 100m, 4, MidpointRounding.AwayFromZero);
            }
            return Math.Round(scored, 4, MidpointRounding.AwayFromZero);
        }
        /// <summary>
        /// 多题型答案比较
        /// </summary>
        /// <param name="examResultDto"></param>
        private QuestionResult MainSubQuestionCompare(CourseQuestionAnswer answer, QuestionResult examResultDto)
        {
            var allItemCount = 0;
            var rightItemCount = 0;
            var answerScore = 0m;
            examResultDto.MultiQuestionResult = new List<QuestionResult>();
            if (answer.MultiQuestionAnswers == null || answer.MultiQuestionAnswers.Count == 0)
            {
                examResultDto.AnswerResult = AnswerResultStatus.Error;
                return examResultDto;
            }

            var multiTopic = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.ParentId == answer.QuestionId && s.IsDelete == CommonConstants.IsNotDelete)
                        .OrderBy(s => s.Sort)
                        .ToListAsync($"{CommonConstants.Cache_GetSubPublicTopicByParentId}{answer.QuestionId}", true, 10 * 60, true).Result;

            foreach (var multiItem in multiTopic)
            {
                var answerItem = answer.MultiQuestionAnswers.FirstOrDefault(m => m.QuestionId == multiItem.Id);
                if (answerItem == null)
                    continue;
                var compareRst = CompareTopicAnswer(multiItem, answerItem);
                if (compareRst.AnswerCountInfo == null)
                {
                    if (compareRst.AnswerResult == AnswerResultStatus.Right)
                        rightItemCount++;
                    allItemCount++;
                }
                else
                {
                    allItemCount += compareRst.AnswerCountInfo.TotalCountInfo.ValidCount;
                    rightItemCount += compareRst.AnswerCountInfo.TotalCountInfo.RightCount;
                }
                examResultDto.MultiQuestionResult.Add(compareRst);
            }
            if (allItemCount == 0)
            {
                examResultDto.AnswerResult = AnswerResultStatus.Error;
                return examResultDto;
            }
            answerScore = examResultDto.MultiQuestionResult.Sum(s => s.AnswerScore);
            examResultDto.AnswerResult = allItemCount == rightItemCount ? AnswerResultStatus.Right : (rightItemCount == 0 ? AnswerResultStatus.Error : AnswerResultStatus.PartRight);
            examResultDto.AnswerScore = answerScore;
            return examResultDto;
        }


        private void UpdateScores(CourseQuestionAnswer answer, QuestionResult examResultDto)
        {
            if (answer.IsIgnore)
            {
                return;
            }

            var gradeDetailId = answer.GradeDetailId;
            if (gradeDetailId > 0)
            {
                var gradeDetail = new ExamCourseUserGradeDetail() { Id = gradeDetailId };
                gradeDetail.Answer = answer.AnswerValue;
                //gradeDetail.AccountEntryAuditStatus = answer.AccountEntryAuditStatus;
                gradeDetail.AccountEntryStatus = answer.AccountEntryStatus;
                gradeDetail.UpdateTime = DateTime.Now;
                gradeDetail.Status = examResultDto.AnswerResult;
                gradeDetail.Score = examResultDto.AnswerScore;
                gradeDetail.AnswerCompareInfo = examResultDto.AnswerResultJson;

                DataMergeHelper.PushData("UpdateScores-CourseGradeDetail", gradeDetail, (arr) =>
                {
                    var details = arr.Cast<ExamCourseUserGradeDetail>().ToList();
                    DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().UpdateDiy.SetSource(details)
                  .UpdateColumns(a => new { a.Status, a.Score, a.AnswerCompareInfo, a.Answer, a.AccountEntryStatus, a.UpdateTime }).ExecuteAffrows();
                });
            }
        }

        #endregion
        #region 交卷
        /// <summary>
        /// 获取未答题数
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<int>> GetBlankCount(long gradeId)
        {
            var result = new ResponseContext<int>();
            var grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>()
                .Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamCourseById}{grade.ExamId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }
            var totalQuestionCount = exam.TotalQuestionCount;

            //Done:计算考卷分数，返回考试成绩信息
            var answerCount = DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.GradeId == grade.Id && a.QuestionId == a.ParentQuestionId)
                .ToList(a => new { a.QuestionId, a.Status, a.Score });
            int blankCount = totalQuestionCount - answerCount.Count;
            result.Data = blankCount;
            result.Code = CommonConstants.SuccessCode;
            return result;
        }
        /// <summary>
        /// 提交考卷---接口方法 
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<GradeInfo>> SubmitExam(long gradeId, UserTicket user)
        {

            var result = new ResponseContext<GradeInfo>();

            #region 校验

            if (gradeId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = $"GradeId都不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到答题记录";
                return result;
            }

            if (grade.Status == StudentExamStatus.End)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"您已经提交过考卷了！";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamCourseById}{grade.ExamId}", true, 10 * 60);
            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }
            #endregion

            switch (exam.ExamType)
            {
                case CourseExamType.CourceTest:

                    return await SubmitCourseTestExam(exam, grade, user);
                case CourseExamType.ClassExam:

                    return await SubmitClassExam(exam, grade, user);
                case CourseExamType.ClassHomeWork:

                    return await SubmitClassHomeWorkExam(exam, grade, user);
                case CourseExamType.ClassTask:
                case CourseExamType.NewCourseTask:

                    return await SubmitClassTaskExam(exam, grade, user);

                case CourseExamType.ClassTest:

                    return await SubmitClassTestExam(exam, grade, user);

                default:

                    throw new NotImplementedException($"{exam.ExamType}类型的考试还没有实现，稍等稍等");
            }
        }
        /// <summary>
        /// 提交考卷--课程练习
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<ResponseContext<GradeInfo>> SubmitCourseTestExam(ExamPaperCourse exam, ExamCourseUserGrade grade, UserTicket user)
        {
            return await SubmitExamBase(exam, grade, user);
        }
        /// <summary>
        /// 提交考卷--课程练习
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<ResponseContext<GradeInfo>> SubmitClassExam(ExamPaperCourse exam, ExamCourseUserGrade grade, UserTicket user)
        {
            return await SubmitExamBase(exam, grade, user);
        }
        /// <summary>
        /// 提交考卷--课程练习
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<ResponseContext<GradeInfo>> SubmitClassHomeWorkExam(ExamPaperCourse exam, ExamCourseUserGrade grade, UserTicket user)
        {
            return await SubmitExamBase(exam, grade, user);
        }
        /// <summary>
        /// 提交考卷--课程练习
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<ResponseContext<GradeInfo>> SubmitClassTaskExam(ExamPaperCourse exam, ExamCourseUserGrade grade, UserTicket user)
        {
            return await SubmitExamBase(exam, grade, user);
        }
        /// <summary>
        /// 提交考卷--课程练习
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<ResponseContext<GradeInfo>> SubmitClassTestExam(ExamPaperCourse exam, ExamCourseUserGrade grade, UserTicket user)
        {
            return await SubmitExamBase(exam, grade, user);
        }
        /// <summary>
        /// 提交考卷基础函数
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<GradeInfo>> SubmitExamBase(ExamPaperCourse exam, ExamCourseUserGrade grade, UserTicket user, bool isAutoSubmit = false)
        {
            var result = new ResponseContext<GradeInfo>();
            try
            {

                SubmitExamGrade(exam, grade, isAutoSubmit);

                result.Data = new GradeInfo()
                {
                    GradeId = grade.Id,
                    GradeScore = grade.Score,
                    UsedSeconds = (long)grade.UsedSeconds,
                    CorrectCount = grade.CorrectCount,
                    ErrorCount = grade.ErrorCount,
                    PartRightCount = grade.PartRightCount,
                    BlankCount = grade.BlankCount,
                    IsPass = grade.Score > exam.PassScore,
                    StudentExamStatus = grade.Status,
                };
            }
            catch (Exception ex)
            {
                result.Msg = "提交考卷异常，请联系管理员！";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"提交考卷异常:gradeId-{grade.Id},UserId:{user.Id}";
                CommonLogger.Error(msg, ex);
            }

            if (result.Code == CommonConstants.SuccessCode)
                result.Msg = "提交成功";
            return result;
        }
        /// <summary>
        /// 提交考试
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="isAutoSubmit">是否自动提交</param>
        /// <param name="isBatchSubmit">是否批量提交</param>
        private void SubmitExamGrade(ExamPaperCourse exam, ExamCourseUserGrade grade, bool isAutoSubmit = false, bool isBatchSubmit = false)
        {
            grade.Status = StudentExamStatus.End;
            grade.IsManualSubmit = !isAutoSubmit;
            grade.UpdateTime = DateTime.Now;
            grade.SubmitTime = DateTime.Now;

            switch (exam.ExamType)
            {
                case CourseExamType.CourceTest:
                    grade.UsedSeconds = (grade.SubmitTime.Value - grade.CreateTime).TotalSeconds;
                    break;
                case CourseExamType.ClassExam:
                case CourseExamType.ClassHomeWork:
                case CourseExamType.ClassTask:
                case CourseExamType.ClassTest:
                case CourseExamType.NewCourseTask:
                    grade.UsedSeconds = (grade.SubmitTime.Value - grade.RecordTime).TotalSeconds;
                    break;
            }

            var totalQuestionCount = exam.TotalQuestionCount;
            if (exam.ExamType == CourseExamType.CourceTest)
                totalQuestionCount = (int)DbContext.FreeSql.GetRepository<YssxSectionTopic>()
                    .Where(s => s.SectionId == exam.SectionId && s.IsDelete == CommonConstants.IsNotDelete).Count();
            //Done:计算考卷分数，返回考试成绩信息
            var answerCount = DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.GradeId == grade.Id && a.QuestionId == a.ParentQuestionId)
               .Master().ToList(a => new { a.QuestionId, a.Status, a.Score });
            grade.CorrectCount = answerCount.Count(a => a.Status == AnswerResultStatus.Right);
            grade.ErrorCount = answerCount.Count(a => a.Status == AnswerResultStatus.Error);
            grade.PartRightCount = answerCount.Count(a => a.Status == AnswerResultStatus.PartRight);
            grade.BlankCount = totalQuestionCount - answerCount.Count;

            if (grade.BlankCount <= 0)
                grade.BlankCount = 0;
            grade.Score = answerCount.Sum(a => a.Score);

            if (!isBatchSubmit)//非批量提交
            {
                DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Update(grade);
            }

        }
        #endregion
        #region 私有方法
        /// <summary>
        /// 获取课程练习试卷信息（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        private async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourceTestExamPaperBasicInfo(ExamPaperCourse exam, long gradeId, UserTicket user)
        {
            var result = new ResponseContext<ExamPaperCourseBasicInfo>();

            var grade = new ExamCourseUserGrade();
            if (gradeId > 0)
            {
                grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(s => s.Id == gradeId && s.UserId == user.Id&& s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

                if (grade == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"答题记录不存在 ";
                    return result;
                }

                if (grade.Status == StudentExamStatus.End)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = $"本次测试已经结束了！";
                    return result;
                }
            }
            else
            {
                //加用户锁
                var gradeResult = RedisLock.WaitLock(user.Id.ToString(), () =>
                {
                    return InsertGrade(exam, user);
                });

                if (gradeResult.Code == CommonConstants.SuccessCode)
                {
                    grade = gradeResult.Data;
                }
                else
                {
                    result.Code = gradeResult.Code;
                    result.Msg = gradeResult.Msg;
                    return result;
                }
            }

            return await GetExamPaperBasicInfoBase(exam, grade);
        }
        /// <summary>
        /// 获取任务试卷信息（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        //private async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetTaskExamPaperBasicInfo(ExamPaperCourse exam, long gradeId, UserTicket user)
        //{
        //    var result = new ResponseContext<ExamPaperCourseBasicInfo>();

        //    var grade = new ExamCourseUserGrade();
        //    grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(s => s.Id == gradeId && s.UserId == user.Id).FirstAsync();

        //    if (grade == null)
        //    {
        //        result.Code = CommonConstants.NotFund;
        //        result.Msg = $"答题记录不存在 ";
        //        return result;
        //    }
        //    else
        //    {
        //        if (grade.IsFirstTime)
        //        {
        //            grade.IsFirstTime = false;
        //            grade.RecordTime = DateTime.Now;
        //            await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().UpdateDiy.SetSource(grade)
        //                .UpdateColumns(a => new { a.IsFirstTime, a.RecordTime }).ExecuteAffrowsAsync();
        //        }
        //    }

        //    if (grade.Status == StudentExamStatus.End)
        //    {
        //        result.Code = CommonConstants.ErrorCode;
        //        result.Msg = $"本次考试任务已经结束了！";
        //        return result;
        //    }
        //    return await GetExamPaperBasicInfoBase(exam, grade);
        //}
        /// <summary>
        /// 获取试卷信息基础函数（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        private async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetExamPaperBasicInfoBase(ExamPaperCourse exam, ExamCourseUserGrade grade)
        {
            var result = new ResponseContext<ExamPaperCourseBasicInfo>();

            var gradeId = grade.Id;

            #region 考试基本信息

            result.Data = new ExamPaperCourseBasicInfo()
            {
                ExamId = exam.Id,
                TotalMinutes = exam.TotalMinutes,
                ExamScore = exam.TotalScore,
                PassScore = exam.PassScore,
                StudentExamStatus = grade.Status
            };

            if (grade.Status != StudentExamStatus.End)
            {
                if (exam.ExamType == CourseExamType.CourceTest)
                    result.Data.QuestionInfoList = await GetCourseExamQuestions(exam, grade);
                else if (exam.ExamType == CourseExamType.ClassTest)
                    result.Data.QuestionInfoList = await GetCourseExamClassTestQuestions(exam, grade);
                else if (exam.ExamType==CourseExamType.NewCourseTask)
                    result.Data.QuestionInfoList = await GetNewTaskExamQuestions(exam, grade);
                else
                    result.Data.QuestionInfoList = await GetTaskExamQuestions(exam, grade);
                if (exam.ExamType == CourseExamType.CourceTest || exam.ExamType == CourseExamType.ClassTest)
                {
                    result.Data.UsedSeconds = (long)(DateTime.Now - grade.CreateTime).TotalSeconds;
                }
                else
                {
                    result.Data.UsedSeconds = (long)(DateTime.Now - grade.RecordTime).TotalSeconds;
                    result.Data.LeftSeconds = (long)GetLeftSeconds(grade.LeftSeconds, grade.RecordTime);
                    //result.Data.UsedSeconds = (long)grade.LeftSeconds - result.Data.LeftSeconds;
                }

                result.Data.GradeId = gradeId;
            }

            #endregion

            #region 考试结束，显示考试详情

            if (grade.Status == StudentExamStatus.End)
            {
                result.Msg = "考试已结束";
                result.Data.UsedSeconds = (long)grade.UsedSeconds;
                result.Data.UserGradeInfo = new UserGradeInfo()
                {
                    GradeId = grade.Id,
                    Score = grade.Score,
                    CorrectCount = grade.CorrectCount,
                    ErrorCount = grade.ErrorCount,
                    PartRightCount = grade.PartRightCount,
                    BlankCount = grade.BlankCount,
                    IsPass = grade.Score > exam.PassScore,
                    Status = grade.Status,
                };
            }

            #endregion

            result.Msg = "操作成功";
            return result;
        }
        /// <summary>
        /// 新增作答记录
        /// </summary>
        /// <param name="exam">考试信息</param>
        /// <param name="tuples">(userId,tenantId,groupId)</userId></param>
        /// <returns></returns>
        private ResponseContext<ExamCourseUserGrade> InsertGrade(ExamPaperCourse exam, UserTicket user)
        {
            var result = new ResponseContext<ExamCourseUserGrade>() { Code = CommonConstants.SuccessCode };

            try
            {
                //插入学生考试作答主表
                var gradeId = IdWorker.NextId();
                var grade = new ExamCourseUserGrade()
                {
                    Id = gradeId,
                    ExamId = exam.Id,
                    CourseId = exam.CourseId,
                    SectionId = exam.SectionId,
                    TaskId = exam.TaskId,
                    ExamPaperScore = exam.TotalScore,
                    LeftSeconds = exam.TotalMinutes * 60,
                    //RecordTime = DateTime.Now,
                    UserId = user.Id,
                    TenantId = user.TenantId,
                    //StudentId = CommonConstants.StudentId,
                    Status = StudentExamStatus.Started
                };

                var lastGrade = new ExamCourseUserLastGrade();
                switch (exam.ExamType)
                {
                    case CourseExamType.CourceTest:
                    case CourseExamType.ClassTest:
                    case CourseExamType.ClassTask:
                    case CourseExamType.NewCourseTask:
                        var lastGradeInfo = DbContext.FreeSql.GetRepository<ExamCourseUserLastGrade>().Select.Where(s => s.ExamId == exam.Id && s.UserId == user.Id)
                            .First(s => new { s.Id, s.UserId });

                        var lastGradeId = lastGradeInfo?.Id ?? 0;

                        if (lastGradeId > 0)
                        {
                            lastGrade.Id = lastGradeId;
                            lastGrade.GradeId = gradeId;
                            lastGrade.UpdateTime = DateTime.Now;
                        }
                        else
                        {
                            lastGrade.GradeId = gradeId;
                            lastGrade.ExamId = exam.Id;
                            lastGrade.CourseId = exam.CourseId;
                            lastGrade.SectionId = exam.SectionId;
                            lastGrade.TaskId = exam.TaskId;
                            lastGrade.UserId = user.Id;
                            lastGrade.TenantId = user.TenantId;
                        }

                        break;
                    default:
                        throw new NotImplementedException($"还没有实现 {exam.ExamType}考试类型,稍等稍等");
                }

                DbContext.FreeSql.Transaction(() =>
                {
                    DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Insert(grade);
                    if (lastGrade.Id > 0)
                    {
                        DbContext.FreeSql.GetRepository<ExamCourseUserLastGrade>().UpdateDiy.SetSource(lastGrade).UpdateColumns(a => new { a.GradeId, a.UpdateTime }).ExecuteAffrows();
                    }
                    else
                    {
                        lastGrade.Id = IdWorker.NextId();
                        DbContext.FreeSql.GetRepository<ExamCourseUserLastGrade>().Insert(lastGrade);
                    }
                });

                result.Data = grade;
            }
            catch (Exception ex)
            {
                result.Msg = "新增答题记录失败，请联系管理员！";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"新增答题记录常:examId-{exam.Id},UserId:{user.Id}";
                CommonLogger.Error(msg, ex);
                return result;
            }

            return result;
        }
        // <summary>
        /// 获取试卷题目信息
        /// </summary>
        /// <param name="examPaper"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<List<CourseQuestionInfo>> GetCourseExamQuestions(ExamPaperCourse exam, ExamCourseUserGrade grade, bool isView = false)
        {
            var questionList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetTopicBySectionId}{exam.SectionId}", () => DbContext.FreeSql.GetRepository<YssxSectionTopic>().Select
                   .From<YssxTopicPublic>((a, b) => a.LeftJoin(aa => aa.QuestionId == b.Id))
                   .Where((a, b) => a.SectionId == exam.SectionId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                   .OrderByDescending((a, b) => (b.QuestionType == QuestionType.MainSubQuestion
                                || b.QuestionType == QuestionType.FillGraphGrid || b.QuestionType == QuestionType.AccountEntry ||
                                b.QuestionType == QuestionType.FillGrid || b.QuestionType == QuestionType.FillBlank || b.QuestionType == QuestionType.ShortAnswerQuestion) ? 99 : 88)
                   .OrderBy((a, b) => b.Sort)
                   .ToList((a, b) => new CourseQuestionInfo()
                   {
                       QuestionId = b.Id,
                       ParentQuestionId = b.ParentId,
                       QuestionType = b.QuestionType,
                       Title = b.Title,
                       Score = b.Score,
                       Sort = b.Sort
                   }), 10 * 60, true, false);
            //用户题目作答信息
            var studentGradeDetails = new List<GradeDtailDto>();

            if (grade != null && grade.Id > 0)
            {
                studentGradeDetails = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.GradeId == grade.Id)
                                         .ToListAsync(a => new GradeDtailDto { Id = a.Id, QuestionId = a.QuestionId, ParentQuestionId = a.ParentQuestionId, Status = a.Status, AccountEntryStatus = a.AccountEntryStatus, Score = a.Score, Sort = -1 });
            }

            if (studentGradeDetails != null && studentGradeDetails.Any())
            {
                questionList.ForEach(a =>
                {
                    if (a.ParentQuestionId == 0)
                    {
                        var gradeDetail = studentGradeDetails.FirstOrDefault(s => s.QuestionId == a.QuestionId);
                        if (gradeDetail != null)
                        {
                            //if (a.QuestionType == QuestionType.MainSubQuestion)
                            //{
                            //    var details = studentGradeDetails.Where(s => s.ParentQuestionId == a.QuestionId && s.QuestionId != a.QuestionId);

                            //    if (details != null && details.Any())
                            //    {
                            //        var accountQuestions = questionList.Where(s => s.ParentQuestionId == a.QuestionId && s.QuestionType == QuestionType.AccountEntry).ToList();

                            //        if (accountQuestions != null && accountQuestions.Any())
                            //        {
                            //            accountQuestions.ForEach(s =>
                            //            {
                            //                var detail = details.FirstOrDefault(d => d.QuestionId == s.QuestionId);
                            //                if (detail != null && detail.Id > 0)
                            //                {
                            //                    detail.Sort = s.Sort;
                            //                }
                            //            });
                            //            //a.AccountEntryStatusDic = details.Where(s => s.Sort >= 0).Select(s => new Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>(s.QuestionId, s.AccountEntryStatus, s.AccountEntryAuditStatus, s.Sort)).ToList();
                            //        }
                            //    }
                            //}
                            //else if (a.QuestionType == QuestionType.AccountEntry)
                            //{
                            //    a.AccountEntryStatusDic = new List<Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>>() {
                            //    new Tuple<long, AccountEntryStatus,AccountEntryAuditStatus,int>(gradeDetail.QuestionId,gradeDetail.AccountEntryStatus,gradeDetail.AccountEntryAuditStatus,a.Sort)
                            //};
                            //}
                            if (exam.CanShowAnswerBeforeEnd || grade.Status == StudentExamStatus.End || isView)
                            {
                                switch (gradeDetail.Status)
                                {
                                    case AnswerResultStatus.Error:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Error;
                                        break;
                                    case AnswerResultStatus.Right:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Right;
                                        break;
                                    case AnswerResultStatus.PartRight:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.PartRight;
                                        break;
                                    default:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                                        break;
                                }

                                a.AnswerScore = gradeDetail.Score;
                            }
                            else
                            {
                                a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                            }
                        }
                        else
                        {
                            a.QuestionAnswerStatus = AnswerDTOStatus.NoAnswer;
                        }
                    }
                });
            }

            questionList.RemoveAll(s => s.ParentQuestionId > 0);
            //questionList = questionList.OrderBy(s => s.Sort).ToList();

            return questionList;
        }

        /// <summary>
        /// 课堂检验
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="isView"></param>
        /// <returns></returns>
        private async Task<List<CourseQuestionInfo>> GetCourseExamClassTestQuestions(ExamPaperCourse exam, ExamCourseUserGrade grade, bool isView = false)
        {
            var questionList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetTopicBySectionId}{exam.Id}", () => DbContext.FreeSql.GetRepository<YssxCourseTestTopic>().Select
                   .From<YssxTopicPublic>((a, b) => a.LeftJoin(aa => aa.TopicId == b.Id))
                   .Where((a, b) => a.TestTaskId == exam.TaskId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                   .ToList((a, b) => new CourseQuestionInfo()
                   {
                       QuestionId = b.Id,
                       ParentQuestionId = b.ParentId,
                       QuestionType = b.QuestionType,
                       Title = b.Title,
                       Score = b.Score,
                       Sort = b.Sort
                   }), 10 * 60, true, false);

            //用户题目作答信息
            var studentGradeDetails = new List<GradeDtailDto>();

            if (grade != null && grade.Id > 0)
            {
                studentGradeDetails = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.GradeId == grade.Id)
                                         .ToListAsync(a => new GradeDtailDto { Id = a.Id, QuestionId = a.QuestionId, ParentQuestionId = a.ParentQuestionId, Status = a.Status, AccountEntryStatus = a.AccountEntryStatus, Score = a.Score, Sort = -1 });
            }

            if (studentGradeDetails != null && studentGradeDetails.Any())
            {
                questionList.ForEach(a =>
                {
                    if (a.ParentQuestionId == 0)
                    {
                        var gradeDetail = studentGradeDetails.FirstOrDefault(s => s.QuestionId == a.QuestionId);
                        if (gradeDetail != null)
                        {

                            if (exam.CanShowAnswerBeforeEnd || grade.Status == StudentExamStatus.End || isView)
                            {
                                switch (gradeDetail.Status)
                                {
                                    case AnswerResultStatus.Error:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Error;
                                        break;
                                    case AnswerResultStatus.Right:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Right;
                                        break;
                                    case AnswerResultStatus.PartRight:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.PartRight;
                                        break;
                                    default:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                                        break;
                                }

                                a.AnswerScore = gradeDetail.Score;
                            }
                            else
                            {
                                a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                            }
                        }
                        else
                        {
                            a.QuestionAnswerStatus = AnswerDTOStatus.NoAnswer;
                        }
                    }
                });
            }

            questionList.RemoveAll(s => s.ParentQuestionId > 0);
            questionList = questionList.OrderBy(s => s.Sort).ToList();

            return questionList;
        }
        // <summary>
        /// 获取试卷题目信息
        /// </summary>
        /// <param name="examPaper"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<List<CourseQuestionInfo>> GetTaskExamQuestions(ExamPaperCourse exam, ExamCourseUserGrade grade, bool isView = false)
        {
            var questionList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetTopicByTaskId}{exam.TaskId}", () => DbContext.FreeSql.GetRepository<YssxTaskTopic>().Select
                   .From<YssxTopicPublic>((a, b) => a.LeftJoin(aa => aa.TopicId == b.Id))
                   .Where((a, b) => a.TaskId == exam.TaskId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                   .ToList((a, b) => new CourseQuestionInfo()
                   {
                       QuestionId = b.Id,
                       ParentQuestionId = b.ParentId,
                       QuestionType = b.QuestionType,
                       Title = b.Title,
                       Score = b.Score,
                       Sort = b.Sort
                   }), 10 * 60, true, false);

            //用户题目作答信息
            var studentGradeDetails = new List<GradeDtailDto>();

            if (grade != null && grade.Id > 0)
            {
                studentGradeDetails = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.GradeId == grade.Id)
                                         .ToListAsync(a => new GradeDtailDto { Id = a.Id, QuestionId = a.QuestionId, ParentQuestionId = a.ParentQuestionId, Status = a.Status, AccountEntryStatus = a.AccountEntryStatus, Score = a.Score, Sort = -1 });
            }

            if (studentGradeDetails != null && studentGradeDetails.Any())
            {
                questionList.ForEach(a =>
                {
                    if (a.ParentQuestionId == 0)
                    {
                        var gradeDetail = studentGradeDetails.FirstOrDefault(s => s.QuestionId == a.QuestionId);
                        if (gradeDetail != null)
                        {
                            if (exam.CanShowAnswerBeforeEnd || grade.Status == StudentExamStatus.End || isView)
                            {
                                switch (gradeDetail.Status)
                                {
                                    case AnswerResultStatus.Error:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Error;
                                        break;
                                    case AnswerResultStatus.Right:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Right;
                                        break;
                                    case AnswerResultStatus.PartRight:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.PartRight;
                                        break;
                                    default:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                                        break;
                                }

                                a.AnswerScore = gradeDetail.Score;
                            }
                            else
                            {
                                a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                            }
                        }
                        else
                        {
                            a.QuestionAnswerStatus = AnswerDTOStatus.NoAnswer;
                        }
                    }
                });
            }

            questionList.RemoveAll(s => s.ParentQuestionId > 0);
            questionList = questionList.OrderBy(s => s.Sort).ToList();

            return questionList;
        }

        // <summary>
        /// 获取课堂测验试卷题目信息
        /// </summary>
        /// <param name="examPaper"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<List<CourseQuestionInfo>> GetCourseTestExamQuestions(ExamPaperCourse exam, ExamCourseUserGrade grade, bool isView = false)
        {
            var questionList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetTopicByTaskId}{exam.TaskId}", () => DbContext.FreeSql.GetRepository<YssxCourseTestTopic>()
                   .Select.From<YssxTopicPublic>((a, b) => a.LeftJoin(aa => aa.TopicId == b.Id))
                   .Where((a, b) => a.TestTaskId == exam.TaskId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                   .ToList((a, b) => new CourseQuestionInfo()
                   {
                       QuestionId = b.Id,
                       ParentQuestionId = b.ParentId,
                       QuestionType = b.QuestionType,
                       Title = b.Title,
                       Score = b.Score,
                       Sort = b.Sort
                   }), 10 * 60, true, false);

            //用户题目作答信息
            var studentGradeDetails = new List<GradeDtailDto>();

            if (grade != null && grade.Id > 0)
            {
                studentGradeDetails = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.GradeId == grade.Id)
                                         .ToListAsync(a => new GradeDtailDto { Id = a.Id, QuestionId = a.QuestionId, ParentQuestionId = a.ParentQuestionId, Status = a.Status, AccountEntryStatus = a.AccountEntryStatus, Score = a.Score, Sort = -1 });
            }

            if (studentGradeDetails != null && studentGradeDetails.Any())
            {
                questionList.ForEach(a =>
                {
                    if (a.ParentQuestionId == 0)
                    {
                        var gradeDetail = studentGradeDetails.FirstOrDefault(s => s.QuestionId == a.QuestionId);
                        if (gradeDetail != null)
                        {
                            if (exam.CanShowAnswerBeforeEnd || grade.Status == StudentExamStatus.End || isView)
                            {
                                switch (gradeDetail.Status)
                                {
                                    case AnswerResultStatus.Error:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Error;
                                        break;
                                    case AnswerResultStatus.Right:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Right;
                                        break;
                                    case AnswerResultStatus.PartRight:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.PartRight;
                                        break;
                                    default:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                                        break;
                                }

                                a.AnswerScore = gradeDetail.Score;
                            }
                            else
                            {
                                a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                            }
                        }
                        else
                        {
                            a.QuestionAnswerStatus = AnswerDTOStatus.NoAnswer;
                        }
                    }
                });
            }

            questionList.RemoveAll(s => s.ParentQuestionId > 0);
            questionList = questionList.OrderBy(s => s.Sort).ToList();

            return questionList;
        }

        // <summary>
        /// 获取试卷题目信息
        /// </summary>
        /// <param name="examPaper"></param>
        /// <param name="grade"></param>
        /// <returns></returns>
        private async Task<List<CourseQuestionInfo>> GetNewTaskExamQuestions(ExamPaperCourse exam, ExamCourseUserGrade grade, bool isView = false)
        {
            //var courseTask = await DbContext.FreeSql.GetRepository<YssxCourseTask>().Where(s => s.Id == exam.TaskId).FirstAsync();
            var questionList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetTopicByCourseTaskId}{exam.TaskId}", () => DbContext.FreeSql.GetRepository<YssxCourseTaskTopic>().Select
                   .From<YssxTopicPublic>((a, b) => a.LeftJoin(aa => aa.TopicId == b.Id))
                   .Where((a, b) => a.TaskId == exam.TaskId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                   .ToList((a, b) => new CourseQuestionInfo()
                   {
                       QuestionId = b.Id,
                       ParentQuestionId = b.ParentId,
                       QuestionType = b.QuestionType,
                       Title = b.Title,
                       Score = b.Score,
                       Sort = b.Sort
                   }), 10 * 60, true, false);

            //用户题目作答信息
            var studentGradeDetails = new List<GradeDtailDto>();

            if (grade != null && grade.Id > 0)
            {
                studentGradeDetails = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.GradeId == grade.Id)
                                         .ToListAsync(a => new GradeDtailDto { Id = a.Id, QuestionId = a.QuestionId, ParentQuestionId = a.ParentQuestionId, Status = a.Status, AccountEntryStatus = a.AccountEntryStatus, Score = a.Score, Sort = -1 });
            }

            if (studentGradeDetails != null && studentGradeDetails.Any())
            {
                questionList.ForEach(a =>
                {
                    if (a.ParentQuestionId == 0)
                    {
                        var gradeDetail = studentGradeDetails.FirstOrDefault(s => s.QuestionId == a.QuestionId);
                        if (gradeDetail != null)
                        {
                            if (exam.CanShowAnswerBeforeEnd || grade.Status == StudentExamStatus.End || isView)
                            {
                                switch (gradeDetail.Status)
                                {
                                    case AnswerResultStatus.Error:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Error;
                                        break;
                                    case AnswerResultStatus.Right:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Right;
                                        break;
                                    case AnswerResultStatus.PartRight:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.PartRight;
                                        break;
                                    default:
                                        a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                                        break;
                                }

                                a.AnswerScore = gradeDetail.Score;
                            }
                            else
                            {
                                a.QuestionAnswerStatus = AnswerDTOStatus.Answered;
                            }
                        }
                        else
                        {
                            a.QuestionAnswerStatus = AnswerDTOStatus.NoAnswer;
                        }
                    }
                });
            }

            questionList.RemoveAll(s => s.ParentQuestionId > 0);
            //if (courseTask.TaskTopicSortType == TaskTopicSortType.Random)
            //    questionList = questionList.OrderBy(s => new Random().NextDouble()).ToList();
            //else
                questionList = questionList.OrderBy(s => s.Sort).ToList();

            return questionList;
        }
        #region 预览相关接口


        /// <summary>
        /// 预览试卷基本信息--接口方法
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetTaskExamPaperInfoView(long examId, UserTicket userTicket)
        {
            var result = new ResponseContext<ExamPaperCourseBasicInfo>();

            var user = userTicket;

            #region 校验

            if (examId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "examId不能为0";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(s => s.Id == examId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamCourseById}{examId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }

            #endregion

            #region 考试基本信息

            result.Data = new ExamPaperCourseBasicInfo()
            {
                ExamId = exam.Id,
                TotalMinutes = exam.TotalMinutes,
                ExamScore = exam.TotalScore,
                PassScore = exam.PassScore,

                //QuestionInfoList = await GetTaskExamQuestions(exam, new ExamCourseUserGrade(), true),
            };
            if (exam.ExamType == CourseExamType.NewCourseTask)
                result.Data.QuestionInfoList = await GetNewTaskExamQuestions(exam, new ExamCourseUserGrade(), true);
            else
                result.Data.QuestionInfoList = await GetTaskExamQuestions(exam, new ExamCourseUserGrade(), true);
            #endregion

            return result;
        }

        /// <summary>
        /// 预览试卷作答信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetExamPaperGradeInfoView(CourseExamType examType, long gradeId, UserTicket userTicket)
        {
            var result = new ResponseContext<ExamPaperCourseBasicInfo>();
            var user = userTicket;

            #region 校验

            if (gradeId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "gradeId不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(s => s.Id == gradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (grade == null || grade.Id == 0)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"答题记录不存在 ";
                return result;
            }

            var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(s => s.Id == grade.ExamId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetExamCourseById}{grade.ExamId}", true, 10 * 60);

            if (exam == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = $"考试信息不存在 ";
                return result;
            }

            #endregion

            #region 考试基本信息
            var questionInfoList = new List<CourseQuestionInfo>();
            switch (examType)
            {
                case CourseExamType.CourceTest:
                case CourseExamType.PrepareCourseTest:
                    questionInfoList = await GetCourseExamQuestions(exam, grade, true);
                    break;
                case CourseExamType.ClassExam:
                case CourseExamType.ClassTask:
                case CourseExamType.ClassHomeWork:                
                    questionInfoList = await GetTaskExamQuestions(exam, grade, true);
                    break;
                case CourseExamType.NewCourseTask:
                    questionInfoList = await GetNewTaskExamQuestions(exam, grade, true);
                    break;
                case CourseExamType.ClassTest:
                    questionInfoList = await GetCourseTestExamQuestions(exam, grade, true);
                    break;
                default:
                    break;
            }
            result.Data = new ExamPaperCourseBasicInfo()
            {
                ExamId = exam.Id,
                TotalMinutes = exam.TotalMinutes,
                ExamScore = exam.TotalScore,
                PassScore = exam.PassScore,
                StudentExamStatus = grade.Status,

                LeftSeconds = (long)grade.LeftSeconds,
                UsedSeconds = (long)grade.UsedSeconds,
                GradeId = gradeId,

                QuestionInfoList = questionInfoList,

                UserGradeInfo = new UserGradeInfo()
                {
                    GradeId = grade.Id,
                    Score = grade.Score,
                    CorrectCount = grade.CorrectCount,
                    ErrorCount = grade.ErrorCount,
                    PartRightCount = grade.PartRightCount,
                    BlankCount = grade.BlankCount,
                    IsPass = grade.Score >= exam.PassScore,
                    Status = grade.Status,
                }
            };

            #endregion

            return result;
        }

        public async Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourceOnlineExamPaperBasicInfo(long examId, long gradeId, UserTicket user)
        {
            var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(s => s.Id == examId && s.IsDelete == CommonConstants.IsNotDelete)
               .FirstAsync($"{CommonConstants.Cache_GetExamPaperCourseById}{examId}", true, 10 * 60);
            if (exam != null)
            {
                return await GetCourceTestExamPaperBasicInfo(exam, gradeId, user);
            }

            return new ResponseContext<ExamPaperCourseBasicInfo> { Code = CommonConstants.ErrorCode, Msg = "没有找到任务试卷！" };
        }

        /// <summary>
        /// 获取剩余考试时间
        /// </summary>
        /// <param name="grade"></param>
        /// <returns></returns>
        private double GetLeftSeconds(double leftSeconds, DateTime recordTime)
        {
            var left = leftSeconds - (DateTime.Now - recordTime).TotalSeconds;
            if (left <= 0)
            {
                return 0;
            }
            return left;
        }


        #endregion


        #endregion

        public async Task<ResponseContext<List<ExamCourseDetailsDto>>> GetExamCourseDetailsList(long sectionId, UserTicket user)
        {
            List<ExamCourseDetailsDto> list = await DbContext.FreeSql.GetRepository<YssxSection>()
                .Select.From<ExamCourseUserGrade>((a, b) => a.InnerJoin(x => x.Id == b.SectionId))
                .Where((a, b) => a.Id == sectionId && b.UserId == user.Id && b.Status == StudentExamStatus.End)
                .OrderByDescending((a,b)=>b.UpdateTime)
                .ToListAsync((a, b) => new ExamCourseDetailsDto
            {
                SectionName = a.SectionTitle,
                Date = b.UpdateTime,
                ExamId = b.ExamId,
                GradeId = b.Id,
                Socre = b.Score
            });

            return new ResponseContext<List<ExamCourseDetailsDto>> { Data = list };
        }


        #region 3.15课程练习相关接口
        /// <summary>
        /// 课程练习/预览
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<CourseSectionInfoDto>> GetCourseInfoByCourseId(long courseId,long userId)
        {
            var courseInfo = new CourseSectionInfoDto();
            var oldstudent = DbContext.FreeSql.Select<YssxStudent>().Where(m => m.UserId == userId && m.IsDelete == CommonConstants.IsNotDelete).First();
            long classId = 0;
            if (oldstudent != null)
                classId = oldstudent.ClassId;

            var parents = await DbContext.FreeSql.GetRepository<YssxSection>()
                .Select.From<YssxPracticeDetails>((a, b) => a.LeftJoin(aa => aa.Id == b.TenantId && b.ClassId == classId && b.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b) => a.CourseId == courseId && a.ParentId == 0 && a.IsDelete == CommonConstants.IsNotDelete)
                   .OrderBy((a,b) => a.Sort)
                   .ToListAsync<SectionInfoListDto>();

            var childs = await DbContext.FreeSql.GetRepository<YssxSection>()
                .Select.From<YssxPracticeDetails>((a, b) => a.LeftJoin(aa => aa.Id == b.TenantId && b.ClassId == classId && b.IsDelete == CommonConstants.IsNotDelete))
                .Where((a,b) => a.CourseId == courseId && a.ParentId > 0 && a.IsDelete == CommonConstants.IsNotDelete)
                   .OrderBy((a,b) => a.Sort)
                   .ToListAsync<SectionInfoListDto>();
            //教材
            var courceJc = await DbContext.FreeSql.GetRepository<YssxSectionFiles>()
                .Where(a => a.CourseId == courseId && a.SectionType == 7 && a.IsDelete == CommonConstants.IsNotDelete)
                .OrderByDescending(a => a.CreateTime)
                .FirstAsync(a => new SectionFileInfoDto
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    KnowledgePointNames=a.KnowledgePointNames,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                    Remark=a.Remark
                });
            //根据课程id查找除教材外所有课程附件集合
            var fileList = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Select.From<YssxSection>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id))
                .Where((a, b) => a.CourseId == courseId && a.SectionType!=7 && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new SectionFileInfoDto
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    SectionName = b.SectionName,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    KnowledgePointNames = a.KnowledgePointNames,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                    IsFile=a.IsFile,
                    Remark=a.Remark
                });
            //根据课程查题目
            var topics = await DbContext.FreeSql.GetRepository<YssxSectionTopic>()
                .Where(s => s.CourseId == courseId && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(s => new { s.Id, s.SectionId });
            foreach (var item in parents)
            {
                item.Level = 1;
                item.CoursewareList= fileList.Where(x => x.SectionType == 1 && x.SectionId==item.Id).ToList();
                item.VideoList = fileList.Where(x => x.SectionType == 3 && x.SectionId == item.Id).ToList();
                item.TeachingPlanList = fileList.Where(x => x.SectionType == 2 && x.SectionId == item.Id).ToList();
                item.SZCaseList = fileList.Where(x => x.SectionType == 6 && x.SectionId == item.Id).ToList();
                item.IsExistTopic = topics.Where(x => x.SectionId == item.Id).Any();
                item.Children = GetChildNodes(childs, item.Id, item.Level, fileList, topics);
            }
            courseInfo.Id = courseId;
            courseInfo.CourseJc = courceJc;
            courseInfo.SectionInfoList = parents;

            courseInfo.SectionCount = parents.Count;
            courseInfo.QuestionCount = await DbContext.FreeSql.GetRepository<YssxSectionTopic>()
                .Where(s => s.CourseId == courseId && s.IsDelete == CommonConstants.IsNotDelete).CountAsync();
            courseInfo.CoursewareCount = fileList.Where(s => s.SectionType == 1).Count();
            courseInfo.TeachingPlanCount= fileList.Where(s => s.SectionType == 2).Count();
            courseInfo.VideoCount= fileList.Where(s => s.SectionType == 3).Count();

            return new ResponseContext<CourseSectionInfoDto>(courseInfo);
        }
        private List<SectionInfoListDto> GetChildNodes(List<SectionInfoListDto> childs, long parentId, int parentLevel,List<SectionFileInfoDto> fileList, IEnumerable<dynamic> topics)
        {
            var sections = new List<SectionInfoListDto>();
            foreach (var item in childs.Where(p => p.ParentId == parentId))
            {
                var section = new SectionInfoListDto();
                section.Id = item.Id;
                section.SectionName = item.SectionName;
                section.SectionTitle = item.SectionTitle;
                section.ParentId = item.ParentId;
                section.SectionType = item.SectionType;
                section.CourseId = item.CourseId;
                section.Sort = item.Sort;
                section.Level = parentLevel + 1;
                section.CoursewareList = fileList.Where(x => x.SectionType == 1 && x.SectionId == item.Id).ToList();
                section.VideoList = fileList.Where(x => x.SectionType == 3 && x.SectionId == item.Id).ToList();
                section.TeachingPlanList = fileList.Where(x => x.SectionType == 2 && x.SectionId == item.Id).ToList();
                section.SZCaseList = fileList.Where(x => x.SectionType == 6 && x.SectionId == item.Id).ToList();
                section.IsExistTopic = topics.Where(x => x.SectionId == item.Id).Any();
                section.Children = GetChildNodes(childs, item.Id, section.Level, fileList,topics);

                sections.Add(section);
            }
            return sections;
        }

        /// <summary>
        /// 根据章节Id获取节作答信息
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<SubSectionInfoDto>> GetSectionExamInfo(long sectionId, long userId)
        {
            var sectionInfo = new SubSectionInfoDto();
            var subSectionInfoList = await DbContext.FreeSql.GetRepository<YssxSection>().Select
            .From<YssxSectionTopic, ExamCourseUserLastGrade, ExamCourseUserGradeDetail>((a, b, c, d) =>
                    a.LeftJoin(aa => aa.Id == b.SectionId && b.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.Id == c.SectionId && c.UserId == userId)
                    .LeftJoin(aa => c.GradeId == d.GradeId && d.QuestionId == d.ParentQuestionId)
            )
            .Where((a, b, c, d) => a.Id == sectionId && a.IsDelete == CommonConstants.IsNotDelete)
            .OrderBy((a, b, c, d) => a.Sort)
            .GroupBy((a, b, c, d) => new { a.Id, a.SectionName, a.SectionTitle, c.GradeId })
            .ToListAsync(a => new SubSectionInfoDto()
            {
                SectionId = a.Key.Id,
                SectionName = a.Key.SectionName,
                SectionTitle = a.Key.SectionTitle,
                TotalScore = 0,
                GradeId = a.Key.GradeId,
                TotalQuestionCount = Convert.ToInt32("count(distinct b.id)"),
                DoQuestionCount = Convert.ToInt32("count(distinct d.id)"),
            });
            List<ExamCourseUserGrade> userGrades = await DbContext.FreeSql.GetRepository<YssxSection>().Select
                .From<ExamCourseUserGrade>((a, b) => a.InnerJoin(x => x.Id == b.SectionId))
                .Where((a, b) => a.Id == sectionId && b.UserId == userId && a.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new ExamCourseUserGrade
                {
                    Id = b.Id,
                    Status = b.Status
                });
            if (subSectionInfoList.Any())
            {
                sectionInfo = subSectionInfoList[0];
                if (sectionInfo.GradeId > 0)
                {
                    var grade = userGrades.Where(e => e.Id == sectionInfo.GradeId).FirstOrDefault();
                    if (grade != null)
                    {
                        sectionInfo.Status = grade.Status;
                        if (sectionInfo.Status == StudentExamStatus.End)
                        {
                            sectionInfo.GradeId = 0;
                            sectionInfo.DoQuestionCount = 0;
                        }
                    }
                    else
                    {
                        sectionInfo.Status = StudentExamStatus.Wait;
                    }

                }
            }
            
            return new ResponseContext<SubSectionInfoDto>(CommonConstants.SuccessCode, null, sectionInfo);
        }
        /// <summary>
        /// 案例分析题打分
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SubmitAnswerByTeacher(ShortQuestionAnswer answer, long userId)
        {
            var result = new ResponseContext<bool>(true);
            var gradeDetail = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>()
                .Where(a => a.GradeId == answer.GradeId && a.ParentQuestionId == answer.QuestionId).Master().FirstAsync();
            if (gradeDetail == null)
            {
                result.Data = false;
                result.Msg = "学生未作答，无法计分";
                return result;
            }
            var nowTime = DateTime.Now;
            var oldScore = gradeDetail.Score;
            gradeDetail.Comment = answer.Comment;
            gradeDetail.IsReadOver = true;
            gradeDetail.Score = answer.Score;
            gradeDetail.UpdateTime = nowTime;
            gradeDetail.UpdateBy = userId;
            var grade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>()
                .Where(s => s.Id == answer.GradeId).FirstAsync();
            grade.Score = grade.Score + gradeDetail.Score - oldScore;
            grade.UpdateTime = nowTime;
            grade.UpdateBy = userId;
            DbContext.FreeSql.Transaction(() =>
            {
                DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().UpdateDiy.SetSource(gradeDetail)
                 .UpdateColumns(a => new {  a.Score,a.Comment,a.IsReadOver, a.UpdateTime,a.UpdateBy }).ExecuteAffrows();
                DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().UpdateDiy.SetSource(grade)
                 .UpdateColumns(a => new { a.Score, a.UpdateTime, a.UpdateBy }).ExecuteAffrows();
            });
            return result;

        }
        /// <summary>
        /// 根据任务查询简答题列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<TaskShortQuestionDto>> GetShortQuestionListByTask(long taskId)
        {
            var result = new ResponseContext<TaskShortQuestionDto>();
            TaskShortQuestionDto model = new TaskShortQuestionDto();
            var taskClassList = await DbContext.FreeSql.GetRepository<YssxCourseTaskClass>().Where(s => s.TaskId == taskId && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(s=>s.ClassId);
            var classIds = string.Join(',', taskClassList.ToArray());
            model.TaskStudentCount = await DbContext.FreeSql.GetRepository<YssxStudent>().Where(s => classIds.Contains(s.ClassId.ToString()) && s.ClassId>0 && s.IsDelete==CommonConstants.IsNotDelete).CountAsync();

            var taskShortQuestions = await DbContext.FreeSql.GetRepository<YssxCourseTaskTopic>().Select.From<YssxTopicPublic>(
                (a, b) =>
                a.InnerJoin(aa => aa.TopicId == b.Id))
                .Where((a, b) => a.TaskId == taskId && a.QuestionType==QuestionType.ShortAnswerQuestion && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new ShortQuestionInfo
                {
                    QuestionId = a.TopicId,
                    QuestionName = b.Title,
                    Score = b.Score
                });
            var questionIds = string.Join(',', taskShortQuestions.Select(s => s.QuestionId).ToArray());
            var gradeDetails= await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Select.From<ExamCourseUserGrade>((a, b) => a.LeftJoin(aa => aa.GradeId == b.Id))
                    .Where((a, b) => b.TaskId == taskId && a.IsReadOver && questionIds.Contains(a.ParentQuestionId.ToString()) && a.IsDelete==CommonConstants.IsNotDelete && b.IsDelete==CommonConstants.IsNotDelete)
                    .ToListAsync((a,b)=>new { a.ParentQuestionId, a.IsReadOver });
            if (gradeDetails.Any())
            {
                foreach (var item in taskShortQuestions)
                {
                    item.CheckedCount = gradeDetails.Where(s=>s.ParentQuestionId==item.QuestionId).Count();
                }
            }
            model.ShortQuestions = taskShortQuestions;
            result.Data = model;
            return result;
        }
        /// <summary>
        /// 根据案例分析题ID获取学生作答记录列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<ShortQuestionAnswerDto>>> GetStudentAnswerList(long taskId, long questionId,long classId,int answerStatus,string keywords)
        {
            var result = new ResponseContext<List<ShortQuestionAnswerDto>>();

            var answerInfo = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Select.From<ExamCourseUserGrade>
                ((a, b) => a.LeftJoin(aa => aa.GradeId == b.Id))
                    .Where((a, b) => b.TaskId == taskId && a.ParentQuestionId == questionId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b) => new
                    {
                        b.UserId,
                        GradeId = a.GradeId,
                        GradeDetailId = a.Id,
                        AnswerValue=a.Answer,
                        a.Score,
                        a.Comment,
                        a.IsReadOver
                    });
            var taskClassList = await DbContext.FreeSql.GetRepository<YssxCourseTaskClass>().Where(s => s.TaskId == taskId).ToListAsync(s => s.ClassId);

            var classIds = string.Join(',', taskClassList.ToArray());

            var studentInfo = await DbContext.FreeSql.GetRepository<YssxStudent>()
                .Where(s => classIds.Contains(s.ClassId.ToString()) && s.ClassId>0 && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(s=>new ShortQuestionAnswerDto
                {
                    UserId=s.UserId,
                    StudentName=s.Name,
                    StudentNo=s.StudentNo,
                    ClassId=s.ClassId,
                    ClassName=s.ClassName,
                });
            foreach (var item in studentInfo)
            {
                var answer = answerInfo.Where(s => s.UserId == item.UserId).FirstOrDefault();
                if (answer != null)
                {
                    item.GradeId = answer.GradeId;
                    item.GradeDetailId = answer.GradeDetailId;
                    item.AnswerValue = answer.AnswerValue;
                    item.Score = answer.Score;
                    item.IsReadOver = answer.IsReadOver;
                    item.Comment = answer.Comment;
                }
                
            }
            if (classId > 0)
                studentInfo = studentInfo.Where(s => s.ClassId == classId).ToList();
            if (answerStatus > 0)
            {
                if (answerStatus == 1)
                    studentInfo = studentInfo.Where(s => s.GradeDetailId > 0).ToList();
                if(answerStatus==2)
                    studentInfo = studentInfo.Where(s => s.GradeDetailId == 0).ToList();
            }
            if (!string.IsNullOrEmpty(keywords))
                studentInfo = studentInfo.Where(s => s.StudentNo == keywords || s.StudentName.Contains(keywords)).ToList();
            result.Data = studentInfo;
            return result;
        }
        #endregion
    }
}
