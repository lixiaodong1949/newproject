﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Tas.Common.IdGenerate;
using Tas.Common.Utils;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Redis;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.Poco;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Exam;
using Yssx.Repository.Extensions;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Logger;
using Yssx.Framework.AutoMapper;
using Yssx.S.Dto;
using Yssx.Framework.Utils;
using Yssx.Framework.AnswerCompare.EntitysV2;
using Yssx.Framework.AnswerCompare;
using Yssx.S.IServices.Examination;
using Yssx.S.Pocos.Topics;
using Yssx.S.Pocos.SceneTraining;
using Yssx.S.Pocos.OaxTable;

namespace Yssx.S.ServiceImpl.Examination
{
    /// <summary>
    /// 用户练习做题相关服务 
    /// </summary>
    public class UserExercisesService : IUserExercisesService
    {
        /// <summary>
        /// 无参构造函数
        /// </summary>
        public UserExercisesService()
        {

        }

        #region 答题相关接口

        #region 获取题目信息

        /// <summary>
        /// 获取题目信息--接口方法
        /// </summary>
        /// <param name="progressId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfo(long progressId, ExercisesType exercisesType, long questionId,UserTicket userTicket)
        {
            var result = new ResponseContext<ExamQuestionInfo>(CommonConstants.BadRequest, $"");
            try
            {
                var user = userTicket;
                YssxProgress progress = null;
                if (progressId > 0)
                {
                    progress = await DbContext.FreeSql.GetRepository<YssxProgress>().Where(s => s.Id == progressId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                    if (progress == null)
                    {
                        result.Code = CommonConstants.NotFund;
                        result.Msg = $"实训记录不存在";
                        return result;
                    }
                }
                
                #region 校验
                var question = await DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.Id == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync($"{CommonConstants.Cache_GetPublicTopicById}{questionId}", true, 10 * 60, true);

                if (question == null)
                {
                    result.Code = CommonConstants.NotFund;
                    result.Msg = $"题目不存在";
                    return result;
                }

                #endregion

                switch (exercisesType)
                {
                    case ExercisesType.Training:

                        return await GetTrainQuestionInfo(progress,question, user);

                    case ExercisesType.CourceExercises:
                        break;
                    default:

                        throw new NotImplementedException($"{exercisesType}类型不存在");
                }
            }
            catch (Exception ex)
            {
                result.Msg = "系统异常";
                result.Code = CommonConstants.ErrorCode;
                CommonLogger.Error(JsonHelper.SerializeObject(ex));
            }

            return result;
        }

        /// <summary>
        /// 获取题目基本信息--场景实训
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="progress"></param>
        /// <param name="question"></param>
        /// <returns></returns>
        private async Task<ResponseContext<ExamQuestionInfo>> GetTrainQuestionInfo(YssxProgress progress, YssxTopicPublic question, UserTicket user)
        {
            return await GetQuestionInfoBase(progress, question, user);
        }
        /// <summary>
        /// 获取题目信息基础函数
        /// </summary>
        /// <returns></returns>
        private async Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfoBase(YssxProgress progress, YssxTopicPublic question, UserTicket user, bool isView = false)
        {
            var result = new ResponseContext<ExamQuestionInfo>();

            var progressId = progress != null ? progress.Id : 0;
            var questionId = question.Id;

            var questionDTO = question.MapTo<ExamQuestionInfo>();
            questionDTO.QuestionId = questionId;

            //加载题目信息
            if (questionDTO.QuestionType == QuestionType.SingleChoice
                || questionDTO.QuestionType == QuestionType.MultiChoice
                || questionDTO.QuestionType == QuestionType.Judge)
            {
                //添加选项信息
                var options = await DbContext.FreeSql.GetRepository<YssxPublicAnswerOption>().Where(a => a.TopicId == questionId).OrderBy(a => a.Sort).ToListAsync($"{CommonConstants.Cache_GetPublicOptionsByTopicId}{questionId}", true, 10 * 60);
                var optionList = new List<ChoiceOption>();
                options.ForEach(a =>
                {
                    optionList.Add(new ChoiceOption()
                    {
                        Id = a.Id,
                        Name = a.Name,
                        Text = a.AnswerOption,
                        AttatchImgUrl = a.AnswerFileUrl
                    });
                });
                questionDTO.Options = optionList;
            }
            else
            {
                //复杂题型
                //1.添加附件信息
                var fileList = new List<QuestionFile>();
                var fileIds = question.TopicFileIds;
                if (!string.IsNullOrEmpty(fileIds))
                {
                    var fileIdArr = fileIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                    if (fileIdArr.Any())
                    {
                        var files = await DbContext.FreeSql.GetRepository<YssxTopicPublicFile>().Where(a => fileIdArr.Contains(a.Id)).Select(a => new QuestionFile() { Name = a.Name, Url = a.Url, Sort = a.Sort }).ToListAsync($"{CommonConstants.Cache_GetPublicTopicFilesByTopicId}{questionId}", true, 10 * 60);
                        questionDTO.QuestionFile = files;
                    }
                }
                //2.添加子题目
                if (questionDTO.QuestionType == QuestionType.MainSubQuestion)
                {
                    var subQuestions = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.ParentId == questionId && s.IsDelete == CommonConstants.IsNotDelete)
                        .OrderBy(s => s.Sort)
                        .ToListAsync($"{CommonConstants.Cache_GetSubPublicTopicByParentId}{questionId}", true, 10 * 60, true, false).Result
                        .Select(s =>
                        new ExamQuestionInfo()
                        {
                            QuestionId = s.Id,
                            QuestionType = s.QuestionType,
                            QuestionContentType = s.QuestionContentType,
                            CalculationType = s.CalculationType,
                            Hint = s.Hint,
                            Content = s.Content,
                            TopicContent = s.TopicContent,
                            FullContent = s.FullContent,
                            AnswerValue = s.AnswerValue,
                            Score = s.Score,
                            Sort = s.Sort,
                            IsCopy = s.IsCopy,
                            IsCopyBigData = s.IsCopyBigData,
                            IsDisorder = s.IsDisorder
                        }).ToList();

                    var detailList = new List<YssxTrainRecordDetail>();
                    if (progressId > 0)
                    {
                        detailList = await DbContext.FreeSql.GetRepository<YssxTrainRecordDetail>().Where(s => s.ProgressId == progressId && s.ParentQuestionId == questionId)
                            .ToListAsync(s => new YssxTrainRecordDetail()
                            {
                                Answer = s.Answer,
                                AnswerCompareInfo = s.AnswerCompareInfo,
                                QuestionId = s.QuestionId
                            });
                    }

                    if (subQuestions.Any() && detailList.Any())
                    {
                        subQuestions.ForEach(s =>
                        {
                            var detail = detailList.FirstOrDefault(a => a.QuestionId == s.QuestionId);
                            if (detail != null && detail.QuestionId > 0)
                            {
                                s.StudentAnswer = detail.Answer;
                                s.AnswerCompareInfo = detail.AnswerCompareInfo;
                            }
                        });
                    }

                    var choiceQuestionIds = subQuestions.Where(a => a.QuestionType == QuestionType.SingleChoice
                    || a.QuestionType == QuestionType.MultiChoice || a.QuestionType == QuestionType.Judge).Select(a => a.QuestionId).ToArray();

                    if (choiceQuestionIds.Any())
                    {
                        var optionAll = await DbContext.FreeSql.GetRepository<YssxPublicAnswerOption>().Where(a => choiceQuestionIds.Contains(a.TopicId)).ToListAsync($"{CommonConstants.Cache_GetPublicOptionsByTopicId}{questionId}", true, 10 * 60);
                        subQuestions.ForEach(a =>
                        {
                            var options = optionAll.Where(c => c.TopicId == a.QuestionId).Select(d => new ChoiceOption()
                            {
                                Id = d.Id,
                                Name = d.Name,
                                Text = d.AnswerOption,
                                AttatchImgUrl = d.AnswerFileUrl
                            });
                            if (options.Any())
                            {
                                a.Options = options.ToList();
                            }
                        });
                    }

                    if (subQuestions.Any(s => s.QuestionType == QuestionType.AccountEntry))
                    {
                        subQuestions.ForEach(s =>
                        {
                            if (s.QuestionType == QuestionType.AccountEntry)
                            {
                                s.CertificateTopic = DbContext.FreeSql.GetRepository<YssxCertificateTopicPublic>().Select.Where(a => a.TopicId == s.QuestionId && a.IsDelete == CommonConstants.IsNotDelete)
                                .Select(a => new CertificateTopicView())
                                .First($"{CommonConstants.Cache_GetCertificateTopicPublicByTopicId}{s.QuestionId}", true, 10 * 60);
                            }
                        });
                    }

                    questionDTO.SubQuestion = subQuestions;
                }
                else if (question.QuestionType == QuestionType.AccountEntry)//分录题
                {
                    questionDTO.CertificateTopic = await DbContext.FreeSql.GetRepository<YssxCertificateTopicPublic>().Select.Where(s => s.TopicId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                        .Select(s => new CertificateTopicView())
                        .FirstAsync($"{CommonConstants.Cache_GetCertificateTopicPublicByTopicId}{questionId}", true, 10 * 60);
                }
            }

            #region 附上答题信息及答案显示

            var gradeDetail = new YssxTrainRecordDetail();
            if (progressId > 0)
            {
                gradeDetail = DbContext.FreeSql.GetRepository<YssxTrainRecordDetail>().Where(a => a.QuestionId == questionId && a.ProgressId == progressId)
                    .First(a => new YssxTrainRecordDetail { Status = a.Status, Answer = a.Answer, AnswerCompareInfo = a.AnswerCompareInfo });
            }
            //实训结束(通关)
            if (progress != null)
            {
                if (progress.TrainType == TrainType.End || isView)
                {
                    if (gradeDetail != null)
                    {
                        questionDTO.AnswerCompareInfo = gradeDetail.AnswerCompareInfo;
                        questionDTO.AnswerResultStatus = gradeDetail.Status;
                    }
                }
                else
                {
                    questionDTO.Hint = null;
                    questionDTO.FullContent = null;
                    questionDTO.AnswerValue = null;
                    questionDTO.AnswerCompareInfo = null;

                    if (questionDTO.SubQuestion != null && questionDTO.SubQuestion.Count > 0)
                    {
                        questionDTO.SubQuestion.ForEach(a =>
                        {
                            a.Hint = null;
                            a.FullContent = null;
                            a.AnswerValue = null;
                            a.AnswerCompareInfo = null;
                        });
                    }

                    questionDTO.AnswerResultStatus = gradeDetail == null ? AnswerResultStatus.None : AnswerResultStatus.Pending;
                }
            }
            
            questionDTO.StudentAnswer = gradeDetail?.Answer;

            #endregion

            result.Data = questionDTO;
            result.Msg = "操作成功";
            result.Code = CommonConstants.SuccessCode;

            return result;
        }

        #endregion

        #region 提交答案

        /// <summary>
        /// 提交答案--接口方法 （场景实训暂时单独弄一版，后期在考虑各个做题模块的共性，公共的部分抽离出来）
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <returns>作答成功信息</returns>
        public async Task<ResponseContext<QuestionResult>> SubmitAnswer(QuestionAnswer model,long caseId, ExercisesType exercisesType,UserTicket userTicket)
        {
            var result = new ResponseContext<QuestionResult>(CommonConstants.BadRequest, "");
            var user = userTicket;

            #region 校验

            if (model.GradeId == 0 || model.QuestionId == 0)
            {
                result.Code = CommonConstants.BadRequest;
                result.Msg = "GradeId,QuestionId都不能为0";
                return result;
            }

            var grade = await DbContext.FreeSql.GetRepository<YssxProgress>().Where(s => s.Id == model.GradeId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

            if (grade == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "没找到实训记录";
                return result;
            }

            if (grade.TrainType == TrainType.End)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = $"考试已结束！";
                return result;
            }

            var question = await DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.Id == model.QuestionId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync($"{CommonConstants.Cache_GetPublicTopicById}{model.QuestionId}", true, 10 * 60, true);
            if (question == null)
            {
                result.Code = CommonConstants.NotFund;
                result.Msg = "题目不存在";
                return result;
            }


            //var validateQuestion = await ValidateQuestion(exam, grade, user, question, model);

            #endregion

            switch (exercisesType)
            {
                case ExercisesType.Training:

                    return await SubmitTrainingAnswer(model, caseId,grade, question, user);

                case ExercisesType.CourceExercises:
                    throw new NotImplementedException($"课程练习待实现");
                default:

                    throw new NotImplementedException($"{exercisesType}类型不存在");
            }
        }

        /// <summary>
        /// 提交答案--自主练习
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="question"></param>
        /// <param name="user"></param>
        /// <returns>作答成功信息</returns>
        private async Task<ResponseContext<QuestionResult>> SubmitTrainingAnswer(QuestionAnswer answer, long masterId,YssxProgress grade, YssxTopicPublic question, UserTicket user)
        {
            return await SubmitAnswerBase(answer,masterId ,question, user);
        }
        /// <summary>
        /// 提交答案基础函数
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <param name="exam"></param>
        /// <param name="question"></param>
        /// <param name="user"></param>
        /// <returns>作答成功信息</returns>
        private async Task<ResponseContext<QuestionResult>> SubmitAnswerBase(QuestionAnswer answer,long masterId, YssxTopicPublic question, UserTicket user)
        {
            var result = new ResponseContext<QuestionResult>() { Data = new QuestionResult() { QuestionId = question.Id } };
            try
            {
                //
                //List<YssxCourseCertificateDataRecord> AddAccountEntryList(QuestionAnswer ans, long detailId)
                //{
                //    var entryList = new List<YssxCourseCertificateDataRecord>();
                //    if (ans.AccountEntryList != null && ans.AccountEntryList.Any())
                //    {
                //        entryList = ans.AccountEntryList.Select(a => a.MapTo<YssxCourseCertificateDataRecord>()).ToList();
                //        entryList.ForEach(a =>
                //        {
                //            a.Id = IdWorker.NextId();
                //            //a.ExamId = exam.Id;
                //            a.GradeId = answer.GradeId;
                //            a.GradeDetailId = detailId;
                //            a.QuestionId = ans.QuestionId;
                //            a.MasterId = masterId;
                //            a.TenantId = user.TenantId;

                //            a.BorrowAmount = a.BorrowAmount;
                //            a.CreditorAmount = a.CreditorAmount;
                //        });
                //    }

                //    return entryList;
                //}

                #region 提交答案

                //var accountEntryList = new List<YssxCourseCertificateDataRecord>();

                result = RedisLock.SkipLock(user.Id.ToString(), () =>
                {
                    var lockResult = new ResponseContext<QuestionResult>() { Data = new QuestionResult() };

                    try
                    {
                        var gradeDetailList = DbContext.FreeSql.GetRepository<YssxTrainRecordDetail>().Where(a => a.ProgressId == answer.GradeId && a.ParentQuestionId == answer.QuestionId).ToList(a => new { a.Id, a.QuestionId, a.Answer });
                        if (gradeDetailList == null || gradeDetailList.Count == 0)
                        {
                            #region 插入作答信息

                            var detailInsertList = new List<YssxTrainRecordDetail>();

                            var mainDetail = new YssxTrainRecordDetail()
                            {
                                Id = IdWorker.NextId(),
                                ProgressId = answer.GradeId,
                                //ExamId = exam.Id,
                                TenantId = user.TenantId,
                                QuestionId = answer.QuestionId,
                                ParentQuestionId = answer.QuestionId,
                                //PostionId = question.PositionId,
                                UserId = user.Id,
                                StudentId = CommonConstants.StudentId,
                                Answer = answer.AnswerValue,
                                CreateTime = DateTime.Now
                            };
                            answer.GradeDetailId = mainDetail.Id;

                            if (question.QuestionType == QuestionType.MainSubQuestion)//多题型 
                            {
                                var multiQuestions = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.ParentId == question.Id && s.IsDelete == CommonConstants.IsNotDelete)
                                    .OrderBy(s => s.Sort)
                                    .ToListAsync($"{CommonConstants.Cache_GetSubPublicTopicByParentId}{question.Id}", true, 10 * 60, true).Result
                                    .Select(s =>
                                    new SubQuestionDto()
                                    {
                                        Id = s.Id,
                                        QuestionType = s.QuestionType,
                                    });

                                foreach (var item in answer.MultiQuestionAnswers)
                                {
                                    var subQuestion = multiQuestions.FirstOrDefault(s => s.Id == item.QuestionId);
                                    var subDetail = new YssxTrainRecordDetail()
                                    {
                                        Id = IdWorker.NextId(),
                                        TenantId = user.TenantId,
                                        //ExamId = exam.Id,
                                        ProgressId = answer.GradeId,
                                        QuestionId = item.QuestionId,
                                        //PostionId = subQuestion.PositionId,
                                        UserId = user.Id,
                                        StudentId = CommonConstants.StudentId,
                                        Answer = item.AnswerValue,
                                        ParentQuestionId = answer.QuestionId,
                                        CreateTime = DateTime.Now
                                    };

                                    item.GradeDetailId = subDetail.Id;

                                    #region 分录题特殊处理

                                    if (subQuestion.QuestionType == QuestionType.AccountEntry)//分录题，插入凭证
                                    {
                                        //subDetail.AccountEntryAuditStatus = item.AccountEntryAuditStatus;
                                        //subDetail.AccountEntryStatus = item.AccountEntryStatus;
                                        //accountEntryList.AddRange(AddAccountEntryList(item, subDetail.Id));
                                    }

                                    #endregion

                                    detailInsertList.Add(subDetail);

                                }
                            }
                            else if (question.QuestionType == QuestionType.AccountEntry)//分录题特殊处理
                            {
                                //mainDetail.AccountEntryAuditStatus = answer.AccountEntryAuditStatus;
                                //mainDetail.AccountEntryStatus = answer.AccountEntryStatus;
                                //accountEntryList = AddAccountEntryList(answer, mainDetail.Id);
                            }

                            detailInsertList.Add(mainDetail);

                            DbContext.FreeSql.Transaction(() =>
                            {
                                DbContext.FreeSql.GetRepository<YssxTrainRecordDetail>().Insert(detailInsertList);
                                //if (accountEntryList.Any())
                                //{
                                //    DbContext.FreeSql.GetRepository<YssxCourseCertificateDataRecord>().Insert(accountEntryList);
                                //}

                                //if (question.QuestionType == QuestionType.SettleAccounts)//结账题特殊处理
                                //{
                                //    DbContext.FreeSql.GetRepository<ExamStudentGrade>().UpdateDiy
                                //    .UpdateColumns(a => new { a.UpdateTime, a.IsSettled })
                                //    .Set(a => a.UpdateTime, DateTime.Now)
                                //    .Set(a => a.IsSettled, true).Where(a => a.Id == answer.GradeId).ExecuteAffrows();
                                //}
                            });

                            #endregion
                        }
                        else
                        {
                            #region 更新作答信息

                            foreach (var detail in gradeDetailList)
                            {
                                if (detail.QuestionId == answer.QuestionId)
                                {
                                    answer.GradeDetailId = detail.Id;
                                    if (question.QuestionType == QuestionType.MainSubQuestion)//多题型
                                    {
                                        var questionIds = gradeDetailList.Select(s => s.QuestionId).Where(s => s != answer.QuestionId);

                                        foreach (var item in gradeDetailList)
                                        {
                                            if (item.QuestionId == question.Id)
                                            {
                                                continue;
                                            }

                                            var dtoDetail = answer.MultiQuestionAnswers.FirstOrDefault(a => a.QuestionId == item.QuestionId);
                                            if (dtoDetail == null || dtoDetail.QuestionId == 0)
                                            {
                                                continue;
                                            }
                                            if (dtoDetail.IsIgnore)
                                            {
                                                dtoDetail.AnswerValue = item.Answer;
                                            }
                                            dtoDetail.GradeDetailId = item.Id;

                                            #region 分录题特殊处理

                                            //if (dtoDetail.QuestionType == QuestionType.AccountEntry)//分录题，插入凭证
                                            //{
                                            //    accountEntryList.AddRange(AddAccountEntryList(dtoDetail, item.Id));
                                            //}

                                            #endregion
                                        }

                                        //先删除之前录入凭证数据
                                        //if (accountEntryList.Any())
                                        //{
                                        //    DbContext.FreeSql.Transaction(() =>
                                        //    {
                                        //        DbContext.FreeSql.GetRepository<YssxCourseCertificateDataRecord>().Delete(a => a.GradeId == answer.GradeId && questionIds.Contains(a.QuestionId));
                                        //        DbContext.FreeSql.GetRepository<YssxCourseCertificateDataRecord>().Insert(accountEntryList);
                                        //    });
                                        //}
                                    }
                                    else
                                    {
                                        #region 结账题特殊处理（有答题记录时取消结账--即删除答题记录）

                                        //if (question.QuestionType == QuestionType.SettleAccounts)
                                        //{
                                        //    //删除答案
                                        //    DbContext.FreeSql.GetRepository<ExamStudentGradeDetail>().Delete(s => s.Id == detail.Id);
                                        //    DbContext.FreeSql.GetRepository<ExamStudentGrade>().UpdateDiy
                                        //    .UpdateColumns(a => new { a.UpdateTime, a.IsSettled })
                                        //    .Set(a => a.UpdateTime, DateTime.Now)
                                        //    .Set(a => a.IsSettled, false).Where(a => a.Id == answer.GradeId).ExecuteAffrows();

                                        //    //zhcc  20190824
                                        //    var questionResult = new QuestionResult()
                                        //    {
                                        //        No = question.Sort,
                                        //        QuestionId = question.Id,
                                        //        AnswerResult = AnswerResultStatus.None
                                        //    };
                                        //    lockResult.Data = questionResult;
                                        //    return lockResult;
                                        //}

                                        #endregion

                                        #region 分录题：录入凭证存储更新（用于账簿查询）

                                        //if (question.QuestionType == QuestionType.AccountEntry)
                                        //{
                                        //    accountEntryList = AddAccountEntryList(answer, detail.Id);
                                        //}

                                        #endregion

                                        //if (accountEntryList.Any())
                                        //{
                                        //    DbContext.FreeSql.Transaction(() =>
                                        //    {
                                        //        //先删除之前录入凭证数据
                                        //        DbContext.FreeSql.GetRepository<YssxCourseCertificateDataRecord>().Delete(a => a.GradeId == answer.GradeId && a.QuestionId == answer.QuestionId);
                                        //        DbContext.FreeSql.GetRepository<YssxCourseCertificateDataRecord>().Insert(accountEntryList);
                                        //    });
                                        //}
                                    }
                                    break;
                                }
                            }
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        lockResult.Code = CommonConstants.ErrorCode;
                        lockResult.Msg = $"提交答案失败";
                        CommonLogger.Error($"答题异常", ex);
                    }

                    return lockResult;

                }, timeoutSeconds: 5);

                if (result == null || result.Code != CommonConstants.SuccessCode)
                {
                    return result;
                }

                if (result.Data.QuestionId > 0)//取消结账，直接返回
                {
                    goto End;
                }

                #endregion

                //TODO:计算题目分数
                var task = Task.Run(() =>
                {
                    var compareRst = CompareTopicAnswer(question, answer);

                    return compareRst;
                }).ConfigureAwait(false);
                result.Data = await task;

                //if (exam.ExamType != ExamType.PracticeTest && exam.CompetitionType == CompetitionType.TeamCompetition)
                //{
                //if (question.QuestionType == QuestionType.MainSubQuestion)
                //{
                //    result.Data.AccountEntryStatusDic = answer.MultiQuestionAnswers.Where(s => s.QuestionType == QuestionType.AccountEntry).Select(s => new Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>(s.QuestionId, s.AccountEntryStatus, s.AccountEntryAuditStatus, s.Sort)).ToList();
                //}
                //else if(question.QuestionType== QuestionType.AccountEntry)
                //{
                //    result.Data.AccountEntryStatusDic = new List<Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>>() { new Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>(answer.QuestionId, answer.AccountEntryStatus, answer.AccountEntryAuditStatus, answer.Sort) };
                //}
                //}

                result.Msg = "成功提交答案";

            }
            catch (Exception ex)
            {
                result.Msg = "系统异常";
                result.Code = CommonConstants.ErrorCode;
                var msg = $"答题异常:progressid-{answer.GradeId},QuestionId-{answer.QuestionId},UserId:{user.Id}";
                CommonLogger.Error(msg + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, ex);
            }

        End:
            return result;
        }

        #endregion
        #region 题目算分相关方法

        /// <summary>
        /// 比较答案
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="answer"></param>
        /// <param name="caseModel"></param>
        /// <returns></returns>
        private QuestionResult CompareTopicAnswer(YssxTopicPublic topic, QuestionAnswer answer)
        {
            var examResultDto = new QuestionResult { AnswerResult = AnswerResultStatus.Error, No = answer.Sort, QuestionId = answer.QuestionId };
            if (topic.QuestionType != QuestionType.MainSubQuestion && string.IsNullOrEmpty(answer.AnswerValue))
            {
                UpdateScores(answer, examResultDto);
                return examResultDto;
            }
            if (topic.QuestionType == QuestionType.SettleAccounts)//结账题特殊处理，直接得分
            {
                examResultDto.AnswerResult = AnswerResultStatus.Right;
                examResultDto.AnswerScore = topic.Score;
                UpdateScores(answer, examResultDto);
                return examResultDto;
            }
            var score = topic.Score;
            var standardAnswerVal = HttpUtility.UrlDecode(topic.AnswerValue);
            var answerTheValue = HttpUtility.UrlDecode(answer.AnswerValue);
            AnswerResultStatus status;
            decimal answerScore = 0;
            switch (topic.QuestionType)
            {
                case QuestionType.SingleChoice:
                case QuestionType.Judge:
                    status = answerTheValue == standardAnswerVal ? AnswerResultStatus.Right : AnswerResultStatus.Error;
                    answerScore = status == AnswerResultStatus.Right ? score : 0m;
                    examResultDto.AnswerResult = status;
                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.MultiChoice:
                    var standardVal = standardAnswerVal.ToLower().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    var answerVal = answerTheValue.ToLower().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    status = CommonMethod.CompareArrayElement(standardVal, answerVal) ? AnswerResultStatus.Right : AnswerResultStatus.Error;

                    answerScore = status == AnswerResultStatus.Right ? score : 0m;
                    examResultDto.AnswerResult = status;
                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.AccountEntry:
                case QuestionType.GridFillBank:
                case QuestionType.FillBlank:
                case QuestionType.FillGrid:
                case QuestionType.FillGraphGrid:
                case QuestionType.FinancialStatements:
                    var standardJObj = JsonHelper.DeserializeObject<Answer>(standardAnswerVal);
                    var answerJObj = JsonHelper.DeserializeObject<Answer>(answerTheValue);
                    var answerCountInfo = AnswerCompareHelperV2.Compare(standardJObj, answerJObj, 500, 600);

                    //var caseSet = new YssxCase();
                    //if (topic.QuestionType == QuestionType.AccountEntry)//分录题获取非完整性得分及分录题占比设置
                    //{
                    //    //caseSet = DbContext.FreeSql.GetRepository<YssxCase>().Where(s => s.Id == topic.CaseId).FirstAsync($"{CommonConstants.Cache_GetCaseById}{topic.CaseId}", true, 10 * 60, true).Result;
                    //}
                    var scale = 0M;// topic.QuestionType == QuestionType.AccountEntry ? caseSet.AccountEntryScale : 0M;
                    switch (topic.CalculationType)
                    {
                        case CalculationType.None:
                        case CalculationType.AvgCellCount:
                            var aroundScored = 0m;
                            int aroundVaildCount = answerCountInfo.HeaderCountInfo.ValidCount + answerCountInfo.FooterCountInfo.ValidCount;
                            int aroundRightCount = answerCountInfo.HeaderCountInfo.RightCount + answerCountInfo.FooterCountInfo.RightCount;
                            if (aroundVaildCount != 0)//只有分录题才有外围
                            {
                                var aroundPercent = 0m;
                                //if (caseSet.SetType == AccountEntryCalculationType.SetAll)//案例设置为“统一设置权重”
                                //{
                                //    aroundPercent = Math.Round((decimal)caseSet.OutVal / (decimal)(caseSet.InVal + caseSet.OutVal), 4, MidpointRounding.AwayFromZero);
                                //}
                                //else//案例设置为“单独设置权重”
                                //{
                                    var certificateTopic = DbContext.FreeSql.GetRepository<YssxCertificateTopicPublic>().Where(s => s.TopicId == topic.Id).FirstAsync($"{CommonConstants.Cache_GetCertificateTopicPublicByTopicIdAll}{topic.Id}", true, 10 * 60).Result;
                                    aroundPercent = Math.Round((decimal)certificateTopic.OutVal / (decimal)(certificateTopic.InVal + certificateTopic.OutVal), 4, MidpointRounding.AwayFromZero);
                                //}
                                var aroundTotalScore = Math.Round(score * aroundPercent, 4, MidpointRounding.AwayFromZero);
                                score = score - aroundTotalScore;
                                aroundScored = aroundTotalScore / aroundVaildCount * aroundRightCount;
                            }
                            var otherValidCount = answerCountInfo.TotalCountInfo.ValidCount - aroundVaildCount;
                            var otherRightCount = answerCountInfo.TotalCountInfo.RightCount - aroundRightCount;
                            if (otherValidCount != 0)
                                answerScore = score / otherValidCount * otherRightCount;
                            answerScore = Math.Round(answerScore + aroundScored, 4, MidpointRounding.AwayFromZero);
                            break;
                        case CalculationType.FreeCalculation:
                            answerScore = answerCountInfo.TotalCountInfo.Scored;
                            break;
                        case CalculationType.CalculatedRow:
                            answerScore = CalcRowsScored(answerJObj.BodyCtrlRows, score);
                            break;
                        case CalculationType.CalculatedColumn:
                            answerScore = CalcRowsScored(answerJObj.BodyCtrlColumns, score);
                            break;
                    }
                    answerScore = GetNonholonomicScore(answerCountInfo, answerScore, scale);
                    status = answerCountInfo.TotalCountInfo.ValidCount == answerCountInfo.TotalCountInfo.RightCount ? AnswerResultStatus.Right : (answerCountInfo.TotalCountInfo.RightCount == 0 ? AnswerResultStatus.Error : AnswerResultStatus.PartRight);
                    examResultDto.AnswerCountInfo = answerCountInfo;
                    examResultDto.AnswerResultJson = HttpUtility.UrlEncode(JsonHelper.SerializeObject(answerJObj));
                    examResultDto.AnswerResult = status;
                    //if (examResultDto.AnswerResult == AnswerResultStatus.Right)
                    //{
                    //    examResultDto.AnswerScore = score;
                    //}
                    //else
                    //{
                    //    examResultDto.AnswerScore = answerScore;
                    //}

                    examResultDto.AnswerScore = answerScore;
                    break;
                case QuestionType.MainSubQuestion:
                    examResultDto = MainSubQuestionCompare(answer, examResultDto);

                    break;

            }
            UpdateScores(answer, examResultDto);
            return examResultDto;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="score"></param>
        /// <returns></returns>
        private static decimal CalcRowsScored(List<AnswerRow> rows, decimal score)
        {
            if (rows == null)
                return 0;
            var columnTotalCount = (decimal)rows.Count;
            //2019-9-7
            //var rigthColumnCount = rows.Count(m => m.Items.All(i => i.IsRight && i.IsValid));
            var rigthColumnCount = (decimal)rows.Count(m => m.Items.All(i => i.IsRight || !i.IsValid) && m.Items.Any(i => i.IsRight));
            return Math.Round(rigthColumnCount / columnTotalCount * score, 4, MidpointRounding.AwayFromZero);
        }
        /// <summary>
        /// 获取非完整得分
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="scored"></param>
        /// <param name="accountEntryScale"></param>
        /// <param name="fillGridScale"></param>
        /// <returns></returns>
        private static decimal GetNonholonomicScore(AnswerCountInfo answerCountInfo, decimal scored, decimal scale)
        {
            if (scale == 0)
                return scored;
            if (answerCountInfo.TotalCountInfo.ValidCount != answerCountInfo.TotalCountInfo.RightCount)//非完整性得分判断
            {
                return Math.Round(scored * scale / 100m, 4, MidpointRounding.AwayFromZero);
            }
            return Math.Round(scored, 4, MidpointRounding.AwayFromZero);
        }
        /// <summary>
        /// 多题型答案比较
        /// </summary>
        /// <param name="examResultDto"></param>
        private QuestionResult MainSubQuestionCompare(QuestionAnswer answer, QuestionResult examResultDto)
        {
            var allItemCount = 0;
            var rightItemCount = 0;
            var answerScore = 0m;
            examResultDto.MultiQuestionResult = new List<QuestionResult>();
            if (answer.MultiQuestionAnswers == null || answer.MultiQuestionAnswers.Count == 0)
            {
                examResultDto.AnswerResult = AnswerResultStatus.Error;
                return examResultDto;
            }

            var multiTopic = DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.ParentId == answer.QuestionId && s.IsDelete == CommonConstants.IsNotDelete)
                        .OrderBy(s => s.Sort)
                        .ToListAsync($"{CommonConstants.Cache_GetSubPublicTopicByParentId}{answer.QuestionId}", true, 10 * 60, true).Result;

            foreach (var multiItem in multiTopic)
            {
                var answerItem = answer.MultiQuestionAnswers.FirstOrDefault(m => m.QuestionId == multiItem.Id);
                if (answerItem == null)
                    continue;
                var compareRst = CompareTopicAnswer(multiItem, answerItem);
                if (compareRst.AnswerCountInfo == null)
                {
                    if (compareRst.AnswerResult == AnswerResultStatus.Right)
                        rightItemCount++;
                    allItemCount++;
                }
                else
                {
                    allItemCount += compareRst.AnswerCountInfo.TotalCountInfo.ValidCount;
                    rightItemCount += compareRst.AnswerCountInfo.TotalCountInfo.RightCount;
                }
                examResultDto.MultiQuestionResult.Add(compareRst);
            }
            if (allItemCount == 0)
            {
                examResultDto.AnswerResult = AnswerResultStatus.Error;
                return examResultDto;
            }
            answerScore = examResultDto.MultiQuestionResult.Sum(s => s.AnswerScore);
            examResultDto.AnswerResult = allItemCount == rightItemCount ? AnswerResultStatus.Right : (rightItemCount == 0 ? AnswerResultStatus.Error : AnswerResultStatus.PartRight);
            examResultDto.AnswerScore = answerScore;
            return examResultDto;
        }


        private void UpdateScores(QuestionAnswer answer, QuestionResult examResultDto)
        {
            if (answer.IsIgnore)
            {
                return;
            }

            var gradeDetailId = answer.GradeDetailId;
            if (gradeDetailId > 0)
            {
                var gradeDetail = new YssxTrainRecordDetail() { Id = gradeDetailId };
                gradeDetail.Answer = answer.AnswerValue;
                //gradeDetail.AccountEntryAuditStatus = answer.AccountEntryAuditStatus;
                //gradeDetail.AccountEntryStatus = answer.AccountEntryStatus;
                gradeDetail.UpdateTime = DateTime.Now;
                gradeDetail.Status = examResultDto.AnswerResult;
                gradeDetail.Score = examResultDto.AnswerScore;
                gradeDetail.AnswerCompareInfo = examResultDto.AnswerResultJson;

                DataMergeHelper.PushData("UpdateScores-TrainRecordDetail", gradeDetail, (arr) =>
                {
                    var details = arr.Cast<YssxTrainRecordDetail>().ToList();
                    DbContext.FreeSql.GetRepository<YssxTrainRecordDetail>().UpdateDiy.SetSource(details)
                  .UpdateColumns(a => new { a.Status, a.Score, a.AnswerCompareInfo, a.Answer, a.UpdateTime }).ExecuteAffrows();
                });
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// 通关记录新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddGradeRecord(OaxGradeRecordRequestDto model)
        {
            return await Task.Run(() =>
            {
                var result = new ResponseContext<bool>(true);
                OaxGradeRecord gradeRecord = model.MapTo<OaxGradeRecord>();
                gradeRecord.Id = IdWorker.NextId();
                try
                {
                    DbContext.OaxFreeSql.Insert(gradeRecord).ExecuteAffrows();
                }
                catch (Exception ex)
                {
                    result.Data = false;
                    result.Msg = ex.Message;
                }
                return result;
            });
        }
    }
}
