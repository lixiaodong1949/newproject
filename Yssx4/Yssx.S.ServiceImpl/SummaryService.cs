﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class SummaryService:ISummaryService
    {
        public async Task<ResponseContext<SummaryDto>> AddSummary(SummaryDto model)
        {
            if (model == null) return new ResponseContext<SummaryDto> { Code = CommonConstants.ErrorCode };
            YssxSummary data = new YssxSummary();
            if (model.Id == 0)
            {
                data = await DbContext.FreeSql.GetRepository<YssxSummary>().InsertAsync(new YssxSummary { Id = IdWorker.NextId(), Summary = model.Name, EnterpriseId = model.EnterpriseId, Type = 1, Sort = model.Sort });
                if (data == null) return new ResponseContext<SummaryDto> { Code = CommonConstants.ErrorCode };
            }
            else
            {
                data.Id = model.Id;
                data.Summary = model.Name;
                data.EnterpriseId = model.EnterpriseId;
                data.Sort = model.Sort;
                int count = await DbContext.FreeSql.GetRepository<YssxSummary>().UpdateAsync(data);
            }
            return new ResponseContext<SummaryDto> { Data = new SummaryDto { Id = data.Id, Name = data.Summary, EnterpriseId = data.EnterpriseId } };
        }

        public async Task<ResponseContext<List<SummaryDto>>> GetSummaryList(long id)
        {
            var data = await DbContext.FreeSql.GetRepository<YssxSummary>().Where(x => x.EnterpriseId == id).ToListAsync();
            return new ResponseContext<List<SummaryDto>> { Data = DataToModel(data) };
        }

        public async Task<ResponseContext<bool>> RemoveSummary(long id)
        {
            bool b = await DbContext.FreeSql.GetRepository<YssxSummary>().DeleteAsync(x => x.Id == id) > 0;
            return new ResponseContext<bool> { Data = b };
        }

        #region 私有方法
        private List<SummaryDto> DataToModel(List<YssxSummary> sources)
        {
            if (sources == null) return null;
            return sources.Select(x => new SummaryDto { Id = x.Id, EnterpriseId = x.EnterpriseId, Name = x.Summary, Sort = x.Sort }).OrderBy(x => x.Sort).ToList();
        }
        #endregion
    }
}
