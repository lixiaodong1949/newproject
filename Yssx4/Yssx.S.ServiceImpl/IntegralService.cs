using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using System.Linq;
using Yssx.Repository.Extensions;
using System.Dynamic;

namespace Yssx.S.Service
{
    /// <summary>
    /// 用户积分服务
    /// </summary>
    public class IntegralService : IIntegralService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Save(IntegralDetailsDto model, long currentUserId)
        {
            return await Add(model, currentUserId);
        }

        /// <summary>
        /// 用户积分 新增
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Add(IntegralDetailsDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<YssxIntegralDetails>(e => e.IsDelete == CommonConstants.IsNotDelete);

            //获取用户当前积分的总分
            var usableIntegral = await DbContext.FreeSql.GetRepository<YssxIntegral>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.UserId == model.UserId)
                .FirstAsync(e => e.Number);

            #region 参数校验
            //var hasName = await repo
            //             .Where(e => e.Name == model.Name)
            //             .AnyAsync();

            //if (hasName)
            //{
            //    result.Msg = $"用户积分名称重复。";
            //    return result;
            //}

            #endregion

            var obj = model.MapTo<YssxIntegralDetails>();
            obj.Id = IdWorker.NextId();//获取唯一Id
            obj.IntegralSum = usableIntegral + obj.Number;

            var yssxIntegral = DbContext.FreeSql.GetRepository<YssxIntegral>(e => e.IsDelete == CommonConstants.IsNotDelete)
                 .Where(e => e.UserId == obj.UserId)
                 .First();

            if (yssxIntegral == null)
            {
                result.Msg = $"用户积分异常。";
                return result;
            }

            yssxIntegral.Number += obj.Number;

            #region 保存到数据库中

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            {
                try
                {
                    var repo_f = uow.GetRepository<YssxIntegralDetails>();
                    var repo_i = uow.GetRepository<YssxIntegral>(e => e.IsDelete == CommonConstants.IsNotDelete);

                    await repo_f.InsertAsync(obj);//添加到数据库

                    //总分加分
                    await repo_i.UpdateDiy.Set(c => new YssxIntegral
                    {
                        Number = c.Number + (obj.StatusType == 1 ? obj.Number : -obj.Number),
                        UpdateTime = DateTime.Now,
                    }).Where(e => e.UserId == obj.UserId).ExecuteAffrowsAsync();

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    CommonLogger.Error(ex.ToString());//写错误日志
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            result.SetSuccess(true, obj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 用户积分 修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Update(IntegralDetailsDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<YssxIntegralDetails>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasobj = await repo
                   .Where(e => e.Id == model.Id)
                   .AnyAsync();

            if (!hasobj)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"用户积分不存在，Id:{model.Id}", false);

            //var hasName = await repo
            //             .Where(e => e.Name == model.Name && e.Id != model.Id)
            //             .AnyAsync();

            //if (hasName)
            //    return new ResponseContext<bool>(CommonConstants.BadRequest, $"用户积分名称重复。", false);

            #endregion
            //赋值
            var updateObj = model.MapTo<YssxIntegralDetails>();
            //updateObj.UpdateBy = currentUserId;
            //updateObj.UpdateTime = DateTime.Now;

            #region 保存到数据库中

            await repo.UpdateDiy.SetSource(updateObj).ExecuteAffrowsAsync();//修改内容

            //await repo.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_f = uow.GetRepository<YssxIntegral>();

            //        await repo_f.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

            //        //await repo_f.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}
            #endregion

            result.SetSuccess(true, updateObj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<IntegralDetailsDto>> Get(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<YssxIntegral>(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .Where(e => e.Id == id)
                      .FirstAsync<IntegralDetailsDto>();

            return new ResponseContext<IntegralDetailsDto>(entity);
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Delete(long currentUserId, long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<YssxIntegralDetails>(e => e.IsDelete == CommonConstants.IsNotDelete)
                 .Where(e => e.Id == id).FirstAsync();

            if (entity == null)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"用户积分不存在，Id:{id}", false);


            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            {
                try
                {
                    var repo_t = uow.GetRepository<YssxIntegralDetails>(e => e.IsDelete == CommonConstants.IsNotDelete);

                    var repo_i = uow.GetRepository<YssxIntegral>(e => e.IsDelete == CommonConstants.IsNotDelete);

                    //删除
                    await repo_t.UpdateDiy.Set(c => new YssxIntegralDetails
                    {
                        IsDelete = CommonConstants.IsDelete,
                    }).Where(e => e.Id == id).ExecuteAffrowsAsync();
                    //删除后总分减分
                    await repo_i.UpdateDiy.Set(c => new YssxIntegral
                    {
                        Number = c.Number - (entity.StatusType == 1 ? entity.Number : -entity.Number),
                        UpdateTime = DateTime.Now,
                    }).Where(e => e.UserId == entity.UserId).ExecuteAffrowsAsync();

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    CommonLogger.Error(ex.ToString());//写错误日志
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }

            return new ResponseContext<bool>(true);
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<IntegralPageResponseDto>> ListPage(IntegralPageRequestDto query)
        {
            long count = 0;
            var entitys = await DbContext.FreeSql.GetRepository<YssxIntegral>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .OrderByDescending(a => a.UserId)
                   .Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .From<YssxUser>((a, b) => a.LeftJoin(aa => aa.UserId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), (a, b) => b.RealName.Contains(query.Name) || b.UserName.Contains(query.Name) || b.MobilePhone.Contains(query.Name))
                   .ToListAsync((a, b) => new IntegralPageResponseDto
                   {
                       Id = a.Id,
                       Number = a.Number,
                       UserId = a.UserId,
                       RealName = b.RealName,
                       UserName = b.UserName,
                       UpdateTime = a.UpdateTime,
                       UserMobile = b.MobilePhone,
                       Sex = b.Sex,
                   });

            return new PageResponse<IntegralPageResponseDto>(query.PageIndex, query.PageSize, count, entitys);
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<IntegralListResponseDto>> List(IntegralListRequestDto query)
        {
            var entitys = await DbContext.FreeSql.GetRepository<YssxIntegral>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   //.WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderByDescending(e => e.UserId)
                   .ToListAsync<IntegralListResponseDto>();

            return new ListResponse<IntegralListResponseDto>(entitys);
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<IntegralDetailsListPageResponseDto>> ListDetailsPageByUserId(IntegralDetailsListPageRequestDto query)
        {
            long count = 0;
            var entitys = await DbContext.FreeSql.GetRepository<YssxIntegralDetails>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete && e.UserId == query.UserId)
                   .OrderByDescending(e => e.Id)
                   .Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .From<YssxUser>((a, b) => a.LeftJoin(aa => aa.UserId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                   .ToListAsync((a, b) => new IntegralDetailsListPageResponseDto
                   {
                       Id = a.Id,
                       Number = a.Number,
                       UserId = a.UserId,
                       RealName = b.RealName,
                       UserName = b.UserName,
                       CreateTime = a.CreateTime,
                       Desc = a.Desc,
                       FromType = a.FromType,
                       RelatedId = a.RelatedId,
                       StatusType = a.StatusType,
                       IntegralSum = a.IntegralSum,
                   });

            return new PageResponse<IntegralDetailsListPageResponseDto>(query.PageIndex, query.PageSize, count, entitys);
        }

        #region 前台
        /// <summary>
        /// 获取用户积分排行
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<IntegralGetTopResponseDto>> GetTop(PageRequest page)
        {
            //var entitys2 = DbContext.FreeSql.GetRepository<YssxIntegral>()
            //       .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
            //       .Limit(page.PageSize)
            //       .Offset(page.PageSize * (page.PageIndex - 1))
            //       .OrderByDescending(e => e.Number)
            //       .OrderByDescending(e => e.UpdateTime)
            //       .From<YssxUser, YssxStudent>((a, b, c) => a.InnerJoin(aa => aa.UserId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
            //                                                  .InnerJoin(aa => aa.UserId == c.UserId && c.IsDelete == CommonConstants.IsNotDelete))
            //       .ToList((a, b, c) => new IntegralGetTopResponseDto
            //       {
            //           UserId = b.Id,
            //           MobilePhone = b.MobilePhone,
            //           RealName = b.RealName,
            //           UserName = b.RealName,
            //           SchoolName = c.SchoolName,
            //           Number = a.Number
            //       });


            var entitys = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetIntegralTop}:{page.PageSize}_{page.PageIndex}",
               () =>
               {
                   //var integrals = DbContext.FreeSql.GetRepository<YssxIntegralTop>().Select
                   // .Limit(page.PageSize)
                   // .Offset(page.PageSize * (page.PageIndex - 1))
                   // .OrderByDescending(e => e.Number)
                   // .ToList(a => new IntegralGetTopResponseDto
                   // {
                   //     UserId = a.UserId,
                   //     //MobilePhone = a.MobilePhone,
                   //     UserName = a.RealName,
                   //     SchoolName = a.SchoolName,
                   //     Number = a.Number
                   // });

                   var integrals = DbContext.FreeSql.GetRepository<YssxIntegral>()
                          .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                          .Limit(page.PageSize)
                          .Offset(page.PageSize * (page.PageIndex - 1))
                          .OrderByDescending(e => e.Number)
                          .OrderByDescending(e => e.UpdateTime)
                          .From<YssxUser, YssxStudent>((a, b, c) => a.InnerJoin(aa => aa.UserId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                                                                     .InnerJoin(aa => aa.UserId == c.UserId && c.IsDelete == CommonConstants.IsNotDelete))
                          .ToList((a, b, c) => new IntegralGetTopResponseDto
                          {
                              UserId = b.Id,
                              MobilePhone = b.MobilePhone,
                              RealName = b.RealName,
                              UserName = b.RealName,
                              SchoolName = c.SchoolName,
                              Number = a.Number
                          });

                   return integrals;

               }, 10 * 60, true, false);

            return new ListResponse<IntegralGetTopResponseDto>(entitys);
        }

        /// <summary>
        /// 获取用户当前可用总积分
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> GetSum(long userid)
        {
            var sum = await DbContext.FreeSql.GetRepository<YssxIntegral>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete && e.UserId == userid)
                   .FirstAsync(e => e.Number);

            return new ResponseContext<long>(sum);
        }
        /// <summary>
        /// 获取当前用户积分总分和商品兑换总数
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<IntegralStatistics>> GetStatistics(long userid)
        {

            var entitys = await DbContext.FreeSql.GetRepository<YssxIntegral>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete && e.UserId == userid).FirstAsync();

            var sum = new IntegralStatistics();

            if (entitys != null)
            {
                sum.ProductExchangeSum = entitys.ProductExchangeNumber;
                sum.UsedIntegralSum = entitys.UsedNumber;
                sum.UsableIntegralSum = entitys.Number;
            }

            return new ResponseContext<IntegralStatistics>(sum);
        }

        /// <summary>
        /// 获取用户的积分详情列表信息 分页
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<IntegralDetailsUserPageResponseDto>> DetailsListByUserId(IntegralDetailsUserRequestDto query)
        {
            long count = 0;
            var entitys = await DbContext.FreeSql.GetRepository<YssxIntegralDetails>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete && e.UserId == query.UserId)
                   .WhereIf(query.StatusType.HasValue, e => e.StatusType == query.StatusType)
                   .OrderByDescending(e => e.CreateTime)
                   .OrderByDescending(e => e.Id)
                   .Limit(query.PageSize)
                   .Offset(query.PageSize * (query.PageIndex - 1))
                   //.Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .ToListAsync<IntegralDetailsUserPageResponseDto>();

            return new PageResponse<IntegralDetailsUserPageResponseDto>(query.PageIndex, query.PageSize, count, entitys);
        }

        #endregion
    }
}