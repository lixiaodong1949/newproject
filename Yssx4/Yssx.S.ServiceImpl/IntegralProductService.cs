using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using System.Linq;
using Yssx.Repository.Extensions;

namespace Yssx.S.Service
{
    /// <summary>
    /// 积分商品服务
    /// </summary>
    public class IntegralProductService : IIntegralProductService
    {
        #region 后台方法

        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Save(IntegralProductDto model, long currentUserId)
        {
            if (model.Id <= 0)//新增
            {
                return await Add(model, currentUserId);
            }
            else //修改
            {
                return await Update(model, currentUserId);
            }
        }

        /// <summary>
        /// 积分商品 新增
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Add(IntegralProductDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<YssxIntegralProduct>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasName = await repo
                         .Where(e => e.Name == model.Name)
                         .AnyAsync();

            if (hasName)
            {
                result.Msg = $"积分商品名称重复。";
                return result;
            }

            #endregion

            var obj = model.MapTo<YssxIntegralProduct>();
            obj.Id = IdWorker.NextId();//获取唯一Id
            obj.CreateBy = currentUserId;
            if (obj.IsActive)
                obj.ActiveDateTime = DateTime.Now;

            #region 保存到数据库中

            await repo.InsertAsync(obj);

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_f = uow.GetRepository<YssxIntegralProduct>();

            //        await repo_f.InsertAsync(obj);//添加到数据库

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}
            #endregion

            result.SetSuccess(true, obj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 积分商品 修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Update(IntegralProductDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<YssxIntegralProduct>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasobj = await repo
                   .Where(e => e.Id == model.Id)
                   .AnyAsync();

            if (!hasobj)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"积分商品不存在，Id:{model.Id}", false);

            var hasName = await repo
                         .Where(e => e.Name == model.Name && e.Id != model.Id)
                         .AnyAsync();

            if (hasName)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"积分商品名称重复。", false);

            #endregion
            //赋值
            var updateObj = model.MapTo<YssxIntegralProduct>();
            updateObj.UpdateBy = currentUserId;
            updateObj.UpdateTime = DateTime.Now;
            if (updateObj.IsActive)
                updateObj.ActiveDateTime = DateTime.Now;

            #region 保存到数据库中

            await repo.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.ExchangeNum }).ExecuteAffrowsAsync();//修改内容

            //await repo.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_f = uow.GetRepository<YssxIntegralProduct>();

            //        await repo_f.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

            //        //await repo_f.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}
            #endregion

            result.SetSuccess(true, updateObj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<IntegralProductDto>> Get(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<YssxIntegralProduct>(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .Where(e => e.Id == id)
                      .FirstAsync<IntegralProductDto>();

            return new ResponseContext<IntegralProductDto>(entity);
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Delete(long currentUserId, params long[] id)
        {
            //删除
            await DbContext.FreeSql.GetRepository<YssxIntegralProduct>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(c => new YssxIntegralProduct
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUserId,
                }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_t = uow.GetRepository<YssxIntegralProduct>(e => e.IsDelete == CommonConstants.IsNotDelete);
            //        //删除
            //        await repo_t.UpdateDiy.Set(c => new YssxIntegralProduct
            //        {
            //            IsDelete = CommonConstants.IsDelete,
            //            UpdateTime = DateTime.Now,
            //            UpdateBy = currentUserId,
            //        }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}

            return new ResponseContext<bool>(true);
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<IntegralProductPageResponseDto>> ListPage(IntegralProductPageRequestDto query)
        {
            long count = 0;
            var entitys = await DbContext.FreeSql.GetRepository<YssxIntegralProduct>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .WhereIf(query.IsActive.HasValue, e => e.IsActive == query.IsActive)
                   .WhereIf(query.Type > 0, e => e.Type == query.Type)
                   .OrderBy(e => e.Sort)
                   .OrderByDescending(e => e.Id)
                   .Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .ToListAsync<IntegralProductPageResponseDto>();

            return new PageResponse<IntegralProductPageResponseDto>(query.PageIndex, query.PageSize, count, entitys);
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<IntegralProductListResponseDto>> List(IntegralProductListRequestDto query)
        {
            var entitys = await DbContext.FreeSql.GetRepository<YssxIntegralProduct>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderBy(e => e.Sort)
                   .OrderByDescending(e => e.Id)
                   .ToListAsync<IntegralProductListResponseDto>();

            return new ListResponse<IntegralProductListResponseDto>(entitys);
        }

        /// <summary>
        /// 上下架商品
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Active(long id, bool isActive, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "");

            var entity = await DbContext.FreeSql.GetRepository<YssxIntegralProduct>()
                            .Where(e => e.Id == id)
                            .FirstAsync<YssxIntegralProduct>();

            if (entity == null)
            {
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"积分商品不存在，Id:{id}", false);
            }

            entity.IsActive = isActive;
            entity.ActiveDateTime = isActive ? DateTime.Now : entity.ActiveDateTime;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            await DbContext.FreeSql.GetRepository<YssxIntegralProduct>()
                .UpdateDiy.SetSource(entity)
                .UpdateColumns(a => new { a.IsActive, a.ActiveDateTime, a.UpdateTime, a.UpdateBy })
                .ExecuteAffrowsAsync();//修改内容

            result.SetSuccess(true);

            return result;
        }

        #endregion

        #region 前台H5
        /// <summary>
        /// 查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<IntegralProductBaseInfoDto>> BaseListPage()
        {
            long count = 0;

            var entitys = DbContext.FreeSql.GetRepository<YssxIntegralProduct>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete && e.IsActive == true && e.Inventory > 0)
                   .OrderBy(e => e.Sort)
                   .OrderByDescending(e => e.Id)
                   .ToList<IntegralProductBaseInfoDto>();

            return new ListResponse<IntegralProductBaseInfoDto>(entitys);
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<IntegralProductInfoDto>> Info(long id)
        {
            var entity = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetIntegralProductInfo}:{id}",
                () => DbContext.FreeSql.GetRepository<YssxIntegralProduct>(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .Where(e => e.Id == id && e.IsActive == true && e.Inventory > 0)
                   .First<IntegralProductInfoDto>(), 10 * 60, true, false);

            if (entity == null)
            {
                return new ResponseContext<IntegralProductInfoDto>(CommonConstants.BadRequest, "商品已下架或库存不足。");
            }

            //实时查询商品库存
            var inventory = await DbContext.FreeSql.GetRepository<YssxIntegralProduct>(e => e.IsDelete == CommonConstants.IsNotDelete)
                 .Where(e => e.Id == id)
                 .FirstAsync(e => e.Inventory);

            entity.Inventory = inventory;

            return new ResponseContext<IntegralProductInfoDto>(entity);
        }
        #endregion
    }
}