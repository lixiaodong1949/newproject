﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Tas.Common.Configurations;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using Yssx.Framework.Logger;
using Yssx.Framework.Payment;
using Yssx.Framework.Payment.Webchat;
using Yssx.S.Dto.Mall;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Mall;
using Yssx.S.ServiceImpl.Web;

namespace Yssx.S.ServiceImpl
{
    public class PaymentServiceV2 : IPaymentServiceV2
    {
        private Yssx.Framework.Payment.Order CreateOrder(CreateMallOrderDto input, int PaymentType, UserTicket currentUser)
        {
            var targetIds = input.Items.Select(i => i.TargetId).ToList();
            List<YssxMallProductItem> yssxMallProductItems = DbContext.FreeSql.Select<YssxMallProductItem>().Where(p => targetIds.Contains(p.TargetId)).ToList();
            if (yssxMallProductItems == null || yssxMallProductItems.Count == 0)
                return null;
            var productIds = yssxMallProductItems.Select(p => p.ProductId).ToList();
            List<YssxMallProduct> yssxMallProducts = DbContext.FreeSql.Select<YssxMallProduct>().Where(p => productIds.Contains(p.Id)).ToList();

            YssxMallOrder yssxMallOrder = new YssxMallOrder();
            yssxMallOrder.Id = IdWorker.NextId();
            yssxMallOrder.UserId = currentUser.Id;
            yssxMallOrder.PaymentType = PaymentType;
            yssxMallOrder.Source = 3;
            yssxMallOrder.Status = 0;
            yssxMallOrder.OrderTotal = yssxMallProducts.Sum(p => p.Price);
            yssxMallOrder.OrderDiscount = yssxMallProducts.Sum(p => p.OldPrice) - yssxMallProducts.Sum(p => p.Price);
            DbContext.FreeSql.Insert<YssxMallOrder>(yssxMallOrder).ExecuteAffrows();

            foreach (var yssxMallProductItem in yssxMallProductItems)
            {
                YssxMallOrderItem yssxMallOrderItem = new YssxMallOrderItem();
                yssxMallOrderItem.Id = IdWorker.NextId();
                yssxMallOrderItem.OrderId = yssxMallOrder.Id;
                yssxMallOrderItem.ProductId = yssxMallProductItem.ProductId;
                yssxMallOrderItem.Quantity = input.Items.Where(i => i.TargetId == yssxMallProductItem.TargetId).First().Quantity;
                yssxMallOrderItem.UnitPrice = yssxMallProducts.Where(p => p.Id == yssxMallProductItem.ProductId).Select(p => p.Price).First();
                yssxMallOrderItem.DiscountAmount = yssxMallProducts.Where(p => p.Id == yssxMallProductItem.ProductId).Select(p => p.OldPrice).First() - yssxMallOrderItem.UnitPrice;
                DbContext.FreeSql.Insert<YssxMallOrderItem>(yssxMallOrderItem).ExecuteAffrows();
            }

            var orderName = string.Empty;
            for (int i = 0; i < yssxMallProducts.Count; i++)
            {
                orderName += yssxMallProducts[i].Name;
                if (i != yssxMallProducts.Count - 1)
                    orderName += "|";
            }

            Yssx.Framework.Payment.Order order = new Yssx.Framework.Payment.Order();
            order.OrderNo = yssxMallOrder.Id.ToString();
            order.Name = orderName;
            order.Amount = yssxMallOrder.OrderTotal.ToString();
            order.Body = orderName;
            order.ProductCode = yssxMallOrder.Id.ToString();

            return order;
        }
        /// <summary>
        /// 单产品购买
        /// </summary>
        /// <returns></returns>
        private Yssx.Framework.Payment.Order CreateOrderV2(CreateMallOrderDto input, int PaymentType, UserTicket currentUser)
        {
            //标的Id、数量、分类、来源
            var rTargetId = input.Items.Select(i => i.TargetId).FirstOrDefault();
            if (rTargetId == 0)
                return null;
            var rQuantity = input.Items.Where(i => i.TargetId == rTargetId).First().Quantity;
            var rCategory = input.Items.Where(i => i.TargetId == rTargetId).First().Category;
            var rSource = input.Items.Where(i => i.TargetId == rTargetId).First().Source;
            //产品ID
            long rProductId = 0;
            if (rCategory == 3)
            {
                rProductId = rTargetId;
            }
            else
            {
                rProductId = DbContext.FreeSql.Select<YssxMallProductItem, YssxMallProductItem>()
                    .InnerJoin((a, b) => a.ProductId == b.ProductId && b.IsDelete == CommonConstants.IsNotDelete)
                    .Where((a, b) => a.TargetId == rTargetId && a.Category == rCategory && a.Source == rSource && a.IsDelete == CommonConstants.IsNotDelete)
                    .GroupBy((a, b) => a.ProductId).Having(x => x.Count() == 1)
                    .Select(x => x.Key).FirstOrDefault();
            }
            YssxMallProduct rMallProducts = DbContext.FreeSql.Select<YssxMallProduct>().Where(p => p.Id == rProductId).First();
            if (rMallProducts == null)
                return null;

            YssxMallOrder yssxMallOrder = new YssxMallOrder();
            yssxMallOrder.Id = IdWorker.NextId();
            yssxMallOrder.UserId = currentUser.Id;
            yssxMallOrder.PaymentType = PaymentType;
            yssxMallOrder.Source = 3;
            yssxMallOrder.Status = 0;
            yssxMallOrder.OrderTotal = rMallProducts.Price;
            yssxMallOrder.OrderDiscount = rMallProducts.OldPrice - rMallProducts.Price;
            //  TODO 此处是否可以丢到消息队列处理
            DbContext.FreeSql.Insert(yssxMallOrder).ExecuteAffrows();

            YssxMallOrderItem yssxMallOrderItem = new YssxMallOrderItem();
            yssxMallOrderItem.Id = IdWorker.NextId();
            yssxMallOrderItem.OrderId = yssxMallOrder.Id;
            yssxMallOrderItem.ProductId = rProductId;
            yssxMallOrderItem.Quantity = input.Items.Where(i => i.TargetId == rTargetId).First().Quantity;
            yssxMallOrderItem.UnitPrice = rMallProducts.Price;
            yssxMallOrderItem.DiscountAmount = rMallProducts.OldPrice - rMallProducts.Price;
            DbContext.FreeSql.Insert(yssxMallOrderItem).ExecuteAffrows();
            

            Yssx.Framework.Payment.Order order = new Yssx.Framework.Payment.Order();
            order.OrderNo = yssxMallOrder.Id.ToString();
            order.Name = rMallProducts.Name;
            order.Amount = yssxMallOrder.OrderTotal.ToString();
            order.Body = rMallProducts.ShortDesc;
            order.ProductCode = yssxMallOrder.Id.ToString();

            return order;
        }


        /// <summary>
        /// 支付宝Web支付
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PayResponse>> Alipay(CreateMallOrderDto input,UserTicket currentUser)
        {
            Yssx.Framework.Payment.Order order = CreateOrderV2(input, 2, currentUser);
            if (order == null)
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, "该商品尚未上架！", null);
            var result = Payment.Alipay(order);
            PayResponse response = new PayResponse();
            if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
            {
                // 支付请求成功
                response.Url = result.Content;
                response.OrderNo = order.OrderNo;
                return new ResponseContext<PayResponse>(CommonConstants.SuccessCode, result.Msg, response);
            }
            return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, result.Msg, null);
        }
        /// <summary>
        /// 支付宝H5支付
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PayResponse>> AlipayWap(CreateMallOrderDto input, UserTicket currentUser)
        {
            Yssx.Framework.Payment.Order order = CreateOrderV2(input, 2, currentUser);
            if (order == null)
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode,"该商品尚未上架！",null);
            var result = Payment.AlipayWap(order);
            PayResponse response = new PayResponse();
            if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
            {
                // 支付请求成功
                response.Url = result.Content;
                response.OrderNo = order.OrderNo;
                return new ResponseContext<PayResponse>(CommonConstants.SuccessCode, result.Msg, response);
            }
            return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, result.Msg, null);
        }
        /// <summary>
        /// 支付宝App支付
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PayResponse>> AlipayApp(CreateMallOrderDto input, int appEnum, UserTicket currentUser)
        {
            Yssx.Framework.Payment.Order order = CreateOrderV2(input, 2, currentUser);
            if (order == null)
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode,"该商品尚未上架！",null);
            var result = new PaymentResult();
            if (appEnum == 1)
                result = Payment.AlipayApp(order);
            if (appEnum == 2)
                result = Payment.AlipayApp3d(order);
            PayResponse response = new PayResponse();
            if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
            {
                // 支付请求成功
                response.Url = result.Content;
                response.OrderNo = order.OrderNo;
                return new ResponseContext<PayResponse>(CommonConstants.SuccessCode, result.Msg, response);
            }
            return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, result.Msg, null);
        }
        /// <summary>
        /// 支付宝App支付 - 待支付订单
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PayResponse>> AlipayAppContinue(long id, long currentUserId)
        {
            var dtNow = DateTime.Now;
            var rMallOrder = DbContext.FreeSql.Select<YssxMallOrder>().Where(x => x.Id == id && x.UserId == currentUserId && x.Status == 0 && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (rMallOrder == null)
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode,"未找到该未支付订单，请刷新后重试");
            if (dtNow > rMallOrder.CreateTime.AddMinutes(30))
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, "该订单已关闭，请重新下单购买");
            var rMallOrderItem = DbContext.FreeSql.Select<YssxMallOrderItem>().Where(x => x.OrderId == id).First();
            if (rMallOrderItem == null)
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, "未找到该订单明细");
            var rMallProduct = DbContext.FreeSql.Select<YssxMallProduct>().Where(x => x.Id == rMallOrderItem.ProductId).First();
            if (rMallOrderItem == null)
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, "未找到该订单商品数据");
            Yssx.Framework.Payment.Order rOrder = new Framework.Payment.Order
            {
                OrderNo = rMallOrder.Id.ToString(),
                Name = rMallProduct.Name,
                Amount = rMallOrder.OrderTotal.ToString(),
                Body = rMallProduct.ShortDesc,
                ProductCode = rMallOrder.Id.ToString()
            };
            var result = Payment.AlipayApp(rOrder);
            PayResponse response = new PayResponse();
            if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
            {
                // 支付请求成功
                response.Url = result.Content;
                response.OrderNo = rOrder.OrderNo;
                return new ResponseContext<PayResponse>(CommonConstants.SuccessCode, result.Msg, response);
            }
            return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, result.Msg, null);
        }

        /// <summary>
        /// 支付宝异步回调
        /// </summary>
        /// <param name="input"></param>
        /// <param name="payType">1.支付宝网页 2.支付宝-云实习APP 3.支付宝-云实习3d-APP</param>
        public async void AlipayNotify(Dictionary<string, string> input, int payType)
        {
            CommonLogger.Error("开始验证支付宝回调Start");
            bool flag = false;
            if (payType == 1)
                flag = Payment.CheckAlipay(input);
            if (payType == 2)
                flag = Payment.CheckAlipayApp(input, 1);
            if (payType == 3)
                flag = Payment.CheckAlipayApp(input, 2);
            CommonLogger.Error("验证支付宝回调参数结果：" + flag);
            if (flag)
            {
                CommonLogger.Error("支付宝交易失败调试1");
                //TODO: 交易成功处理
                var orderNo = input["out_trade_no"];
                YssxMallOrder yssxMallOrder = await DbContext.FreeSql.Select<YssxMallOrder>().Where(o => o.Id == long.Parse(orderNo)).FirstAsync();
                if ("TRADE_SUCCESS".Equals(input["trade_status"], StringComparison.OrdinalIgnoreCase))
                {
                    CommonLogger.Error("支付宝交易失败调试2");
                    yssxMallOrder.Status = 2;
                    yssxMallOrder.TransactionNo = input["trade_no"];
                    yssxMallOrder.PaymentTime = DateTime.Parse(input["gmt_payment"]);
                    DbContext.FreeSql.Update<YssxMallOrder>(yssxMallOrder).SetSource(yssxMallOrder).ExecuteAffrows();

                    CommonLogger.Error("支付宝交易失败调试3");
                    // 生成试卷
                    var yssxMallProductItems = DbContext.FreeSql.Select<YssxMallOrderItem, YssxMallProductItem>()
                        .InnerJoin((a, b) => a.ProductId == b.ProductId && b.IsDelete == CommonConstants.IsNotDelete)
                        .Where((a, b) => a.OrderId == yssxMallOrder.Id && a.IsDelete == CommonConstants.IsNotDelete).ToList((a, b) => b);

                    if (yssxMallProductItems != null && yssxMallProductItems.Count > 0)
                    {
                        CommonLogger.Error("支付宝交易失败调试4");
                        foreach (var item in yssxMallProductItems)
                        {
                            AutoGenerateTestPaperDto autoGenerateTestPaperDto = new AutoGenerateTestPaperDto()
                            {
                                Category = item.Category,
                                TargetId = item.TargetId,
                                UserId = yssxMallOrder.UserId
                            };

                            if (item.Source == 1)
                            {
                                CommonLogger.Error("支付宝交易失败调试5【云实习】");
                                //云实习
                                var result = await AutoGenerateTestPaper(autoGenerateTestPaperDto);
                                CommonLogger.Error("云实习生成试卷：" + result);
                            }
                            else if (item.Source == 2)
                            {
                                //同步实训
                                try
                                {
                                    autoGenerateTestPaperDto.Category = 1;
                                    Service.CaseServiceNew caseService = new Service.CaseServiceNew();
                                    var result2 = caseService.AutoGenerateTestPaper(autoGenerateTestPaperDto).Result;
                                }
                                catch (Exception ex)
                                {
                                    CommonLogger.Error("同步实训生成试卷：" + ex.Message);
                                }
                            }
                            else if (item.Source == 3)
                            {
                                //业税财
                            }
                            else if (item.Source == 4)
                            {
                                //岗位实训
                                try
                                {
                                    CommonLogger.Error("支付宝交易失败调试5【岗位实训】");
                                    autoGenerateTestPaperDto.Category = 1;
                                    InternshipProveService ipService = new InternshipProveService();
                                    var result4 = ipService.AutoGenerateTestPaper(autoGenerateTestPaperDto).Result;
                                    CommonLogger.Error("岗位实训生成试卷：" + result4);
                                }
                                catch (Exception ex)
                                {
                                    CommonLogger.Error("岗位实训生成试卷：" + ex.Message);
                                }
                            }
                            else if (item.Source == 5)
                            {
                                //课程实训
                                CommonLogger.Error("课程实训");
                                autoGenerateTestPaperDto.Category = 1;
                            }
                            else if (item.Source == 6)
                            {
                                //经验案例
                                try
                                {
                                    autoGenerateTestPaperDto.Category = 1;
                                    UploadCaseService ucService = new UploadCaseService();
                                    var result6 = ucService.AutoGenerateTestPaper(autoGenerateTestPaperDto).Result;
                                }
                                catch (Exception ex)
                                {
                                    CommonLogger.Error("经验案例生成试卷：" + ex.Message);
                                }
                            }
                            else if (item.Source == 7)
                            {
                                //初级会计考证
                                try
                                {
                                    CommonLogger.Error("支付宝交易失败调试5【初级会计考证】");
                                    autoGenerateTestPaperDto.Category = 1;
                                }
                                catch (Exception ex)
                                {
                                    CommonLogger.Error("初级会计考证生成试卷：" + ex.Message);
                                }
                            }
                            else
                            {
                                CommonLogger.Error(string.Format("支付宝回调失败参数：orderNo[{0}] source[{1}]",  orderNo, item.Source));
                            }
                        }
                    }

                    CommonLogger.Error("支付宝交易失败调试6");
                    // 短信通知
                    var user = await DbContext.FreeSql.Select<YssxUser>().Where(u => u.Id == yssxMallOrder.UserId).FirstAsync();
                    if (!string.IsNullOrWhiteSpace(user.MobilePhone) && yssxMallOrder.Status != 2)
                    {
                        SendSms(yssxMallOrder, user);
                    }
                    CommonLogger.Error("支付宝交易失败调试7");
                }
                else
                {
                    CommonLogger.Error(string.Format("支付宝回调失败参数：trade_status[{0}] orderNo[{1}]", input["trade_status"], orderNo));
                }
            }
            else
            {
                //TODO: 交易失败处理
                CommonLogger.Error("支付宝交易失败处理：" + flag);
            }
        }

        private async void SendSms(YssxMallOrder yssxMallOrder, YssxUser user)
        {
            YssxMallOrderItem yssxMallOrderItem = DbContext.FreeSql.Select<YssxMallOrderItem>().Where(o => o.OrderId == yssxMallOrder.Id).First();
            List<YssxMallProduct> yssxNewProducts = DbContext.FreeSql.Select<YssxMallProduct>().Where(p => p.Id == yssxMallOrderItem.ProductId).ToList();

            foreach (var yssxNewProduct in yssxNewProducts)
            {
                OrderSms orderSms = new OrderSms();
                if (yssxNewProduct.Id == 1060383200968707)
                {
                    orderSms.Cname = "顶岗实习麓山煤矿";
                    orderSms.Cdate = "5月";
                    orderSms.Copen = "31日";
                }
                else if (yssxNewProduct.Id == 1060383171608577)
                {
                    orderSms.Cname = "课程实训圆梦造纸厂";
                    orderSms.Cdate = "6月";
                    orderSms.Copen = "12日";
                }
                else if (yssxNewProduct.Id == 1060383230328837)
                {
                    orderSms.Cname = "业税财融合南邦水泥厂";
                    orderSms.Cdate = "7月";
                    orderSms.Copen = "31日";
                }
                else if (yssxNewProduct.Id == 1060383259688967)
                {
                    orderSms.Cname = "企业搭配套餐";
                    orderSms.Cdate = "5月";
                    orderSms.Copen = "31日起";
                }
                string sms = JsonConvert.SerializeObject(orderSms);
                CommonLogger.Error(sms);
                var result = DysmsService.SendSms(user.MobilePhone, sms);
                CommonLogger.Error(JsonConvert.SerializeObject(result));
            }
        }

        public async Task<ResponseContext<PayResponse>> Webchat(CreateMallOrderDto input,UserTicket currentUser)
        {
            Yssx.Framework.Payment.Order order = CreateOrderV2(input, 1, currentUser);
            if (order == null)
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, "该商品尚未上架！", null);
            try
            {
                var result = Payment.Webchat(order);
                PayResponse response = new PayResponse();
                if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
                {
                    // 支付请求成功
                    response.Url = result.Content;
                    response.OrderNo = order.OrderNo;
                    return new ResponseContext<PayResponse>(CommonConstants.SuccessCode, result.Msg, response);
                }
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, result.Msg, null);
            }
            catch (Exception ex)
            {
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, ex.Message, null);
                throw;
            }
        }

        public async Task<ResponseContext<PayResponse>> WebchatWap(CreateMallOrderDto input, UserTicket currentUser)
        {
            Yssx.Framework.Payment.Order order = CreateOrderV2(input, 1, currentUser);
            if (order == null)
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, "该商品尚未上架！", null);
            try
            {
                var result = Payment.WebchatWap(order);
                PayResponse response = new PayResponse();
                if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
                {
                    // 支付请求成功
                    response.Url = result.Content;
                    response.OrderNo = order.OrderNo;
                    return new ResponseContext<PayResponse>(CommonConstants.SuccessCode, result.Msg, response);
                }
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, result.Msg, null);
            }
            catch (Exception ex)
            {
                return new ResponseContext<PayResponse>(CommonConstants.ErrorCode, ex.Message, null);
                throw;
            }
        }

        public async Task<ResponseContext<PayWechatResponse>> WechatApp(CreateMallOrderDto input, UserTicket currentUser)
        {
            Yssx.Framework.Payment.Order order = CreateOrderV2(input, 1, currentUser);
            if (order == null)
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, "该商品尚未上架！", null);
            try
            {
                var result = Payment.WechatApp(order);
                PayWechatResponse response = new PayWechatResponse();
                if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
                {
                    // 支付请求成功
                    response.OrderNo = order.OrderNo;
                    response.AppId = result.AppId;
                    response.MchId = result.MchId;
                    response.NonceStr = result.NonceStr;
                    response.Sign = result.Sign;
                    response.PrepayId = result.PrepayId;
                    response.Timestamp = result.Timestamp;
                    return new ResponseContext<PayWechatResponse>(CommonConstants.SuccessCode, result.Msg, response);
                }
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, result.Msg, null);
            }
            catch (Exception ex)
            {
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, ex.Message, null);
                throw;
            }
        }

        public async Task<ResponseContext<PayWechatResponse>> WechatApp3d(CreateMallOrderDto input, UserTicket currentUser)
        {
            Yssx.Framework.Payment.Order order = CreateOrderV2(input, 1, currentUser);
            if (order == null)
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, "该商品尚未上架！", null);
            try
            {
                var result = Payment.WechatApp3d(order);
                PayWechatResponse response = new PayWechatResponse();
                if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
                {
                    // 支付请求成功
                    response.OrderNo = order.OrderNo;
                    response.AppId = result.AppId;
                    response.MchId = result.MchId;
                    response.NonceStr = result.NonceStr;
                    response.Sign = result.Sign;
                    response.PrepayId = result.PrepayId;
                    response.Timestamp = result.Timestamp;
                    return new ResponseContext<PayWechatResponse>(CommonConstants.SuccessCode, result.Msg, response);
                }
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, result.Msg, null);
            }
            catch (Exception ex)
            {
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, ex.Message, null);
                throw;
            }
        }

        public async Task<ResponseContext<PayWechatResponse>> WechatJsapi(CreateMallOrderDto input, UserTicket currentUser)
        {
            Yssx.Framework.Payment.Order order = CreateOrderV2(input, 1, currentUser);
            if (order == null)
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, "该商品尚未上架！", null);
            try
            {
                var result = Payment.WechatJsapi(order,input.OpenId);
                PayWechatResponse response = new PayWechatResponse();
                if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
                {
                    // 支付请求成功
                    response.OrderNo = order.OrderNo;
                    response.AppId = result.AppId;
                    response.MchId = result.MchId;
                    response.NonceStr = result.NonceStr;
                    WxPayData wxPayData = new WxPayData();
                    response.Sign = result.Sign;
                    response.PrepayId = result.PrepayId;
                    response.Timestamp = result.Timestamp;
                    return new ResponseContext<PayWechatResponse>(CommonConstants.SuccessCode, result.Msg, response);
                }
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, result.Msg, null);
            }
            catch (Exception ex)
            {
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, ex.Message, null);
                throw;
            }
        }
        public async Task<ResponseContext<PayWechatResponse>> WechatAppContinue(long id, long currentUserId)
        {
            var dtNow = DateTime.Now;
            var rMallOrder = DbContext.FreeSql.Select<YssxMallOrder>().Where(x => x.Id == id && x.UserId == currentUserId && x.Status == 0 && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (rMallOrder == null)
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, "未找到该未支付订单，请刷新后重试");
            if (dtNow > rMallOrder.CreateTime.AddMinutes(30))
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, "该订单已关闭，请重新下单购买");
            var rMallOrderItem = DbContext.FreeSql.Select<YssxMallOrderItem>().Where(x => x.OrderId == id).First();
            if (rMallOrderItem == null)
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, "未找到该订单明细");
            var rMallProduct = DbContext.FreeSql.Select<YssxMallProduct>().Where(x => x.Id == rMallOrderItem.ProductId).First();
            if (rMallOrderItem == null)
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, "未找到该订单商品数据");
            Yssx.Framework.Payment.Order rOrder = new Framework.Payment.Order
            {
                OrderNo = rMallOrder.Id.ToString(),
                Name = rMallProduct.Name,
                Amount = rMallOrder.OrderTotal.ToString(),
                Body = rMallProduct.ShortDesc,
                ProductCode = rMallOrder.Id.ToString()
            };
            try
            {
                var result = Payment.WechatApp(rOrder);
                PayWechatResponse response = new PayWechatResponse();
                if ("SUCCESS".Equals(result.Code, StringComparison.OrdinalIgnoreCase))
                {
                    // 支付请求成功
                    response.OrderNo = rOrder.OrderNo;
                    response.AppId = result.AppId;
                    response.MchId = result.MchId;
                    response.NonceStr = result.NonceStr;
                    response.Sign = result.Sign;
                    response.PrepayId = result.PrepayId;
                    response.Timestamp = result.Timestamp;
                    return new ResponseContext<PayWechatResponse>(CommonConstants.SuccessCode, result.Msg, response);
                }
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, result.Msg);
            }
            catch (Exception ex)
            {
                return new ResponseContext<PayWechatResponse>(CommonConstants.ErrorCode, ex.Message);
                throw;
            }
        }

        public async void WebchatNotify(string xmlString)
        {
            bool flag = Payment.CheckWebchat(xmlString);
            CommonLogger.Info(xmlString);
            if (flag)
            {
                //TODO: 交易成功处理
                WxPayData data = new WxPayData();
                data.FromXml(xmlString);
                string orderNo = data.GetValue("out_trade_no").ToString();
                YssxMallOrder yssxMallOrder = await DbContext.FreeSql.Select<YssxMallOrder>().Where(o => o.Id == long.Parse(orderNo)).FirstAsync();

                yssxMallOrder.Status = 2;
                yssxMallOrder.TransactionNo = data.GetValue("transaction_id").ToString();
                yssxMallOrder.PaymentTime = DateTime.ParseExact(data.GetValue("time_end").ToString(), "yyyyMMddHHmmss", CultureInfo.CurrentCulture);
                DbContext.FreeSql.Update<YssxMallOrder>(yssxMallOrder).SetSource(yssxMallOrder).ExecuteAffrows();

                // 生成试卷
                var yssxMallProductItems = DbContext.FreeSql.Select<YssxMallOrderItem, YssxMallProductItem>()
                    .InnerJoin((a, b) => a.ProductId == b.ProductId && b.IsDelete == CommonConstants.IsNotDelete)
                    .Where((a, b) => a.OrderId == yssxMallOrder.Id && a.IsDelete == CommonConstants.IsNotDelete).ToList((a, b) => b);

                if (yssxMallProductItems != null && yssxMallProductItems.Count > 0)
                {
                    foreach (var item in yssxMallProductItems)
                    {
                        AutoGenerateTestPaperDto autoGenerateTestPaperDto = new AutoGenerateTestPaperDto()
                        {
                            Category = item.Category,
                            TargetId = item.TargetId,
                            UserId = yssxMallOrder.UserId
                        };
                        if (item.Source == 1)
                        {
                            //云实习
                            var result = await AutoGenerateTestPaper(autoGenerateTestPaperDto);
                            CommonLogger.Error("云实习生成试卷：" + result);
                        }
                        else if (item.Source == 2)
                        {
                            //同步实训
                            try
                            {
                                autoGenerateTestPaperDto.Category = 1;
                                Service.CaseServiceNew caseService = new Service.CaseServiceNew();
                                var result2 = caseService.AutoGenerateTestPaper(autoGenerateTestPaperDto).Result;
                            }
                            catch (Exception ex)
                            {
                                CommonLogger.Error("同步实训生成试卷：" + ex.Message);
                            }
                        }
                        else if (item.Source == 3)
                        {
                            //业税财
                        }
                        else if (item.Source == 4)
                        {
                            //岗位实训
                            try
                            {
                                autoGenerateTestPaperDto.Category = 1;
                                InternshipProveService ipService = new InternshipProveService();
                                var result4 = ipService.AutoGenerateTestPaper(autoGenerateTestPaperDto).Result;
                            }
                            catch (Exception ex)
                            {
                                CommonLogger.Error("岗位实训生成试卷：" + ex.Message);
                            }
                        }
                        else if (item.Source == 5)
                        {
                            //课程实训
                            CommonLogger.Error("课程实训");
                            autoGenerateTestPaperDto.Category = 1;
                        }
                        else if (item.Source == 6)
                        {
                            //经验案例
                            try
                            {
                                autoGenerateTestPaperDto.Category = 1;
                                UploadCaseService ucService = new UploadCaseService();
                                var result6 = ucService.AutoGenerateTestPaper(autoGenerateTestPaperDto).Result;
                            }
                            catch (Exception ex)
                            {
                                CommonLogger.Error("经验案例生成试卷：" + ex.Message);
                            }
                        }
                        else if (item.Source == 7)
                        {
                            //初级会计考证
                            try
                            {
                                autoGenerateTestPaperDto.Category = 1;
                            }
                            catch (Exception ex)
                            {
                                CommonLogger.Error("初级会计考证生成试卷：" + ex.Message);
                            }
                        }
                        else
                        {
                            CommonLogger.Error(string.Format("微信回调失败参数：orderNo[{0}] source[{1}]", orderNo, item.Source));
                        }
                    }
                }
                

                // 短信通知
                var user = await DbContext.FreeSql.Select<YssxUser>().Where(u => u.Id == yssxMallOrder.UserId).FirstAsync();
                if (!string.IsNullOrWhiteSpace(user.MobilePhone) && yssxMallOrder.Status != 2)
                {
                    SendSms(yssxMallOrder, user);
                }

                // 分账
                var amount = (int)(yssxMallOrder.OrderTotal * 0.3M);
                var account = user.WeChat;
                Receiver[] receivers = new Receiver[1] { new Receiver() {  Type = "PERSONAL_OPENID", Account = account, Amount = amount, Description = "云上实训分账"} };
                ProfitSharingOrder profitSharingOrder = new ProfitSharingOrder();
                profitSharingOrder.TransactionId = yssxMallOrder.TransactionNo;
                profitSharingOrder.OrderNo = yssxMallOrder.Id.ToString();
                profitSharingOrder.Receivers = JsonConvert.SerializeObject(receivers);
                WechatWebPay webchat = new WechatWebPay();
                webchat.ProfitSharing(profitSharingOrder);
            }
            else
            {
                //TODO: 交易失败处理
                CommonLogger.Error("微信交易失败处理：" + flag);
            }
        }

        /// <summary>
        /// 生成试卷 - 云实习
        /// </summary>
        /// <returns></returns>
        public async Task<string> AutoGenerateTestPaper(AutoGenerateTestPaperDto input)
        {
            var api = AppSettingConfig.GetSection("yssc_api");
            HttpClient client = new HttpClient();
            var requestContent = new StringContent(JsonConvert.SerializeObject(input));
            requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var response = await client.PostAsync($"{api}yssxc/Onboarding/AutoGenerateTestPaper", requestContent);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }
    }
}
