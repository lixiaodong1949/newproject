﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.Entity;
using Yssx.Framework.AutoMapper;
using Yssx.Repository.Extensions;
using Yssx.S.Poco;
using Zdap.MQ.Produce;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 课程上课服务实现
    /// </summary>
    public class TeachCourseService : ITeachCourseService
    {
        #region 添加教师上课
        /// <summary>
        /// 添加教师上课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> AddTeacherTeachCourse(TeacherTeachCourseRequestModel dto, UserTicket user)
        {
            if (dto == null || dto.ClassList == null || dto.ClassList.Count == 0)
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "数据不允许为空!" };

            if (dto.ClassList.Distinct().Count() != dto.ClassList.Count)
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "班级不能重复!" };

            //YssxTeacherTeachCourse data = await DbContext.FreeSql.GetRepository<YssxTeacherTeachCourse>().Where(x => x.CreateBy == user.Id &&
            //    x.TeachCourseStatus == TeachCourseStatus.Started && x.CourseId == dto.CourseId).FirstAsync();
            //if (data != null)
            //    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Data = 0, Msg = "需先结束进行中的上课!" };

            YssxTeacherTeachCourse entity = new YssxTeacherTeachCourse
            {
                Id = IdWorker.NextId(),
                CourseId = dto.CourseId,
                CoursePrepareId = dto.CoursePrepareId,
                TeachCourseName = dto.TeachCourseName,
                TeachCourseStatus = TeachCourseStatus.Started,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now,
                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                TenantId = user.TenantId,
            };

            //上课班级列表
            List<YssxTeachCourseClass> classList = new List<YssxTeachCourseClass>();
            dto.ClassList.ForEach(x =>
            {
                YssxTeachCourseClass classData = new YssxTeachCourseClass
                {
                    Id = IdWorker.NextId(),
                    CourseId = dto.CourseId,
                    CoursePrepareId = dto.CoursePrepareId,
                    TeachCourseId = entity.Id,
                    ClassId = x.ClassId,
                    ClassName = x.ClassName,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    TenantId = user.TenantId,
                };

                classList.Add(classData);
            });

            #region 新增数据
            bool state = false;
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //课程上课
                    var teachResult = await uow.GetRepository<YssxTeacherTeachCourse>().InsertAsync(entity);
                    //课程上课班级表
                    var classResult = await uow.GetRepository<YssxTeachCourseClass>().InsertAsync(classList);

                    state = teachResult != null && classResult != null;

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }
            #endregion

            return new ResponseContext<long> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = entity.Id, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取教师课程上课列表
        /// <summary>
        /// 获取教师课程上课列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<TeachCourseTeacherViewModel>> GetTeachCourseTeacherList(TeacherTeachCourseQuery query, UserTicket user)
        {
            var result = new PageResponse<TeachCourseTeacherViewModel>();
            if (query == null)
                return result;

            var select = DbContext.FreeSql.GetRepository<YssxTeacherTeachCourse>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.CourseId == query.CourseId && a.CreateBy == user.Id);

            if (query.TeachCourseStatus.HasValue)
                select.Where(a => a.TeachCourseStatus == query.TeachCourseStatus);
            if (!string.IsNullOrEmpty(query.TeachName))
                select.Where(a => a.TeachCourseName.Contains(query.TeachName));

            var totalCount = select.ToList().Count();

            var items = select.OrderByDescending(a => a.CreateTime).Page(query.PageIndex, query.PageSize)
                .ToList(x => new TeachCourseTeacherViewModel
                {
                    Id = x.Id,
                    CoursePrepareId = x.CoursePrepareId,
                    TeachCourseName = x.TeachCourseName,
                    TeachCourseStatus = x.TeachCourseStatus,
                    TeachDate = x.CreateTime,
                });

            //上课Ids
            List<long> teachIds = items.Select(x => x.Id).ToList();

            //获取上课班级列表
            var classList = await DbContext.FreeSql.Select<YssxTeachCourseClass>().From<YssxClass>(
                (a, b) => a.InnerJoin(x => x.ClassId == b.Id))
                .Where((a, b) => teachIds.Contains(a.TeachCourseId) && a.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new TeachCourseClassViewModel
                {
                    TeachCourseId = a.TeachCourseId,
                    ClassId = b.Id,
                    ClassName = b.Name,
                });

            items.ForEach(x =>
            {
                List<TeachCourseClassViewModel> classes = classList.Where(a => a.TeachCourseId == x.Id).ToList();

                //赋值班级列表
                x.ClassList = classes;
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 教师手动结束上课
        /// <summary>
        /// 教师手动结束上课
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> FinishTeacherTeachCourse(long id, UserTicket user)
        {
            //获取上课
            YssxTeacherTeachCourse entity = await DbContext.FreeSql.GetRepository<YssxTeacherTeachCourse>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            //获取上课中 未结束课堂测验列表
            List<YssxCourseTestTask> testList = await DbContext.FreeSql.GetRepository<YssxCourseTestTask>().Where(x => x.TeachCourseId == entity.Id &&
                x.Status != LessonsTaskStatus.End).ToListAsync();

            testList.ForEach(x =>
            {
                x.Status = LessonsTaskStatus.End;
                x.UpdateBy = user.Id;
                x.UpdateTime = DateTime.Now;
            });

            bool state = false;
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //修改上课
                    bool taskStatus = await uow.GetRepository<YssxTeacherTeachCourse>().UpdateDiy
                        .Set(x => x.TeachCourseStatus == TeachCourseStatus.End)
                        .Set(x => x.UpdateBy == user.Id)
                        .Set(x => x.UpdateTime == DateTime.Now)
                        .Where(x => x.Id == id).ExecuteAffrowsAsync() > 0;

                    //修改未结束的课堂测验
                    if (testList.Count > 0)
                        uow.GetRepository<YssxCourseTestTask>().UpdateDiy.SetSource(testList).UpdateColumns(x => new
                        {
                            x.Status,
                            x.UpdateTime,
                            x.UpdateBy
                        }).ExecuteAffrows();

                    if (taskStatus)
                    {
                        uow.Commit();
                        state = true;
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            testList.ForEach(x =>
            {
                //结束测验任务 发布消息
                RedisHelper.Publish("coursetesttaskid", x.Id.ToString());
            });

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "操作成功" : "操作失败！" };
        }
        #endregion

        #region 删除课程上课(根据主键)
        /// <summary>
        /// 删除课程上课(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTeachCourseById(long id, UserTicket user)
        {
            YssxTeacherTeachCourse entity = await DbContext.FreeSql.GetRepository<YssxTeacherTeachCourse>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<YssxTeacherTeachCourse>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取课中上课资源列表(文件、题目)【用于上课】
        /// <summary>
        /// 获取课中上课资源列表(文件、题目)【用于上课】
        /// </summary>
        /// <param name="couPrepareId">课程备课Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<CourseTestResourceViewModel>> GetCourseTestResourceList(long couPrepareId)
        {
            ResponseContext<CourseTestResourceViewModel> response = new ResponseContext<CourseTestResourceViewModel>();

            CourseTestResourceViewModel data = new CourseTestResourceViewModel();

            data.FileList = await DbContext.FreeSql.GetRepository<YssxCoursePrepareFiles>()
                .Where(x => x.CoursePrepareId == couPrepareId && x.ResourceBelong == CoursePrepareResourcesBelong.CourseInClass && x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new CourseTestFileViewModel
                {
                    Id = x.Id,
                    CourseFileId = x.CourseFileId,
                    FileName = x.FileName,
                    File = x.File,
                    FilesType = x.FilesType,
                    ResourceType = x.ResourceType,
                    IsFile = x.IsFile,
                    KnowledgePointNames = x.KnowledgePointNames,
                    Remark = x.Remark,
                    Sort = x.Sort,
                    CreateTime = x.CreateTime
                });

            data.TopicList = await DbContext.FreeSql.GetRepository<YssxCoursePrepareTopic>()
                .Where(x => x.CoursePrepareId == couPrepareId && x.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(x => new CourseTestTopicViewModel
                {
                    Id = x.Id,
                    QuestionId = x.QuestionId,
                    QuestionName = x.QuestionName,
                    Score = x.Score,
                    QuestionType = x.QuestionType,
                    KnowledgePointNames = x.KnowledgePointNames,
                    TopicSource = x.TopicSource,
                    CourseOrCaseId = x.CourseOrCaseId,
                    CourseOrCaseName = x.CourseOrCaseName,
                    ModuleType = x.ModuleType,
                    Sort = x.Sort,
                    CreateTime = x.CreateTime
                });

            response.Data = data;
            return response;
        }
        #endregion

        #region 删除课程备课题目(根据主键)
        /// <summary>
        /// 删除课程备课题目(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCoursePrepareTopicById(long id, UserTicket user)
        {
            YssxCoursePrepareTopic entity = await DbContext.FreeSql.GetRepository<YssxCoursePrepareTopic>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = await DbContext.FreeSql.GetRepository<YssxCoursePrepareTopic>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
            {
                x.IsDelete,
                x.UpdateTime,
                x.UpdateBy
            }).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 发布课堂测验任务
        /// <summary>
        /// 发布课堂测验任务
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> PublishCourseTestTask(CourseTestTaskRequestModel dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };
            bool state = false;

            if (dto.TopicList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "题目为空,请选择测验题目!" };

            if (dto.ClassList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "班级为空,请选择测验班级!" };

            DateTime dateTimeNow = DateTime.Now;

            //测验任务
            YssxCourseTestTask entity = new YssxCourseTestTask
            {
                Id = IdWorker.NextId(),
                TaskName = dto.TaskName,
                StartTime = dateTimeNow,
                TaskType = TaskType.ClassTest,
                CourseId = dto.CourseId,
                TeachCourseId = dto.TeachCourseId,
                Status = LessonsTaskStatus.Started,
                TaskSource = dto.TaskSource,
                CourseOrCaseId = dto.CourseOrCaseId,
                ModuleType = dto.ModuleType,
                TotalScore = dto.TopicList.Sum(x => x.Score),
                IsCanCheckExam = true,
                CreateBy = user.Id,
                CreateTime = dateTimeNow,
                UpdateBy = user.Id,
                UpdateTime = dateTimeNow,
                TenantId = user.TenantId,
            };

            return await Task.Run(() =>
            {
                #region 初始化数据

                //测验题目
                List<YssxCourseTestTopic> topicList = new List<YssxCourseTestTopic>();
                //生成试卷 (一道题生成一张试卷)
                List<ExamPaperCourse> examPaperList = new List<ExamPaperCourse>();
                //测验班级
                List<YssxCourseTestTaskClass> classList = new List<YssxCourseTestTaskClass>();
                //学生主作答记录
                List<ExamCourseUserGrade> studentGradeList = new List<ExamCourseUserGrade>();

                //添加测验任务、题目关联 生成试卷
                if (dto.TopicList != null && dto.TopicList.Count > 0)
                {
                    dto.TopicList.ForEach(x =>
                    {
                        YssxCourseTestTopic modelTopic = new YssxCourseTestTopic();
                        modelTopic.Id = IdWorker.NextId();
                        modelTopic.TeachCourseId = entity.TeachCourseId;
                        modelTopic.TestTaskId = entity.Id;
                        modelTopic.TopicId = x.TopicId;
                        modelTopic.QuestionType = x.QuestionType;
                        modelTopic.Score = x.Score;
                        modelTopic.TopicSource = x.TopicSource;
                        modelTopic.CreateBy = user.Id;
                        modelTopic.CreateTime = dateTimeNow;
                        modelTopic.UpdateBy = user.Id;
                        modelTopic.UpdateTime = dateTimeNow;
                        modelTopic.TenantId = user.TenantId;

                        topicList.Add(modelTopic);
                    });

                    if (topicList != null && topicList.Count > 0)
                    {
                        topicList.ForEach(x =>
                        {
                            //创建试卷                            
                            ExamPaperCourse examPaper = new ExamPaperCourse
                            {
                                Id = IdWorker.NextId(),
                                Name = dto.TaskName,
                                TaskId = entity.Id,
                                CourseId = dto.CourseId,
                                OriginalCourseId = dto.CourseId,
                                ExamType = CourseExamType.ClassTest,
                                CanShowAnswerBeforeEnd = true,
                                TotalScore = x.Score,
                                TotalQuestionCount = 1,
                                BeginTime = dateTimeNow,
                                ReleaseTime = dateTimeNow,
                                IsRelease = true,
                                Status = ExamStatus.Started,
                                CreateBy = user.Id,
                                CreateTime = dateTimeNow,
                                UpdateBy = user.Id,
                                UpdateTime = dateTimeNow,
                                TenantId = user.TenantId,
                            };

                            examPaperList.Add(examPaper);

                            if (x.TopicSource == CoursePrepareTopicSource.Case)
                            {
                                List<CourseTestTaskCacheQuestionRequestModel> cacheList = new List<CourseTestTaskCacheQuestionRequestModel>();

                                CourseTestTaskCacheQuestionRequestModel cacheModel = new CourseTestTaskCacheQuestionRequestModel
                                {
                                    QuestionId = x.TopicId,
                                    Score = x.Score
                                };
                                cacheList.Add(cacheModel);

                                //缓存4小时
                                RedisHelper.SetAsync($"Cache:Task:{entity.Id}:{dto.CourseOrCaseId}", cacheList, 240 * 60);

                                #region 
                                ////加入缓存 记录任务
                                //List<long> questionIds = new List<long> { x.Id };
                                ////缓存4小时
                                //RedisHelper.SetAsync($"Cache:Task:{entity.Id}:{dto.CourseOrCaseId}", questionIds, 240 * 60);
                                #endregion

                            }
                        });
                    }
                }

                //添加测验任务、班级关联
                if (dto.ClassList != null && dto.ClassList.Count > 0)
                {
                    dto.ClassList.ForEach(x =>
                    {
                        YssxCourseTestTaskClass classModel = new YssxCourseTestTaskClass
                        {
                            Id = IdWorker.NextId(),
                            TestTaskId = entity.Id,
                            ClassId = x.ClassId,
                            ClassName = x.ClassName,
                            CreateBy = user.Id,
                            CreateTime = dateTimeNow,
                            UpdateBy = user.Id,
                            UpdateTime = dateTimeNow,
                            TenantId = user.TenantId,
                        };

                        classList.Add(classModel);
                    });

                    //班级Ids
                    var classIds = dto.ClassList.Select(x => x.ClassId).ToList();

                    //生成学生主作答记录 (课程任务)
                    if (entity.TaskSource == CourseTestTaskSource.Course)
                        studentGradeList = CreateStudentGrade(examPaperList, classIds, entity.Id, user);
                }

                #endregion

                #region 新增数据
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //课堂测验任务
                        var task = uow.GetRepository<YssxCourseTestTask>().Insert(entity);
                        //测验题目
                        if (topicList.Count > 0)
                            uow.GetRepository<YssxCourseTestTopic>().Insert(topicList);
                        //保存试卷
                        if (examPaperList.Count > 0)
                            uow.GetRepository<ExamPaperCourse>().Insert(examPaperList);
                        //保存测验任务班级
                        if (classList.Count > 0)
                            uow.GetRepository<YssxCourseTestTaskClass>().Insert(classList);
                        //保存学生主作答记录
                        if (studentGradeList.Count > 0)
                            uow.GetRepository<ExamCourseUserGrade>().Insert(studentGradeList);

                        state = task != null;

                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                    }
                }
                #endregion

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }
        #endregion

        #region 获取课堂测验题目操作状态
        /// <summary>
        /// 获取课堂测验题目操作状态
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<CourseTestTopicOperateViewModel>> GetCourseTestTopicOperateStatus(CourseTestTopicOperateQuery query)
        {
            ResponseContext<CourseTestTopicOperateViewModel> result = new ResponseContext<CourseTestTopicOperateViewModel>();

            CourseTestTopicOperateViewModel data = new CourseTestTopicOperateViewModel();

            //查询课堂测验题目
            YssxCourseTestTopic testTopicData = await DbContext.FreeSql.GetRepository<YssxCourseTestTopic>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.TeachCourseId == query.TeachCourseId && a.TopicId == query.TopicId).FirstAsync();

            //未发布测验
            if (testTopicData == null)
            {
                data.TopicStatus = CourseTestTopicOperateStatus.DidNotTest;

                result.Data = data;
                return result;
            }

            //查询任务信息
            YssxCourseTestTask testTaskData = await DbContext.FreeSql.GetRepository<YssxCourseTestTask>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.TeachCourseId == query.TeachCourseId && a.Id == testTopicData.TestTaskId).FirstAsync();

            //任务不存在
            if (testTopicData == null)
            {
                data.TopicStatus = CourseTestTopicOperateStatus.DidNotTest;

                result.Data = data;
                return result;
            }

            //查询任务试卷信息
            ExamPaperCourse examData = await DbContext.FreeSql.GetRepository<ExamPaperCourse>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.TaskId == testTaskData.Id).FirstAsync();
            //测验进行中
            if (testTaskData.Status == LessonsTaskStatus.Started)
            {
                data.TopicStatus = CourseTestTopicOperateStatus.TestProgress;
                data.TestTaskId = testTaskData.Id;
                data.ExamId = examData?.Id ?? 0;

                result.Data = data;
                return result;
            }
            //测验已结束
            if (testTaskData.Status == LessonsTaskStatus.End)
            {
                data.TopicStatus = CourseTestTopicOperateStatus.TestFinish;
                data.TestTaskId = testTaskData.Id;
                data.ExamId = examData?.Id ?? 0;

                result.Data = data;
                return result;
            }

            result.Data = data;
            return result;
        }
        #endregion

        #region 教师手动结束测验任务
        /// <summary>
        /// 教师手动结束测验任务
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> FinishCourseTestTask(long taskId, UserTicket user)
        {
            YssxCourseTestTask entity = await DbContext.FreeSql.GetRepository<YssxCourseTestTask>().Where(x => x.Id == taskId).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            bool state = false;
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //修改课堂测验任务
                    bool taskStatus = await uow.GetRepository<YssxCourseTestTask>().UpdateDiy
                        .Set(x => x.Status == LessonsTaskStatus.End)
                        .Set(x => x.UpdateBy == user.Id)
                        .Set(x => x.UpdateTime == DateTime.Now)
                        .Where(x => x.Id == taskId).ExecuteAffrowsAsync() > 0;

                    //修改任务试卷
                    bool examStatus = await uow.GetRepository<ExamPaperCourse>().UpdateDiy
                        .Set(x => x.Status == ExamStatus.End)
                        .Set(x => x.UpdateBy == user.Id)
                        .Set(x => x.UpdateTime == DateTime.Now)
                        .Where(x => x.TaskId == taskId).ExecuteAffrowsAsync() > 0;

                    if (taskStatus && examStatus)
                    {
                        uow.Commit();
                        state = true;
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            //结束测验任务 发布消息
            await RedisHelper.PublishAsync("coursetesttaskid", taskId.ToString());

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "操作成功" : "操作失败！" };
        }
        #endregion

        #region 获取学生课堂测验任务列表
        /// <summary>
        /// 获取学生课堂测验任务列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<StudentCourseTestTaskViewModel>> GetStudentCourseTestTaskList(StudentCourseTestTaskQuery query, UserTicket user)
        {
            var result = new PageResponse<StudentCourseTestTaskViewModel>();
            if (query == null)
                return result;

            var select = DbContext.FreeSql.Select<YssxCourseTestTask>().From<ExamPaperCourse, YssxCourseTestTaskClass, YssxStudent>(
                (a, b, c, d) =>
                a.InnerJoin(x => x.Id == b.TaskId)
                .InnerJoin(x => x.Id == c.TestTaskId)
                .InnerJoin(x => c.ClassId == d.ClassId))
                .Where((a, b, c, d) => a.CourseId == query.CourseId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete &&
                c.IsDelete == CommonConstants.IsNotDelete && d.IsDelete == CommonConstants.IsNotDelete && d.UserId == user.Id);

            if (query.TaskStatus.HasValue)
                select.Where((a, b, c, d) => a.Status == query.TaskStatus);
            if (!string.IsNullOrEmpty(query.TaskName))
                select.Where((a, b, c, d) => a.TaskName.Contains(query.TaskName));

            var totalCount = select.ToList().Count();

            //获取课堂测验任务列表
            var originalDatas = await select.OrderByDescending((a, b, c, d) => a.CreateTime).Page(query.PageIndex, query.PageSize)
                .ToListAsync((a, b, c, d) => new
                {
                    Task = a,
                    Exam = b
                });

            List<StudentCourseTestTaskViewModel> list = new List<StudentCourseTestTaskViewModel>();

            originalDatas.ForEach(x =>
            {
                StudentCourseTestTaskViewModel data = new StudentCourseTestTaskViewModel();
                data.TaskId = x.Task.Id;
                data.TaskName = x.Task.TaskName;
                data.TestStartDate = x.Task.CreateTime;
                data.TaskStatus = x.Task.Status;
                data.TaskSource = x.Task.TaskSource;
                data.CourseOrCaseId = x.Task.CourseOrCaseId;
                data.ModuleType = x.Task.ModuleType;
                data.TotalScore = x.Task.TotalScore;
                data.ExamId = x.Exam.Id;

                //课程任务
                if (x.Task.TaskSource == CourseTestTaskSource.Course)
                {
                    //课程任务作答记录
                    ExamCourseUserGrade courseGradeData = DbContext.FreeSql.Select<ExamCourseUserGrade>().From<ExamPaperCourse, YssxCourseTestTask>(
                        (a, b, c) =>
                        a.InnerJoin(q => q.ExamId == b.Id)
                        .InnerJoin(q => b.TaskId == c.Id))
                        .Where((a, b, c) => c.Id == x.Task.Id && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete &&
                        c.IsDelete == CommonConstants.IsNotDelete && a.UserId == user.Id).First();

                    //进行中任务
                    if (x.Task.Status == LessonsTaskStatus.Started)
                    {
                        //主作答记录为空 开始测验
                        data.OperateStatus = CourseTestTaskStudentOperateStatus.StartTest;

                        if (courseGradeData != null)
                        {
                            data.GradeId = courseGradeData.Id;
                            data.Score = courseGradeData.Score;

                            //主作答记录答题结束 查看作答
                            if (courseGradeData.Status == StudentExamStatus.End)
                                data.OperateStatus = CourseTestTaskStudentOperateStatus.CheckAnswer;
                        }
                    }
                    //已结束任务
                    if (x.Task.Status == LessonsTaskStatus.End)
                    {
                        data.TestEndDate = x.Task.UpdateTime;

                        if (courseGradeData == null)
                        {
                            //主作答记录为空 未作答
                            data.OperateStatus = CourseTestTaskStudentOperateStatus.NotAnswer;
                        }
                        else
                        {
                            data.GradeId = courseGradeData.Id;
                            data.Score = courseGradeData.Score;
                            data.OperateStatus = CourseTestTaskStudentOperateStatus.CheckAnswer;

                            //主作答记录答题未结束
                            if (courseGradeData.Status != StudentExamStatus.End)
                            {
                                ExamCourseUserGradeDetail couDetailData = DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.IsDelete ==
                                     CommonConstants.IsNotDelete && a.GradeId == data.GradeId).First();
                                //没有作答详情信息 未作答
                                if (couDetailData == null)
                                    data.OperateStatus = CourseTestTaskStudentOperateStatus.NotAnswer;
                            }
                        }
                    }
                }

                //案例任务
                if (x.Task.TaskSource == CourseTestTaskSource.Case)
                {
                    //案例任务作答记录
                    YssxStudentCaseTaskGradeInfo caseGradeData = DbContext.FreeSql.GetRepository<YssxStudentCaseTaskGradeInfo>().Where(a => a.IsDelete ==
                                     CommonConstants.IsNotDelete && a.TaskId == x.Task.Id && a.UserId == user.Id).First();

                    //进行中任务
                    if (x.Task.Status == LessonsTaskStatus.Started)
                    {
                        //主作答记录为空 开始测验
                        data.OperateStatus = CourseTestTaskStudentOperateStatus.StartTest;

                        if (caseGradeData != null)
                        {
                            data.Score = caseGradeData.Score;

                            //主作答记录答题结束 查看作答
                            if (caseGradeData.IsSubmitExam)
                                data.OperateStatus = CourseTestTaskStudentOperateStatus.CheckAnswer;
                        }
                    }
                    //已结束任务
                    if (x.Task.Status == LessonsTaskStatus.End)
                    {
                        data.TestEndDate = x.Task.UpdateTime;

                        if (caseGradeData == null)
                        {
                            //主作答记录为空 未作答
                            data.OperateStatus = CourseTestTaskStudentOperateStatus.NotAnswer;
                        }
                        else
                        {
                            data.Score = caseGradeData.Score;
                            data.OperateStatus = CourseTestTaskStudentOperateStatus.CheckAnswer;
                        }
                    }

                    //查询是否在缓存中
                    List<CourseTestTaskCacheQuestionRequestModel> cacheList = RedisHelper.Get<List<CourseTestTaskCacheQuestionRequestModel>>
                        ($"Cache:Task:{data.TaskId}:{data.CourseOrCaseId}");

                    if (cacheList == null || cacheList.Count == 0)
                    {
                        YssxCourseTestTopic topicModel = DbContext.FreeSql.GetRepository<YssxCourseTestTopic>().Where(a => a.IsDelete ==
                                      CommonConstants.IsNotDelete && a.TestTaskId == x.Task.Id).First();
                        if (topicModel != null)
                        {
                            CourseTestTaskCacheQuestionRequestModel cacheModel = new CourseTestTaskCacheQuestionRequestModel
                            {
                                QuestionId = topicModel.TopicId,
                                Score = topicModel.Score
                            };
                            cacheList = new List<CourseTestTaskCacheQuestionRequestModel>();
                            cacheList.Add(cacheModel);


                            //缓存4小时
                            RedisHelper.SetAsync($"Cache:Task:{data.TaskId}:{data.CourseOrCaseId}", cacheList, 240 * 60);
                        }
                    }
                }

                list.Add(data);
            });

            result.Data = list;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 获取进行中课堂测验详情列表
        /// <summary>
        /// 获取进行中课堂测验详情列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CourseTestDetailsInProgressViewModel>>> GetInProgressCourseTestDetailsList(CourseTestDetailsInProgressQuery query)
        {
            ResponseContext<List<CourseTestDetailsInProgressViewModel>> response = new ResponseContext<List<CourseTestDetailsInProgressViewModel>>();

            List<CourseTestDetailsInProgressViewModel> listData = new List<CourseTestDetailsInProgressViewModel>();

            YssxCourseTestTask entity = await DbContext.FreeSql.GetRepository<YssxCourseTestTask>().Where(x => x.Id == query.TestTaskId).FirstAsync();
            if (entity == null)
                return response;

            //课堂测验课程任务
            if (entity.TaskSource == CourseTestTaskSource.Course)
            {
                var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxCourseTestTaskClass, ExamPaperCourse, ExamCourseUserGrade>(
                (a, b, c, d) =>
                a.InnerJoin(x => x.ClassId == b.ClassId)
                .LeftJoin(x => c.Id == d.ExamId && x.UserId == d.UserId && d.Status == StudentExamStatus.End && d.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c, d) => b.TestTaskId == query.TestTaskId && c.TaskId == query.TestTaskId && a.IsDelete == CommonConstants.IsNotDelete
                && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete);

                if (query.ClassId.HasValue)
                    select.Where((a, b, c, d) => b.ClassId == query.ClassId);

                var studentList = await select.ToListAsync((a, b, c, d) => new
                {
                    UserId = a.UserId,
                    StudentName = a.Name,
                    ClassId = b.ClassId,
                    ClassName = b.ClassName,
                    GradeId = d.Id
                });

                studentList.ForEach(x =>
                {
                    CourseTestDetailsInProgressViewModel model = new CourseTestDetailsInProgressViewModel();
                    model.UserId = x.UserId;
                    model.StudentName = x.StudentName;
                    model.ClassId = x.ClassId;
                    model.ClassName = x.ClassName;
                    if (x.GradeId > 0)
                        model.IsSubmitExam = true;

                    listData.Add(model);
                });
            }
            //课堂测验案例任务
            if (entity.TaskSource == CourseTestTaskSource.Case)
            {
                var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxCourseTestTaskClass, YssxStudentCaseTaskGradeInfo>(
                (a, b, c) =>
                a.InnerJoin(x => x.ClassId == b.ClassId)
                .LeftJoin(x => x.UserId == c.UserId && c.IsSubmitExam && c.TaskId == query.TestTaskId && c.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c) => b.TestTaskId == query.TestTaskId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);

                if (query.ClassId.HasValue)
                    select.Where((a, b, c) => b.ClassId == query.ClassId);

                var studentList = await select.ToListAsync((a, b, c) => new
                {
                    UserId = a.UserId,
                    StudentName = a.Name,
                    ClassId = b.ClassId,
                    ClassName = b.ClassName,
                    GradeId = c.Id
                });

                studentList.ForEach(x =>
                {
                    CourseTestDetailsInProgressViewModel model = new CourseTestDetailsInProgressViewModel();
                    model.UserId = x.UserId;
                    model.StudentName = x.StudentName;
                    model.ClassId = x.ClassId;
                    model.ClassName = x.ClassName;
                    if (x.GradeId > 0)
                        model.IsSubmitExam = true;

                    listData.Add(model);
                });
            }

            response.Data = listData;
            return response;
        }
        #endregion

        #region 获取已结束课堂测验统计列表
        /// <summary>
        /// 获取已结束课堂测验统计列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CourseTestStatisticsFinishedViewModel>>> GetFinishedCourseTestStatisticsList(CourseTestStatisticsFinishedQuery query)
        {
            ResponseContext<List<CourseTestStatisticsFinishedViewModel>> response = new ResponseContext<List<CourseTestStatisticsFinishedViewModel>>();

            List<CourseTestStatisticsFinishedViewModel> listData = new List<CourseTestStatisticsFinishedViewModel>();

            YssxCourseTestTask entity = await DbContext.FreeSql.GetRepository<YssxCourseTestTask>().Where(x => x.Id == query.TestTaskId).FirstAsync();
            if (entity == null)
                return response;

            //课堂测验课程任务
            if (entity.TaskSource == CourseTestTaskSource.Course)
            {
                var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxCourseTestTaskClass, ExamPaperCourse, ExamCourseUserGrade, ExamCourseUserGradeDetail>(
                (a, b, c, d, e) =>
                a.InnerJoin(x => x.ClassId == b.ClassId)
                .InnerJoin(x => b.TestTaskId == c.TaskId)
                .LeftJoin(x => c.Id == d.ExamId && x.UserId == d.UserId && d.IsDelete == CommonConstants.IsNotDelete)
                .LeftJoin(x => d.Id == e.GradeId && e.IsDelete == CommonConstants.IsNotDelete && e.Status == AnswerResultStatus.Right))
                .Where((a, b, c, d, e) => b.TestTaskId == query.TestTaskId && c.TaskId == query.TestTaskId && a.IsDelete == CommonConstants.IsNotDelete
                && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete);

                if (query.ClassId.HasValue)
                    select.Where((a, b, c, d, e) => b.ClassId == query.ClassId);

                var studentList = await select.ToListAsync((a, b, c, d, e) => new
                {
                    UserId = a.UserId,
                    StudentName = a.Name,
                    ClassId = b.ClassId,
                    ClassName = b.ClassName,
                    GradeDetailId = e.Id
                });

                studentList.ForEach(x =>
                {
                    CourseTestStatisticsFinishedViewModel model = new CourseTestStatisticsFinishedViewModel();
                    model.UserId = x.UserId;
                    model.StudentName = x.StudentName;
                    model.ClassId = x.ClassId;
                    model.ClassName = x.ClassName;
                    if (x.GradeDetailId > 0)
                        model.IsAnswerRight = true;

                    listData.Add(model);
                });
            }
            //课堂测验案例任务
            if (entity.TaskSource == CourseTestTaskSource.Case)
            {
                var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxCourseTestTaskClass, YssxStudentCaseTaskGradeInfo>(
                (a, b, c) =>
                a.InnerJoin(x => x.ClassId == b.ClassId)
                .LeftJoin(x => x.UserId == c.UserId && c.AnswerStatus == AnswerResultStatus.Right && c.TaskId == query.TestTaskId && c.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c) => b.TestTaskId == query.TestTaskId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);

                if (query.ClassId.HasValue)
                    select.Where((a, b, c) => b.ClassId == query.ClassId);

                var studentList = await select.ToListAsync((a, b, c) => new
                {
                    UserId = a.UserId,
                    StudentName = a.Name,
                    ClassId = b.ClassId,
                    ClassName = b.ClassName,
                    GradeDetailId = c.Id
                });

                studentList.ForEach(x =>
                {
                    CourseTestStatisticsFinishedViewModel model = new CourseTestStatisticsFinishedViewModel();
                    model.UserId = x.UserId;
                    model.StudentName = x.StudentName;
                    model.ClassId = x.ClassId;
                    model.ClassName = x.ClassName;
                    if (x.GradeDetailId > 0)
                        model.IsAnswerRight = true;

                    listData.Add(model);
                });
            }

            response.Data = listData;
            return response;
        }
        #endregion

        #region 获取已结束课堂测验简答题作答列表
        /// <summary>
        /// 获取已结束课堂测验简答题作答列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CourseTestShortAnswerFinishedViewModel>>> GetFinishedCourseTestShortAnswerList(CourseTestShortAnswerFinishedQuery query)
        {
            ResponseContext<List<CourseTestShortAnswerFinishedViewModel>> response = new ResponseContext<List<CourseTestShortAnswerFinishedViewModel>>();

            List<CourseTestShortAnswerFinishedViewModel> listData = new List<CourseTestShortAnswerFinishedViewModel>();

            YssxCourseTestTask entity = await DbContext.FreeSql.GetRepository<YssxCourseTestTask>().Where(x => x.Id == query.TestTaskId).FirstAsync();
            if (entity == null)
                return response;

            //课堂测验课程任务
            if (entity.TaskSource == CourseTestTaskSource.Course)
            {
                var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxCourseTestTaskClass, ExamPaperCourse, ExamCourseUserGrade, ExamCourseUserGradeDetail>(
                (a, b, c, d, e) =>
                a.InnerJoin(x => x.ClassId == b.ClassId)
                .InnerJoin(x => b.TestTaskId == c.TaskId)
                .LeftJoin(x => c.Id == d.ExamId && x.UserId == d.UserId && d.IsDelete == CommonConstants.IsNotDelete)
                .LeftJoin(x => d.Id == e.GradeId && e.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c, d, e) => b.TestTaskId == query.TestTaskId && c.TaskId == query.TestTaskId && a.IsDelete == CommonConstants.IsNotDelete
                && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete);

                if (query.ClassId.HasValue)
                    select.Where((a, b, c, d, e) => b.ClassId == query.ClassId);

                var studentList = await select.ToListAsync((a, b, c, d, e) => new
                {
                    UserId = a.UserId,
                    StudentName = a.Name,
                    StudentNo = a.StudentNo,
                    ClassId = b.ClassId,
                    ClassName = b.ClassName,
                    AnswerValue = e.Answer
                });

                studentList.ForEach(x =>
                {
                    CourseTestShortAnswerFinishedViewModel model = new CourseTestShortAnswerFinishedViewModel();
                    model.UserId = x.UserId;
                    model.StudentName = x.StudentName;
                    model.StudentNo = x.StudentNo;
                    model.ClassId = x.ClassId;
                    model.ClassName = x.ClassName;
                    model.ShortAnswerValue = x.AnswerValue;
                    if (!string.IsNullOrEmpty(model.ShortAnswerValue))
                        model.IsHasAnswer = true;

                    listData.Add(model);
                });
            }
            //课堂测验案例任务
            if (entity.TaskSource == CourseTestTaskSource.Case)
            {
                var select = DbContext.FreeSql.Select<YssxStudent>().From<YssxCourseTestTaskClass, YssxStudentCaseTaskGradeInfo>(
                (a, b, c) =>
                a.InnerJoin(x => x.ClassId == b.ClassId)
                .LeftJoin(x => x.UserId == c.UserId && c.TaskId == query.TestTaskId && c.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c) => b.TestTaskId == query.TestTaskId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);

                if (query.ClassId.HasValue)
                    select.Where((a, b, c) => b.ClassId == query.ClassId);

                var studentList = await select.ToListAsync((a, b, c) => new
                {
                    UserId = a.UserId,
                    StudentName = a.Name,
                    StudentNo = a.StudentNo,
                    ClassId = b.ClassId,
                    ClassName = b.ClassName,
                    AnswerValue = c.ShortAnswerValue
                });

                studentList.ForEach(x =>
                {
                    CourseTestShortAnswerFinishedViewModel model = new CourseTestShortAnswerFinishedViewModel();
                    model.UserId = x.UserId;
                    model.StudentName = x.StudentName;
                    model.StudentNo = x.StudentNo;
                    model.ClassId = x.ClassId;
                    model.ClassName = x.ClassName;
                    model.ShortAnswerValue = x.AnswerValue;
                    if (!string.IsNullOrEmpty(model.ShortAnswerValue))
                        model.IsHasAnswer = true;

                    listData.Add(model);
                });
            }

            listData = listData.OrderBy(x => x.StudentNo).ToList();

            response.Data = listData;
            return response;
        }
        #endregion

        #region 私有方法

        #region 生成学生作答记录
        /// <summary>
        /// 生成学生作答记录
        /// </summary>
        /// <param name="examPaperList">试卷列表</param>
        /// <param name="classIds">班级Ids</param>
        /// <param name="testTaskId">测验任务Id</param>
        /// <param name="user"></param>
        /// <returns></returns>
        private List<ExamCourseUserGrade> CreateStudentGrade(List<ExamPaperCourse> examPaperList, List<long> classIds, long testTaskId, UserTicket user)
        {
            List<ExamCourseUserGrade> list = new List<ExamCourseUserGrade>();

            if (classIds == null || classIds.Count <= 0)
                return list;

            //根据班级查询学生列表
            List<YssxStudent> studentList = DbContext.FreeSql.Select<YssxStudent>().Where(x => classIds.Contains(x.ClassId) && x.IsDelete == CommonConstants.IsNotDelete).ToList();

            examPaperList.ForEach(x =>
            {
                studentList.ForEach(a =>
                {
                    ExamCourseUserGrade model = new ExamCourseUserGrade();

                    model.Id = IdWorker.NextId();
                    model.UserId = a.UserId;
                    model.ExamId = x.Id;
                    model.CourseId = x.CourseId;
                    model.OriginalCourseId = x.OriginalCourseId;
                    model.TaskId = x.TaskId;
                    model.ExamPaperScore = x.TotalScore;
                    model.LeftSeconds = x.TotalMinutes * 60;
                    model.RecordTime = x.BeginTime;
                    model.StudentId = CommonConstants.StudentId;
                    model.Status = (int)StudentExamStatus.Wait;
                    model.CreateBy = user.Id;
                    model.CreateTime = DateTime.Now;
                    model.UpdateBy = user.Id;
                    model.UpdateTime = DateTime.Now;
                    model.TenantId = user.TenantId;

                    list.Add(model);
                });
            });

            return list;
        }
        #endregion

        #endregion
    }
}