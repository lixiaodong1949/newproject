﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.Utils;

namespace Yssx.S.ServiceImpl
{
    public class SectionService : ISectionService
    {
        public async Task<ResponseContext<bool>> AddOrEditSection(YssxSectionDto dto)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxSection entity = new YssxSection
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                ParentId = dto.ParentId,
                SectionName = dto.SectionName,
                SectionTitle = dto.SectionTitle,
                KnowledgePointId = dto.KnowledgePointId,
                SectionType = dto.SectionType,
                Sort = dto.Sort,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                YssxSection course = await DbContext.FreeSql.GetRepository<YssxSection>().InsertAsync(entity);
                state = course != null;
                if (dto.ParentId > 0)
                {
                    await AddSectionSummary(course.CourseId, course.Id, (int)SectionSummaryType.Section);
                }
            }
            else
            {
                state = UpdateSection(entity);
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        private bool UpdateSection(YssxSection entity)
        {
            return DbContext.FreeSql.GetRepository<YssxSection>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.SectionName, x.SectionTitle, x.UpdateTime }).ExecuteAffrows() > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private async Task<bool> AddSectionSummary(long cid, long sid, int summaryType)
        {
            YssxSectionSummary entity = await DbContext.FreeSql.GetRepository<YssxSectionSummary>().Where(x => x.CourseId == cid && x.SummaryType == summaryType).FirstAsync();
            bool state = false;
            if (null == entity)
            {
                entity = await DbContext.FreeSql.GetRepository<YssxSectionSummary>().InsertAsync(new YssxSectionSummary { Id = IdWorker.NextId(), CourseId = cid, SectionId = sid, Count = 1, SummaryType = summaryType, UpdateTime = DateTime.Now });
                state = entity != null;
            }
            else
            {
                entity.Count += 1;
                entity.UpdateTime = DateTime.Now;
                state = DbContext.FreeSql.GetRepository<YssxSectionSummary>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.Count, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return state;
        }

        private async Task<bool> RemovSectionSummary(long cid, int summaryType)
        {
            YssxSectionSummary entity = await DbContext.FreeSql.GetRepository<YssxSectionSummary>().Where(x => x.CourseId == cid && x.SummaryType == summaryType).FirstAsync();
            bool state = false;
            if (null != entity)
            {
                entity.Count -= 1;
                entity.UpdateTime = DateTime.Now;
                state = DbContext.FreeSql.GetRepository<YssxSectionSummary>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.Count, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return state;
        }


        public async Task<ResponseContext<bool>> AddOrEditSectionTextBook(YssxSectionTextBookDto dto)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxSectionTextBook entity = new YssxSectionTextBook
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                SectionId = dto.SectionId,
                Content = dto.Content,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                YssxSectionTextBook course = await DbContext.FreeSql.GetRepository<YssxSectionTextBook>().InsertAsync(entity);
                //state = course != null;
                state = await AddSectionSummary(course.CourseId, course.SectionId, (int)SectionSummaryType.TextBook);
            }
            else
            {
                state = UpdateSectionTextBook(entity);
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        private bool UpdateSectionTextBook(YssxSectionTextBook entity)
        {
            return DbContext.FreeSql.GetRepository<YssxSectionTextBook>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.Content, x.UpdateTime }).ExecuteAffrows() > 0;
        }

        public async Task<ResponseContext<List<YssxSectionTextBookDto>>> GetSectionTextBookList(long sid)
        {
            ResponseContext<List<YssxSectionTextBookDto>> response = new ResponseContext<List<YssxSectionTextBookDto>>();
            List<YssxSectionTextBook> list = await DbContext.FreeSql.GetRepository<YssxSectionTextBook>().Where(x => x.SectionId == sid && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            response.Data = list.Select(x => new YssxSectionTextBookDto
            {
                Id = x.Id,
                Content = x.Content,
                CourseId = x.CourseId,
                SectionId = x.SectionId
            }).ToList();
            return response;
        }

        public async Task<ResponseContext<List<YssxSectionDto>>> GetSectionList(long courseId)
        {
            ResponseContext<List<YssxSectionDto>> response = new ResponseContext<List<YssxSectionDto>>();
            List<YssxSection> list = await DbContext.FreeSql.GetRepository<YssxSection>().Where(x => x.CourseId == courseId && x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.Sort).ToListAsync();
            response.Data = list.Select(x => new YssxSectionDto
            {
                Id = x.Id,
                SectionName = x.SectionName,
                SectionTitle = x.SectionTitle,
                ParentId = x.ParentId,
                SectionType = x.SectionType,
                CourseId = x.CourseId,
                Sort = x.Sort
            }).ToList();
            return response;
        }

        public async Task<ResponseContext<bool>> RemoveSection(long id)
        {
            YssxSection entity = await DbContext.FreeSql.GetRepository<YssxSection>().Where(x => x.Id == id).FirstAsync();
            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxSection>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
            //删除节下面的附件及题目 
            DbContext.FreeSql.GetRepository<YssxSectionFiles>().UpdateDiy
                .Set(x => x.IsDelete, CommonConstants.IsDelete)
                .Set(x => x.UpdateTime,DateTime.Now)
                .Where(x => x.SectionId == id).ExecuteAffrows();

            DbContext.FreeSql.GetRepository<YssxSectionTopic>().UpdateDiy
                .Set(x => x.IsDelete, CommonConstants.IsDelete)
                .Set(x => x.UpdateTime, DateTime.Now)
                .Where(x => x.SectionId == id).ExecuteAffrows();
            await RemovSectionSummary(entity.CourseId, (int)SectionSummaryType.Section);
            

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<bool>> RemoveSectionTextBook(long id)
        {
            YssxSectionTextBook entity = await DbContext.FreeSql.GetRepository<YssxSectionTextBook>().Where(x => x.Id == id).FirstAsync();
            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxSectionTextBook>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
            await RemovSectionSummary(entity.CourseId, (int)SectionSummaryType.TextBook);
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<List<YssxSectionSummaryDto>>> GetCourseSummaryList(long sid)
        {
            ResponseContext<List<YssxSectionSummaryDto>> response = new ResponseContext<List<YssxSectionSummaryDto>>();
            List<YssxSectionSummary> list = await DbContext.FreeSql.GetRepository<YssxSectionSummary>().Where(x => x.SectionId == sid).ToListAsync();
            response.Data = list.Select(x => new YssxSectionSummaryDto
            {
                Id = x.Id,
                Count = x.Count,
                CourseId = x.CourseId,
                SectionId = x.SectionId,
                SummaryType = x.SummaryType
            }).ToList();
            return response;
        }

        public async Task<ResponseContext<bool>> AddOrEditSectionFile(YssxSectionFilesDto dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxSectionFiles entity = new YssxSectionFiles
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                SectionId = dto.SectionId,
                KnowledgePointId = dto.KnowledgePointId,               
                File = dto.File,
                FileName = dto.FileName,
                FilesType = dto.FilesType,
                Sort = dto.Sort,
                CreateBy = user.Id,
                SectionType = dto.SectionType,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now,
                CourseFileId = dto.CourseFileId,
                IsFile=dto.IsFile,
                Remark = dto.Remark,
                KnowledgePointNames=dto.KnowledgePointNames
            };
            
            bool state = false;

            if (dto.Id <= 0)
            {
                if (dto.FromType == 1 && dto.IsFile)
                {
                    YssxCourceFiles courceFiles = new YssxCourceFiles
                    {
                        Id = IdWorker.NextId(),
                        CreateBy = user.Id,

                        TenantId = user.TenantId,
                        File = dto.File,
                        FileName = dto.FileName,
                        FileSize = dto.FileSize,
                        Sort = dto.Sort,
                        FilesType = dto.FilesType,
                    };
                    entity.CourseFileId = courceFiles.Id;
                    YssxCourceFiles files = await DbContext.FreeSql.GetRepository<YssxCourceFiles>().InsertAsync(courceFiles);
                }

                YssxSectionFiles course = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().InsertAsync(entity);
                if (dto.IsCloneSZ)
                {
                    var szSectionFile = entity.DeepClone();
                    szSectionFile.Id = IdWorker.NextId();
                    szSectionFile.IsFile = true;
                    szSectionFile.SectionType = 6;
                    var lastSZ = await DbContext.FreeSql.GetRepository<YssxSectionFiles>()
                        .Where(s => s.SectionId == entity.SectionId && s.SectionType == 6 && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                    szSectionFile.Sort = lastSZ != null ? lastSZ.Sort+1 : 1;
                    await DbContext.FreeSql.GetRepository<YssxSectionFiles>().InsertAsync(szSectionFile);
                }
                
                state = (course != null);

                await AddSectionSummary(course.CourseId, course.SectionId, dto.FilesType == 6 ? (int)SectionSummaryType.Video : (int)SectionSummaryType.Files);
            }
            else
            {
                //YssxCourceFiles fileEntity = await DbContext.FreeSql.GetRepository<YssxCourceFiles>().Where(x => x.Id == dto.CourseFileId).FirstAsync();
                state = DbContext.FreeSql.GetRepository<YssxSectionFiles>().UpdateDiy.SetSource(entity).UpdateColumns(x =>
                new
                {
                    x.IsFile,
                    x.FilesType,
                    x.File,
                    x.FileName,
                    x.KnowledgePointId,
                    x.KnowledgePointNames,
                    x.Remark,
                    x.Sort,
                    x.UpdateTime
                }).ExecuteAffrows() > 0;
                //bool state2 = DbContext.FreeSql.GetRepository<YssxCourceFiles>().UpdateDiy.SetSource(fileEntity).UpdateColumns(x => new { x.File, x.FileName, x.Sort, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }


        public async Task<ResponseContext<bool>> AddOrEditSectionTopic(YssxSectionTopicDto dto)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxSectionTopic entity = new YssxSectionTopic
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                SectionId = dto.SectionId,
                QuestionId = dto.QuestionId,
                QuestionName = dto.QuetionName,
                KnowledgePointId = dto.KnowledgePointId,
                Sort = dto.Sort,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };
            bool isExist = await DbContext.FreeSql.GetRepository<YssxSectionTopic>().Where(x => x.CourseId == dto.CourseId && x.QuestionId == dto.QuestionId).AnyAsync();
            if (isExist) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "已经存在相同的题目!" };
            bool state = false;

            if (dto.Id <= 0)
            {
                YssxSectionTopic course = await DbContext.FreeSql.GetRepository<YssxSectionTopic>().InsertAsync(entity);
                state = course != null;
                await AddSectionSummary(course.CourseId, course.SectionId, (int)SectionSummaryType.Exercises);
            }
            else
            {
                state = DbContext.FreeSql.GetRepository<YssxSectionTopic>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.QuestionId, x.QuestionName, x.Sort, x.KnowledgePointId, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesList(long courseId, long sid)
        {
            ResponseContext<List<YssxSectionFilesDto>> response = new ResponseContext<List<YssxSectionFilesDto>>();
            List<YssxSectionFiles> list = new List<YssxSectionFiles>();
            list = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Where(x => x.CourseId == courseId && x.SectionId == sid && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            response.Data = list.Select(x => new YssxSectionFilesDto
            {
                Id = x.Id,
                File = x.File,
                FileName = x.FileName,
                FilesType = x.FilesType,
                SectionId = x.SectionId,
                CourseFileId = x.CourseFileId,
                KnowledgePointId = x.KnowledgePointId,
                SectionType = x.SectionType,
                CourseId = x.CourseId,

            }).ToList();
            return response;
        }

        private async Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesList(int fileType, long courseId)
        {
            ResponseContext<List<YssxSectionFilesDto>> response = new ResponseContext<List<YssxSectionFilesDto>>();

            var selectList = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Select.From<YssxSection>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id)).Where((a, b) => a.CourseId == courseId && a.IsDelete == CommonConstants.IsNotDelete && a.FilesType != 6 && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync((a, b) => new YssxSectionFilesDto
            {
                Id = a.Id,
                File = a.File,
                FileName = a.FileName,
                FilesType = a.FilesType,
                SectionId = a.SectionId,
                SectionName = b.SectionName,
                CourseFileId = a.CourseFileId,
                KnowledgePointId = a.KnowledgePointId,
                SectionType = a.SectionType,
                CourseId = a.CourseId,
            });
            response.Data = selectList;
            return response;
        }

        public async Task<ResponseContext<bool>> AddOrEditSectionCase(YssxSectionCaseDto dto)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxSectionCase entity = new YssxSectionCase
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                SectionId = dto.SectionId,
                CaseId = dto.CaseId,
                KnowledgePointId = dto.KnowledgePointId,
                Sort = dto.Sort,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                YssxSectionCase course = await DbContext.FreeSql.GetRepository<YssxSectionCase>().InsertAsync(entity);
                state = course != null;
                state = await AddSectionSummary(course.CourseId, course.SectionId, (int)SectionSummaryType.Case);
            }
            else
            {
                state = DbContext.FreeSql.GetRepository<YssxSectionCase>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.CaseId, x.Sort, x.KnowledgePointId, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<bool>> RemoveSectionCase(long id)
        {
            YssxSectionCase entity = await DbContext.FreeSql.GetRepository<YssxSectionCase>().Where(x => x.Id == id).FirstAsync();
            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxSectionCase>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
            await RemovSectionSummary(entity.CourseId, (int)SectionSummaryType.Case);
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<bool>> RemoveSectionTopic(long id)
        {
            YssxSectionTopic entity = await DbContext.FreeSql.GetRepository<YssxSectionTopic>().Where(x => x.Id == id).FirstAsync();
            bool state = false;
            if (entity != null)
            {
                entity.IsDelete = 1;
                entity.UpdateTime = DateTime.Now;
                state = DbContext.FreeSql.GetRepository<YssxSectionTopic>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
                await RemovSectionSummary(entity.CourseId, (int)SectionSummaryType.Case);
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<bool>> RemoveSectionFile(long id)
        {
            YssxSectionFiles entity = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Where(x => x.Id == id).FirstAsync();
            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxSectionFiles>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;

            await RemovSectionSummary(entity.CourseId, entity.FilesType == 6 ? (int)SectionSummaryType.Video : (int)SectionSummaryType.Files);
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<List<YssxSectionTopicDto>>> GetYssxSectionTopicList(long sid)
        {
            ResponseContext<List<YssxSectionTopicDto>> response = new ResponseContext<List<YssxSectionTopicDto>>();

            //DbContext.FreeSql.GetRepository<YssxTopicPublic>().Where(s => s.Id == 982561691058177).First();

            List<YssxSectionTopicDto> list = await DbContext.FreeSql.GetRepository<YssxSectionTopic>().Select.From<YssxTopicPublic>((a, b) => a.LeftJoin(aa => aa.QuestionId == b.Id)).Where((a, b) => a.SectionId == sid && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete).OrderBy((a, b) => b.Sort).ToListAsync((a, b) => new YssxSectionTopicDto
            {
                Id = a.Id,
                KnowledgePointId = a.KnowledgePointId,
                QuestionId = a.QuestionId,
                SectionId = a.SectionId,
                QuetionName = b.Title,
                Sort = b.Sort,
                QuestionType = (int)b.QuestionType,
                CourseId = a.CourseId,
                Content = b.Content,
                Score = b.Score,
            });
            //response.Data = list.Select(x => new YssxSectionTopicDto
            //{
            //    Id = x.Id,
            //    KnowledgePointId = x.KnowledgePointId,
            //    QuestionId = x.QuestionId,
            //    SectionId = x.SectionId,
            //    QuetionName = x.QuestionName,
            //    Sort = x.Sort,
            //    CourseId = x.CourseId,

            //}).ToList();
            response.Data = list;
            return response;
        }

        /// <summary>
        /// 根据节获取习题
        /// 一般情况下，获取到所有的习题后，PC端可以自己筛选数据，不需要再次请求接口进行查询
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxSectionTopicDto>>> GetYssxSectionTopicistForPC(SectionTopicQuery query)
        {
            ResponseContext<List<YssxSectionTopicDto>> response = new ResponseContext<List<YssxSectionTopicDto>>();

            var select = DbContext.FreeSql.GetRepository<YssxSectionTopic>().Select.From<YssxTopicPublic>(
                (a, b) =>
                a.InnerJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.SectionId == query.SectionId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);

            if (query.QuestionType.HasValue)
                select.Where((a, b) => b.QuestionType == query.QuestionType);
            if (!string.IsNullOrEmpty(query.Keyword))
                select.Where((a, b) => b.Title.Contains(query.Keyword));

            List<YssxSectionTopicDto> list = await select.OrderBy((a, b) => b.Sort).ToListAsync((a, b) => new YssxSectionTopicDto
            {
                Id = a.Id,
                KnowledgePointId = a.KnowledgePointId,
                QuestionId = a.QuestionId,
                SectionId = a.SectionId,
                QuetionName = b.Title,
                Sort = b.Sort,
                QuestionType = (int)b.QuestionType,
                CourseId = a.CourseId,
                Content = b.Content,
                Score = b.Score,
            });

            response.Data = list;
            return response;
        }

        public async Task<ResponseContext<SectionFileListDto>> GetSectionFilesListByType(long courseId, long sid, int fileType)
        {
            ResponseContext<SectionFileListDto> response = new ResponseContext<SectionFileListDto>();
            response.Data = new SectionFileListDto();
            if (fileType == -1)
            {
                var selectList = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Select.From<YssxSection>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id)).Where((a, b) => a.CourseId == courseId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync((a, b) => new YssxSectionFilesDto
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    SectionName = b.SectionName,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                });

                response.Data.SectionFileList = selectList.Where(x => x.SectionId > 0).ToList(); ;

                response.Data.CourseFileList = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Where(x => x.CourseId == courseId && x.IsDelete == CommonConstants.IsNotDelete && x.SectionId == 0).OrderBy(x=> x.CreateTime).ToListAsync(a => new YssxSectionFilesDto
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                });
            }
            else if (fileType != 6)
            {
                var selectList = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Select.From<YssxSection>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id)).Where((a, b) => a.CourseId == courseId && a.IsDelete == CommonConstants.IsNotDelete && a.FilesType != 6 && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync((a, b) => new YssxSectionFilesDto
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    SectionName = b.SectionName,
                    SectionTitle = b.SectionTitle,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                });

                response.Data.SectionFileList = selectList.Where(x => x.SectionId > 0).ToList();
                response.Data.CourseFileList = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Where(x => x.CourseId == courseId && x.FilesType != 6 && x.IsDelete == CommonConstants.IsNotDelete && x.SectionId == 0).ToListAsync(a => new YssxSectionFilesDto
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                });
            }
            else
            {
                var selectList = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Select.From<YssxSection>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id)).Where((a, b) => a.CourseId == courseId && a.IsDelete == CommonConstants.IsNotDelete && a.FilesType == fileType && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync((a, b) => new YssxSectionFilesDto
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    SectionName = b.SectionName,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                });

                response.Data.SectionFileList = selectList;
            }
            return response;
        }

        /// <summary>
        /// 获取课程下所有的课件,教案,视频按类型筛选 （包含课程章节下的）
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesListByCourseId(long courseId)
        {
            var response = new ResponseContext<List<YssxSectionFilesDto>>();

            var selectList = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Select
            .From<YssxSection>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id))
            .Where((a, b) => a.CourseId == courseId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
            .ToListAsync((a, b) => new YssxSectionFilesDto
            {
                Id = a.Id,
                File = a.File,
                FileName = a.FileName,
                FilesType = a.FilesType,
                SectionId = a.SectionId,
                SectionName = b.SectionName,
                CourseFileId = a.CourseFileId,
                KnowledgePointId = a.KnowledgePointId,
                SectionType = a.SectionType,
                CourseId = a.CourseId,
            });

            response.Data = selectList;
            return response;
        }

        public async Task<ResponseContext<bool>> AddOrEditSectionSceneTraining(YssxSectionSceneTrainingDto dto)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };
            YssxSectionSceneTraining entity = new YssxSectionSceneTraining
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                SectionId = dto.SectionId,
                SceneTrainingId = dto.SceneTrainingId,
                KnowledgePointId = dto.KnowledgePointId,
                Sort = dto.Sort,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                YssxSectionSceneTraining sectionSceneTraining = await DbContext.FreeSql.GetRepository<YssxSectionSceneTraining>().InsertAsync(entity);
                state = sectionSceneTraining != null;
                state = await AddSectionSummary(sectionSceneTraining.CourseId, sectionSceneTraining.SectionId, (int)SectionSummaryType.SectionSceneTraining);
            }
            else
            {
                state = DbContext.FreeSql.GetRepository<YssxSectionSceneTraining>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.SceneTrainingId, x.Sort, x.KnowledgePointId, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<bool>> RemoveSectionSceneTraininge(long id)
        {
            YssxSectionSceneTraining entity = await DbContext.FreeSql.GetRepository<YssxSectionSceneTraining>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源" };
            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            bool state = DbContext.FreeSql.GetRepository<YssxSectionSceneTraining>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows() > 0;
            await RemovSectionSummary(entity.CourseId, (int)SectionSummaryType.SectionSceneTraining);
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        public async Task<ResponseContext<List<YssxSectionSceneTrainingViewModel>>> GetYssxSectionSceneTrainingList(long sid)
        {
            ResponseContext<List<YssxSectionSceneTrainingViewModel>> response = new ResponseContext<List<YssxSectionSceneTrainingViewModel>>();

            var items = await DbContext.FreeSql.Ado.QueryAsync<YssxSectionSceneTrainingViewModel>(
                @"SELECT a.Id,a.CourseId,a.SectionId,a.ScenetrainingId,a.KnowledgePointId,x.Name,x.SelectCompanyId,x.Ywcj,x.Pxxh,x.Ywwl,x.Zzjy,x.Describe, x.YwcjName, 
                        x.SelectCompanyName FROM yssx_section_scenetraining a INNER JOIN(select b.*, c.Name AS YwcjName, d.Name AS SelectCompanyName FROM yssx_scene_training b 
                        LEFT JOIN yssx_business_scene c ON b.YwcjId = c.Id LEFT JOIN yssx_case d on b.SelectCompanyId = d.Id) x ON a.ScenetrainingId = x.Id WHERE  
                        a.IsDelete =" + CommonConstants.IsNotDelete + " AND a.SectionId=" + sid + " ");

            response.Data = items;
            return response;
        }
        #region 3.15
        //1、获取章节列表接口  章节--课件集合--视频集合---教案集合---思政案例集合
        //2、movesort 排序接口
        //3、附件类型增加 思政案例类型 ，思政案例类型上传，是否需要区分上传附件还是超链接？
        //5、添加学校引用案例中间表，后台配置学校课程订单时，默认添加到中间表中，可移除

        //4、附件上传关联的知识点是否来自 知识点库  ? 题目是否有修改
        //6、获取用户章节题目信息，及返回附件列表接口  自主练习
        //7、新题型（简答题：老师手动评分） ，题目增加知识点关联、难度等级
        //8、根据课程查出父级指点id集合，根据父级知识点id集合获取知识点列表
        /// <summary>
        /// 根据课程id查询章节列表信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<SectionListDto>>> GetSectionListByCourseId(long courseId)
        {
            long count = 0;
            var parents = await DbContext.FreeSql.GetRepository<YssxSection>().
                   Where(x => x.CourseId == courseId && x.ParentId==0 && x.IsDelete == CommonConstants.IsNotDelete)
                   .OrderBy(x => x.Sort)
                   .Master()
                   .ToListAsync<SectionListDto>();
            var childs = await DbContext.FreeSql.GetRepository<YssxSection>().
                Where(x => x.CourseId == courseId && x.ParentId > 0 && x.IsDelete == CommonConstants.IsNotDelete)
                   .OrderBy(x => x.Sort)
                   .Master()
                   .ToListAsync<SectionListDto>();
            foreach (var item in parents)
            {
                item.Level = 1;
                item.Children = GetChildNodes(childs, item.Id, item.Level);
            }
            return new ResponseContext<List<SectionListDto>>(parents);
        }
        private List<SectionListDto> GetChildNodes(List<SectionListDto> childs, long parentId, int parentLevel)
        {
            var sections = new List<SectionListDto>();
            foreach (var item in childs.Where(p => p.ParentId == parentId))
            {
                var section = new SectionListDto();
                section.Id = item.Id;
                section.SectionName = item.SectionName;
                section.SectionTitle = item.SectionTitle;
                section.ParentId = item.ParentId;
                section.SectionType = item.SectionType;
                section.CourseId = item.CourseId;
                section.Sort = item.Sort;
                section.Level = parentLevel + 1;
                section.Children = GetChildNodes(childs, item.Id,section.Level);
                
                sections.Add(section);
            }
            return sections;
        }
        /// <summary>
        /// 根据章节id获取附件集合
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        public async Task<ResponseContext<SectionFileListNewDto>> GetSectionFilesListBySectionId(long sid)
        {
            var response = new ResponseContext<SectionFileListNewDto>();
            response.Data = new SectionFileListNewDto();
            var selectList = await DbContext.FreeSql.GetRepository<YssxSectionFiles>().Select.From<YssxSection>((a, b) => a.LeftJoin(aa => aa.SectionId == b.Id))
                .Where((a, b) => a.SectionId == sid && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new YssxSectionFilesDto
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    SectionId = a.SectionId,
                    SectionName = b.SectionName,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                    IsFile=a.IsFile,
                    Remark=a.Remark
                });
            var result = selectList.Where(x => x.SectionId > 0).ToList();
            response.Data.CoursewareList = result.Where(x =>x.SectionType==1).ToList();
            response.Data.VideoList = result.Where(x => x.SectionType == 3).ToList();
            response.Data.TeachingPlanList = result.Where(x => x.SectionType == 2).ToList();
            response.Data.SZCaseList = result.Where(x => x.SectionType == 6).ToList();
            return response;
        }
        /// <summary>
        /// 根据课程id获取教材列表
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<YssxSectionFilesDto>> GetCourseFilesListByCourseId(long courseId)
        {
            var response = new ResponseContext<YssxSectionFilesDto>();
            var courceFile = await DbContext.FreeSql.GetRepository<YssxSectionFiles>()
                .Where(a => a.CourseId == courseId && a.SectionType==7 && a.IsDelete == CommonConstants.IsNotDelete)
                .OrderByDescending(a=>a.CreateTime)
                .FirstAsync(a => new YssxSectionFilesDto
                {
                    Id = a.Id,
                    File = a.File,
                    FileName = a.FileName,
                    FilesType = a.FilesType,
                    CourseFileId = a.CourseFileId,
                    KnowledgePointId = a.KnowledgePointId,
                    SectionType = a.SectionType,
                    CourseId = a.CourseId,
                });

            response.Data = courceFile;
            return response;
        }
        /// <summary>
        /// 新增/修改课程章节
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveSection(YssxSectionDto dto)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };

            YssxSection entity = new YssxSection
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                CourseId = dto.CourseId,
                ParentId = dto.ParentId,
                SectionName = dto.SectionName,
                SectionTitle = dto.SectionTitle,
                KnowledgePointId = dto.KnowledgePointId,
                SectionType = dto.SectionType,
                Sort = dto.Sort,
                CreateTime = DateTime.Now,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                await DbContext.FreeSql.GetRepository<YssxSection>().UpdateDiy.Set(s => s.Sort+1)
                    .Where(s => s.ParentId == entity.ParentId && s.Sort >= entity.Sort).ExecuteAffrowsAsync();

                YssxSection course = await DbContext.FreeSql.GetRepository<YssxSection>().InsertAsync(entity);

                state = course != null;
                if (dto.ParentId > 0)
                {
                    await AddSectionSummary(course.CourseId, course.Id, (int)SectionSummaryType.Section);
                }
            }
            else
            {
                state = UpdateSection(entity);
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion
    }
}
