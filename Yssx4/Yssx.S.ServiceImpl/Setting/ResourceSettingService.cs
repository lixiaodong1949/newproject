﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 资源配置
    /// </summary>
    public class ResourceSettingService : IResourceSettingService
    {
        #region 案例标签
        /// <summary>
        /// 获取案例标签列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<TagDto>> GetTagList(TagRequest model)
        {
            var selectData = await DbContext.FreeSql.GetGuidRepository<YssxTag>().Select.From<YssxCase>((a, b) => a.LeftJoin(aa => aa.CaseId == b.Id))
                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b) => a.Name.Contains(model.Name))
                .OrderByDescending((a, b) => a.CreateTime).ToListAsync((a, b) => new TagDto
                {
                    Id = a.Id,
                    Name = a.Name,
                    TagSourceType = a.TagSourceType,
                    CaseId = a.CaseId,
                    CaseName = b.Name,
                    Status = a.Status,
                    UpdateTime = a.UpdateTime.Value
                });
            var totalCount = selectData.Count();
            selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
            return new PageResponse<TagDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
        }

        /// <summary>
        /// 新增或修改案例标签
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<long>> AddOrEditTag(TagDto model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "案例标签名称不能为空" };
            if (model.TagSourceType == TagSourceType.Case && model.CaseId <= 0)
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "数据来源为案例时，案例ID必须大于0" };
            if (DbContext.FreeSql.Select<YssxTag>().Any(m => m.Name == model.Name && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "案例标签名称重复" };

            bool state = true;
            YssxTag entity = new YssxTag();
            if (model.Id == 0)
            {
                entity = await DbContext.FreeSql.GetRepository<YssxTag>().InsertAsync(new YssxTag
                {
                    Id = IdWorker.NextId(),
                    Name = model.Name,
                    TagSourceType = model.TagSourceType,
                    Status = model.Status,
                    CaseId = model.CaseId,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = DateTime.Now
                });
                state = entity != null;
            }
            else
            {
                entity = await DbContext.FreeSql.GetRepository<YssxTag>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该案例标签!" };

                entity.Status = model.Status;
                entity.Name = model.Name;
                entity.UpdateBy = currentUserId;
                entity.UpdateTime = DateTime.Now;
                var resUpd = await DbContext.FreeSql.Update<YssxTag>().SetSource(entity).UpdateColumns(a => new { a.Name, a.UpdateBy, a.UpdateTime, a.Status }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<long> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = entity.Id, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// 删除案例标签
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveTag(long id, long currentUserId)
        {
            YssxTag entity = await DbContext.FreeSql.GetRepository<YssxTag>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该案例标签!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<YssxTag>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }

        #endregion

        #region 知识点
        /// <summary>
        /// 获取知识点下拉列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<KnowledgePointDto>>> GetKnowledgePointList(long ignoreId)
        {
            ResponseContext<List<KnowledgePointDto>> response = new ResponseContext<List<KnowledgePointDto>>();
            List<YssxKnowledgePoint> listAll = new List<YssxKnowledgePoint>();
            //取出所有数据
            var rKnowledgePointData  = await DbContext.FreeSql.GetGuidRepository<YssxKnowledgePoint>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.Sort).OrderByDescending(x => x.CreateTime).ToListAsync();
            //筛选有效数据
            List<YssxKnowledgePoint> list = rKnowledgePointData.Where(x => x.ParentId == 0 && x.Id != ignoreId).ToList();
            if (list.Count > 0)
            {
                listAll.AddRange(list);

                //筛选下级层级
                Func<long, List<YssxKnowledgePoint>> FindChildLevel = null;
                FindChildLevel = pId =>
                {
                    var childKp = rKnowledgePointData.Where(x => x.ParentId == pId && x.Id != ignoreId).ToList();
                    if (childKp.Count > 0)
                    {
                        listAll.AddRange(childKp);
                        foreach (var item in childKp)
                        {
                            FindChildLevel(item.Id);
                        }
                    }
                    return listAll;
                };

                foreach (var item in list)
                {
                    @FindChildLevel(item.Id);
                }

                ////循环取子节点数据
                //foreach (var item in list)
                //{
                //    //第二级
                //    var listA = rKnowledgePointData.Where(x => x.ParentId == item.Id && x.Id != ignoreId).ToList();
                //    if (listA.Count > 0)
                //        listAll.AddRange(listA);
                //    foreach (var itemA in listA)
                //    {
                //        //第三级
                //        var listB = rKnowledgePointData.Where(x => x.ParentId == itemA.Id && x.Id != ignoreId).ToList();
                //        if (listB.Count > 0)
                //            listAll.AddRange(listB);
                //        foreach (var itemB in listB)
                //        {
                //            //第四级
                //            var listC = rKnowledgePointData.Where(x => x.ParentId == itemB.Id && x.Id != ignoreId).ToList();
                //            if (listC.Count > 0)
                //                listAll.AddRange(listC);
                //        }
                //    }
                //}
            }
            response.Data = listAll.Select(x => new KnowledgePointDto
            {
                Id = x.Id,
                GroupId=x.GroupId,
                Name = x.Name,
                ParentId = x.ParentId,
                Sort = x.Sort,
                Status = x.Status
            }).ToList();
            return response;
        }

        /// <summary>
        /// 新增或修改知识点
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<long>> AddOrEditKnowledgePoint(KnowledgePointDto model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "知识点名称不能为空" };
            if (DbContext.FreeSql.Select<YssxKnowledgePoint>().Any(m => m.Name == model.Name && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "知识点名称重复" };

            bool state = true;
            YssxKnowledgePoint entity = new YssxKnowledgePoint();
            if (model.Id == 0)
            {
                long id = IdWorker.NextId();
                entity = await DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().InsertAsync(new YssxKnowledgePoint
                {
                    Id = id,
                    GroupId = model.ParentId == 0 ? id : model.GroupId,
                    Name = model.Name,
                    ParentId = model.ParentId,
                    Sort = model.Sort,
                    Status = model.Status,
                    CreateBy = currentUserId
                });
                state = entity != null;
            }
            else
            {
                entity = await DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该知识点" };
                if (entity.ParentId != model.ParentId)
                {
                    //统计上级层级
                    var levelParent = 0;
                    Func<long, long> FindParentLevel = null;
                    FindParentLevel = id =>
                    {
                        var parentKp = DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).First();
                        levelParent++;
                        if (parentKp != null && parentKp.ParentId > 0)
                            FindParentLevel(parentKp.ParentId);
                        return levelParent;
                    };
                    @FindParentLevel(model.ParentId);
                    //统计下级层级
                    var levelChild = 1;
                    Func<long, long> FindChildLevel = null;
                    FindChildLevel = pId =>
                    {
                        var childKp = DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.ParentId == pId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
                        if (childKp.Count > 0)
                        {
                            levelChild++;
                            foreach (var item in childKp)
                            {
                                FindChildLevel(item.Id);
                            }
                        }
                        return levelChild;
                    };
                    @FindChildLevel(model.Id);

                    //验证层级不超过四级
                    if (levelParent + levelChild > 4)
                        return new ResponseContext<long> { Code = CommonConstants.ErrorCode, Msg = "操作失败，知识点的层级不能超过四级，请重新选择合适的父级知识点" };
                }

                entity.Name = model.Name;
                entity.ParentId = model.ParentId;
                entity.Sort = model.Sort;
                entity.Status = model.Status;
                entity.UpdateBy = currentUserId;
                entity.UpdateTime = DateTime.Now;
                //var entity1 = await DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().UpdateAsync(entity);
                var resUpd = await DbContext.FreeSql.Update<YssxKnowledgePoint>().SetSource(entity).UpdateColumns(a => new { a.Name, a.ParentId, a.Sort, a.UpdateBy, a.UpdateTime, a.Status }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<long> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = entity.Id, Msg = state ? "成功" : "操作失败" };
        }

        /// <summary>
        /// 删除知识点
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveKnowledgePoint(long id, long currentUserId)
        {
            var dtNow = DateTime.Now;
            YssxKnowledgePoint entity = await DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该知识点!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = dtNow;

            #region 取子级数据
            List<YssxKnowledgePoint> listDel = new List<YssxKnowledgePoint>();
            //取出所有数据
            var rKnowledgePointData = await DbContext.FreeSql.GetGuidRepository<YssxKnowledgePoint>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.Sort).OrderByDescending(x => x.CreateTime).ToListAsync();
            //筛选有效数据
            List<YssxKnowledgePoint> list = rKnowledgePointData.Where(x => x.ParentId == id).ToList();
            if (list.Count > 0)
            {
                listDel.AddRange(list);

                //筛选下级层级
                Func<long, List<YssxKnowledgePoint>> FindChildLevel = null;
                FindChildLevel = pId =>
                {
                    var childKp = rKnowledgePointData.Where(x => x.ParentId == pId).ToList();
                    if (childKp.Count > 0)
                    {
                        listDel.AddRange(childKp);
                        foreach (var item in childKp)
                        {
                            FindChildLevel(item.Id);
                        }
                    }
                    return listDel;
                };

                foreach (var item in list)
                {
                    @FindChildLevel(item.Id);
                }
                if (listDel.Count > 0)
                {
                    listDel.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = currentUserId;
                        a.UpdateTime = dtNow;
                    });
                }
            }
            #endregion

            return await Task.Run(() =>
            {
                bool state = DbContext.FreeSql.GetRepository<YssxKnowledgePoint>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
                if (listDel.Count > 0)
                {
                    DbContext.FreeSql.Update<YssxKnowledgePoint>().SetSource(listDel).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                }

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
            });
        }

        #endregion

        #region 行业
        /// <summary>
        /// 获取行业列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<IndustryDto>> GetIndustryList(IndustryRequest model)
        {
            List<YssxIndustry> list = await DbContext.FreeSql.GetGuidRepository<YssxIndustry>().Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Name), x => x.Name.Contains(model.Name))
                .OrderBy(x => x.Sort).OrderByDescending(x => x.CreateTime).ToListAsync();
            var totalCount = list.Count();
            var selectData = list.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).Select(x => new IndustryDto
            {
                Id = x.Id,
                Name = x.Name,
                Sort = x.Sort,
                Description = x.Description,
                FileUrl = x.FileUrl,
                Status = x.Status
            }).ToList();
            return new PageResponse<IndustryDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
        }

        /// <summary>
        /// 新增或修改行业
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditIndustry(IndustryDto model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "行业名称不能为空" };
            if (DbContext.FreeSql.Select<YssxIndustry>().Any(m => m.Name == model.Name && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "行业名称重复" };

            bool state = true;
            if (model.Id == 0)
            {
                var entity = await DbContext.FreeSql.GetRepository<YssxIndustry>().InsertAsync(new YssxIndustry
                {
                    Id = IdWorker.NextId(),
                    Name = model.Name,
                    Sort = model.Sort,
                    Description = model.Description,
                    FileUrl = model.FileUrl,
                    Status = model.Status,
                    CreateBy = currentUserId
                });
                state = entity != null;
            }
            else
            {
                YssxIndustry entity = await DbContext.FreeSql.GetRepository<YssxIndustry>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该行业!" };

                entity.Name = model.Name;
                entity.Sort = model.Sort;
                entity.Description = model.Description;
                entity.FileUrl = model.FileUrl;
                entity.Status = model.Status;
                entity.UpdateBy = currentUserId;
                entity.UpdateTime = DateTime.Now;
                var resUpd = await DbContext.FreeSql.Update<YssxIndustry>().SetSource(entity).UpdateColumns(a => new { a.Name, a.Sort, a.Description, a.FileUrl, a.UpdateBy, a.UpdateTime, a.Status }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// 删除行业
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveIndustry(long id, long currentUserId)
        {
            YssxIndustry entity = await DbContext.FreeSql.GetRepository<YssxIndustry>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该行业!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<YssxIndustry>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }

        #endregion

        #region 业务场景
        /// <summary>
        /// 获取业务场景列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<BusinessSceneDto>> GetBusinessSceneList(BusinessSceneRequest model)
        {
            List<YssxBusinessScene> list = await DbContext.FreeSql.GetGuidRepository<YssxBusinessScene>().Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Name), x => x.Name.Contains(model.Name))
                .OrderBy(x => x.Sort).OrderByDescending(x => x.CreateTime).ToListAsync();
            var totalCount = list.Count();
            var selectData = list.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).Select(x => new BusinessSceneDto
            {
                Id = x.Id,
                Name = x.Name,
                Sort = x.Sort,
                Description = x.Description,
                FileUrl = x.FileUrl,
                Status = x.Status
            }).ToList();
            return new PageResponse<BusinessSceneDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
        }

        /// <summary>
        /// 新增或修改业务场景
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditBusinessScene(BusinessSceneDto model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "业务场景名称不能为空" };
            if (DbContext.FreeSql.Select<YssxBusinessScene>().Any(m => m.Name == model.Name && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "业务场景名称重复" };

            bool state = true;
            if (model.Id == 0)
            {
                var entity = await DbContext.FreeSql.GetRepository<YssxBusinessScene>().InsertAsync(new YssxBusinessScene
                {
                    Id = IdWorker.NextId(),
                    Name = model.Name,
                    Sort = model.Sort,
                    Description = model.Description,
                    FileUrl = model.FileUrl,
                    Status = model.Status,
                    CreateBy = currentUserId
                });
                state = entity != null;
            }
            else
            {
                YssxBusinessScene entity = await DbContext.FreeSql.GetRepository<YssxBusinessScene>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该业务场景!" };

                entity.Name = model.Name;
                entity.Sort = model.Sort;
                entity.Description = model.Description;
                entity.FileUrl = model.FileUrl;
                entity.Status = model.Status;
                entity.UpdateBy = currentUserId;
                entity.UpdateTime = DateTime.Now;
                var resUpd = await DbContext.FreeSql.Update<YssxBusinessScene>().SetSource(entity).UpdateColumns(a => new { a.Name, a.Sort, a.Description, a.FileUrl, a.UpdateBy, a.UpdateTime, a.Status }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// 删除业务场景
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveBusinessScene(long id, long currentUserId)
        {
            YssxBusinessScene entity = await DbContext.FreeSql.GetRepository<YssxBusinessScene>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该业务场景!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<YssxBusinessScene>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }

        #endregion

        #region 操作日志
        public async Task<PageResponse<OperationLogDto>> OperationLog(OperationLogRequest model, long oId)
        {
            long opreationId = oId;//操作人ID

            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<OperationLog>()
                .Where(m => m.IsDelete == CommonConstants.IsNotDelete&&m.Module==model.Module).OrderByDescending(a => a.CreateTime);

                if (model.Type == 0)
                    select = select.Where(a => a.ModuleId == model.Id && a.CreateBy == opreationId);
                if (model.Type == 1)
                    select = select.Where(a => a.ModuleId == model.Id);

                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("id,Content,CreateTime");
                var items = DbContext.FreeSql.Ado.Query<OperationLogDto>(sql);

                for (int i = 0; i < items.Count; i++)
                    items[i].Content = System.Web.HttpUtility.UrlDecode(items[i].Content, Encoding.UTF8);


                return new PageResponse<OperationLogDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }
        #endregion

        #region Python接口Token

        /// <summary>
        /// 获取Python接口Token
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<string>> GetPythonToken()
        {
            var entity = await DbContext.FreeSql.GetRepository<YssxPythonToken>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            var rToken = entity == null ? "" : entity.Token;

            return new ResponseContext<string> { Code =  CommonConstants.SuccessCode, Data = rToken, Msg = "成功" };
        }

        #endregion

    }
}
