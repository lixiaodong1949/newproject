﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Entity;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 学期服务
    /// </summary>
    public class SemesterService : ISemesterService
    {
        #region 新增/编辑学期
        /// <summary>
        /// 新增/编辑学期
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditSemester(SemesterDto dto, long currentUserId)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };


            YssxSemester entity = new YssxSemester
            {
                Id = dto.Id > 0 ? dto.Id : IdWorker.NextId(),
                Name = dto.Name,
                Sort = dto.Sort,
                IsDisabled = dto.IsDisabled,                
                CreateBy = currentUserId,
                CreateTime = DateTime.Now,
                UpdateBy = currentUserId,
                UpdateTime = DateTime.Now
            };

            bool state = false;

            if (dto.Id <= 0)
            {
                var isExist = DbContext.FreeSql.Select<YssxSemester>().Any(x => x.Name == dto.Name);
                if (isExist)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学期名称不能重复!" };

                YssxSemester model = await DbContext.FreeSql.GetRepository<YssxSemester>().InsertAsync(entity);
                state = model != null;
            }
            else
            {
                var isExist = DbContext.FreeSql.Select<YssxSemester>().Any(x => x.Name == dto.Name && x.Id != dto.Id);
                if (isExist)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学期名称不能重复" };

                state = DbContext.FreeSql.GetRepository<YssxSemester>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                { x.Name, x.Sort, x.IsDisabled, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 获取学期列表
        /// <summary>
        /// 获取学期列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxSemesterViewModel>>> GetSemesterList(YssxSemesterQuery query)
        {
            ResponseContext<List<YssxSemesterViewModel>> response = new ResponseContext<List<YssxSemesterViewModel>>();

            FreeSql.ISelect<YssxSemester> select = DbContext.FreeSql.GetRepository<YssxSemester>().Select.Where(s => s.IsDelete == CommonConstants.IsNotDelete);

            if (query.IsDisabled.HasValue)
                select.Where(x => x.IsDisabled == query.IsDisabled.Value);
            if (query != null && !string.IsNullOrEmpty(query.Name))
                select.Where(x => x.Name.Contains(query.Name));

            response.Data = await select.OrderByDescending(x => x.Sort).ToListAsync(x => new YssxSemesterViewModel
            {
                Id = x.Id,
                Name = x.Name,
                Sort = x.Sort,
                IsDisabled = x.IsDisabled,
                CreateTime = x.CreateTime,
                UpdateTime = x.UpdateTime,
            });

            return response;
        }
        #endregion

        #region 删除学期
        /// <summary>
        /// 删除学期
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveSemester(long id, long currentUserId)
        {
            YssxSemester entity = await DbContext.FreeSql.GetRepository<YssxSemester>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = currentUserId;

            bool state = DbContext.FreeSql.GetRepository<YssxSemester>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

    }
}
