﻿using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Profile;
using Aliyun.Acs.Dysmsapi.Model.V20170525;
using System;
using System.Collections.Generic;
using System.Text;
using Tas.Common.Configurations;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl.Web
{
    /// <summary>
    /// 发送验证码
    /// </summary>
    public class DysmsService
    {
        public static ResponseContext<bool> SendSms(string phone, string sms)
        {
            string product = "Dysmsapi";
            string domain = "dysmsapi.aliyuncs.com";
            string accessKeyId = AppSettingConfig.GetSection("accessKeyId") ?? "LTAI5ArzTDAPOll8";
            string accessKeySecret = AppSettingConfig.GetSection("accessKeySecret") ?? "ZFiPSOKiPPNAE9lydt5GHv4cAt3T7D";
            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();
            try
            {
                request.PhoneNumbers = phone;
                request.SignName = AppSettingConfig.GetSection("SignName") ?? "云上实训";
                request.TemplateCode = "SMS_190215218";
                request.TemplateParam = sms;
                SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);
                if (sendSmsResponse.Message != "OK")
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false ,Msg = sendSmsResponse.Message };
                }
            }
            catch (Exception ex)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = ex.Message};
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 短信服务
        /// </summary>
        /// <returns></returns>
        public static ResponseContext<bool> SendSms(string phone,int? type)
        {
            bool state = false;
            var user = DbContext.FreeSql.Select<YssxUser>().Where(x => x.MobilePhone == phone).First();
            if (type==1)
            {
                if(user==null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码未注册!", Data = false };
            }

            if (type==2)
            {
                if(user!=null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "该手机号码已存在，请重新输入!", Data = false };
            }

            String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）
            String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
            String accessKeyId = AppSettingConfig.GetSection("accessKeyId") ?? "LTAI5ArzTDAPOll8";//你的accessKeyId，参考本文档步骤2
            String accessKeySecret = AppSettingConfig.GetSection("accessKeySecret") ?? "ZFiPSOKiPPNAE9lydt5GHv4cAt3T7D";//你的accessKeySecret，参考本文档步骤2
            int minutes = 0;
            if (!Int32.TryParse(AppSettingConfig.GetSection("SmsMinutes"), out minutes))
            {
                minutes = 15;
            }

            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            //IAcsClient client = new DefaultAcsClient(profile);
            // SingleSendSmsRequest request = new SingleSendSmsRequest();
            //初始化ascClient,暂时不支持多region（请勿修改）
            bool isNeverExpire = false;
            //DefaultProfile defa = new DefaultProfile("cn-hangzhou").AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain, isNeverExpire);
            DefaultProfile.AddEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();
            try
            {
                //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式，发送国际/港澳台消息时，接收号码格式为00+国际区号+号码，如“0085200000000”
                request.PhoneNumbers = phone;
                //必填:短信签名-可在短信控制台中找到
                request.SignName = AppSettingConfig.GetSection("SignName") ?? "云上实训";
                //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
                request.TemplateCode = AppSettingConfig.GetSection("TemplateCode") ?? "SMS_180052960";
                //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
                string verifyCode = IdWorker.Number(4, true);
                request.TemplateParam = "{\"code\":\"" + verifyCode + "\"}"; //"{\"name\":\"Tom\",\"code\":\"123\"}";
                //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
                //request.OutId = "yourOutId";
                //请求失败这里会抛ClientException异常
                SendSmsResponse sendSmsResponse = acsClient.GetAcsResponse(request);

                state = sendSmsResponse.Message == "OK";
                if (state)
                {
                    YssxVerifyCode entity = new YssxVerifyCode { Id = IdWorker.NextId(), Code = verifyCode, MobilePhone = phone, CreateTime = DateTime.Now, ExpirTime = DateTime.Now.AddMinutes(minutes) };
                    DbContext.FreeSql.Insert(entity).ExecuteAffrows();
                    //new YssxVerifyCodeRepository().Add($"{phone}_{verifyCode}", entity, 3000);
                }
            }
            catch (Exception ex)
            {
                //return ex.Message;
            }

            return  new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state };
        }
    }
}
