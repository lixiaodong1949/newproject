﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.Pocos.SceneTraining;

namespace Yssx.S.ServiceImpl
{
    public class CourseProductService : ICourseProductService
    {
        public async Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesListByUid(long courseId, long uid, int fileType)
        {
            ResponseContext<List<YssxSectionFilesDto>> response = new ResponseContext<List<YssxSectionFilesDto>>();
            response.Data = new List<YssxSectionFilesDto>();
            bool isExistOrder = false;
            if (uid > 0)
            {
                isExistOrder = await IsExistOrderByUid(courseId, uid);
            }
            var select = DbContext.FreeSql.GetRepository<YssxSectionFiles>().Select.Where(a => a.CourseId == courseId && a.IsDelete == CommonConstants.IsNotDelete);
            switch (fileType)
            {
                case 1: select.Where(x => x.FilesType != 6 && x.SectionType == 1); break;
                case 2: select.Where(x => x.FilesType == 6 && x.SectionType == 3); break;
                case 3: select.Where(x => x.SectionType == 2); break;
                default:
                    select.Where(x => x.FilesType == 1);
                    break;
            }

            if (!isExistOrder)
            {
                var sql = select.Page(1, 3).ToSql("id,File,FileName,FilesType,SectionId,SectionType,CourseId");
                var list = await DbContext.FreeSql.Ado.QueryAsync<YssxSectionFilesDto>(sql);
                response.Data = list;
            }
            else
            {
                var sql = select.ToSql("id,File,FileName,FilesType,SectionId,SectionType,CourseId");
                var list = await DbContext.FreeSql.Ado.QueryAsync<YssxSectionFilesDto>(sql);
                response.Data = list;
            }

            return response;
        }

        public async Task<ResponseContext<List<YssxSectionDto>>> GetSectionListByUid(long courseId, long uid)
        {
            ResponseContext<List<YssxSectionDto>> response = new ResponseContext<List<YssxSectionDto>>();
            bool isExistOrder = false;
            var select = DbContext.FreeSql.GetRepository<YssxSection>().Where(x => x.CourseId == courseId && x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.Sort);
            if (uid > 0)
            {
                isExistOrder = await IsExistOrderByUid(courseId, uid);
            }

            //if (!isExistOrder)
            //{
            //    var sql = select.Page(1, 3).ToSql("id,SectionName,SectionTitle,ParentId,SectionType,CourseId");
            //    var list = await DbContext.FreeSql.Ado.QueryAsync<YssxSectionDto>(sql);
            //    response.Data = list;
            //}
            //else
            //{
            //    var sql = select.ToSql("id,SectionName,SectionTitle,ParentId,SectionType,CourseId");
            //    var list = await DbContext.FreeSql.Ado.QueryAsync<YssxSectionDto>(sql);
            //    response.Data = list;
            //}

            var sql = select.ToSql("id,SectionName,SectionTitle,ParentId,SectionType,CourseId");
            var list = await DbContext.FreeSql.Ado.QueryAsync<YssxSectionDto>(sql);
            response.Data = list;

            return response;
        }

        private async Task<bool> IsExistOrderByUid(long courseId, long uid)
        {
            return await DbContext.FreeSql.GetGuidRepository<YssxCourseOrder>().Where(x => x.CustomerId == uid && x.CourseId == courseId && x.OrderStatus == (int)OrderStatus.PaymentSuccess).AnyAsync();
        }

        public async Task<ResponseContext<List<YssxSectionTopicDto>>> GetYssxSectionTopicListByUid(long sid, long uid)
        {
            ResponseContext<List<YssxSectionTopicDto>> response = new ResponseContext<List<YssxSectionTopicDto>>();
            List<YssxSectionTopicDto> list = await DbContext.FreeSql.GetRepository<YssxSectionTopic>().Select.From<YssxTopicPublic>((a, b) => a.LeftJoin(aa => aa.QuestionId == b.Id)).Where((a, b) => a.SectionId == sid && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete).OrderBy((a, b) => b.Sort).ToListAsync((a, b) => new YssxSectionTopicDto
            {
                Id = a.Id,
                KnowledgePointId = a.KnowledgePointId,
                QuestionId = a.QuestionId,
                SectionId = a.SectionId,
                QuetionName = b.Title,
                Sort = b.Sort,
                QuestionType = (int)b.QuestionType,
                CourseId = a.CourseId,
            });
            response.Data = list;
            return response;
        }

        public async Task<ResponseContext<int>> CourseDetailsPage(long cid, long uid)
        {
            ResponseContext<int> response = new ResponseContext<int>();
            bool isExistOrder = false;
            if (uid > 0)
            {
                isExistOrder = await IsExistOrderByUid(cid, uid);
            }
            YssxCourseCount courseCount = await DbContext.FreeSql.GetRepository<YssxCourseCount>().Where(x => x.CourseId == cid).FirstAsync();

            if (courseCount != null)
            {
                await DbContext.FreeSql.GetRepository<YssxCourseCount>().UpdateDiy.Set(x => x.ReadCount == courseCount.ReadCount + 1).Where(x => x.CourseId == cid).ExecuteAffrowsAsync();
            }

            if (isExistOrder)
            {
                response.Data = 1;
            }
            return response;

        }

        public async Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesListById(long cid, int fileType)
        {
            ResponseContext<List<YssxSectionFilesDto>> response = new ResponseContext<List<YssxSectionFilesDto>>();
            response.Data = new List<YssxSectionFilesDto>();
         
            var select = DbContext.FreeSql.GetRepository<YssxSectionFiles>().Select.Where(a => a.CourseId == cid && a.IsDelete == CommonConstants.IsNotDelete);
            switch (fileType)
            {
                case 1: select.Where(x => x.FilesType != 6 && x.SectionType == 1); break;
                case 2: select.Where(x => x.FilesType == 6 && x.SectionType == 3); break;
                case 3: select.Where(x => x.SectionType == 2); break;
                default:
                    select.Where(x => x.FilesType == 1);
                    break;
            }

            var sql = select.ToSql("id,File,FileName,FilesType,SectionId,SectionType,CourseId");
            var list = await DbContext.FreeSql.Ado.QueryAsync<YssxSectionFilesDto>(sql);
            response.Data = list;

            return response;
        }

        public async Task<ResponseContext<List<CourseCaseDto>>> GetCourseCaseListById(long cid)
        {
            ResponseContext<List<CourseCaseDto>> response = new ResponseContext<List<CourseCaseDto>>();
            var select = DbContext.FreeSql.Select<YssxSectionCase>().From<YssxSceneTraining>((a,b)=> a.InnerJoin(o=> o.CaseId==b.Id)).Where((a,b) => a.CourseId == cid && a.IsDelete == CommonConstants.IsNotDelete &&b.IsDelete == CommonConstants.IsNotDelete);
            var sql = select.ToSql("a.CourseId,b.id caseId,b.name");
            var list = await DbContext.FreeSql.Ado.QueryAsync<CourseCaseDto>(sql);
            return response;
        }
    }
}
