﻿using FreeSql;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.S.Dto.BigDatas;
using Yssx.S.IServices.BigData;
using Yssx.S.Pocos.BigData;
using Yssx.Framework.AutoMapper;
using DbContext = Yssx.Framework.Dal.DbContext;
using System.IO;
using Microsoft.AspNetCore.StaticFiles;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace Yssx.S.ServiceImpl
{
    public class BigDataService : IBigDataService
    {
        private BaseRepository<YssxBalanceSheet> balancesheetReposition;

        public BigDataService()
        {
            balancesheetReposition = DbContext.BDFreeSql.GetRepository<YssxBalanceSheet>();
        }

        #region 加载大列表
        /// <summary>
        /// 加载大列表
        /// </summary>
        /// <param name="id">表id(1,大数据负债，2，活跃债券统计，3，年金终值系数，4，年金现值系数，5，A股指数0000026，6，金融机构人民币存款基准利率,7,金融机构人民币贷款基准利率,
        /// 8,国债,9,B股指数000003,10,成份B股指数390003, 11,复利终值系数，12，企业债，13，披露财务指标，14，LPR数据，
        /// 15，LPR均值数据，16，中期票据(AAA)2017，17，其他指数，18，政策性金融债（国开行）2017，19，政策性金融债（进出口行、开发行）2017，20，利润表，21，红利分配表，
        /// 22，人民币参考汇率_2014，23，人民币参考汇率_2015，24，人民币参考汇率_2016，25，人民币参考汇率_2017，26，人民币参考汇率_2018，27，人民币汇率指数，28，深证成份指数390001，
        /// 29，深证综指399106，30，深证成指R390002，31，Shibor数据， 32，Shibor均值数据，33，Shibor报价数据，34，短期融资债2017，35，统计数据CPI，
        /// 36，人民币月平均汇率，37，人民币汇率中间价，38，创业板指数399006，39，复利现值系数，40，沪深300指数399300，41，上证综指000001，42，国债2017)</param>
        /// <param name="pageindex">页码</param>
        /// <param name="pagesize">页数</param>
        /// <returns></returns>
        public async Task<ResponseContext<BigDataDto>> GetObjectList(int id, int pageIndex, int pageSize, string year, int parentId)
        {
            return await GetTablebList(id, pageIndex, pageSize, year, parentId);
        }

        #region 加载大列表私有方法
        private async Task<ResponseContext<BigDataDto>> GetTablebList(int id, int pageIndex, int pageSize, string year, int parentId)
        {
            ResponseContext<BigDataDto> result = new ResponseContext<BigDataDto>();
            switch (id)
            {
                case 1:
                    result = await GetBalanceSheetList<BalanceSheetDto, YssxBalanceSheet>(pageIndex, pageSize, year, parentId);
                    break;
                case 2:
                    result = await GetBalanceSheetList<ActiveBondStatisticsDto, YssxBdActiveBondStatistics>(pageIndex, pageSize, year, parentId);
                    break;
                case 3:
                    result = await GetBalanceSheetList<AnnuityFinalValueCoefficientDto, YssxBdAnnuityFinalValueCoefficient>(pageIndex, pageSize, year, parentId);
                    break;
                case 4:
                    result = await GetBalanceSheetList<AnnuityPresentValueFactorDto, YssxBdAnnuityPresentValueFactor>(pageIndex, pageSize, year, parentId);
                    break;
                case 5:
                    result = await GetBalanceSheetList<AShareIndex000002Dto, YssXBdAShareIndex000002>(pageIndex, pageSize, year, parentId);
                    break;
                case 6:
                    result = await GetBalanceSheetList<BenchmarkRMBDepositRateOfFinancialInstitutionsDto, YssxBdBenchmarkRMBDepositRateOfFinancialInstitutions>(pageIndex, pageSize, year, parentId);
                    break;
                case 7:
                    result = await GetBalanceSheetList<BenchmarkRMBLendingRateForFinancialInstitutionsDto, YssxBdBenchmarkRMBLendingRateForFinancialInstitutions>(pageIndex, pageSize, year, parentId);
                    break;
                case 8:
                    result = await GetBalanceSheetList<BondsAreDebt2017Dto, YssxBdBondsAreDebt2017>(pageIndex, pageSize, year, parentId);
                    break;
                case 9:
                    result = await GetBalanceSheetList<BShareIndex000003Dto, YssxBdBShareIndex000003>(pageIndex, pageSize, year, parentId);
                    break;
                case 10:
                    result = await GetBalanceSheetList<ComponentBShareIndex390003Dto, YssxBdComponentBShareIndex390003>(pageIndex, pageSize, year, parentId);
                    break;
                case 11:
                    result = await GetBalanceSheetList<CompoundFinalValueCoefficientDto, YssxBdCompoundFinalValueCoefficient>(pageIndex, pageSize, year, parentId);
                    break;
                case 12:
                    result = await GetBalanceSheetList<CorporateBondsDto, YssxBdCorporateBonds>(pageIndex, pageSize, year, parentId);
                    break;
                case 13:
                    result = await GetBalanceSheetList<DisclosureOfFinancialIndicatorsDto, YssxBdDisclosureOfFinancialIndicators>(pageIndex, pageSize, year, parentId);
                    break;
                case 14:
                    result = await GetBalanceSheetList<LPRDataDto, YssxBdLPRData>(pageIndex, pageSize, year, parentId);
                    break;
                case 15:
                    result = await GetBalanceSheetList<LPRMeanDataDto, YssxBdLPRMeanData>(pageIndex, pageSize, year, parentId);
                    break;
                case 16:
                    result = await GetBalanceSheetList<MediumTermNotes2017Dto, YssxBdMediumTermNotes2017>(pageIndex, pageSize, year, parentId);
                    break;
                case 17:
                    result = await GetBalanceSheetList<OtherIndicatorsDto, YssxBdOtherIndicators>(pageIndex, pageSize, year, parentId);
                    break;
                case 18:
                    result = await GetBalanceSheetList<PolicyBasedFinancialBondsDto, YssxBdPolicyBasedFinancialBonds2017>(pageIndex, pageSize, year, parentId);
                    break;
                case 19:
                    result = await GetBalanceSheetList<ImportAndExportBanksAndIssuanceOfPolicyBasedFinancialBondsDto, YssxBdPolicyFinancialBond2017>(pageIndex, pageSize, year, parentId);
                    break;
                case 20:
                    result = await GetBalanceSheetList<ProfitDto, YssxBdProfit>(pageIndex, pageSize, year, parentId);
                    break;
                case 21:
                    result = await GetBalanceSheetList<DividendDistributionModelDto, YssxBdProfitSharing>(pageIndex, pageSize, year, parentId);
                    break;
                case 22:
                    result = await GetBalanceSheetList<ReferenceRateOfRMB2014Dto, YssxBdReferenceRateOfRMB2014>(pageIndex, pageSize, year, parentId);
                    break;
                case 23:
                    result = await GetBalanceSheetList<ReferenceRateOfRMB2015Dto, YssxBdReferenceRateOfRMB2015>(pageIndex, pageSize, year, parentId);
                    break;
                case 24:
                    result = await GetBalanceSheetList<ReferenceRateOfRMB2016Dto, YssxBdReferenceRateOfRMB2016>(pageIndex, pageSize, year, parentId);
                    break;
                case 25:
                    result = await GetBalanceSheetList<ReferenceRateOfRMB2017Dto, YssxBdReferenceRateOfRMB2017>(pageIndex, pageSize, year, parentId);
                    break;
                case 26:
                    result = await GetBalanceSheetList<ReferenceRateOfRMB2018Dto, YssxBdReferenceRateOfRMB2018>(pageIndex, pageSize, year, parentId);
                    break;
                case 27:
                    result = await GetBalanceSheetList<RMBExchangRateIndexDto, YssxBdRMBExchangRateIndex>(pageIndex, pageSize, year, parentId);
                    break;
                case 28:
                    result = await GetBalanceSheetList<ShenZhenComponentIndex390001Dto, YssxBdShenZhenComponentIndex390001>(pageIndex, pageSize, year, parentId);
                    break;
                case 29:
                    result = await GetBalanceSheetList<ShenzhenCompositeIndex399106Dto, YssxBdShenzhenCompositeIndex399106Model>(pageIndex, pageSize, year, parentId);
                    break;
                case 30:
                    result = await GetBalanceSheetList<ShenZhenStockIndexR390002Dto, YssxBdShenZhenStockIndexR390002>(pageIndex, pageSize, year, parentId);
                    break;
                case 31:
                    result = await GetBalanceSheetList<ShiborDataDto, YssxBdShiborData>(pageIndex, pageSize, year, parentId);
                    break;
                case 32:
                    result = await GetBalanceSheetList<ShiborMeanDataDto, YssxBdShiborMeanData>(pageIndex, pageSize, year, parentId);
                    break;
                case 33:
                    result = await GetBalanceSheetList<ShiborQuoteDataDto, YssxBdShiborQuoteData>(pageIndex, pageSize, year, parentId);
                    break;
                case 34:
                    result = await GetBalanceSheetList<ShortTermFinancingDebt2017Dto, YssxBdShortTermFinancingDebt2017>(pageIndex, pageSize, year, parentId);
                    break;
                case 35:
                    result = await GetBalanceSheetList<StatisticalDataCPIDto, YssxBdStatisticalDataCPI>(pageIndex, pageSize, year, parentId);
                    break;
                case 36:
                    result = await GetBalanceSheetList<TheAverageMonthlyExchangeRateOfRMBDto, YssxBdTheAverageMonthlyExchangeRateOfRMB>(pageIndex, pageSize, year, parentId);
                    break;
                case 37:
                    result = await GetBalanceSheetList<TheCentralParityOfRMBExchangeRateDto, YssxBdTheCentralParityOfRMBExchangeRate>(pageIndex, pageSize, year, parentId);
                    break;
                case 38:
                    result = await GetBalanceSheetList<TheChinextIndexIs399006Dto, YssxBdTheChinextIndexIs399006>(pageIndex, pageSize, year, parentId);
                    break;
                case 39:
                    result = await GetBalanceSheetList<TheCompoundPresentValueFactorDto, YssxBdTheCompoundPresentValueFactor>(pageIndex, pageSize, year, parentId);
                    break;
                case 40:
                    result = await GetBalanceSheetList<TheCsi300IndexIs399300Dto, YssxBdTheCsi300IndexIs399300>(pageIndex, pageSize, year, parentId);
                    break;
                case 41:
                    result = await GetBalanceSheetList<TheShanghaiCompositeIndexIs000001Dto, YssxBdTheShanghaiCompositeIndexIs000001>(pageIndex, pageSize, year, parentId);
                    break;
                case 42:
                    result = await GetBalanceSheetList<TheYieldTreasuryBonds2017Dto, YssxBdTheYieldTreasuryBonds2017Model>(pageIndex, pageSize, year, parentId);
                    break;
                case 43:
                    result = await GetBalanceSheetList<AShareIndex2019Dto, YssxBdAShareIndex2019>(pageIndex, pageSize, year, parentId);
                    break;
                case 44:
                    result = await GetBalanceSheetList<ChinaBoundsDto, YssxBdChinaBounds>(pageIndex, pageSize, year, parentId);
                    break;
                case 45:
                    result = await GetBalanceSheetList<CAPIDto, YssxBdCAPI>(pageIndex, pageSize, year, parentId);
                    break;
                default:
                    break;
            }
            return result;
        }

        private async Task<ResponseContext<BigDataDto>> GetBalanceSheetList<T, T1>(int pageIndex, int pageSize, string year, int parentId) where T1 : class
        {
            ResponseContext<BigDataDto> result = new ResponseContext<BigDataDto>();
            BigDataDto bigData = new BigDataDto();
            var contentModel = new ExcelContentModel();
            var sqlWhere = string.Empty;
            var plist = new List<PropertyInfo>(typeof(T).GetProperties());
            var sql = Framework.Dal.DbContext.BDFreeSql.GetRepository<T1>().Select.ToSql();
            var countSql = DbContext.BDFreeSql.GetRepository<T1>().Select.ToSql("count(Id)");
            var countSelect = DbContext.BDFreeSql.GetRepository<T1>().Select.ToSql();

            //添加年份条件
            //sqlWhere+= year == 2018? " where Year='2018' ": " where Year='2019' ";
            if (year.Equals("2018"))
                sqlWhere += " WHERE Year='2018' ";
            else if (year.Equals("2019"))
                sqlWhere += " WHERE Year='2019' ";
            else if (DateTime.TryParse(year.Trim(), out var date))
            {
                sqlWhere += $" WHERE DATE(Rq)='{date.ToString("yyyy-MM-dd")}'";
                countSelect += $" WHERE DATE(Rq)='{date.ToString("yyyy-MM-dd")}'";
            }

            //添加指数条件
            switch (parentId)
            {
                case 64:
                    sqlWhere += " and Zsdm='000002'";
                    break;
                case 65:
                    sqlWhere += " and Zsdm='000003'";
                    break;
                case 66:
                    sqlWhere += " and Zsdm='399003'";
                    break;
                case 67:
                    sqlWhere += " and Zsdm='399006'";
                    break;
                case 68:
                    sqlWhere += " and Zsdm='399102'";
                    break;
                case 69:
                    sqlWhere += " and Zsdm='000300'";
                    break;
                case 70:
                    sqlWhere += " and Zsdm='000016'";
                    break;
                case 71:
                    sqlWhere += " and Zsdm='000001'";
                    break;
                case 72:
                    sqlWhere += " and Zsdm='399002'";
                    break;
                case 73:
                    sqlWhere += " and Zsdm='399001'";
                    break;
                case 74:
                    sqlWhere += " and Zsdm='399106'";
                    break;
                default:
                    break;
            }

            //总条数
            countSql += sqlWhere;
            var count = DbContext.BDFreeSql.Ado.ExecuteScalar(countSql);

            //分页数据
            sql += sqlWhere;
            sql += $" limit {(pageIndex - 1) * pageSize},{pageSize}";
            var list = await DbContext.BDFreeSql.Ado.QueryAsync<T>(sql);
            var dtoList = list.MapTo<List<T>>();

            var cols = new List<string>();
            List<List<string>> rows = new List<List<string>>();
            List<List<string>> crows = new List<List<string>>();

            //总数据
            if (Convert.ToInt32(count) <= 300)
            {
                var countList = await DbContext.BDFreeSql.Ado.QueryAsync<T1>(countSelect);
                var cdtoList = countList.MapTo<List<T>>();

                cdtoList.ForEach(x =>
                {
                    List<string> rList = new List<string>();
                    for (int i = 0; i < plist.Count; i++)
                    {
                        var o = plist[i].GetValue(x, null);
                        string name = plist[i].Name;
                        rList.Add(o.ToString());
                    }
                    crows.Add(rList);
                });
                contentModel.Crows = crows;
            }


            //获取字段说明
            plist.ForEach(x =>
            {
                var name = x.Name;
                List<CustomAttributeData> v = x.CustomAttributes.ToList();
                var descriptionName = v[0].ConstructorArguments[0].Value.ToString();
                cols.Add(descriptionName);
            });

            //获取行数据
            dtoList.ForEach(x =>
            {
                List<string> rList = new List<string>();
                plist.ForEach(y =>
                {
                    object o = y.GetValue(x, null);
                    string Name = y.Name;
                    rList.Add(o.ToString());
                });
                rows.Add(rList);
            });


            contentModel.Cols = cols;
            contentModel.Rows = rows;

            bigData.Data = contentModel;
            bigData.RecordCount = Convert.ToInt32(count);
            bigData.PageIndex = pageIndex;
            bigData.PageSize = pageSize;
            result.Code = CommonConstants.SuccessCode;
            result.Data = bigData;
            result.Msg = "获取成功";
            return result;
        }
        #endregion
        #endregion

        #region 搜索列表
        /// <summary>
        /// 搜索列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<BigDataDto>> GetSearchList(SearchDto model)
        {
            return await GetTablebList1Async(model);
        }

        #region 搜索私有方法
        public async Task<ResponseContext<BigDataDto>> GetTablebList1Async(SearchDto model)
        {
            ResponseContext<BigDataDto> result = new ResponseContext<BigDataDto>();
            switch (model.Id)
            {
                case 1:
                    result = await TestCompareAsync<BalanceSheetDto, YssxBalanceSheet>(model);
                    break;
                case 2:
                    result = await TestCompareAsync<ActiveBondStatisticsDto, YssxBdActiveBondStatistics>(model);
                    break;
                case 3:
                    result = await TestCompareAsync<AnnuityFinalValueCoefficientDto, YssxBdAnnuityFinalValueCoefficient>(model);
                    break;
                case 4:
                    result = await TestCompareAsync<AnnuityPresentValueFactorDto, YssxBdAnnuityPresentValueFactor>(model);
                    break;
                case 5:
                    result = await TestCompareAsync<AShareIndex000002Dto, YssXBdAShareIndex000002>(model);
                    break;
                case 6:
                    result = await TestCompareAsync<BenchmarkRMBDepositRateOfFinancialInstitutionsDto, YssxBdBenchmarkRMBDepositRateOfFinancialInstitutions>(model);
                    break;
                case 7:
                    result = await TestCompareAsync<BenchmarkRMBLendingRateForFinancialInstitutionsDto, YssxBdBenchmarkRMBLendingRateForFinancialInstitutions>(model);
                    break;
                case 8:
                    result = await TestCompareAsync<BondsAreDebt2017Dto, YssxBdBondsAreDebt2017>(model);
                    break;
                case 9:
                    result = await TestCompareAsync<BShareIndex000003Dto, YssxBdBShareIndex000003>(model);
                    break;
                case 10:
                    result = await TestCompareAsync<ComponentBShareIndex390003Dto, YssxBdComponentBShareIndex390003>(model);
                    break;
                case 11:
                    result = await TestCompareAsync<CompoundFinalValueCoefficientDto, YssxBdCompoundFinalValueCoefficient>(model);
                    break;
                case 12:
                    result = await TestCompareAsync<CorporateBondsDto, YssxBdCorporateBonds>(model);
                    break;
                case 13:
                    result = await TestCompareAsync<DisclosureOfFinancialIndicatorsDto, YssxBdDisclosureOfFinancialIndicators>(model);
                    break;
                case 14:
                    result = await TestCompareAsync<LPRDataDto, YssxBdLPRData>(model);
                    break;
                case 15:
                    result = await TestCompareAsync<LPRMeanDataDto, YssxBdLPRMeanData>(model);
                    break;
                case 16:
                    result = await TestCompareAsync<MediumTermNotes2017Dto, YssxBdMediumTermNotes2017>(model);
                    break;
                case 17:
                    result = await TestCompareAsync<OtherIndicatorsDto, YssxBdOtherIndicators>(model);
                    break;
                case 18:
                    result = await TestCompareAsync<PolicyBasedFinancialBondsDto, YssxBdPolicyBasedFinancialBonds2017>(model);
                    break;
                case 19:
                    result = await TestCompareAsync<ImportAndExportBanksAndIssuanceOfPolicyBasedFinancialBondsDto, YssxBdPolicyFinancialBond2017>(model);
                    break;
                case 20:
                    result = await TestCompareAsync<ProfitDto, YssxBdProfit>(model);
                    break;
                case 21:
                    result = await TestCompareAsync<DividendDistributionModelDto, YssxBdProfitSharing>(model);
                    break;
                case 22:
                    result = await TestCompareAsync<ReferenceRateOfRMB2014Dto, YssxBdReferenceRateOfRMB2014>(model);
                    break;
                case 23:
                    result = await TestCompareAsync<ReferenceRateOfRMB2015Dto, YssxBdReferenceRateOfRMB2015>(model);
                    break;
                case 24:
                    result = await TestCompareAsync<ReferenceRateOfRMB2016Dto, YssxBdReferenceRateOfRMB2016>(model);
                    break;
                case 25:
                    result = await TestCompareAsync<ReferenceRateOfRMB2017Dto, YssxBdReferenceRateOfRMB2017>(model);
                    break;
                case 26:
                    result = await TestCompareAsync<ReferenceRateOfRMB2018Dto, YssxBdReferenceRateOfRMB2018>(model);
                    break;
                case 27:
                    result = await TestCompareAsync<RMBExchangRateIndexDto, YssxBdRMBExchangRateIndex>(model);
                    break;
                case 28:
                    result = await TestCompareAsync<ShenZhenComponentIndex390001Dto, YssxBdShenZhenComponentIndex390001>(model);
                    break;
                case 29:
                    result = await TestCompareAsync<ShenzhenCompositeIndex399106Dto, YssxBdShenzhenCompositeIndex399106Model>(model);
                    break;
                case 30:
                    result = await TestCompareAsync<ShenZhenStockIndexR390002Dto, YssxBdShenZhenStockIndexR390002>(model);
                    break;
                case 31:
                    result = await TestCompareAsync<ShiborDataDto, YssxBdShiborData>(model);
                    break;
                case 32:
                    result = await TestCompareAsync<ShiborMeanDataDto, YssxBdShiborMeanData>(model);
                    break;
                case 33:
                    result = await TestCompareAsync<ShiborQuoteDataDto, YssxBdShiborQuoteData>(model);
                    break;
                case 34:
                    result = await TestCompareAsync<ShortTermFinancingDebt2017Dto, YssxBdShortTermFinancingDebt2017>(model);
                    break;
                case 35:
                    result = await TestCompareAsync<StatisticalDataCPIDto, YssxBdStatisticalDataCPI>(model);
                    break;
                case 36:
                    result = await TestCompareAsync<TheAverageMonthlyExchangeRateOfRMBDto, YssxBdTheAverageMonthlyExchangeRateOfRMB>(model);
                    break;
                case 37:
                    result = await TestCompareAsync<TheCentralParityOfRMBExchangeRateDto, YssxBdTheCentralParityOfRMBExchangeRate>(model);
                    break;
                case 38:
                    result = await TestCompareAsync<TheChinextIndexIs399006Dto, YssxBdTheChinextIndexIs399006>(model);
                    break;
                case 39:
                    result = await TestCompareAsync<TheCompoundPresentValueFactorDto, YssxBdTheCompoundPresentValueFactor>(model);
                    break;
                case 40:
                    result = await TestCompareAsync<TheCsi300IndexIs399300Dto, YssxBdTheCsi300IndexIs399300>(model);
                    break;
                case 41:
                    result = await TestCompareAsync<TheShanghaiCompositeIndexIs000001Dto, YssxBdTheShanghaiCompositeIndexIs000001>(model);
                    break;
                case 42:
                    result = await TestCompareAsync<TheYieldTreasuryBonds2017Dto, YssxBdTheYieldTreasuryBonds2017Model>(model);
                    break;
                case 43:
                    result = await TestCompareAsync<AShareIndex2019Dto, YssxBdAShareIndex2019>(model);
                    break;
                case 44:
                    result = await TestCompareAsync<ChinaBoundsDto, YssxBdChinaBounds>(model);
                    break;
                case 45:
                    result = await TestCompareAsync<CAPIDto, YssxBdCAPI>(model);
                    break;
                default:
                    break;
            }
            return result;
        }

        private async Task<ResponseContext<BigDataDto>> TestCompareAsync<T, T1>(SearchDto sear) where T1 : class
        {
            var sqlColumn = string.Empty;
            var contentModel = new ExcelContentModel();
            var select = string.Empty;
            ResponseContext<BigDataDto> result = new ResponseContext<BigDataDto>();
            var bigData = new BigDataDto();
            var cols = new List<string>();
            List<List<string>> rows = new List<List<string>>();
            List<List<string>> crows = new List<List<string>>();
            var plist = new List<PropertyInfo>(typeof(T).GetProperties());

            try
            {
                var sqlWhere = SelectSql(sear);

                if (sear.Ecols.Any() && plist.Count - 1 != sear.Ecols.Count)
                {
                    for (int i = 0; i < sear.Ecols.Count; i++)
                    {
                        sqlColumn += string.IsNullOrEmpty(sqlColumn) ? sear.Ecols[i] : $",{sear.Ecols[i]}";
                    }
                }

                select = string.IsNullOrEmpty(sqlColumn) ? DbContext.BDFreeSql.Select<T1>().ToSql() : DbContext.BDFreeSql.Select<T1>().ToSql(sqlColumn);

                select += $" {sqlWhere}";

                //总条数
                var countSelect = DbContext.BDFreeSql.Select<T1>().ToSql("count(Id)");
                countSelect += $" {sqlWhere}";
                var recordCount = DbContext.BDFreeSql.Ado.ExecuteScalar(countSelect);

                //总数据
                countSelect = select + $" limit 0,300";
                var countList = await DbContext.BDFreeSql.Ado.QueryAsync<T1>(countSelect);


                //分页数据
                select = select + $" limit {(sear.PageIndex - 1) * sear.PageSize},{sear.PageSize}";
                var selectList = await DbContext.BDFreeSql.Ado.QueryAsync<T1>(select);

                //获取字段说明
                if (sear.Cols.Any())
                {
                    cols = sear.Cols;
                }
                else
                {
                    foreach (var item in plist)
                    {
                        var name = item.Name;
                        List<CustomAttributeData> v = item.CustomAttributes.ToList();
                        var descriptionName = v[0].ConstructorArguments[0].Value.ToString();
                        cols.Add(descriptionName);
                    }
                }

                if (selectList.Count > 0)
                {
                    var dtoList = selectList.MapTo<List<T>>();

                    //获取行数据
                    foreach (T dto in dtoList)
                    {
                        List<string> rList = new List<string>();

                        for (int i = 0; i < sear.Ecols.Count; i++)
                        {
                            string name = plist.FirstOrDefault(x => x.Name.ToLower() == sear.Ecols[i].ToLower()).Name;
                            var o = plist.FirstOrDefault(x => x.Name.ToLower() == sear.Ecols[i].ToLower()).GetValue(dto, null);
                            if (o == null) continue;
                            switch (name)
                            {
                                case "AccountPeriod":
                                case "Rq":
                                case "Date":
                                //case "Ydbs":
                                case "Ykprq":
                                case "Ysprq":
                                case "Dqr":
                                case "Jxrq":
                                case "Tjrq":
                                case "Dqrq":
                                case "Tzsj":
                                case "Jzrq":
                                case "Yaggrq":
                                case "Gddhzkrq":
                                case "Gddhggfbrq":
                                case "Ssfaggrq":
                                case "Gqdjr":
                                case "Cqcxr":
                                case "Pxr":
                                    //case "Tjqj":

                                    rList.Add(DateFromat(o.ToString()));
                                    break;
                                default:
                                    rList.Add(o.ToString());
                                    break;
                            }
                        }
                        rows.Add(rList);
                    }
                }

                if (countList.Count > 0)
                {
                    var dtoList = countList.MapTo<List<T>>();
                    //获取行数据
                    foreach (T dto in dtoList)
                    {
                        List<string> rList = new List<string>();

                        for (int i = 0; i < sear.Ecols.Count; i++)
                        {
                            string name = plist.FirstOrDefault(x => x.Name.ToLower() == sear.Ecols[i].ToLower()).Name;
                            var o = plist.FirstOrDefault(x => x.Name.ToLower() == sear.Ecols[i].ToLower()).GetValue(dto, null);
                            if (o == null) continue;
                            switch (name)
                            {
                                case "AccountPeriod":
                                case "Rq":
                                case "Date":
                                //case "Ydbs":
                                case "Ykprq":
                                case "Ysprq":
                                case "Dqr":
                                case "Jxrq":
                                case "Tjrq":
                                case "Dqrq":
                                case "Tzsj":
                                case "Jzrq":
                                case "Yaggrq":
                                case "Gddhzkrq":
                                case "Gddhggfbrq":
                                case "Ssfaggrq":
                                case "Gqdjr":
                                case "Cqcxr":
                                case "Pxr":
                                    //case "Tjqj":

                                    rList.Add(DateFromat(o.ToString()));
                                    break;
                                default:
                                    rList.Add(o.ToString());
                                    break;
                            }
                        }
                        crows.Add(rList);
                    }
                    contentModel.Cols = cols;
                    contentModel.Rows = rows;
                    contentModel.Crows = crows;
                }

                bigData.RecordCount = (long)recordCount;
                bigData.Data = contentModel;
                bigData.PageIndex = sear.PageIndex;
                bigData.PageSize = sear.PageSize;
                result.Data = bigData;
                result.Code = CommonConstants.SuccessCode;
                result.Msg = "获取成功";
            }
            catch (Exception ex)
            {

                result.Msg = "查询失败!请检查你的查询条件.";
            }

            return result;
        }

        string FilterDate(string field, string val)
        {
            string s = $"{field}='{val}'";
            switch (field)
            {
                case "AccountPeriod":
                case "Rq":
                case "Date":
                    s = $"DATE_FORMAT(AccountPeriod,'%Y-%c-%e')='{DateFromat(val)}'";
                    break;
                default:
                    break;
            }

            return s;
        }

        /// <summary>
        /// 拼接sql语句
        /// </summary>
        /// <param name="sear"></param>
        /// <returns></returns>
        private string SelectSql(SearchDto sear)
        {
            var sqlWhere = string.Empty;

            //添加年份条件
            if (sear.Year.Equals("2018"))
                sqlWhere += " WHERE Year='2018' ";
            else if (sear.Year.Equals("2019"))
                sqlWhere += " WHERE Year='2019' ";
            else if (DateTime.TryParse(sear.Year.Trim(), out var date))
                sqlWhere += $" WHERE DATE(Rq)='{date.ToString("yyyy-MM-dd")}'";

            //添加指数条件
            switch (sear.ParentId)
            {
                case 64:
                    sqlWhere += " AND Zsdm='000002'";
                    break;
                case 65:
                    sqlWhere += " AND Zsdm='000003'";
                    break;
                case 66:
                    sqlWhere += " AND Zsdm='399003'";
                    break;
                case 67:
                    sqlWhere += " AND Zsdm='399006'";
                    break;
                case 68:
                    sqlWhere += " AND Zsdm='399102'";
                    break;
                case 69:
                    sqlWhere += " AND Zsdm='000300'";
                    break;
                case 70:
                    sqlWhere += " AND Zsdm='000016'";
                    break;
                case 71:
                    sqlWhere += " AND Zsdm='000001'";
                    break;
                case 72:
                    sqlWhere += " AND Zsdm='399002'";
                    break;
                case 73:
                    sqlWhere += " AND Zsdm='399001'";
                    break;
                case 74:
                    sqlWhere += " AND Zsdm='399106'";
                    break;
                default:
                    break;
            }
            //拼接sql
            if (sear.Rows.Any())
            {

                for (int i = 0; i < sear.Rows.Count; i++)
                {
                    var str = SpelCondition(sear.Rows[i]);
                    if (!string.IsNullOrEmpty(str))
                    {
                        if (i == 0)
                        {
                            //if (sear.Rows[i].LeftParentheses)
                            //sqlWhere += "(";
                            sqlWhere += $" AND ({str}";
                            //else
                            //sqlWhere += $" AND {str}";
                        }
                        else
                        {
                            if (sear.Rows[i].LeftParentheses)
                            {
                                switch (sear.Rows[i].Way)
                                {
                                    case Way.And:
                                        sqlWhere += $" AND ({str}";
                                        break;
                                    case Way.Or:
                                        sqlWhere += $" OR ({str}";
                                        break;
                                }
                            }
                            else
                            {
                                switch (sear.Rows[i].Way)
                                {
                                    case Way.And:
                                        sqlWhere += $" AND {str}";
                                        break;
                                    case Way.Or:
                                        sqlWhere += $" OR {str}";
                                        break;
                                }
                            }
                        }

                        if (sear.Rows[i].RightParentheses)
                            sqlWhere += ")";

                        if (i == sear.Rows.Count - 1)
                        {
                            sqlWhere += ")";
                        }
                    }
                }
            }

            return sqlWhere;
        }

        string DateFromat(string date)
        {
            string ds = date;
            DateTime dt = DateTime.Now;
            if (DateTime.TryParse(date, out dt))
            {
                ds = dt.ToString("yyyy-M-d");
            }
            return ds;
        }

        bool IsDate(string filter)
        {
            switch (filter)
            {
                case "AccountPeriod":
                case "Rq":
                case "Date":
                case "Ykprq":
                case "Ysprq":
                case "Dqr":
                case "Jxrq":
                case "Tjrq":
                case "Dqrq":
                case "Tzsj":
                case "Jzrq":
                case "Yaggrq":
                case "Gddhzkrq":
                case "Gddhggfbrq":
                case "Ssfaggrq":
                case "Gqdjr":
                case "Cqcxr":
                case "Pxr":
                    //case "Tjqj":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// 拼接条件
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private string SpelCondition(Row row)
        {
            var spelStr = string.Empty;
            if (row.Conditions == Conditions.Null)
            {
                return $"{row.Field} is null or {row.Field}=''";
            }
            else if (row.Conditions == Conditions.NotNull)
            {
                return $"{row.Field} is not null and {row.Field}<>''";
            }

            else if (!string.IsNullOrEmpty(row.Value) && !string.IsNullOrEmpty(row.Field))
            {
                switch (row.Conditions)
                {
                    case Conditions.Equal:
                        if (DateTime.TryParse(row.Value.Trim(), out var date6) && IsDate(row.Field))
                            return $"DATE({row.Field})='{date6.ToString("yyyy-MM-dd")}'";
                        else
                            //return $"{FilterDate(row.Field, row.Value)}";
                            return $"{row.Field}='{row.Value.Trim()}'";
                    //return IsDate(row.Field)? $"DATE({row.Field})='{row.Value.ToString("yyyy-MM")}'":$"{row.Field}='{row.Value.Trim()}'";
                    case Conditions.NotEqual:
                        return $"{row.Field}!='{row.Value.Trim()}'";
                    case Conditions.Greater:
                        return $"{row.Field}>'{row.Value.Trim()}'";
                    case Conditions.GreaterOrEqual:
                        return $"{row.Field}>='{row.Value.Trim()}'";
                    case Conditions.LessThan:
                        return $"{row.Field}<'{row.Value.Trim()}'";
                    case Conditions.LessThanOrEqual:
                        return $"{row.Field}<='{row.Value.Trim()}'";

                    case Conditions.ValueIsEqualTo:
                        return $"{row.Field}={row.Value.Trim()}";
                    case Conditions.ValueIsNotEqual:
                        return $"{row.Field}!={row.Value.Trim()}";
                    case Conditions.ValueGreater:
                        return $"{row.Field}>{row.Value.Trim()}";
                    case Conditions.ValueGreaterOrEqual:
                        return $"{row.Field}>={row.Value.Trim()}";
                    case Conditions.ValueLessThan:
                        return $"{row.Field}<{row.Value.Trim()}";
                    case Conditions.ValueLessThanOrEqual:
                        return $"{row.Field}<={row.Value.Trim()}";

                    //case Conditions.DateIsEqualTo:
                    //    return $"DATE_FORMAT({ row.Field},'%Y-%c-%e')= '{row.Value}'";
                    //case Conditions.DateIsNotEqual:
                    //    return $"DATE_FORMAT({ row.Field},'%Y-%c-%e')!= '{row.Value}'";
                    //case Conditions.DateGreater:
                    //    return $"DATE_FORMAT({row.Field},'%Y-%c-%e')>'{row.Value}'";
                    //case Conditions.DateGreaterOrEqual:
                    //    return $"DATE_FORMAT({row.Field},'%Y-%c-%e')>='{row.Value}'";
                    //case Conditions.DateLessThan:
                    //    return $"DATE_FORMAT({row.Field},'%Y-%c-%e')<'{row.Value}'";
                    //case Conditions.DateLessThanOrEqual:
                    //    return $"DATE_FORMAT({row.Field},'%Y-%c-%e')<='{row.Value}'";

                    case Conditions.DateIsEqualTo:
                        if (DateTime.TryParse(row.Value.Trim(), out var date))
                            return $"DATE({row.Field})='{date.ToString("yyyy-MM-dd")}'";
                        break;
                    //return $"{row.Field}='{row.Value}'";
                    case Conditions.DateIsNotEqual:
                        if (DateTime.TryParse(row.Value.Trim(), out var date3))
                            return $"DATE({row.Field})!='{date3.ToString("yyyy-MM-dd")}'";
                        break;
                    case Conditions.DateGreater:
                        if (DateTime.TryParse(row.Value.Trim(), out var date1))
                            return $"DATE({row.Field})>'{date1.ToString("yyyy-MM-dd")}'";
                        break;
                    case Conditions.DateGreaterOrEqual:
                        if (DateTime.TryParse(row.Value.Trim(), out var date2))
                            return $"DATE({row.Field})>='{date2.ToString("yyyy-MM-dd")}'";
                        break;
                    case Conditions.DateLessThan:
                        if (DateTime.TryParse(row.Value.Trim(), out var date4))
                            return $"DATE({row.Field})<'{date4.ToString("yyyy-MM-dd")}'";
                        break;
                    case Conditions.DateLessThanOrEqual:
                        if (DateTime.TryParse(row.Value.Trim(), out var date5))
                            return $"DATE({row.Field})<='{date5.ToString("yyyy-MM-dd")}'";
                        break;

                    case Conditions.Contains:
                        return IsDate(row.Field) ? $"DATE({row.Field}) like '%{row.Value.Trim()}%'" : $"{row.Field} like '%{row.Value.Trim()}%'";
                    case Conditions.NotContains:
                        return IsDate(row.Field) ? $"DATE({row.Field}) not like '%{row.Value.Trim()}%'" : $"{row.Field} not like '%{row.Value.Trim()}%'";
                    case Conditions.InTheBeginning:
                        return IsDate(row.Field) ? $"DATE({row.Field}) like '{row.Value.Trim()}%'" : $"{row.Field} like '{row.Value.Trim()}%'";
                    case Conditions.InTheEnd:
                        return IsDate(row.Field) ? $"DATE({row.Field}) like '%{row.Value.Trim()}'" : $"{row.Field} like '%{row.Value.Trim()}'";
                        //case Conditions.Null:
                        //    return IsDate(row.Field) ? $"DATE({row.Field}） is null or {row.Field}=''" : $"{row.Field} is null or {row.Field}=''";
                        //case Conditions.NotNull:
                        //    return IsDate(row.Field) ? $"DATE({row.Field}） is null or {row.Field}=''" : $"{row.Field} is not null or {row.Field}<>''";
                        //    return $"{row.Field} is not null or {row.Field}<>''";
                }
            }
            return spelStr;
        }
        #endregion
        #endregion

        #region 搜索下拉列表
        /// <summary>
        /// 搜索下拉列表
        /// </summary>
        /// <param name="id">表id(1,大数据负债，2，活跃债券统计，3，年金终值系数，4，年金现值系数，5，A股指数0000026，6，金融机构人民币存款基准利率,7,金融机构人民币贷款基准利率,
        /// 8,国债,9,B股指数000003,10,成份B股指数390003, 11,复利终值系数，12，企业债，13，披露财务指标，14，LPR数据，
        /// 15，LPR均值数据，16，中期票据(AAA)2017，17，其他指数，18，政策性金融债（国开行）2017，19，政策性金融债（进出口行、开发行）2017，20，利润表，21，红利分配表，
        /// 22，人民币参考汇率_2014，23，人民币参考汇率_2015，24，人民币参考汇率_2016，25，人民币参考汇率_2017，26，人民币参考汇率_2018，27，人民币汇率指数，28，深证成份指数390001，
        /// 29，深证综指399106，30，深证成指R390002，31，Shibor数据， 32，Shibor均值数据，33，Shibor报价数据，34，短期融资债2017，35，统计数据CPI，
        /// 36，人民币月平均汇率，37，人民币汇率中间价，38，创业板指数399006，39，复利现值系数，40，沪深300指数399300，41，上证综指000001，42，国债2017)</param>
        /// <returns></returns>
        public ResponseContext<BigDataDropDownDto> GetDropDownList(int id)
        {
            return GetTablebList(id);
        }

        #region 搜索下拉私有方法
        /// <summary>
        /// 匹配数据表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        #region 匹配数据表
        private ResponseContext<BigDataDropDownDto> GetTablebList(int Id)
        {
            ResponseContext<BigDataDropDownDto> result = new ResponseContext<BigDataDropDownDto>();
            switch (Id)
            {
                case 1:
                    result = TestCompare<BalanceSheetDto, YssxBalanceSheet>();
                    break;
                case 2:
                    result = TestCompare<ActiveBondStatisticsDto, YssxBdActiveBondStatistics>();
                    break;
                case 3:
                    result = TestCompare<AnnuityFinalValueCoefficientDto, YssxBdAnnuityFinalValueCoefficient>();
                    break;
                case 4:
                    result = TestCompare<AnnuityPresentValueFactorDto, YssxBdAnnuityPresentValueFactor>();
                    break;
                case 5:
                    result = TestCompare<AShareIndex000002Dto, YssXBdAShareIndex000002>();
                    break;
                case 6:
                    result = TestCompare<BenchmarkRMBDepositRateOfFinancialInstitutionsDto, YssxBdBenchmarkRMBDepositRateOfFinancialInstitutions>();
                    break;
                case 7:
                    result = TestCompare<BenchmarkRMBLendingRateForFinancialInstitutionsDto, YssxBdBenchmarkRMBLendingRateForFinancialInstitutions>();
                    break;
                case 8:
                    result = TestCompare<BondsAreDebt2017Dto, YssxBdBondsAreDebt2017>();
                    break;
                case 9:
                    result = TestCompare<BShareIndex000003Dto, YssxBdBShareIndex000003>();
                    break;
                case 10:
                    result = TestCompare<ComponentBShareIndex390003Dto, YssxBdComponentBShareIndex390003>();
                    break;
                case 11:
                    result = TestCompare<CompoundFinalValueCoefficientDto, YssxBdCompoundFinalValueCoefficient>();
                    break;
                case 12:
                    result = TestCompare<CorporateBondsDto, YssxBdCorporateBonds>();
                    break;
                case 13:
                    result = TestCompare<DisclosureOfFinancialIndicatorsDto, YssxBdDisclosureOfFinancialIndicators>();
                    break;
                case 14:
                    result = TestCompare<LPRDataDto, YssxBdLPRData>();
                    break;
                case 15:
                    result = TestCompare<LPRMeanDataDto, YssxBdLPRMeanData>();
                    break;
                case 16:
                    result = TestCompare<MediumTermNotes2017Dto, YssxBdMediumTermNotes2017>();
                    break;
                case 17:
                    result = TestCompare<OtherIndicatorsDto, YssxBdOtherIndicators>();
                    break;
                case 18:
                    result = TestCompare<PolicyBasedFinancialBondsDto, YssxBdPolicyBasedFinancialBonds2017>();
                    break;
                case 19:
                    result = TestCompare<ImportAndExportBanksAndIssuanceOfPolicyBasedFinancialBondsDto, YssxBdPolicyFinancialBond2017>();
                    break;
                case 20:
                    result = TestCompare<ProfitDto, YssxBdProfit>();
                    break;
                case 21:
                    result = TestCompare<DividendDistributionModelDto, YssxBdProfitSharing>();
                    break;
                case 22:
                    result = TestCompare<ReferenceRateOfRMB2014Dto, YssxBdReferenceRateOfRMB2014>();
                    break;
                case 23:
                    result = TestCompare<ReferenceRateOfRMB2015Dto, YssxBdReferenceRateOfRMB2015>();
                    break;
                case 24:
                    result = TestCompare<ReferenceRateOfRMB2016Dto, YssxBdReferenceRateOfRMB2016>();
                    break;
                case 25:
                    result = TestCompare<ReferenceRateOfRMB2017Dto, YssxBdReferenceRateOfRMB2017>();
                    break;
                case 26:
                    result = TestCompare<ReferenceRateOfRMB2018Dto, YssxBdReferenceRateOfRMB2018>();
                    break;
                case 27:
                    result = TestCompare<RMBExchangRateIndexDto, YssxBdRMBExchangRateIndex>();
                    break;
                case 28:
                    result = TestCompare<ShenZhenComponentIndex390001Dto, YssxBdShenZhenComponentIndex390001>();
                    break;
                case 29:
                    result = TestCompare<ShenzhenCompositeIndex399106Dto, YssxBdShenzhenCompositeIndex399106Model>();
                    break;
                case 30:
                    result = TestCompare<ShenZhenStockIndexR390002Dto, YssxBdShenZhenStockIndexR390002>();
                    break;
                case 31:
                    result = TestCompare<ShiborDataDto, YssxBdShiborData>();
                    break;
                case 32:
                    result = TestCompare<ShiborMeanDataDto, YssxBdShiborMeanData>();
                    break;
                case 33:
                    result = TestCompare<ShiborQuoteDataDto, YssxBdShiborQuoteData>();
                    break;
                case 34:
                    result = TestCompare<ShortTermFinancingDebt2017Dto, YssxBdShortTermFinancingDebt2017>();
                    break;
                case 35:
                    result = TestCompare<StatisticalDataCPIDto, YssxBdStatisticalDataCPI>();
                    break;
                case 36:
                    result = TestCompare<TheAverageMonthlyExchangeRateOfRMBDto, YssxBdTheAverageMonthlyExchangeRateOfRMB>();
                    break;
                case 37:
                    result = TestCompare<TheCentralParityOfRMBExchangeRateDto, YssxBdTheCentralParityOfRMBExchangeRate>();
                    break;
                case 38:
                    result = TestCompare<TheChinextIndexIs399006Dto, YssxBdTheChinextIndexIs399006>();
                    break;
                case 39:
                    result = TestCompare<TheCompoundPresentValueFactorDto, YssxBdTheCompoundPresentValueFactor>();
                    break;
                case 40:
                    result = TestCompare<TheCsi300IndexIs399300Dto, YssxBdTheCsi300IndexIs399300>();
                    break;
                case 41:
                    result = TestCompare<TheShanghaiCompositeIndexIs000001Dto, YssxBdTheShanghaiCompositeIndexIs000001>();
                    break;
                case 42:
                    result = TestCompare<TheYieldTreasuryBonds2017Dto, YssxBdTheYieldTreasuryBonds2017Model>();
                    break;
                case 43:
                    result = TestCompare<AShareIndex2019Dto, YssxBdAShareIndex2019>();
                    break;
                case 44:
                    result = TestCompare<ChinaBoundsDto, YssxBdChinaBounds>();
                    break;
                case 45:
                    result = TestCompare<CAPIDto, YssxBdCAPI>();
                    break;
                default:
                    break;
            }
            return result;
        }

        #endregion

        public ResponseContext<BigDataDropDownDto> TestCompare<T, T1>() where T1 : class
        {
            var BigDataDropDownDto = new BigDataDropDownDto();
            var result = new ResponseContext<BigDataDropDownDto>();
            Dictionary<string, string> ropDown = new Dictionary<string, string>();

            var plist = new List<PropertyInfo>(typeof(T).GetProperties());

            //获取字段说明
            foreach (var item in plist)
            {
                var fieldName = item.Name;
                List<CustomAttributeData> v = item.CustomAttributes.ToList();
                var descriptionName = v[0].ConstructorArguments[0].Value.ToString();
                if (!descriptionName.Equals("Id"))
                {
                    ropDown.Add(fieldName, descriptionName);
                }

            }

            BigDataDropDownDto.Data = ropDown;
            result.Data = BigDataDropDownDto;
            result.Msg = "获取成功";
            result.Code = CommonConstants.SuccessCode;

            return result;
        }
        #endregion
        #endregion

        #region 加载模块
        /// <summary>
        /// 加载模块
        /// </summary>
        /// <returns></returns>
        public ResponseContext<BigDataParentListDto> GetParentList(string year)
        {
            return ParentList(year);
        }

        #region 加载模块私有方法
        public ResponseContext<BigDataParentListDto> ParentList(string year)
        {
            var result = new ResponseContext<BigDataParentListDto>();
            var parent = new BigDataParentListDto();
            var parentModule = new List<ParentListDto>();
            var childModule = new List<ParentListDto>();
            var grandsonModule = new List<ParentListDto>();
            var theFourthLevel = new List<ParentListDto>();

            var list = DbContext.BDFreeSql.Select<YssxBdParentList>().Where(x => x.Year.Contains(year)).ToList();
            var dtoList = list.MapTo<List<ParentListDto>>();

            foreach (var item in dtoList)
            {
                if (item.TableId == 0)
                    parentModule.Add(item);
                else if (item.TableId == -1)
                    childModule.Add(item);
                else if (item.TableId == 2)
                    theFourthLevel.Add(item);
                else
                    grandsonModule.Add(item);
            }

            parent.parentModule = parentModule;
            parent.childModule = childModule;
            parent.grandsonModule = grandsonModule;
            parent.TheFourthLevel = theFourthLevel;
            result.Code = CommonConstants.SuccessCode;
            result.Msg = "加载成功";
            result.Data = parent;
            //return new ResponseContext<BigDataParentListDto>(1,"asda",new BigDataParentListDto() {TheFourthLevel= parentModule,childModule= parentModule,grandsonModule= parentModule,parentModule= parentModule });
            return result;
        }
        #endregion
        #endregion

        #region 一键复制
        /// <summary>
        /// 一键复制
        /// </summary>
        /// <param name="examId">试卷id</param>
        /// <param name="gradeId">关联id</param>
        /// <param name="coseId">案例id</param>
        /// <param name="questionId">题目id</param>
        /// <param name="model">搜索返回的数据</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveAnswer(ExcelContentModel model)
        {
            string examId = model.ExamId; string gradeId = model.GradeId; string coseId = model.CoseId; string questionId = model.QuestionId; long count = model.Count;
            return await SaveBigDataAnswer(examId, gradeId, coseId, questionId, count, model);
        }

        #region 一键复制的私有方法
        public async Task<ResponseContext<bool>> SaveBigDataAnswer(string examId, string gradeId, string coseId, string questionId, long count, ExcelContentModel exce)
        {
            try
            {
                if (count > 1000)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "筛选条件后当前数据大于1000条， 请继续筛选条件!", Data = true };
                }
                else
                {
                    YssxBdAnswer yb = new YssxBdAnswer()
                    {
                        ExamId = examId,
                        CoseId = coseId,
                        GradeId = gradeId,
                        QuestionId = questionId,
                        Answer = Newtonsoft.Json.JsonConvert.SerializeObject(exce)
                    };
                    var select = DbContext.BDFreeSql.Select<YssxBdAnswer>().Where(x => x.ExamId == examId && x.GradeId == gradeId && x.QuestionId == questionId).ToList();

                    using (var uow = DbContext.BDFreeSql.CreateUnitOfWork())
                    {
                        if (select.Count > 0)
                        {
                            yb.Id = select[0].Id;
                            int i = DbContext.BDFreeSql.Update<YssxBdAnswer>().SetSource(yb).ExecuteAffrows();

                        }
                        else
                        {
                            yb.Id = IdWorker.NextId();
                            DbContext.BDFreeSql.Insert<YssxBdAnswer>().AppendData(yb).ExecuteAffrows();
                        }
                        uow.Commit();
                    }


                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "一键复制成功", Data = true };
                }
            }
            catch (Exception e)
            {

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = e.Message, Data = true };
            }
        }
        #endregion
        #endregion

        #region 查找答案
        /// <summary>
        /// 查找答案
        /// </summary>
        /// <param name="examId">试卷id</param>
        /// <param name="gradeId">关联id</param>
        /// <param name="questionId">题目id</param>
        /// <returns></returns>
        public async Task<ResponseContext<AnswerDto>> GetCase(string examId, string gradeId, string questionId)
        {
            var select =await DbContext.BDFreeSql.GetRepository<YssxBdAnswer>().Where(x => x.ExamId == examId && x.GradeId == gradeId && x.QuestionId == questionId).ToListAsync();
            List<AnswerDto> ls = select.Select(x => new AnswerDto 
            {
                Id=x.Id,
                Answer=x.Answer,
                //CoseId=x.CoseId,
                //ExamId=
            }).ToList();
            return ls.Count > 0 ? new ResponseContext<AnswerDto>(CommonConstants.SuccessCode, "", ls[0]) : new ResponseContext<AnswerDto>(CommonConstants.SuccessCode, "没查找到对象！", null);
        }
        #endregion

        #region 获取搜索条件下拉
        /// <summary>
        /// 获取搜索条件下拉
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<ConditionsDto>>> GetSearchonditionsList()
        {
            var list = DbContext.BDFreeSql.Select<YssxBdConditions>().ToList();
            var dtoList = list.MapTo<List<ConditionsDto>>();
            return dtoList.Count > 0 ? new ResponseContext<List<ConditionsDto>>(CommonConstants.SuccessCode, "获取成功", dtoList) : new ResponseContext<List<ConditionsDto>>(CommonConstants.SuccessCode, "没查找到数据！", null);
        }
        #endregion

        #region excel导出
        /// <summary>
        /// excel导出
        /// </summary>
        /// <param name="name">模块名称</param>
        /// <param name="model">导出数据</param>
        /// <returns></returns>
        public async Task<MemoryStream> EcxelExport(ExcelDto search)
        {
            //ExcelDto se = new ExcelDto();
            //var sear = JsonConvert.DeserializeObject<ExcelDto>(search);
            return await GetTablebList1Async1(search);
        }

        #region excel导出私有方法
        /// <summary>
        /// 匹配数据表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        #region 匹配数据表
        public async Task<MemoryStream> GetTablebList1Async1(ExcelDto model)
        {
            switch (model.Id)
            {
                case 1:
                    return await TestCompareAsync2<BalanceSheetDto, YssxBalanceSheet>(model);

                case 2:
                    return await TestCompareAsync2<ActiveBondStatisticsDto, YssxBdActiveBondStatistics>(model);

                case 3:
                    return await TestCompareAsync2<AnnuityFinalValueCoefficientDto, YssxBdAnnuityFinalValueCoefficient>(model);

                case 4:
                    return await TestCompareAsync2<AnnuityPresentValueFactorDto, YssxBdAnnuityPresentValueFactor>(model);

                case 5:
                    return await TestCompareAsync2<AShareIndex000002Dto, YssXBdAShareIndex000002>(model);

                case 6:
                    return await TestCompareAsync2<BenchmarkRMBDepositRateOfFinancialInstitutionsDto, YssxBdBenchmarkRMBDepositRateOfFinancialInstitutions>(model);

                case 7:
                    return await TestCompareAsync2<BenchmarkRMBLendingRateForFinancialInstitutionsDto, YssxBdBenchmarkRMBLendingRateForFinancialInstitutions>(model);

                case 8:
                    return await TestCompareAsync2<BondsAreDebt2017Dto, YssxBdBondsAreDebt2017>(model);

                case 9:
                    return await TestCompareAsync2<BShareIndex000003Dto, YssxBdBShareIndex000003>(model);

                case 10:
                    return await TestCompareAsync2<ComponentBShareIndex390003Dto, YssxBdComponentBShareIndex390003>(model);

                case 11:
                    return await TestCompareAsync2<CompoundFinalValueCoefficientDto, YssxBdCompoundFinalValueCoefficient>(model);

                case 12:
                    return await TestCompareAsync2<CorporateBondsDto, YssxBdCorporateBonds>(model);

                case 13:
                    return await TestCompareAsync2<DisclosureOfFinancialIndicatorsDto, YssxBdDisclosureOfFinancialIndicators>(model);

                case 14:
                    return await TestCompareAsync2<LPRDataDto, YssxBdLPRData>(model);

                case 15:
                    return await TestCompareAsync2<LPRMeanDataDto, YssxBdLPRMeanData>(model);

                case 16:
                    return await TestCompareAsync2<MediumTermNotes2017Dto, YssxBdMediumTermNotes2017>(model);

                case 17:
                    return await TestCompareAsync2<OtherIndicatorsDto, YssxBdOtherIndicators>(model);

                case 18:
                    return await TestCompareAsync2<PolicyBasedFinancialBondsDto, YssxBdPolicyBasedFinancialBonds2017>(model);

                case 19:
                    return await TestCompareAsync2<ImportAndExportBanksAndIssuanceOfPolicyBasedFinancialBondsDto, YssxBdPolicyFinancialBond2017>(model);

                case 20:
                    return await TestCompareAsync2<ProfitDto, YssxBdProfit>(model);

                case 21:
                    return await TestCompareAsync2<DividendDistributionModelDto, YssxBdProfitSharing>(model);

                case 22:
                    return await TestCompareAsync2<ReferenceRateOfRMB2014Dto, YssxBdReferenceRateOfRMB2014>(model);

                case 23:
                    return await TestCompareAsync2<ReferenceRateOfRMB2015Dto, YssxBdReferenceRateOfRMB2015>(model);

                case 24:
                    return await TestCompareAsync2<ReferenceRateOfRMB2016Dto, YssxBdReferenceRateOfRMB2016>(model);

                case 25:
                    return await TestCompareAsync2<ReferenceRateOfRMB2017Dto, YssxBdReferenceRateOfRMB2017>(model);

                case 26:
                    return await TestCompareAsync2<ReferenceRateOfRMB2018Dto, YssxBdReferenceRateOfRMB2018>(model);

                case 27:
                    return await TestCompareAsync2<RMBExchangRateIndexDto, YssxBdRMBExchangRateIndex>(model);

                case 28:
                    return await TestCompareAsync2<ShenZhenComponentIndex390001Dto, YssxBdShenZhenComponentIndex390001>(model);

                case 29:
                    return await TestCompareAsync2<ShenzhenCompositeIndex399106Dto, YssxBdShenzhenCompositeIndex399106Model>(model);

                case 30:
                    return await TestCompareAsync2<ShenZhenStockIndexR390002Dto, YssxBdShenZhenStockIndexR390002>(model);

                case 31:
                    return await TestCompareAsync2<ShiborDataDto, YssxBdShiborData>(model);

                case 32:
                    return await TestCompareAsync2<ShiborMeanDataDto, YssxBdShiborMeanData>(model);

                case 33:
                    return await TestCompareAsync2<ShiborQuoteDataDto, YssxBdShiborQuoteData>(model);

                case 34:
                    return await TestCompareAsync2<ShortTermFinancingDebt2017Dto, YssxBdShortTermFinancingDebt2017>(model);

                case 35:
                    return await TestCompareAsync2<StatisticalDataCPIDto, YssxBdStatisticalDataCPI>(model);

                case 36:
                    return await TestCompareAsync2<TheAverageMonthlyExchangeRateOfRMBDto, YssxBdTheAverageMonthlyExchangeRateOfRMB>(model);

                case 37:
                    return await TestCompareAsync2<TheCentralParityOfRMBExchangeRateDto, YssxBdTheCentralParityOfRMBExchangeRate>(model);

                case 38:
                    return await TestCompareAsync2<TheChinextIndexIs399006Dto, YssxBdTheChinextIndexIs399006>(model);

                case 39:
                    return await TestCompareAsync2<TheCompoundPresentValueFactorDto, YssxBdTheCompoundPresentValueFactor>(model);

                case 40:
                    return await TestCompareAsync2<TheCsi300IndexIs399300Dto, YssxBdTheCsi300IndexIs399300>(model);

                case 41:
                    return await TestCompareAsync2<TheShanghaiCompositeIndexIs000001Dto, YssxBdTheShanghaiCompositeIndexIs000001>(model);

                case 42:
                    return await TestCompareAsync2<TheYieldTreasuryBonds2017Dto, YssxBdTheYieldTreasuryBonds2017Model>(model);

                case 43:
                    return await TestCompareAsync2<AShareIndex2019Dto, YssxBdAShareIndex2019>(model);

                case 44:
                    return await TestCompareAsync2<ChinaBoundsDto, YssxBdChinaBounds>(model);

                case 45:
                    return await TestCompareAsync2<CAPIDto, YssxBdCAPI>(model);

                default:
                    break;
            }
            return null;
        }
        #endregion

        /// <summary>
        /// 拼接sql语句
        /// </summary>
        /// <param name="sear"></param>
        /// <returns></returns>
        private string SelectSql(ExcelDto sear)
        {
            var sqlWhere = string.Empty;

            //添加年份条件
            if (sear.Year.Equals("2018"))
                sqlWhere += " WHERE Year='2018' ";
            else if (sear.Year.Equals("2019"))
                sqlWhere += " WHERE Year='2019' ";
            else if (DateTime.TryParse(sear.Year.Trim(), out var date))
                sqlWhere += $" WHERE DATE(Rq)='{date.ToString("yyyy-MM-dd")}'";

            //添加指数条件
            switch (sear.ParentId)
            {
                case 64:
                    sqlWhere += " AND Zsdm='000002'";
                    break;
                case 65:
                    sqlWhere += " AND Zsdm='000003'";
                    break;
                case 66:
                    sqlWhere += " AND Zsdm='399003'";
                    break;
                case 67:
                    sqlWhere += " AND Zsdm='399006'";
                    break;
                case 68:
                    sqlWhere += " AND Zsdm='399102'";
                    break;
                case 69:
                    sqlWhere += " AND Zsdm='000300'";
                    break;
                case 70:
                    sqlWhere += " AND Zsdm='000016'";
                    break;
                case 71:
                    sqlWhere += " AND Zsdm='000001'";
                    break;
                case 72:
                    sqlWhere += " AND Zsdm='399002'";
                    break;
                case 73:
                    sqlWhere += " AND Zsdm='399001'";
                    break;
                case 74:
                    sqlWhere += " AND Zsdm='399106'";
                    break;
                default:
                    break;
            }
            //拼接sql
            if (sear.Rows.Any())
            {

                for (int i = 0; i < sear.Rows.Count; i++)
                {
                    var str = SpelCondition(sear.Rows[i]);
                    if (!string.IsNullOrEmpty(str))
                    {
                        if (i == 0)
                        {
                            //if (sear.Rows[i].LeftParentheses)
                            //sqlWhere += "(";
                            sqlWhere += $" AND ({str}";
                            //else
                            //sqlWhere += $" AND {str}";
                        }
                        else
                        {
                            if (sear.Rows[i].LeftParentheses)
                            {
                                switch (sear.Rows[i].Way)
                                {
                                    case Way.And:
                                        sqlWhere += $" AND ({str}";
                                        break;
                                    case Way.Or:
                                        sqlWhere += $" OR ({str}";
                                        break;
                                }
                            }
                            else
                            {
                                switch (sear.Rows[i].Way)
                                {
                                    case Way.And:
                                        sqlWhere += $" AND {str}";
                                        break;
                                    case Way.Or:
                                        sqlWhere += $" OR {str}";
                                        break;
                                }
                            }
                        }

                        if (sear.Rows[i].RightParentheses)
                            sqlWhere += ")";

                        if (i == sear.Rows.Count - 1)
                        {
                            sqlWhere += ")";
                        }
                    }
                }
            }

            return sqlWhere;
        }

        private async Task<MemoryStream> TestCompareAsync2<T, T1>(ExcelDto sear) where T1 : class
        {
            var sqlWhere = SelectSql(sear);
            var sqlColumn = string.Empty;
            var select = string.Empty;
            List<List<string>> rows = new List<List<string>>();
            List<string> cols = new List<string>();

            //获取筛选字段
            if (sear.Ecols.Any())
            {
                for (int i = 0; i < sear.Ecols.Count; i++)
                {
                    sqlColumn += string.IsNullOrEmpty(sqlColumn) ? sear.Ecols[i] : $",{sear.Ecols[i]}";
                }
            }

            //拼接筛选sql
            select = string.IsNullOrEmpty(sqlColumn) ? select = DbContext.BDFreeSql.Select<T1>().ToSql() : select = DbContext.BDFreeSql.Select<T1>().ToSql(sqlColumn);

            select += $" {sqlWhere} limit 0,1000";
            var list = await DbContext.BDFreeSql.Ado.QueryAsync<T1>(select);
            var plist = new List<PropertyInfo>(typeof(T1).GetProperties());

            //try
            //{
            if (!sear.Cols.Any())
            {
                //获取字段说明
                foreach (var item in plist)
                {
                    var name = item.Name;
                    List<CustomAttributeData> v = item.CustomAttributes.ToList();
                    var descriptionName = v[0].ConstructorArguments.Count == 0 ? v[1].ConstructorArguments[0].Value.ToString() : v[0].ConstructorArguments[0].Value.ToString();
                    cols.Add(descriptionName);

                }
            }


            if (!sear.Cols.Any())
            {
                sear.Cols = cols;
            }

            //获取行数据
            foreach (var dto in list)
            {

                List<string> rList = new List<string>();

                if (sear.Ecols.Any())
                {
                    for (int i = 0; i < sear.Ecols.Count; i++)
                    {
                        string name = plist.FirstOrDefault(x => x.Name.ToLower() == sear.Ecols[i].ToLower()).Name;
                        var o = plist.FirstOrDefault(x => x.Name.ToLower() == sear.Ecols[i].ToLower()).GetValue(dto, null);
                        if (o == null) continue;
                        switch (name)
                        {
                            case "AccountPeriod":
                            case "Rq":
                            case "Date":
                            //case "Ydbs":
                            case "Ykprq":
                            case "Ysprq":
                            case "Dqr":
                            case "Jxrq":
                            case "Tjrq":
                            case "Dqrq":
                            case "Tzsj":
                            case "Jzrq":
                            case "Yaggrq":
                            case "Gddhzkrq":
                            case "Gddhggfbrq":
                            case "Ssfaggrq":
                            case "Gqdjr":
                            case "Cqcxr":
                            case "Pxr":
                                //case "Tjqj":

                                rList.Add(DateFromat(o.ToString()));
                                break;
                            default:
                                rList.Add(o.ToString());
                                break;
                        }

                    }
                }
                else
                {
                    for (int i = 0; i < plist.Count; i++)
                    {
                        var o = plist[i].GetValue(dto, null);
                        string id = plist[i].Name;
                        switch (id)
                        {
                            case "AccountPeriod":
                            case "Rq":
                            case "Date":
                            //case "Ydbs":
                            case "Ykprq":
                            case "Ysprq":
                            case "Dqr":
                            case "Jxrq":
                            case "Tjrq":
                            case "Dqrq":
                            case "Tzsj":
                            case "Jzrq":
                            case "Yaggrq":
                            case "Gddhzkrq":
                            case "Gddhggfbrq":
                            case "Ssfaggrq":
                            case "Gqdjr":
                            case "Cqcxr":
                            case "Pxr":
                                //case "Tjqj":

                                rList.Add(DateFromat(o.ToString()));
                                break;
                            default:
                                rList.Add(o.ToString());
                                break;
                        }
                        //rList.Add(o.ToString());

                    }

                }
                rows.Add(rList);
            }

            try
            {
                //创建Excel文件的对象
                HSSFWorkbook book = new HSSFWorkbook();
                //添加一个sheet
                ISheet sheet = book.CreateSheet(sear.Name);

                ICell[] cell = new ICell[sear.Cols.Count];
                IRow[] row = new IRow[rows.Count + 1];

                //读取头部列数据
                row[0] = sheet.CreateRow(0);
                for (int i = 0; i < sear.Cols.Count; i++)
                {

                    cell[i] = row[0].CreateCell(i);
                    cell[i].SetCellValue(sear.Cols[i]);
                }

                //读取行数据
                for (int j = 0; j < rows.Count; j++)
                {
                    row[j + 1] = sheet.CreateRow(j + 1);
                    for (int i = 0; i < sear.Cols.Count; i++)
                    {
                        cell[i] = row[j + 1].CreateCell(i);
                        cell[i].SetCellValue(rows[j][i]);
                    }

                }
                using (MemoryStream stream = new MemoryStream())
                {
                    book.Write(stream);

                    //读取里面的数据
                    //var bytes = stream.ToArray();
                    //var extention = ".xls";
                    //var provider = new FileExtensionContentTypeProvider();
                    //if (!provider.Mappings.TryGetValue(extention, out var memi))
                    //{
                    //    throw new Exception("找不到可导出的文件类型");
                    //}
                    return stream;//new File((bytes, memi, sear.Name + DateTime.Now.ToString("yyyyMMddHHmmssffff") + extention);
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }
    }
    #endregion
    #endregion
}
