using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using System.Linq;
using Yssx.Framework.Authorizations;
using Yssx.Redis;

namespace Yssx.S.Service
{
    /// <summary>
    /// 积分商品兑换服务
    /// </summary>
    public class IntegralProductExchangeService : IIntegralProductExchangeService
    {
        #region 前台

        /// <summary>
        ///  积分商品兑换
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Save(IntegralProductExchangeDto model, UserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<YssxIntegralProductExchange>(e => e.IsDelete == CommonConstants.IsNotDelete);
            int useIntegral = 0;//本次使用的积分
            string shippingAddress = null;
            string shippingName = null;
            string userContact = null;

            #region 参数校验
            var product = await DbContext.FreeSql.GetRepository<YssxIntegralProduct>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.Id == model.IntegralProductId && e.IsActive == true && e.Inventory >= model.ProductNumber)
                .FirstAsync(e => new
                {
                    e.Integral,
                    e.Name,
                    e.Logo,
                    e.Type,
                });

            if (product == null)
            {
                result.Msg = $"商品已经下架或库存不足，请选择其他商品！";
                return result;
            }
            //查询用户可用积分
            var usableIntegral = await DbContext.FreeSql.GetRepository<YssxIntegral>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .Where(e => e.UserId == currentUser.Id)
                .FirstAsync(e => e.Number);

            useIntegral = model.ProductNumber * product.Integral;
            if (usableIntegral < useIntegral)
            {
                result.Msg = $"积分不足！";
                return result;
            }
            if (product.Type == 1)//实物
            {
                var userAddres = await DbContext.FreeSql.GetRepository<YssxUserAddress>(e => e.IsDelete == CommonConstants.IsNotDelete)
                  .Where(e => e.Id == model.UserAddressId)
                  .FirstAsync(e => new
                  {
                      e.ProvinceName,
                      e.Details,
                      e.ShippingName,
                      e.Mobile,
                  });
                if (userAddres == null)
                {
                    result.Msg = "用户收货地址不存在。";
                    return result;
                }
                shippingAddress = userAddres.ProvinceName + userAddres.Details;
                shippingName = userAddres.ShippingName;
                userContact = userAddres.Mobile;
            }

            #endregion

            #region 构建对象
            //积分商品对象信息
            var obj = new YssxIntegralProductExchange
            {
                Id = IdWorker.NextId(),
                CreateBy = currentUser.Id,
                UserId = currentUser.Id,
                IntegralNumber = useIntegral,
                IntegralProductId = model.IntegralProductId,
                IntegralProductName = product.Name,
                ShippingAddress = shippingAddress,
                ShippingName = shippingName,
                IntegralProductLogo = product.Logo,
                ProductNumber = model.ProductNumber,
                UserContact = string.IsNullOrWhiteSpace(model.UserContact) ? userContact : model.UserContact,
                ProductType = product.Type,
            };

            //积分使用详情
            YssxIntegralDetails yssxIntegralDetails = new YssxIntegralDetails
            {
                Id = IdWorker.NextId(),//获取唯一Id
                Number = useIntegral,
                FromType = 0,
                StatusType = 2,
                UserId = currentUser.Id,
                Desc = product.Name,
                Date = DateTime.Now.ToString("yyyy-MM-dd")
            };
            #endregion

            #region 保存到数据库中
            string key = $"{CommonConstants.Lock_IntegralExchangeProduct}:{currentUser.Id}";
            await RedisLock.WaitLock(key, async () =>
             {
                 using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
                 {
                     try
                     {
                         var repo_f = uow.GetRepository<YssxIntegralProductExchange>(e => e.IsDelete == CommonConstants.IsNotDelete);
                         var repo_i = uow.GetRepository<YssxIntegral>(e => e.IsDelete == CommonConstants.IsNotDelete);
                         var repo_id = uow.GetRepository<YssxIntegralDetails>(e => e.IsDelete == CommonConstants.IsNotDelete);
                         var repo_ip = uow.GetRepository<YssxIntegralProduct>(e => e.IsDelete == CommonConstants.IsNotDelete);

                         //更改用户积分使用,可用积分足够才能成功
                         var countI = repo_i.UpdateDiy.Set(c => new YssxIntegral
                         {
                             UpdateTime = DateTime.Now,
                             Number = c.Number - useIntegral,
                             UsedNumber = c.UsedNumber + useIntegral,
                             ProductExchangeNumber = c.ProductExchangeNumber + 1
                         }).Where(e => e.UserId == currentUser.Id && e.Number >= useIntegral)
                          .ExecuteAffrowsAsync();

                         //更改商品库存、兑换数量,可用积分足够才能成功
                         var countIP = repo_ip.UpdateDiy.Set(c => new YssxIntegralProduct
                         {
                             UpdateTime = DateTime.Now,
                             Inventory = c.Inventory - model.ProductNumber,
                             ExchangeNum = c.ExchangeNum + model.ProductNumber
                         }).Where(e => e.Id == model.IntegralProductId && e.Inventory >= model.ProductNumber).ExecuteAffrowsAsync();
                         //添加积分使用详情
                         var id = repo_id.InsertAsync(yssxIntegralDetails);
                         //添加到数据库
                         var ipxc = repo_f.InsertAsync(obj);

                         if ((await countI) > 0 && (await countIP) > 0 && (await id) != null && (await ipxc) != null)
                         {
                             uow.Commit();
                             result.SetSuccess(true, obj.Id.ToString());//设置返回值为成功
                         }
                         else
                         {
                             uow.Rollback();
                             result.SetError("商品已经下架或库存不足，兑换商品失败，请选择其他商品！");
                         }
                     }
                     catch (Exception ex)
                     {
                         uow.Rollback();
                         CommonLogger.Error(ex.ToString());//写错误日志
                         throw;
                     }
                 }
             });
            #endregion

            return result;
        }
        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<IntegralProductExchangePageByUserIdResponseDto>> ListPageByUserId(IntegralProductExchangePageRequestDto query)
        {
            long count = 0;
            var entitys = await DbContext.FreeSql.GetRepository<YssxIntegralProductExchange>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .WhereIf(query.DeliveryStatus.HasValue, e => e.DeliveryStatus == query.DeliveryStatus)
                   .OrderByDescending(e => e.CreateTime)
                   .Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), a => a.ShippingName.Contains(query.Name))
                   .WhereIf(query.UserId > 0, a => query.UserId == a.UserId)
                   .ToListAsync<IntegralProductExchangePageByUserIdResponseDto>();

            return new PageResponse<IntegralProductExchangePageByUserIdResponseDto>(query.PageIndex, query.PageSize, count, entitys);
        }

        #endregion

        #region 后台

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<IntegralProductExchangeDto>> Get(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<YssxIntegralProductExchange>(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .Where(e => e.Id == id)
                      .FirstAsync<IntegralProductExchangeDto>();

            return new ResponseContext<IntegralProductExchangeDto>(entity);
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Delete(long currentUserId, params long[] id)
        {
            //删除
            await DbContext.FreeSql.GetRepository<YssxIntegralProductExchange>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(c => new YssxIntegralProductExchange
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUserId,
                }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_t = uow.GetRepository<YssxIntegralProductExchange>(e => e.IsDelete == CommonConstants.IsNotDelete);
            //        //删除
            //        await repo_t.UpdateDiy.Set(c => new YssxIntegralProductExchange
            //        {
            //            IsDelete = CommonConstants.IsDelete,
            //            UpdateTime = DateTime.Now,
            //            UpdateBy = currentUserId,
            //        }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}

            return new ResponseContext<bool>(true);
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<IntegralProductExchangePageResponseDto>> ListPage(IntegralProductExchangePageRequestDto query)
        {
            long count = 0;
            var entitys = await DbContext.FreeSql.GetRepository<YssxIntegralProductExchange>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .WhereIf(query.DeliveryStatus.HasValue, e => e.DeliveryStatus == query.DeliveryStatus)
                   .From<YssxUser>((a, b) => a.InnerJoin(aa => aa.UserId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), (a, b) => a.UserContact.Contains(query.Name) || a.ShippingName.Contains(query.Name) ||
                            b.RealName.Contains(query.Name) || b.MobilePhone.Contains(query.Name))
                   .WhereIf(query.UserId > 0, (a, b) => query.UserId == a.UserId)
                   .Count(out count)
                   .OrderByDescending((a, b) => a.CreateTime)
                   .Page(query.PageIndex, query.PageSize)
                   .ToListAsync((a, b) => new IntegralProductExchangePageResponseDto
                   {
                       UserId = a.UserId,
                       DeliveryDate = a.DeliveryDate,
                       DeliveryStatus = a.DeliveryStatus,
                       IntegralNumber = a.IntegralNumber,
                       IntegralProductId = a.IntegralProductId,
                       IntegralProductName = a.IntegralProductName,
                       OrderCode = a.OrderCode,
                       ProductLink = a.ProductLink,
                       ProductSource = a.ProductSource,
                       ShippingAddress = a.ShippingAddress,
                       ShippingName = a.ShippingName,
                       UserContact = a.UserContact,
                       UserMobile = b.MobilePhone,
                       UserName = b.RealName,
                       CreateTime = a.CreateTime,
                       Id = a.Id
                   });

            return new PageResponse<IntegralProductExchangePageResponseDto>(query.PageIndex, query.PageSize, count, entitys);
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<IntegralProductExchangeListResponseDto>> List(IntegralProductExchangeListRequestDto query)
        {
            var entitys = await DbContext.FreeSql.GetRepository<YssxIntegralProductExchange>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.UserContact.Contains(query.Name))
                   .OrderByDescending(e => e.CreateTime)
                   .ToListAsync<IntegralProductExchangeListResponseDto>();

            return new ListResponse<IntegralProductExchangeListResponseDto>(entitys);
        }

        /// <summary>
        /// 修改发货信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpdateDeliveryInfo(UpdateDeliveryInfoRequestDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<YssxIntegralProductExchange>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var updateObj = await repo
                   .Where(e => e.Id == model.Id)
                   .FirstAsync();

            if (updateObj == null)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"积分商品兑换不存在，Id:{model.Id}", false);

            #endregion
            //赋值
            updateObj.UpdateBy = currentUserId;
            updateObj.UpdateTime = DateTime.Now;
            updateObj.DeliveryStatus = model.DeliveryStatus;
            updateObj.DeliveryDate = model.DeliveryDate;
            updateObj.OrderCode = model.OrderCode;
            updateObj.ProductSource = model.ProductSource;
            updateObj.ProductLink = model.ProductLink;

            #region 保存到数据库中

            await repo.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new
            {
                a.UpdateBy,
                a.UpdateTime,
                a.DeliveryDate,
                a.DeliveryStatus,
                a.OrderCode,
                a.ProductSource,
                a.ProductLink
            }).ExecuteAffrowsAsync();//修改内容

            //await repo.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_f = uow.GetRepository<YssxIntegralProductExchange>();

            //        await repo_f.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

            //        //await repo_f.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}
            #endregion

            result.SetSuccess(true, updateObj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 修改收货地址
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpdateAddressInfo(ProductExchangeAddressRequestDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<YssxIntegralProductExchange>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var updateObj = await repo
                   .Where(e => e.Id == model.Id)
                   .FirstAsync();

            if (updateObj == null)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"积分商品兑换不存在，Id:{model.Id}", false);

            #endregion
            //赋值
            updateObj.UpdateBy = currentUserId;
            updateObj.UpdateTime = DateTime.Now;
            updateObj.ShippingAddress = model.ShippingAddress;
            updateObj.ShippingName = model.ShippingName;
            updateObj.UserContact = model.UserContact;

            #region 保存到数据库中

            await repo.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new
            {
                a.UpdateBy,
                a.UpdateTime,
                a.ShippingName,
                a.ShippingAddress,
                a.UserContact,
            }).ExecuteAffrowsAsync();//修改内容

            //await repo.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_f = uow.GetRepository<YssxIntegralProductExchange>();

            //        await repo_f.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

            //        //await repo_f.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}
            #endregion

            result.SetSuccess(true, updateObj.Id.ToString());//设置返回值为成功

            return result;
        }

        #endregion
    }
}