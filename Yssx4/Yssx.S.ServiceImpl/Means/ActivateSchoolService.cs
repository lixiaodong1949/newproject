﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Tas.Common.Utils;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.IServices.Means;
using Yssx.S.Pocos;
using System.Linq;
using System.Text;
using Yssx.Framework.Auth;

namespace Yssx.S.ServiceImpl.Means
{
    /// <summary>
    /// 激活学校
    /// </summary>
    public class ActivateSchoolService : IActivateSchoolService
    {
        #region 待删除
        /// <summary>
        /// 验证学校激活码
        /// </summary>
        /// <param name="schoolCode">学校激活码</param>
        /// <returns></returns>
        public async Task<ResponseContext<SchoolActivationSuccessfulDto>> VerifySchoolActivationCode(string schoolCode)
        {
            var shool = DbContext.FreeSql.Select<YssxSchool>().Where(x => x.Code == schoolCode && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (shool == null)
                return new ResponseContext<SchoolActivationSuccessfulDto> { Code = CommonConstants.ErrorCode, Msg = "激活失败，学校已删除!" };

            return await Task.Run(() =>
            {
                var yssxActivationCode = DbContext.FreeSql.Select<YssxActivationCode>().Where(x => x.Code == schoolCode && x.IsDelete == CommonConstants.IsNotDelete).First();
                int day = (DateTime.Now - yssxActivationCode.ActivationDeadline).Days;
                var dto = new SchoolActivationSuccessfulDto
                {
                    SchoolName = shool.Name,
                    Deadline = yssxActivationCode.ActivationDeadline,
                    RemainingDays = day > 0 ? day : 0,
                    SchoolId = shool.Id
                };
                return new ResponseContext<SchoolActivationSuccessfulDto> { Code = CommonConstants.SuccessCode, Msg = "激活成功!", Data = dto };
            });
        }
        #endregion

        /// <summary>
        /// 验证角色激活码
        /// </summary>
        /// <param name="roleCode">角色激活码</param>
        /// <returns></returns>
        public async Task<ResponseContext<VerifyRoleActivationCodeDto>> VerifyRoleActivationCode(string roleCode, long currentUserId)
        {
            var userEntity = DbContext.FreeSql.Select<YssxUser>().Where(x => x.Id == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (string.IsNullOrWhiteSpace(userEntity.MobilePhone))
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Data = null, Msg = "激活之前请先绑定手机号!" };
            var schoolCode = DbContext.FreeSql.Select<YssxActivationCode>().Where(x => x.Code == roleCode && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (schoolCode == null)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Data = null, Msg = "激活码不存在!" };
            if (schoolCode.State == 0)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Data = null, Msg = "激活码禁用!" };
            if (schoolCode.EndTime < DateTime.Now)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Data = null, Msg = "激活码失效!" };
            if (schoolCode.ActivationDeadline < DateTime.Now)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Data = null, Msg = "激活码激活截至日期已到期!" };
            if (schoolCode.ActivatedNumber + 1 > schoolCode.Number)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Data = null, Msg = "可激活数量已满!" };
            //学校信息
            var rSchoolEntity = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.Id == schoolCode.SchoolId && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (rSchoolEntity == null)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Data = null, Msg = "要激活的学校数据异常，请联系平台客服!" };

            return await Task.Run(() =>
            {
                //var role = string.Empty;
                //0，教务，1，教师，2学生
                //if (schoolCode.TypeId == 0)
                //    role = "教务";
                //if (schoolCode.TypeId == 1)
                //    role = "教师";
                //if (schoolCode.TypeId == 2)
                //    role = "学生";
                var dto = new VerifyRoleActivationCodeDto
                {
                    RoleName = schoolCode.TypeName,
                    SchoolId = schoolCode.SchoolId,
                    SchoolName = rSchoolEntity.Name,
                    TypeId = schoolCode.TypeId,
                    ActivationCodeId = schoolCode.Id
                };
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.SuccessCode, Msg = "验证成功!", Data = dto };
            });
        }

        /// <summary>
        /// 填写激活信息
        /// </summary>
        /// <param name="dto">激活信息</param>
        /// <param name="tenantId">租户ID</param>
        /// <returns></returns>
        public async Task<ResponseContext<FillInActivAtionInformationDto>> FillInActivAtionInformation(YssxActivationInformationDto dto, UserTicket user)
        {

            #region 验证
            bool state = false;
            if (RedisHelper.SetNx(dto.ActivationCodeId.ToString(), 1))
            {
                var code = DbContext.FreeSql.Select<YssxActivationCode>().Where(m => m.Id == dto.ActivationCodeId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (code == null)
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "激活码不存在!" };
                if (code.State == 0)
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "激活码禁用!" };
                if (code.EndTime < DateTime.Now)
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "激活码失效!" };
                if (code.ActivationDeadline < DateTime.Now)
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "激活码激活截至日期已到期!" };
                if (code.ActivatedNumber + 1 > code.Number)
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "可激活数量已满!" };

                if (string.IsNullOrEmpty(dto.Nmae))
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "请填写真实姓名" };
                if (string.IsNullOrEmpty(dto.WorkNumber))
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "请填写工学号" };
                if (dto.SchoolId == 0)
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "绑定的学校数据异常，请刷新后重试" };
                if (dto.Departments == 0)
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "请选择院系" };
                if (dto.Professional == 0)
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "请选择专业" };
                if (dto.TypeId == (int)UserTypeEnums.Student && (!dto.Class.HasValue || dto.Class == 0))
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "学生账号激活必须选择班级!" };
                if (DbContext.FreeSql.Select<YssxActivationInformation>().Any(m => m.ActivationCodeId == dto.ActivationCodeId && m.CreateBy == user.Id && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "该账号已激活!" };

                #endregion
                long tenantId = dto.SchoolId;

                FillInActivAtionInformationDto fi = new FillInActivAtionInformationDto();
                var yssxActivationInformation = new YssxActivationInformation
                {
                    Id = IdWorker.NextId(),
                    Nmae = dto.Nmae,
                    TenantId = dto.SchoolId,
                    ClassId = dto.Class,
                    WorkNumber = dto.WorkNumber,
                    TypeId = dto.TypeId,
                    TypeName = dto.TypeName,
                    CreateTime = DateTime.Now,
                    DepartmentsId = dto.Departments,
                    ProfessionalId = dto.Professional,
                    CreateBy = user.Id,
                    UpdateTime = DateTime.Now,
                    ActivationCodeId = dto.ActivationCodeId
                };

                var adto = await DbContext.FreeSql.GetRepository<YssxActivationInformation>().InsertAsync(yssxActivationInformation);
                state = adto != null;
                if (state)
                {
                    DbContext.FreeSql.Update<YssxUser>().Set(x => new YssxUser { RealName = adto.Nmae, UserType = adto.TypeId, UpdateTime = adto.UpdateTime, TenantId = tenantId })
                    .Where(x => x.Id == user.Id).ExecuteAffrows();

                    DbContext.FreeSql.Update<YssxActivationCode>().Set(x => new YssxActivationCode { UpdateTime = adto.UpdateTime, ActivatedNumber = code.ActivatedNumber + 1 })
                    .Where(x => x.Id == dto.ActivationCodeId).ExecuteAffrows();
                    RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{user.MobilePhone}");
                    fi = new FillInActivAtionInformationDto()
                    {
                        ClassName = dto.Class == null ? null : DbContext.FreeSql.Select<YssxClass>().Where(x => x.Id == dto.Class && x.IsDelete == CommonConstants.IsNotDelete).First()?.Name,
                        RealName = dto.Nmae,
                        CollegeName = DbContext.FreeSql.Select<YssxCollege>().Where(x => x.Id == dto.Departments && x.IsDelete == CommonConstants.IsNotDelete).First()?.Name,
                        ProfessionName = DbContext.FreeSql.Select<YssxProfession>().Where(x => x.Id == dto.Professional && x.IsDelete == CommonConstants.IsNotDelete).First()?.Name,
                        SchoolName = dto.SchoolName,
                        TypeName = dto.TypeName,
                        WorkNumber = dto.WorkNumber
                    };
                }

                #region 2020-01-09 新增逻辑

                if (adto != null && fi != null)
                {
                    //添加学生
                    if (dto.TypeId == (int)UserTypeEnums.Student)
                    {
                        var studentList = await DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                        if (studentList != null && studentList.Count > 0)
                            return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "已注册过学生身份!" };

                        YssxStudent student = new YssxStudent
                        {
                            Id = IdWorker.NextId(),
                            Name = dto.Nmae,
                            ExpiredDate = code.ActivationDeadline,
                            UserId = user.Id,
                            StudentNo = dto.WorkNumber,
                            TenantId = dto.SchoolId,
                            SchoolName = dto.SchoolName,
                            CollegeId = dto.Departments,
                            CollegeName = fi.CollegeName,
                            ProfessionId = dto.Professional,
                            ProfessionName = fi.ProfessionName,
                            ClassId = dto.Class ?? 0,
                            ClassName = fi.ClassName,
                            CreateBy = user.Id,
                            UpdateTime = DateTime.Now,
                        };

                        await DbContext.FreeSql.GetRepository<YssxStudent>().InsertAsync(student);
                    }

                    //添加教师、教务
                    if (dto.TypeId == (int)UserTypeEnums.Teacher || dto.TypeId == (int)UserTypeEnums.Educational)
                    {
                        var teacherList = await DbContext.FreeSql.Select<YssxTeacher>().Where(x => x.UserId == user.Id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

                        if (teacherList != null && teacherList.Count > 0)
                            return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "已注册过教师身份!" };

                        YssxTeacher teacher = new YssxTeacher
                        {
                            Id = IdWorker.NextId(),
                            Name = dto.Nmae,
                            UserId = user.Id,
                            TeacherNo = dto.WorkNumber,
                            TenantId = dto.SchoolId,
                            CreateBy = user.Id,
                            UpdateTime = DateTime.Now,
                        };

                        if (dto.TypeId == (int)UserTypeEnums.Teacher)
                            teacher.Type = TeacherType.Teacher;
                        if (dto.TypeId == (int)UserTypeEnums.Educational)
                            teacher.Type = TeacherType.Educational;

                        YssxTeacher teacherModel = await DbContext.FreeSql.GetRepository<YssxTeacher>().InsertAsync(teacher);

                        //教师院系专业关系表
                        if (teacherModel != null)
                        {
                            YssxTeacherCollegeProfession relation = new YssxTeacherCollegeProfession
                            {
                                Id = IdWorker.NextId(),
                                SchoolId = dto.SchoolId,
                                SchoolName = dto.SchoolName,
                                CollegeId = dto.Departments,
                                CollegeName = fi.CollegeName,
                                ProfessionId = dto.Professional,
                                ProfessionName = fi.ProfessionName,
                                TeacherId = teacherModel.Id,
                                UserId = user.Id,
                            };

                            await DbContext.FreeSql.GetRepository<YssxTeacherCollegeProfession>().InsertAsync(relation);
                        }
                    }



                }

                await RedisHelper.DelAsync(dto.ActivationCodeId.ToString());
                return new ResponseContext<FillInActivAtionInformationDto> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = fi, Msg = state ? "成功" : "录入信息失败!" };
            }
            else
            {
                return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "激活人数过多，请再尝试一次激活!" };
            }

            #endregion


        }

        /// <summary>
        /// 下拉数据
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="typeId">1，学校，2，院系，3，专业，4，班级</param>
        /// <returns></returns>
        public async Task<ResponseContext<List<DropdownListDto>>> DropdownList(long id, int typeId)
        {
            return await Task.Run(() =>
            {
                List<DropdownListDto> list = new List<DropdownListDto>();
                switch (typeId)
                {
                    case 1:
                        list = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).Select(x => new DropdownListDto { Id = x.Id, Name = x.Name }).ToList();
                        break;
                    case 2:
                        list = DbContext.FreeSql.Select<YssxCollege>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.TenantId == id).Select(x => new DropdownListDto { Id = x.Id, Name = x.Name }).ToList();
                        break;
                    case 3:
                        list = DbContext.FreeSql.Select<YssxProfession>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CollegeId == id).Select(x => new DropdownListDto { Id = x.Id, Name = x.Name }).ToList();
                        break;
                    case 4:
                        list = DbContext.FreeSql.Select<YssxClass>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.ProfessionId == id).Select(x => new DropdownListDto { Id = x.Id, Name = x.Name }).ToList();
                        break;
                    default:
                        break;
                }
                return new ResponseContext<List<DropdownListDto>> { Code = CommonConstants.SuccessCode, Msg = "获取成功!", Data = list };
            });
        }

        /// <summary>
        /// 获取用户激活信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<ActivationRecordDto>> ActivationRecord(long userId)
        {
            return await Task.Run(() =>
            {
                //用户信息
                var rActRecordEntity = DbContext.FreeSql.GetRepository<YssxUser>()
                    .Where(m => m.Id == userId && m.IsDelete == CommonConstants.IsNotDelete).First(m => new ActivationRecordDto
                    {
                        RealName = m.RealName,
                        UserType = m.UserType,
                        RegistrationType = m.RegistrationType,
                        Status = m.Status,
                        UserId = m.Id,
                        TenantId = m.TenantId,
                        MobilePhone = m.MobilePhone
                    });
                rActRecordEntity.TypeName = rActRecordEntity.UserType == 1 ? "学生" : (rActRecordEntity.UserType == 2 ? "老师" : (rActRecordEntity.UserType == 7 ? "普通用户" : ""));
                //激活信息
                var actInfoEntity = DbContext.FreeSql.GetRepository<YssxActivationInformation>().Where(m => m.CreateBy == userId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (actInfoEntity == null)
                    return new ResponseContext<ActivationRecordDto> { Code = CommonConstants.SuccessCode, Data = null, Msg = "成功" };

                rActRecordEntity.Id = actInfoEntity.Id;
                rActRecordEntity.CreateTime = actInfoEntity.CreateTime;
                rActRecordEntity.ActivationCodeId = actInfoEntity.ActivationCodeId;
                var codeEntity = DbContext.FreeSql.GetRepository<YssxActivationCode>().Where(m => m.Id == actInfoEntity.ActivationCodeId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (codeEntity != null)
                {
                    rActRecordEntity.Code = codeEntity.Code;
                    rActRecordEntity.ActivationDeadline = codeEntity.ActivationDeadline;
                }
                //学生
                long rClassId = 0;
                if (rActRecordEntity.UserType == 1)
                {
                    var studentEntity = DbContext.FreeSql.GetRepository<YssxStudent>().Where(m => m.UserId == userId && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (studentEntity != null)
                    {
                        rActRecordEntity.WorkNumber = studentEntity.StudentNo;
                        rActRecordEntity.CollegeId = studentEntity.CollegeId;
                        rActRecordEntity.ProfessionId = studentEntity.ProfessionId;
                        rClassId = studentEntity.ClassId;
                    }
                }
                //老师
                else if (rActRecordEntity.UserType == 2)
                {
                    var teacherEntity = DbContext.FreeSql.GetRepository<YssxTeacher>().Where(m => m.UserId == userId && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (teacherEntity != null)
                    {
                        rActRecordEntity.WorkNumber = teacherEntity.TeacherNo;
                        var teacherInfoEntity = DbContext.FreeSql.GetRepository<YssxTeacherCollegeProfession>().Where(m => m.TeacherId == teacherEntity.Id && m.UserId == userId && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (teacherInfoEntity != null)
                        {
                            rActRecordEntity.CollegeId = teacherInfoEntity.CollegeId;
                            rActRecordEntity.ProfessionId = teacherInfoEntity.ProfessionId;
                        }
                    }
                }
                //学校
                var schoolEntity = DbContext.FreeSql.GetRepository<YssxSchool>().Where(m => m.Id == rActRecordEntity.TenantId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (schoolEntity != null)
                {
                    rActRecordEntity.SchoolType = (int)schoolEntity.Type;
                    rActRecordEntity.SchoolName = schoolEntity.Name;
                }
                //院系
                var collegeEntity = DbContext.FreeSql.GetRepository<YssxCollege>().Where(m => m.TenantId == rActRecordEntity.TenantId && m.Id == rActRecordEntity.CollegeId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (collegeEntity != null)
                    rActRecordEntity.CollegeName = collegeEntity.Name;
                //专业
                var professionEntity = DbContext.FreeSql.GetRepository<YssxProfession>().Where(m => m.TenantId == rActRecordEntity.TenantId && m.Id == rActRecordEntity.ProfessionId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (professionEntity != null)
                    rActRecordEntity.ProfessionName = professionEntity.Name;
                //班级
                if (rClassId > 0)
                {
                    var classEntity = DbContext.FreeSql.GetRepository<YssxClass>().Where(m => m.TenantId == rActRecordEntity.TenantId && m.Id == rClassId && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (classEntity != null)
                        rActRecordEntity.ClassName = classEntity.Name;
                }
                return new ResponseContext<ActivationRecordDto> { Code = CommonConstants.SuccessCode, Data = rActRecordEntity, Msg = "成功" };
            });

        }

        /// <summary>
        /// 解绑激活码
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UntieActivationCode(long userId, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                //用户信息
                YssxUser rUserEntity = DbContext.FreeSql.GetRepository<YssxUser>().Where(x => x.Id == userId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (rUserEntity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "解绑失败，未找到用户信息!" };
                var rOldUserType = rUserEntity.UserType;
                var rOldMobilePhone = rUserEntity.MobilePhone;
                rUserEntity.UserType = (int)UserTypeEnums.OrdinaryUser;
                rUserEntity.TenantId = 1017666019115035;//解绑用户回到【中国测试大学】1017666019115035
                rUserEntity.UpdateBy = currentUserId;
                rUserEntity.UpdateTime = dtNow;
                //激活信息
                var rYssxActivationInformation = DbContext.FreeSql.GetRepository<YssxActivationInformation>()
                    .Where(x => x.CreateBy == userId && x.IsDelete == CommonConstants.IsNotDelete).ToList(x => new YssxActivationInformation
                    {
                        Id = x.Id,
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = dtNow
                    });
                //学生
                var rYssxStudent = new List<YssxStudent>();
                if (rOldUserType == (int)UserTypeEnums.Student)
                {
                    rYssxStudent = DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == userId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
                    if (rYssxStudent.Count > 0)
                    {
                        foreach (var itemStu in rYssxStudent)
                        {
                            itemStu.IsDelete = CommonConstants.IsDelete;
                            itemStu.UpdateBy = currentUserId;
                            itemStu.UpdateTime = dtNow;
                        }
                    }
                }
                //老师或教务
                var rYssxTeacher = new List<YssxTeacher>();
                var rYssxTeacherCollegeProfession = new List<YssxTeacherCollegeProfession>();
                if (rOldUserType == (int)UserTypeEnums.Teacher || rOldUserType == (int)UserTypeEnums.Educational)
                {
                    rYssxTeacher = DbContext.FreeSql.Select<YssxTeacher>().Where(x => x.UserId == userId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
                    if (rYssxTeacher.Count > 0)
                    {
                        foreach (var itemTeacher in rYssxTeacher)
                        {
                            itemTeacher.IsDelete = CommonConstants.IsDelete;
                            itemTeacher.UpdateBy = currentUserId;
                            itemTeacher.UpdateTime = dtNow;
                        }
                    }
                    rYssxTeacherCollegeProfession = DbContext.FreeSql.Select<YssxTeacherCollegeProfession>().Where(x => x.UserId == userId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
                    if (rYssxTeacherCollegeProfession.Count > 0)
                    {
                        foreach (var itemProfession in rYssxTeacherCollegeProfession)
                        {
                            itemProfession.IsDelete = CommonConstants.IsDelete;
                            itemProfession.UpdateBy = currentUserId;
                            itemProfession.UpdateTime = dtNow;
                        }
                    }
                }

                bool state = true;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    #region 2020-12-23注释 无事物
                    ////用户信息
                    //state = DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.SetSource(rUserEntity).UpdateColumns(x => new { x.UserType, x.TenantId, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
                    ////删除用户缓存
                    //RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{rOldMobilePhone}");
                    ////激活信息
                    //if (rYssxActivationInformation.Count > 0)
                    //    DbContext.FreeSql.Update<YssxActivationInformation>().SetSource(rYssxActivationInformation).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows();
                    ////学生
                    //if (rYssxStudent.Count > 0)
                    //    DbContext.FreeSql.Update<YssxStudent>().SetSource(rYssxStudent).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                    ////老师或教务
                    //if (rYssxTeacher.Count > 0)
                    //    DbContext.FreeSql.Update<YssxTeacher>().SetSource(rYssxTeacher).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                    ////教师院系关系
                    //if (rYssxTeacherCollegeProfession.Count > 0)
                    //    DbContext.FreeSql.Update<YssxTeacherCollegeProfession>().SetSource(rYssxTeacherCollegeProfession).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                    #endregion

                    try
                    {
                        //物理删除学生表(学生表UserId、IsDelete是联合主键)、教师表 
                        //用户信息
                        state = uow.GetRepository<YssxUser>().UpdateDiy.SetSource(rUserEntity).UpdateColumns(x => new { x.UserType, x.TenantId, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
                        //删除用户缓存
                        RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{rOldMobilePhone}");
                        //激活信息
                        if (rYssxActivationInformation.Count > 0)
                            uow.GetRepository<YssxActivationInformation>().UpdateDiy.SetSource(rYssxActivationInformation).UpdateColumns(x => new { x.IsDelete, x.UpdateTime }).ExecuteAffrows();
                        //学生
                        if (rYssxStudent.Count > 0)
                            uow.GetRepository<YssxStudent>().Delete(rYssxStudent);
                        //老师或教务
                        if (rYssxTeacher.Count > 0)
                            uow.GetRepository<YssxTeacher>().Delete(rYssxTeacher);
                        //教师院系关系
                        if (rYssxTeacherCollegeProfession.Count > 0)
                            uow.GetRepository<YssxTeacherCollegeProfession>().Delete(rYssxTeacherCollegeProfession);

                        if (state)
                            uow.Commit();
                        else
                            uow.Rollback();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                    }                    
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "解绑失败!" };
            });
        }


        /// <summary>
        /// 验证角色激活码 - new
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<VerifyRoleActivationCodeDto>> VerifyRoleActCode(string roleCode, int roleType, long currentUserId)
        {
            var dtNow = DateTime.Now;
            #region 验证
            var userEntity = DbContext.FreeSql.Select<YssxUser>().Where(x => x.Id == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (string.IsNullOrWhiteSpace(userEntity.MobilePhone))
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Msg = "激活之前请先绑定手机号!" };
            //if (userEntity.UserType != (int)UserTypeEnums.OrdinaryUser)
            //    return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Msg = "该账号已激活，需联系平台客服申请解绑后才可再次激活!" };
            var schoolCode = DbContext.FreeSql.Select<YssxActivationCode>().Where(x => x.Code == roleCode && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (schoolCode == null)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Msg = "激活码不存在!" };
            if (schoolCode.TypeId != roleType)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Msg = "激活码角色不匹配!" };
            if (schoolCode.State == 0)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Msg = "激活码禁用!" };
            if (schoolCode.EndTime < dtNow)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Msg = "激活码失效!" };
            if (schoolCode.ActivationDeadline < dtNow)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Msg = "激活码激活截至日期已到期!" };
            if (schoolCode.ActivatedNumber + 1 > schoolCode.Number)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Msg = "可激活数量已满!" };
            //学校信息
            var rSchoolEntity = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.Id == schoolCode.SchoolId && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (rSchoolEntity == null)
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Msg = "要激活的学校数据异常，请联系平台客服!" };
            #endregion

            #region 处理数据
            //激活码
            schoolCode.UpdateTime = dtNow;
            schoolCode.ActivatedNumber += 1;
            //用户
            userEntity.UserType = schoolCode.TypeId;
            userEntity.TenantId = schoolCode.SchoolId;
            userEntity.ActivationTime = dtNow;
            userEntity.UpdateBy = currentUserId;
            userEntity.UpdateTime = dtNow;
            //返回数据
            var resData = new VerifyRoleActivationCodeDto
            {
                RoleName = schoolCode.TypeName,
                SchoolId = schoolCode.SchoolId,
                SchoolName = rSchoolEntity.Name,
                TypeId = schoolCode.TypeId,
                ActivationCodeId = schoolCode.Id
            };
            //学生
            YssxStudent rStudent = new YssxStudent();
            if (schoolCode.TypeId == (int)UserTypeEnums.Student)
            {
                #region 2020-12-23 激活不同学校 需 更新用户表、删除学生表
                //YssxStudent studentList = await DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                //if (studentList != null && (studentList.ExpiredDate < DateTime.Now || studentList.ExpiredDate == null))
                //{
                //    await DbContext.FreeSql.GetRepository<YssxStudent>().UpdateDiy.Set(x => x.ExpiredDate, schoolCode.ActivationDeadline).Where(x => x.Id == studentList.Id).ExecuteAffrowsAsync();
                //    return new ResponseContext<VerifyRoleActivationCodeDto> { Data =  resData , Msg = "激活成功"  };
                //}
                #endregion 

                YssxStudent studentModel = await DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (studentModel != null)
                {
                    //学校激活码 
                    if (studentModel.TenantId == schoolCode.SchoolId)
                    {
                        await DbContext.FreeSql.GetRepository<YssxStudent>().UpdateDiy.Set(x => x.ExpiredDate, schoolCode.ActivationDeadline).Set(x => x.UpdateTime, DateTime.Now)
                            .Where(x => x.Id == studentModel.Id).ExecuteAffrowsAsync();

                        return new ResponseContext<VerifyRoleActivationCodeDto> { Data = resData, Msg = "激活成功" };
                    }
                    else
                    {
                        //激活码学校与用户目前学校不符 修改用户表、修改学生表
                        using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                        {
                            try
                            {
                                //删除用户缓存
                                RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{userEntity.MobilePhone}");

                                //1、修改用户表
                                bool updateState = uow.GetRepository<YssxUser>().UpdateDiy.SetSource(userEntity).UpdateColumns(m => new
                                { m.UserType, m.TenantId, m.ActivationTime, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                                //2、修改学生表
                                studentModel.TenantId = userEntity.TenantId;
                                studentModel.SchoolName = rSchoolEntity.Name;
                                studentModel.CollegeId = 0;
                                studentModel.CollegeName = "";
                                studentModel.ProfessionId = 0;
                                studentModel.ProfessionName = "";
                                studentModel.ClassId = 0;
                                studentModel.ClassName = "";
                                studentModel.ExpiredDate = schoolCode.ActivationDeadline;
                                studentModel.UpdateTime = DateTime.Now;

                                updateState = uow.GetRepository<YssxStudent>().UpdateDiy.SetSource(studentModel).UpdateColumns(x => new
                                { x.TenantId, x.SchoolName, x.CollegeId, x.CollegeName, x.ProfessionId, x.ProfessionName, x.ClassId, x.ClassName, x.ExpiredDate, x.UpdateTime }).ExecuteAffrows() > 0;

                                if (updateState)
                                    uow.Commit();
                                else
                                    uow.Rollback();
                            }
                            catch (Exception)
                            {
                                uow.Rollback();
                            }                            
                        }

                        return new ResponseContext<VerifyRoleActivationCodeDto> { Data = resData, Msg = "激活成功" };
                    }
                }

                rStudent = new YssxStudent
                {
                    Id = IdWorker.NextId(),
                    Name = userEntity.RealName,
                    UserId = currentUserId,
                    StudentNo = "",
                    TenantId = userEntity.TenantId,
                    SchoolName = rSchoolEntity.Name,
                    CollegeId = 0,
                    CollegeName = "",
                    ProfessionId = 0,
                    ProfessionName = "",
                    ClassId = 0,
                    ClassName = "",
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    ExpiredDate = schoolCode.ActivationDeadline,
                    UpdateTime = dtNow
                };
            }
            //教师、教务
            YssxTeacher rTeacher = new YssxTeacher();
            if (schoolCode.TypeId == (int)UserTypeEnums.Teacher || schoolCode.TypeId == (int)UserTypeEnums.Educational)
            {
                var teacherList = await DbContext.FreeSql.Select<YssxTeacher>().Where(x => x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                if (teacherList != null && teacherList.Count > 0)
                    return new ResponseContext<VerifyRoleActivationCodeDto> { Code = CommonConstants.ErrorCode, Msg = "已激活过教师身份!" };

                rTeacher = new YssxTeacher
                {
                    Id = IdWorker.NextId(),
                    Name = userEntity.RealName,
                    UserId = currentUserId,
                    TeacherNo = "",
                    TenantId = userEntity.TenantId,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };

                if (schoolCode.TypeId == (int)UserTypeEnums.Teacher)
                    rTeacher.Type = TeacherType.Teacher;
                if (schoolCode.TypeId == (int)UserTypeEnums.Educational)
                    rTeacher.Type = TeacherType.Educational;
            }
            #endregion
            bool state = false;
            return await Task.Run(() =>
            {
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //用户
                    state = DbContext.FreeSql.Update<YssxUser>().SetSource(userEntity).UpdateColumns(m => new { m.UserType, m.TenantId, m.ActivationTime, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                    if (state)
                    {
                        //删除用户缓存
                        RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{userEntity.MobilePhone}");
                        //激活码
                        tran.GetRepository<YssxActivationCode>().UpdateDiy.SetSource(schoolCode).UpdateColumns(m => new { m.UpdateTime, m.ActivatedNumber }).ExecuteAffrows();
                        //学生
                        if (schoolCode.TypeId == (int)UserTypeEnums.Student && rStudent != new YssxStudent())
                            tran.GetRepository<YssxStudent>().InsertAsync(rStudent);
                        //教师、教务
                        if ((schoolCode.TypeId == (int)UserTypeEnums.Teacher || schoolCode.TypeId == (int)UserTypeEnums.Educational) && rTeacher != new YssxTeacher())
                            tran.GetRepository<YssxTeacher>().InsertAsync(rTeacher);
                    }
                    tran.Commit();
                }
                return new ResponseContext<VerifyRoleActivationCodeDto> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state ? resData : null, Msg = state ? "激活成功" : "激活失败!" };
            });
        }

        /// <summary>
        /// 补充激活信息 - new
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<FillInActivAtionInformationDto>> FillInActInformation(ActivationInfoRequest model, long currentUserId, int loginType)
        {
            var dtNow = DateTime.Now;

            #region 验证
            var rUserInfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == currentUserId && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (rUserInfo == null)
                return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "该账号未激活" };
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "请填写真实姓名" };
            if (string.IsNullOrEmpty(model.WorkNumber))
                return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "请填写工学号" };
            if (model.TypeId != rUserInfo.UserType)
                return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "用户当前角色异常，请重新登录后再试" };
            //学校信息
            var rSchoolEntity = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.Id == rUserInfo.TenantId && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (rSchoolEntity == null)
                return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "您绑定的学校数据异常，请联系平台客服!" };
            //if (model.TypeId == (int)UserTypeEnums.Student && (!model.ClassId.HasValue || model.ClassId == 0))
            //    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "学生角色必须选择班级" };
            //if (model.TypeId == (int)UserTypeEnums.Teacher && model.TeacherClassId.Count == 0)
            //    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = string.Format("{0}老师，教师角色必须绑定班级噢", model.Name) };
            //if (model.TypeId == (int)UserTypeEnums.Teacher && model.TeacherClassId.Count > 0 && model.TeacherClassId.Any(m => m == 0))
            //    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = string.Format("{0}老师，您绑定的班级数据异常，请刷新后重试", model.Name) };
            #endregion

            #region 处理数据
            rUserInfo.RealName = model.Name;
            //学生
            var rStudent = new YssxStudent();
            if (model.TypeId == (int)UserTypeEnums.Student)
            {
                var rStudentEntity = DbContext.FreeSql.Select<YssxStudent>().Where(x => x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (rStudentEntity == null)
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "找不到学生数据" };
                var rClassEntity = model.ClassId.HasValue ? DbContext.FreeSql.Select<YssxClass>().Where(x => x.Id == model.ClassId.Value && x.IsDelete == CommonConstants.IsNotDelete).First() : null;

                rStudent = new YssxStudent
                {
                    Id = rStudentEntity.Id,
                    Name = model.Name,
                    StudentNo = model.WorkNumber,
                    CollegeId = model.CollegeId,
                    ProfessionId = model.ProfessionId,
                    ClassId = model.ClassId ?? 0,
                    ClassName = rClassEntity == null ? "" : rClassEntity.Name,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };
            }

            //教师、教务
            var rTeacher = new YssxTeacher();
            //var rAddTeacherClassList = new List<YssxTeacherClass>();
            if (model.TypeId == (int)UserTypeEnums.Teacher || model.TypeId == (int)UserTypeEnums.Educational)
            {
                var rTeacherEntity = DbContext.FreeSql.Select<YssxTeacher>().Where(x => x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (rTeacherEntity == null)
                    return new ResponseContext<FillInActivAtionInformationDto> { Code = CommonConstants.ErrorCode, Msg = "找不到教师数据" };

                rTeacher = new YssxTeacher
                {
                    Id = rTeacherEntity.Id,
                    Name = model.Name,
                    TeacherNo = model.WorkNumber,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow,
                };
                if (model.TypeId == (int)UserTypeEnums.Educational)
                    rTeacher.Type = TeacherType.Educational;
                if (model.TypeId == (int)UserTypeEnums.Teacher)
                {
                    rTeacher.Type = TeacherType.Teacher;
                    ////教师绑定的班级
                    //rAddTeacherClassList = model.TeacherClassId.Select(m => new YssxTeacherClass
                    //{
                    //    Id = IdWorker.NextId(),
                    //    TenantId = model.SchoolId,
                    //    TeacherId = rTeacher.Id,
                    //    ClassId = m
                    //}).ToList();
                }
            }
            #endregion

            FillInActivAtionInformationDto resData = new FillInActivAtionInformationDto();
            bool state = false;
            return await Task.Run(() =>
            {
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //用户
                    state = DbContext.FreeSql.Update<YssxUser>().SetSource(rUserInfo).UpdateColumns(m => new { m.RealName, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                    if (state)
                    {
                        //清除缓存
                        RedisHelper.Del($"{CommonConstants.Cache_GetUserByMobile}{rUserInfo.MobilePhone}");
                        //学生
                        if (model.TypeId == (int)UserTypeEnums.Student && rStudent != new YssxStudent())
                            DbContext.FreeSql.Update<YssxStudent>().SetSource(rStudent).UpdateColumns(m => new { m.Name, m.StudentNo, m.CollegeId, m.ProfessionId, m.ClassId, m.ClassName, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                        //教师、教务
                        if ((model.TypeId == (int)UserTypeEnums.Teacher || model.TypeId == (int)UserTypeEnums.Educational) && rTeacher != new YssxTeacher())
                            DbContext.FreeSql.Update<YssxTeacher>().SetSource(rTeacher).UpdateColumns(m => new { m.Name, m.TeacherNo, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                        ////教师绑定的班级
                        //if (rAddTeacherClassList.Count > 0)
                        //    DbContext.FreeSql.GetRepository<YssxTeacherClass>().InsertAsync(rAddTeacherClassList);

                        resData = new FillInActivAtionInformationDto()
                        {
                            RealName = model.Name,
                            TypeName = model.TypeName,
                            WorkNumber = model.WorkNumber,
                            SchoolName = rSchoolEntity.Name,
                            CollegeName = model.CollegeId == 0 ? "" : DbContext.FreeSql.Select<YssxCollege>().Where(x => x.Id == model.CollegeId && x.IsDelete == CommonConstants.IsNotDelete).First()?.Name,
                            ProfessionName = model.ProfessionId == 0 ? "" : DbContext.FreeSql.Select<YssxProfession>().Where(x => x.Id == model.ProfessionId && x.IsDelete == CommonConstants.IsNotDelete).First()?.Name,
                            ClassName = !model.ClassId.HasValue || model.ClassId == 0 ? "" : DbContext.FreeSql.Select<YssxClass>().Where(x => x.Id == model.ClassId && x.IsDelete == CommonConstants.IsNotDelete).First()?.Name
                        };
                    }
                    tran.Commit();
                }
                return new ResponseContext<FillInActivAtionInformationDto> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = resData, Msg = state ? "成功" : "录入信息失败!" };
            });
        }

    }
}
