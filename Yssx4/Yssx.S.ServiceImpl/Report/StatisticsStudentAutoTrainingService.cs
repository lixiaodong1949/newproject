﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.Entity;
using Yssx.Framework.AutoMapper;
using Yssx.S.Poco;
using Yssx.S.Pocos.Exam;
using Yssx.Framework.Logger;
using Tas.Common.Utils;
using Yssx.Repository.Extensions;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 统计学生自主实训服务
    /// </summary>
    public class StatisticsStudentAutoTrainingService : IStatisticsStudentAutoTrainingService
    {
        #region 获取学校企业(案例)列表
        /// <summary>
        /// 获取学校企业(案例)列表
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<SchoolCaseViewModel>>> GetSchoolCaseList(UserTicket user)
        {
            ResponseContext<List<SchoolCaseViewModel>> response = new ResponseContext<List<SchoolCaseViewModel>>();
            if (user == null)
                return response;

            response.Data = await DbContext.FreeSql.Select<ExamPaper>().From<YssxCase>(
                (a, b) =>
                a.InnerJoin(x => x.CaseId == b.Id))
                .Where((a, b) => a.TenantId == user.TenantId && a.ExamSourceType == ExamSourceType.Order && a.TaskId == 0 && a.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new SchoolCaseViewModel
                {
                    CaseId = a.CaseId,
                    CaseName = b.Name,
                });

            return response;
        }
        #endregion

        #region 获取学生自主实训已完成列表
        /// <summary>
        /// 获取学生自主实训已完成列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<StudentAutoTrainingFinishedViewModel>> GetStudentAutoTrainingFinishedList(StudentAutoTrainingQuery query, UserTicket user)
        {
            var result = new PageResponse<StudentAutoTrainingFinishedViewModel>();
            if (query == null || user == null)
                return result;

            var select = DbContext.FreeSql.Select<ExamStudentGrade>().From<ExamPaper, YssxCase, YssxStudent, YssxTeacherPlanningRelation, YssxClass>(
                (a, b, c, d, e, f) =>
                a.InnerJoin(x => x.ExamId == b.Id)
                .InnerJoin(x => b.CaseId == c.Id)
                .InnerJoin(x => x.UserId == d.UserId)
                .InnerJoin(x => d.ClassId == e.ClassId)
                .InnerJoin(x => d.ClassId == f.Id))
                .Where((a, b, c, d, e, f) => b.ExamSourceType == (int)ExamSourceType.Order && a.Status == StudentExamStatus.End && e.UserId == user.Id
                    && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && d.IsDelete == CommonConstants.IsNotDelete &&
                    e.IsDelete == CommonConstants.IsNotDelete && f.IsDelete == CommonConstants.IsNotDelete);

            if (query.ClassId.HasValue)
                select.Where((a, b, c, d, e, f) => e.ClassId == query.ClassId.Value);
            if (query.CaseId.HasValue)
                select.Where((a, b, c, d, e, f) => c.Id == query.CaseId.Value);
            if (query.StartTime.HasValue)
                select.Where((a, b, c, d, e, f) => a.SubmitTime >= query.StartTime);
            if (query.EndTime.HasValue)
                select.Where((a, b, c, d, e, f) => a.SubmitTime < query.EndTime.Value.AddDays(1));
            if (!string.IsNullOrEmpty(query.StudentName))
                select.Where((a, b, c, d, e, f) => d.Name.Contains(query.StudentName));

            switch (query.SortingType)
            {
                case SortType.StudentNo:
                    select.OrderBy((a, b, c, d, e, f) => d.StudentNo);
                    break;
                case SortType.StudentNoDesc:
                    select.OrderByDescending((a, b, c, d, e, f) => d.StudentNo);
                    break;
                case SortType.Score:
                    select.OrderBy((a, b, c, d, e, f) => a.Score);
                    break;
                case SortType.ScoreDesc:
                    select.OrderByDescending((a, b, c, d, e, f) => a.Score);
                    break;
                case SortType.SubmitTime:
                    select.OrderBy((a, b, c, d, e, f) => a.SubmitTime);
                    break;
                case SortType.SubmitTimeDesc:
                    select.OrderByDescending((a, b, c, d, e, f) => a.SubmitTime);
                    break;
                default:
                    break;
            }

            //if (query.SortingType == 0)
            //    select.OrderByDescending((a, b, c, d, e, f) => a.SubmitTime);
            //if (query.SortingType == 1)
            //    select.OrderByDescending((a, b, c, d, e, f) => a.Score);
            //if (query.SortingType == 2)
            //    select.OrderBy((a, b, c, d, e, f) => a.UsedSeconds);

            long totalCount = 0;
            var items = new List<StudentAutoTrainingFinishedViewModel>(); 
            switch (query.StatisticalType)
            {
                case StatisticalType.All:
                    items=await select.Count(out totalCount).Page(query.PageIndex, query.PageSize)
                       .ToListAsync((a, b, c, d, e, f) => new StudentAutoTrainingFinishedViewModel
                       {
                           UserId=a.UserId,
                           StudentName = d.Name,
                           StudentNo = d.StudentNo,
                           ClassName = f.Name,
                           CaseName = c.Name,
                           Score = a.Score,
                           UsedSeconds = Math.Round(a.UsedSeconds, MidpointRounding.AwayFromZero),
                           SubmitTime = a.SubmitTime,
                           CaseId = b.CaseId,
                           RollUpType = c.RollUpType,
                           CaseYear = c.CaseYear,
                           ExamId = b.Id,
                           GradeId = a.Id
                       });
                    break;
                case StatisticalType.MaxScore:
                    totalCount = select.GroupBy((a, b, c, d, e, f) => new { a.UserId, d.Name, d.StudentNo, b.CaseId, b.Id })
                        .ToList(s => new { count=s.Count()}).Count();

                    items = await select.GroupBy((a, b, c, d, e, f) => new { a.UserId, d.Name, d.StudentNo, b.CaseId, b.Id })
                        .Page(query.PageIndex, query.PageSize)
                        .ToListAsync(s => new StudentAutoTrainingFinishedViewModel
                        {
                            UserId = s.Key.UserId,
                            StudentName = s.Key.Name,
                            StudentNo = s.Key.StudentNo,
                            ClassName = s.Value.Item6.Name,
                            CaseName = s.Value.Item3.Name,
                            Score = Convert.ToDecimal("max(a.Score)"),
                            //UsedSeconds = Math.Round(a.UsedSeconds, MidpointRounding.AwayFromZero),
                            //SubmitTime = a.SubmitTime,
                            CaseId = s.Value.Item2.CaseId,
                            RollUpType = s.Value.Item3.RollUpType,
                            CaseYear = s.Value.Item3.CaseYear,
                            ExamId = s.Key.Id,
                            //GradeId = a.Id
                        });
                    items.ForEach(async s =>
                    {
                        var grade = await DbContext.FreeSql.Select<ExamStudentGrade>()
                        .Where(e => e.UserId == s.UserId && e.ExamId == s.ExamId && e.Score == s.Score).FirstAsync();//$"{CommonConstants.Cache_GetGradeByUserIdExamId}{s.UserId}-{s.ExamId}", true, 30 * 60
                        s.GradeId = grade.Id;
                        s.UsedSeconds = Math.Round(grade.UsedSeconds, MidpointRounding.AwayFromZero);
                        s.SubmitTime = grade.SubmitTime;
                    });
                    break;
                case StatisticalType.SubmitTime:
                    totalCount = select.GroupBy((a, b, c, d, e, f) => new { a.UserId,d.Name, d.StudentNo, b.CaseId, b.Id })
                        .ToList(s => new { count = s.Count() }).Count();

                    items = await select.GroupBy((a, b, c, d, e, f) => new { a.UserId, d.Name, d.StudentNo, b.CaseId, b.Id })
                        .Page(query.PageIndex, query.PageSize)
                        .ToListAsync(s => new StudentAutoTrainingFinishedViewModel
                        {
                            UserId = s.Key.UserId,
                            StudentName = s.Key.Name,
                            StudentNo = s.Key.StudentNo,
                            ClassName = s.Value.Item6.Name,
                            CaseName = s.Value.Item3.Name,
                            SubmitTime = Convert.ToDateTime("max(a.SubmitTime)"),
                            CaseId = s.Value.Item2.CaseId,
                            RollUpType = s.Value.Item3.RollUpType,
                            CaseYear = s.Value.Item3.CaseYear,
                            ExamId = s.Key.Id
                        });
                    items.ForEach(async s =>
                    {
                        var grade = await DbContext.FreeSql.Select<ExamStudentGrade>()
                        .Where(e => e.UserId == s.UserId && e.ExamId == s.ExamId && e.SubmitTime == s.SubmitTime).FirstAsync();//$"{CommonConstants.Cache_GetGradeByUserIdExamId}{s.UserId}-{s.ExamId}", true, 30 * 60
                        s.GradeId = grade.Id;
                        s.UsedSeconds = Math.Round(grade.UsedSeconds, MidpointRounding.AwayFromZero);
                        s.Score = grade.Score;
                    });
                    break;
                default:
                    break;
            }

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 导出学生自主实训已完成列表
        /// <summary>
        /// 导出学生自主实训已完成列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<StudentAutoTrainingFinishedViewModel>>> ExportStudentAutoTrainingFinishedList(StudentAutoTrainingQuery query, UserTicket user)
        {
            ResponseContext<List<StudentAutoTrainingFinishedViewModel>> response = new ResponseContext<List<StudentAutoTrainingFinishedViewModel>>();

            List<StudentAutoTrainingFinishedViewModel> listData = new List<StudentAutoTrainingFinishedViewModel>();
            if (query == null || user == null)
                return response;

            var select = DbContext.FreeSql.Select<ExamStudentGrade>().From<ExamPaper, YssxCase, YssxStudent, YssxTeacherPlanningRelation, YssxClass>(
                (a, b, c, d, e, f) =>
                a.InnerJoin(x => x.ExamId == b.Id)
                .InnerJoin(x => b.CaseId == c.Id)
                .InnerJoin(x => x.UserId == d.UserId)
                .InnerJoin(x => d.ClassId == e.ClassId)
                .InnerJoin(x => d.ClassId == f.Id))
                .Where((a, b, c, d, e, f) => b.ExamSourceType == (int)ExamSourceType.Order && a.Status == StudentExamStatus.End && e.UserId == user.Id
                    && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && d.IsDelete == CommonConstants.IsNotDelete &&
                    e.IsDelete == CommonConstants.IsNotDelete && f.IsDelete == CommonConstants.IsNotDelete);

            if (query.ClassId.HasValue)
                select.Where((a, b, c, d, e, f) => e.ClassId == query.ClassId.Value);
            if (query.CaseId.HasValue)
                select.Where((a, b, c, d, e, f) => c.Id == query.CaseId.Value);
            if (query.StartTime.HasValue)
                select.Where((a, b, c, d, e, f) => a.SubmitTime >= query.StartTime);
            if (query.EndTime.HasValue)
                select.Where((a, b, c, d, e, f) => a.SubmitTime < query.StartTime.Value.AddDays(1));
            if (!string.IsNullOrEmpty(query.StudentName))
                select.Where((a, b, c, d, e, f) => d.Name.Contains(query.StudentName));

            //listData = await select.ToListAsync((a, b, c, d, e, f) => new StudentAutoTrainingFinishedViewModel
            //{
            //    StudentName = d.Name,
            //    ClassName = f.Name,
            //    CaseName = c.Name,
            //    Score = a.Score,
            //    UsedSeconds = (long)a.UsedSeconds,
            //    SubmitTime = a.SubmitTime,
            //    CaseId = b.CaseId,
            //    RollUpType = c.RollUpType,
            //    CaseYear = c.CaseYear,
            //    ExamId = b.Id,
            //    GradeId = a.Id,
            //});

            switch (query.SortingType)
            {
                case SortType.StudentNo:
                    select.OrderBy((a, b, c, d, e, f) => d.StudentNo);
                    break;
                case SortType.StudentNoDesc:
                    select.OrderByDescending((a, b, c, d, e, f) => d.StudentNo);
                    break;
                case SortType.Score:
                    select.OrderBy((a, b, c, d, e, f) => a.Score);
                    break;
                case SortType.ScoreDesc:
                    select.OrderByDescending((a, b, c, d, e, f) => a.Score);
                    break;
                case SortType.SubmitTime:
                    select.OrderBy((a, b, c, d, e, f) => a.SubmitTime);
                    break;
                case SortType.SubmitTimeDesc:
                    select.OrderByDescending((a, b, c, d, e, f) => a.SubmitTime);
                    break;
                default:
                    break;
            }
            var items = new List<StudentAutoTrainingFinishedViewModel>();
            switch (query.StatisticalType)
            {
                case StatisticalType.All:
                    items = await select
                       .ToListAsync((a, b, c, d, e, f) => new StudentAutoTrainingFinishedViewModel
                       {
                           UserId = a.UserId,
                           StudentName = d.Name,
                           StudentNo = d.StudentNo,
                           ClassName = f.Name,
                           CaseName = c.Name,
                           Score = a.Score,
                           UsedSeconds = Math.Round(a.UsedSeconds, MidpointRounding.AwayFromZero),
                           SubmitTime = a.SubmitTime,
                           CaseId = b.CaseId,
                           RollUpType = c.RollUpType,
                           CaseYear = c.CaseYear,
                           ExamId = b.Id,
                           GradeId = a.Id
                       });
                    break;
                case StatisticalType.MaxScore:
                    items = await select.GroupBy((a, b, c, d, e, f) => new { a.UserId, d.Name, d.StudentNo, b.CaseId, b.Id })
                        .ToListAsync(s => new StudentAutoTrainingFinishedViewModel
                        {
                            UserId = s.Key.UserId,
                            StudentName = s.Key.Name,
                            StudentNo = s.Key.StudentNo,
                            ClassName = s.Value.Item6.Name,
                            CaseName = s.Value.Item3.Name,
                            Score = Convert.ToDecimal("max(a.Score)"),
                            CaseId = s.Value.Item2.CaseId,
                            RollUpType = s.Value.Item3.RollUpType,
                            CaseYear = s.Value.Item3.CaseYear,
                            ExamId = s.Key.Id
                        });
                    items.ForEach(async s =>
                    {
                        var grade = await DbContext.FreeSql.Select<ExamStudentGrade>()
                        .Where(e => e.UserId == s.UserId && e.ExamId == s.ExamId && e.Score == s.Score).FirstAsync();//$"{CommonConstants.Cache_GetGradeByUserIdExamId}{s.UserId}-{s.ExamId}", true, 30 * 60
                        s.GradeId = grade.Id;
                        s.UsedSeconds = Math.Round(grade.UsedSeconds, MidpointRounding.AwayFromZero);
                        s.SubmitTime = grade.SubmitTime;
                    });
                    break;
                case StatisticalType.SubmitTime:
                    items = await select.GroupBy((a, b, c, d, e, f) => new { a.UserId, d.Name, d.StudentNo, b.CaseId, b.Id })
                        .ToListAsync(s => new StudentAutoTrainingFinishedViewModel
                        {
                            UserId = s.Key.UserId,
                            StudentName = s.Key.Name,
                            StudentNo = s.Key.StudentNo,
                            ClassName = s.Value.Item6.Name,
                            CaseName = s.Value.Item3.Name,
                            SubmitTime = Convert.ToDateTime("max(a.SubmitTime)"),
                            CaseId = s.Value.Item2.CaseId,
                            RollUpType = s.Value.Item3.RollUpType,
                            CaseYear = s.Value.Item3.CaseYear,
                            ExamId = s.Key.Id
                        });
                    items.ForEach(async s =>
                    {
                        var grade = await DbContext.FreeSql.Select<ExamStudentGrade>()
                        .Where(e => e.UserId == s.UserId && e.ExamId == s.ExamId && e.SubmitTime == s.SubmitTime).FirstAsync();//$"{CommonConstants.Cache_GetGradeByUserIdExamId}{s.UserId}-{s.ExamId}", true, 30 * 60
                        s.GradeId = grade.Id;
                        s.UsedSeconds = Math.Round(grade.UsedSeconds, MidpointRounding.AwayFromZero);
                        s.Score = grade.Score;
                    });
                    break;
                default:
                    break;
            }
            response.Data = items;
            //if (query.SortingType == 0)
            //    response.Data = listData.OrderByDescending(x => x.SubmitTime).ToList();
            //if (query.SortingType == 1)
            //    response.Data = listData.OrderByDescending(x => x.Score).ToList();
            //if (query.SortingType == 2)
            //    response.Data = listData.OrderBy(x => x.UsedSeconds).ToList();

            return response;
        }
        #endregion

    }
}
