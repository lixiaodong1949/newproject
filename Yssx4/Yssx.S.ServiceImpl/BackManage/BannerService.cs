﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos.BackManage;

namespace Yssx.S.ServiceImpl
{
    public class BannerService: IBannerService
    {
        /// <summary>
        /// 增加/修改
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateBanner(BannerRequest model,long userId)
        {
            return await Task.Run(() =>
            {
                var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
                try
                {
                    YssxBanner yssxBanner = model.MapTo<YssxBanner>();
                    if (yssxBanner.Id == 0)
                    {
                        yssxBanner.Id = IdWorker.NextId();
                        yssxBanner.CreateBy = userId;
                        DbContext.FreeSql.Insert(yssxBanner).ExecuteAffrows();
                    }
                    else
                    {
                        yssxBanner.UpdateBy = userId;
                        yssxBanner.UpdateTime = DateTime.Now;
                        DbContext.FreeSql.Update<YssxBanner>(yssxBanner).SetSource(yssxBanner).ExecuteAffrows();
                    }
                }
                catch (Exception)
                {
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
                return result;
            });
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteBanner(long id)
        {
            //删除案例主表  逻辑删除
            await DbContext.FreeSql.GetRepository<YssxBanner>().UpdateDiy.Set(c => new YssxBanner
            {
                IsDelete = CommonConstants.IsDelete,
                UpdateTime = DateTime.Now
            }).Where(c => c.Id == id).ExecuteAffrowsAsync();
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
        /// <summary>
        /// 查询Banner列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<BannerInfoDto>>> GetBannerInfoList(ClientType? clientType)
        {
            var select = DbContext.FreeSql.Select<YssxBanner>()
                .Where(c => c.IsDelete == CommonConstants.IsNotDelete);

            if (clientType!=null)
                select.Where(c => c.ClientType == clientType);

            var sql = select.ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<BannerInfoDto>(sql);

            return new ResponseContext<List<BannerInfoDto>>(CommonConstants.SuccessCode,"",items);
        }
    }
}
