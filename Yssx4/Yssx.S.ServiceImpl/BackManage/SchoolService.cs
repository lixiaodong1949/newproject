﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.Poco;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 学校管理
    /// </summary>
    public class SchoolService : ISchoolService
    {
        #region 学校
        /// <summary>
        /// 新增 更新 学校
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateSchool(SchoolDto model, long Id)
        {
            var dtNow = DateTime.Now;
            var dtNowAddOneYear = dtNow.AddYears(1);
            long opreationId = Id;//操作人ID
            var rEducation = (int)model.Type;
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学校名称不能为空" };
            if (string.IsNullOrEmpty(model.Code))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "院校代码不能为空" };
            if (string.IsNullOrEmpty(model.Province) || string.IsNullOrEmpty(model.City))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学校所属省市不能为空" };
            //修改
            if (model.Id.HasValue)
            {
                var oldSchool = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.Id == model.Id).First();
                if (oldSchool == null)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                }
                return await Task.Run(() =>
                {
                    return UpdateSchool(model, Id);
                });
            }
            //新增
            if (DbContext.FreeSql.Select<YssxSchool>().Any(m => (m.Name == model.Name || m.Code == model.Code) && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学校名称或代码重复" };
            #region 处理数据
            //学校
            var school = model.MapTo<YssxSchool>();
            school.Id = IdWorker.NextId();
            school.CreateBy = opreationId;
            school.CreateTime = dtNow;
            ////订单
            //var rCaseOrder = new CaseOrder
            //{
            //    Id = IdWorker.NextId(),
            //    SchoolId = school.Id,
            //    SchoolName = model.Name,
            //    Status = Status.Enable,
            //    Sort = 1,
            //    CreateBy = opreationId,
            //    CreateTime = dtNow,
            //    UpdateBy = opreationId,
            //    UpdateTime = dtNow
            //};
            ////订单-免费案例
            //var listFreeCase = DbContext.FreeSql.GetRepository<YssxFreeCase>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).ToList();
            //var listSchoolCase = listFreeCase.Select(m => new YssxSchoolCase
            //{
            //    Id = IdWorker.NextId(),
            //    OrderId = rCaseOrder.Id,
            //    SchoolId = school.Id,
            //    CaseId = m.CaseId,
            //    CaseName = m.CaseName,
            //    Education = rEducation,

            //    CaseUserType = CaseUserTypeEnums.Educational,
            //    UserName = "free_case",
            //    OrderUser = OrderUserEnum.StudentAndTeacher,
            //    OpenTime = dtNow,
            //    ExpireTime = dtNowAddOneYear,
            //    PayStatus = PayStatus.Paid,
            //    PersonInCharge = "专业组",
            //    OpenStatus = Status.Enable,
            //    TestStatus = 1,
            //    OfficialStatus = 1,
            //    Sort = 1,
            //    CreateBy = opreationId,
            //    UpdateBy = opreationId,
            //    UpdateTime = dtNow
            //}).ToList();
            ////试卷
            //var rCaseInfoData = await DbContext.FreeSql.GetRepository<YssxFreeCase>().Select.From<YssxCase, YssxTopic>((a, b, c) => a
            //    .LeftJoin(aa => aa.CaseId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
            //    .LeftJoin(aa => b.Id == c.CaseId && c.ParentId == 0 && c.IsDelete == CommonConstants.IsNotDelete))
            //    .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete)
            //    .GroupBy((a, b, c) => new { a.Id, a.CaseId, b.Sort, b.CompetitionType, b.TotalTime })
            //    .ToListAsync(a => new
            //    {
            //        a.Key.CaseId,
            //        a.Key.Sort,
            //        a.Value.Item2.CompetitionType,
            //        a.Value.Item2.TotalTime,
            //        TotalQuestionCount = "Count(c.id)",
            //        TotalScore = a.Sum(a.Value.Item3.Score)
            //    });
            //var listExamPaper = new List<ExamPaper>();
            //foreach (var item in listSchoolCase)
            //{
            //    var rCaseInfo = rCaseInfoData.First(m => m.CaseId == item.CaseId);
            //    if (rCaseInfo == null)
            //        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = string.Format("案例{0}数据异常", item.CaseId) };

            //    var testExamPaper = new ExamPaper()
            //    {
            //        Id = IdWorker.NextId(),
            //        CanShowAnswerBeforeEnd = true,
            //        CaseId = item.CaseId,
            //        Name = item.CaseName,
            //        ExamType = ExamType.PracticeTest,
            //        CompetitionType = rCaseInfo.CompetitionType,
            //        TotalMinutes = rCaseInfo.TotalTime,
            //        TotalScore = rCaseInfo.TotalScore,
            //        TotalQuestionCount = Convert.ToInt32(rCaseInfo.TotalQuestionCount),
            //        TenantId = school.Id,
            //        ExamSourceType = ExamSourceType.Order,
            //        Sort = rCaseInfo.Sort,
            //        IsRelease = true,
            //        CreateBy = opreationId,
            //        UpdateBy = opreationId,
            //        UpdateTime = dtNow
            //    };
            //    listExamPaper.Add(testExamPaper);
            //    //案例是团队赛时, 生成全真模拟试卷
            //    if (rCaseInfo.CompetitionType == CompetitionType.TeamCompetition)
            //    {
            //        var emulationExamPaper = testExamPaper.DeepClone();
            //        emulationExamPaper.Id = IdWorker.NextId();
            //        emulationExamPaper.ExamType = ExamType.EmulationTest;
            //        emulationExamPaper.CanShowAnswerBeforeEnd = false;
            //        listExamPaper.Add(emulationExamPaper);
            //    }
            //}
            #endregion

            bool state = true;
            return await Task.Run(() =>
            {
                //学校
                state = DbContext.FreeSql.Insert(school).ExecuteAffrows() > 0;
                //订单
                //DbContext.FreeSql.Insert(rCaseOrder).ExecuteAffrows();
                ////订单-免费案例
                //if (listFreeCase.Count > 0)
                //    DbContext.FreeSql.Insert<YssxSchoolCase>().AppendData(listSchoolCase).ExecuteAffrows();
                ////试卷
                //if (listExamPaper.Count > 0)
                //    DbContext.FreeSql.Insert<ExamPaper>().AppendData(listExamPaper).ExecuteAffrows();

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }
        /// <summary>
        /// 修改学校信息
        /// </summary>
        /// <returns></returns>
        private ResponseContext<bool> UpdateSchool(SchoolDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            if (DbContext.FreeSql.Select<YssxSchool>().Any(m => (m.Name == model.Name || m.Code == model.Code) && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学校名称或代码重复" };
            }
            var school = model.MapTo<YssxSchool>();
            school.UpdateBy = opreationId;
            school.UpdateTime = DateTime.Now;
            DbContext.FreeSql.Update<YssxSchool>().SetSource(school).IgnoreColumns(m => new { m.IsDelete, m.IsDisplay, m.CreateBy, m.CreateTime }).ExecuteAffrows();
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }
        /// <summary>
        /// 删除学校
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteSchool(SchoolDto model, long Id)
        {
            long opreationId = Id;//操作人ID
            var dtNow = DateTime.Now;

            if (!model.Id.HasValue)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，入参信息异常!" };
            var schoolEntity = await DbContext.FreeSql.GetRepository<YssxSchool>().Where(x => x.Id == model.Id.Value && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (schoolEntity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该学校!" };
            //学校
            schoolEntity.IsDelete = CommonConstants.IsDelete;
            schoolEntity.UpdateBy = opreationId;
            schoolEntity.UpdateTime = dtNow;
            //激活码
            var rYssxActivationCode = DbContext.FreeSql.GetRepository<YssxActivationCode>()
                .Where(x => x.SchoolId == model.Id && x.IsDelete == CommonConstants.IsNotDelete).ToList(x => new YssxActivationCode
                {
                    Id = x.Id,
                    IsDelete = CommonConstants.IsDelete,
                    UpdateBy = opreationId,
                    UpdateTime = dtNow
                });
            return await Task.Run(() =>
            {
                //学校
                var state = DbContext.FreeSql.Update<YssxSchool>().SetSource(schoolEntity).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows() > 0;
                //激活码
                if (rYssxActivationCode.Count > 0)
                    DbContext.FreeSql.Update<YssxActivationCode>().SetSource(rYssxActivationCode).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
            });
        }
        /// <summary>
        /// 获取学校列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<SchoolDto>>> GetSchoolList(SchoolPageRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(m => m.CreateTime);
                if (model.SchoolId.HasValue)
                    select = select.Where(m => m.Id == model.SchoolId);
                if (!String.IsNullOrEmpty(model.Name))
                    select = select.Where(m => m.Name.Contains(model.Name) || m.ShortName.Contains(model.Name));
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql();
                var items = DbContext.FreeSql.Ado.Query<SchoolDto>(sql);
                var res = new PageResponse<SchoolDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
                return new ResponseContext<PageResponse<SchoolDto>> { Code = CommonConstants.SuccessCode, Data = res };
            });
        }

        /// <summary>
        /// 导出学校
        /// </summary>
        /// <returns></returns>      
        public async Task<IActionResult> ExportSchool(long tenantId, string name)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(m => m.CreateTime);
                if (tenantId > 0)
                    select = select.Where(m => m.Id == tenantId);
                if (!String.IsNullOrEmpty(name))
                    select = select.Where(m => m.Name.Contains(name) || m.ShortName.Contains(name));
                var totalCount = select.Count();
                var sql = select.ToSql();
                var rSchoolData = DbContext.FreeSql.Ado.Query<SchoolDto>(sql);

                var selectData = rSchoolData.Select(m => new ExportSchoolDto
                {
                    Name = m.Name,
                    Code = m.Code,
                    SchoolType = m.Type.GetDescription(),
                    ShortName = m.ShortName,
                    Address = m.Address,
                    DeanName = m.DeanName,
                    AccountStartTime = m.AccountStartTime,
                    AccountEndTime = m.AccountEndTime,
                    Status = m.Status.GetDescription()
                }).ToList();
                byte[] ms = NPOIHelper<ExportSchoolDto>.OutputExcel(selectData, new string[] { "111" });

                var provider = new FileExtensionContentTypeProvider();
                if (!provider.Mappings.TryGetValue(".xlsx", out var memi))
                {
                    throw new Exception("找不到可导出的文件类型");
                }
                //return new FileContentResult(ms, memi, Guid.NewGuid().ToString() + ".xlsx");
                return new FileContentResult(ms, memi);
            });
        }
        #endregion

        #region 院系
        /// <summary>
        /// 添加 更新 院系
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateSchoolCollege(CollegeDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            //TODO 教务和平台分开两个方法添加
            if (model.Id.HasValue)
            {
                var oldcollgeg = DbContext.FreeSql.Select<YssxCollege>().Where(m => m.Id == model.Id).First();
                if (oldcollgeg == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

                return await Task.Run(() =>
                {
                    return UpdateSchoolCollege(model, Id);
                });
            }

            if (DbContext.FreeSql.Select<YssxCollege>().Any(m => m.TenantId == model.TenantId && m.Name == model.Name && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学校院系名称重复" };
            var college = model.MapTo<YssxCollege>();
            college.Id = IdWorker.NextId();
            college.CreateBy = opreationId;
            college.CreateTime = DateTime.Now;
            return await Task.Run(() =>
            {
                DbContext.FreeSql.Insert(college).ExecuteAffrows();
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
        /// <summary>
        /// 修改院系信息
        /// </summary>
        /// <returns></returns>
        private ResponseContext<bool> UpdateSchoolCollege(CollegeDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            var oldcollgeg = DbContext.FreeSql.Select<YssxCollege>().Where(m => m.Id == model.Id && m.TenantId == model.TenantId && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (oldcollgeg == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息，学校ID有误!" };
            if (DbContext.FreeSql.Select<YssxCollege>().Any(m => m.TenantId == model.TenantId && m.Name == model.Name && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "院系名称重复" };
            var college = model.MapTo<YssxCollege>();
            college.UpdateBy = opreationId;
            college.UpdateTime = DateTime.Now;
            DbContext.FreeSql.Update<YssxCollege>().SetSource(college).IgnoreColumns(m => new { m.IsDelete, m.CreateBy, m.CreateTime, m.TenantId }).ExecuteAffrows();
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }
        /// <summary>
        /// 删除院系
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteSchoolCollege(CollegeDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            //TODO租户过滤
            if (!model.Id.HasValue)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，ID不能为空!" };
            var oldcollgeg = DbContext.FreeSql.Select<YssxCollege>().Where(m => m.Id == model.Id.Value && m.IsDelete == CommonConstants.IsNotDelete);
            if (!oldcollgeg.Any())
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };
            var college = new YssxCollege
            {
                Id = model.Id.Value,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = opreationId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                DbContext.FreeSql.Update<YssxCollege>().SetSource(college).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
        /// <summary>
        /// 获取 学校-院系 列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<CollegeDto>>> GetSchoolColleges(SchoolCollegeRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxCollege>().From<YssxSchool>((a, b) => a.InnerJoin(aa => aa.TenantId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete).OrderByDescending((a, b) => a.CreateTime);
                if (model.TenantId.HasValue)
                    select = select.Where((a, b) => a.TenantId == model.TenantId);
                if (!String.IsNullOrEmpty(model.Name))
                    select = select.Where((a, b) => a.Name.Contains(model.Name));

                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.*,b.Name as SchoolName");
                var items = DbContext.FreeSql.Ado.Query<CollegeDto>(sql);
                var res = new PageResponse<CollegeDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
                return new ResponseContext<PageResponse<CollegeDto>> { Code = CommonConstants.SuccessCode, Data = res };
            });
        }

        /// <summary>
        /// 导出院系
        /// </summary>
        /// <returns></returns>      
        public async Task<IActionResult> ExportCollege(long tenantId, string name)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxCollege>().From<YssxSchool>((a, b) => a.InnerJoin(aa => aa.TenantId == b.Id))
                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete).OrderByDescending((a, b) => a.CreateTime);
                if (tenantId > 0)
                    select = select.Where((a, b) => a.TenantId == tenantId);
                if (!String.IsNullOrEmpty(name))
                    select = select.Where((a, b) => a.Name.Contains(name));

                var totalCount = select.Count();
                var sql = select.ToSql("a.*,b.Name as SchoolName");
                var rCollegeData = DbContext.FreeSql.Ado.Query<CollegeDto>(sql);

                var selectData = rCollegeData.Select(m => new ExportCollegeDto
                {
                    Name = m.Name,
                    President = m.President,
                    Status = m.Status.GetDescription(),
                    SchoolName = m.SchoolName
                }).ToList();
                byte[] ms = NPOIHelper<ExportCollegeDto>.OutputExcel(selectData, new string[] { "111" });

                var provider = new FileExtensionContentTypeProvider();
                if (!provider.Mappings.TryGetValue(".xlsx", out var memi))
                {
                    throw new Exception("找不到可导出的文件类型");
                }
                //return new FileContentResult(ms, memi, Guid.NewGuid().ToString() + ".xlsx");
                return new FileContentResult(ms, memi);
            });
        }
        #endregion

        #region 专业
        /// <summary>
        /// 添加 更新 专业
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateCollegeProfession(ProfessionDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            if (model.Id.HasValue)
            {
                //修改
                var oldProfession = DbContext.FreeSql.Select<YssxProfession>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (oldProfession == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                var isExist = DbContext.FreeSql.Select<YssxProfession>().Any(m => m.CollegeId == model.CollegeId && m.Name == model.Name && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                if (isExist)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "同一院系专业名称重复" };

                oldProfession.UpdateTime = DateTime.Now;
                oldProfession.UpdateBy = opreationId;
                oldProfession.Name = model.Name;
                return await Task.Run(() =>
                {
                    DbContext.FreeSql.Update<YssxProfession>().SetSource(oldProfession).UpdateColumns(m => new { m.UpdateBy, m.UpdateTime, m.Name }).ExecuteAffrows();
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                });
            }
            else
            {
                //添加
                if (DbContext.FreeSql.Select<YssxProfession>().Any(m => m.CollegeId == model.CollegeId && m.Name == model.Name && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "同一院系专业名称重复" };
                var profession = new YssxProfession
                {
                    Name = model.Name,
                    TenantId = model.TenantId,
                    CollegeId = model.CollegeId,
                    Id = IdWorker.NextId(),
                    CreateBy = opreationId,
                    CreateTime = DateTime.Now,
                };
                return await Task.Run(() =>
                {
                    DbContext.FreeSql.Insert(profession).ExecuteAffrows();
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                });
            }
        }
        /// <summary>
        /// 删除专业
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCollegeProfession(ProfessionDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            //TODO 租户过滤
            if (!model.Id.HasValue)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };
            var profession = new YssxProfession
            {
                Id = model.Id.Value,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = opreationId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                DbContext.FreeSql.Update<YssxProfession>().SetSource(profession).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
        /// <summary>
        /// 获取 学校-院系-专业 列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<ProfessionDto>>> GetCollegeProfessions(CollegeProfessionsRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxProfession>()
                .From<YssxSchool, YssxCollege>((a, b, c) => a.InnerJoin(aa => aa.TenantId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                .InnerJoin(aa => aa.CollegeId == c.Id && c.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete).OrderByDescending((a, b, c) => a.CreateTime);
                //筛选学校、院系
                if (model.TenantId != null)
                    select = select.Where((a, b, c) => a.TenantId == model.TenantId);
                if (model.CollegesId != null)
                    select = select.Where((a, b, c) => a.CollegeId == model.CollegesId);
                if (!String.IsNullOrEmpty(model.Name))
                    select = select.Where((a, b, c) => a.Name.Contains(model.Name));

                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.Id,a.Name,a.CollegeId,a.TenantId,b.Name as SchoolName,c.Name as CollegeName,c.President");
                var items = DbContext.FreeSql.Ado.Query<ProfessionDto>(sql);
                var res = new PageResponse<ProfessionDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
                return new ResponseContext<PageResponse<ProfessionDto>> { Code = CommonConstants.SuccessCode, Data = res };
            });

        }

        /// <summary>
        /// 导出专业
        /// </summary>
        /// <returns></returns>      
        public async Task<IActionResult> ExportProfession(long tenantId, long collegesId, string name)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxProfession>()
                .From<YssxSchool, YssxCollege>((a, b, c) => a.InnerJoin(aa => aa.TenantId == b.Id).InnerJoin(aa => aa.CollegeId == c.Id))
                .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete).OrderByDescending((a, b, c) => a.CreateTime);
                //筛选学校、院系
                if (tenantId > 0)
                    select = select.Where((a, b, c) => a.TenantId == tenantId);
                if (collegesId > 0)
                    select = select.Where((a, b, c) => a.CollegeId == collegesId);
                if (!String.IsNullOrEmpty(name))
                    select = select.Where((a, b, c) => a.Name.Contains(name));

                var totalCount = select.Count();
                var sql = select.ToSql("a.Id,a.Name,a.CollegeId,a.TenantId,b.Name as SchoolName,c.Name as CollegeName,c.President");
                var selectData = DbContext.FreeSql.Ado.Query<ExportProfessionDto>(sql);
                byte[] ms = NPOIHelper<ExportProfessionDto>.OutputExcel(selectData, new string[] { "111" });

                var provider = new FileExtensionContentTypeProvider();
                if (!provider.Mappings.TryGetValue(".xlsx", out var memi))
                {
                    throw new Exception("找不到可导出的文件类型");
                }
                //return new FileContentResult(ms, memi, Guid.NewGuid().ToString() + ".xlsx");
                return new FileContentResult(ms, memi);
            });
        }
        #endregion

        #region 班级
        /// <summary>
        /// 添加 更新 班级
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateClass(ClassDto model, long currentUserId)
        {
            var dtNow = DateTime.Now;
            if (!DateTime.TryParse(model.SchoolYear + "-01-01", out var entranceDateTime))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "入学年份格式有误" };
            }
            if (model.Id.HasValue)
            {
                var oldClass = DbContext.FreeSql.Select<YssxClass>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (oldClass == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                if (string.IsNullOrWhiteSpace(model.Name))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级名称不能为空" };
                if (DbContext.FreeSql.Select<YssxClass>().Any(m => m.TenantId == oldClass.TenantId && m.Name == model.Name && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级名称重复" };

                oldClass.UpdateTime = dtNow;
                oldClass.UpdateBy = currentUserId;
                oldClass.EntranceDateTime = entranceDateTime;
                oldClass.Name = model.Name;
                return await Task.Run(() =>
                {
                    DbContext.FreeSql.Update<YssxClass>().SetSource(oldClass).UpdateColumns(m => new { m.UpdateBy, m.UpdateTime, m.Name, m.EntranceDateTime }).ExecuteAffrows();
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                });
            }

            //验证
            var rClassNameData = model.Name.Split('、').ToList();
            if (rClassNameData.GroupBy(m => m).Count() != rClassNameData.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "待添加的班级名称列表重复" };
            foreach (var itemName in rClassNameData)
            {
                if (string.IsNullOrWhiteSpace(itemName))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级名称不能为空" };
                if (DbContext.FreeSql.Select<YssxClass>().Any(m => m.TenantId == model.TenantId && m.Name == itemName && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = string.Format("班级【{0}】已添加", itemName) };
            }
            //处理数据
            var rClassData = rClassNameData.Select(m => new YssxClass
            {
                Id = IdWorker.NextId(),
                Name = m,
                TenantId = model.TenantId,
                CollegeId = model.CollegeId,
                ProfessionId = model.ProfessionId,
                EntranceDateTime = entranceDateTime,
                CreateBy = currentUserId,
                CreateTime = dtNow,
                UpdateBy = currentUserId,
                UpdateTime = dtNow
            }).ToList();
            return await Task.Run(() =>
            {
                var state = DbContext.FreeSql.Insert<YssxClass>().AppendData(rClassData).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }
        /// <summary>
        /// 删除班级
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteClass(ClassDto model, long currentUserId)
        {
            if (!model.Id.HasValue)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };
            var @class = new YssxClass
            {
                Id = model.Id.Value,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = currentUserId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                DbContext.FreeSql.Update<YssxClass>().SetSource(@class).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
        /// <summary>
        /// 获取 学校-院系-专业-班级 列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<ClassDto>>> GetClasses(ClassRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxClass>().From<YssxProfession, YssxCollege, YssxSchool>((a, b, c, d) =>
                    a.LeftJoin(aa => aa.ProfessionId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.CollegeId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
                    .InnerJoin(aa => aa.TenantId == d.Id && d.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d) => a.IsDelete == CommonConstants.IsNotDelete).OrderByDescending((a, b, c, d) => a.CreateTime);

                //筛选
                if (model.TenantId.HasValue)
                    select = select.Where((a, b, c, d) => a.TenantId == model.TenantId);
                if (model.CollegeId != null)
                    select = select.Where((a, b, c, d) => a.CollegeId == model.CollegeId);
                if (model.ProfessionId != null)
                    select = select.Where((a, b, c, d) => a.ProfessionId == model.ProfessionId);
                if (!String.IsNullOrEmpty(model.Name))
                    select = select.Where((a, b, c, d) => a.Name.Contains(model.Name));
                //取数据
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.*,b.Name ProfessionName,c.Name CollegeName,d.Name SchoolName");
                var items = DbContext.FreeSql.Ado.Query<ClassDto>(sql);
                var classArr = items.Select(a => a.Id).ToArray();
                if (classArr.Any())
                {
                    var classStudentCount = DbContext.FreeSql.Select<YssxStudent>().Where(a => classArr.Contains(a.ClassId) && a.IsDelete == CommonConstants.IsNotDelete)
                        .GroupBy(a => a.ClassId).ToList(a => new { a.Key, Count = a.Count() });
                    var classTeachers = DbContext.FreeSql.Select<YssxTeacherClass>().From<YssxTeacher, YssxUser>((a, b, u) => a.Where(s => s.TeacherId == b.Id && b.UserId == u.Id))
                        .Where((a, b, u) => classArr.Contains(a.ClassId)).ToList((a, b, u) => new { a.TeacherId, a.ClassId, u.RealName });
                    items.ForEach(a =>
                    {
                        a.StudentCount = classStudentCount.FirstOrDefault(s => s.Key == a.Id)?.Count ?? 0;
                        var ts = classTeachers.Where(c => c.ClassId == a.Id).Select(c => c.RealName).Distinct().ToArray();
                        a.TeachersName = string.Join(',', ts);
                        a.SchoolYear = a.EntranceDateTime.Year.ToString();
                    });
                }

                var res = new PageResponse<ClassDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
                return new ResponseContext<PageResponse<ClassDto>> { Code = CommonConstants.SuccessCode, Data = res };
            });
        }

        /// <summary>
        /// 添加 更新 班级 - 教师专用
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateClassOnTeacher(ClassDto model, long currentUserId)
        {
            var dtNow = DateTime.Now;
            if (!DateTime.TryParse(model.SchoolYear + "-01-01", out var entranceDateTime))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "入学年份格式有误" };
            }
            if (model.TenantId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学校ID不能为空" };
            if (model.Id.HasValue)
            {
                var oldClass = DbContext.FreeSql.Select<YssxClass>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (oldClass == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                if (string.IsNullOrWhiteSpace(model.Name))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级名称不能为空" };
                if (DbContext.FreeSql.Select<YssxClass>().Any(m => m.TenantId == oldClass.TenantId && m.Name == model.Name && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级名称重复" };

                oldClass.UpdateTime = dtNow;
                oldClass.UpdateBy = currentUserId;
                oldClass.EntranceDateTime = entranceDateTime;
                oldClass.Name = model.Name;
                return await Task.Run(() =>
                {
                    DbContext.FreeSql.Update<YssxClass>().SetSource(oldClass).UpdateColumns(m => new { m.UpdateBy, m.UpdateTime, m.Name, m.EntranceDateTime }).ExecuteAffrows();
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                });
            }

            //验证
            var rClassNameData = model.Name.Split('、').ToList();
            if (rClassNameData.GroupBy(m => m).Count() != rClassNameData.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "待添加的班级名称列表重复" };
            foreach (var itemName in rClassNameData)
            {
                if (string.IsNullOrWhiteSpace(itemName))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级名称不能为空" };
                if (DbContext.FreeSql.Select<YssxClass>().Any(m => m.TenantId == model.TenantId && m.Name == itemName && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = string.Format("班级【{0}】已添加", itemName) };
            }
            //处理数据
            var rClassData = rClassNameData.Select(m => new YssxClass
            {
                Id = IdWorker.NextId(),
                Name = m,
                TenantId = model.TenantId,
                CollegeId = model.CollegeId,
                ProfessionId = model.ProfessionId,
                EntranceDateTime = entranceDateTime,
                CreateBy = currentUserId,
                CreateTime = dtNow,
                UpdateBy = currentUserId,
                UpdateTime = dtNow
            }).ToList();
            //教师自动绑定班级
            var rAddTeacherClassList = rClassData.Select(m => new YssxTeacherPlanningRelation
            {
                Id = IdWorker.NextId(),
                TenantId = m.TenantId,
                UserId = currentUserId,
                ClassId = m.Id,
                ClassName = m.Name,
                CreateBy = currentUserId,
                CreateTime = dtNow
            }).ToList();

            return await Task.Run(() =>
            {
                //班级
                var state = DbContext.FreeSql.Insert<YssxClass>().AppendData(rClassData).ExecuteAffrows() > 0;
                //教师自动绑定班级
                if (rAddTeacherClassList.Count > 0)
                    DbContext.FreeSql.GetRepository<YssxTeacherPlanningRelation>().InsertAsync(rAddTeacherClassList);

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }
        #endregion

        /// <summary>
        /// 学校激活记录
        /// </summary>
        /// <param name="model">搜索条件</param>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<ActivationRecordDto>>> ActivationRecord(ActivationRecordPageRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxActivationInformation>().From<YssxUser, YssxActivationCode, YssxSchool, YssxCollege, YssxProfession, YssxClass>((a, u, ac, s, c, p, cl) =>
                    a.LeftJoin(aa => aa.CreateBy == u.Id && u.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.ActivationCodeId == ac.Id && ac.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.TenantId == s.Id && s.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.DepartmentsId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.ProfessionalId == p.Id && p.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.ClassId == cl.Id && cl.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, u, ac, s, c, p, cl) => a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderByDescending((a, u, ac, s, c, p, cl) => a.CreateTime);
                if (!string.IsNullOrEmpty(model.Code))
                    select = select.Where((a, u, ac, s, c, p, cl) => a.WorkNumber.Contains(model.Code) || u.RealName.Contains(model.Code) || ac.Code.Contains(model.Code));
                if (model.TypeId.HasValue)
                    select = select.Where((a, u, ac, s, c, p, cl) => a.TypeId == model.TypeId);
                if (model.RegistrationType.HasValue)
                    select = select.Where((a, u, ac, s, c, p, cl) => u.RegistrationType == model.RegistrationType);
                if (model.Status.HasValue)
                    if (model.Status != 3)
                    {
                        select = select.Where((a, u, ac, s, c, p, cl) => u.Status == model.Status);
                    }
                    else
                    {
                        select = select.Where((a, u, ac, s, c, p, cl) => ac.EndTime < DateTime.Now);
                    }

                //取数据
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.*,u.RealName RealName,u.MobilePhone,u.RegistrationType RegistrationType,u.Status Status,ac.ActivationDeadline ActivationDeadline,u.Id UserId,c.Name CollegeName,s.Name SchoolName,p.Name ProfessionName,c.Name ClassName,ac.Code Code");
                var items = DbContext.FreeSql.Ado.Query<ActivationRecordDto>(sql);
                var res = new PageResponse<ActivationRecordDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
                return new ResponseContext<PageResponse<ActivationRecordDto>> { Code = CommonConstants.SuccessCode, Data = res };
            });
        }

        /// <summary>
        /// 禁用,启用,换码
        /// </summary>
        /// <param name="id">禁用启用传UserId，换码传记录Id</param>
        /// <param name="type">0禁用1启用2换码</param>
        /// <param name="code">激活码</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpdateActivationRecord(long id, int type, string code)
        {
            return await Task.Run(() =>
            {
                bool state = false;
                switch (type)
                {
                    case 0:
                        state = DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.Set(x => new YssxUser { Status = type, UpdateTime = DateTime.Now }).Where(x => x.Id == id).ExecuteAffrows() > 0;
                        break;
                    case 1:
                        state = DbContext.FreeSql.GetRepository<YssxUser>().UpdateDiy.Set(x => new YssxUser { Status = type, UpdateTime = DateTime.Now }).Where(x => x.Id == id).ExecuteAffrows() > 0;
                        break;
                    case 2:
                        var acode = DbContext.FreeSql.Select<YssxActivationCode>().Where(m => m.Code == code && m.IsDelete == CommonConstants.IsNotDelete && m.EndTime > DateTime.Now).First();
                        if (acode == null)
                            return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "激活码不存在、被删除、超过激活截至日期!", Data = state };
                        state = DbContext.FreeSql.GetRepository<YssxActivationInformation>().UpdateDiy.Set(x => new YssxActivationInformation { ActivationCodeId = acode.Id, UpdateTime = DateTime.Now }).Where(x => x.Id == id).ExecuteAffrows() > 0;
                        break;
                    default:
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "类型不匹配!", Data = state };
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "操作成功!" : "操作失败!", Data = state };
            });
        }

        public async Task<ResponseContext<List<SchoolYearDto>>> GetSchoolYearList(long id)
        {
            List<SchoolYearDto> codes = await DbContext.FreeSql.Select<YssxActivationCode>().Where(m => m.SchoolId == id && m.IsDelete == CommonConstants.IsNotDelete && m.Year > 0).ToListAsync<SchoolYearDto>();

            codes = codes.Distinct().ToList();
            return new ResponseContext<List<SchoolYearDto>> { Data = codes };
        }


    }
}
