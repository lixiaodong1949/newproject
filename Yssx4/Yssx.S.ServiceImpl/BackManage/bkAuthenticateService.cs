﻿using System;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 认证
    /// </summary>
    public class bkAuthenticateService : IbkAuthenticateService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bkUserTicket>> bkUserAuthentication(UserbkAuthentication dto,string loginIp, PermissionRequirement _requirement)
        {
            return await Task.Run(() =>
            {
                var md5 = dto.Password.Md5();
                var user = DbContext.FreeSql.Select<YssxbkUser>().Where(u => u.UserName == dto.UserName && u.Password == md5 ).First();
                var ticket = user == null ? null : new bkUserTicket() { Name = user.UserName, Id = user.Id, Type = (int)user.UserType, LoginType = 0 };
                if (ticket == null) return new ResponseContext<bkUserTicket> { Code = CommonConstants.ErrorCode, Msg = "登陆失败，账号或密码错误！" };
                if (user.IsDelete == 1) return new ResponseContext<bkUserTicket> { Code = CommonConstants.IsDelete, Msg = "账号停用" };
                //var requirement = ServiceLocator.GetService<PermissionRequirement>();
                ticket = JwtTokenHelper.BuildJwtToken(ticket, _requirement);
                var log = new YssxUserLoginLog
                {
                    AccessToken = ticket.AccessToken,
                    Id = IdWorker.NextId(),
                    LoginIp = loginIp,
                    LoginTime = DateTime.Now,
                    UserId = ticket.Id
                };
                //RedisHelper.Set(string.Format(CacheKeys.UserLoginTokenHashKey, ticket.Id), ticket.AccessToken, 24 * 60 * 60);
                RedisHelper.HSet(CacheKeys.UserLoginTokenHashKey, $"{ ticket.Id}-{ticket.LoginType}", ticket.AccessToken);
                DbContext.FreeSql.Insert<YssxUserLoginLog>(log).ExecuteAffrows();
              
                return new ResponseContext<bkUserTicket> { Code = CommonConstants.SuccessCode, Data = ticket };
            });
        }
    }
}
