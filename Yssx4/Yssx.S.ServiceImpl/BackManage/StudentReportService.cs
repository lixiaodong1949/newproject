﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.M.IServices;
using Yssx.S.Dto.Report;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 成绩统计相关
    /// </summary>
    public class StudentReportService : IStudentReportService
    {
        public Task<ResponseContext<List<StudentErrorQuestionDto>>> GetErrorQuestionList(long examId)
        {
            throw new System.NotImplementedException();
        }

        public Task<PageResponse<StudentGradeInfoDto>> GetStudentGradeList(StudentGradeListRequest model)
        {
            throw new System.NotImplementedException();
        }

        public Task<ResponseContext<StudentGradeInfoDto>> GetStudentLastGrade(long examId)
        {
            throw new System.NotImplementedException();
        }

        public Task<PageResponse<StudentTeamGradeInfoDto>> GetTeamGradeList(StudentGradeListRequest model)
        {
            throw new System.NotImplementedException();
        }

        public Task<ResponseContext<StudentTeamGradeInfoDto>> GetTeamLastGrade(long examId)
        {
            throw new System.NotImplementedException();
        }
    }

}
