﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.M.IServices;
using Yssx.S.Dto;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 教师
    /// </summary>
    public class TeacherService :ITeacherService
    {
        #region 基础操作
        /// <summary>
        /// 添加 更新 教师
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateTeacher(TeacherDto model,long Id)
        {
            long opreationId = Id;//操作人ID
            DateTime dtNow = DateTime.Now;

            if (model.Id.HasValue)
            {
                return await Task.Run(() =>
                {
                    return UpdateTeacher(model,Id);
                });
            }
            #region 验证
            if (model.TenantId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学校不能为空" };
            if (model.CollegeId == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "院系不能为空" };

            if (String.IsNullOrEmpty(model.RealName))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "真实姓名不能为空" };
            if (String.IsNullOrEmpty(model.Email))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "邮箱不能为空" };
            if (String.IsNullOrEmpty(model.MobilePhone))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码不能为空" };
            if (String.IsNullOrEmpty(model.IdNumber))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "身份证号码不能为空" };
            //if (String.IsNullOrEmpty(model.TeacherNo))
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "工号不能为空" };

            if (!model.Id.HasValue || model.Id == 0)
            {
                if (DbContext.FreeSql.Select<YssxUser>().Any(m => m.MobilePhone == model.MobilePhone && m.IsDelete == CommonConstants.IsNotDelete))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码重复" };
                //if (DbContext.FreeSql.Select<YssxUser>().Any(m => m.IdNumber == model.IdNumber && m.IsDelete == CommonConstants.IsNotDelete))
                //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "身份证号码重复" };
                //if (DbContext.FreeSql.Select<YssxStudent>().Any(m => m.StudentNo == model.TeacherNo && m.IsDelete == CommonConstants.IsNotDelete))
                //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "工号重复" };
            }
            var schoolExist = DbContext.FreeSql.Select<YssxSchool>().Any(a => a.Id == model.TenantId);
            if (!schoolExist)
                return new ResponseContext<bool>(CommonConstants.NotFund, "不存在的学校信息", false);

            #endregion

            var user = new YssxUser
            {
                Id = IdWorker.NextId(),
                MobilePhone = model.MobilePhone,
                IdNumber = model.IdNumber,
                RealName = model.RealName,
                UserName = model.UserName,
                Password = CommonConstants.NewUserDefaultPwd.Md5(),
                TenantId = model.TenantId,
                Status = (int)model.Status,
                UserType = model.Type == TeacherType.Teacher ? (int)UserTypeEnums.Teacher : (int)UserTypeEnums.Educational,
                Email = model.Email,
                ClientId = Guid.NewGuid(),
                CreateBy = opreationId,
                CreateTime = dtNow
            };
            var edu = new YssxTeacher
            {
                TenantId = model.TenantId,
                Titles = model.Titles,
                UserId = user.Id,
                Name = model.UserName,
                TeacherNo = model.TeacherNo,
                Id = IdWorker.NextId(),
                Type = model.Type,
                CreateBy = opreationId,
                CreateTime = dtNow
            };
            return await Task.Run(() =>
            {
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Insert<YssxUser>().AppendData(user).ExecuteAffrows();
                    DbContext.FreeSql.Insert<YssxTeacher>().AppendData(edu).ExecuteAffrows();
                    tran.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 修改教师
        /// </summary>
        /// <returns></returns>
        private ResponseContext<bool> UpdateTeacher(TeacherDto model, long Id)
        {
            long opreationId = Id;//操作人ID
            var dtNow = DateTime.Now;

            var teacher = DbContext.FreeSql.Select<YssxTeacher>().Where(m => m.Id == model.Id && m.TenantId == model.TenantId).First();
            if (teacher == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };

            var user = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == teacher.UserId).First();
            if (user == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "教师基本信息异常!" };

            if (DbContext.FreeSql.Select<YssxUser>().Any(m => m.IdNumber == model.IdNumber && m.Id != teacher.UserId && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "手机号码重复" };

            teacher.UpdateTime = dtNow;
            teacher.UpdateBy = opreationId;
            teacher.Titles = model.Titles;
            teacher.Type = model.Type;
            teacher.Name = model.UserName;
            teacher.TeacherNo = model.TeacherNo;

            user.UpdateBy = opreationId;
            user.UpdateTime = dtNow;
            user.MobilePhone = model.MobilePhone;
            user.IdNumber = model.IdNumber;
            user.RealName = model.RealName;
            user.UserName = model.UserName;
            user.Email = model.Email;

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<YssxTeacher>().SetSource(teacher).UpdateColumns(m => new { m.UpdateBy, m.UpdateTime, m.Titles, m.Type, m.Name, m.TeacherNo }).ExecuteAffrows();
                DbContext.FreeSql.Update<YssxUser>().SetSource(user).UpdateColumns(m => new { m.UpdateBy, m.UpdateTime, m.MobilePhone, m.IdNumber, m.RealName, m.UserName, m.Email }).ExecuteAffrows();
                tran.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 删除老师
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTeacher(TeacherDto model, long Id)
        {
            long opreationId = Id;//操作人ID

            if (!model.Id.HasValue)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };
            var teacher = DbContext.FreeSql.Select<YssxTeacher>().Where(m => m.Id == model.Id).First();
            if (teacher == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };

            teacher.IsDelete = CommonConstants.IsDelete;
            teacher.UpdateBy = opreationId;
            teacher.UpdateTime = DateTime.Now;
            var user = new YssxUser
            {
                Id = teacher.UserId,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = opreationId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<YssxUser>().SetSource(user).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    DbContext.FreeSql.Update<YssxTeacher>().SetSource(teacher).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 获取教师列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<TeacherDto>>> GetTeachers(TeacherRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxTeacher>()
                    .From<YssxSchool, YssxUser>((a, b, c) => a.InnerJoin(aa => aa.TenantId == b.Id).InnerJoin(aa => aa.UserId == c.Id))
                    .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete);
                if (model.TeacherType.HasValue)
                    select = select.Where((a, b, c) => a.Type == model.TeacherType);
                if (model.TenantId.HasValue)
                    select = select.Where((a, b, c) => a.TenantId == model.TenantId);
                if (!string.IsNullOrWhiteSpace(model.KeyWord))
                    select = select.Where((a, b, c) => c.RealName.Contains(model.KeyWord) || c.MobilePhone == model.KeyWord || a.TeacherNo == model.KeyWord);
                //if (model.CollegeId != null)
                //    select = select.Where((a, b, c, d) => d.Id == model.CollegeId);

                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.Id,a.Titles,a.Type,a.TenantId,b.Name as SchoolName,c.RealName,c.MobilePhone,c.UserName,c.IdNumber,c.Sex,c.Email,c.Status");
                var rTeacherData = DbContext.FreeSql.Ado.Query<TeacherDto>(sql);
                foreach (var item in rTeacherData)
                {
                    var rCollegeData = DbContext.FreeSql.Select<YssxTeacherClass>().From<YssxClass, YssxCollege>((a, b, c) => a.Where(s => s.ClassId == b.Id && b.CollegeId == c.Id))
                        .Where((a, b, c) => a.TeacherId == item.Id).GroupBy((a, b, c) => new { c.Id, c.Name })
                        .ToList(a => a.Key.Name);
                    if (rCollegeData.Count > 0)
                    {
                        item.CollegeName = string.Join("、", rCollegeData);
                    }
                }

                var res = new PageResponse<TeacherDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = rTeacherData };
                return new ResponseContext<PageResponse<TeacherDto>> { Code = CommonConstants.SuccessCode, Data = res };
            });
        }

        /// <summary>
        /// 获取指定教师信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TeacherDto>> GetTeacherById(long Id)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxTeacher>().InnerJoin<YssxUser>((a, b) => a.UserId == b.Id)
                    .Where(m => m.Id == Id && m.IsDelete == CommonConstants.IsNotDelete);
                var sql = select.Limit(1).ToSql("a.id,a.Titles,a.Type,b.RealName,b.UserName,b.IdNumber,b.Sex,b.Email");
                var item = DbContext.FreeSql.Ado.Query<TeacherDto>(sql).FirstOrDefault();

                return new ResponseContext<TeacherDto> { Code = CommonConstants.SuccessCode, Data = item };
            });
        }

        /// <summary>
        /// 导入老师
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<ImportTeacherResponse>>> ImportTeacher(List<IFormFile> form, long Id)
        {
            long opreationId = Id;//操作人ID
            DateTime dtNow = DateTime.Now;

            #region 取文导入件数据
            var file = form?.FirstOrDefault();
            if (file == null)
            {
                return new ResponseContext<List<ImportTeacherResponse>> { Code = CommonConstants.ErrorCode, Msg = "上传失败" };
            }
            var teachers = new List<ImportTeacher>();
            //using (var ms = new MemoryStream(file.File))
            //{
            //    teachers = NPOIHelper<ImportTeacher>.ImportExcelData(ms);
            //}
            #endregion

            if (teachers == null || !teachers.Any())
            {
                return new ResponseContext<List<ImportTeacherResponse>> { Code = CommonConstants.NotFund, Msg = "Excel中未存在任何有效数据" };
            }
            var rResData = new List<ImportTeacherResponse>();
            var sb = new StringBuilder();
            var loop = 0;
            var teacherList = new List<YssxTeacher>();
            var userList = new List<YssxUser>();
            //TODO加锁
            var errorLoop = 0;
            foreach (var item in teachers)
            {
                loop++;
                sb.Clear();
                long schoolId = 0;
                long collegelId = 0;
                #region 校验

                if (string.IsNullOrEmpty(item.SchoolName))
                    sb.Append("学校不能为空.");
                if (string.IsNullOrEmpty(item.CollegeName))
                    sb.Append("院系不能为空.");

                if (string.IsNullOrEmpty(item.Name))
                    sb.Append("姓名不能为空.");
                if (string.IsNullOrEmpty(item.Email))
                    sb.Append("邮箱不能为空.");
                if (string.IsNullOrEmpty(item.MobilePhone))
                    sb.Append("手机号不能为空.");
                if (string.IsNullOrEmpty(item.IdNumber))
                    sb.Append("身份证号不能为空.");

                if (!string.IsNullOrEmpty(item.SchoolName))
                {
                    schoolId = DbContext.FreeSql.Select<YssxSchool>().Where(a => a.Name == item.SchoolName || a.ShortName == item.SchoolName).First(a => a.Id);
                    if (schoolId == 0)
                    {
                        sb.Append($"系统中不存在【{item.SchoolName}】学校.");
                    }
                }
                if (!string.IsNullOrEmpty(item.CollegeName))
                {
                    collegelId = DbContext.FreeSql.Select<YssxCollege>().Where(a => a.Name == item.CollegeName).First(a => a.Id);
                    if (collegelId == 0)
                    {
                        sb.Append($"不存在此院系【{item.CollegeName}】.");
                    }
                }
                if (DbContext.FreeSql.Select<YssxUser>().Any(m => m.MobilePhone == item.MobilePhone && m.IsDelete == CommonConstants.IsNotDelete))
                    sb.Append($"【{item.SchoolName}】手机号码重复.");
                #region 
                //if (string.IsNullOrEmpty(item.IdNumber))
                //{
                //    //TODO身份证验证
                //    sb.Append("未填写身份证号.");
                //}
                //else
                //{
                //    if (!Regex.IsMatch(item.IdNumber, RegexConstants.IdCardNumberSimple))
                //    {
                //        sb.Append("无效的身份证号.");
                //    }
                //    else
                //    {
                //        //TODO身份证是否是全平台唯一
                //        //校验身份证号是否已存在
                //        //var existIdNumber = DbContext.FreeSql.Select<YssxUser>().Where(a => a.IdNumber == item.IdNumber).TenantFilter(schoolId).Any();
                //        //if (existIdNumber)
                //        //{
                //        //    sb.Append("身份证已存在.");
                //        //}
                //        if (userList.Any(a => a.IdNumber == item.IdNumber))
                //        {
                //            sb.Append("Excel中已存在与此相同的身份证号，此数据已忽略.");
                //        }
                //    }
                //}
                #endregion

                #endregion
                #region 添加有效数据
                if (!string.IsNullOrEmpty(sb.ToString()))
                {
                    rResData.Add(new ImportTeacherResponse()
                    {
                        RowIndex = loop,
                        CollegeName = item.CollegeName,
                        Name = item.Name,
                        IdNumber = item.IdNumber,
                        ErrorMsg = sb.ToString().TrimEnd(',')
                    });
                    errorLoop++;
                }
                else
                {
                    var pwd = CommonConstants.NewUserDefaultPwd.Md5();
                    var user = new YssxUser()
                    {
                        Id = IdWorker.NextId(),
                        TenantId = schoolId,
                        NikeName = item.Name,
                        RealName = item.Name,
                        UserName = item.Name,
                        Status = (int)Status.Enable,
                        Password = pwd,
                        UserType = (int)UserTypeEnums.Teacher,
                        IdNumber = item.IdNumber,
                        MobilePhone = item.MobilePhone,
                        Email = item.Email,
                        ClientId = Guid.NewGuid(),
                        CreateBy = opreationId,
                        CreateTime = dtNow
                    };
                    var entity = new YssxTeacher()
                    {
                        Id = IdWorker.NextId(),
                        TenantId = schoolId,
                        Name = item.Name,
                        UserId = user.Id,
                        Titles = item.Titles,
                        Type = TeacherType.Teacher,
                        CreateBy = opreationId,
                        CreateTime = dtNow
                    };
                    entity.TeacherNo = item.TeacherNo;

                    //var sex = item.Sex == Sex.Man.GetDescription() ? Sex.Man : (item.Sex == Sex.Woman.GetDescription() ? Sex.Woman : Sex.None);
                    user.Sex = item.Sex;
                    teacherList.Add(entity);
                    userList.Add(user);
                }
                #endregion
            }
            //入库
            return await Task.Run(() =>
            {
                if (teacherList.Any())
                {
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        uow.GetRepository<YssxTeacher>().Insert(teacherList);
                        uow.GetRepository<YssxUser>().Insert(userList);
                        uow.Commit();
                    }
                }
                var sbMsg = sb.ToString();
                var rResMsg = (string.IsNullOrEmpty(sbMsg) ? string.Empty : $"错误：{sbMsg}") + $"成功导入{teacherList.Count()}条,失败{teachers.Count() - teacherList.Count()}条,共{teachers.Count()}条数据.";
                return new ResponseContext<List<ImportTeacherResponse>> { Code = CommonConstants.SuccessCode, Data = rResData, Msg = rResMsg };
            });
        }

        /// <summary>
        /// 导出老师
        /// </summary>
        /// <returns></returns>      
        public async Task<IActionResult> ExportTeacher(TeacherType TeacherType, long TenantId, string KeyWord)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxTeacher>().From<YssxSchool, YssxUser>((a, b, c) => a.InnerJoin(aa => aa.TenantId == b.Id).InnerJoin(aa => aa.UserId == c.Id))
                    .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && a.Type == TeacherType);
                if (TenantId > 0)
                    select = select.Where((a, b, c) => a.TenantId == TenantId);
                if (!string.IsNullOrWhiteSpace(KeyWord))
                    select = select.Where((a, b, c) => c.RealName.Contains(KeyWord) || c.MobilePhone == KeyWord || a.TeacherNo == KeyWord);

                var sql = select.ToSql("a.Id,a.Titles,a.Type,c.RealName,c.UserName,c.IdNumber,c.Sex,c.Email,c.MobilePhone");
                var rTeacherData = DbContext.FreeSql.Ado.Query<TeacherDto>(sql);
                if (rTeacherData.Count == 0)
                    throw new Exception("无数据可导出");
                //循环取出老师绑定的院系
                foreach (var item in rTeacherData)
                {
                    var rCollegeData = DbContext.FreeSql.Select<YssxTeacherClass>().From<YssxClass, YssxCollege>((a, b, c) => a.Where(s => s.ClassId == b.Id && b.CollegeId == c.Id))
                        .Where((a, b, c) => a.TeacherId == item.Id).GroupBy((a, b, c) => new { c.Id, c.Name })
                        .ToList(a => a.Key.Name);
                    if (rCollegeData.Count > 0)
                    {
                        item.CollegeName = string.Join("、", rCollegeData);
                    }
                }
                var selectData = rTeacherData.Select(m => new ExportTeacherDto
                {
                    Titles = m.Titles,
                    RealName = m.RealName,
                    CollegeName = m.CollegeName,
                    Email = m.Email,
                    IdNumber = m.IdNumber,
                    MobilePhone = m.MobilePhone
                }).ToList();
                byte[] ms = NPOIHelper<ExportTeacherDto>.OutputExcel(selectData, new string[] { "111" });

                var provider = new FileExtensionContentTypeProvider();
                if (!provider.Mappings.TryGetValue(".xlsx", out var memi))
                {
                    throw new Exception("找不到可导出的文件类型");
                }
                //return new FileContentResult(ms, memi, Guid.NewGuid().ToString() + ".xlsx");
                return new FileContentResult(ms, memi);
            });
        }

        #endregion

        #region 其他操作
        /// <summary>
        /// 学生审核 (只需要传入Id、Type和Status 三个个参数)
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> TeacherAudit(TeacherDto model, long Id)
        {
            long opreationId = Id;//操作人ID
            long tenantId = 0;//操作人-学校ID

            if (!model.Id.HasValue)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "找不到老师信息!" };
            }
            var user = DbContext.FreeSql.Select<YssxUser>().InnerJoin<YssxTeacher>((a, b) => a.Id == b.UserId && b.Id == model.Id && a.TenantId == tenantId).First();
            if (user == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "找不到老师信息!" };
            user.UpdateTime = DateTime.Now;
            user.UpdateBy = opreationId;
            user.Status = (int)model.Status;
            return await Task.Run(() =>
            {
                var rst = DbContext.FreeSql.Update<YssxUser>().SetSource(user).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.Status }).ExecuteAffrows() > 0;
                if (rst)
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "审核失败!" };
            });
        }

        /// <summary>
        /// 获取全部教师列表
        /// </summary>
        public async Task<ResponseContext<PageResponse<TeacherDto>>> GetWhole(TeacherRequest model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxTeacher>()
                    .From<YssxSchool, YssxUser>((a, b, c) => a.InnerJoin(aa => aa.TenantId == b.Id).InnerJoin(aa => aa.UserId == c.Id))
                    .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete);
                if (model.TeacherType.HasValue)
                    select = select.Where((a, b, c) => a.Type == model.TeacherType);
                if (model.TenantId.HasValue)
                    select = select.Where((a, b, c) => a.TenantId == model.TenantId);
                if (!string.IsNullOrWhiteSpace(model.KeyWord) && model.TenantId != null)
                    select = select.Where((a, b, c) => c.RealName.Contains(model.KeyWord) || c.MobilePhone == model.KeyWord || a.TeacherNo == model.KeyWord);

                var totalCount = select.Count();
                var sql = select.ToSql("a.id,a.Titles,a.Type,a.TenantId as SchoolId,c.RealName,c.MobilePhone,c.UserName,c.IdNumber,c.Sex,c.Email,c.Status");
                var items = DbContext.FreeSql.Ado.Query<TeacherDto>(sql);
                var res = new PageResponse<TeacherDto> { PageIndex = 1, PageSize = Convert.ToInt32(totalCount), RecordCount = totalCount, Data = items };
                return new ResponseContext<PageResponse<TeacherDto>> { Code = CommonConstants.SuccessCode, Data = res };
            });
        }

        /// <summary>
        /// 修改指定教师 用户状态
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ChangeTeacherStatus(long Id, long Oid)
        {
            long opreationId = Oid;//操作人ID

            if (Id <= 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，教师ID为空" };
            var teacher = DbContext.FreeSql.Select<YssxTeacher>().Where(m => m.Id == Id).First();
            var teacherUid = teacher?.UserId ?? -1;
            var user = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == teacherUid).First();
            if (teacher == null || user == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息" };
            user.Status = user.Status == (int)Status.Enable ? (int)Status.Disable : (int)Status.Enable;
            user.UpdateBy = opreationId;
            user.UpdateTime = DateTime.Now;

            return await Task.Run(() =>
            {
                DbContext.FreeSql.Update<YssxUser>().SetSource(user).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.Status }).ExecuteAffrows();
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 绑定班级
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddClassToTeacher(TeacherClassRequest model, long Id)
        {
            long opreationId = Id;//操作人ID

            #region 数据验证
            if (model.ClassIds == null || !model.ClassIds.Any())
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择要绑定的班级" };
            //TODO-hy-绑定班级加锁
            var teacherAny = DbContext.FreeSql.Select<YssxTeacher>().Where(a => a.Id == model.TeacherID && a.TenantId == model.TenantId).Any();
            if (!teacherAny)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到该老师信息" };
            //筛选出有效班级
            var newClassIds = model.ClassIds.Distinct().ToArray();
            var validClassIds = DbContext.FreeSql.Select<YssxClass>().Where(a => newClassIds.Contains(a.Id) && a.TenantId == model.TenantId).ToList(a => a.Id);
            if (!validClassIds.Any())
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择有效的班级" };
            #endregion

            var totalCount = model.ClassIds.Count();
            var existClassIds = DbContext.FreeSql.Select<YssxTeacherClass>().Where(a => a.TeacherId == model.TeacherID && a.TenantId == model.TenantId).ToList(a => a.ClassId);
            var teacherClassList = new List<YssxTeacherClass>();
            var addClassIds = model.ClassIds.Where(a => !existClassIds.Contains(a)).ToList();
            addClassIds.ForEach(a =>
            {
                var teacher = new YssxTeacherClass(IdWorker.NextId(), model.TenantId, model.TeacherID, a);
                teacher.CreateBy = opreationId;
                teacherClassList.Add(teacher);
            });

            return await Task.Run(() =>
            {
                if (teacherClassList.Any())
                {
                    DbContext.FreeSql.GetRepository<YssxTeacherClass>().Insert(teacherClassList);
                }
                var rMsg = $"申请绑定{totalCount}个班级， 成功{teacherClassList.Count()}个,失败{totalCount - teacherClassList.Count()}个.";
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = rMsg };
            });
        }
        #endregion


    }
}
