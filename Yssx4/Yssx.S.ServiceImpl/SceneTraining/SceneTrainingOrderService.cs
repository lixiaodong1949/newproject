﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.Utils;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.SceneTraining;
using Yssx.S.IServices.SceneTraining;
using Yssx.S.Pocos;
using Yssx.S.Pocos.SceneTraining;

namespace Yssx.S.ServiceImpl.SceneTraining
{
    /// <summary>
    /// 场景实训订单服务
    /// </summary>
    public class SceneTrainingOrderService : ISceneTrainingOrderService
    {
        #region 获取购买场景实训列表
        /// <summary>
        /// 获取购买场景实训列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<YssxSceneTrainingViewModel>> GetSceneTrainingByBuy(YssxSceneTrainingQuery query, UserTicket user)
        {
            var result = new PageResponse<YssxSceneTrainingViewModel>();

            if (query == null)
                return result;

            List<YssxSceneTrainingViewModel> list = new List<YssxSceneTrainingViewModel>();

            var selectBuy = DbContext.FreeSql.Select<YssxSceneTrainingOrder>().From<YssxSceneTraining, YssxBusinessScene, YssxCase>
                ((a, b, c, d) => a.InnerJoin(x => x.SceneTrainingId == b.Id)
                .LeftJoin(x => b.YwcjId == c.Id)
                .LeftJoin(x => b.SelectCompanyId == d.Id))
                .Where((a, b, c, d) => a.CustomerId == user.Id && (a.OrderStatus == (int)OrderStatus.Presented || a.OrderStatus == (int)OrderStatus.PaymentSuccess));

            if (!string.IsNullOrWhiteSpace(query.Keyword))
                selectBuy.Where((a, b, c, d) => b.Name.Contains(query.Keyword) || d.Name.Contains(query.Keyword));
            if (query.BusinessSceneId.HasValue)
                selectBuy.Where((a, b, c, d) => b.YwcjId == query.BusinessSceneId.Value);

            long totalCount = await selectBuy.CountAsync();

            var sqlBuy = selectBuy.OrderByDescending((a, b, c, d) => a.UpdateTime)
              .Page(query.PageIndex, query.PageSize).ToSql("a.Id AS OrderId,a.SceneTrainingId,b.Name AS SceneTrainingName,d.Name AS CompanyName,c.Name AS BusinessSceneName" +
              ",b.UseNumber,b.UpdateTime");
            list = await DbContext.FreeSql.Ado.QueryAsync<YssxSceneTrainingViewModel>(sqlBuy);

            List<long> sceneTrainingIds = list.Select(x => x.SceneTrainingId).ToList();

            //所有场景实训列表流程详情
            var detailList = DbContext.FreeSql.Select<YssxProcessDetails>().Where(x => sceneTrainingIds.Contains(x.CjsxId) && x.IsDelete == CommonConstants.IsNotDelete).ToList();

            list.ForEach(x =>
            {
                //当前场景实训流程详情
                var presentDetails = detailList.Where(a => a.CjsxId == x.SceneTrainingId).ToList();
                if (presentDetails != null && presentDetails.Count > 0)
                {
                    //流程步骤
                    x.ProcessStepsCount = presentDetails.Count();
                    //题目数量
                    presentDetails.ForEach((a) =>
                    {
                        x.TopicCount += JsonHelper.DeserializeObject<List<Tjtm>>(a.TjtmId).Count;
                    });
                }
            });

            result.Data = list;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

    }
}
