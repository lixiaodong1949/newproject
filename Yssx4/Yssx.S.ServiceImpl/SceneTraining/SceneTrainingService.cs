﻿using Newtonsoft.Json;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Tas.Common.Utils;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.Dto.SceneTraining;
using Yssx.S.IServices;
using Yssx.S.IServices.SceneTraining;
using Yssx.S.Pocos;
using Yssx.S.Pocos.SceneTraining;

namespace Yssx.S.ServiceImpl.SceneTraining
{
    /// <summary>
    /// 场景实训
    /// </summary>
    public class SceneTrainingService : ISceneTrainingService
    {
        /// <summary>
        /// 上传场景实训
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddTrainingService(AddTrainingServiceDto dto, long userId)
        {
            try
            {
                var lst = new List<YssxProcessDetails>();
                var addlst = new List<YssxProcessDetails>();
                var user = DbContext.FreeSql.Select<YssxUser>().Where(x => x.Id == userId).First();
                if (dto.Id.HasValue)
                {
                    if (dto.Type == 0)
                    {
                        DbContext.FreeSql.Update<YssxSceneTraining>().Set(x => new YssxSceneTraining
                        {
                            Name = dto.Name,
                            Pxxh = dto.Pxxh,
                            SelectCompanyId = dto.SelectCompanyId,
                            YwcjId = dto.Ywcj,
                            Ywwl = dto.Ywwl,
                            Zzjy = dto.Zzjy,
                            UpdateTime = DateTime.Now,
                            Uppershelf = 0,
                            AuditStatus = 0
                        }).Where(x => x.Id == dto.Id).ExecuteAffrows();
                    }
                    else
                    {
                        DbContext.FreeSql.Update<YssxSceneTraining>().Set(x => new YssxSceneTraining
                        {
                            Name = dto.Name,
                            Pxxh = dto.Pxxh,
                            SelectCompanyId = dto.SelectCompanyId,
                            YwcjId = dto.Ywcj,
                            Ywwl = dto.Ywwl,
                            Zzjy = dto.Zzjy,
                            UpdateTime = DateTime.Now
                        }).Where(x => x.Id == dto.Id).ExecuteAffrows();
                    }

                    //dto.ProcessDetailsList.ForEach(x =>

                    //x.LcxqId.HasValue || x.LcxqId != 0 {
                    //    lst.Add(new YssxProcessDetails
                    //    {
                    //        Id = IdWorker.NextId(),
                    //        Jnsx = x.Jnsx,
                    //        CjsxId = (long)dto.Id,
                    //        Name = x.Name,
                    //        //Sczl = string.Join(",",x.Sczl.),
                    //        Sczl = JsonConvert.SerializeObject(x.Sczl),
                    //        TjtmId = JsonConvert.SerializeObject(x.TjtmId),
                    //        Xzbm = x.Xzbm,
                    //        Xzry = x.Xzry,
                    //        Zltp = x.Zltp,
                    //        Lcxh = x.Lcxh,
                    //        IsDelete = x.IsDelete
                    //    })}
                    //);
                    foreach (var item in dto.ProcessDetailsList)
                    {
                        if (item.Id.HasValue && item.Id != 0)
                        {
                            lst.Add(new YssxProcessDetails
                            {
                                Id = (long)item.Id,
                                Jnsx = item.Jnsx,
                                CjsxId = (long)dto.Id,
                                Name = item.Name,
                                Sczl = JsonConvert.SerializeObject(item.Sczl),
                                TjtmId = JsonConvert.SerializeObject(item.TjtmId),
                                Xzbm = item.Xzbm,
                                Xzry = item.Xzry,
                                Zltp = item.Zltp,
                                Lcxh = item.Lcxh,
                                IsDelete = item.IsDelete
                            });
                        }
                        else
                        {
                            addlst.Add(new YssxProcessDetails
                            {
                                Id = IdWorker.NextId(),
                                Jnsx = item.Jnsx,
                                CjsxId = (long)dto.Id,
                                Name = item.Name,
                                Sczl = JsonConvert.SerializeObject(item.Sczl),
                                TjtmId = JsonConvert.SerializeObject(item.TjtmId),
                                Xzbm = item.Xzbm,
                                Xzry = item.Xzry,
                                Zltp = item.Zltp,
                                Lcxh = item.Lcxh,
                                IsDelete = item.IsDelete
                            });
                        }
                    }

                    dto.DeleteIdList.ForEach(x =>
                        DbContext.FreeSql.Update<YssxProcessDetails>().Set(z => z.IsDelete == CommonConstants.IsDelete).Where(z => z.Id == x).ExecuteAffrows()
                    );
                    //DbContext.FreeSql.Update<YssxProcessDetails>().Set(x => new YssxProcessDetails { IsDelete = CommonConstants.IsDelete }).Where(x => x.CjsxId == dto.Id).ExecuteAffrows();
                    DbContext.FreeSql.Insert<YssxProcessDetails>().AppendData(addlst).ExecuteAffrows();
                    DbContext.FreeSql.Update<YssxProcessDetails>().SetSource(lst).ExecuteAffrows();
                }
                else
                {
                    YssxSceneTraining st = new YssxSceneTraining()
                    {
                        Id = IdWorker.NextId(),
                        CreateBy = userId,
                        CreateTime = DateTime.Now,
                        Name = dto.Name,
                        Pxxh = dto.Pxxh,
                        SelectCompanyId = dto.SelectCompanyId,
                        YwcjId = dto.Ywcj,
                        Ywwl = dto.Ywwl,
                        Zzjy = dto.Zzjy,
                        LyId = user.UserType == 6 ? 1 : 2
                    };

                    dto.ProcessDetailsList.ForEach(x => lst.Add(new YssxProcessDetails
                    {
                        Id = IdWorker.NextId(),
                        Jnsx = x.Jnsx,
                        CjsxId = st.Id,
                        Name = x.Name,
                        //Sczl = string.Join(",",x.Sczl.),
                        Sczl = JsonConvert.SerializeObject(x.Sczl),
                        TjtmId = JsonConvert.SerializeObject(x.TjtmId),
                        Xzbm = x.Xzbm,
                        Xzry = x.Xzry,
                        Zltp = x.Zltp,
                        Lcxh = x.Lcxh
                    }));
                    int i = DbContext.FreeSql.Insert(st).ExecuteAffrows();
                    DbContext.FreeSql.Insert<YssxProcessDetails>().AppendData(lst).ExecuteAffrows();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };

            }
            catch (Exception e)
            {

                throw;
            }
        }

        /// <summary>
        /// 下拉数据
        /// </summary>
        /// <param name="id">id(公司下拉传0，部门下拉传公司id,人员下来传部门id)</param>
        /// <param name="typeId">1,公司,2,部门,3,人员,4,场景业务</param>
        /// <returns></returns>
        public async Task<ResponseContext<List<DropDownDataDto>>> DropDownData(long id, int typeId)
        {
            return await Task.Run(() =>
            {
                List<DropDownDataDto> list = new List<DropDownDataDto>();
                switch (typeId)
                {
                    case 1:
                        list = DbContext.FreeSql.Select<YssxCase>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).Select(x => new DropDownDataDto { Id = x.Id, Name = x.Name }).ToList();
                        break;
                    case 2:
                        list = DbContext.FreeSql.Select<YssxDepartment>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).Where(x => x.CaseId == id).Select(x => new DropDownDataDto { Id = x.Id, Name = x.Name, FileUrl = x.FileUrl }).ToList();
                        break;
                    case 3:
                        list = DbContext.FreeSql.Select<YssxPostPersonnel>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).Where(x => x.DepartmentId == id).Select(x => new DropDownDataDto { Id = x.Id, Name = x.Persons }).ToList();
                        break;
                    case 4:
                        list = DbContext.FreeSql.Select<YssxBusinessScene>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).Select(x => new DropDownDataDto { Id = x.Id, Name = x.Name }).ToList();
                        //    list = DbContext.FreeSql.Select<YssxClass>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).Where(x => x.ProfessionId == id).Select(x => new DropdownListDto { Id = x.Id, Name = x.Name }).ToList();
                        break;
                    default:
                        break;
                }
                return new ResponseContext<List<DropDownDataDto>> { Code = CommonConstants.SuccessCode, Msg = "获取成功!", Data = list };
            });
        }

        private async Task<PageResponse<YssxSceneTrainingDto>> TrainingServiceList2(TrainingServicePageRequest dto, UserTicket user)
        {
            var select = DbContext.FreeSql.Select<YssxSceneTraining>().From<YssxBusinessScene, YssxCase>((sc, bu, ca) => sc.LeftJoin(aa => aa.YwcjId == bu.Id)
                                                                                                                              .LeftJoin(aa => aa.SelectCompanyId == ca.Id))
                      .Where((sc, bu, ca) => sc.IsDelete == CommonConstants.IsNotDelete).OrderBy((sc, bu, ca) => sc.Pxxh);
            if (user.Type != (int)UserTypeEnums.Professional)
            {
                select.Where((sc, bu, ca) => sc.AuditStatus == 1);
            }

            long totalCount = 0;
            var items = new List<YssxSceneTrainingDto>();
            //YssxProcessDetails
            if (!string.IsNullOrEmpty(dto.Name))
                select.Where((sc, bu, ca) => sc.Name.Contains(dto.Name));
            if (dto.YwcjId.HasValue && dto.YwcjId != 0)
                select.Where((sc, bu, ca) => sc.YwcjId == dto.YwcjId);
            switch (dto.Status)
            {
                case 0: select.Where((sc, bu, ca) => sc.Uppershelf == dto.Status); break;
                case 1: select.Where((sc, bu, ca) => sc.Uppershelf == 1 && sc.AuditStatus == 0); break;
                case 2: select.Where((sc, bu, ca) => sc.AuditStatus == 1); break;
                case 3: select.Where((sc, bu, ca) => sc.AuditStatus == 2); break;
                default:
                    break;
            }

            if (dto.Type == 2)
            {
                if (dto.SelectCompanyId.HasValue && dto.SelectCompanyId != 0)
                    select.Where((sc, bu, ca) => sc.SelectCompanyId == dto.SelectCompanyId);
            }
            if (dto.Type == 1)
            {
                if (dto.LyId.HasValue && dto.LyId != 3)
                    select.Where((sc, bu, ca) => sc.LyId == dto.LyId);
            }

            totalCount = await select.CountAsync();
            var sql = select.OrderByDescending((sc, bu, ca) => sc.CreateTime)
              .Page(dto.PageIndex, dto.PageSize).ToSql("a.UpdateTime,a.UseNumber,a.Id,a.Name,a.SelectCompanyId,a.YwcjId,a.Pxxh,a.Ywwl,a.Zzjy,a.Describe,a.AuditStatus,a.AuditStatusExplain,a.Uppershelf,a.UppershelfExplain,bu.Name YwcjName,ca.Name SelectCompanyName");
            items = await DbContext.FreeSql.Ado.QueryAsync<YssxSceneTrainingDto>(sql);

            List<YssxProgress> pros = await DbContext.FreeSql.Select<YssxProgress>().Where(z => z.CreateBy == user.Id && z.TrainType != TrainType.End).OrderByDescending(z => z.UpdateTime).ToListAsync();

           // List<YssxProcessDetails> details = await DbContext.FreeSql.Select<YssxProcessDetails>().From<YssxProgress>((a, b) => a.LeftJoin(x => x.Id == b.LcxqId)).Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.CreateBy == user.Id && b.TrainType != TrainType.End).ToListAsync();

            items.ForEach(x =>
            {
                x.NikeName = System.Web.HttpUtility.UrlDecode(x.NikeName, Encoding.UTF8);

                if (x.AuditStatus == 0 && x.Uppershelf == 1)
                {
                    x.Status = 1;
                }
                else if (x.AuditStatus == 2)
                {
                    x.Status = 3;
                }
                else if (x.AuditStatus == 1)
                {
                    x.Status = 2;
                }
                else if (x.Uppershelf == 0)
                {
                    x.Status = 0;
                }

                YssxProgress e = pros.FirstOrDefault(s => s.CjsxId == x.Id);
                if (e != null)
                {
                    x.Dqjd = 1;
                    x.YssxProgressId = e.Id;
                }

                //YssxProcessDetails de = details.FirstOrDefault(s => s.CjsxId == x.Id);
                //if (de != null)
                //{
                //    x.Dqbs = (int)de.Lcxh;
                //}
                //x.Zgbs = details.Where(s => s.CjsxId == x.Id).Count();
            });
            return new PageResponse<YssxSceneTrainingDto> { Code = CommonConstants.SuccessCode, Data = items, Msg = "获取成功!", PageIndex = dto.PageIndex, PageSize = dto.PageSize, RecordCount = totalCount };
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="dto">搜索</param>
        /// <returns></returns>
        public async Task<PageResponse<YssxSceneTrainingDto>> TrainingServiceList(TrainingServicePageRequest dto, UserTicket user)
        {
            return await TrainingServiceList2(dto, user);
            //try
            //{
            //    long userId = user.Id;
            //    long totalCount = 0;
            //    var items = new List<YssxSceneTrainingDto>();
            //    //用户列表
            //    if (dto.Type == 3)
            //    {
            //        var select = DbContext.FreeSql.Select<YssxSceneTrainingOrder>().From<YssxSceneTraining, YssxBusinessScene, YssxCase>((or, sc, bu, ca) => or
            //                                                                                                                .LeftJoin(aa => aa.SceneTrainingId == sc.Id)
            //                                                                                                                .LeftJoin(aa => sc.YwcjId == bu.Id)
            //                                                                                                                .LeftJoin(aa => sc.SelectCompanyId == ca.Id))
            //       .Where((or, sc, bu, ca) => (or.OrderStatus == 0 || or.OrderStatus == 4) && or.CustomerId == userId);//订单状态 0:系统创建(赠送场景实训订单) 4:支付成功订单(已支付)
            //        if (!string.IsNullOrEmpty(dto.Name))
            //            select.Where((or, sc, bu, ca) => sc.Name.Contains(dto.Name));
            //        if (dto.SelectCompanyId.HasValue && dto.SelectCompanyId != 0)
            //            select.Where((or, sc, bu, ca) => sc.SelectCompanyId == dto.SelectCompanyId);
            //        if (dto.YwcjId.HasValue && dto.YwcjId != 0)
            //            select.Where((or, sc, bu, ca) => sc.YwcjId == dto.YwcjId);
            //        //if(dto.Tgxz==1)
            //        //    select.Where((or, sc, bu, ca) => pro.TrainType == TrainType.End);
            //        //if (dto.Tgxz == 2)
            //        //    select.Where((or, sc, bu, ca) => pro.TrainType == TrainType.Wait|| pro.TrainType == TrainType.Started);
            //        totalCount = await select.CountAsync();
            //        var sql = select.OrderByDescending((or, sc, bu, ca) => or.CreateTime)
            //          .Page(dto.PageIndex, dto.PageSize).ToSql("a.PaymentTime,a.CreateTime,a.CreateByName NikeName,a.Id SceneTrainingOrderId,sc.Id Id,sc.Name,sc.SelectCompanyId SelectCompanyId,bu.Name YwcjName,ca.Name SelectCompanyName");
            //        items = await DbContext.FreeSql.Ado.QueryAsync<YssxSceneTrainingDto>(sql);
            //        items.ForEach(async x =>
            //        {
            //            x.Tgcs = await DbContext.FreeSql.Select<YssxProgress>().Where(z => z.CjsxId == x.Id && z.CreateBy == userId && z.TrainType == TrainType.End).CountAsync();
            //            var pro = DbContext.FreeSql.Select<YssxProgress>().Where(z => z.CjsxId == x.Id && z.CreateBy == userId).OrderByDescending(z => z.UpdateTime).First();
            //            //var pr = DbContext.FreeSql.Select<YssxProgress>().Where(z => z.CjsxId == x.Id && z.CreateBy == userId && z.TrainType == TrainType.Started).OrderByDescending(z => z.CreateTime).ToSql();
            //            if (pro == null || pro.TrainType == TrainType.End)
            //            {
            //                x.Dqjd = 0;
            //                x.YssxProgressId = 0;
            //            }
            //            else
            //            {
            //                var de = DbContext.FreeSql.Select<YssxProcessDetails>().Where(z => z.Id == pro.LcxqId && z.IsDelete == CommonConstants.IsNotDelete).First();
            //                x.YssxProgressId = pro.Id;
            //                x.Dqjd = 1;
            //                x.Dqbs = (int)de.Lcxh;
            //                x.Zgbs = (int)DbContext.FreeSql.Select<YssxProcessDetails>().Where(z => z.CjsxId == pro.CjsxId && z.IsDelete == CommonConstants.IsNotDelete).Count();
            //            }

            //        });
            //    }
            //    else
            //    {
            //        var select = DbContext.FreeSql.Select<YssxSceneTraining>().From<YssxBusinessScene, YssxCase, YssxUser>((sc, bu, ca, us) => sc.LeftJoin(aa => aa.YwcjId == bu.Id)
            //                                                                                                                    .LeftJoin(aa => aa.SelectCompanyId == ca.Id)
            //                                                                                                                     .LeftJoin(aa => aa.CreateBy == us.Id))
            //            .Where((sc, bu, ca, us) => sc.IsDelete == CommonConstants.IsNotDelete).OrderBy((sc, bu, ca, us) => sc.Pxxh);
            //        if (!string.IsNullOrEmpty(dto.Name))
            //            select.Where((sc, bu, ca, us) => sc.Name.Contains(dto.Name));
            //        if (dto.YwcjId.HasValue && dto.YwcjId != 0)
            //            select.Where((sc, bu, ca, us) => sc.YwcjId == dto.YwcjId);
            //        switch (dto.Status)
            //        {
            //            case 0: select.Where((sc, bu, ca, us) => sc.Uppershelf == dto.Status); break;
            //            case 1: select.Where((sc, bu, ca, us) => sc.Uppershelf == 1 && sc.AuditStatus == 0); break;
            //            case 2: select.Where((sc, bu, ca, us) => sc.AuditStatus == 1); break;
            //            case 3: select.Where((sc, bu, ca, us) => sc.AuditStatus == 2); break;
            //            default:
            //                break;
            //        }

            //        if (dto.Type == 2)
            //        {
            //            if (dto.SelectCompanyId.HasValue && dto.SelectCompanyId != 0)
            //                select.Where((sc, bu, ca, us) => sc.SelectCompanyId == dto.SelectCompanyId);
            //        }
            //        if (dto.Type == 1)
            //        {
            //            if (dto.LyId.HasValue && dto.LyId != 3)
            //                select.Where((sc, bu, ca, us) => sc.LyId == dto.LyId);
            //        }

            //        totalCount = await select.CountAsync();
            //        var sql = select.OrderByDescending((sc, bu, ca, us) => sc.CreateTime)
            //          .Page(dto.PageIndex, dto.PageSize).ToSql("a.UpdateTime,a.UseNumber,a.Id,a.Name,a.SelectCompanyId,a.YwcjId,a.Pxxh,a.Ywwl,a.Zzjy,a.Describe,a.AuditStatus,a.AuditStatusExplain,a.Uppershelf,a.UppershelfExplain,bu.Name YwcjName,ca.Name SelectCompanyName,us.NikeName NikeName");
            //        items = await DbContext.FreeSql.Ado.QueryAsync<YssxSceneTrainingDto>(sql);

            //        items.ForEach(x =>
            //        {
            //            x.NikeName = System.Web.HttpUtility.UrlDecode(x.NikeName, Encoding.UTF8);
            //            if (x.AuditStatus == 0 && x.Uppershelf == 1)
            //            {
            //                x.Status = 1;
            //            }
            //            else if (x.AuditStatus == 2)
            //            {
            //                x.Status = 3;
            //            }
            //            else if (x.AuditStatus == 1)
            //            {
            //                x.Status = 2;
            //            }
            //            else if (x.Uppershelf == 0)
            //            {
            //                x.Status = 0;
            //            }
            //        });
            //    }

            //    return new PageResponse<YssxSceneTrainingDto> { Code = CommonConstants.SuccessCode, Data = items, Msg = "获取成功!", PageIndex = dto.PageIndex, PageSize = dto.PageSize, RecordCount = totalCount };

            //}
            //catch (Exception e)
            //{

            //    throw;
            //}
        }

        /// <summary>
        /// 描述
        /// </summary>
        /// <param name="describe">描述</param>
        /// <param name="id">场景实训id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Describe(string describe, long id)
        {
            return await Task.Run(() =>
            {
                bool state = false;
                var sc = DbContext.FreeSql.Select<YssxSceneTraining>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (sc == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "信息不存在!" };
                state = DbContext.FreeSql.Update<YssxSceneTraining>(sc).Set(x => x.Describe, describe).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "操作成功!" : "操作失败!" };
            });
        }

        /// <summary>
        /// 实训详情
        /// </summary>
        /// <param name="id">场景实训id</param>
        /// <returns></returns>
        public async Task<ResponseContext<TrainingServiceDetailsDto>> TrainingServiceDetails(long id)
        {
            try
            {
                var select = DbContext.FreeSql.Select<YssxSceneTraining>().From<YssxBusinessScene, YssxCase>((sc, bu, ca) => sc.LeftJoin(aa => aa.YwcjId == bu.Id)
                                                                                                                           .LeftJoin(aa => aa.SelectCompanyId == ca.Id))
                    .Where((sc, bu, ca) => sc.Id == id);

                var sql = select.ToSql("a.Id,a.Name,a.SelectCompanyId,a.YwcjId,a.Pxxh,a.Ywwl,a.Zzjy,a.Describe,a.AuditStatus,a.AuditStatusExplain,a.Uppershelf,a.UppershelfExplain,bu.Name YwcjName,ca.Name SelectCompanyName");
                var items = DbContext.FreeSql.Ado.Query<TrainingServiceDetailsDto>(sql);
                //items.ProcessDetails = new List<ProcessDetails>();

                var processdetailsselect = DbContext.FreeSql.Select<YssxProcessDetails>().From<YssxDepartment, YssxPostPersonnel>((pro, dep, pos) => pro.LeftJoin(aa => aa.Xzbm == dep.Id)
                                                                                                                             .LeftJoin(aa => aa.Xzry == pos.Id))
                    .Where((pro, dep, pos) => pro.CjsxId == id && pro.IsDelete == CommonConstants.IsNotDelete);
                sql = processdetailsselect.OrderBy((sc, bu, ca) => sc.Lcxh).ToSql("a.Id,a.Lcxh,a.Name,a.Xzbm,a.Xzry,a.Sczl Sczls,a.Zltp,a.TjtmId TjtmIds,a.Jnsx,a.CjsxId,dep.Name XzbmName,pos.Persons XzryName,pos.PostName PostName");
                List<ProcessDetails> processdetailsList = await DbContext.FreeSql.Ado.QueryAsync<ProcessDetails>(sql);
                processdetailsList.ForEach((x) =>
                                                {
                                                    x.Sczl = JsonConvert.DeserializeObject<List<Sczl>>(x.Sczls);
                                                    x.TjtmId = JsonHelper.DeserializeObject<List<Tjtm>>(x.TjtmIds);
                                                }

                                                );
                items[0].ProcessDetailsList = processdetailsList;

                return new ResponseContext<TrainingServiceDetailsDto> { Data = items[0], Msg = "操作成功!", Code = CommonConstants.SuccessCode };
            }
            catch (Exception e)
            {

                throw;
            }

        }

        /// <summary>
        /// 预览
        /// </summary>
        /// <param name="id">场景实训id</param>
        /// <returns></returns>
        public async Task<ResponseContext<TrainingServicePreviewDto>> TrainingServicePreview(long id)
        {
            var select = DbContext.FreeSql.Select<YssxSceneTraining>().From<YssxBusinessScene, YssxCase>((sc, bu, ca) => sc.LeftJoin(aa => aa.YwcjId == bu.Id)
                                                                                                                           .LeftJoin(aa => aa.SelectCompanyId == ca.Id))
                    .Where((sc, bu, ca) => sc.Id == id);
            var sql = select.ToSql("a.Ywwl,a.Read,a.Fabulous,a.UseNumber,bu.Name Ywcj,ca.EnterpriseInfo EnterpriseInfo,ca.Id CaseId");
            var items = DbContext.FreeSql.Ado.Query<TrainingServicePreviewDto>(sql);
            //业务流程
            items[0].ProcessDetailsList = DbContext.FreeSql.Select<YssxProcessDetails>().Where(x => x.CjsxId == id && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            List<Bmry> bmry = new List<Bmry>();

            foreach (var proce in items[0].ProcessDetailsList)
            {
                var pro = DbContext.FreeSql.Select<YssxDepartment>().Where(x => x.Id == proce.Xzbm).First();
                var post = DbContext.FreeSql.Select<YssxPostPersonnel>().Where(x => x.Id == proce.Xzry).First();

                if (pro != null)
                {
                    Bmry bm = new Bmry()
                    {
                        Bmmc = pro?.Name,
                        Id = pro.Id
                    };
                    Ry ry = new Ry()
                    {
                        PostName = post.PostName,
                        Rymc = post.Persons
                    };
                    if (bmry.Count(x => x.Id == pro.Id) > 0)
                    {
                        bmry.Find(x => x.Id == pro.Id).Ry.Add(ry);
                    }
                    else
                    {
                        bm.Ry.Add(ry);
                        bmry.Add(bm);
                    }
                }
            }
            items[0].BmryList = bmry;
            return new ResponseContext<TrainingServicePreviewDto> { Data = items[0], Msg = "获取成功!", Code = CommonConstants.SuccessCode };
        }

        /// <summary>
        /// 做题开始详情
        /// </summary>
        /// <param name="id">场景实训id</param>
        /// <returns></returns>
        public async Task<ResponseContext<ProblemDetailsDto>> ProblemDetails(long id, long progressId, long userId)
        {
            return await Task.Run(() =>
            {
                try
                {
                    var select = DbContext.FreeSql.Select<YssxSceneTraining>().From<YssxBusinessScene, YssxCase>((sc, bu, ca) => sc.LeftJoin(aa => aa.YwcjId == bu.Id)
                                                                                                                               .LeftJoin(aa => aa.SelectCompanyId == ca.Id))
                        .Where((sc, bu, ca) => sc.Id == id);

                    var sql = select.ToSql("a.Id,a.Name,bu.Name YwcjName,ca.Name SelectCompanyName,ca.BGFileUrl BGFileUrl,ca.Id SelectCompanyId");
                    var items = DbContext.FreeSql.Ado.Query<ProblemDetailsDto>(sql);

                    var processdetailsList = DbContext.FreeSql.Select<YssxProcessDetails>()
                        .Where(pro => pro.CjsxId == id && pro.IsDelete == CommonConstants.IsNotDelete).Select(x => new IdAndNameDetails { Id = x.Id, Lcxh = x.Lcxh, Name = x.Name }).ToList();
                    if (progressId == 0)
                    {
                        YssxProgress progress = new YssxProgress()
                        {
                            //Bs = 0,
                            CjsxId = id,
                            CreateBy = userId,
                            CreateTime = DateTime.Now,
                            Id = IdWorker.NextId(),
                            //LcxqId = 0,
                            //TmId = 0,
                            StartTime = DateTime.Now,
                            TrainType = TrainType.Wait
                        };
                        DbContext.FreeSql.Insert<YssxProgress>(progress).ExecuteAffrows();
                        items[0].ProgressId = progress.Id;
                    }
                    else
                    {
                        items[0].ProgressId = progressId;
                    }
                    items[0].ProcessDetailsList = processdetailsList;

                    return new ResponseContext<ProblemDetailsDto> { Data = items[0], Msg = "操作成功!", Code = CommonConstants.SuccessCode };
                }
                catch (Exception e)
                {

                    throw;
                }
            });
        }

        /// <summary>
        /// 场景实训流程详情 
        /// </summary>
        /// <param name="id">场景实训流程id</param>
        /// <returns></returns>
        public async Task<ResponseContext<ProcessDetails>> SceneTrainingDetails(long id)
        {
            var processdetailsselect = DbContext.FreeSql.Select<YssxProcessDetails>().From<YssxDepartment, YssxPostPersonnel>((pro, dep, pos) => pro.LeftJoin(aa => aa.Xzbm == dep.Id)
                                                                                                                         .LeftJoin(aa => aa.Xzry == pos.Id))
                .Where((pro, dep, pos) => pro.Id == id && pro.IsDelete == CommonConstants.IsNotDelete);
            var sql = processdetailsselect.OrderBy((sc, bu, ca) => sc.Lcxh).ToSql("a.Id,a.Lcxh,a.Name,a.Xzbm,a.Xzry,a.Sczl Sczls,a.Zltp,a.TjtmId TjtmIds,a.Jnsx,a.CjsxId,dep.Name XzbmName,pos.Persons XzryName,pos.PostName");
            List<ProcessDetails> processdetailsList = await DbContext.FreeSql.Ado.QueryAsync<ProcessDetails>(sql);
            processdetailsList.ForEach((x) =>
            {
                x.Sczl = JsonConvert.DeserializeObject<List<Sczl>>(x.Sczls);
                x.TjtmId = JsonHelper.DeserializeObject<List<Tjtm>>(x.TjtmIds);
            });
            //SceneTrainingDetailsDto item = new SceneTrainingDetailsDto()
            //{
            //    CjsxId = processdetailsList[0].CjsxId,
            //    LcId = processdetailsList[0].Id,
            //    Lcxh = processdetailsList[0].Lcxh,
            //    FirstStepData = new FirstStepData
            //    {
            //        Bs = 1,
            //        Name = processdetailsList[0].Name,
            //        PostName = processdetailsList[0].PostName,
            //        Xzbm = processdetailsList[0].Xzbm,
            //        XzbmName = processdetailsList[0].XzbmName,
            //        Xzry = processdetailsList[0].Xzry,
            //        XzryName = processdetailsList[0].XzryName,
            //        Zltp = processdetailsList[0].Zltp
            //    },
            //    StepTwoData = string.IsNullOrEmpty(processdetailsList[0].Sczls) && string.IsNullOrEmpty(processdetailsList[0].Jnsx) ? null
            //                 : new StepTwoData
            //                 {
            //                     Bs = 2,
            //                     Jnsx = processdetailsList[0].Jnsx,
            //                     Sczls = processdetailsList[0].Sczl,
            //                     Zltp = processdetailsList[0].Zltp
            //                 }
            //};
            return new ResponseContext<ProcessDetails> { Data = processdetailsList[0], Msg = "操作成功!", Code = CommonConstants.SuccessCode };
        }

        /// <summary>
        /// 预览-公司下面的场景实训列表
        /// </summary>
        /// <param name="id">公司id</param>
        /// <returns></returns>
        public async Task<ResponseContext<TrainingServicePreviewListDto>> TrainingServicePreviewList(long id)
        {
            return await Task.Run(() =>
            {
                TrainingServicePreviewListDto item = new TrainingServicePreviewListDto();
                var ca = DbContext.FreeSql.Select<YssxCase>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (ca == null)
                    return new ResponseContext<TrainingServicePreviewListDto> { Msg = "公司信息不存在!", Code = CommonConstants.ErrorCode };

                item.SelectCompanyName = ca.Name;
                item.SceneTrainingListDto = DbContext.FreeSql.Select<YssxSceneTraining>().Where(x => x.SelectCompanyId == id && x.IsDelete == CommonConstants.IsNotDelete).Select(x => new SceneTrainingDto { Id = x.Id, Name = x.Name }).ToList();
                return new ResponseContext<TrainingServicePreviewListDto> { Data = item, Msg = "获取成功!", Code = CommonConstants.SuccessCode };
            });
        }

        /// <summary>
        /// 添加实训进度
        /// </summary>
        /// <param name="dto">添加实训进度dto</param>
        /// <param name="userId">用户id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddProgress(AddProgressDto dto, long userId)
        {
            bool state = false;
            YssxProgress pro = await DbContext.FreeSql.Select<YssxProgress>().Where(x => x.Id == dto.ProgressId && x.CreateBy == userId).FirstAsync();
            pro.Bs = dto.Bs;
            pro.LcxqId = dto.LcxqId;
            pro.CjsxId = dto.CjsxId;
            pro.TmId = dto.TmId;
            pro.TrainType = TrainType.Started;
            pro.UpdateTime = DateTime.Now;
            pro.CreateBy = userId;
            if (null == pro)
            {
                pro.Id = IdWorker.NextId();
                state = await DbContext.FreeSql.GetRepository<YssxProgress>().InsertAsync(pro) != null;
            }
            else
            {
                state = await DbContext.FreeSql.GetRepository<YssxProgress>().UpdateAsync(pro) > 0;
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "操作成功!" : "操作失败!", Data = state };
        }

        /// <summary>
        /// 实训进度详情
        /// </summary>
        /// <param name="id">实训记录id</param>
        /// <param name="userId">用户id</param>
        /// <returns></returns>
        public async Task<ResponseContext<ProgressDetailsDto>> ProgressDetails(long id, long userId)
        {
            try
            {

                ProgressDetailsDto dto = new ProgressDetailsDto();

                var yssxProgress = DbContext.FreeSql.Select<YssxProgress>().Where(x => x.Id == id).First();
                var processdetailsselect = DbContext.FreeSql.Select<YssxProcessDetails>().From<YssxDepartment, YssxPostPersonnel>((pro, dep, pos) => pro.LeftJoin(aa => aa.Xzbm == dep.Id)
                                                                                                                             .LeftJoin(aa => aa.Xzry == pos.Id))
                    .Where((pro, dep, pos) => pro.Id == yssxProgress.LcxqId && pro.IsDelete == CommonConstants.IsNotDelete);
                var sql = processdetailsselect.OrderBy((sc, bu, ca) => sc.Lcxh).ToSql("a.Id,a.Lcxh,a.Name,a.Xzbm,a.Xzry,a.Sczl Sczls,a.Zltp,a.TjtmId TjtmIds,a.Jnsx,a.CjsxId,dep.Name XzbmName,pos.Persons XzryName,pos.PostName");
                List<ProcessDetails> processdetailsList = await DbContext.FreeSql.Ado.QueryAsync<ProcessDetails>(sql);
                var yssxSceneTraining = DbContext.FreeSql.Select<YssxSceneTraining>().Where(x => x.Id == yssxProgress.CjsxId).First();

                dto.Name = yssxSceneTraining.Name;
                dto.LcxqId = processdetailsList[0].Id;
                dto.Lcxh = processdetailsList[0].Lcxh;
                dto.Bs = yssxProgress.Bs;
                //第一步：部门、岗位、人名、实训流程名称、部门图片
                if (yssxProgress.Bs == 1)
                {
                    dto.Xsnr = new Xsnr
                    {
                        XzbmName = processdetailsList[0].XzbmName,
                        PostName = processdetailsList[0].PostName,
                        XzryName = processdetailsList[0].XzryName,
                        Lcmc = processdetailsList[0].Name,
                        FileUrl = processdetailsList[0].Zltp
                    };
                }
                //第二步：实训流程获得资料和注意事项、部门图片
                if (yssxProgress.Bs == 2)
                {
                    dto.Xsnr = new Xsnr
                    {
                        Sczl = JsonConvert.DeserializeObject<List<Sczl>>(processdetailsList[0].Sczls),
                        Zysx = processdetailsList[0].Jnsx,
                        FileUrl = processdetailsList[0].Zltp
                    };
                }
                //第三步：部门、题目列表、总题数、部门图片
                if (yssxProgress.Bs == 3)
                {
                    dto.Xsnr = new Xsnr
                    {
                        XzbmName = processdetailsList[0].XzbmName,
                        Tmlist = JsonConvert.DeserializeObject<List<Tjtm>>(processdetailsList[0].TjtmIds),
                        FileUrl = processdetailsList[0].Zltp
                    };
                }
                //第四步：题目Id
                if (yssxProgress.Bs == 4)
                {
                    dto.Xsnr = new Xsnr
                    {
                        TmId = (long)yssxProgress.TmId
                    };
                }

                //保存的流程序号为1返回数据
                if (processdetailsList[0].Lcxh == 1)
                {
                    if (yssxProgress.Bs == 3)
                    {
                        dto.Sczl = JsonConvert.DeserializeObject<List<Sczl>>(processdetailsList[0].Sczls);
                        dto.Jnsx.Add(processdetailsList[0].Jnsx);
                    }
                    if (yssxProgress.Bs == 4)
                    {
                        List<Tjtm> tmList = new List<Tjtm>();
                        dto.Sczl = JsonConvert.DeserializeObject<List<Sczl>>(processdetailsList[0].Sczls);
                        dto.Jnsx.Add(processdetailsList[0].Jnsx);
                        tmList.AddRange(JsonConvert.DeserializeObject<List<Tjtm>>(processdetailsList[0].TjtmIds));
                        foreach (var item in tmList)
                        {
                            var trainrecordDetail = DbContext.FreeSql.Select<YssxTrainRecordDetail>().Where(x => x.ProgressId == yssxProgress.Id && x.QuestionId == item.Id).First();

                            Tm tm = new Tm()
                            {
                                Id = item.Id,
                                Name = item.Name,
                                Statu = trainrecordDetail == null || trainrecordDetail.Status == AnswerResultStatus.None ? 0 : 1
                            };
                            dto.ZgtmList.Add(tm);
                        }
                    }
                }
                //保存的流程序号大于1返回数据
                else
                {
                    //var processDetailsList = DbContext.FreeSql.Select<YssxProcessDetails>().Where(x => x.CjsxId == yssxProgress.CjsxId && x.Lcxh <= processdetailsList[0].Lcxh && x.IsDelete == CommonConstants.IsNotDelete).ToList();
                    var prosql = DbContext.FreeSql.Select<YssxProcessDetails>().Where(x => x.CjsxId == yssxProgress.CjsxId && x.IsDelete == CommonConstants.IsNotDelete);
                    List<Tjtm> tmList = new List<Tjtm>();
                    ProcessDetailsLcxhAndName pro = new ProcessDetailsLcxhAndName();
                    if (yssxProgress.Bs == 1 || yssxProgress.Bs == 2)
                    {
                        var processDetailsList = prosql.Where(x => x.Lcxh < processdetailsList[0].Lcxh).ToList();
                        foreach (var item in processDetailsList)
                        {
                            dto.Sczl.AddRange(JsonConvert.DeserializeObject<List<Sczl>>(item.Sczl));
                            dto.Jnsx.Add(item.Jnsx);
                            pro.Lcxh = (int)item.Lcxh;
                            pro.Name = item.Name;
                            dto.ProcessDetailsLcxhAndNameList.Add(pro);
                            tmList.AddRange(JsonConvert.DeserializeObject<List<Tjtm>>(item.TjtmId));
                        }
                        foreach (var item in tmList)
                        {
                            var trainrecordDetail = DbContext.FreeSql.Select<YssxTrainRecordDetail>().Where(x => x.ProgressId == yssxProgress.Id && x.QuestionId == item.Id).First();

                            Tm tm = new Tm()
                            {
                                Id = item.Id,
                                Name = item.Name,
                                Statu = trainrecordDetail == null || trainrecordDetail.Status == AnswerResultStatus.None ? 0 : 1
                            };
                            dto.ZgtmList.Add(tm);
                        }
                    }
                    if (yssxProgress.Bs == 3)
                    {
                        var processDetailsList = prosql.Where(x => x.Lcxh <= processdetailsList[0].Lcxh).ToList();
                        //foreach (var item in processDetailsList)
                        //{
                        //    dto.Sczl.AddRange(JsonConvert.DeserializeObject<List<Sczl>>(item.Sczl));
                        //    dto.Jnsx.Add(item.Jnsx);
                        //            pro.Lcxh = (int)item.Lcxh;
                        //        pro.Name = item.Name;
                        //    dto.ProcessDetailsLcxhAndNameList.Add(pro);
                        //}

                        for (int i = 0; i < processDetailsList.Count; i++)
                        {
                            dto.Sczl.AddRange(JsonConvert.DeserializeObject<List<Sczl>>(processDetailsList[i].Sczl));
                            dto.Jnsx.Add(processDetailsList[i].Jnsx);
                            pro.Lcxh = (int)processDetailsList[i].Lcxh;
                            pro.Name = processDetailsList[i].Name;
                            dto.ProcessDetailsLcxhAndNameList.Add(pro);
                            if (i != processDetailsList.Count - 1)
                            {
                                tmList.AddRange(JsonConvert.DeserializeObject<List<Tjtm>>(processDetailsList[i].TjtmId));
                            }
                        }
                        foreach (var item in tmList)
                        {
                            var trainrecordDetail = DbContext.FreeSql.Select<YssxTrainRecordDetail>().Where(x => x.ProgressId == yssxProgress.Id && x.QuestionId == item.Id).First();

                            Tm tm = new Tm()
                            {
                                Id = item.Id,
                                Name = item.Name,
                                Statu = trainrecordDetail == null || trainrecordDetail.Status == AnswerResultStatus.None ? 0 : 1
                            };
                            dto.ZgtmList.Add(tm);
                        }
                    }
                    if (yssxProgress.Bs == 4)
                    {
                        var processDetailsList = prosql.Where(x => x.Lcxh <= processdetailsList[0].Lcxh).ToList();
                        foreach (var item in processDetailsList)
                        {
                            dto.Sczl.AddRange(JsonConvert.DeserializeObject<List<Sczl>>(item.Sczl));
                            dto.Jnsx.Add(item.Jnsx);
                            pro.Lcxh = (int)item.Lcxh;
                            pro.Name = item.Name;
                            dto.ProcessDetailsLcxhAndNameList.Add(pro);
                            tmList.AddRange(JsonConvert.DeserializeObject<List<Tjtm>>(item.TjtmId));
                        }
                        foreach (var item in tmList)
                        {
                            var trainrecordDetail = DbContext.FreeSql.Select<YssxTrainRecordDetail>().Where(x => x.ProgressId == yssxProgress.Id && x.QuestionId == item.Id).First();

                            Tm tm = new Tm()
                            {
                                Id = item.Id,
                                Name = item.Name,
                                Statu = trainrecordDetail == null || trainrecordDetail.Status == AnswerResultStatus.None ? 0 : 1
                            };
                            dto.ZgtmList.Add(tm);
                        }
                    }
                }
                return new ResponseContext<ProgressDetailsDto> { Data = dto, Msg = "操作成功!", Code = CommonConstants.SuccessCode };

            }
            catch (Exception e)
            {

                throw;
            }
        }

        /// <summary>
        /// 复盘
        /// </summary>
        /// <param name="id">场景实训Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<CheckingSummaryListDto>> CheckingSummaryList(long id)
        {
            CheckingSummaryListDto item = new CheckingSummaryListDto();
            item.Tjtm = new List<Tjtm>();
            item.Xzbm = new List<Xzbm>();
            //item.Xzbm.
            //场景实训对象
            var sce = DbContext.FreeSql.Select<YssxSceneTraining>().Where(x => x.Id == id).First();
            //作者寄语
            item.Zzjy = sce.Zzjy;

            //流程详情集合
            var processdetailsselect = DbContext.FreeSql.Select<YssxProcessDetails>().From<YssxDepartment, YssxPostPersonnel>((pro, dep, pos) => pro.LeftJoin(aa => aa.Xzbm == dep.Id)
                                                                                                                         .LeftJoin(aa => aa.Xzry == pos.Id))
                .Where((pro, dep, pos) => pro.CjsxId == id && pro.IsDelete == CommonConstants.IsNotDelete);
            var sql = processdetailsselect.OrderBy((sc, bu, ca) => sc.Lcxh).ToSql("a.Id,a.Lcxh,a.Name,a.Xzbm,a.Xzry,a.Sczl Sczls,a.Zltp,a.TjtmId TjtmIds,a.Jnsx,a.CjsxId,dep.Name XzbmName,pos.Persons XzryName,pos.PostName PostName");
            List<ProcessDetails> processdetailsList = await DbContext.FreeSql.Ado.QueryAsync<ProcessDetails>(sql);
            processdetailsList.ForEach((x) =>
            {
                x.Sczl = JsonConvert.DeserializeObject<List<Sczl>>(x.Sczls);
                x.TjtmId = JsonHelper.DeserializeObject<List<Tjtm>>(x.TjtmIds);
            });
            item.ProcessDetailsList = processdetailsList;
            processdetailsList.ForEach((x) =>
            {
                List<Tjtm> lst = JsonHelper.DeserializeObject<List<Tjtm>>(x.TjtmIds);
                if (lst.Count > 0)
                {
                    //题目集合
                    item.Tjtm.AddRange(lst);
                }
                Xzbm xzbm = new Xzbm()
                {
                    Id = x.Xzbm,
                    Name = x.XzbmName
                };
                Jnzysx jnzysx = new Jnzysx()
                {
                    Jnsx = x.Jnsx,
                    Lcname = x.Name,
                    PostName = x.PostName,
                    Sczl = JsonConvert.DeserializeObject<List<Sczl>>(x.Sczls),
                    Xzry = x.Xzry,
                    XzryName = x.XzryName
                };
                var xz = item.Xzbm.Find(z => z.Name == xzbm.Name);

                if (xz == null)
                {
                    xzbm.Jnzysx.Add(jnzysx);
                }
                else
                {
                    xz.Jnzysx.Add(jnzysx);
                }


                if (item.Xzbm.Count == 0)
                {
                    item.Xzbm.Add(xzbm);
                }
                else if (item.Xzbm.Count > 0 && xz == null)
                {
                    item.Xzbm.Add(xzbm);
                }
            });
            return new ResponseContext<CheckingSummaryListDto> { Code = CommonConstants.SuccessCode, Msg = "操作成功!", Data = item };
        }

        /// <summary>
        ///修改进度状态
        /// </summary>
        /// <param name="id">实训进度id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpdateProgress(long id)
        {
            var state = false;
            var progress = await DbContext.FreeSql.GetRepository<YssxProgress>().Where(s => s.Id == id).FirstAsync();
            progress.TrainType = TrainType.End;
            progress.UpdateTime = DateTime.Now;
            //state = DbContext.FreeSql.Update<YssxProgress>().Set(x => x.TrainType == TrainType.End).Where(x => x.Id == id).ExecuteAffrows() > 0;
            state = await DbContext.FreeSql.GetRepository<YssxProgress>().UpdateAsync(progress) > 0;
            return new ResponseContext<bool>() { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "操作成功!" : "操作失败!", Data = state };
        }
    }
}
