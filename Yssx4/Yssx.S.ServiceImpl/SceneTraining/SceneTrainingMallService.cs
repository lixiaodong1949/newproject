﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.S.Pocos.SceneTraining;

namespace Yssx.S.ServiceImpl.SceneTraining
{
    /// <summary>
    /// 场景实训-首页商城
    /// </summary>
    public class SceneTrainingMallService: ISceneTrainingMallService
    {
        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<PageResponse<SceneTrainingMallResult>> Find(SceneTrainingMallDto model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxSceneTraining>()
                .From<YssxCase, YssxIndustry, YssxBusinessScene>((a,b,c,d)=>a.LeftJoin(aa=>aa.SelectCompanyId==b.Id).LeftJoin(aa=>b.Industry==c.Id).LeftJoin(aa=>aa.YwcjId==d.Id))
                .Where((a, b, c, d) => a.IsDelete == CommonConstants.IsNotDelete);//&& a.Uppershelf == 2

                if (!string.IsNullOrEmpty(model.Name))
                    select = select.Where((a, b, c, d) => a.Name.Contains(model.Name));

                if (model.Sort == 0 || model.Sort == null) select = select.OrderByDescending((a, b, c, d) => a.CreateTime);
                if (model.Sort == 1) select = select.OrderByDescending((a, b, c, d) => a.Fabulous);
                if (model.Sort == 11) select = select.OrderBy((a, b, c, d) => a.Fabulous);
                if (model.Sort == 2) select = select.OrderByDescending((a, b, c, d) => a.Read);
                if (model.Sort == 22) select = select.OrderBy((a, b, c, d) => a.Read);
                if (model.Sort == 3) select = select.OrderByDescending((a, b, c, d) => a.UpdateTime);
                if (model.Sort == 33) select = select.OrderBy((a, b, c, d) => a.UpdateTime);

                if (model.Industry.HasValue){
                    var oldUserinfo = DbContext.FreeSql.Select<YssxCase>().Where(m => m.Industry == model.Industry).First();
                    var CypId = oldUserinfo == null ? 0 : oldUserinfo.Id;
                    select = select.Where((a, b, c, d) => a.SelectCompanyId == CypId);
                }
                if (model.Scene.HasValue) select = select.Where((a, b, c, d) => a.YwcjId==model.Scene);

                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.*,b.`Name` CorporateName,b.CompanyScale Scale,b.TaxPayerType PayTaxes ,c.Id IndustryId,c.`Name` Industry,d.Id BusinessId,d.`Name` Scene");
                var items = DbContext.FreeSql.Ado.Query<SceneTrainingMall>(sql);

                var list = new List<SceneTrainingMallResult>();
                var obj = new SceneTrainingMallResult();
                obj.SceneTrainingMall = items;
                obj.AdvertisingPosition = DbContext.FreeSql.Ado.Query<AdvertisingPosition>("SELECT Id,`Name` FROM yssx_scene_training WHERE IsDelete=0 ORDER BY CreateTime DESC LIMIT 0,4");//&& a.Uppershelf == 2
                obj.Industrys = DbContext.FreeSql.Ado.Query<Industrys>("SELECT Id IndustryId,`Name` FROM yssx_industry  WHERE IsDelete=0");
                obj.Scenes= DbContext.FreeSql.Ado.Query<Scenes>("SELECT Id BusinessId,`Name` FROM yssx_business_scene  WHERE IsDelete=0");
                obj.HotTraining = DbContext.FreeSql.Ado.Query<HotTraining>("SELECT a.Id,a.`Name`,a.`Read`,b.`Name` CorporateName FROM yssx_scene_training a LEFT JOIN yssx_case b ON a.SelectCompanyId=b.Id WHERE a.IsDelete=0 ORDER BY `Read` DESC LIMIT 0,12");//&& a.Uppershelf == 2
                obj.LatestTraining = DbContext.FreeSql.Ado.Query<LatestTraining>("SELECT a.Id,a.`Name`,a.UpdateTime,b.`Name` CorporateName FROM yssx_scene_training a LEFT JOIN yssx_case b ON a.SelectCompanyId=b.Id WHERE a.IsDelete=0 ORDER BY UpdateTime DESC LIMIT 0,12");//&& a.Uppershelf == 2
                list.Add(obj);

                return new PageResponse<SceneTrainingMallResult> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = list };
            });
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<SceneTrainingMallRequest>> Details(long Id)
        {
            return await Task.Run(() =>
            {

                var select = DbContext.FreeSql.Select<YssxSceneTraining>()
                .From<YssxCase, YssxIndustry, YssxBusinessScene>((a,b,c,d)=>a.LeftJoin(aa=>aa.SelectCompanyId==b.Id).LeftJoin(aa=>b.Industry==c.Id).LeftJoin(aa=>aa.YwcjId==d.Id))
                .Where((a, b, c, d) => a.Id==Id&&a.IsDelete == CommonConstants.IsNotDelete);//&& a.Uppershelf == 2

                var totalCount = select.Count();
                var sql = select.ToSql("a.*,b.`Name` CorporateName,b.CompanyScale Scale,b.TaxPayerType PayTaxes,b.EnterpriseInfo CompanyProfile ,c.Id IndustryId,c.`Name` Industry,d.Id BusinessId,d.`Name` Scene ");
                var items = DbContext.FreeSql.Ado.Query<SceneTrainingMallRequest>(sql);

                if (items.Count == 0) return new ResponseContext<SceneTrainingMallRequest> { Code = CommonConstants.ErrorCode, Msg = "获取失败，找不到原始信息!" };

                var obj = new SceneTrainingMallRequest();
                items[0].CompanyProfile = System.Web.HttpUtility.UrlDecode(items[0].CompanyProfile, Encoding.UTF8);
                obj = items[0];
                obj.HotTraining = DbContext.FreeSql.Ado.Query<HotTraining>("SELECT a.Id,a.`Name`,a.`Read`,b.`Name` CorporateName FROM yssx_scene_training a LEFT JOIN yssx_case b ON a.SelectCompanyId=b.Id WHERE a.IsDelete=0 ORDER BY `Read` DESC LIMIT 0,12");//&& a.Uppershelf == 2
                obj.LatestTraining = DbContext.FreeSql.Ado.Query<LatestTraining>("SELECT a.Id,a.`Name`,a.UpdateTime,b.`Name` CorporateName FROM yssx_scene_training a LEFT JOIN yssx_case b ON a.SelectCompanyId=b.Id WHERE a.IsDelete=0 ORDER BY UpdateTime DESC LIMIT 0,12");//&& a.Uppershelf == 2

                var yssxSceneTraining = new YssxSceneTraining { Id = Id, Read = items[0].Read + +1 };
                DbContext.FreeSql.Update<YssxSceneTraining>().SetSource(yssxSceneTraining).UpdateColumns(m => new { m.Read }).ExecuteAffrows();

                return new ResponseContext<SceneTrainingMallRequest> { Code = CommonConstants.SuccessCode, Data = obj };
            });
        }

        /// <summary>
        ///  点赞
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Fabulous(FabulousDao model, long oId)
        {
            long opreationId = oId;//操作人ID

            if (!model.Id.HasValue)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "点赞失败，找不到原始信息!" };

            var oldUserinfo = DbContext.FreeSql.Select<SceneTrainingFabulous>().Where(m => m.CreateBy == opreationId && m.TenantId == model.Id.Value && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (oldUserinfo != null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已点赞，不能在点!" };

            var sceneTrainingFabulous = new SceneTrainingFabulous
            {
                Id = IdWorker.NextId(),
                TenantId = model.Id.Value,
                CreateBy = opreationId,
                //UpdateBy = opreationId,
                //UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                var i = DbContext.FreeSql.GetRepository<SceneTrainingFabulous>().Insert(sceneTrainingFabulous);
                if (i == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "点赞失败" };
                else{
                    var oldUploadCase = DbContext.FreeSql.Select<YssxSceneTraining>().Where(m => m.Id == model.Id.Value).First();
                    var YssxSceneTraining = new YssxSceneTraining { Id = model.Id.Value, Fabulous = oldUploadCase.Fabulous + 1 };
                    DbContext.FreeSql.Update<YssxSceneTraining>().SetSource(YssxSceneTraining).UpdateColumns(m => new { m.Fabulous }).ExecuteAffrows();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
    }
}
