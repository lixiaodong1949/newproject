﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.IServices.Mall;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Mall;

namespace Yssx.S.ServiceImpl.Mall
{
    /// <summary>
    /// 商品
    /// </summary>
    public class MallProductService : IMallProductService
    {

        #region 待废弃
        public async Task<PageResponse<MallProductDto>> GetProductList(GetMallProductDto input)
        {
            var select = DbContext.FreeSql.Select<YssxMallProduct>()
                .WhereIf(input.Id.HasValue && input.Id > 0, p => p.Id == input.Id)
                .WhereIf(!string.IsNullOrEmpty(input.Name), p => p.Name.Contains(input.Name))
                .WhereIf(input.IsActive.HasValue, p => p.IsActive == input.IsActive);
            var totalCount = select.Count();
            var sql = select.Page(input.PageIndex, input.PageSize).ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<MallProductDto>(sql);
            return new PageResponse<MallProductDto>() { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items };
        }

        /// <summary>
        /// 新增商品 - 待废弃
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CreateProduct(CreateMallProductDto input)
        {
            if (string.IsNullOrEmpty(input.Name))
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "必须指定商品名称！", false);
            else if (input.Price <= 0 || input.OldPrice <= 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "必须指定商品现价和原价！", false);
            else if (input.MallProductItemDtos == null || input.MallProductItemDtos.Count == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "必须指定商品的组成项！", false);

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    var yssxMallProduct = input.MapTo<YssxMallProduct>();
                    yssxMallProduct.Id = IdWorker.NextId();
                    await DbContext.FreeSql.Insert<YssxMallProduct>(yssxMallProduct).ExecuteAffrowsAsync();

                    List<YssxMallProductItem> yssxMallProductItems = new List<YssxMallProductItem>();
                    input.MallProductItemDtos.ForEach(pi =>
                    {
                        var yssxMallProductItem = pi.MapTo<YssxMallProductItem>();
                        yssxMallProductItem.Id = IdWorker.NextId();
                        yssxMallProductItem.ProductId = yssxMallProduct.Id;
                        yssxMallProductItems.Add(yssxMallProductItem);
                    });
                    await DbContext.FreeSql.Insert<YssxMallProductItem>(yssxMallProductItems).ExecuteAffrowsAsync();

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "新增商品时异常！", false);
                }
            }


            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        /// <summary>
        /// 删除商品 - 待废弃
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteProduct(DeleteMallProductDto input)
        {
            if (input == null || input.Ids.Length == 0)
                return new ResponseContext<bool>(CommonConstants.SuccessCode, "请指定要删除的商品id数组！", false);

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    var yssxMallProducts = await DbContext.FreeSql.Select<YssxMallProduct>().Where(p => input.Ids.Contains(p.Id)).Where(p => p.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                    if (yssxMallProducts == null || yssxMallProducts.Count == 0)
                        return new ResponseContext<bool>(CommonConstants.ErrorCode, "系统中不存在您要删除的商品！", false);

                    yssxMallProducts.ForEach(p =>
                    {
                        p.IsDelete = CommonConstants.IsDelete;
                        DbContext.FreeSql.Update<YssxMallProduct>(p).SetSource(p).ExecuteAffrows();
                    });

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "删除商品时异常！", false);
                }
            }

            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
        #endregion

        #region 商品
        /// <summary>
        /// 获取商品价格
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetProductPriceOutputDto>>> GetProductPrices(GetProductPriceInputDto input)
        {
            List<GetProductPriceOutputDto> result = new List<GetProductPriceOutputDto>();
            foreach (var item in input.Items)
            {
                if (item.Category == 3)
                {
                    var productPrices = await DbContext.FreeSql.Select<YssxMallProduct>()
                        .Where(x => x.Id == item.TargetId && x.IsDelete == CommonConstants.IsNotDelete)
                        .ToListAsync(x => new GetProductPriceOutputDto
                        {
                            TargetId = item.TargetId,
                            Category = item.Category,
                            Name = x.Name,
                            Price = x.Price,
                            OldPrice = x.OldPrice
                        });
                    result.AddRange(productPrices);
                }
                else
                {
                    var productPrices = await DbContext.FreeSql.Select<YssxMallProduct, YssxMallProductItem>()
                        .InnerJoin((p, pi) => p.Id == pi.ProductId && pi.IsDelete == CommonConstants.IsNotDelete)
                        .Where((p, pi) => pi.TargetId == item.TargetId && pi.Category == item.Category && p.IsDelete == CommonConstants.IsNotDelete)
                        .ToListAsync((p, pi) => new GetProductPriceOutputDto
                        {
                            TargetId = pi.TargetId,
                            Category = pi.Category,
                            Name = p.Name,
                            Price = p.Price,
                            OldPrice = p.OldPrice
                        });
                    result.AddRange(productPrices);
                }
            }

            return new ResponseContext<List<GetProductPriceOutputDto>>(CommonConstants.SuccessCode, "", result);
        }


        /// <summary>
        /// 保存商品
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveProduct(SaveProductDto model, long currentUserId)
        {
            DateTime dtNow = DateTime.Now;
            return await Task.Run(() =>
            {
                #region 校验
                //TODO 校验
                if (model.Id > 0)
                {
                    var rAnyEntity = DbContext.FreeSql.GetRepository<YssxMallProduct>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rAnyEntity.Any())
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到商品信息" };
                }
                if (string.IsNullOrEmpty(model.Name))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "商品名称不能为空" };
                if (string.IsNullOrEmpty(model.ShortDesc))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "商品简介不能为空" };
                if (string.IsNullOrEmpty(model.FullDesc))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "商品描述不能为空" };
                if (model.OldPrice <= 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "商品原价不能小于等于0" };
                if (string.IsNullOrEmpty(model.Category))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择商品分类" };
                if (model.SaleStatus == 1 && !model.RegularSaleTime.HasValue)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请填写定时开售时间" };
                if (model.SaleStatus == 1 && model.RegularSalePrice <= 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请填写定时开售价格" };
                #endregion
                //商品数据
                var product = model.MapTo<YssxMallProduct>();
                if (model.Id == 0) { product.Id = IdWorker.NextId(); }
                product.CreateBy = currentUserId;
                product.CreateTime = dtNow;
                product.UpdateBy = currentUserId;
                product.UpdateTime = dtNow;
                //分类数据
                var rCategoryIds = model.Category.Split(',').Select(x => Convert.ToInt64(x)).ToList();
                var rSelectCategoryData = DbContext.FreeSql.GetRepository<YssxMallCategory>()
                    .Where(m => rCategoryIds.Contains(m.Id) && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                var rAddProductCategory = new List<YssxMallProductCategory>();
                var rDelProductCategory = new List<YssxMallProductCategory>();
                if (model.Id == 0)
                {
                    rAddProductCategory = rSelectCategoryData.Select(x => new YssxMallProductCategory
                    {
                        Id = IdWorker.NextId(),
                        ProductId = product.Id,
                        FirstCategoryId = x.ParentId,
                        SecondCategoryId = x.Id,
                        CreateBy = currentUserId,
                        CreateTime = dtNow,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                }
                else
                {
                    var rProductCategoryData = DbContext.FreeSql.GetRepository<YssxMallProductCategory>().Where(m => m.ProductId == model.Id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                    var rOldCategoryId = rProductCategoryData.Select(m => m.SecondCategoryId).ToList();
                    var rNewCategoryId = rSelectCategoryData.Select(m => m.Id).ToList();
                    //取新增和删除的分类数据
                    rAddProductCategory = rSelectCategoryData.Where(x => !rOldCategoryId.Contains(x.Id)).Select(x => new YssxMallProductCategory
                    {
                        Id = IdWorker.NextId(),
                        ProductId = product.Id,
                        FirstCategoryId = x.ParentId,
                        SecondCategoryId = x.Id,
                        CreateBy = currentUserId,
                        CreateTime = dtNow,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    rDelProductCategory = rProductCategoryData.Where(x => !rNewCategoryId.Contains(x.SecondCategoryId)).Select(x => new YssxMallProductCategory
                    {
                        Id = x.Id,
                        IsDelete = CommonConstants.IsDelete,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                }

                if (model.Id == 0)
                {
                    //添加数据
                    var state = false;
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        state = DbContext.FreeSql.Insert(product).ExecuteAffrows() > 0;
                        if (state)
                            DbContext.FreeSql.Insert<YssxMallProductCategory>().AppendData(rAddProductCategory).ExecuteAffrows();
                        uow.Commit();
                    }
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state, Msg = state ? "成功" : "操作失败" };
                }
                else
                {
                    //更新数据
                    var state = false;
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        //更新商品
                        state = DbContext.FreeSql.Update<YssxMallProduct>().SetSource(product).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.IsDelete, a.ViewsNumber, a.LikesNumber }).ExecuteAffrows() > 0;
                        //添加明细
                        if (state && rAddProductCategory.Count > 0)
                            DbContext.FreeSql.Insert<YssxMallProductCategory>().AppendData(rAddProductCategory).ExecuteAffrows();
                        //删除明细
                        if (state && rDelProductCategory.Count > 0)
                            DbContext.FreeSql.Update<YssxMallProductCategory>().SetSource(rDelProductCategory).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();

                        uow.Commit();
                    }
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state, Msg = state ? "成功" : "操作失败" };
                }
            });
        }

        /// <summary>
        /// 删除商品
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteProduct(long id, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;

                var entity = DbContext.FreeSql.GetRepository<YssxMallProduct>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到商品数据!" };

                entity.IsDelete = CommonConstants.IsDelete;
                entity.UpdateTime = dtNow;
                entity.UpdateBy = currentUserId;

                var listDetail = DbContext.FreeSql.GetRepository<YssxMallProductItem>().Where(x => x.ProductId == id && x.IsDelete == CommonConstants.IsNotDelete).ToList(m => new YssxMallProductItem
                {
                    Id = m.Id,
                    IsDelete = CommonConstants.IsDelete,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                });

                bool state = false;
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //商品
                    state = DbContext.FreeSql.Update<YssxMallProduct>().SetSource(entity).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                    //明细
                    if (state && listDetail.Count > 0)
                        DbContext.FreeSql.Update<YssxMallProductItem>().SetSource(listDetail).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();

                    tran.Commit();
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 获取商品列表
        /// </summary>
        public async Task<PageResponse<GetProductByPageDto>> GetProductByPage(GetProductByPageRequest model)
        {
            return await Task.Run(() =>
            {
                var rQueryProductId = new List<long>();
                if (model.Category > 0)
                {
                    rQueryProductId = DbContext.FreeSql.GetRepository<YssxMallProductCategory>()
                        .Where(m => (m.FirstCategoryId == model.Category || m.SecondCategoryId == model.Category) && m.IsDelete == CommonConstants.IsNotDelete)
                        .ToList(m => m.ProductId);
                }

                var rProductId = Convert.ToInt64(model.Id);
                var select = DbContext.FreeSql.GetRepository<YssxMallProduct>().Select.From<YssxUser>((a, b) =>
                    a.LeftJoin(aa => aa.UpdateBy == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete)
                    .WhereIf(model.IsActive == 0, (a, b) => !a.IsActive)
                    .WhereIf(model.IsActive == 1, (a, b) => a.IsActive)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b) => a.Name.Contains(model.Name))
                    .WhereIf(rProductId > 0, (a, b) => a.Id == rProductId)
                    .WhereIf(model.Category > 0 && rQueryProductId.Count > 0, (a, b) => rQueryProductId.Contains(a.Id))
                    .WhereIf(model.CreateTimeBegin.HasValue, (a, b) => a.CreateTime >= model.CreateTimeBegin)
                    .WhereIf(model.CreateTimeEnd.HasValue, (a, b) => a.CreateTime <= model.CreateTimeEnd)
                    .WhereIf(model.PriceLow > 0, (a, b) => a.Price >= model.PriceLow)
                    .WhereIf(model.PriceHigh > 0, (a, b) => a.Price <= model.PriceHigh)
                    .OrderByDescending((a, b) => a.CreateTime);

                var totalCount = select.Count();
                var selectData = select.Page(model.PageIndex, model.PageSize).ToList((a, b) => new GetProductByPageDto
                {
                    Id = a.Id,
                    Img = a.Img,
                    Name = a.Name,
                    ShortDesc = a.ShortDesc,
                    Price = a.Price,
                    OldPrice = a.OldPrice,
                    IsActive = a.IsActive,
                    SalesVolume = 0,
                    Category = "",
                    CategoryName = "",
                    CreateTime = a.CreateTime,
                    UpdateByName = b.RealName,
                    UpdateTime = a.UpdateTime
                });
                //分类名称
                foreach (var item in selectData)
                {
                    var selectCategoryData = DbContext.FreeSql.Select<YssxMallProductCategory, YssxMallCategory>()
                        .InnerJoin((a, b) => a.SecondCategoryId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                        .Where((a, b) => a.ProductId == item.Id && a.IsDelete == CommonConstants.IsNotDelete)
                        .ToList((a, b) => new { b.Id, b.Name });
                    item.Category = string.Join(',', selectCategoryData.Select(x => x.Id));
                    item.CategoryName = string.Join(',', selectCategoryData.Select(x => x.Name));
                }


                return new PageResponse<GetProductByPageDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 获取指定商品数据
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<SaveProductDto>> GetProductInfoById(long id)
        {
            return await Task.Run(() =>
            {
                var entity = DbContext.FreeSql.GetGuidRepository<YssxMallProduct>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<SaveProductDto> { Code = CommonConstants.ErrorCode, Msg = "找不到商品数据" };
                //更新观看数
                entity.ViewsNumber += 1;
                bool viewState = DbContext.FreeSql.Update<YssxMallProduct>().SetSource(entity).UpdateColumns(m => new { m.ViewsNumber }).ExecuteAffrows() > 0;
                var resData = entity.MapTo<SaveProductDto>();
                //分类
                var selectCategoryData = DbContext.FreeSql.Select<YssxMallProductCategory, YssxMallCategory>()
                    .InnerJoin((a, b) => a.SecondCategoryId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                    .Where((a, b) => a.ProductId == id && a.IsDelete == CommonConstants.IsNotDelete)
                    .ToList((a, b) => b.Id);
                resData.Category = string.Join(',', selectCategoryData);
                //商品明细
                var detailData = DbContext.FreeSql.GetGuidRepository<YssxMallProductItem>().Where(x => x.ProductId == id && x.IsDelete == CommonConstants.IsNotDelete)
                    .ToList(x => new SaveProductDetailDto
                    {
                        Id = x.Id,
                        ProductId = x.ProductId,
                        Category = x.Category,
                        TargetId = x.TargetId,
                        Source = x.Source
                    });
                resData.ItemDetail = detailData;

                return new ResponseContext<SaveProductDto> { Code = CommonConstants.SuccessCode, Data = resData };
            });
        }

        /// <summary>
        /// 商品上下架
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetProductStatus(long id, bool isActive, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;

                var entity = DbContext.FreeSql.GetRepository<YssxMallProduct>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到商品数据" };

                if (isActive && !DbContext.FreeSql.GetRepository<YssxMallProductItem>().Where(x => x.ProductId == id && x.IsDelete == CommonConstants.IsNotDelete).Any())
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，要上架商品未配置商品明细" };

                entity.IsActive = isActive;
                entity.UpdateTime = dtNow;
                entity.UpdateBy = currentUserId;

                bool state = false;
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //商品
                    state = DbContext.FreeSql.Update<YssxMallProduct>().SetSource(entity).UpdateColumns(m => new { m.IsActive, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                    
                    tran.Commit();
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }




        /// <summary>
        /// 保存商品明细
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveProductDetail(SaveProductDetailDto model, long currentUserId)
        {
            DateTime dtNow = DateTime.Now;
            return await Task.Run(() =>
            {
                #region 校验
                //TODO 校验
                var rAnyEntity = DbContext.FreeSql.GetRepository<YssxMallProduct>().Where(m => m.Id == model.ProductId && m.IsDelete == CommonConstants.IsNotDelete);
                if (!rAnyEntity.Any())
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到商品信息" };
                if (model.TargetId == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，关联案例ID错误" };
                if (string.IsNullOrEmpty(model.TargetName))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，关联案例名称不能为空" };
                if (model.Category <= 0 || model.Category > 2)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，分类错误" };
                var rAnyTargetId = DbContext.FreeSql.GetRepository<YssxMallProductItem>().Where(m => m.ProductId == model.ProductId && m.TargetId == model.TargetId && m.IsDelete == CommonConstants.IsNotDelete);
                if (rAnyTargetId.Any())
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，关联的案例或月度任务已存在" };
                #endregion

                var rProductItems = new YssxMallProductItem()
                {
                    Id = IdWorker.NextId(),
                    ProductId = model.ProductId,
                    Category = model.Category,
                    TargetId = model.TargetId,
                    TargetName = model.TargetName,
                    Source = model.Source,
                    CreateBy = currentUserId,
                    CreateTime = dtNow,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };
                //添加数据
                var state = DbContext.FreeSql.Insert(rProductItems).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state };
            });
        }

        /// <summary>
        /// 删除商品明细
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteProductDetail(long id, long productId, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;

                var entity = DbContext.FreeSql.GetRepository<YssxMallProductItem>().Where(m => m.Id == id && m.ProductId == productId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到商品明细数据!" };

                entity.IsDelete = CommonConstants.IsDelete;
                entity.UpdateTime = dtNow;
                entity.UpdateBy = currentUserId;

                //商品明细
                bool state = DbContext.FreeSql.Update<YssxMallProductItem>().SetSource(entity).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }


        /// <summary>
        /// 商品移出指定分类
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteProductCategory(long id, long categoryId, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                var entity = DbContext.FreeSql.GetRepository<YssxMallProductCategory>().Where(m => m.ProductId == id && m.SecondCategoryId == categoryId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到商品分类关联数据!" };

                entity.IsDelete = CommonConstants.IsDelete;
                entity.UpdateTime = dtNow;
                entity.UpdateBy = currentUserId;

                //商品明细
                bool state = DbContext.FreeSql.Update<YssxMallProductCategory>().SetSource(entity).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 设置商品分类信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetProductCategoryInfo(SetProductCategoryInfoDto model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                var product = DbContext.FreeSql.GetRepository<YssxMallProduct>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (product == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到商品数据!" };

                product.UpdateTime = dtNow;
                product.UpdateBy = currentUserId;
                //分类数据
                var rCategoryIds = model.Category.Split(',').Select(x => Convert.ToInt64(x)).ToList();
                var rSelectCategoryData = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(m => rCategoryIds.Contains(m.Id) && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                
                var rAddProductCategory = new List<YssxMallProductCategory>();
                var rDelProductCategory = new List<YssxMallProductCategory>();

                var rProductCategoryData = DbContext.FreeSql.GetRepository<YssxMallProductCategory>().Where(m => m.ProductId == model.Id && m.IsDelete == CommonConstants.IsNotDelete).ToList();
                var rOldCategoryId = rProductCategoryData.Select(m => m.SecondCategoryId).ToList();
                var rNewCategoryId = rSelectCategoryData.Select(m => m.Id).ToList();
                //取新增和删除的分类数据
                rAddProductCategory = rSelectCategoryData.Where(x => !rOldCategoryId.Contains(x.Id)).Select(x => new YssxMallProductCategory
                {
                    Id = IdWorker.NextId(),
                    ProductId = model.Id,
                    FirstCategoryId = x.ParentId,
                    SecondCategoryId = x.Id,
                    CreateBy = currentUserId,
                    CreateTime = dtNow,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                }).ToList();
                rDelProductCategory = rProductCategoryData.Where(x => !rNewCategoryId.Contains(x.SecondCategoryId)).Select(x => new YssxMallProductCategory
                {
                    Id = x.Id,
                    IsDelete = CommonConstants.IsDelete,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                }).ToList();

                //更新数据
                var state = false;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //更新商品
                    state = DbContext.FreeSql.Update<YssxMallProduct>().SetSource(product).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime }).ExecuteAffrows() > 0;
                    //添加明细
                    if (state && rAddProductCategory.Count > 0)
                        DbContext.FreeSql.Insert<YssxMallProductCategory>().AppendData(rAddProductCategory).ExecuteAffrows();
                    //删除明细
                    if (state && rDelProductCategory.Count > 0)
                        DbContext.FreeSql.Update<YssxMallProductCategory>().SetSource(rDelProductCategory).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();

                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state, Msg = state ? "成功" : "操作失败" };
            });

        }

        /// <summary>
        /// 设置商品排序
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetProductSortInfo(SetProductSortInfoDto model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                if (model.Detail.Count == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，入参不能为空" };

                List<YssxMallCategory> rMallCategoryData = new List<YssxMallCategory>();
                var loop = 0;
                foreach (var item in model.Detail)
                {
                    loop++;
                    var rFirstLevelEntity = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(m => m.Id == item.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (rFirstLevelEntity != null && rFirstLevelEntity.Sort != loop)
                    {
                        var rFirstLevelData = new YssxMallCategory
                        {
                            Id = item.Id,
                            Sort = loop,
                            UpdateBy = currentUserId,
                            UpdateTime = dtNow
                        };
                        rMallCategoryData.Add(rFirstLevelData);
                    }
                    //明细
                    var loopItem = 0;
                    foreach (var itemDetail in item.ItemDetail)
                    {
                        loopItem++;
                        var rSecondLevelEntity = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(m => m.Id == itemDetail.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                        if (rSecondLevelEntity != null && rSecondLevelEntity.Sort != loop)
                        {
                            var rSecondLevelData = new YssxMallCategory
                            {
                                Id = itemDetail.Id,
                                Sort = loopItem,
                                UpdateBy = currentUserId,
                                UpdateTime = dtNow
                            };
                            rMallCategoryData.Add(rSecondLevelData);
                        }
                    }
                }
                //更新数据
                var state = DbContext.FreeSql.Update<YssxMallCategory>().SetSource(rMallCategoryData).UpdateColumns(a => new { a.Sort, a.UpdateBy, a.UpdateTime }).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state, Msg = state ? "成功" : "操作失败" };
            });
        }

        /// <summary>
        /// 商品点赞
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetProductLikesNumber(long id, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;

                var entity = DbContext.FreeSql.GetRepository<YssxMallProduct>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到商品数据!" };
                var rAnyEntity = DbContext.FreeSql.GetRepository<YssxMallProductLike>().Where(m => m.ProductId == id && m.UserId == currentUserId && m.IsDelete == CommonConstants.IsNotDelete);
                if (rAnyEntity.Any())
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "成功", Data = true };
                //点赞数
                entity.LikesNumber += 1;
                //点赞记录
                var rProductLike = new YssxMallProductLike()
                {
                    Id = IdWorker.NextId(),
                    ProductId = entity.Id,
                    UserId = currentUserId,
                    CreateBy = currentUserId,
                    CreateTime = dtNow,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };
                //添加数据
                var state = DbContext.FreeSql.Insert(rProductLike).ExecuteAffrows() > 0;
                if (state)
                    DbContext.FreeSql.Update<YssxMallProduct>().SetSource(entity).UpdateColumns(m => new { m.LikesNumber }).ExecuteAffrows();
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }


        #endregion

        #region 商品分类
        /// <summary>
        /// 获取商品分类列表
        /// </summary>
        public async Task<PageResponse<GetMallCategoryListDto>> GetMallCategoryList()
        {
            return await Task.Run(() =>
            {
                var selectAllCategory = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.Sort).ToList();
                var selectAllProduct = DbContext.FreeSql.GetRepository<YssxMallProductCategory>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).ToList();
                //一级分类
                var selectData = selectAllCategory.Where(x => x.ParentId == 0).Select(x => new GetMallCategoryListDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Subtitle = x.Subtitle,
                    Sort = x.Sort,
                    LayoutType = x.LayoutType,
                    CreateTime = x.CreateTime
                }).ToList();
                //二级分类
                foreach (var item in selectData)
                {
                    var selectDetailData = selectAllCategory.Where(x => x.ParentId == item.Id).Select(x => new GetMallCategoryListDetailDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Sort = x.Sort,
                        Quantity = 0,
                        CreateTime = x.CreateTime
                    }).ToList();
                    foreach (var itemDetail in selectDetailData)
                    {
                        itemDetail.Quantity = selectAllProduct.Where(x => x.SecondCategoryId == itemDetail.Id).Count();
                    }
                    item.ItemDetail = selectDetailData;
                }

                return new PageResponse<GetMallCategoryListDto> { PageIndex = 1, PageSize = selectData.Count, RecordCount = selectData.Count, Data = selectData };
            });
        }

        /// <summary>
        /// 保存商品分类
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveMallCategory(SaveMallCategoryDto model, long currentUserId)
        {
            DateTime dtNow = DateTime.Now;
            return await Task.Run(() =>
            {
                #region 校验
                //TODO 校验
                if (model.Id > 0)
                {
                    var rAnyEntity = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rAnyEntity.Any())
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到商品分类信息" };
                }
                if (string.IsNullOrEmpty(model.Name))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "商品名称不能为空" };
                var rAnyNameEntity = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(m => m.Id != model.Id && m.Name == model.Name && m.IsDelete == CommonConstants.IsNotDelete);
                if (rAnyNameEntity.Any())
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，已存在该商品分类" };
                #endregion

                var rProductCategory = new YssxMallCategory
                {
                    Id = model.Id > 0 ? model.Id : IdWorker.NextId(),
                    Name = model.Name,
                    Subtitle = model.Subtitle,
                    Sort = 0,
                    ParentId = model.ParentId,
                    LayoutType = model.LayoutType,
                    CreateBy = currentUserId,
                    CreateTime = dtNow,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                };
                if (model.Id == 0)
                {
                    var rMaxEntity = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(m => m.ParentId == model.ParentId && m.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(x => x.Sort).First();
                    rProductCategory.Sort = rMaxEntity == null ? 1 : rMaxEntity.Sort + 1;
                }

                //更新数据
                var state = false;
                if (model.Id == 0)
                    state = DbContext.FreeSql.Insert(rProductCategory).ExecuteAffrows() > 0;
                else
                    state = DbContext.FreeSql.Update<YssxMallCategory>().SetSource(rProductCategory).UpdateColumns(m => new { m.Name, m.Subtitle, m.LayoutType, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state };
            });
        }

        /// <summary>
        /// 删除商品分类
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteMallCategory(long id, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;

                var entity = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到原始信息!" };

                entity.IsDelete = CommonConstants.IsDelete;
                entity.UpdateTime = dtNow;
                entity.UpdateBy = currentUserId;

                var listDetail = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(x => x.ParentId == id && x.IsDelete == CommonConstants.IsNotDelete).ToList(m => new YssxMallCategory
                {
                    Id = m.Id,
                    IsDelete = CommonConstants.IsDelete,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                });

                bool state = false;
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //一级分类
                    state = DbContext.FreeSql.Update<YssxMallCategory>().SetSource(entity).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;
                    //二级分类
                    if (state && listDetail.Count > 0)
                        DbContext.FreeSql.Update<YssxMallCategory>().SetSource(listDetail).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();

                    tran.Commit();
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 获取二级分类下商品列表
        /// </summary>
        public async Task<PageResponse<GetSecondLevelProductByPageDto>> GetSecondLevelProductByPage(GetSecondLevelProductByPageRequest model)
        {
            return await Task.Run(() =>
            {
                var selectAllCategory = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.Sort).ToList();

                var select = DbContext.FreeSql.GetRepository<YssxMallProduct>().Select.From<YssxMallProductCategory,YssxUser>((a, b, c) =>
                    a.InnerJoin(aa => aa.Id == b.ProductId && b.SecondCategoryId == model.Id && b.IsDelete == CommonConstants.IsNotDelete)
                    .LeftJoin(aa => aa.UpdateBy == c.Id && c.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderByDescending((a, b, c) => a.UpdateTime);

                var totalCount = select.Count();
                var selectData = select.Page(model.PageIndex, model.PageSize).ToList((a, b, c) => new GetSecondLevelProductByPageDto
                {
                    Id = a.Id,
                    Img = a.Img,
                    Name = a.Name,
                    Price = a.Price,
                    OldPrice = a.OldPrice,
                    UpdateByName = c.RealName,
                    UpdateTime = a.UpdateTime
                });
                //取分类名称
                foreach (var item in selectData)
                {
                    var rSecondCategoryId = DbContext.FreeSql.GetRepository<YssxMallProductCategory>()
                        .Where(x => x.ProductId == item.Id && x.IsDelete == CommonConstants.IsNotDelete).ToList(x => x.SecondCategoryId);
                    if (selectAllCategory.Count > 0)
                    {
                        var rCategoryName = selectAllCategory.Where(x => rSecondCategoryId.Contains(x.Id)).Select(x => x.Name).ToList();
                        item.Category = string.Join(',', rSecondCategoryId);
                        item.CategoryName = string.Join(',', rCategoryName);
                    }
                }
                return new PageResponse<GetSecondLevelProductByPageDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 分类升为一级分类
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SetMallCategoryLevel(long id, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;

                var entity = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，找不到分类数据!" };

                entity.ParentId = 0;
                entity.UpdateTime = dtNow;
                entity.UpdateBy = currentUserId;
                //取最大排序数字
                var rMaxEntity = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(m => m.ParentId == 0 && m.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(x => x.Sort).First();
                entity.Sort = rMaxEntity == null ? 1 : rMaxEntity.Sort + 1;

                bool state = DbContext.FreeSql.Update<YssxMallCategory>().SetSource(entity).UpdateColumns(m => new { m.ParentId, m.Sort, m.UpdateBy, m.UpdateTime }).ExecuteAffrows() > 0;

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }


        #endregion

        #region 获取关联数据
        /// <summary>
        /// 获取关联数据 课程列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<YssxCourseDto>> GetBindDataInCourse(GetBindDataInCourseDto model)
        {
            var result = new PageResponse<YssxCourseDto>();
            var select = DbContext.FreeSql.GetRepository<YssxCourse>().Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.AuditStatus == 1)
                .WhereIf(!string.IsNullOrEmpty(model.Name), x => x.CourseName.Contains(model.Name));
            var totalCount = await select.CountAsync();
            var sql = select.OrderByDescending(a => a.CreateTime)
              .Page(model.PageIndex, model.PageSize).ToSql("a.Id,a.CourseName,a.CourseType,a.CourseTitle,a.CourseTypeId,a.Author,a.Education,a.Images,a.PublishingHouse,a.PublishingDate,a.Intro,a.Description,a.CreateTime,a.UpdateTime");
            var items = await DbContext.FreeSql.Ado.QueryAsync<YssxCourseDto>(sql);

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = model.PageSize;
            result.PageIndex = model.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }

        #endregion

        #region APP商城

        /// <summary>
        /// 获取商品分类列表 - APP
        /// </summary>
        public async Task<PageResponse<GetMallCategoryListDto>> GetMallCategoryListApp()
        {
            return await Task.Run(() =>
            {
                var selectAllCategory = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(x => x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.Sort).ToList();
                //一级分类
                var selectData = selectAllCategory.Where(x => x.ParentId == 0).Select(x => new GetMallCategoryListDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Subtitle = x.Subtitle,
                    Sort = x.Sort,
                    LayoutType = x.LayoutType,
                    CreateTime = x.CreateTime
                }).ToList();
                //二级分类
                foreach (var item in selectData)
                {
                    var selectDetailData = selectAllCategory.Where(x => x.ParentId == item.Id).Select(x => new GetMallCategoryListDetailDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Sort = x.Sort,
                        Quantity = 0,
                        CreateTime = x.CreateTime
                    }).ToList();
                    item.ItemDetail = selectDetailData;
                }

                return new PageResponse<GetMallCategoryListDto> { PageIndex = 1, PageSize = selectData.Count, RecordCount = selectData.Count, Data = selectData };
            });
        }

        /// <summary>
        /// 获取商品二级分类列表 - APP
        /// </summary>
        public async Task<PageResponse<GetMallCategoryListDetailDto>> GetMallCategoryByParentIdApp(long id)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetRepository<YssxMallCategory>().Where(x => x.ParentId == id && x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.Sort)
                .ToList(x => new GetMallCategoryListDetailDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Sort = x.Sort,
                    Quantity = 0,
                    CreateTime = x.CreateTime
                });

                return new PageResponse<GetMallCategoryListDetailDto> { PageIndex = 1, PageSize = selectData.Count, RecordCount = selectData.Count, Data = selectData };
            });
        }

        /// <summary>
        /// 首页获取一级分类下TOP部分商品
        /// </summary>
        /// <param name="top"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetCategoryProductsTopToHomeResponseDto>>> GetCategoryProductsTopToHomeApp(int top = 4)
        {
            return await Task.Run(() =>
            {
                var Categorys = DbContext.FreeSql.GetGuidRepository<YssxMallCategory>()
                  .Where(x => x.IsDelete == CommonConstants.IsNotDelete && x.ParentId == 0).OrderBy(x => x.Sort)
                  .ToList<GetCategoryProductsTopToHomeResponseDto>();

                if (Categorys != null && Categorys.Count > 0)
                {
                    foreach (var item in Categorys)
                    {
                        var selectData = DbContext.FreeSql.GetGuidRepository<YssxMallProduct>(x => x.IsDelete == CommonConstants.IsNotDelete).Select
                            .From<YssxMallProductCategory>((a, b) => a.InnerJoin(aa => aa.Id == b.ProductId && b.IsDelete == CommonConstants.IsNotDelete))
                            .Where((a, b) => b.FirstCategoryId == item.Id || b.SecondCategoryId == item.Id)
                            .GroupBy((a, b) => a.Id)
                            .OrderBy(x => x.Key)
                            .Page(0, top).Select(x => new ProductBaseResponseDto
                            {
                                Id = x.Key,
                                Name = x.Value.Item1.Name,
                                ShortDesc = x.Value.Item1.ShortDesc,
                                Img = x.Value.Item1.Img,
                                LikesNumber = x.Value.Item1.LikesNumber,
                                ViewsNumber = x.Value.Item1.ViewsNumber
                            }).ToList();
                        item.Products = selectData;
                    }
                }
                return new ResponseContext<List<GetCategoryProductsTopToHomeResponseDto>> { Code = CommonConstants.SuccessCode, Data = Categorys };
            });
        }

        /// <summary>
        /// 商品列表
        /// </summary>
        /// <param name="top"></param>
        /// <returns></returns>
        public async Task<PageResponse<ProductBaseResponseDto>> QueryProductsApp(QueryProductsRequestDto model)
        {
            long count = 0;

            #region 2020-12-15
            /*
            var select = DbContext.FreeSql.GetGuidRepository<YssxMallProduct>(x => x.IsDelete == CommonConstants.IsNotDelete).Select
                        .From<YssxMallProductCategory>((a, b) => a.InnerJoin(aa => aa.Id == b.ProductId && b.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b) => a.IsActive == true)
                        .WhereIf(model.FirstCategoryId > 0, (a, b) => b.FirstCategoryId == model.FirstCategoryId || b.SecondCategoryId == model.FirstCategoryId)
                        .WhereIf(model.SecondCategoryId > 0, (a, b) => b.SecondCategoryId == model.SecondCategoryId)
                        .WhereIf(!string.IsNullOrEmpty(model.Keyword), (a, b) => a.Name.Contains(model.Keyword))
                        .GroupBy((a, b) => a.Id);            
            */
            #endregion

            var select = DbContext.FreeSql.GetGuidRepository<YssxMallProduct>(x => x.IsDelete == CommonConstants.IsNotDelete)
                    .Select.From<YssxMallProductCategory, YssxMallProductItem>((a, b, c) =>
                        a.InnerJoin(x => x.Id == b.ProductId && b.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(x => x.Id == c.ProductId && c.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b, c) => a.IsActive == true && a.IsDelete == CommonConstants.IsNotDelete)
                        .WhereIf(model.FirstCategoryId > 0, (a, b, c) => b.FirstCategoryId == model.FirstCategoryId || b.SecondCategoryId == model.FirstCategoryId)
                        .WhereIf(model.SecondCategoryId > 0, (a, b, c) => b.SecondCategoryId == model.SecondCategoryId)
                        .WhereIf(!string.IsNullOrEmpty(model.Keyword), (a, b, c) => a.Name.Contains(model.Keyword))
                        .GroupBy((a, b, c) => a.Id);

            //排序
            if (model.Orderby == 0)
                select = select.OrderBy(x => x.Value.Item1.Id);
            else
                select = select.OrderByDescending(x => x.Value.Item1.CreateTime);

            count = select.ToList(x => x.Key).Count;
            var selectData = await select.Page(model.PageIndex, model.PageSize).ToListAsync(x => new ProductBaseResponseDto
            {
                Id = x.Key,
                Name = x.Value.Item1.Name,
                ShortDesc = x.Value.Item1.ShortDesc,
                Img = x.Value.Item1.Img,
                LikesNumber = x.Value.Item1.LikesNumber,
                ViewsNumber = x.Value.Item1.ViewsNumber
            });

            var data = new PageResponse<ProductBaseResponseDto>
            {
                Code = CommonConstants.SuccessCode,
                Data = selectData,
                RecordCount = count,
                PageSize = model.PageSize,
                PageIndex = model.PageIndex,
            };
            return data;
        }

        #endregion

    }
}
