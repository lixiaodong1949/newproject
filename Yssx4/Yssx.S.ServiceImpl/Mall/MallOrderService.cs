﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.Configurations;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.IServices.Mall;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Mall;
using Yssx.S.Pocos.Order;

namespace Yssx.S.ServiceImpl.Mall
{
    /// <summary>
    /// 订单
    /// </summary>
    public class MallOrderService : IMallOrderService
    {
        #region 订单
        /// <summary>
        /// 查询用户所有订单
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<UserMallOrder>> GetUserOrders(GetUserMallOrder input)
        {
            UserMallOrder userMallOrders = new UserMallOrder();
            var yssxUser = await DbContext.FreeSql.Select<YssxUser>().Where(u => u.Id == input.UserId && u.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (yssxUser == null)
                return new ResponseContext<UserMallOrder>(CommonConstants.ErrorCode, "没有找到该用户！", null);
            var school = await DbContext.FreeSql.Select<YssxSchool>().Where(s => s.Id == yssxUser.TenantId && s.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (school == null)
                return new ResponseContext<UserMallOrder>(CommonConstants.ErrorCode, "没有找到该用户所属的学校！", null);
            else if (school.Id == CommonConstants.DefaultTenantId)
                userMallOrders.SchoolName = "";
            else
                userMallOrders.SchoolName = school.Name;
            userMallOrders.UserName = yssxUser.UserName;
            userMallOrders.MobilePhone = yssxUser.MobilePhone;
            userMallOrders.RealName = yssxUser.RealName;
            userMallOrders.UserType = yssxUser.UserType;
            userMallOrders.Wechat = yssxUser.WeChat;
            userMallOrders.RegisterTime = yssxUser.CreateTime;

            var select = DbContext.FreeSql.Select<YssxMallOrder>().Where(o => o.UserId == input.UserId);
            var totalCount = select.Count();
            var sql = select.Page(input.PageIndex, input.PageSize).OrderByDescending(o => o.PaymentTime).ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<MallOrderDto>(sql);
            userMallOrders.Orders = new PageResponse<MallOrderDto> { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items };
            return new ResponseContext<UserMallOrder>(CommonConstants.SuccessCode, "", userMallOrders);
        }

        /// <summary>
        /// 迁移
        /// </summary>
        /// <returns></returns>
        public async Task MigrateAsync()
        {
            #region sxRtpOrder订单迁移【已结束 2020.05.30】
            //// 将 sxRtpOrder 的订单迁移到商城订单表
            //List<SxRtpOrder> sxRtpOrders = DbContext.FreeSql.Select<SxRtpOrder>().ToList();
            //foreach (var sxRtpOrder in sxRtpOrders)
            //{
            //    if (DbContext.FreeSql.Select<YssxMallOrder>().Where(o => o.Id == sxRtpOrder.Id).Any())
            //        continue;

            //    YssxMallOrder yssxMallOrder = new YssxMallOrder();
            //    yssxMallOrder.Id = sxRtpOrder.Id;
            //    yssxMallOrder.UserId = sxRtpOrder.UserId;
            //    yssxMallOrder.PaymentType = sxRtpOrder.PaymentType;
            //    yssxMallOrder.PaymentTime = sxRtpOrder.PaymentTime;
            //    yssxMallOrder.Source = 1;
            //    yssxMallOrder.OrderTotal = sxRtpOrder.DiscountPrice;
            //    yssxMallOrder.OrderDiscount = sxRtpOrder.OriginalPrice - sxRtpOrder.DiscountPrice;
            //    yssxMallOrder.Status = sxRtpOrder.Status;
            //    yssxMallOrder.TransactionNo = sxRtpOrder.TransactionNo;
            //    yssxMallOrder.AffiliateId = sxRtpOrder.PromoterId;
            //    DbContext.FreeSql.Insert<YssxMallOrder>(yssxMallOrder).ExecuteAffrows();

            //    YssxMallOrderItem yssxMallOrderItem = new YssxMallOrderItem();
            //    yssxMallOrderItem.Id = IdWorker.NextId();
            //    yssxMallOrderItem.OrderId = yssxMallOrder.Id;

            //    if (sxRtpOrder.CaseId == 1)
            //    {
            //        yssxMallOrderItem.ProductId = 1060383171608577;
            //    }
            //    else if (sxRtpOrder.CaseId == 2)
            //    {
            //        yssxMallOrderItem.ProductId = 1060383200968707;
            //    }
            //    else if (sxRtpOrder.CaseId == 3)
            //    {
            //        yssxMallOrderItem.ProductId = 1060383230328837;
            //    }
            //    else
            //    {
            //        yssxMallOrderItem.ProductId = 1060383259688967;
            //    }
            //    yssxMallOrderItem.Quantity = 1;
            //    yssxMallOrderItem.UnitPrice = sxRtpOrder.DiscountPrice;
            //    yssxMallOrderItem.DiscountAmount = sxRtpOrder.OriginalPrice - sxRtpOrder.DiscountPrice;
            //    DbContext.FreeSql.Insert<YssxMallOrderItem>(yssxMallOrderItem).ExecuteAffrows();

            //    // 生成试卷 - 已付款
            //    if (yssxMallOrder.Status == 2)
            //    {
            //        var yssxMallProductItems = DbContext.FreeSql.Select<YssxMallProductItem, YssxMallOrderItem>()
            //            .LeftJoin((pi, oi) => pi.ProductId == oi.ProductId)
            //            .Where((pi, oi) => oi.OrderId == yssxMallOrder.Id).ToList();

            //        if (yssxMallProductItems != null && yssxMallProductItems.Count > 0)
            //        {
            //            foreach (var item in yssxMallProductItems)
            //            {
            //                //云实习
            //                if (item.Source == 1)
            //                {
            //                    AutoGenerateTestPaperDto autoGenerateTestPaperDto = new AutoGenerateTestPaperDto()
            //                    {
            //                        Category = item.Category,
            //                        TargetId = item.TargetId,
            //                        UserId = yssxMallOrder.UserId
            //                    };
            //                    var result = await AutoGenerateTestPaper(autoGenerateTestPaperDto);
            //                }
            //            }
            //        }
            //    }
            //}
            #endregion

            #region 同步实训订单 [圆梦造纸] 生成试卷【已结束 2020.06.12】

            //var dtNow = DateTime.Now;
            //long rTargetId = 1065167840477187;
            //long rIgonorUserId = 1056011835511020;

            //var rCaseInfo = DbContext.FreeSql.GetRepository<YssxCaseNew>().Where(x => x.Id == rTargetId && x.IsActive && x.IsDelete == CommonConstants.IsNotDelete).First();
            //var rItemProductId = DbContext.FreeSql.Select<YssxMallProductItem>()
            //    .Where(x => x.TargetId == rTargetId && x.Category == 1 && x.Source == 2 && x.IsDelete == CommonConstants.IsNotDelete).ToList(x => x.ProductId);

            //var rMallOrderData = DbContext.FreeSql.Select<YssxMallOrder, YssxMallOrderItem>()
            //        .InnerJoin((a, b) => a.Id == b.OrderId && b.IsDelete == CommonConstants.IsNotDelete)
            //        .Where((a, b) => a.Status == 2 && rItemProductId.Contains(b.ProductId) && a.UserId > 0 && a.UserId != rIgonorUserId && a.IsDelete == CommonConstants.IsNotDelete)
            //        .GroupBy((a, b) => a.UserId).ToList(x => new {
            //            UserId = x.Key,
            //            PaidCount = x.Count()
            //        });
            //if (rCaseInfo != null && rMallOrderData.Count > 0)
            //{
            //    foreach (var itemOrder in rMallOrderData)
            //    {
            //        var rEndTime = dtNow.AddYears(itemOrder.PaidCount);
            //        var rSxExamPaper = DbContext.FreeSql.GetRepository<ExamPaperNew>().Where(x => x.CaseId == rTargetId && x.UserId == itemOrder.UserId && x.IsDelete == CommonConstants.IsNotDelete).First();
            //        if (rSxExamPaper == null)
            //        {
            //            //生成试卷
            //            var rExamPaper = new ExamPaperNew
            //            {
            //                Id = IdWorker.NextId(),
            //                Name = rCaseInfo.Name,
            //                UserId = itemOrder.UserId,
            //                CaseId = rCaseInfo.Id,
            //                ExamType = ExamType.PracticeTest,
            //                CompetitionType = CompetitionType.IndividualCompetition,
            //                CanShowAnswerBeforeEnd = true,
            //                TotalScore = 0,
            //                PassScore = 0,
            //                TotalQuestionCount = 0,
            //                BeginTime = dtNow,
            //                EndTime = rEndTime,
            //                Sort = 1,
            //                IsRelease = true,
            //                Status = ExamStatus.Wait,
            //                ExamSourceType = ExamSourceType.Order,
            //                CreateBy = itemOrder.UserId,
            //                CreateTime = dtNow
            //            };
            //            //试卷题目
            //            var rTopicList = DbContext.FreeSql.Select<YssxTopicNew>().Where(x => x.CaseId == rCaseInfo.Id && x.ParentId == 0 && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            //            if (rTopicList.Count > 0)
            //            {
            //                var rTotalScore = rTopicList.Sum(x => x.Score);
            //                rExamPaper.TotalScore = rTotalScore;
            //                rExamPaper.PassScore = rTotalScore * (decimal)0.6;
            //                rExamPaper.TotalQuestionCount = rTopicList.Count;
            //            }
            //            var result = await DbContext.FreeSql.Insert(rExamPaper).ExecuteAffrowsAsync() > 0;
            //        }
            //    }
            //}
            #endregion

        }

        ///// <summary>
        ///// 自动生成试卷 - 迁移云实习
        ///// </summary>
        ///// <returns></returns>
        //private async Task<string> AutoGenerateTestPaper(AutoGenerateTestPaperDto input)
        //{
        //    var api = AppSettingConfig.GetSection("yssc_api");
        //    HttpClient client = new HttpClient();
        //    var requestContent = new StringContent(JsonConvert.SerializeObject(input));
        //    requestContent.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
        //    var response = await client.PostAsync($"{api}yssxc/Onboarding/AutoGenerateTestPaper", requestContent);
        //    response.EnsureSuccessStatusCode();
        //    return await response.Content.ReadAsStringAsync();
        //}

        /// <summary>
        /// 查询指定订单
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public async Task<ResponseContext<MallOrderDto>> GetUserOrder(long orderNo)
        {
            YssxMallOrder yssxMallOrder = await DbContext.FreeSql.Select<YssxMallOrder>().Where(o => o.Id == orderNo).FirstAsync();
            if (yssxMallOrder == null)
                return new ResponseContext<MallOrderDto>(CommonConstants.ErrorCode, "该订单不存在！", null);
            MallOrderDto mallOrderDto = new MallOrderDto();
            mallOrderDto = yssxMallOrder.MapTo<MallOrderDto>();
            return new ResponseContext<MallOrderDto>(CommonConstants.SuccessCode, "", mallOrderDto);
        }

        /// <summary>
        /// 查询订单列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResponse<MallOrderDto>> GetOrderList(GetMallOrderDto input)
        {
            var dtNow = DateTime.Now;
            var select = DbContext.FreeSql.Select<YssxMallOrder, YssxUser>()
                .LeftJoin((o, u) => o.UserId == u.Id && u.IsDelete == CommonConstants.IsNotDelete)
                .Where((o, u) => o.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(input.UserId.HasValue && input.UserId > 0, (o, u) => o.UserId == input.UserId && o.Status == 2)
                .WhereIf(!string.IsNullOrEmpty(input.OrderNo), (o, u) => o.Id == long.Parse(input.OrderNo))
                .WhereIf(!string.IsNullOrEmpty(input.MobilePhone), (o, u) => u.MobilePhone.Contains(input.MobilePhone))
                .WhereIf(!string.IsNullOrEmpty(input.RealName), (o, u) => u.RealName.Contains(input.RealName))
                .WhereIf(input.Status == 0, (o, u) => o.Status == input.Status && dtNow <= o.CreateTime.AddMinutes(30))
                .WhereIf(input.Status == 3, (o, u) => o.Status == 0 && dtNow > o.CreateTime.AddMinutes(30))
                .WhereIf(input.Status > 0 && input.Status != 3, (o, u) => o.Status == input.Status)
                .WhereIf(input.PaymentTimeBegin.HasValue, (o, u) => o.PaymentTime >= input.PaymentTimeBegin)
                .WhereIf(input.PaymentTimeEnd.HasValue, (o, u) => o.PaymentTime <= input.PaymentTimeEnd);
            var totalCount = select.Count();
            var sql = select.Page(input.PageIndex, input.PageSize).OrderByDescending((o, u) => o.PaymentTime).ToSql("a.*,b.MobilePhone,b.RealName,b.WeChat");
            var items = await DbContext.FreeSql.Ado.QueryAsync<MallOrderDto>(sql);
            //查询订单明细
            foreach (var itemOrder in items)
            {
                itemOrder.ItemDetail = DbContext.FreeSql.Select<YssxMallOrderItem, YssxMallProduct>()
                    .InnerJoin((a, b) => a.ProductId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                    .Where((a, b) => a.OrderId == itemOrder.Id && a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy((a, b) => a.CreateTime)
                    .ToList((a, b) => new MallOrderItemDto
                    {
                        Id = a.ProductId,
                        Name = b.Name,
                        Quantity = a.Quantity,
                        UnitPrice = a.UnitPrice,
                        DiscountAmount = a.DiscountAmount
                    });
                itemOrder.ProductName = string.Join("、", itemOrder.ItemDetail.Select(x => x.Name));
            }

            return new PageResponse<MallOrderDto>() { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items };
        }

        /// <summary>
        /// 统计
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<MallOrderStatsDto>> GetStats()
        {
            MallOrderStatsDto mallOrderStatsDto = new MallOrderStatsDto();
            List<YssxMallOrder> yssxMallOrders = DbContext.FreeSql.Select<YssxMallOrder>().Where(o => o.PaymentTime >= DateTime.Now.Date).Where(o => o.PaymentTime <= DateTime.Now).Where(o => o.Status == 2).ToList();
            mallOrderStatsDto.TodayAmount = yssxMallOrders.Sum(o => o.OrderTotal);
            mallOrderStatsDto.TodayCount = yssxMallOrders.Count();
            mallOrderStatsDto.TotalAmount = await DbContext.FreeSql.Select<YssxMallOrder>().Where(o => o.Status == 2).SumAsync(o => o.OrderTotal);
            return new ResponseContext<MallOrderStatsDto>(CommonConstants.SuccessCode, "", mallOrderStatsDto);
        }
        #endregion

        #region APP

        /// <summary>
        /// 查询用户订单列表 - APP
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<GetUserOrderListAppDto>> GetUserOrderListApp(GetUserOrderListAppRequest model, long currentUserId)
        {
            var dtNow = DateTime.Now;
            long totalCount = 0;
            var selectData = DbContext.FreeSql.Select<YssxMallOrderItem, YssxMallOrder, YssxMallProduct>()
                .InnerJoin((a, b, c) => a.OrderId == b.Id && b.IsDelete == CommonConstants.IsNotDelete && a.ProductId == c.Id)
                .Where((a, b, c) => b.UserId == currentUserId && a.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.SearchName), (a, b, c) => b.Id == long.Parse(model.SearchName) || c.Name.Contains(model.SearchName))
                .WhereIf(model.Status == 0, (a, b, c) => b.Status == model.Status && dtNow <= b.CreateTime.AddMinutes(30))
                .WhereIf(model.Status == 3, (a, b, c) => b.Status == 0 && dtNow > b.CreateTime.AddMinutes(30))
                .WhereIf(model.Status > 0 && model.Status != 3, (a, b, c) => b.Status == model.Status)
                .OrderByDescending((a, b, c) => b.CreateTime)
                .Count(out totalCount)
                .Page(model.PageIndex, model.PageSize)
                .ToList((a, b, c) => new GetUserOrderListAppDto
                {
                    OrderId = a.OrderId,
                    ProductId = a.ProductId,
                    Name = c.Name,
                    OrderTotal = b.OrderTotal,
                    OrderDiscount = b.OrderDiscount,
                    Status = b.Status,
                    CreateTime = b.CreateTime
                });

            return new PageResponse<GetUserOrderListAppDto>() { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
        }

        /// <summary>
        /// 查询指定订单 - APP
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<MallOrderDto>> GetOrderByIdApp(long id, long currentUserId)
        {
            return await Task.Run(() =>
            {
                YssxMallOrder rMallOrder = DbContext.FreeSql.Select<YssxMallOrder>().Where(x => x.Id == id && x.UserId == currentUserId).First();
                if (rMallOrder == null)
                    return new ResponseContext<MallOrderDto>(CommonConstants.ErrorCode, "该订单不存在！", null);
                //订单
                MallOrderDto resData = new MallOrderDto();
                resData = rMallOrder.MapTo<MallOrderDto>();
                //订单明细
                resData.ItemDetail = DbContext.FreeSql.Select<YssxMallOrderItem, YssxMallProduct>()
                    .InnerJoin((a, b) => a.ProductId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                    .Where((a, b) => a.OrderId == rMallOrder.Id && a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy((a, b) => a.CreateTime)
                    .ToList((a, b) => new MallOrderItemDto
                    {
                        Id = a.ProductId,
                        Name = b.Name,
                        Quantity = a.Quantity,
                        UnitPrice = a.UnitPrice,
                        DiscountAmount = a.DiscountAmount,
                        Img = b.Img
                    });
                if (resData.ItemDetail.Count > 0)
                {
                    resData.ProductName = resData.ItemDetail[0].Name;
                    resData.ProductImg = resData.ItemDetail[0].Img;
                }
                return new ResponseContext<MallOrderDto> { Code = CommonConstants.SuccessCode, Data = resData };
            });
        }


        #endregion

        #region 手动配置记录
        /// <summary>
        /// 获取手动配置记录列表（分页）
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<FreeTestPaperDto>> GetFreeTestPaper(GetFreeTestPaperRequest model)
        {
            long totalCount = 0;
            var selectData = await DbContext.FreeSql.Select<YssxFreeTestPaper, YssxUser, YssxbkUser>()
                .InnerJoin((a, b, c) => a.UserId == b.Id && a.IsDelete == CommonConstants.IsNotDelete)
                .InnerJoin((a, b, c) => a.CreateBy == c.Id && a.IsDelete == CommonConstants.IsNotDelete)
                .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b, c) => a.TargetName.Contains(model.Name) || b.RealName.Contains(model.Name) || b.MobilePhone == model.Name || c.UserName == model.Name)
                .WhereIf(model.Source > 0, (a, b, c) => a.Source == model.Source)
                .Count(out totalCount)
                .Page(model.PageIndex, model.PageSize)
                .OrderByDescending((a, b, c) => a.CreateTime)
                .ToListAsync((a, b, c) => new FreeTestPaperDto
                {
                    Id = a.Id,
                    UserId = a.UserId,
                    RealName = b.RealName,
                    Phone = b.MobilePhone,
                    TargetId = a.TargetId,
                    TargetName = a.TargetName,
                    CreateBy = a.CreateBy,
                    CreateByName = c.UserName,
                    CreateTime = a.CreateTime
                });

            return new PageResponse<FreeTestPaperDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
        }
        #endregion

    }
}
