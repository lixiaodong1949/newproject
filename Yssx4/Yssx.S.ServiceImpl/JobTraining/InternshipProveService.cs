﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.IServices;
using Yssx.S.Poco;
using Yssx.S.Pocos;
using Yssx.S.Pocos.Exam;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 实习证书
    /// 注解：接口里面统一做类型值转换中文名称，同时存储类型和类型名称值，方便数据追溯和统一管理
    /// </summary>
    public class InternshipProveService : IInternshipProveService
    {
        #region 实习证书
        /// <summary>
        /// 获取生成实习证书的资料
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<InternshipProveInfoDto>> GetInternshipProveInfo(long gradeId, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                #region 验证
                //用户
                var rUserInfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == currentUserId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rUserInfo == null)
                    return new ResponseContext<InternshipProveInfoDto> { Code = CommonConstants.ErrorCode, Msg = "您的用户数据异常，请联系平台客服" };
                if (string.IsNullOrEmpty(rUserInfo.RealName))
                    return new ResponseContext<InternshipProveInfoDto> { Code = CommonConstants.ErrorCode, Msg = "请在个人资料中补充您的真实姓名" };
                //作答记录
                var rGradeInfo = DbContext.FreeSql.Select<ExamStudentGrade>().Where(m => m.Id == gradeId && m.UserId == currentUserId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rGradeInfo == null)
                    return new ResponseContext<InternshipProveInfoDto> { Code = CommonConstants.ErrorCode, Msg = "找不到作答记录" };
                if (!rGradeInfo.SubmitTime.HasValue)
                    return new ResponseContext<InternshipProveInfoDto> { Code = CommonConstants.ErrorCode, Msg = "通关记录的交卷时间异常，请联系平台客服" };
                var rQualifiedScore = rGradeInfo.ExamPaperScore * (decimal)0.6;
                if (rGradeInfo.Score < rQualifiedScore)
                    return new ResponseContext<InternshipProveInfoDto> { Code = CommonConstants.ErrorCode, Msg = "sorry! 试卷得分低于可生成实习证书的最低标准" };
                //试卷
                var rExamPaperInfo = DbContext.FreeSql.Select<ExamPaper>().Where(m => m.Id == rGradeInfo.ExamId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rExamPaperInfo == null)
                    return new ResponseContext<InternshipProveInfoDto> { Code = CommonConstants.ErrorCode, Msg = "试卷数据异常，请联系平台客服" };
                //学校
                var rSchoolInfo = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.Id == rGradeInfo.TenantId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rSchoolInfo == null)
                    return new ResponseContext<InternshipProveInfoDto> { Code = CommonConstants.ErrorCode, Msg = "学校数据异常，请联系平台客服" };
                //企业
                var rCaseInfo = DbContext.FreeSql.Select<YssxCase>().Where(m => m.Id == rExamPaperInfo.CaseId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rCaseInfo == null)
                    return new ResponseContext<InternshipProveInfoDto> { Code = CommonConstants.ErrorCode, Msg = "企业数据异常，请联系平台客服" };
                if (rCaseInfo.SxType < 0 || rCaseInfo.SxType > 3)
                    return new ResponseContext<InternshipProveInfoDto> { Code = CommonConstants.ErrorCode, Msg = "企业实训类型数据异常，请联系平台客服" };
                //企业-所属行业
                var rIndustryInfo = DbContext.FreeSql.Select<YssxIndustry>().Where(m => m.Id == rCaseInfo.Industry && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rIndustryInfo == null)
                    return new ResponseContext<InternshipProveInfoDto> { Code = CommonConstants.ErrorCode, Msg = "企业所属行业异常，请联系平台客服" };
                #endregion

                #region 处理数据
                InternshipProveInfoDto resData = new InternshipProveInfoDto();
                resData.SchoolName = rSchoolInfo.Name;
                resData.UserName = rUserInfo.RealName;
                resData.SubmitTime = rGradeInfo.SubmitTime.Value;
                resData.CaseName = rCaseInfo.Name;
                resData.IssueTime = dtNow;
                resData.SxTypeName = rCaseInfo.SxType == 1 ? "出纳实训" : (rCaseInfo.SxType == 2 ? "大数据实训" : (rCaseInfo.SxType == 3 ? "业财税实训" : "会计实训"));
                resData.IndustryName = rIndustryInfo.Name;
                resData.Score = rGradeInfo.Score;
                resData.ExamPaperScore = rGradeInfo.ExamPaperScore;
                //生成证书编号CreateTime
                var dtNowStr = dtNow.ToString("yyyyMMdd");
                Random r = new Random();
                var rFourNumber = r.Next(1000, 10000).ToString();
                resData.SN = "SX" + dtNowStr + rFourNumber;
                #endregion

                return new ResponseContext<InternshipProveInfoDto> { Code = CommonConstants.SuccessCode, Msg = "成功", Data = resData };
            });
        }

        /// <summary>
        /// 生成实习证书
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddInternshipProve(AddInternshipProveDto model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var dtNow = DateTime.Now;
                #region 验证
                if (model.GradeId == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "通关记录ID错误" };
                if (string.IsNullOrEmpty(model.SN))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "证书编号不能为空" };
                if (string.IsNullOrEmpty(model.Url))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "证书地址不能为空" };
                //用户
                var rUserInfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == currentUserId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rUserInfo == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "您的用户数据异常，请联系平台客服" };
                if (string.IsNullOrEmpty(rUserInfo.RealName))
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请在个人资料中补充您的真实姓名" };
                //作答记录
                var rGradeInfo = DbContext.FreeSql.Select<ExamStudentGrade>().Where(m => m.Id == model.GradeId && m.UserId == currentUserId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rGradeInfo == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "找不到作答记录" };
                if (!rGradeInfo.SubmitTime.HasValue)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "通关记录的交卷时间异常，请联系平台客服!" };
                var rQualifiedScore = rGradeInfo.ExamPaperScore * (decimal)0.6;
                if (rGradeInfo.Score < rQualifiedScore)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "sorry! 试卷得分低于可生成实习证书的最低标准" };
                //试卷
                var rExamPaperInfo = DbContext.FreeSql.Select<ExamPaper>().Where(m => m.Id == rGradeInfo.ExamId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rExamPaperInfo == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "试卷数据异常，请联系平台客服!" };
                //学校
                var rSchoolInfo = DbContext.FreeSql.Select<YssxSchool>().Where(m => m.Id == rGradeInfo.TenantId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rSchoolInfo == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "学校数据异常，请联系平台客服!" };
                //企业
                var rCaseInfo = DbContext.FreeSql.Select<YssxCase>().Where(m => m.Id == rExamPaperInfo.CaseId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rCaseInfo == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "企业数据异常，请联系平台客服!" };
                if (rCaseInfo.SxType < 0 || rCaseInfo.SxType > 3)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "企业实训类型数据异常，请联系平台客服" };
                //企业-所属行业
                var rIndustryInfo = DbContext.FreeSql.Select<YssxIndustry>().Where(m => m.Id == rCaseInfo.Industry && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rIndustryInfo == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "企业所属行业异常，请联系平台客服!" };
                #endregion

                #region 处理数据
                YssxInternshipProve rInternshipProve = new YssxInternshipProve();
                rInternshipProve.Id = IdWorker.NextId();
                rInternshipProve.GradeId = model.GradeId;
                rInternshipProve.UserId = rGradeInfo.UserId;
                rInternshipProve.UserName = rUserInfo.RealName;
                rInternshipProve.SN = model.SN;
                rInternshipProve.Url = model.Url;
                rInternshipProve.SubmitTime = rGradeInfo.SubmitTime.Value;
                rInternshipProve.TenantId = rGradeInfo.TenantId;
                rInternshipProve.SchoolName = rSchoolInfo.Name;
                rInternshipProve.CaseId = rExamPaperInfo.CaseId;
                rInternshipProve.CaseName = rCaseInfo.Name;
                rInternshipProve.SxType = rCaseInfo.SxType;
                rInternshipProve.SxTypeName = rCaseInfo.SxType == 1 ? "出纳实训" : (rCaseInfo.SxType == 2 ? "大数据实训" : (rCaseInfo.SxType == 3 ? "业财税实训" : "会计实训"));
                rInternshipProve.Industry = rCaseInfo.Industry;
                rInternshipProve.IndustryName = rIndustryInfo.Name;
                rInternshipProve.Score = rGradeInfo.Score;
                rInternshipProve.ExamPaperScore = rGradeInfo.ExamPaperScore;
                //级别
                var rGoodScore = rGradeInfo.ExamPaperScore * (decimal)0.7;
                var rExcellentScore = rGradeInfo.ExamPaperScore * (decimal)0.8;
                rInternshipProve.Level = rGradeInfo.Score > rExcellentScore ? 2 : (rGradeInfo.Score >= rGoodScore && rGradeInfo.Score < rExcellentScore ? 1 : 0);
                rInternshipProve.LevelName = rInternshipProve.Level == 2 ? "优秀" : (rInternshipProve.Level == 2 ? "良好" : "合格");
                rInternshipProve.CreateBy = currentUserId;
                rInternshipProve.CreateTime = dtNow;
                //旧证书
                var rOldInternshipProveData = DbContext.FreeSql.GetRepository<YssxInternshipProve>()
                    .Where(x => x.GradeId == model.GradeId && x.IsDelete == CommonConstants.IsNotDelete).ToList(m => new YssxInternshipProve
                    {
                        Id = m.Id,
                        IsDelete = CommonConstants.IsDelete,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    });
                #endregion

                bool state = false;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //新证书
                    var rYssxInternshipProve = DbContext.FreeSql.GetRepository<YssxInternshipProve>().Insert(rInternshipProve);
                    //旧证书 - 删除
                    if (rOldInternshipProveData.Count > 0)
                        DbContext.FreeSql.Update<YssxInternshipProve>().SetSource(rOldInternshipProveData).UpdateColumns(a => new { a.IsDelete, a.UpdateBy, a.UpdateTime }).ExecuteAffrows();

                    state = rYssxInternshipProve != null;
                    uow.Commit();
                }

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }

        /// <summary>
        /// 查询证书列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<InternshipProveListDto>> GetInternshipProveList(InternshipProveListRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetGuidRepository<YssxInternshipProve>().Select
                    .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.UserId == currentUserId)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), a => a.CaseName.Contains(model.Name) || a.SN.Contains(model.Name))
                    .WhereIf(model.SxType > -1, a => a.SxType == model.SxType)
                    .WhereIf(model.Industry > 0, a => a.Industry == model.Industry)
                    .OrderByDescending(a => a.CreateTime)
                    .ToList(a => new InternshipProveListDto
                    {
                        SN = a.SN,
                        Url = a.Url,
                        SchoolName = a.SchoolName,
                        UserName = a.UserName,
                        SubmitTime = a.SubmitTime,
                        CaseName = a.CaseName,
                        SxTypeName = a.SxTypeName,
                        IndustryName = a.IndustryName,
                        Score = a.Score,
                        ExamPaperScore = a.ExamPaperScore,
                        IssueTime = a.CreateTime,
                        LevelName = a.LevelName
                    });

                var totalCount = selectData.Count();
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();

                return new PageResponse<InternshipProveListDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 实习证书查询
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<InternshipProveListDto>> GetInternshipProveBySN(string sn)
        {
            return await Task.Run(() =>
            {
                var rUpperSN = sn.ToUpper();
                var rInternshipInfo = DbContext.FreeSql.Select<YssxInternshipProve>().Where(m => m.SN == rUpperSN && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rInternshipInfo == null)
                    return new ResponseContext<InternshipProveListDto> { Code = CommonConstants.SuccessCode, Msg = "没有找到这个实习证书，请核对证书编号" };

                InternshipProveListDto resData = new InternshipProveListDto();
                resData.UserName = rInternshipInfo.UserName;
                resData.SN = rInternshipInfo.SN;
                resData.SchoolName = rInternshipInfo.SchoolName;
                resData.CaseName = rInternshipInfo.CaseName;
                resData.SxTypeName = rInternshipInfo.SxTypeName;
                resData.IndustryName = rInternshipInfo.IndustryName;
                resData.Score = rInternshipInfo.Score;
                resData.ExamPaperScore = rInternshipInfo.ExamPaperScore;
                resData.SubmitTime = rInternshipInfo.SubmitTime;
                resData.IssueTime = rInternshipInfo.CreateTime;
                resData.LevelName = rInternshipInfo.LevelName;

                return new ResponseContext<InternshipProveListDto> { Code = CommonConstants.SuccessCode, Msg = "成功", Data = resData };
            });
        }
        #endregion

        #region 实训列表
        /// <summary>
        /// 实训列表 - 岗位实训
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<GetPaidJobInternshipListDto>> GetPaidJobInternshipList(GetPaidJobInternshipListRequest model, long currentUserId)
        {
            #region 

            //var select = DbContext.FreeSql.GetRepository<YssxCase>().Select.From<ExamPaper, YssxIndustry>((a, b, c) =>
            //    a.InnerJoin(aa => aa.Id == b.CaseId && b.IsDelete == CommonConstants.IsNotDelete)
            //    .InnerJoin(aa => aa.Industry == c.Id && b.IsDelete == CommonConstants.IsNotDelete))
            //    .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && b.UserId == currentUserId && b.ExamSourceType == ExamSourceType.UserOrder)
            //    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b, c) => a.Name.Contains(model.Name))
            //    .WhereIf(model.CaseType.HasValue, (a, b, c) => a.SxType == model.CaseType.Value)
            //    .OrderByDescending((a, b, c) => b.CreateTime);

            //var totalCount = select.Count();
            //var selectData = select.Page(model.PageIndex, model.PageSize).ToList((a, b, c) => new GetPaidJobInternshipListDto
            //{
            //    CaseId = a.Id,
            //    ExamId = b.Id,
            //    GradeId = 0,
            //    Img = a.BGFileUrl,
            //    CaseName = a.Name,
            //    ShortDesc = a.SxInfo,
            //    BeginTime = b.BeginTime,
            //    EndTime = b.EndTime,
            //    IndustryId = c.Id,
            //    IndustryName = c.Name,
            //    ExamCount = 0,
            //    CaseType = a.SxType,
            //});
            ////明细数据
            //foreach (var item in selectData)
            //{
            //    //作答记录
            //    var rGradeEntity = DbContext.FreeSql.GetGuidRepository<ExamStudentGrade>()
            //        .Where(x => x.ExamId == item.ExamId && x.Status != StudentExamStatus.End && x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
            //        .OrderByDescending(x => x.CreateTime).First();
            //    item.GradeId = rGradeEntity == null ? 0 : rGradeEntity.Id;
            //    //交卷次数
            //    var rExamCount = DbContext.FreeSql.GetGuidRepository<ExamStudentGrade>()
            //        .Where(x => x.ExamId == item.ExamId && x.Status == StudentExamStatus.End && x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).Count();
            //    item.ExamCount = Convert.ToInt32(rExamCount);
            //}

            //return new PageResponse<GetPaidJobInternshipListDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };

            #endregion

            var result = new PageResponse<GetPaidJobInternshipListDto>();

            var totalCount = await DbContext.FreeSql.GetRepository<ExamPaper>().Select.From<YssxCase, YssxIndustry>((a, b, c) =>
                  a.InnerJoin(x => x.CaseId == b.Id)
                  .LeftJoin(x => b.Industry == c.Id))
                .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && a.UserId == currentUserId && a.ExamSourceType == ExamSourceType.UserOrder)
                .WhereIf(model.CaseType.HasValue, (a, b, c) => b.SxType == model.CaseType.Value)
                .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b, c) => a.Name.Contains(model.Name))
                .Distinct().ToListAsync((a, b, c) => a.CaseId);

            var examPaper = await DbContext.FreeSql.GetRepository<ExamPaper>().Select
                .From<YssxCase, ExamStudentLastGrade, ExamStudentGrade, ExamStudentGrade, ExamStudentGradeDetail, YssxIndustry>((a, b, c, d, e, f, g) =>
                  a.InnerJoin(aa => aa.CaseId == b.Id)
                  .LeftJoin(aa => b.Industry == g.Id)
                  .LeftJoin(aa => aa.Id == c.ExamId && c.UserId == currentUserId)
                  .LeftJoin(aa => c.GradeId == d.Id)
                  .LeftJoin(aa => d.Id == f.GradeId && f.QuestionId == f.ParentQuestionId)
                  .LeftJoin(aa => aa.Id == e.ExamId && e.UserId == currentUserId && e.Status == StudentExamStatus.End)
                )
                .Where((a, b, c, d, e, f, g) => a.IsDelete == CommonConstants.IsNotDelete && a.UserId == currentUserId && a.ExamSourceType == ExamSourceType.UserOrder)
                .WhereIf(model.CaseType.HasValue, (a, b, c, d, e, f, g) => b.SxType == model.CaseType.Value)
                .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b, c, d, e, f, g) => a.Name.Contains(model.Name))
                .GroupBy((a, b, c, d, e, f, g) => new
                {
                    a.Id,
                    a.Name,
                    a.CaseId,
                    a.Sort,
                    a.TotalScore,
                    a.TotalQuestionCount,
                    b.CaseYear,
                    b.RollUpType,
                    b.BGFileUrl,
                    b.SxInfo,
                    IndustryName = g.Name,
                    IndustryId = g.Id,
                    c.GradeId,
                    d.Status,
                    a.CreateTime,
                    b.SxType
                })
                .OrderByDescending(a => a.Key.CreateTime)
                .Page(model.PageIndex, model.PageSize)
                .ToListAsync(a => new GetPaidJobInternshipListDto
                {
                    ExamId = a.Key.Id,
                    Name = a.Key.Name,
                    CaseId = a.Key.CaseId,
                    Sort = a.Key.Sort,
                    TotalScore = a.Key.TotalScore,
                    TotalQuestionCount = a.Key.TotalQuestionCount,
                    DoQuestionCount = Convert.ToInt32("count(distinct f.id)"),
                    ExamCount = Convert.ToInt32("count(distinct e.id)"),
                    GradeId = a.Key.GradeId,
                    Status = a.Key.Status,
                    CaseYear = a.Key.CaseYear,
                    Year = a.Key.CreateTime.Year,
                    RollUpType = a.Key.RollUpType,
                    BGFileUrl = a.Key.BGFileUrl,
                    SxInfo = a.Key.SxInfo,
                    IndustryName = a.Key.IndustryName,
                    IndustryId = a.Key.IndustryId,
                    CaseType = a.Key.SxType
                });

            if (examPaper.Any())
            {
                examPaper.AsParallel().ForAll(a =>
                {
                    if (a.GradeId > 0)
                    {
                        if (a.Status == StudentExamStatus.End)//状态为已结束则设置gradeId=0
                        {
                            a.GradeId = 0;
                        }
                    }
                });
            }
            result.PageIndex = model.PageIndex;
            result.PageSize = model.PageSize;
            result.Data = examPaper;
            result.Code = CommonConstants.SuccessCode;
            result.RecordCount = totalCount.Count();

            return result;
        }

        /// <summary>
        /// 查询通关记录
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<JobTrainingEndRecordDto>> GetJobTrainingEndRecord(JobTrainingEndRecordRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetGuidRepository<ExamStudentGrade>().Select
                    .From<ExamPaper, YssxCase, YssxUser, YssxInternshipProve>((a, b, c, d, e) =>
                        a.InnerJoin(aa => aa.ExamId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => b.CaseId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => aa.UserId == d.Id && d.IsDelete == CommonConstants.IsNotDelete)
                        .LeftJoin(aa => aa.Id == e.GradeId && e.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d, e) => a.ExamId == model.ExamId && a.UserId == currentUserId && a.Status == StudentExamStatus.End && a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderByDescending((a, b, c, d, e) => a.CreateTime)
                    .ToList((a, b, c, d, e) => new JobTrainingEndRecordDto
                    {
                        GradeId = a.Id,
                        CaseId = c.Id,
                        ExamId = b.Id,
                        Name = c.Name,
                        SubmitTime = a.SubmitTime.Value,
                        ExamPaperScore = a.ExamPaperScore,
                        Score = a.Score,
                        SN = e.SN,
                        Url = e.Url
                    });

                var totalCount = selectData.Count();
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                return new PageResponse<JobTrainingEndRecordDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }
        #endregion

        #region 生成试卷
        /// <summary>
        /// 购买后自动生成试卷 - 岗位实训
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AutoGenerateTestPaper(AutoGenerateTestPaperDto model)
        {
            DateTime dtNow = DateTime.Now;
            var rEndTime = dtNow.AddYears(1);
            //验证
            if (model.Category != 1)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "分类入参错误");
            if (model.TargetId == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "标的物Id参数错误");
            if (model.UserId == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "目标用户ID参数错误");

            //查找企业
            var rCaseInfo = DbContext.FreeSql.GetRepository<YssxCase>().Where(x => x.Id == model.TargetId && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (rCaseInfo == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "未找到该企业");

            var rAnyExamPaper = DbContext.FreeSql.GetRepository<ExamPaper>().Where(x => x.CaseId == model.TargetId && x.UserId == model.UserId && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (rAnyExamPaper != null)
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "操作成功", Data = true };
            //生成试卷
            var rExamPaper = new ExamPaper
            {
                Id = IdWorker.NextId(),
                Name = rCaseInfo.Name,
                TenantId = 0,
                UserId = model.UserId,
                CaseId = rCaseInfo.Id,
                ExamType = ExamType.PracticeTest,
                CompetitionType = CompetitionType.IndividualCompetition,
                CanShowAnswerBeforeEnd = true,
                TotalScore = 0,
                PassScore = 0,
                TotalQuestionCount = 0,
                BeginTime = dtNow,
                EndTime = rEndTime,
                Sort = 1,
                IsRelease = true,
                Status = ExamStatus.Wait,
                ExamSourceType = ExamSourceType.UserOrder,
                CreateBy = model.UserId,
                CreateTime = dtNow
            };
            //试卷题目
            var rTopicList = DbContext.FreeSql.Select<YssxTopic>().Where(x => x.CaseId == rCaseInfo.Id && x.ParentId == 0 && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            if (rTopicList.Count > 0)
            {
                var rTotalScore = rTopicList.Sum(x => x.Score);
                rExamPaper.TotalScore = rTotalScore;
                rExamPaper.PassScore = rTotalScore * (decimal)0.6;
                rExamPaper.TotalQuestionCount = rTopicList.Count;
            }

            return await Task.Run(() =>
            {
                var state = DbContext.FreeSql.Insert(rExamPaper).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "操作成功", Data = state };
            });
        }
        #endregion

    }
}
