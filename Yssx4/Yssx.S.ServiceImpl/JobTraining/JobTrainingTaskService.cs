﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using System.Linq;
using Org.BouncyCastle.Crypto.Prng;
using Yssx.Framework.Authorizations;
using Yssx.S.Pocos.SceneTraining;
using Newtonsoft.Json;
using Yssx.Framework.Entity;
using Yssx.Framework.AutoMapper;
using Yssx.S.Poco;
using Yssx.S.Pocos.Exam;
using Yssx.Framework.Logger;
using Tas.Common.Utils;
using Yssx.Repository.Extensions;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 岗位实训任务服务
    /// </summary>
    public class JobTrainingTaskService : IJobTrainingTaskService
    {
        #region 发布或修改岗位实训任务
        /// <summary>
        /// 发布或修改岗位实训任务
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> PublishOrUpdateJobTraingingTask(JobTrainingTaskDto dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "对象是空的!" };
            bool state = false;

            if (dto.Id == 0)
            {
                if (dto.TrainStartTime.HasValue && dto.TrainStartTime <= DateTime.Now)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "开始时间须大于当前时间!" };
            }

            if (dto.Id == 0 && dto.TopicList.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "任务套题为空,请选择案例!" };

            if (dto.TrainStartTime.HasValue && dto.TrainEndTime.HasValue && (dto.TrainStartTime > dto.TrainEndTime))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "开始时间须小于结束时间!" };

            if (dto.TrainEndTime.HasValue && (dto.TrainEndTime <= DateTime.Now))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "结束时间须大于当前时间!" };

            //发布任务
            if (dto.Id == 0)
            {
                var oriData = await DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().Where(x => x.TaskName == dto.TaskName
                     && x.IsDelete == CommonConstants.IsNotDelete && x.CreateBy == user.Id).ToListAsync();
                if (oriData != null && oriData.Count > 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "任务名称已存在!" };

                YssxJobTrainingTask entity = new YssxJobTrainingTask
                {
                    Id = IdWorker.NextId(),
                    TaskName = dto.TaskName,
                    ExamType = dto.ExamType,
                    TrainStartTime = dto.TrainStartTime,
                    TrainEndTime = dto.TrainEndTime,
                    TaskPassword = dto.TaskPassword,
                    TrainType = dto.TrainType,
                    GroupCount = dto.GroupCount,
                    AnswerShowType = dto.AnswerShowType,
                    Remark = dto.Remark,
                    IsCanCheckExam = dto.IsCanCheckExam,
                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                    TenantId = user.TenantId,
                    TaskStatus = 0,
                };

                return await Task.Run(() =>
                {
                    #region 初始化数据

                    //任务套题
                    List<YssxJobTrainingTaskTopic> topicList = new List<YssxJobTrainingTaskTopic>();
                    //生成试卷 (一套题生成一张试卷)
                    List<ExamPaper> examPaperList = new List<ExamPaper>();
                    //学生作答信息
                    List<ExamStudentGrade> gradeList = new List<ExamStudentGrade>();
                    //任务班级
                    List<YssxJobTrainingTaskClass> classList = new List<YssxJobTrainingTaskClass>();
                    //学生主作答记录
                    List<ExamStudentGrade> studentGradeList = new List<ExamStudentGrade>();
                    //实训套题列表数量
                    int topicListCount = dto.TopicList.Distinct().Count();

                    //添加岗位实训任务、套题关联 生成试卷
                    if (dto.TopicList != null && dto.TopicList.Count > 0)
                    {
                        var dtoTopicList = dto.TopicList.Distinct().ToList();
                        dtoTopicList.ForEach(x =>
                        {
                            YssxJobTrainingTaskTopic modelTopic = new YssxJobTrainingTaskTopic();
                            modelTopic.JobTrainingId = x.JobTrainingId;
                            modelTopic.JobTrainingName = x.JobTrainingName;

                            if (!topicList.Any(a => a.JobTrainingId == x.JobTrainingId))
                                topicList.Add(modelTopic);
                        });

                        if (topicList != null && topicList.Count > 0)
                        {
                            topicList.ForEach(x =>
                            {
                                x.Id = IdWorker.NextId();
                                x.JobTrainingTaskId = entity.Id;
                                x.CreateBy = user.Id;
                                x.CreateTime = DateTime.Now;
                                x.UpdateBy = user.Id;
                                x.UpdateTime = DateTime.Now;
                            });

                            //岗位实训Ids
                            var jobTrainingIds = topicList.Select(a => a.JobTrainingId).ToList();
                            List<YssxTopic> topics = DbContext.FreeSql.GetRepository<YssxTopic>().Where(x => jobTrainingIds.Contains(x.CaseId)
                                && x.ParentId == 0 && x.IsDelete == CommonConstants.IsNotDelete).ToList();

                            topicList.ForEach(x =>
                            {
                                //总题数
                                int TotalQuestionCount = topics.Where(a => a.CaseId == x.JobTrainingId).Count();
                                //总分数
                                decimal totalScore = topics.Where(a => a.CaseId == x.JobTrainingId).Sum(a => a.Score);

                                TimeSpan ts = dto.TrainEndTime.Value.Subtract(dto.TrainStartTime.Value);
                                int totalMinutes = Convert.ToInt32(ts.TotalMinutes);

                                ExamPaper examPaper = new ExamPaper
                                {
                                    Id = IdWorker.NextId(),
                                    Name = string.Format("{0}-{1}", entity.TaskName, x.JobTrainingName),
                                    CaseId = x.JobTrainingId,
                                    CanShowAnswerBeforeEnd = dto.AnswerShowType == 0 ? false : true,
                                    TotalScore = totalScore,
                                    PassScore = 0,
                                    TotalQuestionCount = TotalQuestionCount,
                                    TotalMinutes = totalMinutes,
                                    BeginTime = dto.TrainStartTime.Value,
                                    EndTime = dto.TrainEndTime,
                                    ReleaseTime = entity.CreateTime,
                                    IsRelease = true,
                                    Status = (int)ExamStatus.Wait,
                                    TaskId = entity.Id,
                                    CreateBy = user.Id,
                                    CreateTime = DateTime.Now,
                                    UpdateBy = user.Id,
                                    UpdateTime = DateTime.Now,
                                    TenantId = user.TenantId,
                                    ExamSourceType = ExamSourceType.TrainingTask,
                                };
                                //自主练习 一人多岗
                                if (dto.TrainType == 0)
                                    examPaper.ExamType = ExamType.PracticeTest;

                                if (!examPaperList.Any(a => a.TaskId == entity.Id && a.CaseId == x.JobTrainingId))
                                    examPaperList.Add(examPaper);
                            });
                        }
                    }

                    //验证实训套题、试卷数量是否一致
                    if ((topicListCount != topicList.Count) || topicListCount != examPaperList.Count)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "生成试卷失败!" };

                    //添加岗位实训任务、班级关联
                    if (dto.ClassList != null && dto.ClassList.Count > 0)
                    {
                        classList = dto.ClassList.MapTo<List<YssxJobTrainingTaskClass>>();
                        if (classList != null && classList.Count > 0)
                        {
                            classList.ForEach(x =>
                            {
                                x.Id = IdWorker.NextId();
                                x.JobTrainingTaskId = entity.Id;
                                x.CreateBy = user.Id;
                                x.CreateTime = DateTime.Now;
                                x.UpdateBy = user.Id;
                                x.UpdateTime = DateTime.Now;
                            });
                        }

                        //班级Ids
                        var classIds = dto.ClassList.Select(x => x.ClassId).ToList();

                        //生成学生主作答记录
                        studentGradeList = CreateStudentGrade(examPaperList, classIds, entity.Id, user);
                    }

                    #endregion

                    #region 新增数据
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        try
                        {
                            //岗位实训任务任务
                            var task = uow.GetRepository<YssxJobTrainingTask>().Insert(entity);
                            //岗位实训任务套题
                            if (topicList.Count > 0)
                                uow.GetRepository<YssxJobTrainingTaskTopic>().Insert(topicList);
                            //保存试卷
                            if (examPaperList.Count > 0)
                                uow.GetRepository<ExamPaper>().Insert(examPaperList);
                            //保存学生作答信息
                            if (gradeList.Count > 0)
                                uow.GetRepository<ExamStudentGrade>().Insert(gradeList);
                            //保存岗位实训任务班级
                            if (classList.Count > 0)
                                uow.GetRepository<YssxJobTrainingTaskClass>().Insert(classList);
                            //保存学生主作答记录
                            if (studentGradeList.Count > 0)
                                uow.GetRepository<ExamStudentGrade>().Insert(studentGradeList);

                            state = task != null;

                            uow.Commit();
                        }
                        catch (Exception)
                        {
                            uow.Rollback();
                        }
                    }
                    #endregion

                    return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
                });
            }
            //修改任务
            else
            {
                return await Task.Run(() =>
                {
                    #region 初始化数据

                    //var oriData = DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().Where(x => x.Id != dto.Id && x.TaskName == dto.TaskName
                    // && x.IsDelete == CommonConstants.IsNotDelete && x.CreateBy == user.Id).ToList();
                    //if (oriData != null && oriData.Count > 0)
                    //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "任务名称已存在!" };

                    YssxJobTrainingTask entity = DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().Where(x => x.Id == dto.Id).First();
                    if (entity == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "没有该数据源!" };

                    //任务试卷列表
                    List<ExamPaper> examPapers = new List<ExamPaper>();

                    //如果更新开始时间、结束时间 需要更新试卷、主作答记录的开始时间、结束时间
                    if (entity.TrainStartTime != dto.TrainStartTime || entity.TrainEndTime != dto.TrainEndTime)
                    {
                        examPapers = DbContext.FreeSql.GetRepository<ExamPaper>().Where(x => x.TaskId == dto.Id).ToList();

                        TimeSpan ts = dto.TrainEndTime.Value.Subtract(dto.TrainStartTime.Value);
                        int totalMinutes = Convert.ToInt32(ts.TotalMinutes);

                        examPapers.ForEach(x =>
                        {
                            //删除试卷缓存
                            RedisHelper.Del($"{CommonConstants.Cache_GetExamById}{x.Id}");

                            x.Status = ExamStatus.Started;
                            x.TotalMinutes = totalMinutes;
                            x.BeginTime = dto.TrainStartTime.Value;
                            x.EndTime = dto.TrainEndTime;                            
                            x.UpdateTime = DateTime.Now;
                            x.UpdateBy = user.Id;
                        });
                    }

                    entity.TrainStartTime = dto.TrainStartTime;
                    entity.TrainEndTime = dto.TrainEndTime;
                    entity.IsCanCheckExam = dto.IsCanCheckExam;
                    entity.UpdateBy = user.Id;
                    entity.UpdateTime = DateTime.Now;

                    #endregion

                    #region 更新数据
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        try
                        {
                            //更新任务
                            state = uow.GetRepository<YssxJobTrainingTask>().UpdateDiy.SetSource(entity).UpdateColumns(x => new
                            {
                                x.TrainStartTime,
                                x.TrainEndTime,
                                x.IsCanCheckExam,
                                x.UpdateBy,
                                x.UpdateTime
                            }).ExecuteAffrows() > 0;

                            //更新任务试卷
                            if (examPapers.Count > 0)
                                uow.GetRepository<ExamPaper>().UpdateDiy.SetSource(examPapers).UpdateColumns(x => new
                                {
                                    x.Status,
                                    x.TotalMinutes,
                                    x.BeginTime,
                                    x.EndTime,
                                    x.UpdateTime,
                                    x.UpdateBy
                                }).ExecuteAffrows();

                            uow.Commit();
                        }
                        catch (Exception)
                        {
                            uow.Rollback();
                        }
                    }
                    #endregion

                    return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
                });
            }
        }
        #endregion

        #region 生成学生作答记录
        /// <summary>
        /// 生成学生作答记录
        /// </summary>
        /// <param name="examPaperList">试卷列表</param>
        /// <param name="classIds">班级Ids</param>
        /// <param name="jobTrainingTaskId">任务Id</param>
        /// <param name="user"></param>
        /// <returns></returns>
        private List<ExamStudentGrade> CreateStudentGrade(List<ExamPaper> examPaperList, List<long> classIds, long jobTrainingTaskId, UserTicket user)
        {
            List<ExamStudentGrade> list = new List<ExamStudentGrade>();

            if (classIds == null || classIds.Count <= 0)
                return list;

            //根据班级查询学生列表
            List<YssxStudent> studentList = DbContext.FreeSql.Select<YssxStudent>().Where(x => classIds.Contains(x.ClassId) && x.IsDelete == CommonConstants.IsNotDelete).ToList();

            examPaperList.ForEach(x =>
            {
                studentList.ForEach(a =>
                {
                    ExamStudentGrade model = new ExamStudentGrade();

                    model.Id = IdWorker.NextId();
                    model.UserId = a.UserId;
                    model.ExamId = x.Id;
                    model.ExamPaperScore = x.TotalScore;
                    model.LeftSeconds = x.TotalMinutes * 60;
                    model.RecordTime = x.BeginTime;
                    model.GroupId = 0;
                    model.StudentId = CommonConstants.StudentId;
                    model.Status = (int)StudentExamStatus.Wait;
                    model.CreateBy = user.Id;
                    model.CreateTime = DateTime.Now;
                    model.UpdateBy = user.Id;
                    model.UpdateTime = DateTime.Now;
                    model.TenantId = user.TenantId;

                    list.Add(model);
                });
            });

            return list;
        }
        #endregion

        #region 获取教师岗位实训任务列表
        /// <summary>
        /// 获取教师岗位实训任务列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<YssxJobTrainingTaskTeacherViewModel>> GetJobTrainingTaskTeacherList(YssxJobTrainingTaskTeacherQuery query, UserTicket user)
        {
            var result = new PageResponse<YssxJobTrainingTaskTeacherViewModel>();
            if (query == null || user == null)
                return result;

            var select = DbContext.FreeSql.GetRepository<YssxJobTrainingTask>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.CreateBy == user.Id);

            if (!string.IsNullOrEmpty(query.Keyword))
                select.Where(a => a.TaskName.Contains(query.Keyword));
            if (query.TrainType.HasValue)
                select.Where(a => a.TrainType == query.TrainType.Value);
            if (query.ExamType.HasValue)
                select.Where(a => a.ExamType == query.ExamType.Value);
            if (query.TaskStatus.HasValue)
            {
                if (query.TaskStatus == (int)TrainType.Wait)
                    select.Where(a => DateTime.Now < a.TrainStartTime && a.TaskStatus != (int)TrainType.End);
                if (query.TaskStatus == (int)TrainType.Started)
                    select.Where(a => a.TrainStartTime <= DateTime.Now && DateTime.Now < a.TrainEndTime && a.TaskStatus != (int)TrainType.End);
                if (query.TaskStatus == (int)TrainType.End)
                    select.Where(a => (a.TrainStartTime <= DateTime.Now && a.TrainEndTime <= DateTime.Now) || a.TaskStatus == (int)TrainType.End);
            }

            var totalCount = select.ToList().Count();

            var items = select.OrderByDescending(a => a.CreateTime).Page(query.PageIndex, query.PageSize)
                .ToList(x => new YssxJobTrainingTaskTeacherViewModel
                {
                    Id = x.Id,
                    TaskName = x.TaskName,
                    ExamType = x.ExamType,
                    TrainStartTime = x.TrainStartTime,
                    TrainEndTime = x.TrainEndTime,
                    TaskPassword = x.TaskPassword,
                    TrainType = x.TrainType,
                    GroupCount = x.GroupCount,
                    AnswerShowType = x.AnswerShowType,
                    Remark = x.Remark,
                    TaskStatus = x.TaskStatus,
                    IsCanCheckExam = x.IsCanCheckExam
                });

            //任务Ids
            List<long> taskIds = items.Select(x => x.Id).ToList();

            //试卷列表
            List<ExamPaper> examPaperList = DbContext.FreeSql.Select<ExamPaper>().Where(x => taskIds.Contains(x.TaskId)
                  && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            //试卷Ids
            List<long> examIds = examPaperList.Select(a => a.Id).ToList();

            //学生作答主信息列表(综合岗位实训 一人作答全部岗位的习题)
            List<ExamStudentGrade> studentGradeList = DbContext.FreeSql.Select<ExamStudentGrade>().Where(x => examIds.Contains(x.ExamId)
                   && x.IsDelete == CommonConstants.IsNotDelete && x.GroupId == 0).ToList();

            //岗位实训任务套题列表
            var jobTrainingList = await DbContext.FreeSql.Select<YssxJobTrainingTaskTopic>().From<YssxCase>(
                (a, b) => a.InnerJoin(x => x.JobTrainingId == b.Id))
                .Where((a, b) => taskIds.Contains(a.JobTrainingTaskId) && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new
                {
                    JobTrainingTaskId = a.JobTrainingTaskId,
                    JobTrainingId = b.Id,
                    JobTrainingName = b.Name,
                    RollUpType = b.RollUpType,
                    CaseYear = b.CaseYear,
                });

            //岗位实训任务班级列表
            List<YssxJobTrainingTaskClass> classList = DbContext.FreeSql.Select<YssxJobTrainingTaskClass>().Where(x => taskIds.Contains(x.JobTrainingTaskId)
                && x.IsDelete == CommonConstants.IsNotDelete).ToList();

            //班级Ids
            List<long> classIds = classList.Select(x => x.ClassId).Distinct().ToList();
            //学生列表
            List<YssxStudent> studentList = DbContext.FreeSql.Select<YssxStudent>().Where(x => classIds.Contains(x.ClassId)
                 && x.IsDelete == CommonConstants.IsNotDelete).ToList();

            items.ForEach(x =>
            {
                //实训套题列表
                var jobTrainings = jobTrainingList.Where(a => a.JobTrainingTaskId == x.Id).ToList();
                if (jobTrainings != null && jobTrainings.Count > 0)
                {
                    List<YssxJobTrainingViewModel> jobTrainingVw = new List<YssxJobTrainingViewModel>();
                    jobTrainings.ForEach(a => jobTrainingVw.Add(new YssxJobTrainingViewModel
                    {
                        JobTrainingId = a.JobTrainingId,
                        JobTrainingName = a.JobTrainingName,
                        RollUpType = a.RollUpType,
                        CaseYear = a.CaseYear,
                    }));

                    x.TopicList = jobTrainingVw.Distinct().ToList();

                    //查找试卷Id、作答记录Id
                    x.TopicList.ForEach(a =>
                    {
                        //试卷Id
                        a.ExamId = examPaperList.Where(b => b.TaskId == x.Id && b.CaseId == a.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault()?.Id;
                        if (examPaperList.Where(b => b.TaskId == x.Id && b.CaseId == a.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault()?.Status != null)
                        {
                            if (examPaperList.Where(b => b.TaskId == x.Id && b.CaseId == a.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault().Status == ExamStatus.End)
                                a.ExamStatus = 1;
                            else
                                a.ExamStatus = 0;
                        }
                    });
                }

                //实训班级列表
                List<YssxJobTrainingTaskClass> classes = classList.Where(a => a.JobTrainingTaskId == x.Id).ToList();
                if (classes != null && classes.Count > 0)
                {
                    List<YssxClassViewModel> classVw = new List<YssxClassViewModel>();
                    classes.ForEach(a => classVw.Add(new YssxClassViewModel
                    {
                        ClassId = a.ClassId,
                        ClassName = a.ClassName,
                        SchoolId = a.SchoolId,
                        SchoolName = a.SchoolName,
                        CollegeId = a.CollegeId,
                        CollegeName = a.CollegeName,
                        ProfessionId = a.ProfessionId,
                        ProfessionName = a.ProfessionName,
                    }));

                    x.ClassList = classVw;
                }

                //任务班级Ids
                List<long> taskClassIds = x.ClassList.Select(a => a.ClassId).ToList();

                //学生数量
                x.StudentCount = studentList.Where(a => taskClassIds.Contains(a.ClassId)).Count();

                //试卷Ids
                List<long> examPaperIds = examPaperList.Where(a => a.TaskId == x.Id).Select(a => a.Id).ToList();
                //综合岗位实训 一人作答全部岗位的习题
                if (x.TrainType == 0 && studentGradeList.Count > 0)
                {
                    //学生作答列表
                    List<ExamStudentGrade> studentGrades = studentGradeList.Where(a => examPaperIds.Contains(a.ExamId)).ToList();
                    //作答学生Ids
                    List<long> studentUserIds = studentGrades.Select(a => a.UserId).Distinct().ToList();
                    int submittedCount = 0;
                    studentUserIds.ForEach(a =>
                    {
                        //学生交卷的数量=试卷的数量 则任务已交
                        if (studentGrades.Where(b => b.Status == StudentExamStatus.End && b.UserId == a).Count() == examPaperIds.Count)
                            submittedCount++;
                    });
                    //已交卷的学生数量
                    x.SubmittedCount = submittedCount;
                }
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion               

        #region 获取学生岗位实训任务列表
        /// <summary>
        /// 获取学生岗位实训任务列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<PageResponse<YssxJobTrainingTaskStudentViewModel>> GetJobTrainingTaskStudentList(YssxJobTrainingTaskStudentQuery query, UserTicket user)
        {
            var result = new PageResponse<YssxJobTrainingTaskStudentViewModel>();
            if (query == null || user == null)
                return result;

            var select = DbContext.FreeSql.Select<YssxJobTrainingTask>().From<YssxJobTrainingTaskClass, YssxStudent>(
                (a, b, c) =>
                a.InnerJoin(x => x.Id == b.JobTrainingTaskId)
                .InnerJoin(x => b.ClassId == c.ClassId))
                .Where((a, b, c) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete
                    && c.UserId == user.Id).Distinct();

            if (!string.IsNullOrEmpty(query.Keyword))
                select.Where((a, b, c) => a.TaskName.Contains(query.Keyword) || b.ClassName.Contains(query.Keyword));
            if (query.TrainType.HasValue)
                select.Where((a, b, c) => a.TrainType == query.TrainType.Value);
            if (query.ExamType.HasValue)
                select.Where((a, b, c) => a.ExamType == query.ExamType.Value);
            if (query.TaskStatus.HasValue)
            {
                if (query.TaskStatus == (int)TrainType.Wait)
                    select.Where((a, b, c) => DateTime.Now < a.TrainStartTime && a.TaskStatus != (int)TrainType.End);
                if (query.TaskStatus == (int)TrainType.Started)
                    select.Where((a, b, c) => a.TrainStartTime <= DateTime.Now && DateTime.Now < a.TrainEndTime && a.TaskStatus != (int)TrainType.End);
                if (query.TaskStatus == (int)TrainType.End)
                    select.Where((a, b, c) => (a.TrainStartTime <= DateTime.Now && a.TrainEndTime <= DateTime.Now) || a.TaskStatus == (int)TrainType.End);
            }

            var totalCount = select.ToList().Count();

            var items = select.OrderByDescending((a, b, c) => a.UpdateTime).Page(query.PageIndex, query.PageSize)
                .ToList((a, b, c) => new YssxJobTrainingTaskStudentViewModel
                {
                    Id = a.Id,
                    TaskName = a.TaskName,
                    ExamType = a.ExamType,
                    TrainStartTime = a.TrainStartTime,
                    TrainEndTime = a.TrainEndTime,
                    TaskPassword = a.TaskPassword,
                    TrainType = a.TrainType,
                    GroupCount = a.GroupCount,
                    AnswerShowType = a.AnswerShowType,
                    Remark = a.Remark,
                    TaskStatus = a.TaskStatus,
                    IsCanCheckExam = a.IsCanCheckExam
                });
              
            //任务Ids
            List<long> taskIds = items.Select(x => x.Id).ToList();

            //试卷列表
            List<ExamPaper> examPaperList = DbContext.FreeSql.Select<ExamPaper>().Where(x => taskIds.Contains(x.TaskId)
                  && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            //试卷Ids
            List<long> examIds = examPaperList.Select(q => q.Id).ToList();

            //学生作答主信息列表(综合岗位实训 一人作答全部岗位的习题)
            List<ExamStudentGrade> studentGradeList = DbContext.FreeSql.Select<ExamStudentGrade>()
                .Where(x => x.UserId == user.Id && examIds.Contains(x.ExamId) && x.IsDelete == CommonConstants.IsNotDelete && x.GroupId == 0).ToList();

            //学生作答明细列表
            List<ExamStudentGradeDetail> studentGradeDetailList = DbContext.FreeSql.Select<ExamStudentGradeDetail>()
                .Where(a => a.UserId == user.Id && examIds.Contains(a.ExamId) && a.IsDelete == CommonConstants.IsNotDelete && a.QuestionId == a.ParentQuestionId).ToList();

            //岗位实训任务套题列表
            var jobTrainingList = await DbContext.FreeSql.Select<YssxJobTrainingTaskTopic>().From<YssxCase>(
                (a, b) => a.InnerJoin(x => x.JobTrainingId == b.Id))
                .Where((a, b) => taskIds.Contains(a.JobTrainingTaskId) && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new
                {
                    JobTrainingTaskId = a.JobTrainingTaskId,
                    JobTrainingId = b.Id,
                    JobTrainingName = b.Name,
                    RollUpType = b.RollUpType,
                    CaseYear = b.CaseYear,
                });

            items.ForEach(x =>
            {
                if (x.TrainEndTime.HasValue && x.TrainEndTime.HasValue)
                {
                    TimeSpan span = x.TrainEndTime.Value.Subtract(x.TrainStartTime.Value);
                    x.TaskBurningTime = span.Days + "天" + span.Hours + "小时" + span.Minutes + "分钟";
                }

                //实训套题列表
                var jobTrainings = jobTrainingList.Where(a => a.JobTrainingTaskId == x.Id).ToList();
                if (jobTrainings != null && jobTrainings.Count > 0)
                {
                    List<YssxJobTrainingViewModel> jobTrainingVw = new List<YssxJobTrainingViewModel>();
                    jobTrainings.ForEach(a =>
                    {
                        if (examPaperList.Where(b => b.TaskId == x.Id && b.CaseId == a.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault() != null)
                        {
                            YssxJobTrainingViewModel model = new YssxJobTrainingViewModel();
                            model.JobTrainingId = a.JobTrainingId;
                            model.JobTrainingName = a.JobTrainingName;
                            model.RollUpType = a.RollUpType;
                            model.CaseYear = a.CaseYear;
                            model.ExamId = examPaperList.Where(b => b.TaskId == x.Id && b.CaseId == a.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault()?.Id;
                            model.ExamScore = examPaperList.Where(b => b.TaskId == x.Id && b.CaseId == a.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault()?.TotalScore ?? 0;
                            //综合岗位实训 作答记录Id
                            if (x.TrainType == 0 && model.ExamId.HasValue)
                            {
                                model.GradeId = studentGradeList.Where(b => b.ExamId == model.ExamId).FirstOrDefault()?.Id;
                                model.Score = studentGradeList.Where(b => b.ExamId == model.ExamId).FirstOrDefault()?.Score ?? 0;
                                if (studentGradeList.Where(b => b.ExamId == model.ExamId).FirstOrDefault()?.Status != null)
                                    model.StudentExamStatus = (int)studentGradeList.Where(b => b.ExamId == model.ExamId).FirstOrDefault()?.Status;
                            }

                            jobTrainingVw.Add(model);
                        }
                    });

                    //所有案例都已交卷
                    if (jobTrainingVw.Where(a => a.StudentExamStatus == 2).Count() == jobTrainingVw.Count)
                        x.IsAllSubmit = true;

                    x.TopicList = jobTrainingVw.Distinct().ToList();
                }

                //任务状态为已结束 才可以看分数
                if (x.TaskFinalStatus == (int)ExamStatus.End)
                {
                    //试卷Ids
                    List<long> examPaperIds = examPaperList.Where(a => a.TaskId == x.Id).Select(a => a.Id).ToList();

                    //任务下所有试卷的总分
                    x.ExamPaperScore = examPaperList.Where(a => a.TaskId == x.Id).Sum(a => a.TotalScore);
                    //学生所有试卷得分
                    x.Score = studentGradeDetailList.Where(a => examPaperIds.Contains(a.ExamId)).Sum(a => a.Score);
                }
            });

            result.Data = items;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }
        #endregion

        #region 验证岗位实训任务密码
        /// <summary>
        /// 验证岗位实训任务密码
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ValidateJobTraingingTaskPassword(long id, string password)
        {
            YssxJobTrainingTask entity = await DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该任务!" };

            bool state = false;

            if (entity.TaskPassword.Equals(password))
                state = true;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "密码正确" : "密码不正确!" };
        }
        #endregion

        #region 结束岗位实训任务
        /// <summary>
        /// 结束岗位实训任务 触发交卷
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> FinishJobTraingingTask(long id, UserTicket user)
        {
            YssxJobTrainingTask entity = await DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            bool state = false;

            //试卷列表
            List<ExamPaper> examPaperList = await DbContext.FreeSql.Select<ExamPaper>().Where(x => x.TaskId == entity.Id
                  && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            //试卷Ids
            List<long> examIds = examPaperList.Select(x => x.Id).ToList();

            //根据试卷查询主作答记录
            List<ExamStudentGrade> studentGradeList = await DbContext.FreeSql.Select<ExamStudentGrade>().Where(x =>
                examIds.Contains(x.ExamId) && x.IsDelete == CommonConstants.IsNotDelete && x.Status != StudentExamStatus.End).ToListAsync();

            //主作答记录Ids
            List<long> gradeIdList = studentGradeList.Select(x => x.Id).ToList();

            //根据主作答Ids查询作答明细
            List<ExamStudentGradeDetail> studentGradeDetailList = await DbContext.FreeSql.Select<ExamStudentGradeDetail>().Where(x =>
                gradeIdList.Contains(x.GradeId) && x.QuestionId == x.ParentQuestionId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //作答明细唯一组合键(过滤一道题重复作答的情况)
            var gardeUniqueList = studentGradeDetailList.GroupBy(x => new { x.GradeId, x.ParentQuestionId, x.QuestionId }).Select(x => new
            {
                GradeId = x.Key.GradeId,
                ParentQuestionId = x.Key.ParentQuestionId,
                QuestionId = x.Key.QuestionId
            }).ToList();

            List<ExamStudentGradeDetail> deleteList = new List<ExamStudentGradeDetail>();

            gardeUniqueList.ForEach(x =>
            {
                List<ExamStudentGradeDetail> removeList = new List<ExamStudentGradeDetail>();

                //作答某一题列表(应为一条记录)
                var questionList = studentGradeDetailList.Where(a => a.GradeId == x.GradeId && a.ParentQuestionId == x.ParentQuestionId && x.QuestionId == x.QuestionId)
                    .OrderByDescending(a => a.UpdateTime).ToList();

                if (questionList.Count > 1)
                {
                    questionList.RemoveAt(0);
                    removeList = questionList;
                }

                //添加需移除的作答明细
                deleteList.AddRange(removeList);
            });

            //移除重复的作答明细
            studentGradeDetailList.RemoveAll(it => deleteList.Select(x => x.Id).Contains(it.Id));

            studentGradeList.ForEach(x =>
            {
                var totalQuestionCount = examPaperList.Where(a => a.Id == x.ExamId).FirstOrDefault()?.TotalQuestionCount;

                if (totalQuestionCount != null)
                {
                    var answerCount = studentGradeDetailList.Where(a => a.GradeId == x.Id && a.QuestionId == a.ParentQuestionId).ToList();

                    x.CorrectCount = answerCount.Count(a => a.Status == AnswerResultStatus.Right);
                    x.ErrorCount = answerCount.Count(a => a.Status == AnswerResultStatus.Error);
                    x.PartRightCount = answerCount.Count(a => a.Status == AnswerResultStatus.PartRight);
                    x.BlankCount = (int)totalQuestionCount - answerCount.Count;
                }

                //交卷、统计分数
                x.Status = StudentExamStatus.End;
                x.Score = studentGradeDetailList.Where(a => a.GradeId == x.Id).Sum(a => a.Score);
                x.SubmitTime = DateTime.Now;
                x.UsedSeconds = (DateTime.Now - x.CreateTime).TotalSeconds;
                x.UpdateBy = user.Id;
                x.UpdateTime = DateTime.Now;
            });

            #region 更新数据
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //1、更新岗位实训任务
                    entity.TaskStatus = (int)TrainType.End;
                    entity.UpdateTime = DateTime.Now;
                    entity.UpdateBy = user.Id;

                    state = uow.GetRepository<YssxJobTrainingTask>().UpdateDiy.SetSource(entity).UpdateColumns(x =>
                        new { x.TaskStatus, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;

                    //2、更新试卷状态为End
                    if (examPaperList.Count > 0)
                    {
                        examPaperList.ForEach(x =>
                        {
                            //删除试卷缓存
                            RedisHelper.Del($"{CommonConstants.Cache_GetExamById}{x.Id}");

                            x.Status = ExamStatus.End;
                            x.UpdateBy = user.Id;
                            x.UpdateTime = DateTime.Now;
                        });
                        state = uow.GetRepository<ExamPaper>().UpdateDiy.SetSource(examPaperList).UpdateColumns(x => new
                        {
                            x.Status,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrows() > 0;
                    }

                    //3、更新主作答记录
                    if (studentGradeList.Count > 0)
                    {
                        state = uow.GetRepository<ExamStudentGrade>().UpdateDiy.SetSource(studentGradeList).UpdateColumns(x => new
                        {
                            x.CorrectCount,
                            x.ErrorCount,
                            x.PartRightCount,
                            x.BlankCount,
                            x.Status,
                            x.Score,
                            x.SubmitTime,
                            x.UsedSeconds,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrows() > 0;
                    }

                    //4、删除作答明细
                    if (deleteList.Count > 0)
                    {
                        deleteList.ForEach(x =>
                        {
                            x.IsDelete = 1;
                            x.UpdateTime = DateTime.Now;
                            x.UpdateBy = user.Id;
                        });

                        state = uow.GetRepository<ExamStudentGradeDetail>().UpdateDiy.SetSource(deleteList).UpdateColumns(x => new
                        {
                            x.IsDelete,
                            x.UpdateTime,
                            x.UpdateBy
                        }).ExecuteAffrows() > 0;
                    }

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }
            #endregion

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 删除岗位实训任务
        /// <summary>
        /// 删除岗位实训任务
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveJobTraingingTask(long id, UserTicket user)
        {
            YssxJobTrainingTask entity = await DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().Where(x => x.Id == id).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            entity.IsDelete = 1;
            entity.UpdateTime = DateTime.Now;
            entity.UpdateBy = user.Id;

            bool state = DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 获取岗位实训任务试卷作答记录
        /// <summary>
        /// 获取岗位实训任务试卷作答记录
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxJobTrainingTaskExamAnswerRecordViewModel>>> GetJobTrainingTaskExamAnswerRecord(YssxJobTrainingTaskExamAnswerRecordQuery query)
        {
            ResponseContext<List<YssxJobTrainingTaskExamAnswerRecordViewModel>> response = new ResponseContext<List<YssxJobTrainingTaskExamAnswerRecordViewModel>>();

            List<YssxJobTrainingTaskExamAnswerRecordViewModel> listData = new List<YssxJobTrainingTaskExamAnswerRecordViewModel>();

            if (query == null || !query.ExamId.HasValue)
                return response;

            //查询任务实体
            YssxJobTrainingTask entity = await DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().Where(x => x.Id == query.JobTrainingTaskId).FirstAsync();
            if (entity == null)
                return response;

            //任务状态 0:未开始 1:进行中 2:已结束
            int taskStatus = 0;

            if (DateTime.Now < entity.TrainStartTime)
                taskStatus = 0;
            if (entity.TrainStartTime <= DateTime.Now && (!entity.TrainEndTime.HasValue || DateTime.Now < entity.TrainEndTime))
                taskStatus = 1;
            if (entity.TrainStartTime <= DateTime.Now && (entity.TrainEndTime.HasValue && entity.TrainEndTime <= DateTime.Now))
                taskStatus = 2;
            if (entity.TaskStatus.HasValue && entity.TaskStatus == (int)ExamStatus.End)
                taskStatus = 2;

            //未开始
            if (taskStatus == 0)
                return response;

            //进行中
            if (taskStatus == 1)
            {
                //查询已结束的试卷作答记录列表
                var sourceData = DbContext.FreeSql.Select<ExamStudentGrade>().From<YssxStudent>((a, b) => a.InnerJoin(x => x.UserId == b.UserId))
                   .Where((a, b) => a.ExamId == query.ExamId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                   .ToList((a, b) => new
                   {
                       StudentUserId = a.UserId,
                       Score = a.Score,
                       StartTime = a.CreateTime,     //开始作答时间
                       SubmitTime = a.SubmitTime,    //交卷时间
                       UsedSeconds = a.UsedSeconds,  //作答总用时
                       Status = a.Status,            //开始状态
                       Name = b.Name,
                       StudentNo = b.StudentNo,
                       GradeId = a.Id,
                       ClassId = b.ClassId,
                       ClassName = b.ClassName,
                       StudentExamStatus = a.Status,
                   });

                if (sourceData == null || sourceData.Count <= 0)
                    return response;

                sourceData.ForEach(x =>
                {
                    YssxJobTrainingTaskExamAnswerRecordViewModel model = new YssxJobTrainingTaskExamAnswerRecordViewModel();
                    model.SubmitTime = x.SubmitTime;
                    model.StudentNo = x.StudentNo;
                    model.Name = x.Name;
                    model.TotalSeconds = x.UsedSeconds;
                    model.TotalScore = x.Score;
                    model.GradeId = x.GradeId;
                    model.ClassId = x.ClassId;
                    model.ClassName = x.ClassName;
                    model.StudentExamStatus = (int)x.StudentExamStatus;

                    listData.Add(model);
                });
            }

            //已结束
            if (taskStatus == 2)
            {
                //查询试卷作答记录列表
                var sourceData = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetExamAnswerRecordByExamId}{query.ExamId}",
                    () => DbContext.FreeSql.Select<ExamStudentGrade>().From<YssxStudent>((a, b) => a.InnerJoin(x => x.UserId == b.UserId))
                   .Where((a, b) => a.ExamId == query.ExamId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                   .ToList((a, b) => new
                   {
                       StudentUserId = a.UserId,
                       Score = a.Score,
                       StartTime = a.CreateTime,     //开始作答时间
                       SubmitTime = a.SubmitTime,    //交卷时间
                       UsedSeconds = a.UsedSeconds,  //作答总用时
                       Status = a.Status,            //开始状态
                       Name = b.Name,
                       StudentNo = b.StudentNo,
                       GradeId = a.Id,
                       ClassId = b.ClassId,
                       ClassName = b.ClassName,
                       StudentExamStatus = a.Status,

                   }), 10 * 60, true, false);

                if (sourceData == null || sourceData.Count <= 0)
                    return response;

                //已结束的作答记录
                var endList = sourceData.Where(x => x.Status == StudentExamStatus.End).Distinct().ToList();
                endList.ForEach(x =>
                {
                    YssxJobTrainingTaskExamAnswerRecordViewModel model = new YssxJobTrainingTaskExamAnswerRecordViewModel();
                    model.SubmitTime = x.SubmitTime;
                    model.StudentNo = x.StudentNo;
                    model.Name = x.Name;
                    model.TotalSeconds = x.UsedSeconds;
                    model.TotalScore = x.Score;
                    model.GradeId = x.GradeId;
                    model.ClassId = x.ClassId;
                    model.ClassName = x.ClassName;
                    model.StudentExamStatus = (int)x.StudentExamStatus;

                    listData.Add(model);
                });

                //已开始作答未交卷的作答记录
                var startList = sourceData.Where(x => x.Status != StudentExamStatus.End).Distinct().ToList();
                //学生用户Ids
                List<long> studentUserIds = startList.Select(x => x.StudentUserId).ToList();
                //学生作答记录明细
                List<ExamStudentGradeDetail> gradeDetailList = await DbContext.FreeSql.GetRepository<ExamStudentGradeDetail>()
                    .Where(x => studentUserIds.Contains(x.UserId) && x.ExamId == query.ExamId && x.IsDelete == CommonConstants.IsNotDelete && x.QuestionId == x.ParentQuestionId)
                    .ToListAsync($"{CommonConstants.Cache_GetStudentAnswerRecordDetailByExamId}{query.ExamId}", true, 10 * 60, true, false);

                startList.ForEach(x =>
                {
                    YssxJobTrainingTaskExamAnswerRecordViewModel model = new YssxJobTrainingTaskExamAnswerRecordViewModel();
                    //交卷时间等于任务手动结束时间
                    //或交卷时间等于任务结束时间
                    if (entity.TaskStatus == (int)TrainType.End)
                        model.SubmitTime = entity.UpdateTime;
                    else
                        model.SubmitTime = entity.TrainEndTime;
                    model.StudentNo = x.StudentNo;
                    model.Name = x.Name;
                    model.TotalSeconds = (model.SubmitTime.Value - x.StartTime).TotalSeconds;
                    model.TotalScore = gradeDetailList.Where(a => a.UserId == x.StudentUserId && a.GradeId == x.GradeId).Sum(a => a.Score);//统计作答明细得分
                    model.GradeId = x.GradeId;
                    model.ClassId = x.ClassId;
                    model.ClassName = x.ClassName;
                    model.StudentExamStatus = (int)x.StudentExamStatus;

                    listData.Add(model);
                });
            }

            //按得分、用时排名
            listData = listData.OrderByDescending(x => x.TotalScore).ThenBy(x => x.TotalSeconds).ToList();

            int rank = 1;
            listData.ForEach(x =>
            {
                x.Ranking = $"第{rank}名";
                rank++;
            });

            //按学号正序排列
            listData = listData.OrderBy(x => x.StudentNo).ThenBy(x => x.TotalScore).ToList();

            response.Data = listData;

            return response;
        }
        #endregion

        #region 根据岗位实训任务获取绑定班级的学生列表
        /// <summary>
        /// 根据岗位实训任务获取绑定班级的学生列表
        /// </summary>
        /// <param name="jobTrainingTaskId">任务Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<YssxJobTrainingTaskBindingStudentViewModel>> GetJobTrainingTaskBindingStudentList(long jobTrainingTaskId)
        {
            ResponseContext<YssxJobTrainingTaskBindingStudentViewModel> response = new ResponseContext<YssxJobTrainingTaskBindingStudentViewModel>();

            YssxJobTrainingTaskBindingStudentViewModel data = new YssxJobTrainingTaskBindingStudentViewModel();

            //学生列表
            List<YssxJobTrainingTaskBindingStudentList> taskStudentList = new List<YssxJobTrainingTaskBindingStudentList>();

            //查询任务实体
            YssxJobTrainingTask entity = await DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().Where(x => x.Id == jobTrainingTaskId)
                .FirstAsync($"{CommonConstants.Cache_GetJobTrainingTaskEntityByTaskId}{jobTrainingTaskId}", true, 10 * 60, true, false);
            if (entity == null)
                return response;

            //任务状态 0:未开始 1:进行中 2:已结束
            int taskStatus = 0;

            if (DateTime.Now < entity.TrainStartTime)
                taskStatus = 0;
            if (entity.TrainStartTime <= DateTime.Now && (!entity.TrainEndTime.HasValue || DateTime.Now < entity.TrainEndTime))
                taskStatus = 1;
            if (entity.TrainStartTime <= DateTime.Now && (entity.TrainEndTime.HasValue && entity.TrainEndTime <= DateTime.Now))
                taskStatus = 2;
            if (entity.TaskStatus.HasValue && entity.TaskStatus == (int)ExamStatus.End)
                taskStatus = 2;

            //岗位实训任务学生列表
            List<YssxStudent> studentList = await DbContext.FreeSql.Select<YssxStudent>().From<YssxJobTrainingTaskClass>(
                (a, b) => a.InnerJoin(x => x.ClassId == b.ClassId))
                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && b.JobTrainingTaskId == jobTrainingTaskId).ToListAsync();
            if (studentList.Count <= 0)
                return response;

            //任务未开始
            if (taskStatus == 0)
            {
                studentList.ForEach(x =>
                {
                    YssxJobTrainingTaskBindingStudentList model = new YssxJobTrainingTaskBindingStudentList();
                    model.CollegeId = x.CollegeId;
                    model.CollegeName = x.CollegeName;
                    model.ProfessionId = x.ProfessionId;
                    model.ProfessionName = x.ProfessionName;
                    model.ClassId = x.ClassId;
                    model.ClassName = x.ClassName;
                    model.Name = x.Name;
                    model.StudentNo = x.StudentNo;
                    model.StudentId = x.UserId;
                    taskStudentList.Add(model);
                });
            }
            else
            {
                //试卷列表
                List<ExamPaper> examPaperList = await DbContext.FreeSql.Select<ExamPaper>().Where(x => x.TaskId == jobTrainingTaskId
                       && x.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync($"{CommonConstants.Cache_GeExamPaperListByJobTrainingTaskId}{jobTrainingTaskId}", true, 10 * 60, true, false);

                //试卷Ids
                List<long> examIds = examPaperList.Select(a => a.Id).ToList();

                //学生作答主信息列表(综合岗位实训 一人作答全部岗位的习题)
                List<ExamStudentGrade> studentGradeList = await DbContext.FreeSql.Select<ExamStudentGrade>().Where(x => examIds.Contains(x.ExamId)
                        && x.IsDelete == CommonConstants.IsNotDelete && x.GroupId == 0).ToListAsync();

                studentList.ForEach(x =>
                {
                    YssxJobTrainingTaskBindingStudentList model = new YssxJobTrainingTaskBindingStudentList();
                    model.CollegeId = x.CollegeId;
                    model.CollegeName = x.CollegeName;
                    model.ProfessionId = x.ProfessionId;
                    model.ProfessionName = x.ProfessionName;
                    model.ClassId = x.ClassId;
                    model.ClassName = x.ClassName;
                    model.Name = x.Name;
                    model.StudentNo = x.StudentNo;
                    model.StudentId = x.UserId;
                    //学生交卷的数量=试卷的数量 则任务已交
                    if (studentGradeList.Where(b => b.Status == StudentExamStatus.End && b.UserId == x.UserId).Count() == examIds.Count)
                        model.StudentTaskStatus = 1;
                    else
                        model.StudentTaskStatus = 0;

                    taskStudentList.Add(model);
                });
            }

            data.TaskStatus = taskStatus;
            data.StudentList = taskStudentList;

            response.Data = data;

            return response;
        }
        #endregion

        #region 根据试卷Id强制交卷、统计分数
        /// <summary>
        /// 根据试卷Id统一交卷、统计分数
        /// </summary>
        /// <param name="examId"></param>
        /// <param name="user"></param>
        public async Task<ResponseContext<bool>> SubmitPaperAndCalculateScoreByExamId(long examId, UserTicket user)
        {
            //查询试卷信息
            ExamPaper examEnter = await DbContext.FreeSql.GetRepository<ExamPaper>().Where(x => x.Id == examId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (examEnter == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "试卷不存在!" };

            bool state = false;

            //根据试卷查询主作答记录
            List<ExamStudentGrade> studentGradeList = await DbContext.FreeSql.Select<ExamStudentGrade>().Where(x =>
                x.ExamId == examId && x.IsDelete == CommonConstants.IsNotDelete && x.Status != StudentExamStatus.End).ToListAsync();
            if (studentGradeList == null || studentGradeList.Count <= 0)
            {
                //将试卷状态改为End
                examEnter.Status = ExamStatus.End;
                examEnter.UpdateBy = user.Id;
                examEnter.UpdateTime = DateTime.Now;
                state = DbContext.FreeSql.GetRepository<ExamPaper>().UpdateDiy.SetSource(examEnter).UpdateColumns(x => new
                {
                    x.Status,
                    x.UpdateBy,
                    x.UpdateTime
                }).ExecuteAffrows() > 0;

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "已全部交卷!" : "操作失败!" };
            }

            //主作答记录Ids
            List<long> gradeIdList = studentGradeList.Select(x => x.Id).ToList();

            //根据主作答Ids查询作答明细
            List<ExamStudentGradeDetail> studentGradeDetailList = await DbContext.FreeSql.Select<ExamStudentGradeDetail>().Where(x =>
                x.ExamId == examId && gradeIdList.Contains(x.GradeId) && x.QuestionId == x.ParentQuestionId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //作答明细唯一组合键(过滤一道题重复作答的情况)
            var gardeUniqueList = studentGradeDetailList.GroupBy(x => new { x.GradeId, x.ParentQuestionId, x.QuestionId }).Select(x => new
            {
                GradeId = x.Key.GradeId,
                ParentQuestionId = x.Key.ParentQuestionId,
                QuestionId = x.Key.QuestionId
            }).ToList();

            List<ExamStudentGradeDetail> deleteList = new List<ExamStudentGradeDetail>();

            gardeUniqueList.ForEach(x =>
            {
                List<ExamStudentGradeDetail> removeList = new List<ExamStudentGradeDetail>();

                //作答某一题列表(应为一条记录)
                var questionList = studentGradeDetailList.Where(a => a.GradeId == x.GradeId && a.ParentQuestionId == x.ParentQuestionId && x.QuestionId == x.QuestionId)
                    .OrderByDescending(a => a.UpdateTime).ToList();

                if (questionList.Count > 1)
                {
                    questionList.RemoveAt(0);
                    removeList = questionList;
                }

                //添加需移除的作答明细
                deleteList.AddRange(removeList);
            });

            //移除重复的作答明细
            studentGradeDetailList.RemoveAll(it => deleteList.Select(x => x.Id).Contains(it.Id));

            studentGradeList.ForEach(x =>
            {
                var totalQuestionCount = examEnter.TotalQuestionCount;

                var answerCount = studentGradeDetailList.Where(a => a.GradeId == x.Id && a.QuestionId == a.ParentQuestionId).ToList();

                x.CorrectCount = answerCount.Count(a => a.Status == AnswerResultStatus.Right);
                x.ErrorCount = answerCount.Count(a => a.Status == AnswerResultStatus.Error);
                x.PartRightCount = answerCount.Count(a => a.Status == AnswerResultStatus.PartRight);
                x.BlankCount = totalQuestionCount - answerCount.Count;

                //交卷、统计分数
                x.Status = StudentExamStatus.End;
                x.Score = studentGradeDetailList.Where(a => a.GradeId == x.Id).Sum(a => a.Score);
                x.SubmitTime = DateTime.Now;
                x.UsedSeconds = (DateTime.Now - x.CreateTime).TotalSeconds;
                x.UpdateBy = user.Id;
                x.UpdateTime = DateTime.Now;
            });

            #region 更新数据
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    //1、更新试卷状态为End
                    examEnter.Status = ExamStatus.End;
                    examEnter.UpdateBy = user.Id;
                    examEnter.UpdateTime = DateTime.Now;
                    state = uow.GetRepository<ExamPaper>().UpdateDiy.SetSource(examEnter).UpdateColumns(x => new
                    {
                        x.Status,
                        x.UpdateBy,
                        x.UpdateTime
                    }).ExecuteAffrows() > 0;

                    //2、更新主作答记录
                    if (studentGradeList.Count > 0)
                    {
                        state = uow.GetRepository<ExamStudentGrade>().UpdateDiy.SetSource(studentGradeList).UpdateColumns(x => new
                        {
                            x.CorrectCount,
                            x.ErrorCount,
                            x.PartRightCount,
                            x.BlankCount,
                            x.Status,
                            x.Score,
                            x.SubmitTime,
                            x.UsedSeconds,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrows() > 0;
                    }

                    //3、删除作答明细
                    if (deleteList.Count > 0)
                    {
                        deleteList.ForEach(x =>
                        {
                            x.IsDelete = 1;
                            x.UpdateTime = DateTime.Now;
                            x.UpdateBy = user.Id;
                        });

                        state = uow.GetRepository<ExamStudentGradeDetail>().UpdateDiy.SetSource(deleteList).UpdateColumns(x => new
                        {
                            x.IsDelete,
                            x.UpdateTime,
                            x.UpdateBy
                        }).ExecuteAffrows() > 0;
                    }

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }
            #endregion

            #region 删除缓存
            //删除已交卷学生缓存
            RedisHelper.Del($"{CommonConstants.Cache_GetHaveHandExamAnswerRecordByExamId}{examId}");
            //删除未交卷学生缓存
            RedisHelper.Del($"{CommonConstants.Cache_GetNotHandExamAnswerRecordByExamId}{examId}");
            //删除学生试卷作答缓存
            RedisHelper.Del($"{CommonConstants.Cache_GetStudentExamAnswerRecordByExamId}{examId}");
            #endregion

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "已全部交卷!" : "操作失败!" };
        }
        #endregion

        #region 重新开启任务设置
        public async Task<ResponseContext<bool>> SetTaskStatus(long taskId, DateTime endDate, bool isCanCheckExam)
        {
            if (endDate == null || taskId <= 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "参数不合法" };
            if (endDate < DateTime.Now) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "不能小于当前时间" };

            List<ExamPaper> papers = await DbContext.FreeSql.GetRepository<ExamPaper>().Where(x => x.TaskId == taskId).ToListAsync();
            bool state = false;
            bool state2 = false;
            if (papers.Count > 0)
            {
                papers.ForEach(x =>
                {
                    x.Status = ExamStatus.Started;
                    x.EndTime = endDate;
                });
                state2 = await DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().UpdateDiy
                    .Set(x => x.TaskStatus, 1)
                    .Set(x => x.TrainEndTime, endDate)
                    .Set(x => x.IsCanCheckExam, isCanCheckExam)
                    .Where(x => x.Id == taskId).ExecuteAffrowsAsync() > 0;
                state = await DbContext.FreeSql.Update<ExamPaper>().SetSource(papers).IgnoreColumns(a => new { a.Id }).ExecuteAffrowsAsync() > 0;
            }

            bool isOk = state && state2;
            return new ResponseContext<bool> { Code = isOk ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = isOk ? "ok" : "开启失败!", Data = state };
        }

        public async Task<ResponseContext<bool>> SetStudentStatus(long taskId, long studentId)
        {
            List<ExamStudentGrade> exams = await DbContext.FreeSql.GetRepository<ExamPaper>().Select.From<ExamStudentGrade>((a, b) => a.InnerJoin(x => x.Id == b.ExamId)).Where((a, b) => a.TaskId == taskId && b.UserId == studentId && b.Status == StudentExamStatus.End && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync((a, b) => new ExamStudentGrade
            {
                Id = b.Id
            });
            bool state = false;
            if (exams.Count > 0)
            {
                exams.ForEach(x =>
                {
                    x.Status = StudentExamStatus.Started;
                });
                state = await DbContext.FreeSql.Update<ExamStudentGrade>().SetSource(exams).Set(x => x.Status, StudentExamStatus.Started).ExecuteAffrowsAsync() > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "ok" : "开启失败!", Data = state };
        }
        public async Task<ResponseContext<bool>> SetStudentStatus(long gradeId, StudentExamStatus status)
        {
            bool state = await DbContext.FreeSql.GetRepository<ExamStudentGrade>().UpdateDiy.Set(x => x.Status, status).Where(x => x.Id == gradeId).ExecuteAffrowsAsync() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "ok" : "开启失败!", Data = state };
        }
        #endregion

        #region 2020-12-25需求改版

        #region 获取岗位实训任务套题(案例)列表
        /// <summary>
        /// 获取岗位实训任务套题(案例)列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<JobTrainingCaseViewModel>>> GetJobTrainingCaseList(long taskId)
        {
            ResponseContext<List<JobTrainingCaseViewModel>> response = new ResponseContext<List<JobTrainingCaseViewModel>>();

            //任务试卷列表
            List<ExamPaper> examPaperList = DbContext.FreeSql.Select<ExamPaper>().Where(x => x.TaskId == taskId && x.IsDelete == CommonConstants.IsNotDelete).ToList();

            //岗位实训任务套题列表
            var jobTrainingList = await DbContext.FreeSql.Select<YssxJobTrainingTaskTopic>().From<YssxCase>(
                (a, b) => a.InnerJoin(x => x.JobTrainingId == b.Id))
                .Where((a, b) => a.JobTrainingTaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new JobTrainingCaseViewModel
                {
                    JobTrainingTaskId = a.JobTrainingTaskId,
                    JobTrainingId = b.Id,
                    JobTrainingName = b.Name,
                    RollUpType = b.RollUpType,
                    CaseYear = b.CaseYear,
                });

            jobTrainingList.ForEach(x =>
            {
                //试卷Id
                x.ExamId = examPaperList.Where(b => b.TaskId == taskId && b.CaseId == x.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault()?.Id;

                var examData = examPaperList.Where(b => b.TaskId == taskId && b.CaseId == x.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault();
                if (examData != null)
                {
                    x.ExamId = examData.Id;
                    if (examData.Status == ExamStatus.End)
                        x.ExamStatus = 1;
                    else
                        x.ExamStatus = 0;
                }

                var gradeList = DbContext.FreeSql.Select<ExamStudentGrade>().Where(a => a.ExamId == x.ExamId && a.IsDelete == CommonConstants.IsNotDelete).ToList();

                x.StudentCount = gradeList.Count;
                x.SubmittedCount = gradeList.Where(a => a.Status == StudentExamStatus.End).Count();
            });

            response.Data = jobTrainingList;

            return response;
        }
        #endregion

        #region 获取岗位实训任务试卷学生实时作答详情
        /// <summary>
        /// 获取岗位实训任务试卷学生实时作答详情
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<JobTrainingTaskExamStudentAnswerRecordViewModel>> GetJobTrainingTaskExamStudentAnswerRecord(JobTrainingTaskExamStudentAnswerRecordQuery query)
        {
            ResponseContext<JobTrainingTaskExamStudentAnswerRecordViewModel> response = new ResponseContext<JobTrainingTaskExamStudentAnswerRecordViewModel>();

            JobTrainingTaskExamStudentAnswerRecordViewModel data = new JobTrainingTaskExamStudentAnswerRecordViewModel();
            List<JobTrainingTaskStudentAnswerRecordViewModel> listData = new List<JobTrainingTaskStudentAnswerRecordViewModel>();

            if (query == null || !query.ExamId.HasValue)
                return response;

            //查询任务实体
            YssxJobTrainingTask entity = await DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().Where(x => x.Id == query.JobTrainingTaskId).FirstAsync();
            if (entity == null)
                return response;

            ExamPaper examPaper = await DbContext.FreeSql.GetRepository<ExamPaper>().Where(x => x.Id == query.ExamId).FirstAsync();
            if (examPaper == null)
                return response;

            //任务状态 0:未开始 1:进行中 2:已结束
            int taskStatus = 0;

            if (DateTime.Now < entity.TrainStartTime)
                taskStatus = 0;
            if (entity.TrainStartTime <= DateTime.Now && (!entity.TrainEndTime.HasValue || DateTime.Now < entity.TrainEndTime))
                taskStatus = 1;
            if (entity.TrainStartTime <= DateTime.Now && (entity.TrainEndTime.HasValue && entity.TrainEndTime <= DateTime.Now))
                taskStatus = 2;
            if (entity.TaskStatus.HasValue && entity.TaskStatus == (int)ExamStatus.End)
                taskStatus = 2;

            //赋值
            data.TrainStartTime = entity.TrainStartTime;
            data.TrainEndTime = entity.TrainEndTime;
            data.TaskStatus = entity.TaskStatus;
            data.TotalQuestionCount = examPaper.TotalQuestionCount;

            //未开始
            if (taskStatus == 0)
            {
                response.Data = data;
                return response;
            }

            //取实时数据 删除缓存
            if (query.IsRealTimeData)
            {
                //删除已交卷学生缓存
                RedisHelper.Del($"{CommonConstants.Cache_GetHaveHandExamAnswerRecordByExamId}{query.ExamId}");
                //删除未交卷学生缓存
                RedisHelper.Del($"{CommonConstants.Cache_GetNotHandExamAnswerRecordByExamId}{query.ExamId}");
                //删除学生试卷作答缓存
                RedisHelper.Del($"{CommonConstants.Cache_GetStudentExamAnswerRecordByExamId}{query.ExamId}");
            }

            //缓存时间
            int cacheTime = 0;

            //进行中 缓存时间3分钟
            if (taskStatus == 1)
                cacheTime = 3 * 60;
            //已结束 缓存时间10分钟
            if (taskStatus == 2)
                cacheTime = 10 * 60;

            #region
            //查询任务班级学生数量
            /*
            data.StudentCount = (int)await DbContext.FreeSql.Select<YssxStudent>().From<YssxJobTrainingTaskClass>(
                (a, b) => a.InnerJoin(x => x.ClassId == b.ClassId))
                .Where((a, b) => b.JobTrainingTaskId == query.JobTrainingTaskId && b.IsDelete == CommonConstants.IsNotDelete && a.IsDelete == CommonConstants.IsNotDelete)
                .CountAsync();
            */
            #endregion

            #region 
            /*
            #region 查询已交卷学生

            //查询已交卷学生
            List<JobTrainingTaskStudentAnswerRecordViewModel> haveHandList = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetHaveHandExamAnswerRecordByExamId}" +
                $"{query.ExamId}",
               () => DbContext.FreeSql.Select<ExamStudentGrade>().From<YssxStudent, YssxJobTrainingTaskClass>(
                (a, b, c) => a.InnerJoin(x => x.UserId == b.UserId)
                .InnerJoin(x => b.ClassId == c.ClassId))
               .WhereIf(query.ClassId.HasValue, (a, b, c) => c.ClassId == query.ClassId)
                .Where((a, b, c) => a.ExamId == query.ExamId && a.Status == StudentExamStatus.End && c.JobTrainingTaskId == query.JobTrainingTaskId &&
                c.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && a.IsDelete == CommonConstants.IsNotDelete)
                .ToList((a, b, c) => new JobTrainingTaskStudentAnswerRecordViewModel
                {
                    StudentNo = b.StudentNo,
                    Name = b.Name,
                    ClassId = b.ClassId,
                    ClassName = b.ClassName,
                    CorrectCount = a.CorrectCount,
                    ErrorCount = a.ErrorCount,
                    PartRightCount = a.PartRightCount,
                    BlankCount = a.BlankCount,
                    TotalScore = a.Score,
                    TotalSeconds = a.UsedSeconds,
                    SubmitTime = a.SubmitTime,
                    GradeId = a.Id,
                    StudentExamStatus = a.Status
                }), cacheTime, true, false);

            haveHandList.ForEach(x =>
            {
                x.AnswerCount = x.CorrectCount + x.ErrorCount + x.PartRightCount;

                //总题数
                int questionTotalCount = x.CorrectCount + x.ErrorCount + x.PartRightCount + x.BlankCount;

                if (questionTotalCount == 0 || x.CorrectCount == 0)
                    x.AnswerRate = 0;
                else
                    x.AnswerRate = Math.Round(((decimal)x.CorrectCount / (decimal)questionTotalCount), 4) * 100;
            });

            //已交学生数量
            data.SubmittedCount = haveHandList.Count;

            listData.AddRange(haveHandList);

            #endregion

            #region 查询未交卷学生

            //查询未交卷学生作答详情
            var notHand = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetNotHandExamAnswerRecordByExamId}{query.ExamId}",
                    () => DbContext.FreeSql.Select<ExamStudentGrade>().From<YssxStudent, YssxJobTrainingTaskClass, ExamStudentGradeDetail>(
                     (a, b, c, d) => a.InnerJoin(x => x.UserId == b.UserId)
                     .InnerJoin(x => b.ClassId == c.ClassId)
                     .LeftJoin(x => x.Id == d.GradeId && d.QuestionId == d.ParentQuestionId && d.IsDelete == CommonConstants.IsNotDelete))
                    .WhereIf(query.ClassId.HasValue, (a, b, c, d) => c.ClassId == query.ClassId)
                     .Where((a, b, c, d) => a.ExamId == query.ExamId && a.Status != StudentExamStatus.End && c.JobTrainingTaskId == query.JobTrainingTaskId
                     && c.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && a.IsDelete == CommonConstants.IsNotDelete)
                     .ToList((a, b, c, d) => new
                     {
                         StudentNo = b.StudentNo,
                         Name = b.Name,
                         ClassId = b.ClassId,
                         ClassName = b.ClassName,
                         TotalScore = d.Score,
                         GradeId = a.Id,
                         AnswerResultStatus = d.Status
                     }), 3 * 60, true, false);

            //未交卷学生列表
            var notHandUser = notHand.GroupBy(x => new { x.StudentNo, x.Name, x.ClassId, x.ClassName, x.GradeId }).ToList();

            //未交卷记录列表
            List<JobTrainingTaskStudentAnswerRecordViewModel> notHandList = new List<JobTrainingTaskStudentAnswerRecordViewModel>();

            notHandUser.ForEach(x =>
            {
                JobTrainingTaskStudentAnswerRecordViewModel answerModel = new JobTrainingTaskStudentAnswerRecordViewModel();
                answerModel.StudentNo = x.Key.StudentNo;
                answerModel.Name = x.Key.Name;
                answerModel.ClassId = x.Key.ClassId;
                answerModel.ClassName = x.Key.ClassName;
                answerModel.AnswerCount = notHand.Where(a => a.GradeId == x.Key.GradeId && a.AnswerResultStatus != AnswerResultStatus.None).Count();
                answerModel.CorrectCount = notHand.Where(a => a.GradeId == x.Key.GradeId && a.AnswerResultStatus == AnswerResultStatus.Right).Count();
                answerModel.ErrorCount = notHand.Where(a => a.GradeId == x.Key.GradeId && a.AnswerResultStatus == AnswerResultStatus.Error).Count();
                answerModel.PartRightCount = notHand.Where(a => a.GradeId == x.Key.GradeId && a.AnswerResultStatus == AnswerResultStatus.PartRight).Count();
                answerModel.BlankCount = examPaper.TotalQuestionCount - answerModel.AnswerCount;
                answerModel.TotalScore = notHand.Where(a => a.GradeId == x.Key.GradeId).Sum(a => a.TotalScore);
                answerModel.GradeId = x.Key.GradeId;
                answerModel.StudentExamStatus = StudentExamStatus.Started;

                if (answerModel.AnswerCount == 0 || answerModel.CorrectCount == 0)
                    answerModel.AnswerRate = 0;
                else
                    answerModel.AnswerRate = Math.Round(((decimal)answerModel.CorrectCount / (decimal)answerModel.AnswerCount), 2) * 100;

                notHandList.Add(answerModel);
            });

            listData.AddRange(notHandList);

            #endregion
            */
            #endregion

            #region 查询学生作答

            //查询学生作答详情
            var notHand = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetStudentExamAnswerRecordByExamId}{query.ExamId}",
                    () => DbContext.FreeSql.Select<ExamStudentGrade>().From<YssxStudent, YssxJobTrainingTaskClass, ExamStudentGradeDetail>(
                     (a, b, c, d) => a.InnerJoin(x => x.UserId == b.UserId)
                     .InnerJoin(x => b.ClassId == c.ClassId)
                     .LeftJoin(x => x.Id == d.GradeId && d.QuestionId == d.ParentQuestionId && d.IsDelete == CommonConstants.IsNotDelete))
                    .WhereIf(query.ClassId.HasValue, (a, b, c, d) => c.ClassId == query.ClassId)
                     .Where((a, b, c, d) => a.ExamId == query.ExamId && c.JobTrainingTaskId == query.JobTrainingTaskId
                     && c.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && a.IsDelete == CommonConstants.IsNotDelete)
                     .ToList((a, b, c, d) => new
                     {
                         StudentExamStatus = a.Status,
                         StudentNo = b.StudentNo,
                         Name = b.Name,
                         ClassId = b.ClassId,
                         ClassName = b.ClassName,
                         TotalScore = d.Score,
                         GradeId = a.Id,
                         AnswerResultStatus = d.Status,
                         TotalSeconds = a.UsedSeconds,
                         SubmitTime = a.SubmitTime,                         
                         
                     }), 3 * 60, true, false);

            //学生列表
            var notHandUser = notHand.GroupBy(x => new { x.StudentExamStatus, x.StudentNo, x.Name, x.ClassId, x.ClassName, x.GradeId, x.TotalSeconds, x.SubmitTime }).ToList();

            //记录列表
            List<JobTrainingTaskStudentAnswerRecordViewModel> notHandList = new List<JobTrainingTaskStudentAnswerRecordViewModel>();

            notHandUser.ForEach(x =>
            {
                JobTrainingTaskStudentAnswerRecordViewModel answerModel = new JobTrainingTaskStudentAnswerRecordViewModel();
                answerModel.StudentNo = x.Key.StudentNo;
                answerModel.Name = x.Key.Name;
                answerModel.ClassId = x.Key.ClassId;
                answerModel.ClassName = x.Key.ClassName;
                answerModel.AnswerCount = notHand.Where(a => a.GradeId == x.Key.GradeId && a.AnswerResultStatus != AnswerResultStatus.None).Count();
                answerModel.CorrectCount = notHand.Where(a => a.GradeId == x.Key.GradeId && a.AnswerResultStatus == AnswerResultStatus.Right).Count();
                answerModel.ErrorCount = notHand.Where(a => a.GradeId == x.Key.GradeId && a.AnswerResultStatus == AnswerResultStatus.Error).Count();
                answerModel.PartRightCount = notHand.Where(a => a.GradeId == x.Key.GradeId && a.AnswerResultStatus == AnswerResultStatus.PartRight).Count();
                answerModel.BlankCount = examPaper.TotalQuestionCount - answerModel.AnswerCount;
                answerModel.TotalScore = notHand.Where(a => a.GradeId == x.Key.GradeId).Sum(a => a.TotalScore);
                answerModel.GradeId = x.Key.GradeId;
                answerModel.StudentExamStatus = x.Key.StudentExamStatus;
                answerModel.TotalSeconds = x.Key.TotalSeconds;
                answerModel.SubmitTime = x.Key.SubmitTime;

                if (answerModel.AnswerCount == 0 || answerModel.CorrectCount == 0)
                    answerModel.AnswerRate = 0;
                else
                    answerModel.AnswerRate = Math.Round(((decimal)answerModel.CorrectCount / (decimal)answerModel.AnswerCount), 4) * 100;

                notHandList.Add(answerModel);
            });

            //查询任务班级已作答学生数量
            data.StudentCount = notHandList.Count;

            //已交学生数量
            data.SubmittedCount = notHandList.Where(x => x.StudentExamStatus == StudentExamStatus.End).Count();

            //作答题数为0的列表
            var emptyAnswerList = notHandList.Where(x => x.AnswerCount == 0).ToList();
            //作答题数大于0的列表 按得分、作答时长排序
            var hasAnswerList = notHandList.Where(x => x.AnswerCount > 0).OrderByDescending(x => x.TotalScore).ThenBy(x => x.TotalSeconds).ToList();

            int rank = 1;
            hasAnswerList.ForEach(x =>
            {
                x.Ranking = $"第{rank}名"; 
                rank++;
            });
            emptyAnswerList.ForEach(x =>
            {
                x.Ranking = $"第{rank}名";
                rank++;
            });

            listData.AddRange(hasAnswerList);
            listData.AddRange(emptyAnswerList);

            #endregion

            //按学号排序
            listData = listData.OrderBy(x => x.StudentNo).ThenBy(x => x.TotalScore).ToList();

            data.AnswerRecordList = listData;

            response.Data = data;
            return response;
        }
        #endregion

        #region 取消交卷
        /// <summary>
        /// 取消交卷
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CancelExamSubmit(CancelSubmitRequestModel dto, UserTicket user)
        {
            if (dto == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "参数不能为空!" };

            ExamStudentGrade examGrade = await DbContext.FreeSql.GetRepository<ExamStudentGrade>().Where(x => x.Id == dto.GradeId).FirstAsync();
            if (examGrade == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "作答记录不存在!" };
            if (examGrade.Status != StudentExamStatus.End)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "未交卷,无法取消!!" };

            ExamPaper examPaper = await DbContext.FreeSql.GetRepository<ExamPaper>().Where(x => x.Id == dto.ExamId).FirstAsync();
            if (examPaper == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "试卷不存在!" };

            if (examPaper.EndTime <= DateTime.Now)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "考试已结束,无法取消!" };
            if (examPaper.Status == ExamStatus.End)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "试卷已结束,无法取消!" };

            bool state = await DbContext.FreeSql.GetRepository<ExamStudentGrade>().UpdateDiy
                .Set(x => x.Status, StudentExamStatus.Started)
                .Set(x => x.UpdateBy, user.Id)
                .Set(x => x.UpdateTime, DateTime.Now)
                .Where(x => x.Id == dto.GradeId).ExecuteAffrowsAsync() > 0;

            #region 删除缓存
            //删除已交卷学生缓存
            RedisHelper.Del($"{CommonConstants.Cache_GetHaveHandExamAnswerRecordByExamId}{dto.ExamId}");
            //删除未交卷学生缓存
            RedisHelper.Del($"{CommonConstants.Cache_GetNotHandExamAnswerRecordByExamId}{dto.ExamId}");
            //删除学生试卷作答缓存
            RedisHelper.Del($"{CommonConstants.Cache_GetStudentExamAnswerRecordByExamId}{dto.ExamId}");
            #endregion

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "操作成功!" : "取消失败!", Data = state };
        }
        #endregion

        #region 打开或关闭学生查看试卷入口
        /// <summary>
        /// 打开或关闭学生查看试卷入口
        /// </summary>
        /// <param name="request"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> OpenOrCloseJobTrainingTaskCheckExam(CloseCheckExamRequestModel request, UserTicket user)
        {
            if (request == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "参数不能为空!" };
            
            YssxJobTrainingTask entity = await DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().Where(x => x.Id == request.TaskId).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            bool state = await DbContext.FreeSql.GetRepository<YssxJobTrainingTask>().UpdateDiy
                .Set(x => x.IsCanCheckExam, request.IsCanCheckExam)
                .Set(x => x.UpdateBy, user.Id)
                .Set(x => x.UpdateTime, DateTime.Now)
                .Where(x => x.Id == request.TaskId).ExecuteAffrowsAsync() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = state ? "操作成功!" : "失败!", Data = state };
        }
        #endregion

        #region 获取岗位实训任务套题(案例)列表【学生端】
        /// <summary>
        /// 获取岗位实训任务套题(案例)列表【学生端】
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxJobTrainingViewModel>>> GetStudentTaskCaseList(long taskId, UserTicket user)
        {
            ResponseContext<List<YssxJobTrainingViewModel>> response = new ResponseContext<List<YssxJobTrainingViewModel>>();

            List<YssxJobTrainingViewModel> listData = new List<YssxJobTrainingViewModel>();

            //试卷列表
            List<ExamPaper> examPaperList = DbContext.FreeSql.Select<ExamPaper>().Where(x => x.TaskId == taskId
                  && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            //试卷Ids
            List<long> examIds = examPaperList.Select(q => q.Id).ToList();

            //学生作答主信息列表(综合岗位实训 一人作答全部岗位的习题)
            List<ExamStudentGrade> studentGradeList = DbContext.FreeSql.Select<ExamStudentGrade>()
                .Where(x => x.UserId == user.Id && examIds.Contains(x.ExamId) && x.IsDelete == CommonConstants.IsNotDelete && x.GroupId == 0).ToList();

            //学生作答明细列表
            List<ExamStudentGradeDetail> studentGradeDetailList = DbContext.FreeSql.Select<ExamStudentGradeDetail>()
                .Where(a => a.UserId == user.Id && examIds.Contains(a.ExamId) && a.IsDelete == CommonConstants.IsNotDelete && a.QuestionId == a.ParentQuestionId).ToList();

            //岗位实训任务套题列表
            var jobTrainingList = await DbContext.FreeSql.Select<YssxJobTrainingTaskTopic>().From<YssxCase>(
                (a, b) => a.InnerJoin(x => x.JobTrainingId == b.Id))
                .Where((a, b) => a.JobTrainingTaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new
                {
                    JobTrainingTaskId = a.JobTrainingTaskId,
                    JobTrainingId = b.Id,
                    JobTrainingName = b.Name,
                    RollUpType = b.RollUpType,
                    CaseYear = b.CaseYear,
                });

            jobTrainingList.ForEach(a =>
            {
                if (examPaperList.Where(b => b.CaseId == a.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault() != null)
                {
                    YssxJobTrainingViewModel model = new YssxJobTrainingViewModel();
                    model.JobTrainingId = a.JobTrainingId;
                    model.JobTrainingName = a.JobTrainingName;
                    model.RollUpType = a.RollUpType;
                    model.CaseYear = a.CaseYear;
                    model.ExamId = examPaperList.Where(b => b.CaseId == a.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault()?.Id;
                    model.ExamScore = examPaperList.Where(b => b.CaseId == a.JobTrainingId).OrderBy(e => e.Id).FirstOrDefault()?.TotalScore ?? 0;
                    //综合岗位实训 作答记录Id
                    if (model.ExamId.HasValue)
                    {
                        model.GradeId = studentGradeList.Where(b => b.ExamId == model.ExamId).FirstOrDefault()?.Id;
                        model.Score = studentGradeList.Where(b => b.ExamId == model.ExamId).FirstOrDefault()?.Score ?? 0;
                        if (studentGradeList.Where(b => b.ExamId == model.ExamId).FirstOrDefault()?.Status != null)
                            model.StudentExamStatus = (int)studentGradeList.Where(b => b.ExamId == model.ExamId).FirstOrDefault()?.Status;
                    }

                    listData.Add(model);
                }
            });

            response.Data = listData.Distinct().ToList();

            return response;
        }
        #endregion



        #endregion

    }
}
