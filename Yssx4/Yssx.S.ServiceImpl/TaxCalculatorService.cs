﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto.Tax;
using Yssx.S.IServices;

namespace Yssx.S.ServiceImpl
{
    public class TaxCalculatorService : ITaxCalculatorService
    {
        /// <summary>
        /// 计算工具
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="taxType"></param>
        /// <returns></returns>
        public async Task<ResponseContext<TaxCalculatorDto>> TaxCalculator(int taxType, decimal x1, decimal x2,int monthType)
        {
            return await TaxCalculatorFunc(taxType, 0, 0, monthType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taxType"></param>
        /// <returns></returns>
        public async Task<ResponseContext<TaxCalculatorSignDto>> TaxCalculatorSign(int taxType, decimal amount)
        {
            return await TaxCalculatorFunc(taxType, amount);
        }

        private async Task<ResponseContext<TaxCalculatorSignDto>> TaxCalculatorFunc(int taxType, decimal amount)
        {
            return await Task.Factory.StartNew<ResponseContext<TaxCalculatorSignDto>>(() =>
            {
                TaxCalculatorSignDto signDto = new TaxCalculatorSignDto();
                decimal a = 800;
                decimal b = 0.2m;
                bool isCache = false;
                TaxCalculatorDto retVal = new TaxCalculatorDto();
                retVal.TaxList = new List<TaxToolDto>
                {
                    new TaxToolDto{Salary=amount},
                };
                retVal.TaxSalarieList = new List<TaxSalaryDto>
                {
                    new TaxSalaryDto{TaxableIncome=amount}
                };
                retVal.LeaseList = new List<TaxLeaseDto>
                {
                    new TaxLeaseDto{Salary=amount}
                };
                switch (taxType)
                {
                    case 0:
                        TaxCalculatorFunc1(retVal.TaxList, a, b, isCache); signDto.TaxAmountPayable = retVal.TaxList[0].TaxAmountPayable; break;
                    case 1:
                        TaxCalculatorFunc2(retVal.TaxList, a, b, isCache); signDto.TaxAmountPayable = retVal.TaxList[0].TaxAmountPayable; break;
                    case 2:
                        TaxCalculatorFunc3(retVal.TaxList, a, b, isCache); signDto.TaxAmountPayable = retVal.TaxList[0].TaxAmountPayable; break;
                    case 3:
                        TaxCalculatorFunc4_ToMonth(retVal.TaxSalarieList, a, b, isCache); signDto.TaxAmountPayable = retVal.TaxSalarieList[0].TaxAmountPayable; break;
                    case 4: TaxCalculatorFunc5(retVal.LeaseList, isCache); signDto.TaxAmountPayable = retVal.LeaseList[0].TaxAmountPayable; break;
                    case 5: TaxCalculatorFunc6(retVal.TaxList, a, b, isCache); signDto.TaxAmountPayable = retVal.TaxSalarieList[0].TaxAmountPayable; break;
                    default:
                        break;
                }

                return new ResponseContext<TaxCalculatorSignDto> { Data = signDto };
            });
        }

        /// <summary>
        /// 税率计算
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        /// <returns></returns>
        private async Task<ResponseContext<TaxCalculatorDto>> TaxCalculatorFunc(int taxType, decimal x1, decimal x2, int monthType)
        {
            return await Task.Factory.StartNew<ResponseContext<TaxCalculatorDto>>(() =>
            {
                TaxCalculatorDto retVal = GetJsonData(taxType);
                decimal a = x1 <= 0 ? 800 : x1;
                decimal b = x2 <= 0 ? 0.2m : x2;
                switch (taxType)
                {
                    case 0:
                        TaxCalculatorFunc1(retVal.TaxList, a, b); break;
                    case 1:
                        TaxCalculatorFunc2(retVal.TaxList, a, b); break;
                    case 2:
                        TaxCalculatorFunc3(retVal.TaxList, a, b); break;
                    case 3:
                        if (monthType == 0)
                            TaxCalculatorFunc4(retVal.TaxSalarieList, a, b);
                        else TaxCalculatorFunc4_ToMonth(retVal.TaxSalarieList, a, b);
                        break;
                    case 4: TaxCalculatorFunc5(retVal.LeaseList); break;
                    case 5: TaxCalculatorFunc6(retVal.TaxList, a, b); break;
                    default:
                        break;
                }
                return new ResponseContext<TaxCalculatorDto> { Data = retVal };
            });
        }

        private TaxCalculatorDto GetJsonData(int taxType)
        {
            TaxCalculatorDto dto = new TaxCalculatorDto();
            string json = "";
            switch (taxType)
            {
                case 0:
                case 1:
                case 2:
                case 5:
                    json = RedisHelper.Get($"Cache:Tax:disclosure_data:{taxType}"); if (!string.IsNullOrEmpty(json)) dto.TaxList = JsonConvert.DeserializeObject<List<TaxToolDto>>(json); break;
                case 3: json = RedisHelper.Get($"Cache:Salary:salary_data"); if (!string.IsNullOrEmpty(json)) dto.TaxSalarieList = JsonConvert.DeserializeObject<List<TaxSalaryDto>>(json); break;
                case 4: json = RedisHelper.Get($"Cache:Repair:repair_data"); if (!string.IsNullOrEmpty(json)) dto.LeaseList = JsonConvert.DeserializeObject<List<TaxLeaseDto>>(json); break;
                default:
                    break;
            }
            return dto;
        }

        /// <summary>
        /// 特权计算
        /// </summary>
        /// <param name="taxTools"></param>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        private void TaxCalculatorFunc1(List<TaxToolDto> taxTools, decimal x1, decimal x2, bool isCache = true)
        {
            if (taxTools == null) return;
            taxTools.ForEach(x =>
            {
                if (x.Salary <= 4000)
                {
                    x.TaxAmountPayable = (x.Salary - x1) * x2;
                }
                else
                {
                    x.TaxAmountPayable = x.Salary * 0.8m * x2;
                }
            });
            if (isCache)
            {
                RedisHelper.Set($"Cache:Tax:disclosure_data:0", taxTools, 10 * 60);
            }
        }


        /// <summary>
        /// 稿酬计算
        /// </summary>
        /// <param name="taxTools"></param>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        private void TaxCalculatorFunc2(List<TaxToolDto> taxTools, decimal x1, decimal x2, bool isCache = true)
        {
            if (taxTools == null) return;
            taxTools.ForEach(x =>
            {
                if (x.Salary <= 4000)
                {
                    x.TaxAmountPayable = (x.Salary - x1) * x2 * (1 - 0.3m);
                }
                else
                {
                    x.TaxAmountPayable = x.Salary * 0.8m * x2 * (1 - 0.3m);
                }
            });
            if (isCache)
                RedisHelper.Set($"Cache:Tax:disclosure_data:1", taxTools, 10 * 60);
        }


        /// <summary>
        /// 劳务计算
        /// </summary>
        /// <param name="taxTools"></param>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        private void TaxCalculatorFunc3(List<TaxToolDto> taxTools, decimal x1, decimal x2, bool isCache = true)
        {
            if (taxTools == null) return;
            taxTools.ForEach(x =>
            {
                if (x.Salary <= 4000)
                {
                    x.TaxAmountPayable = (x.Salary - 800) * 0.2m;
                }
                else if (x.Salary > 4000 && x.Salary * 0.8m <= 20000)
                {
                    x.TaxAmountPayable = x.Salary * (1 - 0.2m) * 0.2m;
                }
                else if (x.Salary > 20000 && x.Salary * 0.8m <= 50000)
                {
                    x.TaxAmountPayable = x.Salary * (1 - 0.2m) * 0.3m - 2000;
                }
                else if (x.Salary * 0.8m > 50000)
                {
                    x.TaxAmountPayable = x.Salary * (1 - 0.2m) * 0.4m - 7000;
                }
            });
            if (isCache)
                RedisHelper.Set($"Cache:Tax:disclosure_data:2", taxTools, 10 * 60);
        }

        /// <summary>
        /// 工资，薪酬计算
        /// </summary>
        /// <param name="taxTools"></param>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        private void TaxCalculatorFunc4(List<TaxSalaryDto> taxTools, decimal x1, decimal x2, bool isCache = true)
        {
            if (taxTools == null) return;
            
            taxTools.ForEach(x =>
            {
                if (x.TaxableIncome > 0)
                {
                    if (x.TaxableIncome <= 36000)
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.03m;
                    }
                    else if (x.TaxableIncome > 36000 && x.TaxableIncome <= 144000)
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.1m - 2520;
                    }
                    else if (x.TaxableIncome > 144000 && x.TaxableIncome <= 300000)
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.2m - 16920;
                    }
                    else if (x.TaxableIncome > 300000 && x.TaxableIncome <= 420000)
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.25m - 31920;
                    }
                    else if (x.TaxableIncome > 420000 && x.TaxableIncome <= 660000)
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.3m - 52920;
                    }
                    else if (x.TaxableIncome > 660000 && x.TaxableIncome <= 960000)
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.35m - 85920;
                    }
                    else
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.45m - 181920;
                    }
                }
            });
            if (isCache)
                RedisHelper.Set($"Cache:Salary:salary_data", taxTools, 10 * 60);
        }

        private void TaxCalculatorFunc4_ToMonth(List<TaxSalaryDto> taxTools, decimal x1, decimal x2, bool isCache = true)
        {
            if (taxTools == null) return;
            taxTools.ForEach(x =>
            {
                if (x.TaxableIncome > 0)
                {
                    if (x.TaxableIncome <= (36000 / 12))
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.03m;
                    }
                    else if (x.TaxableIncome > (36000 / 12) && x.TaxableIncome <= (144000 / 12))
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.1m - (2520 / 12);
                    }
                    else if (x.TaxableIncome > (144000 / 12) && x.TaxableIncome <= (300000 / 12))
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.2m - (16920 / 12);
                    }
                    else if (x.TaxableIncome > (300000 / 12) && x.TaxableIncome <= (420000 / 12))
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.25m - (31920 / 12);
                    }
                    else if (x.TaxableIncome > (420000 / 12) && x.TaxableIncome <= (660000 / 12))
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.3m - (52920 / 12);
                    }
                    else if (x.TaxableIncome > (660000 / 12) && x.TaxableIncome <= (960000 / 12))
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.35m - (85920 / 12);
                    }
                    else
                    {
                        x.TaxAmountPayable = x.TaxableIncome * 0.45m - (181920 / 12);
                    }
                }
            });
            if (isCache)
                RedisHelper.Set($"Cache:Salary:salary_data", taxTools, 10 * 60);
        }

        /// <summary>
        /// 租赁
        /// </summary>
        /// <param name="taxTools"></param>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        private void TaxCalculatorFunc5(List<TaxLeaseDto> taxTools, bool isCache = true)
        {
            if (taxTools == null) return;
            taxTools.ForEach(x =>
            {
                if (x.Salary <= 4000)
                {
                    x.TaxAmountPayable = (x.Salary - x.RepairAmount - 800) * 0.1m;
                }
                else if (x.Salary > 4000)
                {
                    x.TaxAmountPayable = (x.Salary - x.RepairAmount) * 0.8m * 0.1m;
                }

            });
            if (isCache)
                RedisHelper.Set($"Cache:Repair:repair_data", taxTools, 10 * 60);
        }

        /// <summary>
        /// 偶然所得
        /// </summary>
        /// <param name="taxTools"></param>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        private void TaxCalculatorFunc6(List<TaxToolDto> taxTools, decimal x1, decimal x2, bool isCache = true)
        {
            if (taxTools == null) return;
            taxTools.ForEach(x =>
            {

                x.TaxAmountPayable = x.Salary * 0.2m;


            });

            if (isCache)
                RedisHelper.Set($"Cache:Tax:disclosure_data:5", taxTools, 10 * 60);
        }


    }
}
