﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Utils;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 上课服务 [Obsolete]
    /// </summary>
    public class AttendClassService : IAttendClassService
    {
        #region 上课
        /// <summary>
        /// 上课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CreateAttendClassRecord(AttendClassDto dto, UserTicket user)
        {
            bool state = false;

            if (dto == null || dto.ClassList == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "对象是空的!" };
            if (!dto.CourseId.HasValue || !dto.SemesterId.HasValue) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "参数不能为空!" };

            //用于标识哪些班级一起上课
            long idCode = IdWorker.NextId();

            List<YssxClassRecord> list = new List<YssxClassRecord>();

            dto.ClassList.ForEach(x =>
            {
                YssxClassRecord model = new YssxClassRecord
                {
                    Id = IdWorker.NextId(),
                    IdentifyCode = idCode,
                    TenantId = user.TenantId,
                    CollegeId = x.CollegeId,
                    CollegeName = x.CollegeName,
                    ProfessionId = x.ProfessionId,
                    ProfessionName = x.ProfessionName,
                    ClassId = x.ClassId,
                    ClassName = x.ClassName,
                    TeacherUserId = user.Id,
                    CourseId = dto.CourseId.Value,
                    CourseName = dto.CourseName,
                    SemesterId = dto.SemesterId,
                    SemesterName = dto.SemesterName,
                    CreateBy = user.Id,
                    CreateTime = DateTime.Now,
                    UpdateBy = user.Id,
                    UpdateTime = DateTime.Now,
                };

                list.Add(model);
            });

            var resultList = await DbContext.FreeSql.GetRepository<YssxClassRecord>().InsertAsync(list);
            state = resultList != null;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 获取课堂记录
        /// <summary>
        /// 获取课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<YssxClassRecordViewModel>>> GetClassRecordList(YssxClassRecordQuery query, UserTicket user)
        {
            if (query == null || !query.DataSourceType.HasValue)
                return new ResponseContext<List<YssxClassRecordViewModel>> { Code = CommonConstants.ErrorCode, Data = null, Msg = "需指定数据来源!" };

            ResponseContext<List<YssxClassRecordViewModel>> response = new ResponseContext<List<YssxClassRecordViewModel>>();

            var select = DbContext.FreeSql.Select<YssxClassRecord>().From<YssxSemester>(
                (a, b) =>
                a.LeftJoin(x => x.SemesterId == b.Id)
                ).Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete);

            if (query.DataSourceType.HasValue && query.DataSourceType.Value == (int)DataSourceType.Oneself)
                select.Where((a, b) => a.CreateBy == user.Id);

            if (query != null && !string.IsNullOrWhiteSpace(query.Keyword))
                select.Where((a, b) => a.CourseName.Contains(query.Keyword) || a.ClassName.Contains(query.Keyword));
            if (query.StartTime.HasValue)
                select.Where((a, b) => a.CreateTime >= query.StartTime.Value.Date);
            if (query.EndTime.HasValue)
                select.Where((a, b) => a.CreateTime < query.EndTime.Value.AddDays(1).Date);
            if (query.SemesterId.HasValue)
                select.Where((a, b) => a.SemesterId == query.SemesterId);

            response.Data = await select.OrderByDescending((a, b) => a.CreateTime).ToListAsync((a, b) => new YssxClassRecordViewModel
            {
                RecordId = a.Id,
                IdentifyCode = a.IdentifyCode,
                CourseName = a.CourseName,
                TeacherName = user.RealName,
                AttendClassTime = a.CreateTime,
                ClassId = a.ClassId,
                ClassName = a.ClassName,
                SemesterName = b.Name,
            });

            return response;
        }
        #endregion

        #region 删除课堂记录
        /// <summary>
        /// 删除课堂记录
        /// </summary>
        /// <param name="identifyCode"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveClassRecord(long identifyCode, UserTicket user)
        {
            List<YssxClassRecord> list = await DbContext.FreeSql.GetRepository<YssxClassRecord>().Where(x => x.IdentifyCode == identifyCode).ToListAsync();
            if (list == null || list.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "没有该数据源!" };

            list.ForEach(x =>
            {
                x.IsDelete = 1;
                x.UpdateTime = DateTime.Now;
                x.UpdateBy = user.Id;
            });
            
            bool state = DbContext.FreeSql.GetRepository<YssxClassRecord>().UpdateDiy.SetSource(list).UpdateColumns(x => new { x.IsDelete, x.UpdateTime, x.UpdateBy }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }
        #endregion

        #region 导出课堂记录
        /// <summary>
        /// 导出课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<IActionResult> ExportClassRecord(YssxClassRecordQuery query, UserTicket user)
        {
            //查询课堂记录
            ResponseContext<List<YssxClassRecordViewModel>> sourceData = await GetClassRecordList(query, user);

            List<ExportYssxClassRecordViewModel> data = sourceData.Data.MapTo<List<ExportYssxClassRecordViewModel>>();
            
            byte[] ms = new byte[1024 * 2];
            if (data != null && data.Count > 0)
                ms = NPOIHelper<ExportYssxClassRecordViewModel>.OutputExcel(data, new string[] { "课堂记录 " });

            var provider = new FileExtensionContentTypeProvider();
            if (!provider.Mappings.TryGetValue(".xlsx", out var type))
            {
                throw new Exception("找不到可导出的文件类型");
            }

            return new FileContentResult(ms, type);
        }
        #endregion

    }
}
