﻿using System;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.IServices;

namespace Yssx.S.ServiceImpl
{
    /// <summary>
    /// 实时消息相关服务
    /// </summary>
    public class ImService : IImService
    {
        /// <summary>
        /// 获取服务器地址
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<string>> GetConnectServerUrl(Guid clientId,string clientIp)
        {
            var ip = clientIp;
            return await Task.Run(() =>
            {
                var wsserver = ImHelper.PrevConnectServer(clientId, ip);
                return new ResponseContext<string>(wsserver);
            });
        }

        /// <summary>
        /// 群聊,加入群聊
        /// </summary>
        /// <param name="groupId">群聊id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> JoinGroup(string groupId, Guid clientId)
        {
            return await Task.Run(() =>
            {
                ImHelper.JoinChan(clientId, groupId);
                return new ResponseContext<bool>(true);
            });
        }

        /// <summary>
        /// 群聊,离开群聊
        /// </summary>
        /// <param name="groupId">群聊id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> LeaveGroup(string groupId, Guid clientId)
        {
            return await Task.Run(() =>
            {
                ImHelper.LeaveChan(clientId, groupId);
                return new ResponseContext<bool>(true);
            });
        }

        /// <summary>
        /// 群聊，获取群聊所有客户端id
        /// </summary>
        /// <param name="groupId">群聊id</param>
        /// <returns></returns>
        public async Task<ResponseContext<Guid[]>> GetGroupClientList(string groupId)
        {
            return await Task.Run(() =>
            {
                var clientIds = ImHelper.GetChanClientList(groupId);
                return new ResponseContext<Guid[]>(clientIds);
            });
        }

        /// <summary>
        /// 群聊，发送群聊消息(群聊id,消息内容-groupid,message)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SendGroupMsg(ImDto model, Guid clientId)
        {
            if (string.IsNullOrEmpty(model.Message))
            {
                return new ResponseContext<bool>() { Code = CommonConstants.BadRequest, Msg = $"消息内容不能为空" };
            }

            var sendClientId = clientId;
            return await Task.Run(() =>
            {
                ImHelper.SendChanMessage(sendClientId, model.GroupId, model.Message);

                if (model.IsSave)//保存聊天记录
                {
                    RedisHelper.LPush(CommonConstants.ImMessagePre + model.GroupId, model.Message);
                }

                return new ResponseContext<bool>(true);
            });
        }

        /// <summary>
        /// 单聊(接收者,发送内容,是否需要回执-ReceiveClientId,message,isReceipt)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SendMsg(ImDto model, Guid clientId)
        {
            if (string.IsNullOrEmpty(model.Message))
            {
                return new ResponseContext<bool>() { Code = CommonConstants.BadRequest, Msg = $"消息内容不能为空" };
            }

            return await Task.Run(() =>
            {
                ImHelper.SendMessage(clientId, new Guid[] { model.ReceiveClientId }, model.Message, model.IsReceipt);
                return new ResponseContext<bool>(true);
            });
        }

        /// <summary>
        /// 获取群聊聊天记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<string[]>> GetGroupMsgHistory(ImMsgRequest model)
        {
            var msgList = await RedisHelper.LRangeAsync(CommonConstants.ImMessagePre + model.GroupId, model.Start, model.Lenth);
            return new ResponseContext<string[]>(msgList);
        }
    }
}
