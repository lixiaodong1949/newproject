﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;
using Yssx.Framework.AutoMapper;
using System.Linq;
using Yssx.S.Poco;
using NPOI.SS.Formula.Functions;
using MySqlX.XDevAPI.Common;
using System.Text;

namespace Yssx.S.ServiceImpl
{
    public class CourseTaskService : ICourseTaskService
    {
        /// <summary>
        /// 根据查询结果获取题型列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<QuestionTypeData>>> GetQuestionTypesByQuery(CourseTopicQuery query)
        {
            var result = new ResponseContext<List<QuestionTypeData>>();

            var data = await DbContext.FreeSql.GetRepository<YssxSectionTopic>().Select.From<YssxTopicPublic>(
                (a, b) =>
                a.InnerJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.CourseId == query.CourseId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(query.SectionId > 0, (a, b) => a.SectionId == query.SectionId)
                //.WhereIf(query.DifficultyLevel != -1, (a, b) => b.DifficultyLevel == query.DifficultyLevel)
                .WhereIf(query.CenterKnowledgePointId > 0, (a, b) => b.CenterKnowledgePointIds.Contains(query.CenterKnowledgePointId.ToString()))
                .GroupBy((a, b) => new { b.QuestionType })
                .ToListAsync(s => new QuestionTypeData { QuestionType = s.Key.QuestionType });

            result.Data = data;
            return result;
        }

        /// <summary>
        /// 获取习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<CourseTaskTopicDto>> GetCourseTopicList(CourseTopicQuery query, long userId)
        {
            var result = new PageResponse<CourseTaskTopicDto>();

            var select = DbContext.FreeSql.GetRepository<YssxSectionTopic>().Select.From<YssxTopicPublic>(
                (a, b) =>
                a.InnerJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.CourseId == query.CourseId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete);

            select.WhereIf(!string.IsNullOrEmpty(query.QuestionTypes), (a, b) => query.QuestionTypes.Contains(("," + (int)b.QuestionType).ToString() + ","));
            select.WhereIf(query.SectionId > 0, (a, b) => a.SectionId == query.SectionId);
            select.WhereIf(!string.IsNullOrEmpty(query.DifficultyLevel), (a, b) => query.DifficultyLevel.Contains(b.DifficultyLevel.ToString()));
            select.WhereIf(query.CenterKnowledgePointId > 0, (a, b) => b.CenterKnowledgePointIds.Contains(query.CenterKnowledgePointId.ToString()));

            long totalCount = 0;

            List<CourseTaskTopicDto> list = await select.Count(out totalCount).Page(query.PageIndex, query.PageSize).OrderBy((a, b) => b.Sort)
            .ToListAsync((a, b) => new CourseTaskTopicDto
            {
                Id = a.Id,
                CenterKnowledgePointNames = b.CenterKnowledgePointNames,
                QuestionId = a.QuestionId,
                SectionId = a.SectionId,
                QuestionName = b.Title,
                Sort = b.Sort,
                QuestionType = (int)b.QuestionType,
                CourseId = a.CourseId,
                Content = b.Content,
                Score = b.Score,
            });
            var cartQuestions = await DbContext.FreeSql.GetRepository<YssxTaskCart>()
                .Where(s => s.CourseId == query.CourseId && s.UserId == userId && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(s => s.QuestionId);
            foreach (var item in list)
            {
                item.IsExist = cartQuestions.Contains(item.QuestionId);
            }
            result.Data = list;
            result.Code = CommonConstants.SuccessCode;
            result.PageSize = query.PageSize;
            result.PageIndex = query.PageIndex;
            result.RecordCount = totalCount;
            return result;
        }

        #region 任务篮
        /// <summary>
        /// 加入任务篮
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddTaskCart(TaskCartRequest model, long userId)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            var taskCart = model.MapTo<YssxTaskCart>();
            taskCart.Id = IdWorker.NextId();
            taskCart.UserId = userId;
            taskCart.CreateBy = userId;
            taskCart.CreateTime = DateTime.Now;
            try
            {
                await DbContext.FreeSql.GetRepository<YssxTaskCart>().InsertAsync(taskCart);
                //await DbContext.FreeSql.Insert(taskCart).ExecuteAffrowsAsync();
            }
            catch (Exception ex)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Data = false;
                result.Msg = $"加入任务篮时出现异常{ex.Message}";
            }
            return result;
        }
        /// <summary>
        /// 批量加入任务篮
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddTaskCartBatch(BatchTaskCartRequest model, long userId)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            var taskCartList = model.TaskCarts.MapTo<List<YssxTaskCart>>();

            taskCartList.ForEach(taskCart =>
            {
                taskCart.Id = IdWorker.NextId();
                taskCart.UserId = userId;
                taskCart.CreateBy = userId;
                taskCart.CreateTime = DateTime.Now;
            });

            try
            {
                await DbContext.FreeSql.GetRepository<YssxTaskCart>().InsertAsync(taskCartList);
            }
            catch (Exception ex)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Data = false;
                result.Msg = $"批量加入任务篮时出现异常{ex.Message}";
            }
            return result;
        }
        /// <summary>
        /// 移除任务篮题目
        /// </summary>
        /// <param name="taskCartId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTaskCart(DeleteTaskCartRequest model, long userId)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);

            try
            {
                await DbContext.FreeSql.Delete<YssxTaskCart>().Where(s => s.CourseId == model.CourseId && model.QuestionIds.Contains(s.QuestionId.ToString()) && s.UserId == userId).ExecuteAffrowsAsync();
            }
            catch (Exception ex)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Data = false;
                result.Msg = $"移除任务篮时出现异常{ex.Message}";
            }
            return result;
        }
        /// <summary>
        /// 根据课程Id和题型移除任务篮
        /// </summary>
        /// <param name="taskCartId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTaskCartByQuestionType(long courseId, QuestionType questionType, long userId)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);

            try
            {
                await DbContext.FreeSql.Delete<YssxTaskCart>().Where(s => s.CourseId == courseId && s.UserId == userId && s.QuestionType == questionType).ExecuteAffrowsAsync();
            }
            catch (Exception ex)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Data = false;
                result.Msg = $"移除任务篮时出现异常{ex.Message}";
            }
            return result;
        }
        /// <summary>
        /// 清空任务篮
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ClearTaskCart(long courseId, long userId)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);

            try
            {
                await DbContext.FreeSql.Delete<YssxTaskCart>().Where(s => s.CourseId == courseId && s.UserId == userId).ExecuteAffrowsAsync();
            }
            catch (Exception ex)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Data = false;
                result.Msg = $"清空任务篮时出现异常{ex.Message}";
            }
            return result;
        }

        /// <summary>
        /// 获取任务篮列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<TaskCartTopicDto>>> GetTaskCartList(long userId, long courseId)
        {
            var result = new ResponseContext<List<TaskCartTopicDto>>();

            var data = await DbContext.FreeSql.GetRepository<YssxTaskCart>().Select.From<YssxTopicPublic>(
                (a, b) =>
                a.InnerJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.CourseId == courseId && a.UserId == userId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new TaskCartTopicDto
                {
                    QuestionType = a.QuestionType,
                    QuestionId = a.QuestionId,
                    QuetionName = a.QuestionName,
                    Score = a.Score
                });
            result.Data = data;
            return result;

        }
        /// <summary>
        /// 根据课程查找任务篮题目数量
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<long>> GetTaskCartCountByCourseId(long userId, long courseId)
        {
            var count = await DbContext.FreeSql.GetRepository<YssxTaskCart>()
                .Where(s => s.CourseId == courseId && s.UserId == userId && s.IsDelete == CommonConstants.IsNotDelete).CountAsync();

            return new ResponseContext<long>(count);
        }

        /// <summary>
        /// 任务篮弹窗
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<TaskCartQuestionCountDto>>> GetTaskCarts(long userId, long courseId)
        {
            var result = new ResponseContext<List<TaskCartQuestionCountDto>>();

            var data = await DbContext.FreeSql.GetRepository<YssxTaskCart>().Select.From<YssxTopicPublic>(
                (a, b) =>
                a.InnerJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.CourseId == courseId && a.UserId == userId && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .GroupBy((a, b) => new { a.QuestionType })
                .ToListAsync(a => new TaskCartQuestionCountDto
                {
                    QuestionType = a.Key.QuestionType,
                    QuestionCount = Convert.ToInt32("Count(distinct b.Id)"),
                    TotalScore = Convert.ToDecimal("Sum(b.Score)")
                });

            result.Data = data;
            return result;
        }

        /// <summary>
        /// 智能组卷生成题目
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RandomAddTaskCarts(RandomTaskCartRequest model, long userId)
        {
            var result = new ResponseContext<bool>(true);
            var cartQuestions = await DbContext.FreeSql.GetRepository<YssxTaskCart>()
                .Where(s => s.CourseId == model.CourseId && s.UserId == userId && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(s => s.QuestionId);
            string cartQuestionIds = string.Join(',', cartQuestions.ToArray());
            var taskCartQuestions = new List<TaskCartRequest>();
            var questionInfos = new List<TaskCartRequest>();
            try
            {
                if (!string.IsNullOrEmpty(model.CenterKnowledgePointIds))
                {
                    var sql = DbContext.FreeSql.GetRepository<YssxSectionTopic>().Select.From<YssxTopicPublic>(
                    (a, b) =>
                    a.InnerJoin(aa => aa.QuestionId == b.Id))
                    .Where((a, b) => a.CourseId == model.CourseId && !cartQuestionIds.Contains(a.QuestionId.ToString()) && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                    .ToSql("b.Id as QuestionId,b.questiontype,b.title as QuestionName,b.Score");

                    StringBuilder _sb = new StringBuilder(sql);
                    var knowledgePointIdList = model.CenterKnowledgePointIds.Split(',');

                    _sb.Append(" and ( ");

                    foreach (var item in knowledgePointIdList)
                    {
                        _sb.Append($" b.CenterKnowledgePointIds like '%{item}%' or ");
                    }
                    _sb.Append(")");

                    var sqlStr = _sb.ToString();

                    int index = sqlStr.LastIndexOf("or");
                    sqlStr = sqlStr.Substring(0, index) + sqlStr.Substring(index + 2, sqlStr.Length - (index + 2));

                    questionInfos = DbContext.FreeSql.Ado.Query<TaskCartRequest>(sqlStr);
                }
                if (!string.IsNullOrEmpty(model.SectionIds))
                {
                    questionInfos = await DbContext.FreeSql.GetRepository<YssxSectionTopic>().Select.From<YssxTopicPublic>(
                    (a, b) =>
                    a.InnerJoin(aa => aa.QuestionId == b.Id))
                    .Where((a, b) => a.CourseId == model.CourseId && !cartQuestionIds.Contains(a.QuestionId.ToString()) && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                    .Where((a, b) => model.SectionIds.Contains(a.SectionId.ToString()))
                    .ToListAsync<TaskCartRequest>();

                }
                if (questionInfos.Count > 0)
                {
                    foreach (var item in model.RandomQuestionTypes)
                    {
                        var questions = questionInfos.Where(s => s.QuestionType == item.QuestionType)
                            .OrderBy(s => new Random().NextDouble()).Take(item.QuestionCount).ToList();
                        taskCartQuestions.AddRange(questions);
                    }
                    var taskCartList = taskCartQuestions.MapTo<List<YssxTaskCart>>();

                    taskCartList.ForEach(taskCart =>
                    {
                        taskCart.Id = IdWorker.NextId();
                        taskCart.CourseId = model.CourseId;
                        taskCart.CourseName = model.CourseName;
                        taskCart.UserId = userId;
                        taskCart.CreateBy = userId;
                        taskCart.CreateTime = DateTime.Now;
                    });
                    await DbContext.FreeSql.GetRepository<YssxTaskCart>().InsertAsync(taskCartList);
                }
            }
            catch (Exception)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Data = false;
                result.Msg = "智能生成题目失败！";
            }
            return result;
        }

        //

        /// <summary>
        /// 智能组卷时根据查询条件返回题型及题目数量
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<RandomTaskCartQuestionTypeDto>>> GetRandomQuestionType(RandomTaskCartRequest model, long userId)
        {
            var result = new ResponseContext<List<RandomTaskCartQuestionTypeDto>>();
            var cartQuestions = await DbContext.FreeSql.GetRepository<YssxTaskCart>()
                .Where(s => s.CourseId == model.CourseId && s.UserId == userId && s.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync(s => s.QuestionId);
            string cartQuestionIds = string.Join(',', cartQuestions.ToArray());



            if (!string.IsNullOrEmpty(model.CenterKnowledgePointIds))
            {
                var sql = DbContext.FreeSql.GetRepository<YssxSectionTopic>().Select.From<YssxTopicPublic>(
                (a, b) =>
                a.InnerJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.CourseId == model.CourseId && !cartQuestionIds.Contains(a.QuestionId.ToString()) && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .ToSql("b.questiontype");

                StringBuilder _sb = new StringBuilder(sql);
                var knowledgePointIdList = model.CenterKnowledgePointIds.Split(',');

                _sb.Append(" and ( ");

                foreach (var item in knowledgePointIdList)
                {
                    _sb.Append($" b.CenterKnowledgePointIds like '%{item}%' or ");
                }
                _sb.Append(")");

                var sqlStr = _sb.ToString();

                int index = sqlStr.LastIndexOf("or");
                sqlStr = sqlStr.Substring(0, index) + sqlStr.Substring(index + 2, sqlStr.Length - (index + 2));

                var questionTypes = DbContext.FreeSql.Ado.Query<QuestionType>(sqlStr);

                var resultData = questionTypes.GroupBy(s => new { s }).Select(t => new RandomTaskCartQuestionTypeDto { QuestionType = t.Key.s, QuestionCount = t.Count() }).ToList();

                //.GroupBy((a, b) => new { b.QuestionType })
                //.ToListAsync(s => new RandomTaskCartQuestionTypeDto
                //{
                //    QuestionType = s.Key.QuestionType,
                //    QuestionCount = Convert.ToInt32("Count(a.Id)")
                //});
                result.Data = resultData;
            }
            if (!string.IsNullOrEmpty(model.SectionIds))
            {
                var data = await DbContext.FreeSql.GetRepository<YssxSectionTopic>().Select.From<YssxTopicPublic>(
                (a, b) =>
                a.InnerJoin(aa => aa.QuestionId == b.Id))
                .Where((a, b) => a.CourseId == model.CourseId && !cartQuestionIds.Contains(a.QuestionId.ToString()) && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete)
                .Where((a, b) => model.SectionIds.Contains(a.SectionId.ToString()))
                .GroupBy((a, b) => new { b.QuestionType })
                .ToListAsync(s => new RandomTaskCartQuestionTypeDto
                {
                    QuestionType = s.Key.QuestionType,
                    QuestionCount = Convert.ToInt32("Count(a.Id)")
                });
                result.Data = data;
            }

            return result;
        }

        //发布任务 YssxTask  ExamPaperCourse YssxTaskStudent YssxTaskTopic ExamCourseUserGrade

        /// <summary>
        /// 发布任务
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <param name="tenantId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> PublishCourseTask(PublicCourseTaskDto model, long currentUserId, long tenantId)
        {
            var dtNow = DateTime.Now;
            #region 校验
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
            if (model.StartTime > model.CloseTime)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "开始时间不得大于结束时间" };
            if (model.CloseTime < dtNow)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "结束时间不得小于当前时间" };
            //if (model.TotalMinutes <= 0)
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "考试时长必须大于0" };
            if (!model.ClassInfos.Any())
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级不能为空" };
            if (model.TopicDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择题目" };
            //if (model.TotalScore <= 0)
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "总分必须大于0" };
            if (model.TopicDetail.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "请选择题目" };
            if (model.TopicDetail.GroupBy(m => m.TopicId).Count() != model.TopicDetail.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "题目信息重复" };
            if (model.ClassInfos.GroupBy(m => m.ClassId).Count() != model.ClassInfos.Count)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "班级信息重复" };
            var totalScore = model.TopicDetail.Sum(m => m.Score);
            //if (model.TotalScore != totalScore)
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = $"卷面分与考核分数不一致，考核总分为{ totalScore }分，卷面分为{ model.TotalScore }" };
            //课程信息相关
            var course = DbContext.FreeSql.Select<YssxCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Id == model.CourseId).First();
            if (course == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到课程信息" };

            #endregion

            return await Task.Run(() =>
            {
                bool state = true;
                //处理数据
                if (model.Id == 0)
                {
                    #region 处理数据
                    var maxSort = DbContext.FreeSql.Select<YssxCourseTask>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId)
                        .OrderByDescending(m => m.Sort).First(m => m.Sort);
                    var rExamId = IdWorker.NextId();
                    TimeSpan ts = model.CloseTime.Subtract(model.StartTime);
                    int totalMinutes = Convert.ToInt32(ts.TotalMinutes);
                    //课业任务
                    var courseTask = new YssxCourseTask
                    {
                        Id = IdWorker.NextId(),
                        TaskType = model.TaskType,
                        TenantId = tenantId,
                        Name = model.Name,
                        StartTime = model.StartTime,
                        CloseTime = model.CloseTime,
                        TotalMinutes = totalMinutes, //model.TotalMinutes,
                        CourseId = model.CourseId,
                        Sort = maxSort + 1,
                        Content = model.Content,
                        ExamId = rExamId,
                        Status = LessonsTaskStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow,
                        IsCanCheckExam = model.IsCanCheckExam,
                        TaskTopicSortType = model.TaskTopicSortType,
                        CanShowAnswerBeforeEnd = model.CanShowAnswerBeforeEnd
                    };
                    //试卷
                    ExamPaperCourse coursePaper = new ExamPaperCourse
                    {
                        Id = rExamId,
                        Name = courseTask.Name,
                        TaskId = courseTask.Id,
                        TenantId = tenantId,
                        CourseId = courseTask.CourseId,
                        ExamType = CourseExamType.NewCourseTask,
                        CanShowAnswerBeforeEnd = model.CanShowAnswerBeforeEnd,
                        TotalScore = totalScore,
                        TotalQuestionCount = model.TopicDetail.Count,
                        TotalMinutes = totalMinutes,
                        BeginTime = courseTask.StartTime,
                        EndTime = courseTask.CloseTime,
                        ReleaseTime = dtNow,
                        Sort = courseTask.Sort,
                        IsRelease = true,
                        Status = ExamStatus.Wait,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    };
                    //班级
                    var taskClass = model.ClassInfos.Select(item => new YssxCourseTaskClass
                    {
                        Id = IdWorker.NextId(),
                        TaskId = courseTask.Id,
                        ClassId = item.ClassId,
                        ClassName = item.ClassName,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();

                    //List<YssxCourseTaskTopic> newTaskTopics = new List<YssxCourseTaskTopic>();
                    //题目
                    var taskTopics = model.TopicDetail.Select(m => new YssxCourseTaskTopic
                    {
                        Id = IdWorker.NextId(),
                        TaskId = courseTask.Id,
                        TopicId = m.TopicId,
                        QuestionType = m.QuestionType,
                        Score = m.Score,
                        Sort = m.Sort,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    if (model.TaskTopicSortType == TaskTopicSortType.Random)
                    {
                        Random random = new Random();

                        foreach (YssxCourseTaskTopic item in taskTopics)
                        {
                            var randomNum = random.Next(taskTopics.Count);
                            item.Sort = randomNum;
                        }
                    }
                    else
                    {
                        var sortNum = 0;
                        foreach (YssxCourseTaskTopic item in taskTopics)
                        {
                            item.Sort = sortNum;
                            sortNum++;
                        }
                    }
                    var taskTopicIds = string.Join(',', taskTopics.Select(s => s.TopicId).ToArray());
                    #endregion

                    #region 新增数据
                    DbContext.FreeSql.Transaction(() =>
                    {
                        //课业任务
                        DbContext.FreeSql.GetRepository<YssxCourseTask>().Insert(courseTask);
                        //试卷
                        DbContext.FreeSql.GetRepository<ExamPaperCourse>().Insert(coursePaper);
                        //班级
                        if (taskClass.Count > 0)
                            DbContext.FreeSql.Insert<YssxCourseTaskClass>().AppendData(taskClass).ExecuteAffrows();
                        //题目
                        if (taskTopics.Count > 0)
                            DbContext.FreeSql.GetRepository<YssxCourseTaskTopic>().Insert(taskTopics);
                        //移除任务篮题目
                        if (!string.IsNullOrEmpty(taskTopicIds))
                            DbContext.FreeSql.Delete<YssxTaskCart>()
                            .Where(s => s.CourseId == model.CourseId && taskTopicIds.Contains(s.QuestionId.ToString()) && s.UserId == currentUserId).ExecuteAffrowsAsync();
                    });
                    #endregion
                }
                else
                {
                    #region 处理数据
                    var courseTask = DbContext.FreeSql.Select<YssxCourseTask>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == model.Id).First();
                    if (courseTask == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到课业任务" };

                    ExamPaperCourse rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == courseTask.ExamId).First();
                    if (rPaperEntity == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };

                    //课业任务
                    //rTaskEntity.Status = LessonsTaskStatus.Wait;
                    courseTask.Name = model.Name;
                    courseTask.StartTime = model.StartTime;
                    courseTask.CloseTime = model.CloseTime;
                    courseTask.TotalMinutes = model.TotalMinutes;
                    courseTask.Content = model.Content;
                    courseTask.UpdateBy = currentUserId;
                    courseTask.UpdateTime = dtNow;
                    //试卷
                    //rPaperEntity.Status = ExamStatus.Wait;
                    rPaperEntity.BeginTime = courseTask.StartTime;
                    rPaperEntity.EndTime = courseTask.CloseTime;
                    rPaperEntity.TotalScore = totalScore;
                    rPaperEntity.TotalQuestionCount = model.TopicDetail.Count;
                    rPaperEntity.TotalMinutes = model.TotalMinutes;
                    rPaperEntity.UpdateBy = currentUserId;
                    rPaperEntity.UpdateTime = dtNow;

                    #region 题目
                    //取新增的题目
                    var addTopicData = model.TopicDetail.Where(m => m.Id == 0).Select(m => new YssxCourseTaskTopic
                    {
                        Id = IdWorker.NextId(),
                        TaskId = courseTask.Id,
                        TopicId = m.TopicId,
                        QuestionType = m.QuestionType,
                        Score = m.Score,
                        Sort = m.Sort,
                        CreateBy = currentUserId,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //取修改的题目
                    var updateTopicData = model.TopicDetail.Where(m => m.Id > 0).Select(m => new YssxCourseTaskTopic
                    {
                        Id = m.Id,
                        Sort = m.Sort,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    }).ToList();
                    //取删除的题目
                    var topicIds = model.TopicDetail.Select(m => m.TopicId).ToList();
                    var delTopicData = DbContext.FreeSql.Select<YssxCourseTaskTopic>()
                        .Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.TaskId == courseTask.Id && !topicIds.Contains(m.TopicId))
                        .ToList(m => new YssxCourseTaskTopic
                        {
                            Id = m.Id,
                            IsDelete = CommonConstants.IsDelete,
                            UpdateBy = currentUserId,
                            UpdateTime = dtNow
                        });
                    #endregion

                    #endregion

                    #region 更新数据
                    DbContext.FreeSql.Transaction(() =>
                    {
                        //课业任务
                        DbContext.FreeSql.Update<YssxCourseTask>().SetSource(courseTask).UpdateColumns(a => new
                        {
                            a.Name,
                            a.StartTime,
                            a.CloseTime,
                            a.TotalMinutes,
                            a.Content,
                            a.UpdateBy,
                            a.UpdateTime
                        }).ExecuteAffrows();
                        //试卷信息
                        var resUpd = DbContext.FreeSql.Update<ExamPaperCourse>().SetSource(rPaperEntity).UpdateColumns(a => new
                        {
                            a.BeginTime,
                            a.EndTime,
                            a.TotalScore,
                            a.TotalQuestionCount,
                            a.TotalMinutes,
                            a.UpdateBy,
                            a.UpdateTime
                        }).ExecuteAffrows();

                        //新增的题目
                        if (addTopicData.Count > 0)
                            DbContext.FreeSql.Insert<YssxCourseTaskTopic>().AppendData(addTopicData).ExecuteAffrows();
                        //修改的题目
                        if (updateTopicData.Count > 0)
                            DbContext.FreeSql.Update<YssxCourseTaskTopic>().SetSource(updateTopicData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.Sort }).ExecuteAffrows();
                        //删除的题目
                        if (delTopicData.Count > 0)
                            DbContext.FreeSql.Update<YssxCourseTaskTopic>().SetSource(delTopicData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();

                    });
                    #endregion
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }


        /// <summary>
        /// 课程任务基础信息修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> EditTaskBasicInfo(EditTaskBasicInfoDto model, long currentUserId)
        {
            var dtNow = DateTime.Now;
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
            if (model.IsUpdateExamTime)
            {
                if (model.StartTime > model.CloseTime)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "开始时间不得大于结束时间" };
                if (model.CloseTime < dtNow)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "结束时间不得小于当前时间" };
            }

            return await Task.Run(() =>
            {
                bool state = true;

                #region 处理数据
                // 处理数据
                ExamPaperCourse rPaperEntity = new ExamPaperCourse();
                var rTaskEntity = DbContext.FreeSql.GetRepository<YssxCourseTask>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (rTaskEntity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "未找到该课业任务" };
                if (rTaskEntity.ExamId == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "此课业任务数据异常，试卷ID为0" };
                rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == rTaskEntity.ExamId).First();
                if (rPaperEntity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };
                TimeSpan ts = model.CloseTime.Subtract(model.StartTime);
                int totalMinutes = Convert.ToInt32(ts.TotalMinutes);
                //课业任务
                if (model.CloseTime > DateTime.Now && model.StartTime <= DateTime.Now && rTaskEntity.CloseTime != model.CloseTime)
                {
                    rTaskEntity.Status = LessonsTaskStatus.Started;
                }
                else if (model.StartTime > DateTime.Now && model.CloseTime != model.CloseTime)
                {
                    rTaskEntity.Status = LessonsTaskStatus.Wait;
                }
                rTaskEntity.Name = model.Name;
                rTaskEntity.StartTime = model.StartTime;
                rTaskEntity.CloseTime = model.CloseTime;
                //rTaskEntity.Status = (model.IsUpdateExamTime && model.CloseTime > DateTime.Now) ? LessonsTaskStatus.Started : rTaskEntity.Status;
                rTaskEntity.TotalMinutes = totalMinutes;
                rTaskEntity.IsCanCheckExam = model.IsCanCheckExam;
                rTaskEntity.UpdateBy = currentUserId;
                rTaskEntity.UpdateTime = dtNow;
                rTaskEntity.Content = model.Content;
                rTaskEntity.CanShowAnswerBeforeEnd = model.CanShowAnswerBeforeEnd;

                //试卷
                rPaperEntity.Name = model.Name;
                rPaperEntity.BeginTime = model.StartTime;
                rPaperEntity.EndTime = model.CloseTime;
                rPaperEntity.TotalMinutes = totalMinutes;
                rPaperEntity.UpdateBy = currentUserId;
                rPaperEntity.UpdateTime = dtNow;
                rPaperEntity.CanShowAnswerBeforeEnd = model.CanShowAnswerBeforeEnd;
                //班级
                var taskClass = model.ClassInfos.Select(item => new YssxCourseTaskClass
                {
                    Id = IdWorker.NextId(),
                    TaskId = model.Id,
                    ClassId = item.ClassId,
                    ClassName = item.ClassName,
                    CreateBy = currentUserId,
                    UpdateBy = currentUserId,
                    UpdateTime = dtNow
                }).ToList();
                //作答记录 - 更新
                var rUpdExamGradeData = DbContext.FreeSql.GetRepository<ExamCourseUserGrade>()
                    .Where(x => x.TaskId == model.Id && x.IsDelete == CommonConstants.IsNotDelete).ToList(m => new ExamCourseUserGrade
                    {
                        Id = m.Id,
                        LeftSeconds = totalMinutes * 60,
                        RecordTime = model.StartTime,
                        UpdateBy = currentUserId,
                        UpdateTime = dtNow
                    });
                #endregion

                #region 更新数据
                DbContext.FreeSql.Transaction(() =>
                {
                    //课业任务
                    DbContext.FreeSql.Update<YssxCourseTask>().SetSource(rTaskEntity).UpdateColumns(a => new
                    {
                        a.Name,
                        a.StartTime,
                        a.CloseTime,
                        a.TotalMinutes,
                        a.UpdateBy,
                        a.UpdateTime,
                        a.Content,
                        a.IsCanCheckExam,
                        a.CanShowAnswerBeforeEnd,
                        a.Status
                    }).ExecuteAffrows();
                    //试卷
                    var resUpd = DbContext.FreeSql.Update<ExamPaperCourse>().SetSource(rPaperEntity).UpdateColumns(a => new
                    {
                        a.Name,
                        a.BeginTime,
                        a.EndTime,
                        a.TotalMinutes,
                        a.UpdateBy,
                        a.UpdateTime,
                        a.CanShowAnswerBeforeEnd,
                    }).ExecuteAffrows();
                    //班级(删除原有班级)
                    DbContext.FreeSql.Delete<YssxCourseTaskClass>().Where(s => s.TaskId == model.Id).ExecuteAffrows();
                    DbContext.FreeSql.GetRepository<YssxCourseTaskClass>().Insert(taskClass);
                    //作答记录 - 更新
                    if (rUpdExamGradeData.Count > 0)
                        DbContext.FreeSql.Update<ExamCourseUserGrade>().SetSource(rUpdExamGradeData).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.LeftSeconds, a.RecordTime }).ExecuteAffrows();

                });
                RedisHelper.Del($"{CommonConstants.Cache_GetExamCourseById}{rPaperEntity.Id}");
                #endregion
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }
        /// <summary>
        /// 查询课业任务列表 - 教师端
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<CourseTaskListDto>> GetTeacherCourseTaskList(CourseTaskQueryRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var nowTime = DateTime.Now;
                long totalCount = 0;
                //查询数据
                var data = DbContext.FreeSql.GetGuidRepository<YssxCourseTask>().Select.From<YssxCourse>((a, b) => a.LeftJoin(aa => aa.CourseId == b.Id))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.CreateBy == currentUserId)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b) => a.Name.Contains(model.Name))
                    .WhereIf(model.CourseId > 0, (a, b) => a.CourseId == model.CourseId)
                    .WhereIf(model.Status == 0, (a, b) => a.StartTime > nowTime)
                    .WhereIf(model.Status == 1, (a, b) => a.StartTime <= nowTime && a.CloseTime > nowTime)
                    .WhereIf(model.Status == 2, (a, b) => a.CloseTime <= nowTime)
                    .Count(out totalCount).Page(model.PageIndex, model.PageSize)
                    .OrderByDescending((a, b) => a.Sort).ToList((a, b) => new CourseTaskListDto
                    {
                        Id = a.Id,
                        TaskName = a.Name,
                        CourseId = b.Id,
                        CourseName = b.CourseName,
                        StartTime = a.StartTime,
                        CloseTime = a.CloseTime,
                        Status = a.Status,
                        Content = a.Content,
                        ExamId = a.ExamId,
                        IsCanCheckExam = a.IsCanCheckExam,
                        CanShowAnswerBeforeEnd = a.CanShowAnswerBeforeEnd,
                        Sort = a.Sort,
                    });
                var taskIds = "";
                if (data.Count > 0)
                    taskIds = string.Join(',', data.Select(s => s.Id).ToArray());
                var taskClassList = DbContext.FreeSql.GetRepository<YssxCourseTaskClass>().Where(s => taskIds.Contains(s.TaskId.ToString())).ToList();
                foreach (var item in data)
                {
                    item.ClassInfos = taskClassList.Where(s => s.TaskId == item.Id).Select(s => new CourseTaskClassInfo
                    {
                        ClassId = s.ClassId,
                        ClassName = s.ClassName
                    }).ToList();
                    item.IsExistShortQuestion = DbContext.FreeSql.GetRepository<YssxCourseTaskTopic>()
                    .Where(s => s.QuestionType == QuestionType.ShortAnswerQuestion && s.TaskId == item.Id && s.IsDelete == CommonConstants.IsNotDelete).Any();
                }
                return new PageResponse<CourseTaskListDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = data };
            });
        }

        /// <summary>
        /// 查询课业任务列表 - 学生端
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        public async Task<PageResponse<StudentCourseListDto>> GetStudentCourseTaskList(StudentCourseTaskRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                long classId = 0;
                var student = DbContext.FreeSql.GetRepository<YssxStudent>().Where(s => s.UserId == currentUserId && s.IsDelete == CommonConstants.IsNotDelete).First();
                if (student != null)
                    classId = student.ClassId;
                var nowTime = DateTime.Now;
                long totalCount = 0;
                var selectData = DbContext.FreeSql.GetRepository<YssxCourseTask>().Select
                    .From<YssxCourse, YssxCourseTaskClass, ExamCourseUserGrade, ExamPaperCourse>((a, b, c, d, e) =>
                        a.InnerJoin(aa => aa.CourseId == b.Id)
                        .InnerJoin(aa => aa.Id == c.TaskId && c.IsDelete == CommonConstants.IsNotDelete)
                        .LeftJoin(aa => aa.Id == d.TaskId && d.UserId == currentUserId && d.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => aa.Id == e.TaskId && e.IsDelete == CommonConstants.IsNotDelete))

                    .Where((a, b, c, d, e) => a.IsDelete == CommonConstants.IsNotDelete && c.ClassId == classId)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b, c, d, e) => a.Name.Contains(model.Name))
                    .WhereIf(model.CourseId > 0, (a, b, c, d, e) => a.CourseId == model.CourseId)
                    .WhereIf(model.Status == 0, (a, b, c, d, e) => a.StartTime > nowTime)
                    .WhereIf(model.Status == 1, (a, b, c, d, e) => a.StartTime <= nowTime && a.CloseTime > nowTime)
                    .WhereIf(model.Status == 2, (a, b, c, d, e) => a.CloseTime <= nowTime)
                    .Count(out totalCount).Page(model.PageIndex, model.PageSize)
                    .OrderByDescending((a, b, c, d, e) => a.CreateTime).ToList((a, b, c, d, e) => new StudentCourseListDto
                    {
                        Id = a.Id,
                        Name = a.Name,
                        //TaskType = a.TaskType,
                        CourseId = a.CourseId,
                        CourseName = b.CourseName,
                        StartTime = a.StartTime,
                        CloseTime = a.CloseTime,
                        TotalMinutes = a.TotalMinutes,
                        Status = a.Status,
                        GradeStatus = d.Status,
                        Content = a.Content,
                        Sort = a.Sort,
                        ExamId = a.ExamId,
                        GradeId = d.Id,
                        ErrorCount = d.ErrorCount,
                        ExamPaperScore = e.TotalScore,
                        Score = d.Score,
                        IsCanCheckExam = a.IsCanCheckExam,
                        CanShowAnswerBeforeEnd=a.CanShowAnswerBeforeEnd
                    });

                return new PageResponse<StudentCourseListDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }
        /// <summary>
        /// 删除课业任务
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveTask(long id, long currentUserId)
        {
            var dtNow = DateTime.Now;

            var taskEntity = await DbContext.FreeSql.GetRepository<YssxCourseTask>().Where(x => x.Id == id && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (taskEntity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该课业任务!" };
            taskEntity.IsDelete = CommonConstants.IsDelete;
            taskEntity.UpdateBy = currentUserId;
            taskEntity.UpdateTime = dtNow;

            var paperEntity = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (paperEntity != null)
            {
                paperEntity.IsDelete = CommonConstants.IsDelete;
                paperEntity.UpdateBy = currentUserId;
                paperEntity.UpdateTime = dtNow;
            }
            var listTaskClass = await DbContext.FreeSql.GetRepository<YssxCourseTaskClass>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync(m => new YssxCourseTaskClass
            {
                Id = m.Id,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = currentUserId,
                UpdateTime = dtNow
            });
            var listTaskTopic = await DbContext.FreeSql.GetRepository<YssxCourseTaskTopic>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync(m => new YssxCourseTaskTopic
            {
                Id = m.Id,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = currentUserId,
                UpdateTime = dtNow
            });
            var listExamGrade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync(m => new ExamCourseUserGrade
            {
                Id = m.Id,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = currentUserId,
                UpdateTime = dtNow
            });

            return await Task.Run(() =>
            {
                bool state = true;
                try
                {
                    DbContext.FreeSql.Transaction(() =>
                    {
                        //课业任务
                        state = DbContext.FreeSql.GetRepository<YssxCourseTask>().UpdateDiy.SetSource(taskEntity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
                        //试卷
                        if (paperEntity != null)
                            DbContext.FreeSql.GetRepository<ExamPaperCourse>().UpdateDiy.SetSource(paperEntity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                        //学生
                        if (listTaskClass.Count > 0)
                            DbContext.FreeSql.Update<YssxCourseTaskClass>().SetSource(listTaskClass).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                        //题目
                        if (listTaskTopic.Count > 0)
                            DbContext.FreeSql.Update<YssxCourseTaskTopic>().SetSource(listTaskTopic).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();
                        //作答记录
                        if (listExamGrade.Count > 0)
                            DbContext.FreeSql.Update<ExamCourseUserGrade>().SetSource(listExamGrade).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrows();

                    });
                }
                catch (Exception)
                {
                    state = false;
                }

                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
            });
        }
        /// <summary>
        /// 结束课业任务
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ClosedTask(long id, long currentUserId)
        {
            var dtNow = DateTime.Now;

            var taskEntity = await DbContext.FreeSql.GetRepository<YssxCourseTask>().Where(x => x.Id == id && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (taskEntity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该课业任务!" };
            taskEntity.Status = LessonsTaskStatus.End;
            taskEntity.UpdateBy = currentUserId;
            taskEntity.UpdateTime = dtNow;

            var paperEntity = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (paperEntity != null)
            {
                paperEntity.Status = ExamStatus.End;
                paperEntity.UpdateBy = currentUserId;
                paperEntity.UpdateTime = dtNow;
            }
            var listExamGrade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync(m => new ExamCourseUserGrade
            {
                Id = m.Id,
                Status = StudentExamStatus.End,
                UpdateBy = currentUserId,
                UpdateTime = dtNow
            });

            return await Task.Run(() =>
            {
                bool state = true;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    //课业任务
                    state = DbContext.FreeSql.GetRepository<YssxCourseTask>().UpdateDiy.SetSource(taskEntity).UpdateColumns(x => new { x.Status, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
                    //试卷
                    if (paperEntity != null)
                        DbContext.FreeSql.GetRepository<ExamPaperCourse>().UpdateDiy.SetSource(paperEntity).UpdateColumns(x => new { x.Status, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                    //作答记录
                    if (listExamGrade.Count > 0)
                        DbContext.FreeSql.Update<ExamCourseUserGrade>().SetSource(listExamGrade).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.Status }).ExecuteAffrows();

                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
            });
        }
        /// <summary>
        /// 一键交卷
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BatchSubmitExam(long id, long currentUserId)
        {
            var result = new ResponseContext<bool>(true);
            var dtNow = DateTime.Now;
            var taskEntity = await DbContext.FreeSql.GetRepository<YssxCourseTask>().Where(x => x.Id == id && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (taskEntity == null)
            {
                result.Code = CommonConstants.ErrorCode;
                result.Msg = "操作失败，未找到该课业任务!";
            }
            taskEntity.Status = LessonsTaskStatus.End;
            taskEntity.UpdateBy = currentUserId;
            taskEntity.UpdateTime = dtNow;

            var exam = await DbContext.FreeSql.GetRepository<ExamPaperCourse>().Where(x => x.TaskId == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (exam != null)
            {
                exam.Status = ExamStatus.End;
                exam.UpdateBy = currentUserId;
                exam.UpdateTime = dtNow;
            }
            var listExamGrade = await DbContext.FreeSql.GetRepository<ExamCourseUserGrade>()
                .Where(x => x.TaskId == id && x.Status != StudentExamStatus.End && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            var updateGradeList = new List<ExamCourseUserGrade>();

            foreach (var grade in listExamGrade)
            {
                //提交考卷
                SubmitExamGrade(exam, grade, true);
                updateGradeList.Add(grade);
            }

            return await Task.Run(() =>
            {
                try
                {
                    DbContext.FreeSql.Transaction(() =>
                    {
                        //课业任务
                        DbContext.FreeSql.GetRepository<YssxCourseTask>().UpdateDiy.SetSource(taskEntity).UpdateColumns(x => new { x.Status, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                        //试卷
                        if (exam != null)
                            DbContext.FreeSql.GetRepository<ExamPaperCourse>().UpdateDiy.SetSource(exam).UpdateColumns(x => new { x.Status, x.UpdateBy, x.UpdateTime }).ExecuteAffrows();
                        //作答记录
                        if (updateGradeList.Any())
                            DbContext.FreeSql.GetRepository<ExamCourseUserGrade>().UpdateDiy.SetSource(updateGradeList).ExecuteAffrows();
                    });
                }
                catch (Exception ex)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = "一件交卷数据异常，请联系管理员";
                }

                return result;
            });

        }
        /// <summary>
        /// 提交考试
        /// </summary>
        /// <param name="exam"></param>
        /// <param name="grade"></param>
        /// <param name="isBatchSubmit">true时代表老师一件交卷</param>
        private void SubmitExamGrade(ExamPaperCourse exam, ExamCourseUserGrade grade, bool isBatchSubmit = false)
        {
            grade.Status = StudentExamStatus.End;
            grade.IsManualSubmit = !isBatchSubmit;
            grade.UpdateTime = DateTime.Now;
            grade.SubmitTime = DateTime.Now;

            switch (exam.ExamType)
            {
                case CourseExamType.CourceTest:
                    grade.UsedSeconds = (grade.SubmitTime.Value - grade.CreateTime).TotalSeconds;
                    break;
                case CourseExamType.ClassExam:
                case CourseExamType.ClassHomeWork:
                case CourseExamType.ClassTask:
                case CourseExamType.ClassTest:
                case CourseExamType.NewCourseTask:
                    grade.UsedSeconds = (grade.SubmitTime.Value - grade.RecordTime).TotalSeconds;
                    break;
            }

            var totalQuestionCount = exam.TotalQuestionCount;

            //Done:计算考卷分数，返回考试成绩信息
            var answerCount = DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Where(a => a.GradeId == grade.Id && a.QuestionId == a.ParentQuestionId)
               .Master().ToList(a => new { a.QuestionId, a.Status, a.Score });
            grade.CorrectCount = answerCount.Count(a => a.Status == AnswerResultStatus.Right);
            grade.ErrorCount = answerCount.Count(a => a.Status == AnswerResultStatus.Error);
            grade.PartRightCount = answerCount.Count(a => a.Status == AnswerResultStatus.PartRight);
            grade.BlankCount = totalQuestionCount - answerCount.Count;

            if (grade.BlankCount <= 0)
                grade.BlankCount = 0;
            grade.Score = answerCount.Sum(a => a.Score);

        }
        /// <summary>
        /// 预览课程任务信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<CourseTaskInfoDto>> GetCourseTaskInfoById(long taskId, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var result = new ResponseContext<CourseTaskInfoDto>();
                YssxCourseTask taskEntity = DbContext.FreeSql.GetRepository<YssxCourseTask>().Where(x => x.Id == taskId && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (taskEntity == null)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = "操作失败，未找到该任务数据!";
                    return result;
                }
                var rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>()
                .Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == taskEntity.ExamId).First();
                if (rPaperEntity == null)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = "找不到试卷数据!";
                    return result;
                }
                var rCourseEntity = DbContext.FreeSql.Select<YssxCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.Id == taskEntity.CourseId).First();
                if (rCourseEntity == null)
                {
                    result.Code = CommonConstants.ErrorCode;
                    result.Msg = "任务关联的课程找不到!";
                    return result;
                }

                var taskInfoData = taskEntity.MapTo<CourseTaskInfoDto>();
                taskInfoData.TotalScore = rPaperEntity.TotalScore;
                taskInfoData.CourseName = rCourseEntity.CourseName;
                //查询题目列表
                var rTopicDetail = DbContext.FreeSql.GetRepository<YssxCourseTaskTopic>().Select
                    .From<YssxTopicPublic>((a, b) => a.InnerJoin(aa => aa.TopicId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.TaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy((a,b)=>b.Sort)
                    .ToList((a, b) => new CourseTaskTopic
                    {
                        Id = a.Id,
                        TopicId = a.TopicId,
                        QuestionType = a.QuestionType,
                        Score = a.Score,
                        Sort = a.Sort,
                        Title = b.Title
                    });
                taskInfoData.TopicDetail = rTopicDetail;
                result.Code = CommonConstants.SuccessCode;
                result.Msg = "成功";
                result.Data = taskInfoData;
                return result;
            });
        }
        /// <summary>
        /// 课业任务统计信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<CourseTaskGradeReportDto>> GetCourseGradeDetailByTask(long taskId, string classIds, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var taskEntity = DbContext.FreeSql.GetRepository<YssxCourseTask>().Where(x => x.Id == taskId && x.CreateBy == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (taskEntity == null)
                    return new ResponseContext<CourseTaskGradeReportDto> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该数据!" };
                var rPaperEntity = DbContext.FreeSql.Select<ExamPaperCourse>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == currentUserId && m.Id == taskEntity.ExamId).First();
                if (rPaperEntity == null)
                    return new ResponseContext<CourseTaskGradeReportDto> { Code = CommonConstants.ErrorCode, Msg = "找不到试卷数据" };

                var rTaskGradeEntity = new CourseTaskGradeReportDto();
                //课业任务
                rTaskGradeEntity = taskEntity.MapTo<CourseTaskGradeReportDto>();
                rTaskGradeEntity.TotalScore = rPaperEntity.TotalScore;

                //查询课程名称
                rTaskGradeEntity.CourseName = DbContext.FreeSql.GetRepository<YssxCourse>().Where(x => x.Id == rTaskGradeEntity.CourseId).First()?.CourseName;

                var gradeDetails = DbContext.FreeSql.GetRepository<YssxCourseTaskClass>().Select
                        .From<YssxStudent, ExamCourseUserGrade, ExamCourseUserGradeDetail, YssxUser>((a, b, c, d, e) =>
                               a.LeftJoin(aa => aa.ClassId == b.ClassId && b.IsDelete == CommonConstants.IsNotDelete)
                               .LeftJoin(aa => b.UserId == c.UserId && c.TaskId == taskId && c.IsDelete == CommonConstants.IsNotDelete)
                               .LeftJoin(aa => c.Id == d.GradeId && d.IsDelete == CommonConstants.IsNotDelete && d.QuestionId == d.ParentQuestionId)
                               .LeftJoin(aa => b.UserId == e.Id && e.IsDelete == CommonConstants.IsNotDelete))
                        .Where((a, b, c, d, e) => a.TaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete)
                        .WhereIf(!string.IsNullOrEmpty(classIds), (a, b, c, d, e) => classIds.Contains(a.ClassId.ToString()))
                        //.GroupBy((a,b,c,d,e)=>new {a.GradeId,c.RealName,d.StudentNo,e.Name,e.Id,b.Status,b.UsedSeconds})                        
                        .ToList((a, b, c, d, e) => new
                        {
                            GradeId = c.Id,
                            UserId = e.Id,
                            UserName = e.RealName,
                            ClassId = a.ClassId,
                            ClassName = a.ClassName,
                            StudentNo = b.StudentNo,
                            Score = d.Score,
                            Status = d.Status,
                            UsedSeconds = c.UsedSeconds,
                            ExamStatus = c.Status
                        });
                var resultData = gradeDetails.GroupBy(s => new { s.GradeId, s.UserId, s.UserName, s.ClassId, s.ClassName, s.StudentNo, s.ExamStatus, s.UsedSeconds })
                                .Select(s => new CourseTaskGradeReportGradeDto
                                {
                                    GradeId = s.Key.GradeId,
                                    UserName = s.Key.UserName == null ? "" : s.Key.UserName,
                                    ClassId = s.Key.ClassId,
                                    ClassName = s.Key.ClassName,
                                    StudentNo = s.Key.StudentNo == null ? "" : s.Key.StudentNo,
                                    Score = s.Sum(c => c.Score),
                                    Status = s.Key.ExamStatus,
                                    UsedSeconds = s.Key.UsedSeconds
                                }).ToList();
                var correctCount = 0;
                var errorCount = 0;

                resultData = resultData.OrderByDescending(m => m.Score).ToList();

                resultData.ForEach(s =>
                {
                    s.CorrectCount = gradeDetails.Where(d => d.Status == AnswerResultStatus.Right && d.GradeId == s.GradeId).Count();
                    s.ErrorCount = gradeDetails.Where(d => (d.Status == AnswerResultStatus.Error || d.Status == AnswerResultStatus.PartRight) && d.GradeId == s.GradeId).Count();
                    s.TotalQuestionCount = rPaperEntity.TotalQuestionCount;
                    s.DoQuestionCount = s.CorrectCount + s.ErrorCount;
                });
                int rank = 1;
                int doQuestionStudentCount = resultData.Where(s => (s.CorrectCount + s.ErrorCount) > 0).Count();
                resultData.ForEach(s =>
                {
                    if (s.DoQuestionCount > 0)
                    {
                        s.Ranking = rank;
                        rank++;
                    }
                    else
                    {
                        s.Ranking = doQuestionStudentCount + 1;
                        doQuestionStudentCount++;
                    }
                });

                resultData = resultData.OrderByDescending(m => m.StudentNo).ToList();

                rTaskGradeEntity.GradeDetail = resultData;
                rTaskGradeEntity.SubmitedCount = resultData.Where(d => d.Status == StudentExamStatus.End).Count();
                rTaskGradeEntity.NoSubmitedCount = resultData.Where(d => d.Status != StudentExamStatus.End && d.GradeId > 0).Count();
                rTaskGradeEntity.MissCount = resultData.Where(d => d.GradeId <= 0).Count();
                //计算数据
                rTaskGradeEntity.MaxScore = resultData.Count == 0 ? 0 : Math.Round(resultData.Max(m => m.Score), 4);
                rTaskGradeEntity.MinScore = resultData.Count == 0 ? 0 : Math.Round(resultData.Min(m => m.Score), 4);
                rTaskGradeEntity.AvgScore = resultData.Count == 0 ? 0 : Math.Round(resultData.Average(m => m.Score), 4);

                rTaskGradeEntity.ExcellentScore = Convert.ToInt32(Math.Ceiling(rPaperEntity.TotalScore * (decimal)0.85));
                rTaskGradeEntity.GoodScore = Convert.ToInt32(Math.Ceiling(rPaperEntity.TotalScore * (decimal)0.75));
                rTaskGradeEntity.QualifiedScore = Convert.ToInt32(Math.Ceiling(rPaperEntity.TotalScore * (decimal)0.6));

                rTaskGradeEntity.ExcellentNumber = resultData.Count(m => m.Score >= rTaskGradeEntity.ExcellentScore);
                rTaskGradeEntity.GoodNumber = resultData.Count(m => m.Score >= rTaskGradeEntity.GoodScore && m.Score < rTaskGradeEntity.ExcellentScore);
                rTaskGradeEntity.QualifiedNumber = resultData.Count(m => m.Score >= rTaskGradeEntity.QualifiedScore && m.Score < rTaskGradeEntity.GoodScore);
                rTaskGradeEntity.UnqualifiedNumber = resultData.Count(m => m.Score < rTaskGradeEntity.QualifiedScore);
                //班级数据
                var rClassDetail = resultData.GroupBy(m => new { m.ClassId, m.ClassName }).Select(a => new CourseTaskGradeReportClassDto
                {
                    ClassId = a.Key.ClassId,
                    ClassName = a.Key.ClassName,
                    StudentCount = a.Count(),
                    AbsentStudentCount = a.Count(x => x.IsAbsent)
                }).ToList();
                rTaskGradeEntity.ClassDetail = rClassDetail;
                return new ResponseContext<CourseTaskGradeReportDto> { Code = CommonConstants.SuccessCode, Msg = "成功", Data = rTaskGradeEntity };
            });
        }
        /// <summary>
        /// 获取学生答题对错详情
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<StudentGradeInfoDto>> GetTaskTopicGradeDetailList(long gradeId, long taskId)
        {
            var result = new ResponseContext<StudentGradeInfoDto>();
            StudentGradeInfoDto model = new StudentGradeInfoDto();
            var data = await DbContext.FreeSql.GetRepository<YssxCourseTaskTopic>().Select
                    .From<YssxTopicPublic, ExamCourseUserGradeDetail>((a, b, c) =>
                        a.LeftJoin(aa => aa.TopicId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                        .LeftJoin(aa => aa.TopicId == c.ParentQuestionId && c.GradeId == gradeId && c.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c) => a.TaskId==taskId && a.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b, c) => new TaskTopicGradeDetailInfo
                    {
                        Title = b.Title,
                        Status = c.Status
                    });
            model.TaskName = await DbContext.FreeSql.GetRepository<YssxCourseTask>().Where(s => s.Id == taskId).Select(s => s.Name).FirstAsync();
            model.TopicGradeDetail = data;

            result.Data = model;
            return result;
        }
        /// <summary>
        /// 章节习题分析
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<TaskSectionGradeInfoDto>>> GetTaskSectionGradeDetailList(long taskId)
        {
            var result = new ResponseContext<List<TaskSectionGradeInfoDto>>();

            var taskTopics = await DbContext.FreeSql.GetRepository<YssxCourseTaskTopic>().Select
                    .From<YssxTopicPublic>((a, b) =>
                        a.LeftJoin(aa => aa.TopicId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => a.TaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync((a, b) => new TaskSectionGradeInfoDto
                    {
                        QuestionId = b.Id,
                        QuestionName = b.Content,
                        QuestionType = b.QuestionType,
                        CenterKnowledgePointNames = b.CenterKnowledgePointNames,
                        Score = b.Score
                    });

            var gradeDetails = await DbContext.FreeSql.GetRepository<ExamCourseUserGradeDetail>().Select
                .From<ExamCourseUserGrade>((a, b) =>
                a.LeftJoin(aa => aa.GradeId == b.Id && b.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b) => b.TaskId == taskId && a.ParentQuestionId == a.QuestionId && a.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b) => new
                {
                    a.QuestionId,
                    a.Status
                });

            var taskStudentCount = await DbContext.FreeSql.GetRepository<YssxCourseTaskClass>().Select
                .From<YssxStudent>((a, b) =>
                a.LeftJoin(aa => aa.ClassId == b.ClassId && b.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b) => a.TaskId == taskId && a.IsDelete == CommonConstants.IsNotDelete).CountAsync();

            foreach (var item in taskTopics)
            {
                item.TotalCount = taskStudentCount;
                item.FinishCount = gradeDetails.Where(s => s.QuestionId == item.QuestionId).Count();
                item.ErrorCount = gradeDetails.Where(s => s.QuestionId == item.QuestionId && (s.Status == AnswerResultStatus.Error || s.Status == AnswerResultStatus.PartRight)).Count();
            }

            result.Data = taskTopics;
            return result;
        }
        //TODO 编辑任务基本信息和修改任务时间接口分开    老师做题接口  发布任务班级重复问题  作答记录重复问题

        #endregion
    }
}
