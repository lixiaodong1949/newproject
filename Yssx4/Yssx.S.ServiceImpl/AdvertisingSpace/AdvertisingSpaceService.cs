﻿using Microsoft.Extensions.DependencyModel.Resolution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Repository.Extensions;
using Yssx.S.Dto;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class AdvertisingSpaceService : IAdvertisingSpaceService
    {
        /// <summary>
        /// 获取 广告位列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<AdvertisingSpaceListResponse>> GetAdvertisingSpaceList(AdvertisingSpaceListDao model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxAdvertisingSpace>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Name), a => a.Name == model.Name)
                .WhereIf(model.Type >= 0, a => a.Type == model.Type)
                .WhereIf(model.PlatformType > 0, a => a.PlatformType == model.PlatformType);;
                //.OrderByDescending(a => a.Id);

                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("Id,Name,Type,PlatformType,Difference");
                var items = DbContext.FreeSql.Ado.Query<AdvertisingSpaceListResponse>(sql);

                return new PageResponse<AdvertisingSpaceListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }

        /// <summary>
        /// 添加 更新 广告位
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateAdvertisingSpace(AdvertisingSpaceDao md, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID

                if (md.Id>0)
                {
                    //修改
                    var oldinfo = DbContext.FreeSql.Select<YssxAdvertisingSpace>().Where(m => m.Id == md.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    oldinfo.PlatformType = md.PlatformType;
                    oldinfo.Name = md.Name;
                    oldinfo.Type = md.Type;
                    oldinfo.Difference = md.Difference;
                    oldinfo.UpdateBy = opreationId;
                    oldinfo.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<YssxAdvertisingSpace>().SetSource(oldinfo).UpdateColumns(m => new { m.PlatformType, m.Name, m.Type, m.Difference, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();//m.Parent, m.YCoordinate, 
                    if (i == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }
                //添加
                var newinfo = new YssxAdvertisingSpace
                {
                    Id = IdWorker.NextId(),
                    PlatformType = md.PlatformType,
                    Name = md.Name,
                    Type = md.Type,
                    Difference = md.Difference,
                    CreateBy = opreationId
                };
                DbContext.FreeSql.GetRepository<YssxAdvertisingSpace>().Insert(newinfo);

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 获取 广告位列表-详情
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<AdvertisingSpaceDetailsListResponse>> GetAdvertisingSpaceDetailList(AdvertisingSpaceDetailsListDao model, long oId)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select< YssxAdvertisingSpaceDetails>()
                .From<YssxAdvertisingSpace>((a,b)=>a.LeftJoin(aa=> b.Id== aa.TenantId && b.IsDelete == CommonConstants.IsNotDelete))
                .Where((a,b) => a.IsDelete == CommonConstants.IsNotDelete)//&& b.Difference==model.Difference
                .WhereIf(model.PlatformType>0, (a,b)=>b.PlatformType==model.PlatformType)
                .WhereIf(model.AdvertisingSpaceId > 0, (a, b) => a.TenantId == model.AdvertisingSpaceId)
                .OrderByDescending((a,b) => a.CreateTime);

                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.Id,a.Title,a.AdvertisingMap,a.Visitlink,a.Sort,a.StartTime,a.EndTime,a.State");
                var items = DbContext.FreeSql.Ado.Query<AdvertisingSpaceDetailsListResponse>(sql);

                //过期得时间更新已失效
                var ids = DbContext.FreeSql.Ado.Query<YssxAdvertisingSpaceDetails>($@"SELECT Id FROM yssx_advertisingspace_details WHERE EndTime<now() AND TenantId={model.AdvertisingSpaceId} AND State=0 AND IsDelete=0");
                if(ids.Count!=0)
                     DbContext.FreeSql.Update<YssxAdvertisingSpaceDetails>().SetSource(ids).Set(x => x.State, 1).Set(a => a.UpdateBy, oId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows();

                return new PageResponse<AdvertisingSpaceDetailsListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }

        /// <summary>
        /// 添加 更新 广告位-详情
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateAdvertisingSpaceDetails(AdvertisingSpaceDetailsDao md, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID

                if (md.Id > 0)
                {
                    //修改
                    var oldinfo = DbContext.FreeSql.Select<YssxAdvertisingSpaceDetails>().Where(m => m.Id == md.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    oldinfo.Title = md.Title;
                    oldinfo.TenantId = md.TenantId;
                    oldinfo.AdvertisingMap = md.AdvertisingMap;
                    oldinfo.Visitlink = md.Visitlink;
                    oldinfo.Sort = md.Sort;
                    oldinfo.StartTime = md.StartTime;
                    oldinfo.EndTime = md.EndTime;
                    oldinfo.State = md.State;
                    oldinfo.ProductId = 0;
                    oldinfo.UpdateBy = opreationId;
                    oldinfo.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<YssxAdvertisingSpaceDetails>().SetSource(oldinfo).UpdateColumns(m => new { m.Title, m.TenantId, m.AdvertisingMap, m.Visitlink, m.Sort, m.StartTime, m.EndTime, m.State,m.ProductId,m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    if (i == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }
                //添加
                var newinfo = new YssxAdvertisingSpaceDetails
                {
                    Id = IdWorker.NextId(),
                    Title = md.Title,
                    TenantId = md.TenantId,
                    AdvertisingMap = md.AdvertisingMap,
                    Visitlink = md.Visitlink,
                    Sort = md.Sort,
                    StartTime = md.StartTime,
                    EndTime = md.EndTime,
                    State = 0,
                    ProductId = 0,
                    CreateBy = opreationId
                };
                DbContext.FreeSql.GetRepository<YssxAdvertisingSpaceDetails>().Insert(newinfo);

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 删除 广告位-详情
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Delete(long Id,long oId)
        {
            if (Id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };
            var oldinfo = new YssxAdvertisingSpaceDetails
            {
                Id = Id,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = oId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                int i = DbContext.FreeSql.Update<YssxAdvertisingSpaceDetails>().SetSource(oldinfo).UpdateColumns(m => new { m.IsDelete, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                if (i == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "删除失败" };
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 状态 广告位-详情
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> State(long Id, int State, long oId)
        {
            if (Id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "状态更新失败，找不到原始信息!" };
            var oldinfo = new YssxAdvertisingSpaceDetails
            {
                Id = Id,
                State = State,
                UpdateBy = oId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                int i = DbContext.FreeSql.Update<YssxAdvertisingSpaceDetails>().SetSource(oldinfo).UpdateColumns(m => new { m.State, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                if (i == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "状态更新失败" };
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }




        /// <summary>
        /// 获取 广告位
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<AdvertisingSpaceChartResponse>> AdvertisingSpaceChart(int PlatformType)
        {
            return await Task.Run(() =>
            {
                var obj = new AdvertisingSpaceChartResponse();
                var select = DbContext.FreeSql.Select<YssxAdvertisingSpace>()
                .From<YssxAdvertisingSpaceDetails>((a, b) => a.LeftJoin(aa => aa.Id == b.TenantId))
                .Where((a,b)=>a.PlatformType==PlatformType && a.IsDelete== CommonConstants.IsNotDelete && b.StartTime<DateTime.Now && b.EndTime>DateTime.Now && b.State==0 && b.IsDelete== CommonConstants.IsNotDelete);
                var sql = select.ToSql("b.AdvertisingMap,b.Visitlink,b.Sort,b.StartTime,b.EndTime,a.Difference,b.ProductId");
                var list = DbContext.FreeSql.Ado.Query<AdvertisingSpaceResponse>(sql);

                //启动页
                var sPage = list.Where(s => s.Difference == 0).OrderBy(o => o.Sort).ToList();
                if (sPage.Count != 0)
                {
                    obj.Img = sPage[0].AdvertisingMap;
                }
                else
                {
                    obj.Img = string.Empty;
                }
                obj.StartupPage = new List<StartupPage>();
                foreach (var sobj in sPage)
                {
                    obj.StartupPage.Add(new StartupPage { Img = sobj.AdvertisingMap, Url = sobj.Visitlink });
                }
                //轮播图
                var rotationChart = list.Where(s => s.Difference == 1).OrderBy(o => o.Sort).ToList();
                obj.banner = new List<banner>();
                foreach (var robj in rotationChart)
                {
                    obj.banner.Add(new banner {ProductId= robj.ProductId, Img=robj.AdvertisingMap,Url=robj.Visitlink });
                }

                return new ResponseContext<AdvertisingSpaceChartResponse> { Code = CommonConstants.SuccessCode, Data = obj };
            });
        }
    }
}
