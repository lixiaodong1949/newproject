﻿using Microsoft.Extensions.DependencyModel.Resolution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Repository.Extensions;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.IServices;
using Yssx.S.Pocos;

namespace Yssx.S.ServiceImpl
{
    public class UploadCaseService : IUploadCaseService
    {

        #region 经典案例操作
        /// <summary>
        /// 添加 更新 案例
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateCase(UploadCaseDto md, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID

                var Userinfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == opreationId).First();//&& m.IsDelete == CommonConstants.IsNotDelete&&m.Status==1
                if (Userinfo == null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "当前用户不存在，找不到原始信息!" };

                var content = string.Empty;
                var uploads = string.Empty;
                if (Userinfo.UserType == 6) uploads = "云自营"; else uploads = Userinfo.NikeName;
                uploads = HttpUtility.UrlDecode(uploads, Encoding.UTF8);

                if (md.Id.HasValue)
                {
                    //修改
                    var oldUserinfo = DbContext.FreeSql.Select<YssxUploadCase>().Where(m => m.Id == md.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldUserinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    oldUserinfo.Classification = md.Classification;
                    oldUserinfo.Title = md.Title;
                    oldUserinfo.CaseSummary = HttpUtility.UrlEncode(md.CaseSummary, Encoding.UTF8);
                    oldUserinfo.ProposedScheme = HttpUtility.UrlEncode(md.ProposedScheme, Encoding.UTF8);
                    oldUserinfo.Implementation = HttpUtility.UrlEncode(md.Implementation, Encoding.UTF8);
                    oldUserinfo.ExecutionResult = HttpUtility.UrlEncode(md.ExecutionResult, Encoding.UTF8);
                    oldUserinfo.DecisionMakingBasis = HttpUtility.UrlEncode(md.DecisionMakingBasis, Encoding.UTF8);
                    oldUserinfo.CaseSummaryImg = md.CaseSummaryImg;
                    oldUserinfo.ProposedSchemeImg = md.ProposedSchemeImg;
                    oldUserinfo.ImplementationImg = md.ImplementationImg;
                    oldUserinfo.ExecutionResultImg = md.ExecutionResultImg;
                    oldUserinfo.DecisionMakingBasisImg = md.DecisionMakingBasisImg;
                    oldUserinfo.DataUpload = md.DataUpload;
                    if (md.StatusType != 0)
                    {
                        oldUserinfo.CaseStatus = 0; //保存状态待上架
                        oldUserinfo.Uploads = string.IsNullOrEmpty(uploads) ? Userinfo.Id.ToString() : uploads;
                    }
                    oldUserinfo.Cover = md.Cover;
                    oldUserinfo.Summary = md.Summary;
                    oldUserinfo.ValuePoint = md.ValuePoint;
                    oldUserinfo.CaseLabel = md.CaseLabel;
                    oldUserinfo.Copyright = md.Copyright;
                    oldUserinfo.UpdateBy = opreationId;
                    oldUserinfo.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(oldUserinfo).UpdateColumns(m => new { m.Classification, m.Title, m.CaseSummary, m.ProposedScheme, m.Implementation, m.ExecutionResult, m.DecisionMakingBasis, m.CaseSummaryImg, m.ProposedSchemeImg, m.ImplementationImg, m.ExecutionResultImg, m.DecisionMakingBasisImg, m.DataUpload, m.CaseStatus, m.Uploads, m.Cover, m.Summary, m.ValuePoint, m.CaseLabel,m.Copyright, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                    if (i == 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };

                    //操作日志
                    if (Userinfo != null) content = string.IsNullOrEmpty(Userinfo.RealName) ? string.IsNullOrEmpty(Userinfo.NikeName) ? Userinfo.Id.ToString() : Userinfo.NikeName : Userinfo.RealName;
                    DbContext.FreeSql.GetRepository<OperationLog>().Insert(new OperationLog { Id = IdWorker.NextId(), ModuleId = md.Id, Module = 5, Content = Userinfo.UserType == 6 ? "平台 编辑" : content + " 编辑", CreateBy = opreationId });

                    //设置缓存立即失效
                    FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{md.Id}", new UploadCaseResponse(), 1, true);//预览
                    FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCaseGoodArticle}", new GoodArticle() , 1, true);//好文推荐
                    FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{md.Id}", new NoUserUploadCaseResponse(), 1, true);//无用户登陆预览

                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true ,Msg=oldUserinfo.Id.ToString() };
                }
                ////添加
                //if (DbContext.FreeSql.Select<YssxUploadCase>().Any(m => m.UserName == md.UserName))
                //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "添加标题重复" };
                var cid = IdWorker.NextId();
                var yssxUploadCase = new YssxUploadCase
                {
                    Id = cid,
                    Classification = md.Classification,
                    Title = md.Title,
                    CaseSummary = HttpUtility.UrlEncode(md.CaseSummary, Encoding.UTF8),
                    ProposedScheme = HttpUtility.UrlEncode(md.ProposedScheme, Encoding.UTF8),
                    Implementation = HttpUtility.UrlEncode(md.Implementation, Encoding.UTF8),
                    ExecutionResult = HttpUtility.UrlEncode(md.ExecutionResult, Encoding.UTF8),
                    DecisionMakingBasis = HttpUtility.UrlEncode(md.DecisionMakingBasis, Encoding.UTF8),
                    CaseSummaryImg = md.CaseSummaryImg,
                    ProposedSchemeImg = md.ProposedSchemeImg,
                    ImplementationImg = md.ImplementationImg,
                    ExecutionResultImg = md.ExecutionResultImg,
                    DecisionMakingBasisImg = md.DecisionMakingBasisImg,
                    DataUpload = md.DataUpload,
                    CaseStatus = 0, //保存状态待上架
                    Uploads = string.IsNullOrEmpty(uploads) ? Userinfo.Id.ToString() : uploads,
                    Cover = md.Cover,
                    Summary = md.Summary,
                    ValuePoint = md.ValuePoint,
                    CaseLabel = md.CaseLabel,
                    Copyright=md.Copyright,
                    CreateBy = opreationId,
                    //CreateTime = DateTime.Now
                    UpdateTime = DateTime.Now
                };
                DbContext.FreeSql.GetRepository<YssxUploadCase>().Insert(yssxUploadCase);

                //操作日志
                if (Userinfo != null) content = string.IsNullOrEmpty(Userinfo.RealName) ? string.IsNullOrEmpty(Userinfo.NikeName) ? Userinfo.Id.ToString() : Userinfo.NikeName : Userinfo.RealName;
                DbContext.FreeSql.GetRepository<OperationLog>().Insert(new OperationLog { Id = IdWorker.NextId(), ModuleId = cid, Module = 5, Content = Userinfo.UserType == 6 ? "平台 添加" : content + " 添加", CreateBy = opreationId });

                //设置缓存立即失效
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCaseGoodArticle}", new GoodArticle(), 1, true);//好文推荐

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true , Msg = cid.ToString() };
            });
        }

        /// <summary>
        /// 删除 案例
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteUpdateCase(long Id, long oId)
        {
            long opreationId = oId;//操作人ID
            //TODO 租户过滤
            if (Id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };
            var activationCode = new YssxUploadCase
            {
                Id = Id,
                IsDelete = CommonConstants.IsDelete,
                UpdateBy = opreationId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                int i = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(activationCode).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                if (i == 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "删除失败" };
                //设置缓存立即失效
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{Id}", new UploadCaseResponse(), 1, true);//预览
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCaseGoodArticle}", new GoodArticle(), 1, true);//好文推荐
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{Id}", new NoUserUploadCaseResponse(), 1, true);//无用户登陆预览
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 获取 案例列表
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<PageResponse<UploadCaseResult>> FindUpdateCase(UploadCaseQueryDao model, long oId)
        {
            long opreationId = oId;//操作人ID

            return await Task.Run(() =>
            {
                long totalCount = 0;
                var sql = string.Empty;
                var items = new List<UploadCaseList>();//UploadCaseResponse
                var list = new List<UploadCaseResult>();
                var obj = new UploadCaseResult();

            //Type null用户 0专业组 1购买 2PC商城页 3APP商城页
            if (model.Type == null || model.Type == 0)
            {
                    var select = DbContext.FreeSql.Select<YssxUploadCase>().Where(a => a.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(a => a.UpdateTime);
                    //筛选 当前用户案例 标题 案例类型 资料状态 来源类型
                    if (model.Type == null)
                        select = select.Where(a => a.CreateBy == opreationId);
                    if (!string.IsNullOrEmpty(model.Title))
                        select = select.Where(a => a.Title.Contains(model.Title)||a.Uploads.Contains(model.Title));
                    if (model.Classification != 4 && model.Classification != null)
                        select = select.Where(a => a.Classification == model.Classification);
                    if (model.Status != 4 && model.Status != null)
                        select = select.Where(a => a.CaseStatus == model.Status);
                    if (model.SourceType != 2 && model.SourceType != null)
                    {
                        if (model.SourceType == 0)
                            select = select.Where(a => a.Uploads == "云自营" || a.Uploads == "%e4%ba%91%e8%87%aa%e8%90%a5");
                        else
                            select = select.Where(a => a.Uploads != "云自营" && a.Uploads != "%e4%ba%91%e8%87%aa%e8%90%a5");
                    }

                    totalCount = select.Count();
                    sql = select.Page(model.PageIndex, model.PageSize).ToSql("Id,CreateTime,UpdateTime,ValuePoint,CertificateNumber,Certificate,Fabulous,RejectionTime,ShelfTime,CaseGrade,Uploads,Remarks,CaseStatus,SubmissionTime,CaseLabel,`Read`,Classification,Title,Cover,Summary");//,CaseSummary,ProposedScheme
                    items = DbContext.FreeSql.Ado.Query<UploadCaseList>(sql);

                    //缓存列表
                    //var List = select.ToList($"{CommonConstants.Cache_GetUploadCaseList}{HttpUtility.UrlEncode(model.Title, Encoding.UTF8)}{model.PageIndex}{model.PageSize}{model.Type}", true, 5 * 60, true, false);
                    //items = List.Count == 0 ? DbContext.FreeSql.Ado.Query<UploadCaseList>(sql) : List.Select(x => new UploadCaseList { Id = x.Id, Title = x.Title, CaseSummary = x.CaseSummary, Read = x.Read, Fabulous = x.Fabulous, Uploads = x.Uploads, UpdateTime = x.UpdateTime, CreateTime = x.CreateTime }).ToList();

                    for (int i = 0; i < items.Count; i++)
                    {
                        items[i].Title = HttpUtility.UrlDecode(items[i].Title, Encoding.UTF8);
                        items[i].Uploads = HttpUtility.UrlDecode(items[i].Uploads, Encoding.UTF8);
                        #region
                        //items[i].CaseSummary = HttpUtility.UrlDecode(items[i].CaseSummary, Encoding.UTF8);
                        //items[i].ProposedScheme = HttpUtility.UrlDecode(items[i].ProposedScheme, Encoding.UTF8);
                        //items[i].Implementation = HttpUtility.UrlDecode(items[i].Implementation, Encoding.UTF8);
                        //items[i].ExecutionResult =  HttpUtility.UrlDecode(items[i].ExecutionResult, Encoding.UTF8);
                        //items[i].DecisionMakingBasis = HttpUtility.UrlDecode(items[i].DecisionMakingBasis, Encoding.UTF8);
                        #endregion
                    }

                    //if (!string.IsNullOrEmpty(model.Title))
                    //    items =  items.FindAll(c=>c.Title.Contains(model.Title)|| c.Uploads.Contains(model.Title));

                    obj = new UploadCaseResult();
                    obj.UploadCaseResponse = items;
                    list.Add(obj);
                }
                else if (model.Type == 1)
                {
                    var select = DbContext.FreeSql.Select<YssxUploadCase>()
                   .From<UploadcasePurchase>((a, b) => a.RightJoin(aa => b.TenantId == aa.Id && b.CreateBy == opreationId && b.IsDelete == CommonConstants.IsNotDelete && b.Status == 2))// && b.CreateBy == opreationId
                   .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete)
                   .OrderByDescending((a, b) => a.UpdateTime);

                    //var select = DbContext.FreeSql.Select<YssxUploadCase>()
                    //.From<UploadcasePurchase>((a, b) => a.RightJoin(aa => aa.CaseStatus == 2 && b.IsDelete == CommonConstants.IsNotDelete))
                    //.Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete)
                    //.OrderByDescending((a, b) => a.UpdateTime);

                    //筛选 标题 类型 来源类型
                    if (!string.IsNullOrEmpty(model.Title))
                        select = select.Where((a, b) => a.Title.Contains(model.Title) || a.Uploads.Contains(model.Title));

                    if (model.Classification != 4 && model.Classification != null)
                        select = select.Where((a, b) => a.Classification == model.Classification);

                    if (model.SourceType != 2 && model.SourceType != null)
                    {
                        if (model.SourceType == 0)
                            select = select.Where((a, b) => a.Uploads == "云自营" || a.Uploads == "%e4%ba%91%e8%87%aa%e8%90%a5");
                        else
                            select = select.Where((a, b) => a.Uploads != "云自营" && a.Uploads != "%e4%ba%91%e8%87%aa%e8%90%a5");
                    }
                    totalCount = select.Count();
                    sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.Id,a.CreateTime,a.UpdateTime,a.ValuePoint,a.CertificateNumber,a.Certificate,a.Fabulous,a.RejectionTime,a.ShelfTime,a.CaseGrade,a.Uploads,a.Remarks,a.CaseStatus,a.SubmissionTime,a.CaseLabel,a.`Read`,a.Classification,a.Title,a.Cover,a.Summary  ,b.PurchaseType PurchaseType,b.CreateTime PurchaseTime,b.Status");//a.CreateTime
                    items = DbContext.FreeSql.Ado.Query<UploadCaseList>(sql);

                    for (int i = 0; i < items.Count; i++)
                    {
                        items[i].Title = HttpUtility.UrlDecode(items[i].Title, Encoding.UTF8);
                        items[i].Uploads = HttpUtility.UrlDecode(items[i].Uploads, Encoding.UTF8);
                    }

                    //if (!string.IsNullOrEmpty(model.Title))
                    //    items = items.FindAll(c => c.Title.Contains(model.Title) || c.Uploads.Contains(model.Title));

                    obj = new UploadCaseResult();
                    obj.UploadCaseResponse = items;
                    list.Add(obj);
                }
                else if (model.Type == 2)
                {
                    var select = DbContext.FreeSql.Select<YssxUploadCase>()
                    .From<UploadcasePurchase>((a, b) => a.LeftJoin(aa => b.TenantId == aa.Id && b.IsDelete == CommonConstants.IsNotDelete && b.CreateBy==opreationId&&b.Status==2))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.CaseStatus == 2);

                    //筛选  标题 类型
                    if (!string.IsNullOrEmpty(model.Title))
                        select = select.Where((a, b) => a.Title.Contains(model.Title) || a.Uploads.Contains(model.Title));

                    if (model.Classification != 4 && model.Classification != null)
                        select = select.Where((a, b) => a.Classification == model.Classification);

                    if (model.Sort == 0 || model.Sort == null) select = select.OrderByDescending((a, b) => a.UpdateTime);
                    if (model.Sort == 1) select = select.OrderByDescending((a, b) => a.Read);//Fabulous
                    if (model.Sort == 11) select = select.OrderBy((a, b) => a.Read);
                    if (model.Sort == 2) select = select.OrderByDescending((a, b) => a.Read);
                    if (model.Sort == 22) select = select.OrderBy((a, b) => a.Read);
                    if (model.Sort == 3) select = select.OrderByDescending((a, b) => a.UpdateTime);
                    if (model.Sort == 33) select = select.OrderBy((a, b) => a.UpdateTime);
                    if (model.Sort == 4) select = select.OrderByDescending((a, b) => a.Read);
                    if (model.Sort == 44) select = select.OrderBy((a, b) => a.Read);

                    totalCount = select.Count();
                    //sql = select.Page(model.PageIndex, model.PageSize).ToSql("Id,Title,CaseSummary,ProposedScheme,UpdateTime,CreateTime,Fabulous,Uploads,CaseLabel,`Read`,Classification,CaseStatus,(case when CaseSummaryImg <> '{\"list\":[]}' then CaseSummaryImg else case when ProposedSchemeImg <> '{\"list\":[]}' then ProposedSchemeImg else case when ImplementationImg <> '{\"list\":[]}' then ImplementationImg else case when ExecutionResultImg <> '{\"list\":[]}' then ExecutionResultImg else case when DecisionMakingBasisImg <> '{\"list\":[]}' then DecisionMakingBasisImg  end end end end end) FirstPicture");
                    sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.Id,Title,CaseSummary,ProposedScheme,a.UpdateTime,a.CreateTime,Fabulous,Uploads,CaseLabel,`Read`,Classification,CaseStatus,Cover,Summary,ValuePoint  ,b.PurchaseType PurchaseType,b.CreateTime PurchaseTime,b.Status");
                    items = DbContext.FreeSql.Ado.Query<UploadCaseList>(sql);

                    for (int i = 0; i < items.Count; i++)
                    {
                        items[i].Title = HttpUtility.UrlDecode(items[i].Title, Encoding.UTF8);
                        items[i].CaseSummary = HttpUtility.UrlDecode(items[i].CaseSummary, Encoding.UTF8);
                        items[i].ProposedScheme = HttpUtility.UrlDecode(items[i].ProposedScheme, Encoding.UTF8);
                        items[i].Uploads = HttpUtility.UrlDecode(items[i].Uploads, Encoding.UTF8);
                    }

                    //if (!string.IsNullOrEmpty(model.Title))
                    //    items = items.FindAll(c => c.Title.Contains(model.Title) || c.Uploads.Contains(model.Title));

                    obj = new UploadCaseResult();
                    obj.UploadCaseResponse = items;
                    list.Add(obj);
                }
                else if (model.Type == 3)
                {
                    var select = DbContext.FreeSql.Select<YssxUploadCase>()
                    .From<UploadcasePurchase>((a, b) => a.LeftJoin(aa => b.TenantId == aa.Id  && b.IsDelete == CommonConstants.IsNotDelete && b.CreateBy == opreationId && b.Status == 2))
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.CaseStatus == 2);

                    //筛选  标题
                    if (!string.IsNullOrEmpty(model.Title))
                        select = select.Where((a, b) => a.Title.Contains(model.Title) || a.Uploads.Contains(model.Title));

                    if (model.Sort == 0 || model.Sort == null) select = select.OrderByDescending((a, b) => a.UpdateTime);
                    if (model.Sort == 1) select = select.OrderByDescending((a, b) => a.Read);
                    if (model.Sort == 11) select = select.OrderBy((a, b) => a.Read);
                    if (model.Sort == 2) select = select.OrderByDescending((a, b) => a.Read);
                    if (model.Sort == 22) select = select.OrderBy((a, b) => a.Read);
                    if (model.Sort == 3) select = select.OrderByDescending((a, b) => a.UpdateTime);
                    if (model.Sort == 33) select = select.OrderBy((a, b) => a.UpdateTime);
                    if (model.Sort == 4) select = select.OrderByDescending((a, b) => a.Read);
                    if (model.Sort == 44) select = select.OrderBy((a, b) => a.Read);

                    totalCount = select.Count();//,CaseSummary,ProposedScheme
                    sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.Id,Title,a.UpdateTime,a.CreateTime,Fabulous,Uploads,CaseLabel,`Read`,Classification,CaseStatus,  Cover,Summary,ValuePoint  ,b.PurchaseType PurchaseType,b.CreateTime PurchaseTime,b.Status");
                    items = DbContext.FreeSql.Ado.Query<UploadCaseList>(sql);

                    for (int i = 0; i < items.Count; i++)
                    {
                        items[i].Title = HttpUtility.UrlDecode(items[i].Title, Encoding.UTF8);
                        items[i].Uploads = HttpUtility.UrlDecode(items[i].Uploads, Encoding.UTF8);
                    }

                    //if (!string.IsNullOrEmpty(model.Title))
                    //    items = items.FindAll(c => c.Title.Contains(model.Title) || c.Uploads.Contains(model.Title));

                    obj = new UploadCaseResult();
                    obj.UploadCaseResponse = items;
                    list.Add(obj);
                }
                return new PageResponse<UploadCaseResult> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = list };
            });
        }

        /// <summary>
        /// 获取 商城案例列表--无用户登陆
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<PageResponse<NoUserUploadCaseListResponse>> NoUserUploadCaseList(NoUserUploadCaseQueryDao model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxUploadCase>().Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.CaseStatus == 2);
                if (!string.IsNullOrEmpty(model.Title)) 
                    select = select.Where(a => a.Title.Contains(model.Title) || a.Uploads == model.Title);

                if (model.Sort == 0 || model.Sort == null) select = select.OrderByDescending(a => a.UpdateTime);
                if (model.Sort == 1) select = select.OrderByDescending(a => a.Read);//Fabulous
                if (model.Sort == 11) select = select.OrderBy(a => a.Read);
                if (model.Sort == 2) select = select.OrderByDescending(a => a.Read);
                if (model.Sort == 22) select = select.OrderBy(a => a.Read);
                if (model.Sort == 3) select = select.OrderByDescending(a => a.UpdateTime);
                if (model.Sort == 33) select = select.OrderBy(a => a.UpdateTime);
                if (model.Sort == 4) select = select.OrderByDescending(a => a.Read);
                if (model.Sort == 44) select = select.OrderBy(a => a.Read);

                var totalCount = select.Count();
                var sql = string.Empty;
                if (model.Type == 3)
                    sql = select.Page(model.PageIndex, model.PageSize).ToSql("Id,Title,UpdateTime,CreateTime,Fabulous,Uploads,`Read`,  Cover,Summary,CaseLabel");
                else
                    sql = select.Page(model.PageIndex, model.PageSize).ToSql("Id,Title,CaseSummary,UpdateTime,CreateTime,Fabulous,Uploads,`Read`,Cover,Summary,CaseLabel");
                //var items = DbContext.FreeSql.Ado.Query<NoUserUploadCaseListResponse>(sql); 

                //缓存列表
                //var List = select.ToList($"{CommonConstants.Cache_GetNoUserUploadCaseList}{HttpUtility.UrlEncode(model.Title, Encoding.UTF8)}{model.PageIndex}{model.PageSize}", true, 5*60, true, false);
                //var items = List.Count == 0 ? DbContext.FreeSql.Ado.Query<NoUserUploadCaseListResponse>(sql) : List.Select(x => new NoUserUploadCaseListResponse {Id = x.Id, Title = x.Title, CaseSummary = x.CaseSummary, Read = x.Read, Fabulous = x.Fabulous, Uploads = x.Uploads, UpdateTime = x.UpdateTime, CreateTime = x.CreateTime,Cover=x.Cover,Summary=x.Summary,CaseLabel=x.CaseLabel }).ToList();
                var items = DbContext.FreeSql.Ado.Query<NoUserUploadCaseListResponse>(sql);

                for (int i = 0; i < items.Count; i++)
                {
                    items[i].Title = HttpUtility.UrlDecode(items[i].Title, Encoding.UTF8);
                    items[i].CaseSummary = HttpUtility.UrlDecode(items[i].CaseSummary, Encoding.UTF8);
                    items[i].Uploads = HttpUtility.UrlDecode(items[i].Uploads, Encoding.UTF8);
                    //if (model.Type != 3)
                    //    items[i].FirstPicture = DbContext.FreeSql.Ado.Query<NoUserploaUdCaseListResponse>("SELECT (case when CaseSummaryImg <> '{\"list\":[]}' then CaseSummaryImg else case when ProposedSchemeImg <> '{\"list\":[]}' then ProposedSchemeImg else case when ImplementationImg <> '{\"list\":[]}' then ImplementationImg else case when ExecutionResultImg <> '{\"list\":[]}' then ExecutionResultImg else case when DecisionMakingBasisImg <> '{\"list\":[]}' then DecisionMakingBasisImg  end end end end end) FirstPicture FROM yssx_uploadcase WHERE Id="+items[i].Id+"")[0].FirstPicture ;
                }

                //if (!string.IsNullOrEmpty(model.Title))
                //    items = items.FindAll(c => c.Title.Contains(model.Title) || c.Uploads.Contains(model.Title));

                return new PageResponse<NoUserUploadCaseListResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };

            });
        }

        /// <summary>
        /// 获取 商城案例预览--无用户登陆
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<NoUserUploadCaseResponse>> NoUserUploadCase(long Id)
        {
            return await Task.Run(() =>
            {
                //取缓存
                //var items = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{Id}", () => new NoUserUploadCaseResponse(), 24 * 3600, true, false);

                //if (items.Id == 0) 
                //{
                    var select = DbContext.FreeSql.Select<YssxUploadCase>().Where(m => m.Id == Id && m.IsDelete == CommonConstants.IsNotDelete && m.CaseStatus == 2);

                    var sql = select.ToSql("Id,Title,CaseSummary,ProposedScheme,UpdateTime,CreateTime,Fabulous,Uploads,CaseLabel,`Read`,Classification,CaseStatus,Cover,Summary,CreateBy,Certificate,CertificateNumber");
                    var items = DbContext.FreeSql.Ado.Query<NoUserUploadCaseResponse>(sql).FirstOrDefault();
                    if (items == null) 
                        return new ResponseContext<NoUserUploadCaseResponse> { Code = CommonConstants.ErrorCode, Msg = "获取失败，找不到原始信息!" };

                    items.Title = HttpUtility.UrlDecode(items.Title, Encoding.UTF8);
                    items.CaseSummary = HttpUtility.UrlDecode(items.CaseSummary, Encoding.UTF8);
                    items.ProposedScheme = HttpUtility.UrlDecode(items.ProposedScheme, Encoding.UTF8);
                    items.Uploads = HttpUtility.UrlDecode(items.Uploads, Encoding.UTF8);
                    //设置缓存
                    FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{Id}", items, 24 * 3600, true);
                //}
                //历史当前作者头像
                items.UserAvatar = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == items.CreateBy).First().Photo;
                //历史当前作者案例数量
                items.UserCaseNum = DbContext.FreeSql.Ado.Query<UploadCaseResponse>($"SELECT  COUNT(*) UserCaseNum FROM yssx_uploadcase WHERE CreateBy='{items.CreateBy}' AND IsDelete = 0 AND CaseStatus = 2")[0].UserCaseNum;

                return new ResponseContext<NoUserUploadCaseResponse> { Code = CommonConstants.SuccessCode, Data = items };
            });
        }

        /// <summary>
        /// 上架 案例 
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CaseRack(CaseRackDao model, long oId)
        {
            long opreationId = oId;//操作人ID
            //上架
            if (model.Id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "上架失败，找不到原始信息!" };
            var yssxUploadCase = new YssxUploadCase
            {
                Id = model.Id,
                CaseStatus = 1,
                Uppershelf=1,
                SubmissionTime = DateTime.Now,
                //CaseLabel = DbContext.FreeSql.Select<YssxUploadCase>().Where(m => m.Id == model.Id).First().CaseLabel, //添加标签
                UpdateBy = opreationId,
                UpdateTime = DateTime.Now
            };

            return await Task.Run(() =>
            {
                int i = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(yssxUploadCase).UpdateColumns(m => new { m.CaseStatus, m.Uppershelf, m.SubmissionTime, m.UpdateTime, m.UpdateBy }).ExecuteAffrows();//m.CaseLabel,
                if (i == 0) 
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "上架失败" };

                var content = string.Empty;
                var Userinfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == opreationId).First();
                if (Userinfo != null) content = string.IsNullOrEmpty(Userinfo.RealName) ? string.IsNullOrEmpty(Userinfo.NikeName) ? Userinfo.Id.ToString() : Userinfo.NikeName : Userinfo.RealName;
                DbContext.FreeSql.GetRepository<OperationLog>().Insert(new OperationLog { Id = IdWorker.NextId(), ModuleId = model.Id, Module = 5, Content = Userinfo.UserType == 6 ? "平台 上架" : content + " 上架", CreateBy = opreationId });
                //设置缓存立即失效
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{model.Id}", new UploadCaseResponse(), 1, true);//预览
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCaseGoodArticle}", new GoodArticle(), 1, true);//好文推荐
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{model.Id}", new NoUserUploadCaseResponse(), 1, true);//无用户登陆预览

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 下架 案例 
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> CaseFrame(long Id, long oId)
        {
            long opreationId = oId;//操作人ID
            //下架
            if (Id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "下架失败，找不到原始信息!" };
            var yssxUploadCase = new YssxUploadCase
            {
                Id = Id,
                CaseStatus = 0,
                Uppershelf = 0,
                SubmissionTime = DateTime.Now,
                UpdateBy = opreationId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                int i = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(yssxUploadCase).UpdateColumns(m => new { m.CaseStatus, m.Uppershelf, m.SubmissionTime, m.UpdateTime, m.UpdateBy }).ExecuteAffrows();
                if (i == 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "下架失败" };

                var content = string.Empty;
                var Userinfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == opreationId).First();
                if (Userinfo != null) content = string.IsNullOrEmpty(Userinfo.RealName) ? string.IsNullOrEmpty(Userinfo.NikeName) ? Userinfo.Id.ToString() : Userinfo.NikeName : Userinfo.RealName;
                DbContext.FreeSql.GetRepository<OperationLog>().Insert(new OperationLog { Id = IdWorker.NextId(), ModuleId = Id, Module = 5, Content = Userinfo.UserType == 6 ? "平台 下架" : content + " 下架", CreateBy = opreationId });
                //设置缓存立即失效
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{Id}", new UploadCaseResponse(), 1, true);//预览
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCaseGoodArticle}", new GoodArticle(), 1, true);//好文推荐
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{Id}", new NoUserUploadCaseResponse(), 1, true);//无用户登陆预览

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 撤销 案例
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Revoke(long Id, long oId)
        {
            long opreationId = oId;//操作人ID

            //撤销
            if (Id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "撤销失败，找不到原始信息!" };
            var yssxUploadCase = new YssxUploadCase
            {
                Id = Id,
                CaseStatus = 0,//StayOnTheShelf
                Uppershelf = 0,
                SubmissionTime = DateTime.Now,
                //Remarks="语句不通，杀伤性十大",
                UpdateBy = opreationId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                var i = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(yssxUploadCase).UpdateColumns(m => new { m.CaseStatus, m.Uppershelf, m.SubmissionTime, m.UpdateTime, m.UpdateBy }).ExecuteAffrows();
                if (i == 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "撤销失败" };

                var content = string.Empty;
                var Userinfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == opreationId).First();
                if (Userinfo != null) content = string.IsNullOrEmpty(Userinfo.RealName) ? string.IsNullOrEmpty(Userinfo.NikeName) ? Userinfo.Id.ToString() : Userinfo.NikeName : Userinfo.RealName;
                DbContext.FreeSql.GetRepository<OperationLog>().Insert(new OperationLog { Id = IdWorker.NextId(), ModuleId = Id, Module = 5, Content = Userinfo.UserType == 6 ? "平台 撤销" : content + " 撤销", CreateBy = opreationId });
                //设置缓存立即失效
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{Id}", new UploadCaseResponse(), 1, true);//预览
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCaseGoodArticle}", new GoodArticle(), 1, true);//好文推荐
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{Id}", new NoUserUploadCaseResponse(), 1, true);//无用户登陆预览

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 预览 案例
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<UploadCaseResponse>> Preview(long Id, long oId)
        {

            long opreationId = oId;//操作人ID

            return await Task.Run(() =>
            {
                //取缓存
                //var itemss = DbContext.FreeSql.Select<UploadCaseResponse>().Where(m => m.Id == Id && m.IsDelete == CommonConstants.IsNotDelete).First($"{CommonConstants.Cache_GetUploadCasePreview}{Id}", true, 24 * 3600, true, false);// 720 * 60

                //取缓存
                //var items = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetUploadCasePreview}{Id}", () => new UploadCaseResponse(), 24 * 3600, true, false);
                //if (items.Id==0)
                //{
                    //获取预览
                    var select = DbContext.FreeSql.Select<YssxUploadCase>().Where(m => m.Id == Id && m.IsDelete == CommonConstants.IsNotDelete);
                    var sql = select.ToSql();
                    var items = DbContext.FreeSql.Ado.Query<UploadCaseResponse>(sql).FirstOrDefault();
                    if (items == null) return new ResponseContext<UploadCaseResponse> { Code = CommonConstants.ErrorCode, Msg = "预览失败，找不到原始信息!" };
                    //是否点赞
                    items.FabulousWhether = DbContext.FreeSql.Ado.Query<UploadCaseFabulous>("SELECT COUNT(*) TenantId FROM yssx_uploadcase_fabulous WHERE TenantId=" + Id + " AND CreateBy=" + opreationId + " AND IsDelete=0 ")[0].TenantId != 0 ? true : false;
                    //历史当前作者头像
                    items.UserAvatar = DbContext.FreeSql.Select<YssxUser>().Where(m => m.Id == items.CreateBy).First().Photo;
                    //历史当前作者案例数量
                    items.UserCaseNum = DbContext.FreeSql.Ado.Query<UploadCaseResponse>($"SELECT  COUNT(*) UserCaseNum FROM yssx_uploadcase WHERE CreateBy='{items.CreateBy}' AND IsDelete = 0 AND CaseStatus = 2")[0].UserCaseNum;
                    //是否购买
                    var uploadcasePurchase = DbContext.FreeSql.Select<UploadcasePurchase>().Where(m => m.TenantId == Id && m.CreateBy == opreationId && m.IsDelete == CommonConstants.IsNotDelete&&m.Status==2).First();
                    if (uploadcasePurchase != null)
                    {
                        items.PurchaseTime = uploadcasePurchase.CreateTime;
                        items.PurchaseType = uploadcasePurchase.PurchaseType;
                        items.Status = uploadcasePurchase.Status;
                    }
                //}

                //案例阅读次数
                items.Read = items.Read + 1;
                DbContext.FreeSql.Update<YssxUploadCase>().SetSource(new YssxUploadCase { Id = Id, Read = items.Read }).UpdateColumns(m => new { m.Read }).ExecuteAffrows();

                items.Title = HttpUtility.UrlDecode(items.Title, Encoding.UTF8);
                items.CaseSummary = HttpUtility.UrlDecode(items.CaseSummary, Encoding.UTF8);
                items.ProposedScheme = HttpUtility.UrlDecode(items.ProposedScheme, Encoding.UTF8);
                items.Implementation = HttpUtility.UrlDecode(items.Implementation, Encoding.UTF8);
                items.ExecutionResult = HttpUtility.UrlDecode(items.ExecutionResult, Encoding.UTF8);
                items.DecisionMakingBasis = HttpUtility.UrlDecode(items.DecisionMakingBasis, Encoding.UTF8);
                items.Uploads = HttpUtility.UrlDecode(items.Uploads, Encoding.UTF8);

                //设置缓存
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{Id}", items, 24 * 3600, true);

                return new ResponseContext<UploadCaseResponse> { Code = CommonConstants.SuccessCode, Data = items };
            });
        }

        /// <summary>
        /// 获取 标签列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<UploadCaseTag>> GetTagLib(UploadCaseTagLibDao model)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxTag>().Where(m => m.IsDelete == CommonConstants.IsNotDelete).OrderByDescending(a => a.Name);

                if (!string.IsNullOrEmpty(model.Name))
                    select = select.Where(a => a.Name.Contains(model.Name));

                //获取标签库
                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("id,name");
                //缓存标签库列表  //FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCaseList}{HttpUtility.UrlEncode(model.Title, Encoding.UTF8)}{model.PageIndex}{model.PageSize}", new YssxUploadCase(), 1, true);
                var tags = select.ToList($"{CommonConstants.Cache_GetUploadCaseTagLibraryList}{model.Name}{model.PageIndex}{model.PageSize}", true, 30 * 60, true, false);
                var items = tags.Count == 0 ? DbContext.FreeSql.Ado.Query<UploadCaseTag>(sql) : tags.Select(x => new UploadCaseTag { Id = x.Id, Name = x.Name }).ToList();
   
                return new PageResponse<UploadCaseTag> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }

        /// <summary>
        ///  点赞 商城案例
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Fabulous(CaseFabulousDao model, long oId)
        {
            long opreationId = oId;//操作人ID

            if (!model.Id.HasValue)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "点赞失败，找不到原始信息!" };

            var oldUserinfo = DbContext.FreeSql.Select<UploadCaseFabulous>().Where(m => m.CreateBy == opreationId && m.TenantId == model.Id.Value && m.IsDelete == CommonConstants.IsNotDelete).First();
            if (oldUserinfo != null) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已点赞，不能在点!" };

            var fabulous = new UploadCaseFabulous
            {
                Id = IdWorker.NextId(),
                TenantId = model.Id.Value,
                CreateBy = opreationId,
                //UpdateBy = opreationId,
                //UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                var i = DbContext.FreeSql.GetRepository<UploadCaseFabulous>().Insert(fabulous);
                if (i == null) 
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "点赞失败" };
                else
                {
                    var oldFabulous = DbContext.FreeSql.Select<YssxUploadCase>().Where(m => m.Id == model.Id.Value).First().Fabulous;
                    DbContext.FreeSql.Update<YssxUploadCase>().SetSource(new YssxUploadCase { Id = model.Id.Value, Fabulous = oldFabulous + 1 }).UpdateColumns(m => new { m.Fabulous }).ExecuteAffrows();
                }
                //设置缓存立即失效
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{model.Id}", new UploadCaseResponse(), 1, true);//预览
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCaseGoodArticle}", new GoodArticle(), 1, true);//好文推荐
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{model.Id}", new NoUserUploadCaseResponse(), 1, true);//无用户登陆预览
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 上传 证书 
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Certificate(CertificateDao model, long oId)
        {
            long opreationId = oId;//操作人ID

            if (model.Id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "上传证书失败，找不到原始信息!" };

            var yssxUploadCase = new YssxUploadCase
            {
                Id = model.Id,
                CertificateNumber = model.CertificateNumber,
                Certificate = model.Certificate,
                UpdateBy = opreationId,
                UpdateTime = DateTime.Now
            };
            return await Task.Run(() =>
            {
                int i = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(yssxUploadCase).UpdateColumns(m => new { m.CertificateNumber, m.Certificate, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                if (i == 0) 
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "上传证书失败" };
                //设置缓存立即失效
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCasePreview}{model.Id}", new UploadCaseResponse(), 1, true);//预览
                FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetNoUserUploadCase}{model.Id}", new NoUserUploadCaseResponse(), 1, true);//无用户登陆预览

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 获取 商城好文推荐
        /// </summary>
        /// <returns></returns>
        public async  Task<ResponseContext<GoodArticle>> GoodCase()
        {
            return await Task.Run(() =>
            {
                //取缓存
                var goodArticle = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetUploadCaseGoodArticle}", () => new GoodArticle(), 24 * 3600, true, false);
                //获取好文推荐
                if (goodArticle.Piece==0&&goodArticle.HotCase==null&&goodArticle.LatestUpdates==null&&goodArticle.PointPraiseCase==null)
                {
                    goodArticle.Piece = DbContext.FreeSql.Ado.Query<GoodArticle>("SELECT  COUNT(*) Piece FROM yssx_uploadcase WHERE IsDelete = 0 AND CaseStatus = 2")[0].Piece;

                    goodArticle.HotCase = DbContext.FreeSql.Ado.Query<HotCase>("SELECT Id,Title,`Read`,CreateTime  FROM yssx_uploadcase WHERE IsDelete = 0 AND CaseStatus = 2 ORDER BY `Read` DESC LIMIT 0,10");
                    for (int i = 0; i < goodArticle.HotCase.Count; i++)
                        goodArticle.HotCase[i].Title = HttpUtility.UrlDecode(goodArticle.HotCase[i].Title, Encoding.UTF8);

                    goodArticle.LatestUpdates = DbContext.FreeSql.Ado.Query<LatestUpdates>("SELECT Id,Title,UpdateTime CreateTime FROM yssx_uploadcase WHERE IsDelete = 0 AND CaseStatus = 2 ORDER BY UpdateTime DESC LIMIT 0,10");
                    for (int i = 0; i < goodArticle.LatestUpdates.Count; i++)
                        goodArticle.LatestUpdates[i].Title = HttpUtility.UrlDecode(goodArticle.LatestUpdates[i].Title, Encoding.UTF8);

                    goodArticle.PointPraiseCase = DbContext.FreeSql.Ado.Query<PointPraiseCase>("SELECT Id,Title,Fabulous,CreateTime FROM yssx_uploadcase WHERE IsDelete = 0 AND CaseStatus = 2 ORDER BY Fabulous DESC LIMIT 0,10");
                    for (int i = 0; i < goodArticle.PointPraiseCase.Count; i++)
                        goodArticle.PointPraiseCase[i].Title = HttpUtility.UrlDecode(goodArticle.PointPraiseCase[i].Title, Encoding.UTF8);

                    //设置缓存
                    FreeSqlCacheExtension.SetCache($"{CommonConstants.Cache_GetUploadCaseGoodArticle}", goodArticle, 24 * 3600, true);
                }

                return new ResponseContext<GoodArticle> { Code = CommonConstants.SuccessCode, Data = goodArticle };
            });
        }

        /// <summary>
        /// 获取 交易记录
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<TransactionRecords>> GetTransactionRecords(TransactionRecordsDao model)
        {
            return await Task.Run(() =>
            {
                #region tese
                //var yssxUploadCase = new UploadcasePurchaseInput
                //{
                //    Id = IdWorker.NextId(),
                //    CreateBy = 1,
                //    UpdateTime = DateTime.Now
                //};
                //DbContext.FreeSql.GetRepository<UploadcasePurchaseInput>().Insert(yssxUploadCase);

                //var uploadCase = DbContext.FreeSql.Ado.Query<YssxUploadCase>("SELECT * FROM yssx_uploadcase ORDER BY CreateTime DESC;");
                //foreach (var dd in uploadCase)
                //{
                //    var yssxUploadCase = new YssxUploadCase
                //    {
                //        Id = dd.Id,
                //        Title = HttpUtility.UrlDecode(dd.Title, Encoding.UTF8),
                //        Uploads= HttpUtility.UrlDecode(dd.Uploads, Encoding.UTF8)
                //    };
                //    int i = DbContext.FreeSql.Update<YssxUploadCase>().SetSource(yssxUploadCase).UpdateColumns(m => new { m.Title,m.Uploads }).ExecuteAffrows();
                //}

                //var sda = new List<TransactionRecords>();
                //sda.Add(new TransactionRecords());
                //sda[0].Student = "2121";
                //sda[0].TransactionDetails=transactionDetails;
                #region 扩展
                //var skillUser = DbContext.JNFreeSql.Ado.Query<YssxJnccUser>("SELECT * FROM yssx_user WHERE MobilePhone REGEXP \"^[1][35678][0-9]{9}$\" AND `Status`='Enable' AND UserType='Student' AND IsDelete=0"); //JSFreeSql

                ////抽查
                //foreach (var s in skillUser)
                //{
                //    var Userinfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.MobilePhone == s.MobilePhone).First();
                //    if (Userinfo == null)
                //    {
                //        var yssxUser = new YssxUser
                //        {
                //            Id = s.Id,
                //            Email = s.Email,
                //            MobilePhone = s.MobilePhone,
                //            Sex = (int)s.Sex == 0 ? "男" : (int)s.Sex == 1 ? "女" : "",
                //            NikeName = s.NikeName,
                //            Password = s.Password,
                //            Photo = s.Photo,
                //            QQ = s.QQ,
                //            RealName = s.RealName,
                //            Status = 1,
                //            UserName = s.UserName,
                //            UserType = 1,
                //            WeChat = s.WeChat,
                //            IdNumber = s.IdNumber,
                //            //IsDelete = 0, //111
                //            ActivationTime = s.ActivateTime,
                //            //Unionid = "121",
                //            //TenantId = "121",
                //            //Unionid = "121",TenantId
                //            CreateTime = DateTime.Now,
                //            ClientId = Guid.NewGuid(),
                //            RegistrationType = 2
                //        };
                //        var e = DbContext.FreeSql.GetRepository<YssxUser>().Insert(yssxUser);
                //    }
                //}

                ////竞赛
                //foreach (var s in skillUser )
                //{
                //    var Userinfo = DbContext.FreeSql.Select<YssxUser>().Where(m => m.MobilePhone == s.MobilePhone).First();
                //    if (Userinfo==null)
                //    {
                //        var yssxUser = new YssxUser
                //        {
                //            Id = s.Id,
                //            Email = s.Email,
                //            MobilePhone = s.MobilePhone,
                //            Sex = (int)s.Sex == 0 ? "男" : (int)s.Sex == 1 ? "女" : "",
                //            NikeName = s.NikeName,
                //            Password = s.Password,
                //            Photo = s.Photo,
                //            QQ = s.QQ,
                //            RealName = s.RealName,
                //            Status = 1,
                //            UserName = s.UserName,
                //            UserType = 1,
                //            WeChat = s.WeChat,
                //            IdNumber = s.IdNumber,
                //            //IsDelete = 0, //111
                //            //ActivationTime = DateTime.Now,
                //            //Unionid = "121",
                //            //TenantId = "121",
                //            //Unionid = "121",TenantId
                //            CreateTime = DateTime.Now,
                //            ClientId = Guid.NewGuid(),
                //            RegistrationType = 2
                //        };
                //        var e = DbContext.FreeSql.GetRepository<YssxUser>().Insert(yssxUser);
                //    }
                //}
                #endregion
                #endregion

                var select = DbContext.FreeSql.Select<UploadcasePurchase>()
                .From<YssxUser, YssxSchool>((a, b, c) => 
                a.LeftJoin(aa => b.Id == aa.CreateBy && b.IsDelete == CommonConstants.IsNotDelete)
                .LeftJoin(aa => c.Id == b.TenantId && c.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b, c) => a.TenantId == model.Id && a.IsDelete == CommonConstants.IsNotDelete && a.Status==2);

                var items = new TransactionRecords();
                var sql1 = string.Empty;
                sql1 += "SELECT SUM(NumberOfTraders) NumberOfTraders,SUM(Teacher) Teacher,SUM(Student) Student,SUM(Other) Other FROM ( ";
                sql1 += select.ToSql("COUNT(*) NumberOfTraders,SUM(b.UserType=2) Teacher,SUM(b.UserType=1) Student,SUM(b.UserType<>2 AND b.UserType<>1) Other");
                sql1 += $"UNION ALL SELECT COUNT(*) NumberOfTraders,SUM(Role='教师') Teacher,SUM(Role='学生') Student,SUM(Role='其他') Other FROM yssx_uploadcase_purchase_input WHERE (`TenantId` = {model.Id} AND `IsDelete` = 0) ) a";
                var obj = DbContext.FreeSql.Ado.Query<TransactionRecords>(sql1).FirstOrDefault();

                var totalCount = obj.NumberOfTraders; //select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("CASE WHEN b.MobilePhone<>'' THEN REPLACE(b.MobilePhone,SUBSTRING(b.MobilePhone,4,4),'****') ELSE REPLACE(b.Id,SUBSTRING(b.Id,4,8),'****') END BuyingCustomers,CASE b.UserType WHEN 1 THEN '学生' WHEN 2 THEN '教师' ELSE '其他' END Role,c.Name School,a.CreateTime PurchaseTime");
                sql =  sql.Replace("limit", $"UNION ALL SELECT BuyingCustomers,Role,School,CreateTime PurchaseTime FROM yssx_uploadcase_purchase_input WHERE (`TenantId` = {model.Id} AND `IsDelete` = 0) limit ");

                var transactionRecords = new TransactionRecords();
                transactionRecords.TransactionDetails = DbContext.FreeSql.Ado.Query<TransactionDetails>(sql);
                items = transactionRecords;
                items.NumberOfTraders = obj.NumberOfTraders;
                items.Teacher = obj.Teacher;
                items.Student = obj.Student;
                items.Other = obj.Other;

                var list = new List<TransactionRecords>();
                list.Add(items);

                return new PageResponse<TransactionRecords> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = list };
            });
        }

        /// <summary>
        /// 添加 更新 批注(追加批注)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddUpdateComments(CommentsDao md, long Id)
        {
            return await Task.Run(() =>
            {
                long opreationId = Id;//操作人ID

                if (md.Id.HasValue)
                {
                    //修改
                    var oldinfo = DbContext.FreeSql.Select<UploadcaseComments>().Where(m => m.Id == md.Id && m.IsDelete == CommonConstants.IsNotDelete).First();
                    if (oldinfo == null)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息!" };
                    //oldinfo.Parent = md.Parent;
                    //oldinfo.YCoordinate = md.YCoordinate;
                    oldinfo.Comment = md.Comment;
                    oldinfo.UpdateBy = opreationId;
                    oldinfo.UpdateTime = DateTime.Now;
                    var i = DbContext.FreeSql.Update<UploadcaseComments>().SetSource(oldinfo).UpdateColumns(m => new { m.Comment, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();//m.Parent, m.YCoordinate, 
                    if (i == 0) 
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "修改失败" };
                    return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
                }
                var uploadcaseComments = new UploadcaseComments
                {
                    Id = IdWorker.NextId(),
                    Parent = md.Parent,
                    YCoordinate = md.YCoordinate,
                    Comment=md.Comment,
                    CreateBy = opreationId
                };
                DbContext.FreeSql.GetRepository<UploadcaseComments>().Insert(uploadcaseComments);

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true,Msg= uploadcaseComments.Id.ToString() };
            });
        }

        /// <summary>
        /// 获取 批注
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CommentsResponse>>> GetComments(long Id)
        {
            return await Task.Run(() =>
            {
                var items = new List<CommentsResponse>();

                var select = DbContext.FreeSql.Select<UploadcaseComments>()
                .From<YssxUser>((a, b) => a.LeftJoin(aa => b.Id == aa.CreateBy && b.IsDelete == CommonConstants.IsNotDelete))
                .Where((a, b) => a.Parent == Id ).OrderBy((a,b)=>a.YCoordinate).OrderBy((a, b) => a.Comment);//&& a.IsDelete == CommonConstants.IsNotDelete

                var sql = select.ToSql("a.Id,YCoordinate");//,Comment,a.CreateTime,b.Photo,b.RealName
                items = DbContext.FreeSql.Ado.Query<CommentsResponse>(sql);
                var listItems = new List<CommentsResponse>();
                int listInt = 0;

                //for(int c=0;c<items.Count;c++)
                //{
                //    var rlist = DbContext.FreeSql.Ado.Query<UploadcaseComments>($@"SELECT * FROM yssx_uploadcase_comments WHERE Id={items[c].Id} OR Parent={items[c].Id}");
                //    items.Remove(items[c]);
                //}

                for (int i=0;i<items.Count;i++)
                {
                    var list = items.Where(c => c.YCoordinate == items[i].YCoordinate).ToList();
                    for (int j = 0; j < list.Count; j++)
                    {
                        //检查如果没有跳过
                        var cc=  DbContext.FreeSql.Ado.Query<CommentsDetails1>($@"SELECT * FROM (SELECT a.Id,Comment,a.CreateTime,b.Photo,b.RealName,CASE b.UserType WHEN 6 THEN 1 ELSE 0 END Role,a.`IsDelete`  
                                                                                 FROM `yssx_uploadcase_comments` a 
                                                                                 LEFT JOIN `yssx_user` b ON b.`Id` = a.`CreateBy` AND b.`IsDelete` = 0 
                                                                                 WHERE a.Parent= {list[j].Id} OR a.Id= {list[j].Id} AND a.`IsDelete` = 0 ORDER BY a.CreateTime) c WHERE `IsDelete` = 0");
                        if (cc.Count != 0)
                        {
                            if (j == 0)
                            {
                                var jj = listItems.Count();
                                listItems.Add(new CommentsResponse());
                                listItems[jj].Id = list[j].Id;
                                listItems[jj].YCoordinate = list[j].YCoordinate;

                                var obj = new CommentsDetails();
                                obj.Id = list[j].Id;
                                obj.CommentsDetails1 = DbContext.FreeSql.Ado.Query<CommentsDetails1>($@"SELECT * FROM (SELECT a.Id,Comment,a.CreateTime,b.Photo,b.RealName,CASE b.UserType WHEN 6 THEN 1 ELSE 0 END Role,a.`IsDelete`  
                                                                                        FROM `yssx_uploadcase_comments` a 
                                                                                        LEFT JOIN `yssx_user` b ON b.`Id` = a.`CreateBy` AND b.`IsDelete` = 0 
                                                                                        WHERE a.Parent= {list[j].Id} OR a.Id= {list[j].Id} AND a.`IsDelete` = 0 ORDER BY a.CreateTime) c WHERE `IsDelete` = 0");//,Parent
                                var listDetails = new List<CommentsDetails>();
                                listDetails.Add(obj);
                                listItems[jj].CommentsDetails = listDetails;
                                listInt = jj;
                            }
                            else
                            {
                                var commentsDetails = new CommentsDetails();
                                commentsDetails.Id = list[j].Id;
                                commentsDetails.CommentsDetails1 = DbContext.FreeSql.Ado.Query<CommentsDetails1>($@"SELECT * FROM (SELECT a.Id,Comment,a.CreateTime,b.Photo,b.RealName,CASE b.UserType WHEN 6 THEN 1 ELSE 0 END Role,a.`IsDelete`  
                                                                                        FROM `yssx_uploadcase_comments` a 
                                                                                        LEFT JOIN `yssx_user` b ON b.`Id` = a.`CreateBy` AND b.`IsDelete` = 0 
                                                                                        WHERE a.Parent= {list[j].Id} OR a.Id= {list[j].Id} AND a.`IsDelete` = 0 ORDER BY a.CreateTime) c WHERE `IsDelete` = 0");//,Parent


                                listItems[listInt].CommentsDetails.Add(commentsDetails);
                            }
                        }
                        items.Remove(list[j]);
                        list.Remove(list[j]);
                        j--;
                    }
                    i--;

                    #region
                    //items[i].CommentsDetails = new List<CommentsDetails>();

                    //var list = items.Where(c => c.YCoordinate == items[i].YCoordinate).ToList();
                    //for (int j = 0; j < list.Count; j++)
                    //{


                    //    var obj = new CommentsDetails();
                    //    obj.Id = list[j].Id;
                    //    obj.CommentsDetails1 = DbContext.FreeSql.Ado.Query<CommentsDetails1>($@"SELECT a.Id,Comment,a.CreateTime,b.Photo,b.RealName,CASE b.UserType WHEN 6 THEN 1 ELSE 0 END Role 
                    //                                                             FROM `yssx_uploadcase_comments` a 
                    //                                                             LEFT JOIN `yssx_user` b ON b.`Id` = a.`CreateBy` AND b.`IsDelete` = 0 
                    //                                                             WHERE a.Parent= {list[j].Id} OR a.Parent= {Id} AND a.Id= {list[j].Id} AND a.`IsDelete` = 0 ORDER BY a.CreateTime");//,Parent
                    //    items[i].CommentsDetails.Add(obj);
                    //    items.Remove(list[j]);
                    //}

                    //items[i].CommentsDetails =new List<CommentsDetails>();
                    //items[i].CommentsDetails.Add(new CommentsDetails());

                    //items[i].CommentsDetails[0].Id = items[i].Id;
                    //items[i].CommentsDetails[0].CommentsDetails1 = DbContext.FreeSql.Ado.Query<CommentsDetails1>($@"SELECT a.Id,Comment,a.CreateTime,b.Photo,b.RealName,CASE b.UserType WHEN 6 THEN 1 ELSE 0 END Role 
                    //                                                             FROM `yssx_uploadcase_comments` a 
                    //                                                             LEFT JOIN `yssx_user` b ON b.`Id` = a.`CreateBy` AND b.`IsDelete` = 0 
                    //                                                             WHERE a.Parent= {items[i].Id} OR a.Parent= {Id} AND a.Id= {items[i].Id} AND a.`IsDelete` = 0 ORDER BY a.CreateTime");//,Parent

                    //List<pople> poples = liset.Select((p) => new pople { PID = p.Id, PName = p.Id.ToString() }).ToList();
                    //var ts = classTeachers.Where(c => c.ClassId == a.Id).Select(c => c.RealName).Distinct().ToArray();

                    //var list = items.Where(c=>c.YCoordinate==items[i].YCoordinate).ToList();
                    //for (int j = 0; j < list.Count; j++)
                    //{
                    //    items.Remove(list[j]);
                    //}
                    #endregion
                }

                var total = DbContext.FreeSql.Ado.Query<CommentsResponse>($"SELECT COUNT(*) Id FROM yssx_uploadcase_comments WHERE Parent={Id} AND IsDelete=0 OR Parent in (SELECT Id FROM yssx_uploadcase_comments WHERE Parent={Id} AND IsDelete=0)").FirstOrDefault().Id;

                return new ResponseContext<List<CommentsResponse>> { Code = CommonConstants.SuccessCode, Data = listItems, Msg= total.ToString() };
            });
        }

        /// <summary>
        /// 删除 批注
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteComments(DeleteCommentsDao md, long oId)
        {
            long opreationId = oId;//操作人ID

            if (md.Id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };

            return await Task.Run(() =>
            {
                if (md.type == 1)//把ID關連下的都刪了
                {
                    var items = DbContext.FreeSql.Ado.Query<UploadcaseComments>($@"SELECT * FROM yssx_uploadcase_comments WHERE Id={md.Id} OR Parent={md.Id}");

                    int i = DbContext.FreeSql.GetRepository<UploadcaseComments>().UpdateDiy.SetSource(items).Set(a => a.IsDelete, CommonConstants.IsDelete).Set(a => a.UpdateBy, opreationId).Set(a => a.UpdateTime, DateTime.Now).ExecuteAffrows();
                    if (i == 0)
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "删除失败" };
                }
                if (md.type == 2)
                {
                    var items = DbContext.FreeSql.Ado.Query<UploadcaseComments>($@"SELECT * FROM yssx_uploadcase_comments a WHERE Parent={md.Id}  
                    OR Id in (SELECT Id FROM yssx_uploadcase_comments WHERE Parent in (SELECT Id FROM yssx_uploadcase_comments a WHERE Parent={md.Id}  ))");

                    var i = DbContext.FreeSql.GetRepository<UploadcaseComments>().UpdateDiy.SetSource(items).Set(a => a.IsDelete, CommonConstants.IsDelete).Set(a => a.UpdateBy, opreationId).Set(a => a.UpdateTime, DateTime.Now).ExecuteAffrows();
                    if (i == 0) 
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "清空失败" };
                }

                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
        #endregion

        #region 扩展
        /// <summary>
        /// 获取 购买案例
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<PageResponse<UploadCaseResponse>> FindPurchaseCase(PurchUploadCaseDao model, long oId)
        {
            long opreationId = oId;//操作人ID

            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.Select<YssxUploadCase>()
                .From<YssxConfig>((a, b) => a.RightJoin(aa => b.ConfigName == "PurchaseCase" && b.ConfigValue == aa.Id.ToString() && b.CreateBy == opreationId))//&&b.IsDelete== (int)CasePurchaseType.Give
                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete)
                .OrderByDescending((a, b) => a.Id);

                //筛选 云自营 其他
                if (!string.IsNullOrEmpty(model.Title))
                    select = select.Where((a, b) => a.Title.Contains(model.Title));
                if (model.SourceType != CaseSourceType.all && model.SourceType != null)
                {
                    if (model.SourceType == CaseSourceType.CloudSelfCamp)
                        select = select.Where((a, b) => a.Uploads == "云自营");
                    else
                        select = select.Where((a, b) => a.Uploads != "云自营");
                }

                var totalCount = select.Count();
                var sql = select.Page(model.PageIndex, model.PageSize).ToSql("a.* ,b.CreateTime as PurchaseTime,b.IsDelete PurchaseType");
                var items = DbContext.FreeSql.Ado.Query<UploadCaseResponse>(sql);

                return new PageResponse<UploadCaseResponse> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };
            });
        }

        /// <summary>
        /// 批量 上传案例
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> BatchUploadCase(BatchUploadCaseDao model, long oId)
        {
            //tags.Result.ForEach( x =>{x.Id = 32; x.Name = x.Name; });
            //var sda = tags.Result.Select( x => new UploadCaseTag{ Id = x.Id, Name = x.Name } ).ToList();
            //取缓存中 要更新的数据
            //List<UploadCaseResponse> sda = new List<UploadCaseResponse>();
            //List<UploadCaseResponse> cacheModel = sda.SingleOrDefault(x => x.Id == entity.Id);
            //if (cacheModel != null)
            //{
            //    items.SingleOrDefault(x => x.Id == entity.Id).SectionName = entity.SectionName;
            //    items.SingleOrDefault(x => x.Id == entity.Id).SectionTitle = entity.SectionTitle;
            //    items.SingleOrDefault(x => x.Id == entity.Id).Sort = entity.Sort;
            //    items.SingleOrDefault(x => x.Id == entity.Id).UpdateBy = entity.UpdateBy;
            //    items.SingleOrDefault(x => x.Id == entity.Id).UpdateTime = entity.UpdateTime;
            //}
            long opreationId = oId;//操作人ID

            //TODO 租户过滤
            if (model.Id.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息!" };

            var items = new List<YssxUploadCase>();
            for (var i = 0; i < model.Id.Count; i++) items.Add(new YssxUploadCase { Id = model.Id[i] });

            var e = 0;
            return await Task.Run(() =>
            {
                if (model.Type == 1)
                    e = DbContext.FreeSql.GetRepository<YssxUploadCase>().UpdateDiy.SetSource(items).Set(a => a.CaseStatus, 1).Set(a => a.SubmissionTime, DateTime.Now).Set(a => a.UpdateBy, opreationId).Set(a => a.UpdateTime, DateTime.Now).ExecuteAffrows();
                if (model.Type == 2)
                    e = DbContext.FreeSql.GetRepository<YssxUploadCase>().UpdateDiy.SetSource(items).Set(a => a.IsDelete, CommonConstants.IsDelete).Set(a => a.UpdateBy, opreationId).Set(a => a.UpdateTime, DateTime.Now).ExecuteAffrows();
                if (model.Type == 3)
                    e = DbContext.FreeSql.GetRepository<YssxUploadCase>().UpdateDiy.SetSource(items).Set(a => a.CaseStatus, 0).Set(a => a.UpdateBy, opreationId).Set(a => a.UpdateTime, DateTime.Now).ExecuteAffrows();
                if (e == 0) return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "操作失败" };
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 更新 订单交易记录和状态
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> OrderTransactions(long Id, long oId)
        {
            long opreationId = oId;//操作人ID
            if (Id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "订单记录更新失败，找不到原始信息！" };
            var uploadcasePurchase = new UploadcasePurchase
            {
                Id = IdWorker.NextId(),
                TenantId = Id,
                PurchaseType = 1,
                Status = 2,
                IsDelete = CommonConstants.IsNotDelete,
                CreateBy = opreationId,
            };
            return await Task.Run(() =>
            {
                var info = DbContext.FreeSql.Select<UploadcasePurchase>().Where(m => m.TenantId == Id && m.IsDelete == CommonConstants.IsNotDelete && m.CreateBy == opreationId && m.Status == 2).First();
                if (info != null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "你已购买该案例！" };

                var entity = DbContext.FreeSql.GetRepository<UploadcasePurchase>().Insert(uploadcasePurchase);
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = false, Msg = "订单记录更新失败！" };
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
        #endregion

        #region 生成试卷
        /// <summary>
        /// 购买后自动生成试卷 - 经典案例
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AutoGenerateTestPaper(AutoGenerateTestPaperDto model)
        {
            DateTime dtNow = DateTime.Now;
            var rEndTime = dtNow.AddYears(1);
            //验证
            if (model.Category != 1)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "分类入参错误");
            if (model.TargetId == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "标的物Id参数错误");
            if (model.UserId == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "目标用户ID参数错误");

            //查找经典案例
            var rCaseInfo = DbContext.FreeSql.GetRepository<YssxUploadCase>().Where(x => x.Id == model.TargetId && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (rCaseInfo == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "未找到该经验案例");

            var rAnyExamPaper = DbContext.FreeSql.GetRepository<UploadcasePurchase>().Where(x => x.TenantId == model.TargetId && x.CreateBy == model.UserId && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (rAnyExamPaper != null)
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "操作成功", Data = true };
            //生成试卷(购买记录)
            var rExamPaper = new UploadcasePurchase
            {
                Id = IdWorker.NextId(),
                TenantId = model.TargetId,
                PurchaseType = 1,
                Status = 2,
                CreateBy = model.UserId,
                CreateTime = dtNow
            };

            return await Task.Run(() =>
            {
                var state = DbContext.FreeSql.Insert(rExamPaper).ExecuteAffrows() > 0;
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "操作成功", Data = state };
            });
        }
        #endregion

    }

}
