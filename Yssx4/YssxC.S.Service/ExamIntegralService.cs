﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    /// <summary>
    /// 用户考试获得积分
    /// </summary>
    public class ExamIntegralService : IExamIntegralService
    {
        /// <summary>
        /// 获取总积分
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<decimal>> GetSum(long userId)
        {
            var sumIntegrals = await DbContext.FreeSql.GetGuidRepository<SxExamIntegral>(x => x.IsDelete == CommonConstants.IsNotDelete)
              .Where(e => e.UserId == userId)
              .SumAsync(e => e.Integral);

            return new ResponseContext<decimal>(sumIntegrals);
        }
    }
}
