﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using YssxC.S.Dto.Subject;
using YssxC.S.IServices.Subject;
using YssxC.S.Pocos;
using YssxC.S.Pocos.Subject;

namespace YssxC.S.Service.Subject
{
    public class SubjectService : ISubjectService
    {
        public SubjectService()
        {

        }
        public async Task<ResponseContext<bool>> AddSubject(SubjectDto model)
        {
            long subjectCode = 0;
            if (long.TryParse($"{model.Code1}{model.Code2}{model.Code3}{model.Code4}", out subjectCode))
            {
                SxSubject subject = DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == model.CaseId && x.SubjectType == model.SubjectType && x.SubjectCode == subjectCode && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (null == subject)
                {
                    subject = new SxSubject();

                    subject.SubjectCode = subjectCode;
                    subject.Id = IdWorker.NextId();
                    subject.SubjectName = model.SubjectName;
                    if (model.ParentId > 0)
                    {
                        subject.SubjectName = GetSubjectName(model.ParentId, model.SubjectName);
                        model.Direction = DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.Id == model.ParentId).First()?.Direction;
                    }
                    subject.Code1 = model.Code1;
                    subject.Code2 = model.Code2;
                    subject.Code3 = model.Code3;
                    subject.Code4 = model.Code4;
                    subject.SubjectType = model.SubjectType;
                    subject.Direction = model.Direction;
                    subject.CaseId = model.CaseId;
                    subject.ParentId = model.ParentId;
                    //subject.NormType = model.NormType;
                    // subject.AssistCode = PinYinHelper.GetFirstPinyin(model.SubjectName);
                    subject.CreateTime = DateTime.Now;
                    subject.ProfitLossSubjectType = model.ProfitLossSubjectType;
                    var result = await DbContext.FreeSql.GetRepository<SxSubject>().InsertAsync(subject);
                    return new ResponseContext<bool> { Data = result != null };
                }
                else
                {
                    return new ResponseContext<bool> { Data = true };
                }

            }
            else
            {
                return new ResponseContext<bool> { Data = false, Code = CommonConstants.ErrorCode, Msg = "科目编码有问题!" };
            }
        }

        public async Task<ResponseContext<string>> GetNextNum(long id)
        {
            string num = "01";
            int n = 0;
            List<SxSubject> yssxCoreSubjects = await DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.ParentId == id).OrderBy(x => x.SubjectCode).ToListAsync();
            if (yssxCoreSubjects == null || yssxCoreSubjects.Count == 0)
            {
                num = "01";
            }
            else
            {
                SxSubject yssx = yssxCoreSubjects.LastOrDefault();
                if (!string.IsNullOrEmpty(yssx.Code2) && string.IsNullOrEmpty(yssx.Code3))
                {
                    if (int.TryParse(yssx.Code2, out n) && n > 0)
                    {
                        n += 1;
                        if (n <= 9)
                        {
                            num = $"0{n}";
                        }
                        else
                        {
                            num = $"{n}";
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(yssx.Code3) && string.IsNullOrEmpty(yssx.Code4))
                {
                    if (int.TryParse(yssx.Code3, out n) && n > 0)
                    {
                        n += 1;
                        if (n <= 9)
                        {
                            num = $"0{n}";
                        }
                        else
                        {
                            num = $"{n}";
                        }
                    }
                }
                else if (!string.IsNullOrEmpty(yssx.Code4))
                {
                    if (int.TryParse(yssx.Code4, out n) && n > 0)
                    {
                        n += 1;
                        if (n <= 9)
                        {
                            num = $"0{n}";
                        }
                        else
                        {
                            num = $"{n}";
                        }
                    }
                }

            }

            return new ResponseContext<string> { Data = num };
        }

        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectLastNodeList(long id, int type)
        {
            List<SxSubject> subjects = await DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == id && x.ParentId == 0 && x.IsDelete == CommonConstants.IsNotDelete).WhereIf(type > 0, a => a.SubjectType == type).ToListAsync();
            //List<SxSubject> p_subjects = new List<SxSubject>();

            //if (p_subjects == null) return null;

            //List<SxSubject> retLis = new List<SxSubject>();
            //foreach (var item in p_subjects)
            //{
            //    retLis.Add(item);
            //    FindChildNode(item.Id, retLis, subjects);
            //}

            //if (retLis == null) return null;

            List<SubjectDto> coreSubjects = DataToModel(subjects);

            //List<SubjectDto> coreSubjects2 = new List<SubjectDto>();
            //coreSubjects.ForEach(x =>
            //{
            //    if (!retLis.Any(o => o.ParentId == x.Id))
            //    {
            //        coreSubjects2.Add(x);
            //    }
            //});

            return new ResponseContext<List<SubjectDto>> { Data = coreSubjects };
        }

        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectsList(long id)
        {
            List<SxSubject> list = await DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == id).ToListAsync();
            return new ResponseContext<List<SubjectDto>> { Data = DataToModel(list) };
        }

        public async Task<ResponseContext<bool>> RemoveSubject(long id)
        {
            bool retVal = await DbContext.FreeSql.GetRepository<SxSubject>().UpdateDiy.Set(x => x.IsDelete, 1).Where(x => x.Id == id).ExecuteAffrowsAsync() > 0;
            return new ResponseContext<bool> { Data = retVal, Code = retVal ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Msg = retVal ? "OK" : "删除失败！" };
        }

        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectByType(long id, int type)
        {
            var repo_s = DbContext.FreeSql.GetRepository<SxSubject>(x => x.IsDelete == CommonConstants.IsNotDelete);

            List<SxSubject> subjects = new List<SxSubject>();
            if (type > 0)
            {
                subjects = await repo_s.Where(x => x.CaseId == id && x.SubjectType == type).ToListAsync();
                List<SxSubject> assistacSubjects = await GetAssistacCountList(subjects, id, type);
                subjects.AddRange(assistacSubjects);
            }
            else
            {
                subjects = await repo_s.Where(x => x.CaseId == id && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                //List<SxSubject> assistacSubjects = await GetAssistacCountList(subjects, id, type);
                //subjects.AddRange(assistacSubjects);
            }

            List<SxSubject> p_subjects = new List<SxSubject>();

            p_subjects = subjects.Where(x => x.ParentId == 0).OrderBy(x => x.SubjectCode).ToList();

            if (p_subjects == null) return null;


            List<SxSubject> retLis = new List<SxSubject>();
            foreach (var item in p_subjects)
            {
                retLis.Add(item);
                FindChildNode(item.Id, retLis, subjects);
            }

            if (retLis == null) return null;

            List<SubjectDto> coreSubjects = DataToModel(retLis);

            coreSubjects.ForEach(x =>
            {
                if (x.AssistacId == 0)
                {
                    if (!retLis.Any(o => o.ParentId == x.Id))
                        x.IsLastNode = 1;
                }
                else
                {
                    if (!retLis.Any(o => o.AssistacParentId == x.AssistacId))
                        x.IsLastNode = 1;
                }
            });

            return new ResponseContext<List<SubjectDto>> { Data = coreSubjects };
        }

        private async Task<List<SxSubject>> GetAssistacCountList(List<SxSubject> sxSubjects, long caseId, int type)
        {
            List<SxAssistAccounting> list = await DbContext.FreeSql.GetRepository<SxAssistAccounting>().Where(x => x.CaseId == caseId && x.IsDelete == CommonConstants.IsNotDelete && x.SubjectId > 0).ToListAsync();

            List<SxSubject> subjects = list.Select(x => new SxSubject
            {
                Id = x.SubjectId != null ? (long)x.SubjectId : 0,
                ParentId = x.SubjectId != null ? (long)x.SubjectId : 0,
                AssistacParentId = x.ParentId,
                SubjectName = x.Name,
                AssistacId = x.Id,
                SubjectType = type,
                BorrowAmount = x.BorrowAmount,
                CreditorAmount = x.CreditorAmount
            }).ToList();

            subjects.ForEach(x =>
            {
                SxSubject sx = sxSubjects.FirstOrDefault(o => o.Id == x.Id);
                if (null != sx)
                {
                    x.SubjectName = string.Format("{0}_{1}", sx.SubjectName, x.SubjectName);
                }
            });
            return subjects;
        }


        public async Task<ResponseContext<bool>> AddValues(SubjectValueDto model)
        {
            ResponseContext<bool> result = new ResponseContext<bool>();

            List<SxSubject> yssxBeginningData = model.BeginningDatas.Where(x => x.AssistacId == 0).Select(x => new SxSubject
            {
                Id = x.Id,
                BorrowAmount = x.BorrowAmount,
                UpdateTime = DateTime.Now,
                AssistacId = x.AssistacId,
                CreditorAmount = x.CreditorAmount
            }).ToList();

            //foreach (var item in yssxBeginningData)
            //{
            //    DbContext.FreeSql.GetRepository<SxSubject>().UpdateDiy.SetSource(item).UpdateColumns(x => new { x.BorrowAmount, x.CreditorAmount, x.UpdateTime }).ExecuteAffrows();
            //}

            List<SxAssistAccounting> sxAssists = model.BeginningDatas.Where(x => x.AssistacId > 0).Select(x => new SxAssistAccounting
            {
                Id = x.AssistacId,
                BorrowAmount = x.BorrowAmount,
                CreditorAmount = x.CreditorAmount,
            }).ToList();

            await DbContext.FreeSql.GetRepository<SxSubject>().UpdateDiy.SetSource(yssxBeginningData).UpdateColumns(x => new { x.BorrowAmount, x.CreditorAmount, x.UpdateTime }).ExecuteAffrowsAsync();
            await DbContext.FreeSql.GetRepository<SxAssistAccounting>().UpdateDiy.SetSource(sxAssists).UpdateColumns(x => new { x.BorrowAmount, x.CreditorAmount }).ExecuteAffrowsAsync();
            result.Code = CommonConstants.SuccessCode;
            result.Data = true;
            return result;
        }

        public async Task<ResponseContext<TrialBalancingDto>> TrialBalancing(long caseId)
        {
            var enterpriseSubjects = await DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == caseId && x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.SubjectCode).ToListAsync();
            List<SxSubject> assistacSubjects = await GetAssistacCountList(enterpriseSubjects, caseId, 0);
            enterpriseSubjects.AddRange(assistacSubjects);
            var list = enterpriseSubjects.Where(m => m.ParentId == 0).ToList();
            List<SxSubject> retLis = new List<SxSubject>();
            foreach (var item in list)
            {
                retLis.Add(item);
                FindChildNode(item.Id, retLis, enterpriseSubjects);
            }
            List<SxSubject> temps = retLis.ToList();
            decimal c = 0;
            decimal b = 0;
            temps.ForEach(x =>
            {
                if (x.ParentId == 0)
                {
                    c += x.CreditorAmount;
                    b += x.BorrowAmount;
                }
            });
            string msg = b == c ? "数据平衡" : "数据不平衡";
            return new ResponseContext<TrialBalancingDto> { Data = new TrialBalancingDto { BorrowAmountSummer = b, CreditorAmountSummer = c }, Msg = msg };
        }

        #region 私有方法
        private string GetSubjectName(long parentId, string subjectName)
        {
            List<SxSubject> retList = new List<SxSubject>();
            FindParentNode(parentId, retList);
            StringBuilder sb = new StringBuilder();
            retList = retList.OrderBy(x => x.SubjectCode).ToList();
            foreach (var i in retList)
            {
                if (string.IsNullOrEmpty(i.SubjectName)) continue;
                if (!i.SubjectName.Contains("-"))
                {
                    sb.Append(i.SubjectName).Append("-");
                }
                else
                {
                    string[] s = i.SubjectName.Split('-');
                    sb.Append(s[s.Length - 1]).Append("-");
                }
            }

            sb.Append(subjectName);

            return sb.ToString();
        }

        /// <summary>
        /// 查找父节点
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="retList"></param>
        private void FindParentNode(long parentId, List<SxSubject> retList)
        {
            if (parentId == 0) return;
            SxSubject yssxCoreSubjects = DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.Id == parentId).First();
            if (null == yssxCoreSubjects) return;
            retList.Add(yssxCoreSubjects);
            FindParentNode(yssxCoreSubjects.ParentId, retList);
        }

        private void FindParentNode(long id, List<SxSubject> subjects, List<SxSubject> retList)
        {
            SxSubject yssxCoreSubjects = subjects.Where(x => x.Id == id).FirstOrDefault();
            if (null == yssxCoreSubjects) return;
            if (yssxCoreSubjects.ParentId == 0)
            {
                retList.Add(yssxCoreSubjects);
            }
            else
            {
                FindParentNode(yssxCoreSubjects.ParentId, subjects, retList);
            }
        }

        /// <summary>
        /// 查找子节点
        /// </summary>
        private void FindChildNode(long id, List<SxSubject> retList, List<SxSubject> enterpriseSubjects, long assistacId = 0)
        {
            if (id == 0) return;

            //List<YssxCoreSubject> yssxCoreSubjects = DbContext.FreeSql.Select<YssxCoreSubject>().Where(x => x.ParentId == id).ToList();
            List<SxSubject> yssxCoreSubjects = new List<SxSubject>();
            if (assistacId == 0)
                yssxCoreSubjects = enterpriseSubjects.Where(x => x.ParentId == id).OrderBy(x => x.SubjectCode).ToList();
            else
                yssxCoreSubjects = enterpriseSubjects.Where(x => x.ParentId == assistacId).OrderBy(x => x.SubjectCode).ToList();
            if (null == yssxCoreSubjects) return;
            foreach (var item in yssxCoreSubjects)
            {
                retList.Add(item);
                FindChildNode(item.Id, retList, enterpriseSubjects, item.AssistacId);
            }
        }

        private List<SubjectDto> DataToModel(List<SxSubject> sources)
        {
            if (null == sources) return null;
            return sources.Select(x => new SubjectDto
            {
                Id = x.Id,
                AssistCode = x.AssistCode,
                BorrowAmount = x.BorrowAmount,
                Code1 = x.Code1,
                Code2 = x.Code2,
                Code3 = x.Code3,
                Code4 = x.Code4,
                CreditorAmount = x.CreditorAmount,
                Direction = x.Direction,
                CaseId = x.CaseId,
                ParentId = x.ParentId,
                SubjectCode = x.SubjectCode,
                SubjectName = x.SubjectName,
                SubjectType = x.SubjectType,
                IsHaveCheck = x.IsHaveCheck,
                AssistacId = x.AssistacId,
                ProfitLossSubjectType = x.ProfitLossSubjectType,
            }).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="assistType"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<SxAssistSubjectDto>>> GetAssistacSubjectList(long id, int assistType)
        {
            List<SxAssistAccounting> list = new List<SxAssistAccounting>();
            if (assistType == -1)
            {
                list = await DbContext.FreeSql.GetRepository<SxAssistAccounting>().Where(x => x.CaseId == id && x.IsDelete == CommonConstants.IsNotDelete && x.SubjectId > 0).ToListAsync();
            }
            else
            {
                string sql = $"SELECT a.Name,a.Type FROM sx_assistac_counting a WHERE a.CaseId={id} and a.IsDelete=0 and a.SubjectId>0 and a.Type={assistType} group by a.name";
                list = await DbContext.FreeSql.Ado.QueryAsync<SxAssistAccounting>(sql);
            }
            List<SxAssistSubjectDto> subjectDtos = list.Select(x => new SxAssistSubjectDto
            {
                Name = x.Name,
                Type = (int)x.Type,
                //Id = (int)x.SubjectId,
                //BorrowAmount = x.BorrowAmount,
                //CreditorAmount = x.CreditorAmount,
                //CaseId = x.CaseId,
                //ParentId = x.ParentId,
                //SubjectCode = x.SubjectCode,
                //SubjectName = x.Name,
                //AssistacId = x.Id,
            }).ToList();
            return new ResponseContext<List<SxAssistSubjectDto>> { Data = subjectDtos };
        }

        public async Task<ResponseContext<List<SubjectDto>>> GetSubjectByAssistacId(long caseId, string name)
        {
            if (string.IsNullOrWhiteSpace(name)) return new ResponseContext<List<SubjectDto>> { };
            List<SxSubject> subjects = await DbContext.FreeSql.GetRepository<SxSubject>().Where(x => x.CaseId == caseId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            List<SxAssistAccounting> sxAssists = await DbContext.FreeSql.GetRepository<SxAssistAccounting>().Where(a => a.Name == name && a.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
            List<SxSubject> parentSubjects = new List<SxSubject>();
            List<SxSubject> retLis = new List<SxSubject>();
            foreach (var item in sxAssists)
            {
                FindParentNode((long)item.SubjectId, subjects, parentSubjects);
            }

            foreach (var item in parentSubjects)
            {
                retLis.Add(item);
                FindChildNode(item.Id, retLis, subjects);
            }


            List<SubjectDto> coreSubjects = DataToModel(retLis);

            coreSubjects.ForEach(x =>
            {
                if (x.AssistacId == 0)
                {
                    if (!retLis.Any(o => o.ParentId == x.Id))
                        x.IsLastNode = 1;
                }
            });
            return new ResponseContext<List<SubjectDto>> { Data = coreSubjects };
        }

        public async Task<ResponseContext<List<SxAssistSubjectDto>>> GetAssisSubjectById(long subjectId)
        {
            List<SxAssistAccounting> list = await DbContext.FreeSql.GetRepository<SxAssistAccounting>().Where(x => x.SubjectId == subjectId && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            List<SxAssistSubjectDto> subjectDtos = list.Select(x => new SxAssistSubjectDto
            {
                Name = x.Name,
                Type = (int)x.Type,
            }).ToList();
            return new ResponseContext<List<SxAssistSubjectDto>> { Data = subjectDtos };
        }

        #endregion
    }
}
