﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;
using YssxC.S.Pocos.Subject;

namespace YssxC.S.Service
{
    /// <summary>
    /// 账簿-辅助核算业务服务
    /// </summary>
    public class AssistAccountingService : IAssistAccountingService
    {
        /// <summary>
        /// 保存账簿-辅助核算业务服务
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Save(AssistAccountingDto model, UserTicket currentUser)
        {
            var reslut = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var entity = model.MapTo<SxAssistAccounting>();
            var repo_aa2 = DbContext.FreeSql.GetRepository<SxAssistAccounting>(e => e.IsDelete == CommonConstants.IsNotDelete && e.CaseId == model.CaseId);
            #region 参数校验
            var hasName = await repo_aa2
                .Where(e => e.Name == model.Name && e.SubjectId == model.SubjectId)
                .WhereIf(model.Id > 0, e => e.Id != model.Id).AnyAsync();
            if (hasName)
            {
                reslut.Msg = $"辅助核算名称重复【{model.Name}】";
                return reslut;
            }
            #endregion

            if (model.Id <= 0)//新增
            {
                if (model.SubjectId > 0)
                {
                    var subjcet = await DbContext.FreeSql.GetRepository<SxSubject>()
                        .Where(a => a.Id == model.SubjectId && a.IsDelete == CommonConstants.IsNotDelete)
                        .FirstAsync();
                    if (subjcet == null)
                    {
                        reslut.Msg = "科目不存在";
                        return reslut;
                    }
                    entity.SubjectCode = subjcet.SubjectCode;
                }

                entity.Id = IdWorker.NextId();
                //entity.GroupId = IdWorker.NextId();
                entity.CreateBy = currentUser.Id;

                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        var repo_s = uow.GetRepository<SxSubject>(e => e.IsDelete == CommonConstants.IsNotDelete);
                        var repo_aa = uow.GetRepository<SxAssistAccounting>(e => e.IsDelete == CommonConstants.IsNotDelete);
                        //插入数据库
                        await repo_aa.InsertAsync(entity);
                        await repo_s.UpdateDiy.Set(c => new SxSubject
                        {
                            IsHaveCheck = 1,
                            UpdateBy = currentUser.Id,
                            UpdateTime = DateTime.Now
                        }).Where(e => e.Id == model.SubjectId).ExecuteAffrowsAsync();

                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
            }
            else//修改
            {
                var oldentity = await DbContext.FreeSql.GetRepository<SxAssistAccounting>()
                    .Where(a => a.Id == model.Id && a.IsDelete == CommonConstants.IsNotDelete).FirstAsync();

                if (oldentity == null)
                {
                    reslut.Msg = "辅助核算不存在";
                    return reslut;
                }

                entity.UpdateTime = DateTime.Now;
                entity.UpdateBy = currentUser.Id;

                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        var repo_s = uow.GetRepository<SxSubject>(e => e.IsDelete == CommonConstants.IsNotDelete);
                        var repo_aa = uow.GetRepository<SxAssistAccounting>(e => e.IsDelete == CommonConstants.IsNotDelete);
                        //更新数据库数据
                        await repo_aa.UpdateDiy.SetSource(entity).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();
                        if (entity.SubjectId != oldentity.SubjectId)
                        {
                            await repo_s.UpdateDiy.Set(c => new SxSubject
                            {
                                IsHaveCheck = 1,
                                UpdateBy = currentUser.Id,
                                UpdateTime = DateTime.Now
                            }).Where(e => e.Id == entity.SubjectId).ExecuteAffrowsAsync();

                            if (oldentity.SubjectId > 0)//旧的科目信息修改
                            {
                                //查询科目下是否有辅助核算信息
                                var saCount = await repo_aa
                                    .Where(e => e.SubjectId == oldentity.SubjectId)
                                    .AnyAsync();
                                if (!saCount)//科目下没有辅助核算信息，修改科目为无科目信息
                                {
                                    await repo_s.UpdateDiy.Set(c => new SxSubject
                                    {
                                        IsHaveCheck = 0,
                                        UpdateBy = currentUser.Id,
                                        UpdateTime = DateTime.Now
                                    }).Where(e => e.Id == oldentity.SubjectId).ExecuteAffrowsAsync();
                                }
                            }
                        }
                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 删除 账簿-辅助核算信息（未使用，没有删除功能了）
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteById(long id, UserTicket currentUser)
        {
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    var repo_aa = uow.GetRepository<SxAssistAccounting>(e => e.IsDelete == CommonConstants.IsNotDelete);
                    var repo_s = uow.GetRepository<SxSubject>(e => e.IsDelete == CommonConstants.IsNotDelete);

                    var sa = await repo_aa
                        .Where(e => e.Id == id)
                        .FirstAsync();

                    //删除
                    await repo_aa.UpdateDiy.Set(c => new SxAssistAccounting
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now
                    }).Where(c => c.IsDelete == CommonConstants.IsNotDelete && (c.Id == id || c.ParentId == id)).ExecuteAffrowsAsync();

                    //查询科目下是否有辅助核算信息
                    var saCount = await repo_aa
                        .Where(e => e.SubjectId == sa.SubjectId)
                        .AnyAsync();
                    if (!saCount)//科目下没有辅助核算信息，修改科目为无科目信息
                    {
                        await repo_s.UpdateDiy.Set(c => new SxSubject
                        {
                            IsHaveCheck = 0,
                            UpdateBy = currentUser.Id,
                            UpdateTime = DateTime.Now
                        }).Where(e => e.Id == sa.SubjectId).ExecuteAffrowsAsync();
                    }
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    throw;
                }
            }

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 根据Id获取单条账簿-辅助核算信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<AssistAccountingDto>> GetInfoById(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<SxAssistAccounting>().Select
                .Where(e => e.IsDelete == CommonConstants.IsNotDelete && e.Id == id)
                .FirstAsync<AssistAccountingDto>();

            return new ResponseContext<AssistAccountingDto> { Code = CommonConstants.SuccessCode, Data = entity };
        }

        /// <summary>
        /// 根据案例id获取账簿-辅助核算信息列表
        /// </summary>
        /// <param name="caseId">案例Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<List<AssistAccountingQueryListResponseDto>>> GetListByCaseId(long caseId)
        {
            var entity = await DbContext.FreeSql.Select<SxAssistAccounting, SxSubject>()
                    .LeftJoin((a, b) => a.SubjectId == b.Id)
                    .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.CaseId == caseId)
                    .OrderBy((a, b) => a.CreateTime)
                    .ToListAsync((a, b) => new AssistAccountingQueryListResponseDto
                    {
                        BankAccountNo = a.BankAccountNo,
                        BankBranchName = a.BankBranchName,
                        BankName = a.BankName,
                        Code = a.Code,
                        Id = a.Id,
                        ParentId = a.ParentId,
                        Name = a.Name,
                        Type = a.Type,
                        SubjectName = b.SubjectName,
                        SubjectCode = b.SubjectCode,
                        SubjectId = b.Id,
                    });

            return new ResponseContext<List<AssistAccountingQueryListResponseDto>> { Code = CommonConstants.SuccessCode, Data = entity };
        }

        /// <summary>
        /// 根据caseid获取所有收款账号公司名称
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetNameListByCaseIdResponseDto>>> GetNameListByCaseId(long caseId)
        {
            var list = await DbContext.FreeSql.GetRepository<SxAssistAccounting>().Select
                    .Where(e => e.IsDelete == CommonConstants.IsNotDelete && e.CaseId == caseId && e.ParentId == 0)
                    .OrderBy(e => e.CreateTime)
                    .ToListAsync(e => new GetNameListByCaseIdResponseDto
                    {
                        Id = e.Id,
                        Name = e.Name
                    });

            return new ResponseContext<List<GetNameListByCaseIdResponseDto>> { Code = CommonConstants.SuccessCode, Data = list };
        }

        /// <summary>
        /// 根据公司id获取账簿-辅助核算分页列表
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<AssistAccountingQueryPageResponseDto>>> GetListPage(AssistAccountingQueryPageDto queryModel)
        {
            long count = 0;
            var list = await DbContext.FreeSql.Select<SxAssistAccounting, SxSubject>()
                .LeftJoin((a, b) => a.SubjectId == b.Id)
                .Where((a, b) => a.IsDelete == CommonConstants.IsNotDelete && a.ParentId == 0)
                .WhereIf(queryModel.CaseId > 0, (a, b) => a.CaseId == queryModel.CaseId)
                .WhereIf(queryModel.Type.HasValue, (a, b) => a.Type == queryModel.Type.Value)
                .WhereIf(!string.IsNullOrEmpty(queryModel.Name), (a, b) => a.Name.Contains(queryModel.Name) || a.Code.Contains(queryModel.Name))
                .Count(out count)//统计总数
                .OrderBy((a, b) => a.CreateTime)
                .Page(queryModel.PageIndex, queryModel.PageSize)//分页查询
                .ToListAsync((a, b) => new AssistAccountingQueryPageResponseDto
                {
                    BankAccountNo = a.BankAccountNo,
                    BankBranchName = a.BankBranchName,
                    BankName = a.BankName,
                    Code = a.Code,
                    Id = a.Id,
                    Name = a.Name,
                    Type = a.Type,
                    SubjectName = b.SubjectCode + " " + b.SubjectName,
                });

            var data = new PageResponse<AssistAccountingQueryPageResponseDto>(queryModel.PageIndex, queryModel.PageSize, count, list);

            return new ResponseContext<PageResponse<AssistAccountingQueryPageResponseDto>> { Code = CommonConstants.SuccessCode, Data = data };
        }

        ///// <summary>
        ///// 构建子类
        ///// </summary>
        ///// <param name="models"></param>
        ///// <param name="entity"></param>
        ///// <param name="currentUser"></param>
        //private static List<SxAssistAccounting> CreateSub(AssistAccountingDto model, SxAssistAccounting parent, UserTicket currentUser)
        //{
        //    List<SxAssistAccounting> entitys = null;
        //    if (model != null && model.Subs != null && model.Subs.Count > 0)
        //    {
        //        entitys = new List<SxAssistAccounting>();
        //        foreach (var item in model.Subs)
        //        {
        //            var sub = model.MapTo<SxAssistAccounting>();
        //            sub.Id = IdWorker.NextId();
        //            sub.Name = item.Name;
        //            sub.Type = item.Type;
        //            sub.CreateBy = currentUser.Id;
        //            sub.ParentId = parent.Id;
        //            sub.GroupId = parent.GroupId;
        //            entitys.Add(sub);
        //        }
        //    }
        //    return entitys;
        //}

        /// <summary>
        /// 根据公司id获取账簿-辅助核算名称列表
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetNameListResponseDto>>> GetNameList(GetNameListDto queryModel)
        {
            var list = await DbContext.FreeSql.GetRepository<SxAssistAccounting>()
                .Where(a => a.IsDelete == CommonConstants.IsNotDelete && a.ParentId == 0)
                .WhereIf(queryModel.CaseId > 0, a => a.CaseId == queryModel.CaseId)
                .WhereIf(queryModel.Type.HasValue, a => a.Type == queryModel.Type.Value)
                .WhereIf(queryModel.SubjectId.HasValue, a => a.SubjectId == queryModel.SubjectId.Value)
                .WhereIf(!string.IsNullOrEmpty(queryModel.Name), a => a.Name.Contains(queryModel.Name) || a.Code.Contains(queryModel.Name))
                .ToListAsync();

            List<GetNameListResponseDto> data = null;
            data = new List<GetNameListResponseDto>();
            foreach (var item in list.GroupBy(e => e.Type))
            {
                data.Add(new GetNameListResponseDto
                {
                    Type = item.Key,
                    Name = item.Key.GetDescription(),
                    Children = item.MapTo<List<SubAssistAccountingNameResponseDto>>()
                });
            }
            return new ResponseContext<List<GetNameListResponseDto>> { Code = CommonConstants.SuccessCode, Data = data };
        }
        /// <summary>
        /// 根据caseid获取所有收款账号公司名称(去重)
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetNameDistinctListByCaseIdResponseDto>>> GetNameDistinctListByCaseId(AssistAccountingQueryDto queryDto)
        {
            var select = DbContext.FreeSql.GetRepository<SxAssistAccounting>(a => a.IsDelete == CommonConstants.IsNotDelete).Select
                    .Where(a => a.CaseId == queryDto.CaseId)
                    .WhereIf(!string.IsNullOrEmpty(queryDto.Name), a => a.Name.Contains(queryDto.Name) || a.Code.Contains(queryDto.Name));


            string sqlwhere = "";
            if (queryDto.Types != null && queryDto.Types.Count > 0)
            {
                sqlwhere += $" and Type in({queryDto.Types.Join(",")})";
            }

            var sql = select.ToSql() + $"{sqlwhere} Order by CreateTime,Id";

            var list = await DbContext.FreeSql.Ado.QueryAsync<SxAssistAccounting>(sql);

            var data = new List<GetNameDistinctListByCaseIdResponseDto>();

            Dictionary<string, int> dic = new Dictionary<string, int>();
            if (list != null && list.Count > 0)
            {
                foreach (var item in list)
                {
                    string key = item.Name;
                    if (!dic.Keys.Contains(key))
                    {
                        dic[key] = 0;

                        data.Add(new GetNameDistinctListByCaseIdResponseDto
                        {
                            Id = item.Id,
                            Name = item.Name,
                            BankAccountNo = item.BankAccountNo,
                            BankBranchName = item.BankBranchName,
                            Type = item.Type,
                            BankName = item.BankName,
                        });
                    }
                }
            }

            return new ResponseContext<List<GetNameDistinctListByCaseIdResponseDto>> { Code = CommonConstants.SuccessCode, Data = data };
        }
    }
}
