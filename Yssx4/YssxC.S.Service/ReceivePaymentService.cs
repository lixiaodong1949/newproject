﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    /// <summary>
    /// 收付款服务
    /// </summary>
    public class ReceivePaymentService : IReceivePaymentService
    {
        #region 2020-06-22 逻辑修改 修改理由:显示学生作答记录
        //付款题学生作答
        // sx_exam_user_grade_detail 表中 Answer 存 金额,收款单位Id
        #endregion

        #region 获取用户收付款票据列表(未做)
        /// <summary>
        /// 获取用户收付款票据列表(未做)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<UserReceivingBillViewModel>>> GetUserReceivingBillTopicList(UserCalendarReceivingQuery query, UserTicket user)
        {
            ResponseContext<List<UserReceivingBillViewModel>> response = new ResponseContext<List<UserReceivingBillViewModel>>();
            List<UserReceivingBillViewModel> listData = new List<UserReceivingBillViewModel>();

            if (query == null || !query.StartDate.HasValue || !query.EndDate.HasValue)
                return new ResponseContext<List<UserReceivingBillViewModel>> { Code = CommonConstants.ErrorCode, Msg = "传参不能为空!" };

            if (query.StartDate > query.EndDate)
                return new ResponseContext<List<UserReceivingBillViewModel>> { Code = CommonConstants.ErrorCode, Msg = "日历开始日期不能大于日历结束日期!" };

            //结束日期加1天
            query.EndDate = query.EndDate.Value.AddDays(1);

            //查询主作答记录
            SxExamUserGrade gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == query.GradeId && x.UserId == user.Id).FirstAsync();
            if (gradeData == null)
                return new ResponseContext<List<UserReceivingBillViewModel>> { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };

            //查询票据已审核的 收付款 票据
            var select = DbContext.FreeSql.Select<SxTopicFile>().From<SxAuditedBill>(
                (a, b) =>
                a.InnerJoin(x => x.TopicId == b.QuestionId))
                .Where((a, b) => b.IsHasReceivePayment && !b.IsDoneReceivePayment && b.BusinessDate >= query.StartDate && b.BusinessDate < query.EndDate && b.UserId == user.Id
                   && b.GradeId == query.GradeId && (a.Type == TopicFileType.Normal || a.Type == TopicFileType.Push) && a.IsDelete == CommonConstants.IsNotDelete
                   && b.IsDelete == CommonConstants.IsNotDelete);

            //票据详情
            List<SxTopicFile> billFileList = new List<SxTopicFile>();

            if (query.TopicType == (int)ReceivePaymentType.PaymentTopic)
            {
                billFileList = await select.Where((a, b) => b.ReceivePaymentType == ReceivePaymentType.PaymentTopic).ToListAsync();
            }
            if (query.TopicType == (int)ReceivePaymentType.ReceiveTopic)
            {
                billFileList = await select.Where((a, b) => b.ReceivePaymentType == ReceivePaymentType.ReceiveTopic).ToListAsync();
            }

            //题目父级Id
            List<long> parentIds = billFileList.Select(x => x.ParentTopicId).Distinct().ToList();
            //回执单详情
            List<SxTopicFile> receiptFileList = await DbContext.FreeSql.Select<SxTopicFile>().From<SxExamUserGradeDetail>(
                (a, b) =>
                a.InnerJoin(x => x.TopicId == b.QuestionId))
                .OrderBy((a, b) => b.CreateTime)
                .Where((a, b) => b.GradeId == query.GradeId && b.UserId == user.Id && parentIds.Contains(b.ParentQuestionId) && (b.QuestionType == QuestionType.ReceiptTopic ||
                b.QuestionType == QuestionType.PayTopic) && a.Type == TopicFileType.Reply && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete ==
                CommonConstants.IsNotDelete).ToListAsync();
            //添加回执单
            billFileList.AddRange(receiptFileList);

            List<SxTopic> topicList = await DbContext.FreeSql.Select<SxTopic>().Where(x => parentIds.Contains(x.Id)).ToListAsync();
            topicList.ForEach(x =>
            {
                UserReceivingBillViewModel model = new UserReceivingBillViewModel();
                model.ParentQuestionId = x.Id;
                model.TopicName = x.Title;
                model.TopicType = query.TopicType;

                List<SxTopicFile> fileList = billFileList.Where(a => a.ParentTopicId == x.Id).ToList();
                //设置封面排序
                fileList = SetBillTopicFileSort(fileList);
                fileList.ForEach(a =>
                 model.BillReceiptFileList.Add(new BillReceiptFileViewModel
                 {
                     Url = a.Url,
                     CoverBack = (Nullable<int>)a.CoverBack,
                 }));

                listData.Add(model);
            });

            response.Data = listData;

            return response;
        }
        #endregion

        #region 获取付款题列表
        /// <summary>
        /// 获取付款题列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<PaymentTopicInfoViewModel>>> GetPaymentTopicInfoList(ReceivePaymentTopicInfoQuery query, UserTicket user)
        {
            ResponseContext<List<PaymentTopicInfoViewModel>> response = new ResponseContext<List<PaymentTopicInfoViewModel>>();
            List<PaymentTopicInfoViewModel> listData = new List<PaymentTopicInfoViewModel>();

            if (query == null)
                return new ResponseContext<List<PaymentTopicInfoViewModel>> { Code = CommonConstants.ErrorCode, Msg = "传参不能为空!" };

            //查询主作答记录
            SxExamUserGrade gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == query.GradeId && x.UserId == user.Id).FirstAsync();
            if (gradeData == null)
                return new ResponseContext<List<PaymentTopicInfoViewModel>> { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };

            #region 无答案查询
            /*
            //获取题目列表
            var topicList = await DbContext.FreeSql.Select<SxTopic>().From<SxTopicPayAccountInfo, SxExamUserGradeAsset>(
                (a, b, c) =>
                a.InnerJoin(x => x.Id == b.TopicId)
                .InnerJoin(x => b.PayBankId == c.BankAccountId))
                .Where((a, b, c) => a.ParentId == query.ParentQuestionId && a.QuestionType == QuestionType.PayTopic && c.GradeId == query.GradeId && c.UserId == user.Id
                    && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b, c) => new
                {
                    QuestionId = a.Id,
                    PaymentOpenBank = c.OpeningBank,
                    PaymentSubBank = c.SubBranch,
                    PaymentAccount = c.BankAccountNo,
                    PaymentAccountBalance = c.AccountBalance,
                });
            */
            #endregion

            //获取题目列表
            var topicList = await DbContext.FreeSql.Select<SxTopic>().From<SxTopicPayAccountInfo, SxAssistAccounting, SxExamUserGradeAsset>(
                (a, b, c, d) =>
                a.InnerJoin(x => x.Id == b.TopicId)
                .InnerJoin(x => b.PayeeCompanyId == c.Id)
                .InnerJoin(x => b.PayBankId == d.BankAccountId))
                .Where((a, b, c, d) => a.ParentId == query.ParentQuestionId && a.QuestionType == QuestionType.PayTopic && d.GradeId == query.GradeId && d.UserId == user.Id
                    && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete
                     && d.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b, c, d) => new
                {
                    QuestionId = a.Id,
                    ReceivingBankId = c.Id,
                    Type = c.Type,
                    ReceivingBankName = c.Name,
                    ReceivingOpenBank = c.BankName,
                    ReceivingBranchBank = c.BankBranchName,
                    ReceivingAccount = c.BankAccountNo,
                    PaymentOpenBank = d.OpeningBank,
                    PaymentSubBank = d.SubBranch,
                    PaymentAccount = d.BankAccountNo,
                    PaymentAccountBalance = d.AccountBalance,
                });

            //获取已作答题目回执单
            List<SxTopicFile> receiptList = await DbContext.FreeSql.Select<SxTopicFile>().From<SxExamUserGradeDetail>(
                (a, b) =>
                a.InnerJoin(x => x.TopicId == b.QuestionId))
                .Where((a, b) => b.GradeId == query.GradeId && b.UserId == user.Id && b.QuestionType == QuestionType.PayTopic && b.ParentQuestionId == query.ParentQuestionId
                && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            topicList.ForEach(x =>
            {
                PaymentTopicInfoViewModel model = new PaymentTopicInfoViewModel();
                model.PaymentQuestionId = x.QuestionId;
                model.ReceivingBankId = x.ReceivingBankId;
                model.Type = (Nullable<int>)x.Type;
                model.ReceivingBankName = x.ReceivingBankName;
                model.ReceivingOpenBank = x.ReceivingOpenBank;
                model.ReceivingBranchBank = x.ReceivingBranchBank;
                model.ReceivingAccount = x.ReceivingAccount;
                model.PaymentOpenBank = x.PaymentOpenBank;
                model.PaymentSubBank = x.PaymentSubBank;
                model.PaymentAccount = x.PaymentAccount;
                model.PaymentAccountBalance = x.PaymentAccountBalance;

                SxTopicFile receiptData = receiptList.Where(a => a.TopicId == x.QuestionId).FirstOrDefault();
                if (receiptData != null)
                {
                    model.ReceiptUrl = receiptData.Url;
                    model.IsPaid = true;
                }

                listData.Add(model);
            });

            response.Data = listData;

            return response;
        }
        #endregion

        #region 获取收款题列表
        /// <summary>
        /// 获取收款题列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<ReceiveTopicInfoViewModel>>> GetReceiveTopicInfoList(ReceivePaymentTopicInfoQuery query, UserTicket user)
        {
            ResponseContext<List<ReceiveTopicInfoViewModel>> response = new ResponseContext<List<ReceiveTopicInfoViewModel>>();
            List<ReceiveTopicInfoViewModel> listData = new List<ReceiveTopicInfoViewModel>();

            if (query == null)
                return new ResponseContext<List<ReceiveTopicInfoViewModel>> { Code = CommonConstants.ErrorCode, Msg = "传参不能为空!" };

            //查询主作答记录
            SxExamUserGrade gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == query.GradeId && x.UserId == user.Id).FirstAsync();
            if (gradeData == null)
                return new ResponseContext<List<ReceiveTopicInfoViewModel>> { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };

            //获取题目列表
            var topicList = await DbContext.FreeSql.Select<SxTopic>().From<SxTopicPayAccountInfo, SxAssistAccounting, SxExamUserGradeAsset>(
                (a, b, c, d) =>
                a.InnerJoin(x => x.Id == b.TopicId)
                .InnerJoin(x => b.PayCompanyId == c.Id)
                .InnerJoin(x => b.PayeeBankId == d.BankAccountId))
                .Where((a, b, c, d) => a.ParentId == query.ParentQuestionId && a.QuestionType == QuestionType.ReceiptTopic && d.GradeId == query.GradeId && d.UserId == user.Id
                    && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete && c.IsDelete == CommonConstants.IsNotDelete
                     && d.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync((a, b, c, d) => new
                {
                    BusinessDate = a.BusinessDate,
                    QuestionId = a.Id,
                    Amount = b.Amount,
                    PaymentUnitName = c.Name,
                    PaymentOpenBank = c.BankName,
                    PaymentSubBank = c.BankBranchName,
                    PaymentAccount = c.BankAccountNo,
                    ReceivingOpenBank = d.OpeningBank,
                    ReceivingBranchBank = d.SubBranch,
                    ReceivingAccount = d.BankAccountNo,
                    ReceivingBalance = d.AccountBalance,
                });

            //获取已作答题目回执单
            List<SxTopicFile> receiptList = await DbContext.FreeSql.Select<SxTopicFile>().From<SxExamUserGradeDetail>(
                (a, b) =>
                a.InnerJoin(x => x.TopicId == b.QuestionId))
                .Where((a, b) => b.GradeId == query.GradeId && b.UserId == user.Id && b.QuestionType == QuestionType.ReceiptTopic && b.ParentQuestionId == query.ParentQuestionId
                && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            topicList.ForEach(x =>
            {
                ReceiveTopicInfoViewModel model = new ReceiveTopicInfoViewModel();
                model.BusinessDate = x.BusinessDate;
                model.ReceiveQuestionId = x.QuestionId;
                model.ReceivingOpenBank = x.ReceivingOpenBank;
                model.ReceivingBranchBank = x.ReceivingBranchBank;
                model.ReceivingAccount = x.ReceivingAccount;
                model.ReceivingBalance = x.ReceivingBalance;
                model.PaymentUnitName = x.PaymentUnitName;
                model.PaymentOpenBank = x.PaymentOpenBank;
                model.PaymentSubBank = x.PaymentSubBank;
                model.PaymentAccount = x.PaymentAccount;
                model.Amount = x.Amount;

                SxTopicFile receiptData = receiptList.Where(a => a.TopicId == x.QuestionId).FirstOrDefault();
                if (receiptData != null)
                {
                    model.ReceiptUrl = receiptData.Url;
                    model.IsReceived = true;
                }

                listData.Add(model);
            });

            response.Data = listData;

            return response;
        }
        #endregion

        #region 付款操作
        /// <summary>
        /// 付款操作
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserPaymentTopicViewModel>> PaymentOperate(UserPaymentTopicDto dto, UserTicket user)
        {
            ResponseContext<UserPaymentTopicViewModel> response = new ResponseContext<UserPaymentTopicViewModel>();
            UserPaymentTopicViewModel data = new UserPaymentTopicViewModel();

            #region 参数验证

            //查询试卷信息
            var examData = await DbContext.FreeSql.Select<SxExamPaper>().Where(a => a.Status != ExamStatus.End && a.Id == dto.ExamId && a.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync();
            if (examData == null)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "试卷不存在!" };

            //查询主作答记录
            SxExamUserGrade gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == dto.GradeId && x.UserId == user.Id).FirstAsync();
            if (gradeData == null)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };
            if (gradeData.Status == StudentExamStatus.End)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "答题结束,不允许作答!" };
            if (gradeData.IsSettled)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "已结账,不允许作答!" };

            //查询题目信息
            SxTopic topicData = await DbContext.FreeSql.GetRepository<SxTopic>().Where(x => x.Id == dto.PaymentQuestionId).FirstAsync();
            if (topicData == null)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目不存在!" };
            if (topicData.QuestionType != QuestionType.PayTopic)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目不是付款题!" };
            if (topicData.PositionId != dto.PositionId && topicData.PointPositionId != dto.PositionId)//综岗，分岗
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "做题岗位和题目岗位不匹配!" };

            //查询作答记录明细
            SxExamUserGradeDetail gradeDetailData = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Where(x => x.GradeId == dto.GradeId && x.UserId == user.Id
                && x.QuestionId == dto.PaymentQuestionId && x.IsDelete == CommonConstants.IsNotDelete).Master().FirstAsync();
            if (gradeDetailData != null)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目已作答!" };

            //查询已审核票据
            SxAuditedBill auditeData = await DbContext.FreeSql.GetRepository<SxAuditedBill>().Where(x => x.ParentQuestionId == topicData.ParentId && x.GradeId == dto.GradeId
               && x.UserId == user.Id && x.ReceivePaymentType == ReceivePaymentType.PaymentTopic && !x.IsDoneReceivePayment && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (auditeData == null)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目错误,请联系管理员!" };

            //查询分录综合题下有几道付款题
            long topicCount = await DbContext.FreeSql.GetRepository<SxTopic>().Where(x => x.ParentId == topicData.ParentId && x.QuestionType == QuestionType.PayTopic
                && x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
            //查询已做付款题数量
            long hasDoneCount = await DbContext.FreeSql.Select<SxExamUserGradeDetail>()
                .Where(x => x.GradeId == dto.GradeId && x.UserId == user.Id && x.QuestionType == QuestionType.PayTopic && x.ParentQuestionId == topicData.ParentId
                && x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
            if (topicCount == hasDoneCount)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "付款题已做完!" };

            //是否更新已审票据表 标记付款题都已完成
            bool IsUpdateAudite = false;
            if (topicCount - hasDoneCount == 1)
                IsUpdateAudite = true;

            //查询答案信息
            SxTopicPayAccountInfo payAccountData = await DbContext.FreeSql.GetRepository<SxTopicPayAccountInfo>().Where(x => x.TopicId == topicData.Id &&
                x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (payAccountData == null)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目错误,答案不存在,请联系管理员!" };

            //查询用户作答信息资产
            SxExamUserGradeAsset assetGradeDetail = await DbContext.FreeSql.GetRepository<SxExamUserGradeAsset>().Where(x => x.BankAccountId == payAccountData.PayBankId &&
             x.GradeId == dto.GradeId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (assetGradeDetail == null)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目错误,用户银行账户不存在,请联系管理员!" };

            //查询回单
            SxTopicFile fileData = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => x.TopicId == topicData.Id && x.Type == TopicFileType.Reply &&
                x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            data.ReceiptUrl = fileData?.Url;

            #endregion

            #region 初始化数据

            //修改用户账户余额
            //已用金额
            assetGradeDetail.AccountUsedAmount = assetGradeDetail.AccountUsedAmount + payAccountData.Amount;
            //剩余金额
            assetGradeDetail.AccountBalance = assetGradeDetail.AccountBalance - payAccountData.Amount;
            if (assetGradeDetail.AccountBalance < 0)
                return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目错误,余额小于0,请联系管理员!" };
            assetGradeDetail.UpdateBy = user.Id;
            assetGradeDetail.UpdateTime = DateTime.Now;

            //初始化题目作答明细
            SxExamUserGradeDetail topicGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                QuestionId = dto.PaymentQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                QuestionType = topicData.QuestionType,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = true,
                Answer = string.Format("{0},{1}", dto.Amount, dto.ReceivingBankId), // 2020-06-22 付款 存 金额,收款单位Id

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            #endregion

            #region 判断答案是否正确
            //作答正确
            if (dto.ReceivingBankId == payAccountData.PayeeCompanyId && dto.Amount == payAccountData.Amount)
            {
                topicGradeDetail.Status = AnswerResultStatus.Right;
                //记录分数
                topicGradeDetail.Score = topicData.Score;

                data.IsRightAnswers = true;
                data.Score = topicData.Score;

            }
            //作答错误
            else
            {
                topicGradeDetail.Status = AnswerResultStatus.Error;
                //记录分数
                topicGradeDetail.Score = 0;

                data.IsRightAnswers = false;
                data.Score = 0;
                data.RightAmount = payAccountData.Amount;

                //查询正确收款单位
                SxAssistAccounting assistAccountData = await DbContext.FreeSql.GetRepository<SxAssistAccounting>().Where(x => x.Id == payAccountData.PayeeCompanyId &&
                    x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                data.ReceivingBankName = assistAccountData?.Name;

                //查询付款错误突发机制
                ResponseContext<UserCheckBillTopicViewModel> emResponse = await QueryEmergencyMechanism((int)EmergencyType.PaymentError);
                if (emResponse.Code == CommonConstants.SuccessCode)
                {
                    data.ErrorWriting = emResponse.Data.ErrorWriting;
                    data.ImgUrl = emResponse.Data.ImgUrl;
                }
            }
            #endregion

            #region 处理数据

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    // 生成综合分录题作答明细
                    SxExamUserGradeDetail comGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(topicGradeDetail);
                    //更新账户余额
                    int executeRow = await uow.GetRepository<SxExamUserGradeAsset>().UpdateDiy.SetSource(assetGradeDetail).UpdateColumns(x => new
                    {
                        x.AccountUsedAmount,
                        x.AccountBalance,
                        x.UpdateBy,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();
                    //更新已审核票据 改为付款题已做完
                    if (IsUpdateAudite)
                    {
                        auditeData.IsDoneReceivePayment = true;
                        auditeData.UpdateBy = user.Id;
                        auditeData.UpdateTime = DateTime.Now;

                        await uow.GetRepository<SxAuditedBill>().UpdateDiy.SetSource(auditeData).UpdateColumns(x => new
                        {
                            x.IsDoneReceivePayment,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrowsAsync();
                    }

                    if (comGrade != null && executeRow > 0)
                    {
                        uow.Commit();
                    }
                    else
                    {
                        uow.Rollback();
                        return new ResponseContext<UserPaymentTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "操作失败!" };
                    }

                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            #endregion

            response.Data = data;

            return response;
        }
        #endregion

        #region 收款操作
        /// <summary>
        /// 收款操作
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<UserReceiveTopicViewModel>> ReceiveOperate(UserReceiveTopicDto dto, UserTicket user)
        {
            ResponseContext<UserReceiveTopicViewModel> response = new ResponseContext<UserReceiveTopicViewModel>();
            UserReceiveTopicViewModel data = new UserReceiveTopicViewModel();

            #region 参数验证

            //查询试卷信息
            var examData = await DbContext.FreeSql.Select<SxExamPaper>().Where(a => a.Status != ExamStatus.End && a.Id == dto.ExamId && a.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync();
            if (examData == null)
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "试卷不存在!" };

            //查询主作答记录
            SxExamUserGrade gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == dto.GradeId && x.UserId == user.Id).FirstAsync();
            if (gradeData == null)
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };
            if (gradeData.Status == StudentExamStatus.End)
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "答题结束,不允许作答!" };
            if (gradeData.IsSettled)
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "已结账,不允许作答!" };

            //查询题目信息
            SxTopic topicData = await DbContext.FreeSql.GetRepository<SxTopic>().Where(x => x.Id == dto.ReceiveQuestionId).FirstAsync();
            if (topicData == null)
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目不存在!" };
            if (topicData.QuestionType != QuestionType.ReceiptTopic)
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目不是收款题!" };
            if (topicData.PositionId != dto.PositionId && topicData.PointPositionId != dto.PositionId)//分岗、综岗
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "做题岗位和题目岗位不匹配!" };

            //查询作答记录明细
            SxExamUserGradeDetail gradeDetailData = await DbContext.FreeSql.GetRepository<SxExamUserGradeDetail>().Where(x => x.GradeId == dto.GradeId && x.UserId == user.Id
                && x.QuestionId == dto.ReceiveQuestionId && x.IsDelete == CommonConstants.IsNotDelete).Master().FirstAsync();
            if (gradeDetailData != null)
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目已作答!" };

            //查询已审核票据
            SxAuditedBill auditeData = await DbContext.FreeSql.GetRepository<SxAuditedBill>().Where(x => x.ParentQuestionId == topicData.ParentId && x.GradeId == dto.GradeId
               && x.UserId == user.Id && x.ReceivePaymentType == ReceivePaymentType.ReceiveTopic && !x.IsDoneReceivePayment && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (auditeData == null)
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目错误,请联系管理员!" };

            //查询分录综合题下有几道收款题
            long topicCount = await DbContext.FreeSql.GetRepository<SxTopic>().Where(x => x.ParentId == topicData.ParentId && x.QuestionType == QuestionType.ReceiptTopic
                && x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
            //查询已做收款题数量
            long hasDoneCount = await DbContext.FreeSql.Select<SxExamUserGradeDetail>()
                .Where(x => x.GradeId == dto.GradeId && x.UserId == user.Id && x.QuestionType == QuestionType.ReceiptTopic && x.ParentQuestionId == topicData.ParentId
                && x.IsDelete == CommonConstants.IsNotDelete).CountAsync();
            if (topicCount == hasDoneCount)
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "收款题已做完!" };

            //是否更新已审票据表 标记收款题都已完成
            bool IsUpdateAudite = false;
            if (topicCount - hasDoneCount == 1)
                IsUpdateAudite = true;

            //查询答案信息
            SxTopicPayAccountInfo payAccountData = await DbContext.FreeSql.GetRepository<SxTopicPayAccountInfo>().Where(x => x.TopicId == topicData.Id &&
                x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (payAccountData == null)
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目错误,付款账户不存在,请联系管理员!" };

            //查询用户作答信息资产
            SxExamUserGradeAsset assetGradeDetail = await DbContext.FreeSql.GetRepository<SxExamUserGradeAsset>().Where(x => x.BankAccountId == payAccountData.PayeeBankId &&
             x.GradeId == dto.GradeId && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (assetGradeDetail == null)
                return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "题目错误,收款银行不存在,请联系管理员!" };

            //查询回单
            SxTopicFile fileData = await DbContext.FreeSql.GetRepository<SxTopicFile>().Where(x => x.TopicId == topicData.Id && x.Type == TopicFileType.Reply &&
                x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            data.ReceiptUrl = fileData?.Url;

            #endregion

            #region 初始化数据

            //修改用户账户余额            
            //账户总额
            assetGradeDetail.TotalAccount = assetGradeDetail.TotalAccount + payAccountData.Amount;
            //账户余额
            assetGradeDetail.AccountBalance = assetGradeDetail.AccountBalance + payAccountData.Amount;
            assetGradeDetail.UpdateBy = user.Id;
            assetGradeDetail.UpdateTime = DateTime.Now;

            //初始化题目作答明细
            SxExamUserGradeDetail topicGradeDetail = new SxExamUserGradeDetail
            {
                Id = IdWorker.NextId(),
                UserId = user.Id,
                ExamId = examData.Id,
                GradeId = dto.GradeId,
                //记录分数
                Score = topicData.Score,
                Status = AnswerResultStatus.Right,
                QuestionId = dto.ReceiveQuestionId,
                ParentQuestionId = topicData.ParentId,
                PositionId = dto.PositionId,
                QuestionType = topicData.QuestionType,
                BusinessDate = topicData.BusinessDate,
                //记录是否有收付款
                IsHasReceivePayment = true,

                CreateBy = user.Id,
                CreateTime = DateTime.Now,
                UpdateBy = user.Id,
                UpdateTime = DateTime.Now
            };

            #endregion

            #region 处理数据

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    // 生成综合分录题作答明细
                    SxExamUserGradeDetail comGrade = await uow.GetRepository<SxExamUserGradeDetail>().InsertAsync(topicGradeDetail);
                    //更新账户余额
                    int executeRow = await uow.GetRepository<SxExamUserGradeAsset>().UpdateDiy.SetSource(assetGradeDetail).UpdateColumns(x => new
                    {
                        x.TotalAccount,
                        x.AccountBalance,
                        x.UpdateBy,
                        x.UpdateTime
                    }).ExecuteAffrowsAsync();
                    //更新已审核票据 改为收款题已做完
                    if (IsUpdateAudite)
                    {
                        auditeData.IsDoneReceivePayment = true;
                        auditeData.UpdateBy = user.Id;
                        auditeData.UpdateTime = DateTime.Now;

                        await uow.GetRepository<SxAuditedBill>().UpdateDiy.SetSource(auditeData).UpdateColumns(x => new
                        {
                            x.IsDoneReceivePayment,
                            x.UpdateBy,
                            x.UpdateTime
                        }).ExecuteAffrowsAsync();
                    }

                    if (comGrade != null && executeRow > 0)
                    {
                        uow.Commit();
                    }
                    else
                    {
                        uow.Rollback();
                        return new ResponseContext<UserReceiveTopicViewModel> { Code = CommonConstants.ErrorCode, Msg = "操作失败!" };
                    }
                }
                catch (Exception)
                {
                    uow.Rollback();
                }
            }

            #endregion

            data.Score = topicData.Score;

            response.Data = data;

            return response;
        }
        #endregion

        #region 查询用户收付款列表(已做)
        /// <summary>
        /// 查询用户收付款列表(已做)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<UserReceivingBillViewModel>>> QueryUserHasDoneReceivingList(UserHasDoneReceivingQuery query, UserTicket user)
        {
            ResponseContext<List<UserReceivingBillViewModel>> response = new ResponseContext<List<UserReceivingBillViewModel>>();
            List<UserReceivingBillViewModel> listData = new List<UserReceivingBillViewModel>();

            if (query == null || !query.StartDate.HasValue || !query.EndDate.HasValue)
                return new ResponseContext<List<UserReceivingBillViewModel>> { Code = CommonConstants.ErrorCode, Msg = "传参不能为空!" };

            if (query.StartDate > query.EndDate)
                return new ResponseContext<List<UserReceivingBillViewModel>> { Code = CommonConstants.ErrorCode, Msg = "日历开始日期不能大于日历结束日期!" };

            //结束日期加1天
            query.EndDate = query.EndDate.Value.AddDays(1);

            //查询主作答记录
            SxExamUserGrade gradeData = await DbContext.FreeSql.GetRepository<SxExamUserGrade>().Where(x => x.Id == query.GradeId && x.UserId == user.Id).FirstAsync();
            if (gradeData == null)
                return new ResponseContext<List<UserReceivingBillViewModel>> { Code = CommonConstants.ErrorCode, Msg = "主作答记录不存在!" };

            //查询票据已审核的 收付款 票据、回单
            List<SxTopicFile> billFileList = await DbContext.FreeSql.Select<SxTopicFile>().From<SxAuditedBill>(
                (a, b) =>
                a.InnerJoin(x => x.ParentTopicId == b.ParentQuestionId))
                .Where((a, b) => b.IsHasReceivePayment && b.IsDoneReceivePayment && b.BusinessDate >= query.StartDate && b.BusinessDate < query.EndDate && b.UserId == user.Id
                   && b.GradeId == query.GradeId && (a.Type == TopicFileType.Normal || a.Type == TopicFileType.Push ||
                   a.Type == TopicFileType.Reply) && a.IsDelete == CommonConstants.IsNotDelete && b.IsDelete == CommonConstants.IsNotDelete).ToListAsync();

            //题目父级Id
            List<long> parentIds = billFileList.Select(x => x.ParentTopicId).Distinct().ToList();

            List<SxTopic> topicList = await DbContext.FreeSql.Select<SxTopic>().Where(x => parentIds.Contains(x.Id)).ToListAsync();
            topicList.ForEach(x =>
            {
                UserReceivingBillViewModel model = new UserReceivingBillViewModel();
                model.ParentQuestionId = x.Id;
                model.TopicName = x.Title;
                model.TopicType = (int)x.ReceivePaymentType;

                List<SxTopicFile> fileList = billFileList.Where(a => a.ParentTopicId == x.Id).ToList();
                //设置封面排序
                fileList = SetBillTopicFileSort(fileList);
                fileList.ForEach(a =>
                 model.BillReceiptFileList.Add(new BillReceiptFileViewModel
                 {
                     Url = a.Url,
                     CoverBack = (Nullable<int>)a.CoverBack,
                 }));

                listData.Add(model);
            });

            response.Data = listData;

            return response;
        }
        #endregion

        #region 私有方法

        #region 设置票据详情列表排序(封面/底面)
        /// <summary>
        /// 设置票据详情列表排序(封面/底面)
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private List<SxTopicFile> SetBillTopicFileSort(List<SxTopicFile> list)
        {
            List<SxTopicFile> result = new List<SxTopicFile>();
            if (list == null || list.Count == 0)
                return result;

            list = list.OrderBy(x => x.Sort).ThenBy(x => x.CreateTime).ToList();
            int listCount = list.Count;

            //封面
            SxTopicFile coverModel = list.FirstOrDefault(a => a.CoverBack == TopicFileCoverBack.Cover);
            if (coverModel != null)
                list.Remove(coverModel);

            if (coverModel != null)
                result.Add(coverModel);

            result.AddRange(list);

            return result;
        }
        #endregion

        #region 查询突发机制
        /// <summary>
        /// 查询突发机制
        /// </summary>
        /// <param name="emergencyType">类别标记</param>
        /// <returns></returns>
        private async Task<ResponseContext<UserCheckBillTopicViewModel>> QueryEmergencyMechanism(int emergencyType)
        {
            ResponseContext<UserCheckBillTopicViewModel> response = new ResponseContext<UserCheckBillTopicViewModel>();
            UserCheckBillTopicViewModel data = new UserCheckBillTopicViewModel();

            //查询突发机制类别
            SxEmergency emergencyData = await DbContext.FreeSql.GetRepository<SxEmergency>().Where(x => x.CategoryMark == emergencyType
                && x.Status == 0).FirstAsync();
            if (emergencyData == null)
                return new ResponseContext<UserCheckBillTopicViewModel> { Code = CommonConstants.ErrorCode, Data = data, Msg = "不存在突发机制!" };

            //随机查询一条突发机制
            string sql = string.Format("SELECT a.Details AS ErrorWriting,a.ImgUrl FROM sx_emergencydetails a WHERE TenantId={0} AND a.IsDelete = 0 ORDER BY RAND() LIMIT 1",
                    emergencyData.Id);
            var emergencyDetails = await DbContext.FreeSql.Ado.QueryAsync<UserCheckBillTopicViewModel>(sql);
            if (emergencyDetails != null && emergencyDetails.Count > 0)
            {
                data.ErrorWriting = emergencyDetails[0].ErrorWriting;
                data.ImgUrl = emergencyDetails[0].ImgUrl;
            }

            response.Data = data;

            return response;
        }
        #endregion

        #endregion
    }
}
