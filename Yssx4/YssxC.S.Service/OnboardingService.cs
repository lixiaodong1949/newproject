using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    /// <summary>
    /// 入职
    /// </summary>
    public class OnboardingService : IOnboardingService
    {
        #region 选择行业
        /// <summary>
        /// 获取所有行业 - 地图数据
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetIndustryInMapDto>>> GetIndustryInMap(int userType)
        {
            var selectData = await DbContext.FreeSql.GetRepository<SxIndustry>()
                .Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(userType != (int)UserTypeEnums.Professional, x => x.Status == 0)
                .OrderBy(x => x.Sort)
                .OrderByDescending(x => x.CreateTime)
                .ToListAsync<GetIndustryInMapDto>();

            return new ResponseContext<List<GetIndustryInMapDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
        }

        /// <summary>
        /// 获取所有行业 - 下拉框数据
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetIndustryAllDataDto>>> GetIndustryAllData(int userType)
        {
            var selectData = await DbContext.FreeSql.Select<SxIndustry>()
                .Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(userType != (int)UserTypeEnums.Professional, x => x.Status == 0)
                .OrderBy(x => x.Sort).OrderByDescending(x => x.CreateTime)
                .ToListAsync(x => new GetIndustryAllDataDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Map = x.Map
                });
            return new ResponseContext<List<GetIndustryAllDataDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
        }

        /// <summary>
        /// 获取指定行业信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<IndustryDto>> GetIndustryById(long id, int userType)
        {
            return await Task.Run(() =>
            {
                var entity = DbContext.FreeSql.Select<SxIndustry>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (entity == null)
                    return new ResponseContext<IndustryDto> { Code = CommonConstants.ErrorCode, Msg = "找不到行业数据" };
                if (userType != (int)UserTypeEnums.Professional && entity.Status == 1)
                    return new ResponseContext<IndustryDto> { Code = CommonConstants.ErrorCode, Msg = "行业已禁用" };
                var resData = entity.MapTo<IndustryDto>();

                return new ResponseContext<IndustryDto> { Code = CommonConstants.SuccessCode, Data = resData };
            });
        }

        /// <summary>
        /// 根据行业id获取行业基本信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<IndustryBaseInfoDto>> GetIndustryBaseInfo(long id, int userType)
        {
            if (id <= 0)
                return new ResponseContext<IndustryBaseInfoDto> { Code = CommonConstants.ErrorCode, Msg = "参数不能为空" };

            var entity = await DbContext.FreeSql.Select<SxIndustry>()
                .Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(userType != (int)UserTypeEnums.Professional, x => x.Status == 0)
                .FirstAsync<IndustryBaseInfoDto>();

            return new ResponseContext<IndustryBaseInfoDto> { Code = CommonConstants.SuccessCode, Data = entity };
        }

        #endregion

        #region 选择企业
        /// <summary>
        /// 获取行业下所有企业 - 地图数据
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetCaseByIndustryDto>>> GetCaseByIndustry(long id, int userType)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetRepository<SxCase>().Where(x => x.IndustryId == id && x.IsDelete == CommonConstants.IsNotDelete)
                    .WhereIf(userType != (int)UserTypeEnums.Professional, x => x.IsActive)
                    .OrderBy(x => x.Sort).OrderByDescending(x => x.CreateTime).ToList(x => new GetCaseByIndustryDto
                    {
                        Id = x.Id,
                        Name = x.Name,
                        BriefName = x.BriefName,
                        LogoUrl = x.LogoUrl,
                        XCoordinate = x.XCoordinate,
                        YCoordinate = x.YCoordinate,
                        Sort = x.Sort
                    });
                return new ResponseContext<List<GetCaseByIndustryDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
            });
        }

        /// <summary>
        /// 获取指定企业详细数据
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<GetCaseInfoOnboardingDto>> GetCaseInfoOnboarding(long id, int userType)
        {
            return await Task.Run(() =>
            {
                var rIsProfessional = userType == (int)UserTypeEnums.Professional;
                var entity = DbContext.FreeSql.GetRepository<SxCase>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete)
                    .WhereIf(!rIsProfessional, x => x.IsActive).First();
                if (entity == null)
                    return new ResponseContext<GetCaseInfoOnboardingDto> { Code = CommonConstants.ErrorCode, Msg = "找不到企业数据" };
                var resData = entity.MapTo<GetCaseInfoOnboardingDto>();
                //行业
                var industryEntity = DbContext.FreeSql.GetRepository<SxIndustry>().Where(x => x.Id == entity.IndustryId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (industryEntity != null)
                    resData.IndustryName = industryEntity.Name;
                //银行账户数据
                var rBankAccountData = DbContext.FreeSql.GetRepository<SxBankAccount>().Where(x => x.CaseId == id && x.IsDelete == CommonConstants.IsNotDelete && x.Default).ToList();
                resData.BankAccountDetail = rBankAccountData.Select(x => new BankAccountDetail
                {
                    OpeningBank = x.OpeningBank,
                    Branch = x.Branch,
                    AccountNo = x.AccountNo,
                }).ToList();
                //默认 开户行、分行、账户
                var bankEntity = rBankAccountData.Where(x => x.Default).FirstOrDefault();
                if (bankEntity != null)
                {
                    resData.OpeningBank = bankEntity.OpeningBank;
                    resData.Branch = bankEntity.Branch;
                    resData.AccountNo = bankEntity.AccountNo;
                }
                //月度任务
                var monthData = DbContext.FreeSql.GetRepository<SxTask>().Where(x => x.CaseId == id && x.IsDelete == CommonConstants.IsNotDelete)
                    .WhereIf(!rIsProfessional, x => x.IsActive)
                    .OrderBy(x => x.IssueDate).ToList(x => new CaseInfoMonthDetail
                    {
                        Id = x.Id,
                        Name = x.Name
                    });
                resData.MonthDetail = monthData;

                return new ResponseContext<GetCaseInfoOnboardingDto> { Code = CommonConstants.SuccessCode, Data = resData };
            });
        }

        #endregion

        #region 月度任务
        /// <summary>
        /// 根据企业查询月度任务
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetMonthTaskListDto>>> GetMonthTaskList(long id, long currentUserId, int userType, int platform = 0)
        {
            var selectData = await DbContext.FreeSql.Select<SxTask>().Where(a => a.CaseId == id && a.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(userType != (int)UserTypeEnums.Professional, a => a.IsActive)
                .OrderBy(a => a.IssueDate)
                .OrderByDescending(a => a.CreateTime)
                .ToListAsync(a => new GetMonthTaskListDto
                {
                    Id = a.Id,
                    Name = a.Name,
                    Description = a.Description,
                    Target = a.Target,
                    RequireAccountSet = a.RequireAccountSet,
                    Integral = a.Integral,
                    IsCompleted = false,
                    IsPaid = false,
                    ExamId = 0,
                    GradeId = 0,
                    SuggestDuration = a.SuggestDuration
                });

            foreach (var item in selectData)
            {
                var rExamPaper = await DbContext.FreeSql.Select<SxExamPaper>()
                    .Where(a => a.CaseId == id && a.MonthTaskId == item.Id && a.UserId == currentUserId && a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy(a => a.CreateTime)
                    .FirstAsync();
                if (rExamPaper != null && rExamPaper.Id > 0)
                {
                    item.ExamId = rExamPaper.Id;
                    if (!currentUserId.Equals(CommonConstants.DefaultUserId))
                    {
                        item.IsPaid = true;
                        var rGradeData = await DbContext.FreeSql.GetGuidRepository<SxExamUserGrade>(x => x.IsDelete == CommonConstants.IsNotDelete)
                        .Where(x => x.ExamId == item.ExamId && x.UserId == currentUserId && x.Platform == platform && x.Status != StudentExamStatus.End)
                            .OrderByDescending(x => x.CreateTime).FirstAsync();
                        item.GradeId = rGradeData == null ? 0 : rGradeData.Id;
                        item.IsCompleted = rGradeData != null && rGradeData.Status == StudentExamStatus.End;
                    }
                }
                else
                {
                    var rSxExamPaper = await DbContext.FreeSql.Select<SxExamPaper>()
                        .Where(x => x.CaseId == id && x.MonthTaskId == item.Id && x.UserId == CommonConstants.DefaultUserId && x.IsDelete == CommonConstants.IsNotDelete)
                        .OrderByDescending(x => x.CreateTime).FirstAsync();
                    item.ExamId = rSxExamPaper == null ? 0 : rSxExamPaper.Id;
                }
            }
            return new ResponseContext<List<GetMonthTaskListDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
        }

        /// <summary>
        /// 查询指定月度任务 - PC端
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<GetMonthTaskListDto>> GetMonthTaskById(long id, long currentUserId, int userType)
        {
            return await Task.Run(() =>
            {
                var resData = DbContext.FreeSql.Select<SxTask, SxExamPaper>()
                    .InnerJoin((a, b) => a.Id == b.MonthTaskId && a.CaseId == b.CaseId && b.UserId == currentUserId && a.IsDelete == CommonConstants.IsNotDelete)
                    .Where((a, b) => a.Id == id && a.IsDelete == CommonConstants.IsNotDelete)
                    .WhereIf(userType != (int)UserTypeEnums.Professional, (a, b) => a.IsActive)
                    .OrderBy((a, b) => a.IssueDate).OrderByDescending((a, b) => a.CreateTime)
                    .ToList((a, b) => new GetMonthTaskListDto
                    {
                        Id = a.Id,
                        Name = a.Name,
                        CaseId = a.CaseId,
                        Description = a.Description,
                        Target = a.Target,
                        RequireAccountSet = a.RequireAccountSet,
                        Integral = a.Integral,
                        SuggestDuration = a.SuggestDuration,
                        IsCompleted = false,
                        IsPaid = !currentUserId.Equals(CommonConstants.DefaultUserId) && b.Id > 0,
                        ExamId = b.Id,
                        GradeId = 0
                    }).FirstOrDefault();
                if (resData == null)
                    return new ResponseContext<GetMonthTaskListDto> { Code = CommonConstants.ErrorCode, Msg = "找不到任务数据或试卷数据" };

                var rGradeData = DbContext.FreeSql.Select<SxExamUserGrade>().Where(x => x.ExamId == resData.ExamId && x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                    .OrderByDescending(x => x.CreateTime).First();
                resData.GradeId = rGradeData == null ? 0 : rGradeData.Id;
                resData.IsCompleted = rGradeData != null && rGradeData.Status == StudentExamStatus.End;

                return new ResponseContext<GetMonthTaskListDto> { Code = CommonConstants.SuccessCode, Data = resData };
            });
        }

        /// <summary>
        /// 查询指定月度任务 - APP商城
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<GetMonthTaskListDto>> GetMonthTaskByIdApp(long id, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var resData = DbContext.FreeSql.Select<SxTask>().Where(a => a.Id == id && a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy(a => a.IssueDate).OrderByDescending(a => a.CreateTime).ToList(a => new GetMonthTaskListDto
                    {
                        Id = a.Id,
                        Name = a.Name,
                        CaseId = a.CaseId,
                        Description = a.Description,
                        Target = a.Target,
                        RequireAccountSet = a.RequireAccountSet,
                        Integral = a.Integral,
                        SuggestDuration = a.SuggestDuration,
                        IsCompleted = false,
                        IsPaid = false,
                        ExamId = 0,
                        GradeId = 0
                    }).FirstOrDefault();
                if (resData == null)
                    return new ResponseContext<GetMonthTaskListDto> { Code = CommonConstants.ErrorCode, Msg = "找不到任务数据" };
                var rExamPaper = DbContext.FreeSql.Select<SxExamPaper>()
                    .Where(a => a.CaseId == resData.CaseId && a.MonthTaskId == resData.Id && a.UserId == currentUserId && a.IsDelete == CommonConstants.IsNotDelete).OrderBy(a => a.CreateTime).First();
                if (rExamPaper != null && rExamPaper.Id > 0)
                {
                    resData.ExamId = rExamPaper.Id;
                    if (!currentUserId.Equals(CommonConstants.DefaultUserId))
                    {
                        resData.IsPaid = true;
                        var rGradeData = DbContext.FreeSql.Select<SxExamUserGrade>().Where(x => x.ExamId == resData.ExamId && x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                            .OrderByDescending(x => x.CreateTime).First();
                        resData.GradeId = rGradeData == null ? 0 : rGradeData.Id;
                        resData.IsCompleted = rGradeData != null && rGradeData.Status == StudentExamStatus.End;
                    }
                }
                else
                {
                    var rSxExamPaper = DbContext.FreeSql.Select<SxExamPaper>()
                        .Where(x => x.CaseId == resData.CaseId && x.MonthTaskId == resData.Id && x.UserId == CommonConstants.DefaultUserId && x.IsDelete == CommonConstants.IsNotDelete)
                        .OrderByDescending(x => x.CreateTime).First();
                    resData.ExamId = rSxExamPaper == null ? 0 : rSxExamPaper.Id;
                }

                return new ResponseContext<GetMonthTaskListDto> { Code = CommonConstants.SuccessCode, Data = resData };
            });
        }

        #endregion

        #region 部门介绍
        /// <summary>
        /// 根据企业查询部门介绍
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<GetCaseRegulationListDto>>> GetCaseRegulationList(long id)
        {
            return await Task.Run(() =>
            {
                //制度模板分类
                var selectData = DbContext.FreeSql.Select<SxRegulation>().From<SxCaseRegulation>((a, b) =>
                    a.InnerJoin(aa => aa.Id == b.RegulationParentId && b.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b) => b.CaseId == id && a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy((a, b) => a.CreateTime).GroupBy((a, b) => new { a.Id, a.Name }).ToList(x => new GetCaseRegulationListDto
                    {
                        Id = x.Key.Id,
                        Name = x.Key.Name
                    });
                //公司制度
                var selectItemData = DbContext.FreeSql.Select<SxCaseRegulation>().Where(x => x.CaseId == id && x.IsDelete == CommonConstants.IsNotDelete).OrderBy(x => x.UseDate).ToList();
                foreach (var item in selectData)
                {
                    item.itemDetail = selectItemData.Where(x => x.RegulationParentId == item.Id).Select(x => new GetCaseRegulationDetailDto
                    {
                        Name = x.Name,
                        Content = x.Content
                    }).ToList();
                }
                return new ResponseContext<List<GetCaseRegulationListDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
            });
        }

        #endregion

        #region 生成试卷
        /// <summary>
        /// 生成试卷
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> GenerateTestPaper(long id, long currentUserId)
        {
            DateTime dtNow = DateTime.Now;
            var rEndTime = dtNow.AddYears(1);
            //查找任务
            var sxTask = DbContext.FreeSql.GetRepository<SxTask>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (sxTask == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "该月度任务不存在");
            var rSxExamPaper = DbContext.FreeSql.GetRepository<SxExamPaper>().Where(x => x.MonthTaskId == sxTask.Id && x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (rSxExamPaper != null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "已生成试卷，请刷新页面");
            //生成试卷
            var sxExamPaper = new SxExamPaper
            {
                Id = IdWorker.NextId(),
                Name = sxTask.Name,
                UserId = currentUserId,
                CaseId = sxTask.CaseId,
                MonthTaskId = sxTask.Id,
                ExamType = ExamType.PracticeTest,
                CompetitionType = CompetitionType.IndividualCompetition,
                CanShowAnswerBeforeEnd = true,
                TotalScore = 0,
                PassScore = 0,
                TotalQuestionCount = 0,
                BeginTime = dtNow,
                EndTime = rEndTime,
                Sort = 1,
                IsRelease = true,
                Status = ExamStatus.Wait,
                ExamSourceType = SxExamSourceType.Order,
                CreateBy = currentUserId,
                CreateTime = dtNow
            };
            //试卷题目
            var rTopicList = DbContext.FreeSql.Select<SxTopic>().Where(x => x.TaskId == sxTask.Id && x.ParentId == 0 && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            if (rTopicList.Count > 0)
            {
                var rTotalScore = rTopicList.Sum(x => x.Score);
                sxExamPaper.TotalScore = rTotalScore;
                sxExamPaper.PassScore = rTotalScore * (decimal)0.6;
                sxExamPaper.TotalQuestionCount = rTopicList.Count;
            }

            return await Task.Run(() =>
            {
                var state = false;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    state = DbContext.FreeSql.GetRepository<SxExamPaper>().InsertAsync(sxExamPaper) != null;
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "操作成功", Data = state };
            });
        }

        /// <summary>
        /// 购买后自动生成试卷 - 云实习
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AutoGenerateTestPaper(AutoGenerateTestPaperDto model, long currentUserId)
        {
            DateTime dtNow = DateTime.Now;
            var rEndTime = dtNow.AddYears(1);
            //验证
            if (model.Category == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "分类入参错误");
            if (model.TargetId == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "标的物Id参数错误");
            if (model.UserId == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "目标用户ID参数错误");

            //查找任务
            var rTaskData = new List<SxTask>();
            if (model.Category == 1)
                rTaskData = DbContext.FreeSql.GetRepository<SxTask>().Where(x => x.CaseId == model.TargetId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            else
                rTaskData = DbContext.FreeSql.GetRepository<SxTask>().Where(x => x.Id == model.TargetId && x.IsDelete == CommonConstants.IsNotDelete).ToList();
            if (rTaskData.Count == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到月度任务数据");
            //生成试卷
            var rExamPaperData = new List<SxExamPaper>();
            var rFreeTestPaperData = new List<YssxFreeTestPaper>();
            foreach (var item in rTaskData)
            {
                var rSxExamPaper = DbContext.FreeSql.GetRepository<SxExamPaper>().Where(x => x.MonthTaskId == item.Id && x.UserId == model.UserId && x.IsDelete == CommonConstants.IsNotDelete).First();
                if (rSxExamPaper != null)
                    continue;
                //生成试卷
                var sxExamPaper = new SxExamPaper
                {
                    Id = IdWorker.NextId(),
                    Name = item.Name,
                    UserId = model.UserId,
                    CaseId = item.CaseId,
                    MonthTaskId = item.Id,
                    ExamType = ExamType.PracticeTest,
                    CompetitionType = CompetitionType.IndividualCompetition,
                    CanShowAnswerBeforeEnd = true,
                    TotalScore = 0,
                    PassScore = 0,
                    TotalQuestionCount = 0,
                    BeginTime = dtNow,
                    EndTime = rEndTime,
                    Sort = 1,
                    IsRelease = true,
                    Status = ExamStatus.Wait,
                    ExamSourceType = SxExamSourceType.Order,
                    CreateBy = model.UserId,
                    CreateTime = dtNow
                };
                //试卷题目
                var rTopicList = DbContext.FreeSql.Select<SxTopic>().Where(x => x.TaskId == item.Id && x.ParentId == 0 && x.IsDelete == CommonConstants.IsNotDelete).ToList();
                if (rTopicList.Count > 0)
                {
                    var rTotalScore = rTopicList.Sum(x => x.Score);
                    sxExamPaper.TotalScore = rTotalScore;
                    sxExamPaper.PassScore = rTotalScore * (decimal)0.6;
                    sxExamPaper.TotalQuestionCount = rTopicList.Count;
                }
                rExamPaperData.Add(sxExamPaper);
                //添加操作记录
                if (currentUserId > 0)
                {
                    var rSxCase = DbContext.FreeSql.GetRepository<SxCase>().Where(x => x.Id == item.CaseId && x.IsDelete == CommonConstants.IsNotDelete).First();
                    if (rSxCase == null)
                        continue;

                    var rYssxFreeTestPaper = new YssxFreeTestPaper
                    {
                        Id = IdWorker.NextId(),
                        UserId = model.UserId,
                        TargetId = model.TargetId,
                        TargetName = rSxCase.Name + item.Name,
                        Source = 1,
                        CreateBy = currentUserId,
                        CreateTime = dtNow
                    };
                    rFreeTestPaperData.Add(rYssxFreeTestPaper);
                }
            }
            if (rExamPaperData.Count == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "已购买或已配置，不需要再手动配置" };

            return await Task.Run(() =>
            {
                var state = false;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    state = DbContext.FreeSql.Insert<SxExamPaper>().AppendData(rExamPaperData).ExecuteAffrows() > 0;
                    if (state && currentUserId > 0 && rFreeTestPaperData.Count > 0)
                        DbContext.SxFreeSql.Insert<YssxFreeTestPaper>().AppendData(rFreeTestPaperData).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "操作成功", Data = state };
            });
        }

        #endregion

        #region 实习列表
        /// <summary>
        /// 实习列表 - 顶岗实习
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<GetCloudInternshipListDto>> GetCloudInternshipList(GetCloudInternshipListRequest model, long currentUserId, int userType)
        {
            return await Task.Run(() =>
            {
                var select = DbContext.FreeSql.GetRepository<SxExamPaper>().Select.From<SxCase, SxTask, SxIndustry>((a, b, c, d) =>
                    a.InnerJoin(aa => aa.CaseId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                    .InnerJoin(aa => aa.MonthTaskId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
                    .InnerJoin(aa => b.IndustryId == d.Id && d.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d) => a.IsDelete == CommonConstants.IsNotDelete && a.UserId == currentUserId)
                    .WhereIf(!string.IsNullOrEmpty(model.Name), (a, b, c, d) => b.Name.Contains(model.Name))
                    .WhereIf(userType != (int)UserTypeEnums.Professional, (a, b, c, d) => b.IsActive && c.IsActive)
                    .OrderByDescending((a, b, c, d) => a.CreateTime);

                var totalCount = select.Count();
                var selectData = select.Page(model.PageIndex, model.PageSize).ToList((a, b, c, d) => new GetCloudInternshipListDto
                {
                    ExamId = a.Id,
                    CaseId = b.Id,
                    MonthTaskId = c.Id,
                    GradeId = 0,
                    Img = b.LogoUrl,
                    CaseName = b.Name,
                    MonthTaskName = c.Name,
                    ShortDesc = b.Description,
                    BeginTime = a.BeginTime.Value,
                    EndTime = a.EndTime,
                    IndustryId = d.Id,
                    IndustryName = d.Name
                });
                //明细数据
                foreach (var item in selectData)
                {
                    //作答记录
                    var rGradeEntity = DbContext.FreeSql.GetRepository<SxExamUserGrade>()
                        .Where(x => x.ExamId == item.ExamId && x.Status != StudentExamStatus.End && x.UserId == currentUserId && x.IsDelete == CommonConstants.IsNotDelete)
                        .OrderByDescending(x => x.CreateTime).First();
                    item.GradeId = rGradeEntity == null ? 0 : rGradeEntity.Id;
                }

                return new PageResponse<GetCloudInternshipListDto> { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        /// <summary>
        /// 查询通关记录
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<GetCloudInternshipRecordDto>> GetCloudInternshipRecord(GetCloudInternshipRecordRequest model, long currentUserId)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetRepository<SxExamUserGrade>().Select
                    .From<SxExamPaper, SxCase, SxTask>((a, b, c, d) =>
                        a.InnerJoin(aa => aa.ExamId == b.Id && b.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => b.CaseId == c.Id && c.IsDelete == CommonConstants.IsNotDelete)
                        .InnerJoin(aa => b.MonthTaskId == d.Id && d.IsDelete == CommonConstants.IsNotDelete))
                    .Where((a, b, c, d) => a.ExamId == model.ExamId && a.UserId == currentUserId && a.Status == StudentExamStatus.End && a.IsDelete == CommonConstants.IsNotDelete)
                    .OrderByDescending((a, b, c, d) => a.CreateTime)
                    .ToList((a, b, c, d) => new GetCloudInternshipRecordDto
                    {
                        GradeId = a.Id,
                        ExamId = b.Id,
                        CaseName = c.Name,
                        MonthTaskName = d.Name,
                        SubmitTime = a.SubmitTime.Value,
                        ExamPaperScore = a.ExamPaperScore,
                        Score = a.Score
                    });

                var totalCount = selectData.Count();
                selectData = selectData.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();
                return new PageResponse<GetCloudInternshipRecordDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
            });
        }

        #endregion

    }
}