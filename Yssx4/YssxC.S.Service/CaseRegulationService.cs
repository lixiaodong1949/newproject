﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    /// <summary>
    /// 案例规章制度业务服务
    /// </summary>
    public class CaseRegulationService : ICaseRegulationService
    {
        /// <summary>
        /// 根据Id删除案例规章制度
        /// </summary>
        /// <param name="id">案例规章制度Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteById(long id, UserTicket currentUser)
        {
            //删除
            await DbContext.FreeSql.GetRepository<SxCaseRegulation>().UpdateDiy.Set(c => new SxCaseRegulation
            {
                IsDelete = CommonConstants.IsDelete,
                UpdateTime = DateTime.Now
            }).Where(c => c.Id == id).ExecuteAffrowsAsync();

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }
        /// <summary>
        /// 根据id获取规章制度信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<CaseRegulationDto>> GetInfoById(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<SxCaseRegulation>()
               .Where(e => e.Id == id)
               .FirstAsync<CaseRegulationDto>();

            return new ResponseContext<CaseRegulationDto> { Code = CommonConstants.SuccessCode, Data = entity };
        }
        /// <summary>
        /// 根据案例id获取案例所有的规章制度信息
        /// </summary>
        /// <param name="caseId">案例id</param>
        /// <returns></returns>
        public async Task<ResponseContext<List<CaseRegulationListDto>>> GetListByCaseId(long caseId)
        {
            var entity = await DbContext.FreeSql.GetRepository<SxCaseRegulation>()
                   .Where(e => e.CaseId == caseId && e.IsDelete == CommonConstants.IsNotDelete)
                   .OrderBy(e => e.CreateTime)
                   .ToListAsync<CaseRegulationListDto>();

            return new ResponseContext<List<CaseRegulationListDto>> { Code = CommonConstants.SuccessCode, Data = entity };
        }

        public async Task<ResponseContext<bool>> Save(CaseRegulationDto model, UserTicket currentUser)
        {
            var entity = model.MapTo<SxCaseRegulation>();
            var repo_r = DbContext.FreeSql.GetRepository<SxRegulation>();

            var r = await repo_r.Where(e => e.Id == model.RegulationId).FirstAsync();

            if (r == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = false, Msg = "模板不存在。" };
            }

            entity.RegulationParentId = r.ParentId;

            if (entity.Id <= 0)//新增
            {
                entity.Id = IdWorker.NextId();//获取唯一Id
                entity.CreateBy = currentUser.Id;

                await DbContext.FreeSql.GetRepository<SxCaseRegulation>().InsertAsync(entity);
            }
            else//修改
            {
                entity.UpdateTime = DateTime.Now;
                entity.UpdateBy = currentUser.Id;

                await DbContext.FreeSql.GetRepository<SxCaseRegulation>().UpdateAsync(entity);
            }

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 保存规章制度 应用规章制度模板
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveCaseUseRegulation(CaseUseRegulationsDto model, UserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);

            var hascase = await DbContext.FreeSql.GetRepository<SxCase>(a => a.IsDelete == CommonConstants.IsNotDelete)
                .Where(a => a.Id == model.CaseId).AnyAsync();

            if (!hascase)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"案例id不存在.", false);

            //查询制度模板信息
            var regulations = await DbContext.FreeSql.GetRepository<SxRegulation>(a => a.IsDelete == CommonConstants.IsNotDelete)
                .Where(a => model.RegulationIds.Contains(a.Id))
                .ToListAsync();

            if (regulations == null || regulations.Count < model.RegulationIds.Count)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"制度模板不存在.", false);

            List<SxCaseRegulation> caseRegulations = new List<SxCaseRegulation>();

            foreach (var regulation in regulations)
            {
                var r = CreateCaseRegulation(regulation, model.CaseId, currentUser);
                if (r != null)
                    caseRegulations.Add(r);
            }

            if (caseRegulations.Count > 0) //保存到数据库中           
                await DbContext.FreeSql.GetRepository<SxCaseRegulation>().InsertAsync(caseRegulations);

            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        internal static SxCaseRegulation CreateCaseRegulation(SxRegulation regulation, long casseId, UserTicket currentUser)
        {
            if (regulation == null)
                return null;

            return new SxCaseRegulation
            {
                Id = IdWorker.NextId(),
                CaseId = casseId,
                CreateBy = currentUser.Id,
                Content = regulation.Content,
                RegulationId = regulation.Id,
                Name = regulation.Name,
                RegulationParentId = regulation.ParentId,
            };
        }
    }
}
