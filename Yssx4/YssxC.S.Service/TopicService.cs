﻿using AspectCore.DynamicProxy;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.Repository.Extensions;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    /// <summary>
    /// 题目业务服务
    /// </summary>
    public class TopicService : ITopicService
    {
        #region 单选题、多选题、判断题 新增 修改
        /// <summary>
        /// 添加简单类型题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveSimpleQuestion(SimpleQuestionRequest model, UserTicket currentUser)
        {
            long opreationId = currentUser.Id;//操作人ID
            DateTime dtNow = DateTime.Now;
            SxTopic oldTopic = null;
            #region 校验
            if (model.Id > 0)
            {
                oldTopic = await DbContext.FreeSql.GetRepository<SxTopic>()
                .Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync();

                if (oldTopic == null)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                }
            }

            #endregion
            var topic = model.MapTo<SxTopic>();
            topic.IsGzip = CommonConstants.IsGzip;
            topic.CreateBy = opreationId;
            topic.CreateTime = dtNow;
            if (model.Id == 0) { topic.Id = IdWorker.NextId(); }

            #region 处理选项排序
            //选项信息
            List<SxAnswerOption> addAnswerOptions = null;
            List<SxAnswerOption> updateAnswerOptions = null;
            var tuple = CreateAnswerOption(model.Options, topic);
            if (tuple != null)
            {
                addAnswerOptions = tuple.Item1;
                updateAnswerOptions = tuple.Item2;
            }

            #endregion

            if (model.Id == 0)//新增
            {
                #region 添加题目，选项，题目类别关联信息
                //添加到数据库
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //添加题目
                        await uow.GetRepository<SxTopic>().InsertAsync(topic);
                        //题目选项和答案
                        await uow.GetRepository<SxAnswerOption>().InsertAsync(addAnswerOptions);

                        // 修改任务题目总数
                        UpdateTaskTopicCount(uow, topic.TaskId, 1, currentUser);
                        // 修改案例题目总分
                        UpdateCaseTopicTotalScore(uow, topic.CaseId, topic.Score, currentUser);

                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
                #endregion
            }
            else//修改
            {
                topic.UpdateTime = dtNow;
                topic.UpdateBy = opreationId;

                #region 删除的选项--添加ID

                //取删除的选项
                List<long> existOptionId = null;
                if (updateAnswerOptions != null && updateAnswerOptions.Count > 0)
                    existOptionId = updateAnswerOptions.Select(a => a.Id).ToList();

                var delOption = await DbContext.FreeSql.GetRepository<SxAnswerOption>(m => m.IsDelete == CommonConstants.IsNotDelete)
                .Where(m => m.TopicId == topic.Id && m.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(existOptionId != null && existOptionId.Count > 0, m => !existOptionId.Contains(m.Id))
                .ToListAsync();

                if (delOption.Count > 0)
                {
                    delOption.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                }
                #endregion

                #region 更新题目,选项(更新，添加)。题目类别关联信息不允许修改
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        var repo_t = DbContext.FreeSql.GetRepository<SxTopic>();
                        var repo_ao = DbContext.FreeSql.GetRepository<SxAnswerOption>();

                        //更新题目
                        await repo_t.UpdateDiy.SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.CaseId, a.TaskId }).ExecuteAffrowsAsync();

                        #region 选项(更新，添加)
                        //添加
                        if (addAnswerOptions != null && addAnswerOptions.Count > 0)
                        {
                            await repo_ao.InsertAsync(addAnswerOptions);
                        }
                        //删除的选项
                        if (delOption.Count > 0)
                        {
                            await repo_ao.UpdateDiy.SetSource(delOption).UpdateColumns(a => new { a.UpdateBy, a.UpdateTime, a.IsDelete }).ExecuteAffrowsAsync();
                        }
                        //更新选项
                        if (updateAnswerOptions != null && updateAnswerOptions.Count > 0)
                        {
                            await repo_ao.UpdateDiy.SetSource(updateAnswerOptions).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();
                        }
                        #endregion

                        // 修改任务题目总数
                        UpdateCaseTopicTotalScore(uow, topic.CaseId, topic.Score - oldTopic.Score, currentUser);

                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
                #endregion

            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = topic.Id.ToString() };

        }
        #endregion

        #region 表格题、案例题、申报表题 新增 修改

        /// <summary>
        ///  添加复杂类型题目（表格题、案例题、申报表题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveComplexQuestion(ComplexQuestionRequest model, UserTicket currentUser)
        {
            long opreationId = currentUser.Id;//操作人ID
            long parentDeclarationId = 0;//申报表父Id
            DateTime dtNow = DateTime.Now;

            var repo_t2 = DbContext.FreeSql.GetRepository<SxTopic>(e => e.IsDelete == CommonConstants.IsNotDelete);
            #region 参数校验
            if (model.QuestionType == QuestionType.DeclarationTpoic)//申报表题，同一个申报表只能拥有一个题目。
            {
                var repo_dc = DbContext.FreeSql.GetRepository<SxDeclarationChild>(c => c.IsDelete == CommonConstants.IsNotDelete);
                var sxDeclarationChild = await repo_dc.Where(e => e.Id == model.DeclarationId).FirstAsync();
                if (sxDeclarationChild == null)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.BadRequest, Data = false, Msg = "该申报表不存在" };
                }
                parentDeclarationId = sxDeclarationChild.ParentId;
                var dTopicId = await repo_t2.Where(e => e.DeclarationId == model.DeclarationId)
                    .WhereIf(model.Id > 0, e => e.Id != model.Id)
                    .FirstAsync(e => e.Id);
                if (dTopicId > 0)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.BadRequest, Data = false, Msg = "该申报表已经添加题目：" + dTopicId };
                }
            }
            #endregion

            var topic = model.MapTo<SxTopic>();
            topic.ParentDeclarationId = parentDeclarationId;
            topic.IsGzip = CommonConstants.IsGzip;
            if (model.Id <= 0)//新增
            {
                topic.Id = IdWorker.NextId();
                topic.CreateBy = opreationId;
                topic.CreateTime = dtNow;
            }
            else
            {
                topic.UpdateTime = DateTime.Now;
                topic.UpdateBy = opreationId;
            }

            #region 文件排序
            //添加附件
            var files = new List<SxTopicFile>();
            topic.TopicFileIds = CreateTopicFile(model.QuestionFile, topic.CaseId, topic.Id, currentUser.Id, files, topic.TaskId, topic.ParentId);//生成题目附件数据
            #endregion

            if (model.Id == 0)//新增
            {
                //添加到数据库
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //添加题目
                        await uow.GetRepository<SxTopic>().InsertAsync(topic);
                        //添加附件
                        await uow.GetRepository<SxTopicFile>().InsertAsync(files);

                        // 修改任务题目总数
                        UpdateTaskTopicCount(uow, topic.TaskId, 1, currentUser);
                        // 修改任务题目总数
                        UpdateCaseTopicTotalScore(uow, topic.CaseId, topic.Score, currentUser);

                        //申报表题,更新申报表，父表子表中的分数
                        if (topic.QuestionType == QuestionType.DeclarationTpoic)
                        {
                            UpdateDeclarationTopicTotalScore(uow, topic.DeclarationId, topic.ParentDeclarationId, topic.Score, currentUser);
                        }

                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
            }
            else//修改
            {
                var oldTopic = await DbContext.FreeSql.GetRepository<SxTopic>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (oldTopic == null)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                }

                #region 取删除的文件
                var delTopicFile = new List<SxTopicFile>();
                if (!String.IsNullOrEmpty(oldTopic.TopicFileIds))
                {
                    var rOldfileIds = oldTopic.TopicFileIds.SplitToArray<long>(',');
                    var rNewFileIds = files.Select(m => m.Id);
                    var rDelFileIds = rOldfileIds.Where(m => !rNewFileIds.Contains(m));
                    if (rDelFileIds.Count() > 0)
                    {
                        delTopicFile = await DbContext.FreeSql.Select<SxTopicFile>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && rDelFileIds.Contains(m.Id)).ToListAsync();
                        if (delTopicFile.Count > 0)
                        {
                            delTopicFile.ForEach(a =>
                            {
                                a.IsDelete = CommonConstants.IsDelete;
                                a.UpdateBy = opreationId;
                                a.UpdateTime = dtNow;
                            });
                        }
                    }
                }
                #endregion

                #region 更新题目，更新文件顺序字段，添加文件顺序
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        var repo_tf = uow.GetRepository<SxTopicFile>();
                        var repo_t = uow.GetRepository<SxTopic>();
                        if (delTopicFile.Count > 0)
                        {
                            await repo_tf.UpdateDiy.SetSource(delTopicFile).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                        }
                        //添加附件
                        await repo_tf.InsertAsync(files);

                        topic.TopicFileIds = string.Join(",", files.Select(m => m.Id));
                        //更新题目
                        await repo_t.UpdateDiy.SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.CaseId, a.TaskId }).ExecuteAffrowsAsync();

                        // 修改任务题目总数
                        UpdateCaseTopicTotalScore(uow, topic.CaseId, (topic.Score - oldTopic.Score), currentUser);

                        //申报表题,更新申报表，父表子表中的分数
                        if (topic.QuestionType == QuestionType.DeclarationTpoic)
                        {
                            UpdateDeclarationTopicTotalScore(uow, topic.DeclarationId, topic.ParentDeclarationId, (topic.Score - oldTopic.Score), currentUser);
                        }
                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
                #endregion
            }

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = topic.Id.ToString() };
        }

        #endregion

        #region 财务报表题(拆分为：财务报表题、财务报表纳税申报题) 新增、修改

        /// <summary>
        ///  财务报表题 新增、修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveFinancialStatements(FinancialStatementsRequest model, UserTicket currentUser)
        {
            var repo_t2 = DbContext.FreeSql.GetRepository<SxTopic>(e => e.IsDelete == CommonConstants.IsNotDelete);

            List<SxTopicFile> files = new List<SxTopicFile>();
            var tuple = CreateFinancialStatements(model, currentUser, files);
            SxTopic topic = tuple.Item1;
            SxTopic fSDTopic = tuple.Item2;

            if (model.Id == 0)//新增
            {
                //添加到数据库
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //添加题目
                        await uow.GetRepository<SxTopic>().InsertAsync(topic);
                        await uow.GetRepository<SxTopic>().InsertAsync(fSDTopic);
                        //添加附件
                        await uow.GetRepository<SxTopicFile>().InsertAsync(files);

                        // 修改任务题目总数
                        UpdateTaskTopicCount(uow, topic.TaskId, 1, currentUser);
                        // 修改案例总分
                        UpdateCaseTopicTotalScore(uow, topic.CaseId, topic.Score + fSDTopic.Score, currentUser);

                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
            }
            else//修改
            {
                var oldTopic = await DbContext.FreeSql.GetRepository<SxTopic>()
                    .Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete)
                    .FirstAsync();

                var oldfSDTopic = await DbContext.FreeSql.GetRepository<SxTopic>(m => m.IsDelete == CommonConstants.IsNotDelete)
                    .Where(m => m.GroupId == oldTopic.GroupId && m.QuestionType == QuestionType.FinancialStatementsDeclaration)
                    .FirstAsync();
                if (oldTopic == null)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                }
                if (oldfSDTopic == null)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                }
                fSDTopic.Id = oldfSDTopic.Id;

                #region 取删除的文件
                var delTopicFile = new List<SxTopicFile>();
                if (!String.IsNullOrEmpty(oldTopic.TopicFileIds))
                {
                    var rOldfileIds = oldTopic.TopicFileIds.SplitToArray<long>(',');
                    var rNewFileIds = files.Select(m => m.Id);
                    var rDelFileIds = rOldfileIds.Where(m => !rNewFileIds.Contains(m));
                    if (rDelFileIds.Count() > 0)
                    {
                        delTopicFile = await DbContext.FreeSql.Select<SxTopicFile>().Where(m => m.IsDelete == CommonConstants.IsNotDelete && rDelFileIds.Contains(m.Id)).ToListAsync();
                        if (delTopicFile.Count > 0)
                        {
                            delTopicFile.ForEach(a =>
                            {
                                a.IsDelete = CommonConstants.IsDelete;
                                a.UpdateBy = currentUser.Id;
                                a.UpdateTime = DateTime.Now;
                            });
                        }
                    }
                }
                #endregion

                #region 更新题目，更新文件顺序字段，添加文件顺序
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        var repo_tf = uow.GetRepository<SxTopicFile>();
                        var repo_t = uow.GetRepository<SxTopic>();
                        if (delTopicFile.Count > 0)
                        {
                            await repo_tf.UpdateDiy.SetSource(delTopicFile).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                        }
                        //添加附件
                        await repo_tf.InsertAsync(files);

                        topic.TopicFileIds = string.Join(",", files.Select(m => m.Id));
                        //更新题目
                        await repo_t.UpdateDiy.SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.GroupId, a.CaseId, a.TaskId }).ExecuteAffrowsAsync();
                        await repo_t.UpdateDiy.SetSource(fSDTopic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.GroupId, a.CaseId, a.TaskId }).ExecuteAffrowsAsync();

                        // 修改案例总分数
                        UpdateCaseTopicTotalScore(uow, topic.CaseId, (topic.Score - oldTopic.Score) + (fSDTopic.Score - oldfSDTopic.Score), currentUser);

                        uow.Commit();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        throw;
                    }
                }
                #endregion
            }

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = topic.Id.ToString() };
        }

        private static Tuple<SxTopic, SxTopic> CreateFinancialStatements(FinancialStatementsRequest model, UserTicket currentUser, List<SxTopicFile> files)
        {
            var opreationId = currentUser.Id;
            var dtNow = DateTime.Now;

            var topic = model.MapTo<SxTopic>();
            topic.IsGzip = CommonConstants.IsGzip;
            topic.PositionId = model.PositionId;

            //财务报表纳税申报题
            var fSDTopic = model.MapTo<SxTopic>();
            fSDTopic.QuestionType = QuestionType.FinancialStatementsDeclaration;
            fSDTopic.PositionId = model.DeclarationPositionId;
            fSDTopic.PointPositionId = model.DeclarationPointPositionId;
            fSDTopic.SkillId = model.DeclarationSkillId;
            fSDTopic.IsGzip = CommonConstants.IsGzip;
            fSDTopic.Score = model.DeclarationScore;// 纳税申报报表分数 财务表报题

            if (model.Id <= 0)//新增
            {
                topic.Id = IdWorker.NextId();
                topic.CreateBy = opreationId;
                topic.CreateTime = dtNow;

                fSDTopic.Id = IdWorker.NextId();
                fSDTopic.CreateBy = opreationId;
                fSDTopic.CreateTime = dtNow;
                fSDTopic.GroupId = topic.GroupId = IdWorker.NextId();
            }
            else
            {
                topic.UpdateTime = dtNow;
                topic.UpdateBy = opreationId;

                fSDTopic.UpdateTime = dtNow;
                fSDTopic.UpdateBy = opreationId;
            }

            #region 文件排序
            //添加附件
            topic.TopicFileIds = CreateTopicFile(model.QuestionFile, topic.CaseId, topic.Id, currentUser.Id, files, topic.TaskId, topic.ParentId);//生成题目附件数据
            fSDTopic.TopicFileIds = topic.TopicFileIds;

            #endregion

            return new Tuple<SxTopic, SxTopic>(topic, fSDTopic);
        }

        #endregion

        #region 综合题(财务分析题) 添加 修改

        /// <summary>
        /// 添加多题型题目（综合题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveMainSubQuestion(MainSubQuestionRequest model, UserTicket currentUser)
        {
            return await Task.Run(() =>
            {
                long opreationId = currentUser.Id;//操作人ID
                DateTime dtNow = DateTime.Now;

                #region 校验
                //TODO 校验
                var rSxCase = DbContext.FreeSql.GetRepository<SxCase>().Where(m => m.Id == model.CaseId && m.IsDelete == CommonConstants.IsNotDelete).First();
                if (rSxCase == null)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到案例信息" };
                }
                var oldTopic = new SxTopic();
                if (model.Id > 0)
                {
                    var rSxTopic = DbContext.FreeSql.GetRepository<SxTopic>().Where(m => m.Id == model.Id && m.IsDelete == CommonConstants.IsNotDelete);
                    if (!rSxTopic.Any())
                    {
                        return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到题目信息" };
                    }
                    oldTopic = rSxTopic.First();
                }
                if (model.SubQuestion.Count == 0)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，子题目为0" };
                //验证题目类型是否有效
                var rIsInvalidQuestionType = model.SubQuestion
                    .Where(m => m.QuestionType == QuestionType.SingleChoice
                            || m.QuestionType == QuestionType.MultiChoice
                            || m.QuestionType == QuestionType.Judge
                            || m.QuestionType == QuestionType.FillGrid
                            || m.QuestionType == QuestionType.FillBlank
                            || m.QuestionType == QuestionType.FillGraphGrid).Count();
                if (rIsInvalidQuestionType != model.SubQuestion.Count)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，子题目类型有误" };
                }
                #endregion


                //支持选择题-选项
                var mainTopic = model.MapTo<SxTopic>();
                mainTopic.IsGzip = CommonConstants.IsGzip;

                if (model.Id == 0)
                {
                    mainTopic.CreateBy = opreationId;
                    mainTopic.CreateTime = dtNow;
                    mainTopic.Id = IdWorker.NextId();
                    mainTopic.GroupId = IdWorker.NextId();
                }
                else
                {
                    mainTopic.CaseId = oldTopic.CaseId;
                    mainTopic.TaskId = oldTopic.TaskId;
                    mainTopic.GroupId = oldTopic.GroupId;
                    mainTopic.UpdateTime = dtNow;
                    mainTopic.UpdateBy = opreationId;
                }

                #region 文件排序
                //添加附件--用于排序需要
                var files = new List<SxTopicFile>();
                if (model.QuestionFile != null)
                {
                    mainTopic.TopicFileIds = CreateTopicFile(model.QuestionFile, mainTopic.CaseId, mainTopic.Id, currentUser.Id, files, mainTopic.TaskId, mainTopic.ParentId);//生成题目附件数据
                }

                #endregion

                if (model.Id == 0)//新增题目
                {
                    //子题目 排序 添加父节点ID
                    #region 构建子题数据
                    List<SxTopic> subQuestion = null;
                    List<SxAnswerOption> addAnswerOptions = null;
                    var tuple = CreateSubQuestion(model.SubQuestion, mainTopic);
                    if (tuple != null)
                    {
                        subQuestion = tuple.Item1;
                        addAnswerOptions = tuple.Item2;
                    }
                    #endregion
                    #region 添加到数据库中
                    //添加到数据库中
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        try
                        {
                            var repo_t = uow.GetRepository<SxTopic>();
                            var repo_tf = uow.GetRepository<SxTopicFile>();
                            var repo_ao = uow.GetRepository<SxAnswerOption>();

                            //添加题目
                            repo_t.InsertAsync(mainTopic);

                            if (files != null && files.Count > 0)  //添加附件                              
                                repo_tf.InsertAsync(files);

                            //添加子题目
                            repo_t.InsertAsync(subQuestion);

                            //添加子题目-选项
                            if (addAnswerOptions != null && addAnswerOptions.Any())
                                repo_ao.InsertAsync(addAnswerOptions);

                            // 修改任务题目总数
                            UpdateTaskTopicCount(uow, mainTopic.TaskId, 1, currentUser);
                            // 修改任务题目总数
                            UpdateCaseTopicTotalScore(uow, mainTopic.CaseId, mainTopic.Score, currentUser);

                            uow.Commit();
                        }
                        catch (Exception ex)
                        {
                            uow.Rollback();
                            throw;
                        }
                    }
                    #endregion
                }
                else//修改题目
                {
                    //子题目 排序 添加父节点ID
                    #region 处理数据
                    var certificateDataRecords = new List<SxCertificateDataRecord>();
                    var addanswerOptions = new List<SxAnswerOption>();//题目选项和答案
                    var delanswerOptions = new List<SxAnswerOption>();//题目选项和答案
                    List<SxAnswerOption> updateanswerOptions = null;//题目选项和答案                                       
                    List<SxTopic> addSubTopic = null;//需要新添加的子题
                    List<SxTopic> updateSubTopic = null; //需要修改的子题
                    List<SxTopic> rDelSubQuestion = null; //需要删除的子题


                    var addSubQuestion = model.SubQuestion//需要新添加的子题
                      .Where(a => a.Id == 0).ToList();

                    var tuple1 = CreateSubQuestion(addSubQuestion, mainTopic);
                    if (tuple1 != null)
                    {
                        addSubTopic = tuple1.Item1;
                        if (tuple1.Item2 != null)
                            addanswerOptions.AddRange(tuple1.Item2);
                    }

                    var updateSubQuestion = model.SubQuestion //需要修改的子题
                      .Where(a => a.Id > 0).ToList();
                    var tuple2 = CreateSubQuestion(updateSubQuestion, mainTopic);
                    if (tuple2 != null)
                    {
                        updateSubTopic = tuple2.Item1;
                        if (tuple2.Item2 != null)
                            addanswerOptions.AddRange(tuple2.Item2);
                        updateanswerOptions = tuple2.Item3;
                    }

                    #endregion

                    #region 取待删除数据
                    //取子题目
                    List<long> existIds = null;
                    if (updateSubQuestion != null && updateSubQuestion.Count > 0)
                    {
                        existIds = updateSubQuestion.Select(a => a.Id).ToList();
                    }
                    rDelSubQuestion = DbContext.FreeSql.GetRepository<SxTopic>(e => e.IsDelete == CommonConstants.IsNotDelete)
                    .Where(m => m.ParentId == mainTopic.Id)
                    .WhereIf(existIds != null && existIds.Count > 0, m => !existIds.Contains(m.Id))
                    .ToList();
                    rDelSubQuestion.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                    //取子题目-选项
                    List<long> topicIds = new List<long>();
                    List<long> optionIds = new List<long>();
                    if (updateanswerOptions != null && updateanswerOptions.Count > 0)
                    {
                        var utopicIds = updateanswerOptions.Select(a => a.TopicId).Distinct().ToList();
                        if (utopicIds != null && utopicIds.Count > 0)
                            topicIds.AddRange(utopicIds);

                        optionIds = updateanswerOptions.Select(a => a.Id).ToList();

                    }
                    if (rDelSubQuestion != null && rDelSubQuestion.Count > 0)
                    {
                        var d = rDelSubQuestion.Select(e => e.Id).ToList();
                        if (d != null && d.Count > 0)
                            topicIds.AddRange(d);
                    }
                    if (topicIds != null && topicIds.Count > 0)
                    {
                        delanswerOptions = DbContext.FreeSql.GetRepository<SxAnswerOption>(e => e.IsDelete == CommonConstants.IsNotDelete)
                        .Where(a => topicIds.Contains(a.TopicId))
                        .WhereIf(optionIds != null && optionIds.Count > 0, a => !optionIds.Contains(a.Id))
                        .ToList();
                        delanswerOptions.ForEach(a =>
                        {
                            a.IsDelete = CommonConstants.IsDelete;
                            a.UpdateBy = opreationId;
                            a.UpdateTime = dtNow;
                        });
                    }

                    #endregion

                    #region 更新题目，更新文件顺序字段，添加文件顺序
                    using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                    {
                        try
                        {
                            var repo_t = uow.GetRepository<SxTopic>();
                            var repo_tf = uow.GetRepository<SxTopicFile>();
                            var repo_ao = uow.GetRepository<SxAnswerOption>();

                            #region 附件 处理 删除旧附件 再添加新的附件
                            if (!string.IsNullOrEmpty(oldTopic.TopicFileIds))
                            {
                                var fileIds = oldTopic.TopicFileIds.SplitToArray<long>(',');
                                //repo_tf.DeleteAsync(a => fileIds.Contains(a.Id));
                                repo_tf.UpdateDiy.Set(c => new SxTopicFile
                                {
                                    IsDelete = CommonConstants.IsDelete,
                                    UpdateTime = DateTime.Now,
                                    UpdateBy = currentUser.Id,
                                }).Where(a => fileIds.Contains(a.Id)).ExecuteAffrows();
                            }
                            //添加附件
                            repo_tf.InsertAsync(files);
                            #endregion

                            //更新主题目
                            repo_t.UpdateDiy.SetSource(mainTopic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.QuestionContentType, a.GroupId, a.CaseId, a.TaskId }).ExecuteAffrows();

                            #region 子题目操作 删除不存在的题目  添加新题目 更新已存在题目
                            //删除不存在的题目
                            if (rDelSubQuestion != null && rDelSubQuestion.Any())
                            {
                                repo_t.UpdateDiy.SetSource(rDelSubQuestion).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                            }
                            //添加新题目
                            if (addSubTopic != null && addSubTopic.Any())
                            {
                                repo_t.InsertAsync(addSubTopic);
                            }
                            //更新已存在题目
                            if (updateSubTopic != null && updateSubTopic.Any())
                            {
                                //更新题目内容
                                repo_t.UpdateDiy.SetSource(updateSubTopic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.QuestionContentType, a.GroupId }).ExecuteAffrows();
                            }
                            #endregion

                            #region 子题目-选择题-选项 选项(更新，添加)
                            //删除选项
                            if (delanswerOptions != null && delanswerOptions.Any())
                            {
                                repo_ao.UpdateDiy.SetSource(delanswerOptions).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                            }
                            //添加
                            if (addanswerOptions != null && addanswerOptions.Any())
                            {
                                repo_ao.InsertAsync(addanswerOptions);
                            }
                            //更新选项
                            if (updateanswerOptions != null && updateanswerOptions.Any())
                            {
                                repo_ao.UpdateDiy.SetSource(updateanswerOptions).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.CaseId, a.TaskId }).ExecuteAffrows();
                            }
                            #endregion

                            // 修改任务题目总数
                            UpdateCaseTopicTotalScore(uow, mainTopic.CaseId, mainTopic.Score - oldTopic.Score, currentUser);

                            uow.Commit();
                        }
                        catch (Exception ex)
                        {
                            uow.Rollback();
                            throw;
                        }
                    }
                    #endregion
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = mainTopic.Id.ToString() };
            });
        }

        private static Tuple<List<SxTopic>, List<SxAnswerOption>, List<SxAnswerOption>> CreateSubQuestion(List<SubQuestion> subQuestions, SxTopic mainTopic)
        {
            if (subQuestions == null && mainTopic == null)
                return null;

            var subQuestion = new List<SxTopic>();
            //选项信息
            List<SxAnswerOption> addAnswerOptions = new List<SxAnswerOption>();
            List<SxAnswerOption> updateAnswerOptions = new List<SxAnswerOption>();
            subQuestions.ForEach(a =>
            {
                SxTopic item = CreateSubQuestion(a, mainTopic);
                subQuestion.Add(item);

                #region 题目答案
                if (a.Options != null && a.Options.Any())
                {
                    var tuple = CreateAnswerOption(a.Options, item);
                    if (tuple != null)
                    {
                        if (tuple.Item1 != null && tuple.Item1.Count > 0)
                            addAnswerOptions.AddRange(tuple.Item1);

                        if (tuple.Item2 != null && tuple.Item2.Count > 0)
                            updateAnswerOptions = tuple.Item2;
                    }
                }
                #endregion
            });
            return new Tuple<List<SxTopic>, List<SxAnswerOption>, List<SxAnswerOption>>(subQuestion, addAnswerOptions, updateAnswerOptions);
        }

        /// <summary>
        /// 构建题目的选项+答案对象，新增和修改
        /// </summary>
        /// <param name="choiceOption"></param>
        /// <param name="topic"></param>
        /// <param name="options"></param>
        private static Tuple<List<SxAnswerOption>, List<SxAnswerOption>> CreateAnswerOption(List<ChoiceOption> choiceOption, SxTopic topic)
        {

            if (choiceOption == null)
                return null;
            var addoptions = new List<SxAnswerOption>();
            var updateoptions = new List<SxAnswerOption>();
            var loop = 0;
            choiceOption.ForEach(a =>
            {
                var option = a.MapTo<SxAnswerOption>();
                option.AnswerKeysContent = topic.AnswerValue;
                option.AnswerOption = a.Text?.Trim();
                option.TopicId = topic.Id;
                option.TopicParentId = topic.ParentId;
                option.CaseId = topic.CaseId;
                option.TaskId = topic.TaskId;
                option.AnswerKeysContent = "";
                option.AnswerFileUrl = "";

                option.Sort = ++loop;

                if (a.Id <= 0)//新增
                {
                    option.Id = IdWorker.NextId();
                    option.CreateBy = topic.CreateBy;
                    addoptions.Add(option);
                }
                else
                {
                    option.UpdateBy = topic.UpdateBy;
                    option.UpdateTime = DateTime.Now;

                    updateoptions.Add(option);
                }
            });

            return new Tuple<List<SxAnswerOption>, List<SxAnswerOption>>(addoptions, updateoptions);
        }
        /// <summary>
        /// 构建综合题的子题对象，新增和修改
        /// </summary>
        /// <param name="subQuestion"></param>
        /// <param name="mainTopic"></param>
        /// <returns></returns>
        private static SxTopic CreateSubQuestion(SubQuestion subQuestion, SxTopic mainTopic)
        {
            var item = subQuestion.MapTo<SxTopic>();

            item.ParentId = mainTopic.Id;
            item.GroupId = mainTopic.GroupId;
            item.TaskId = mainTopic.TaskId;
            item.CaseId = mainTopic.CaseId;
            item.IsGzip = CommonConstants.IsGzip;
            item.PositionId = mainTopic.PositionId;
            item.BusinessDate = mainTopic.BusinessDate;
            item.Title = mainTopic.Title;

            if (subQuestion.Id <= 0)//新增
            {
                item.CreateBy = mainTopic.CreateBy;
                item.Id = IdWorker.NextId();
            }
            else
            {
                item.UpdateBy = mainTopic.UpdateBy;
                item.UpdateTime = mainTopic.UpdateTime;
            }
            return item;
        }

        #endregion

        #region 综合分录题 添加 修改

        /// <summary>
        /// 综合题分录题 添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddCertificateMainSubTopic(CertificateMainSubTopic model, UserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);

            List<SxTopicFile> topicFiles = new List<SxTopicFile>();

            //主题目、题目关联
            var maintuple = CreateMainTopic(model, currentUser);
            SxTopic mainTopic = maintuple.Item1;
            List<SxTopicRelation> topicRelations = maintuple.Item2;

            //票据题目
            SxTopic teticketTopic = CreateTeticketTopic(model.TeticketTopic, mainTopic, currentUser, topicFiles);
            mainTopic.PushDate = teticketTopic.PushDate;
            mainTopic.IsTrap = teticketTopic.IsTrap;

            List<SxTopicPayAccountInfo> topicPayAccounts = new List<SxTopicPayAccountInfo>();
            //收款题目
            var receiptTopics = CreateReceiptTopic(model.ReceiptTopics, mainTopic, currentUser, topicFiles, topicPayAccounts);

            //付款题目
            var payTopics = CreatePayTopic(model.PayTopics, mainTopic, currentUser, topicFiles, topicPayAccounts);

            #region 记账凭证题目
            //记账凭证题目（分录题）
            var tuple = CreateCertificateTopic(model.CertificateTopic, mainTopic, topicFiles);

            SxTopic certificateTopic = tuple.Item1;
            SxCertificateTopic yssxCore = tuple.Item2;
            List<SxCertificateDataRecord> yssxCertificateData = tuple.Item3;
            List<SxCertificateAssistAccounting> assistAccountings = tuple.Item4;//科目辅助核算信息

            //附件，给主题目和记账凭证题目附件字段赋值
            if (topicFiles.Count > 0)
            {
                var cfileIds = topicFiles
                    .Where(e => e.Type != TopicFileType.Fault)//去错误票据
                    .Select(e => e.Id)
                    .ToArray();

                certificateTopic.TopicFileIds = cfileIds.Join(",");
                yssxCore.InvoicesNum = cfileIds.Length;

                mainTopic.TopicFileIds = topicFiles.Select(e => e.Id).ToArray().Join(",");//多有文件Id保存在主题目中
            }
            #endregion

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo_t = uow.GetGuidRepository<SxTopic>();
                    var repo_tpai = uow.GetGuidRepository<SxTopicPayAccountInfo>();

                    await repo_t.InsertAsync(mainTopic);//主题目内容
                    if (topicRelations != null)
                        await uow.GetGuidRepository<SxTopicRelation>().InsertAsync(topicRelations);//题目关联

                    await repo_t.InsertAsync(teticketTopic);//票据内容

                    await repo_t.InsertAsync(certificateTopic);//记账凭证内容
                    await uow.GetGuidRepository<SxCertificateTopic>().InsertAsync(yssxCore);//记账凭证内容
                    if (yssxCertificateData != null)
                        await uow.GetGuidRepository<SxCertificateDataRecord>().InsertAsync(yssxCertificateData);//记账凭证内容
                    if (assistAccountings != null)
                        await uow.GetGuidRepository<SxCertificateAssistAccounting>().InsertAsync(assistAccountings);//科目辅助核算信息

                    if (receiptTopics != null && receiptTopics.Count > 0)
                        await repo_t.InsertAsync(receiptTopics);//收款内容
                    if (payTopics != null && payTopics.Count > 0)
                        await repo_t.InsertAsync(payTopics);//付款内容
                    if (topicPayAccounts != null && topicPayAccounts.Count > 0)
                        await repo_tpai.InsertAsync(topicPayAccounts);//题目银行收付款账户信息

                    if (topicFiles.Count > 0)
                        await uow.GetGuidRepository<SxTopicFile>().InsertAsync(topicFiles);//题目附件，包含主题和子题的所有附件

                    // 修改任务题目总数
                    UpdateTaskTopicCount(uow, mainTopic.TaskId, 1, currentUser);
                    // 修改任务题目总数
                    UpdateCaseTopicTotalScore(uow, mainTopic.CaseId, mainTopic.Score, currentUser);

                    uow.Commit();

                    result.Msg = mainTopic.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            return result;
        }

        /// <summary>
        /// 综合题分录题 修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpdateCertificateMainSubTopic(CertificateMainSubTopic model, UserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            List<SxTopicFile> topicFiles = new List<SxTopicFile>();
            var repo_t = DbContext.FreeSql.GetRepository<SxTopic>();

            #region 主题目
            var oldmainTopic = await repo_t
                .Where(e => e.Id == model.MainTopic.Id && e.IsDelete != CommonConstants.IsDelete).FirstAsync();

            if (oldmainTopic == null)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"题目不存在，Id:{model.MainTopic.Id}", false);

            model.MainTopic.CaseId = oldmainTopic.CaseId;
            model.MainTopic.TaskId = oldmainTopic.TaskId;

            var maintuple = CreateMainTopic(model, currentUser);
            SxTopic mainTopic = maintuple.Item1;
            List<SxTopicRelation> topicRelations = maintuple.Item2;
            mainTopic.GroupId = oldmainTopic.GroupId;
            #endregion

            #region 票据
            var hasteticketTopic = await repo_t
                 .Where(e => e.Id == model.TeticketTopic.Id && e.ParentId == mainTopic.Id && e.IsDelete != CommonConstants.IsDelete)
                 .AnyAsync();

            if (!hasteticketTopic)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"题目不存在，Id:{model.TeticketTopic.Id}", false);

            SxTopic newteticketTopic = CreateTeticketTopic(model.TeticketTopic, mainTopic, currentUser, topicFiles);

            mainTopic.PushDate = newteticketTopic.PushDate;
            mainTopic.IsTrap = newteticketTopic.IsTrap;
            #endregion

            #region 收付款

            List<SxTopic> addReceiptTopics = null;//收款
            List<SxTopic> updateReceiptTopics = null;//收款
            List<SxTopic> addPayTopics = null;//付款
            List<SxTopic> updatepayTopics = null;//付款
            List<SxTopicPayAccountInfo> topicPayAccounts = new List<SxTopicPayAccountInfo>();

            if (model.ReceiptTopics != null && model.ReceiptTopics.Count > 0)
            {
                addReceiptTopics = new List<SxTopic>();//收款
                updateReceiptTopics = new List<SxTopic>();//收款
                foreach (var item in model.ReceiptTopics)
                {
                    //收款题目
                    var receiptTopic = CreateReceiptTopic(item, mainTopic, currentUser, topicFiles, topicPayAccounts);
                    if (receiptTopic != null)
                    {
                        //收款
                        if (item.Id <= 0)//新增
                        {
                            addReceiptTopics.Add(receiptTopic);
                        }
                        else//修改
                        {
                            var hasreceiptTopic = await repo_t
                               .Where(e => e.Id == item.Id && e.ParentId == mainTopic.Id && e.IsDelete != CommonConstants.IsDelete).AnyAsync();

                            if (!hasreceiptTopic)
                                return new ResponseContext<bool>(CommonConstants.BadRequest, $"题目不存在，Id:{item.Id}", false);

                            updateReceiptTopics.Add(receiptTopic);
                        }
                    }
                }
            }
            //付款
            if (model.PayTopics != null && model.PayTopics.Count > 0)
            {
                addPayTopics = new List<SxTopic>();//收款
                updatepayTopics = new List<SxTopic>();//收款
                foreach (var item in model.PayTopics)
                {
                    var payTopic = CreatePayTopic(item, mainTopic, currentUser, topicFiles, topicPayAccounts);//付款

                    if (item.Id <= 0)//新增
                    {
                        addPayTopics.Add(payTopic);
                    }
                    else//修改
                    {
                        var haspayTopic = await repo_t
                            .Where(e => e.Id == item.Id && e.ParentId == mainTopic.Id && e.IsDelete != CommonConstants.IsDelete).AnyAsync();

                        if (!haspayTopic)
                            return new ResponseContext<bool>(CommonConstants.BadRequest, $"题目不存在，Id:{item.Id}", false);

                        updatepayTopics.Add(payTopic);
                    }
                }
            }

            #endregion

            #region 记账凭证
            var hascertificateTopic = await repo_t
                      .Where(e => e.Id == model.CertificateTopic.Id && e.ParentId == mainTopic.Id && e.IsDelete != CommonConstants.IsDelete).AnyAsync();

            if (!hascertificateTopic)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"题目不存在，Id:{model.CertificateTopic.Id}", false);

            var oldyssxCoreId = await DbContext.FreeSql.GetRepository<SxCertificateTopic>()
                               .Where(e => e.TopicId == model.CertificateTopic.Id && e.IsDelete != CommonConstants.IsDelete).FirstAsync(e => e.Id);

            if (oldyssxCoreId <= 0)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"题目不存在，sxCertificateTopic Id:{model.CertificateTopic.Id}", false);

            //记账凭证题目（分录题）
            var tuple = CreateCertificateTopic(model.CertificateTopic, mainTopic, topicFiles, oldyssxCoreId);// 构建修改分录题的修改对象
            var certificateTopic = tuple.Item1;
            var yssxCore = tuple.Item2;
            var yssxCertificateData = tuple.Item3;
            List<SxCertificateAssistAccounting> assistAccountings = tuple.Item4;//科目辅助核算信息

            #endregion

            #region 题目附件
            //附件文件
            if (topicFiles.Count > 0)
            {
                var cfileIds = topicFiles
                    .Where(e => e.Type != TopicFileType.Fault)//去错误票据
                    .Select(e => e.Id)
                    .ToArray();

                certificateTopic.TopicFileIds = cfileIds.Join(",");
                yssxCore.InvoicesNum = cfileIds.Length;

                mainTopic.TopicFileIds = topicFiles.Select(e => e.Id).ToArray().Join(",");//多有文件Id保存在主题目中
            }
            #endregion

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo_tr = uow.GetRepository<SxTopicRelation>();
                    var repo_tf = uow.GetRepository<SxTopicFile>();
                    var repo_cdr = uow.GetRepository<SxCertificateDataRecord>();
                    var repo_taa = uow.GetGuidRepository<SxCertificateAssistAccounting>();
                    var repo_t2 = uow.GetRepository<SxTopic>();
                    var repo_tpai = uow.GetGuidRepository<SxTopicPayAccountInfo>();

                    //主题目内容
                    await repo_t2.UpdateDiy.SetSource(mainTopic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.GroupId }).ExecuteAffrowsAsync();//修改主题目内容
                    //票据内容
                    await repo_t2.UpdateDiy.SetSource(newteticketTopic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.GroupId }).ExecuteAffrowsAsync();//修改票据内容

                    #region 题目关联 
                    //删除旧的题目关联    
                    await repo_tr.UpdateDiy.Set(c => new SxTopicRelation
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateBy = currentUser.Id,
                        UpdateTime = DateTime.Now
                    }).Where(e => e.TopicId == mainTopic.Id && e.IsDelete != CommonConstants.IsDelete).ExecuteAffrowsAsync();
                    //添加新的题目关联
                    if (topicRelations != null)
                        await repo_tr.InsertAsync(topicRelations);
                    #endregion

                    #region 记账凭证
                    //修改题目内容 Topic
                    await repo_t2.UpdateDiy.SetSource(certificateTopic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.GroupId }).ExecuteAffrowsAsync();
                    //修改记账凭证内容 CertificateTopic
                    await uow.GetRepository<SxCertificateTopic>().UpdateDiy.SetSource(yssxCore).IgnoreColumns(a => new { a.CreateBy, a.CreateTime }).ExecuteAffrowsAsync();
                    //删除旧的录题凭证数据记录
                    await repo_cdr.UpdateDiy.Set(c => new SxCertificateDataRecord
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateBy = currentUser.Id,
                        UpdateTime = DateTime.Now
                    }).Where(e => e.CertificateTopicId == yssxCore.Id).ExecuteAffrowsAsync();
                    //添加新的录题凭证数据
                    if (yssxCertificateData != null)
                        await repo_cdr.InsertAsync(yssxCertificateData);
                    //删除旧的题目核算信息
                    await repo_taa.UpdateDiy.Set(c => new SxCertificateAssistAccounting
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateBy = currentUser.Id,
                        UpdateTime = DateTime.Now
                    }).Where(e => e.TopicId == certificateTopic.Id).ExecuteAffrowsAsync();
                    if (assistAccountings != null)
                        await repo_taa.InsertAsync(assistAccountings);//科目辅助核算信息
                    #endregion

                    #region 收款 逻辑：先修改在删除最后添加

                    var notDelReceiptTopicIds = new List<long>();//不删除的收款题
                    //修改收款内容
                    if (updateReceiptTopics != null && updateReceiptTopics.Count > 0)
                    {
                        await repo_t2.UpdateDiy.SetSource(updateReceiptTopics).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.GroupId }).ExecuteAffrowsAsync();//收款内容
                        notDelReceiptTopicIds = updateReceiptTopics.Select(e => e.Id).ToList();//更改的收款题id添加到不能删除的集合中
                    }
                    //删除收付款题目
                    var del_ReceiptTopicId = repo_t2.UpdateDiy.Set(c => new SxTopic
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateBy = currentUser.Id,
                        UpdateTime = DateTime.Now
                    }).Where(e => e.ParentId == mainTopic.Id
                            && e.QuestionType == QuestionType.ReceiptTopic
                            && e.IsDelete == CommonConstants.IsNotDelete);
                    if (notDelReceiptTopicIds != null && notDelReceiptTopicIds.Count > 0)//去除掉更改的id
                        del_ReceiptTopicId.Where(e => !notDelReceiptTopicIds.Contains(e.Id));
                    await del_ReceiptTopicId.ExecuteAffrowsAsync();

                    //添加收款内容
                    if (addReceiptTopics != null && addReceiptTopics.Count > 0)//收款
                    {
                        //添加收款内容
                        await repo_t2.InsertAsync(addReceiptTopics);
                    }

                    #endregion

                    #region 付款 逻辑：先修改在删除最后添加

                    var notDelpayTopicIds = new List<long>();
                    //修改付款内容
                    if (updatepayTopics != null && updatepayTopics.Count > 0)
                    {
                        await repo_t2.UpdateDiy.SetSource(updatepayTopics).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.GroupId }).ExecuteAffrowsAsync(); //收款内容
                        notDelpayTopicIds = updatepayTopics.Select(e => e.Id).ToList();
                    }
                    //删除付款题目
                    var delpayTopic = repo_t2.UpdateDiy.Set(c => new SxTopic
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateBy = currentUser.Id,
                        UpdateTime = DateTime.Now
                    }).Where(e => e.ParentId == mainTopic.Id
                                && e.QuestionType == QuestionType.PayTopic
                                && e.IsDelete == CommonConstants.IsNotDelete);
                    if (notDelpayTopicIds != null && notDelpayTopicIds.Count > 0)//去除掉更改的id
                        delpayTopic.Where(e => !notDelpayTopicIds.Contains(e.Id));
                    await delpayTopic.ExecuteAffrowsAsync();

                    //新增付款内容
                    if (addPayTopics != null && addPayTopics.Count > 0)
                    {
                        await repo_t2.InsertAsync(addPayTopics);//新增付款内容
                    }

                    #endregion

                    #region 收付款题目的收付款信息

                    //删除收付款题目的收付款信息
                    await repo_tpai.UpdateDiy.Set(c => new SxTopicPayAccountInfo
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateBy = currentUser.Id,
                        UpdateTime = DateTime.Now
                    }).Where(e => e.ParentTopicId == mainTopic.Id && e.IsDelete == CommonConstants.IsNotDelete).ExecuteAffrowsAsync();
                    //添加收付款题目的收付款信息
                    if (topicPayAccounts != null && topicPayAccounts.Count > 0)
                        await repo_tpai.InsertAsync(topicPayAccounts);
                    #endregion

                    #region 题目附件
                    //删除旧的题目附件
                    //await repo_tf.DeleteAsync(a => a.ParentTopicId == mainTopic.Id);
                    await repo_tf.UpdateDiy.Set(c => new SxTopicFile
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(a => a.ParentTopicId == mainTopic.Id).ExecuteAffrowsAsync();
                    //添加新的题目文件
                    if (topicFiles.Count > 0)
                        await repo_tf.InsertAsync(topicFiles);//题目附件，包含主题和子题的所有附件
                    #endregion
                    uow.Commit();
                    result.Msg = mainTopic.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion
            return result;
        }


        /// <summary>
        /// 构建添加主题目对象
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private static Tuple<SxTopic, List<SxTopicRelation>> CreateMainTopic(CertificateMainSubTopic model, UserTicket currentUser)
        {
            var mainTopic = model.MainTopic.MapTo<SxTopic>();

            mainTopic.CreateBy = currentUser.Id;
            mainTopic.QuestionType = QuestionType.CertificateMainSubTopic;

            if (model.ReceiptTopics != null && model.ReceiptTopics.Count > 0 && model.PayTopics != null && model.PayTopics.Count > 0)
            {
                mainTopic.IsReceipt = true;
                mainTopic.ReceivePaymentType = ReceivePaymentType.ReceivePayTopic;
            }
            else if (model.ReceiptTopics != null && model.ReceiptTopics.Count > 0)
            {
                mainTopic.IsReceipt = true;
                mainTopic.ReceivePaymentType = ReceivePaymentType.ReceiveTopic;
            }
            else if (model.PayTopics != null && model.PayTopics.Count > 0)
            {
                mainTopic.IsReceipt = true;
                mainTopic.ReceivePaymentType = ReceivePaymentType.PaymentTopic;
            }
            else//没有收付款题目
            {
                mainTopic.IsReceipt = false;
                mainTopic.ReceivePaymentType = ReceivePaymentType.None;
            }

            if (mainTopic.Id <= 0)//新增
            {
                mainTopic.Id = IdWorker.NextId();//主题目Id
                mainTopic.GroupId = IdWorker.NextId();//分组Id
            }
            else//修改
            {
                mainTopic.UpdateTime = DateTime.Now;
                mainTopic.UpdateBy = currentUser.Id;
            }

            // 构建管理题目数据
            List<SxTopicRelation> topicRelations = CreateTopicRelation(model.MainTopic.TopicRelations, mainTopic.Id, mainTopic.CaseId, currentUser, mainTopic.TaskId);

            return new Tuple<SxTopic, List<SxTopicRelation>>(mainTopic, topicRelations);
        }

        /// <summary>
        /// 构建新增票据题目对象
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mainTopic"></param>
        /// <param name="currentUser"></param>
        /// <param name="topicFiles"></param>
        /// <returns></returns>
        private static SxTopic CreateTeticketTopic(TeticketTopic model, SxTopic mainTopic, UserTicket currentUser, List<SxTopicFile> topicFiles)
        {
            var obj = model.MapTo<SxTopic>();

            obj.CaseId = mainTopic.CaseId;//案例id
            obj.ParentId = mainTopic.Id;
            obj.TaskId = mainTopic.TaskId;
            obj.GroupId = mainTopic.GroupId;
            obj.Title = mainTopic.Title;
            obj.IsShowHint = mainTopic.IsShowHint;
            obj.IsShowAnswer = mainTopic.IsShowAnswer;
            obj.Status = mainTopic.Status;
            obj.BusinessDate = mainTopic.BusinessDate;
            obj.PushDate = model.PushDate;
            obj.QuestionType = QuestionType.TeticketTopic;
            obj.TopicRelationStatus = mainTopic.TopicRelationStatus;
            obj.Sort = mainTopic.Sort;

            if (obj.PushDate.HasValue)//如果推送时间不为空，即为干扰题
                obj.IsTrap = true;
            else
                obj.IsTrap = false;

            if (obj.Id <= 0)//新增
            {
                obj.Id = IdWorker.NextId();//获取唯一id

                obj.CreateBy = currentUser.Id;
            }
            else//修改
            {
                obj.UpdateBy = currentUser.Id;
                obj.UpdateTime = DateTime.Now;
            }

            obj.TopicFileIds = CreateTopicFile(model.Files, obj.CaseId, obj.Id, currentUser.Id, topicFiles, obj.TaskId, obj.ParentId);//生成票据附件数据
            return obj;
        }

        /// <summary>
        /// 构建付款题目(保存和修改)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mainTopic"></param>
        /// <param name="currentUser"></param>
        /// <param name="topicFiles"></param>
        /// <returns></returns>
        private static List<SxTopic> CreatePayTopic(List<PayTopic> models, SxTopic mainTopic, UserTicket currentUser, List<SxTopicFile> topicFiles, List<SxTopicPayAccountInfo> topicPayAccounts)
        {
            if (models == null || models.Count == 0)
                return null;
            List<SxTopic> topics = new List<SxTopic>();
            //付款
            foreach (var model in models)
            {
                var t = CreatePayTopic(model, mainTopic, currentUser, topicFiles, topicPayAccounts);
                if (t != null)
                    topics.Add(t);
            }
            return topics;
        }

        private static SxTopic CreatePayTopic(PayTopic model, SxTopic mainTopic, UserTicket currentUser, List<SxTopicFile> topicFiles, List<SxTopicPayAccountInfo> topicPayAccounts)
        {
            if (model == null)
                return null;
            SxTopic obj = model.MapTo<SxTopic>();

            obj.CaseId = mainTopic.CaseId;//案例id
            obj.ParentId = mainTopic.Id;
            obj.Title = mainTopic.Title;
            obj.TaskId = mainTopic.TaskId;
            obj.IsShowHint = mainTopic.IsShowHint;
            obj.IsShowAnswer = mainTopic.IsShowAnswer;
            obj.Status = mainTopic.Status;
            obj.BusinessDate = mainTopic.BusinessDate;
            obj.QuestionType = QuestionType.PayTopic;
            obj.TopicRelationStatus = mainTopic.TopicRelationStatus;
            obj.Sort = mainTopic.Sort;
            if (obj.Id <= 0)//添加
            {
                obj.CreateBy = currentUser.Id;
                obj.Id = IdWorker.NextId();//获取唯一id
                obj.GroupId = mainTopic.GroupId;
            }
            else//修改
            {
                obj.UpdateBy = currentUser.Id;
                obj.UpdateTime = DateTime.Now;
            }
            obj.TopicFileIds = CreateTopicFile(model.Files, obj.CaseId, obj.Id, currentUser.Id, topicFiles, obj.TaskId, obj.ParentId);//生成票据附件数据

            var account = CreateTopicPayAccountInfo(model, obj);//收付款账户信息
            if (account != null)
            {
                topicPayAccounts.Add(account);
            }
            return obj;
        }

        /// <summary>
        /// 构建收款题目
        /// </summary>
        /// <param name="models"></param>
        /// <param name="mainTopic"></param>
        /// <param name="currentUser"></param>
        /// <param name="topicFiles"></param>
        /// <returns></returns>
        private static List<SxTopic> CreateReceiptTopic(List<ReceiptTopic> models, SxTopic mainTopic, UserTicket currentUser, List<SxTopicFile> topicFiles, List<SxTopicPayAccountInfo> topicPayAccounts)
        {
            if (models == null || models.Count == 0)
                return null;

            List<SxTopic> topics = new List<SxTopic>();
            foreach (var model in models)
            {
                SxTopic obj = CreateReceiptTopic(model, mainTopic, currentUser, topicFiles, topicPayAccounts);
                if (obj != null)
                {
                    topics.Add(obj);
                }

            }
            return topics;
        }

        private static SxTopic CreateReceiptTopic(ReceiptTopic model, SxTopic mainTopic, UserTicket currentUser, List<SxTopicFile> topicFiles, List<SxTopicPayAccountInfo> accounts)
        {
            if (model == null)
                return null;
            //收款
            SxTopic obj = model.MapTo<SxTopic>();

            obj.CaseId = mainTopic.CaseId;//案例id
            obj.ParentId = mainTopic.Id;
            obj.TaskId = mainTopic.TaskId;
            obj.Title = mainTopic.Title;
            obj.IsShowHint = mainTopic.IsShowHint;
            obj.IsShowAnswer = mainTopic.IsShowAnswer;
            obj.Status = mainTopic.Status;
            obj.BusinessDate = mainTopic.BusinessDate;
            obj.QuestionType = QuestionType.ReceiptTopic;
            obj.TopicRelationStatus = mainTopic.TopicRelationStatus;
            obj.Sort = mainTopic.Sort;

            if (obj.Id <= 0)
            {
                obj.GroupId = mainTopic.GroupId;
                obj.CreateBy = currentUser.Id;
                obj.Id = IdWorker.NextId();//获取唯一id
            }
            else
            {
                obj.UpdateBy = currentUser.Id;
                obj.UpdateTime = DateTime.Now;
            }

            obj.TopicFileIds = CreateTopicFile(model.Files, obj.CaseId, obj.Id, currentUser.Id, topicFiles, obj.TaskId, obj.ParentId);//生成票据附件数据

            var account = CreateTopicPayAccountInfo(model, obj);//收付款账户信息
            if (account != null)
            {
                accounts.Add(account);
            }
            return obj;
        }
        private static SxTopicPayAccountInfo CreateTopicPayAccountInfo(PayTopic model, SxTopic obj)
        {
            if (model == null)
                return null;
            var account = model.MapTo<SxTopicPayAccountInfo>();
            account.Id = IdWorker.NextId();//获取唯一id
            account.TopicId = obj.Id;
            account.ParentTopicId = obj.ParentId;
            account.TaskId = obj.TaskId;
            account.CaseId = obj.CaseId;
            account.CreateBy = obj.CreateBy;
            return account;
        }
        private static SxTopicPayAccountInfo CreateTopicPayAccountInfo(ReceiptTopic model, SxTopic obj)
        {
            if (model == null)
                return null;
            var account = model.MapTo<SxTopicPayAccountInfo>();
            account.Id = IdWorker.NextId();//获取唯一id
            account.TopicId = obj.Id;
            account.ParentTopicId = obj.ParentId;
            account.TaskId = obj.TaskId;
            account.CaseId = obj.CaseId;
            account.CreateBy = obj.CreateBy;
            return account;
        }

        /// <summary>
        /// 生成记账凭证 (添加和修改)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mainTopic"></param>
        /// <param name="topicFiles"></param>
        /// <param name="certificateTopic"></param>
        /// <param name="yssxCore"></param>
        /// <param name="yssxCertificateData"></param>
        /// <param name="certificateTopicId">certificateTopic表中的id(修改时需要赋值)</param>
        private static Tuple<SxTopic, SxCertificateTopic, List<SxCertificateDataRecord>, List<SxCertificateAssistAccounting>> CreateCertificateTopic
            (CertificateTopic model, SxTopic mainTopic, List<SxTopicFile> topicFiles, long certificateTopicId = 0)
        {
            //记账凭证
            var obj = model.MapTo<SxTopic>();
            long caseId = mainTopic.CaseId;
            obj.CaseId = caseId;//案例id
            obj.ParentId = mainTopic.Id;
            obj.GroupId = mainTopic.GroupId;
            obj.Title = mainTopic.Title;
            obj.TaskId = mainTopic.TaskId;
            obj.IsShowHint = mainTopic.IsShowHint;
            obj.IsShowAnswer = mainTopic.IsShowAnswer;
            obj.Status = mainTopic.Status;
            obj.BusinessDate = mainTopic.BusinessDate;
            obj.QuestionType = QuestionType.CertifMSub_AccountEntry;
            obj.TopicRelationStatus = mainTopic.TopicRelationStatus;
            obj.Sort = mainTopic.Sort;
            obj.SettlementType = mainTopic.SettlementType;

            SxCertificateTopic certificateTopic = model.MapTo<SxCertificateTopic>();
            certificateTopic.Title = mainTopic.Title;
            certificateTopic.SummaryInfos = model.SummaryInfos;
            certificateTopic.CaseId = obj.CaseId;
            certificateTopic.TaskId = obj.TaskId;

            if (obj.Id <= 0) //新增
            {
                obj.Id = IdWorker.NextId();//获取唯一id
                obj.CreateBy = mainTopic.CreateBy;

                certificateTopicId = certificateTopic.Id = IdWorker.NextId();//获取唯一id
                certificateTopic.CreateBy = mainTopic.CreateBy;
            }
            else//修改
            {
                obj.UpdateBy = mainTopic.UpdateBy;
                obj.UpdateTime = DateTime.Now;

                certificateTopic.Id = certificateTopicId;//修改的时候赋值原id
                certificateTopic.UpdateBy = mainTopic.UpdateBy;
                certificateTopic.UpdateTime = DateTime.Now;
            }

            certificateTopic.TopicId = obj.Id; //不能移动
            // 构建录题凭证数据记录表
            List<SxCertificateDataRecord> yssxCertificateDatas = CreateCertificateDataRecord(model.DataRecords, certificateTopic);
            //科目辅助核算信息
            List<SxCertificateAssistAccounting> assistAccountings = CreateCertificateAssistAccounting(model.AssistAccountings, caseId, obj.Id, mainTopic.CreateBy, obj.TaskId);

            return new Tuple<SxTopic, SxCertificateTopic, List<SxCertificateDataRecord>, List<SxCertificateAssistAccounting>>(obj, certificateTopic, yssxCertificateDatas, assistAccountings);
        }

        /// <summary>
        /// 科目辅助核算信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="mainTopic"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static List<SxCertificateAssistAccounting> CreateCertificateAssistAccounting(List<TopicAssistAccounting> topicAssistAccountings, long caseId, long topicId, long createBy, long taskId)
        {
            //科目辅助核算信息
            List<SxCertificateAssistAccounting> assistAccountings = null;
            if (topicAssistAccountings != null && topicAssistAccountings.Count > 0)
            {
                assistAccountings = new List<SxCertificateAssistAccounting>();
                topicAssistAccountings.ForEach(e =>
                {
                    assistAccountings.Add(new SxCertificateAssistAccounting
                    {
                        Id = IdWorker.NextId(),//设置id
                        SubjectId = e.SubjectId,
                        AssistAccountingType = e.AssistAccountingType,
                        CreateBy = createBy,
                        CaseId = caseId,
                        TaskId = taskId,
                        TopicId = topicId,
                    });
                });
            }

            return assistAccountings;
        }

        /// <summary>
        /// 构建录题凭证数据记录表
        /// </summary>
        /// <param name="dataRecords"></param>
        /// <param name="sxCertificateTopic"></param>
        /// <returns></returns>
        private static List<SxCertificateDataRecord> CreateCertificateDataRecord(List<CertificateDataRecord> dataRecords, SxCertificateTopic sxCertificateTopic)
        {
            List<SxCertificateDataRecord> yssxCertificateDatas = null;
            if (dataRecords != null && dataRecords.Count > 0)
            {
                yssxCertificateDatas = new List<SxCertificateDataRecord>();

                for (int i = 0; i < dataRecords.Count; i++)
                {
                    var item = dataRecords[i];

                    yssxCertificateDatas.Add(new SxCertificateDataRecord
                    {
                        Id = IdWorker.NextId(),//设置id
                        BorrowAmount = item.BorrowAmount,
                        CreditorAmount = item.CreditorAmount,
                        SubjectId = item.SubjectId,
                        SummaryInfo = item.SummaryInfo,
                        CreateBy = sxCertificateTopic.CreateBy,
                        CaseId = sxCertificateTopic.CaseId,
                        TaskId = sxCertificateTopic.TaskId,
                        TopicId = sxCertificateTopic.TopicId,
                        CertificateTopicId = sxCertificateTopic.Id,
                        CertificateDate = sxCertificateTopic.CertificateDate,
                        CertificateNo = sxCertificateTopic.CertificateNo,
                        CertificateWord = sxCertificateTopic.CertificateWord.GetDescription(),
                    });
                }
            }

            return yssxCertificateDatas;
        }

        /// <summary>
        /// 构建管理题目数据
        /// </summary>
        /// <param name="topicRelations"></param>
        /// <param name="caseId"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private static List<SxTopicRelation> CreateTopicRelation(List<TopicRelation> topicRelations, long topicId, long caseId, UserTicket currentUser, long taskId)
        {
            List<SxTopicRelation> list = null;
            if (topicRelations != null)
            {
                list = new List<SxTopicRelation>();
                foreach (var item in topicRelations)
                {
                    //题目关联数据设置
                    var tr = new SxTopicRelation
                    {
                        Id = IdWorker.NextId(),//获取唯一id
                        CreateBy = currentUser.Id,
                        CaseId = caseId,
                        TopicId = topicId,
                        TaskId = taskId,
                        PreTopicId = item.TopicId,
                        PreTopicParentId = item.TopicParentId,
                        RelationTopicType = item.RelationTopicType,
                        IsShow = item.IsShow,
                    };

                    list.Add(tr);
                }
            }

            return list;
        }

        /// <summary>
        /// 生成题目附件数据
        /// </summary>
        /// <param name="files">附件源数据</param>
        /// <param name="caseId">案例id</param>
        /// <param name="topicId">主题目id</param>
        /// <param name="topicFiles">返回的题目附件集合</param>
        /// <param name="parentTopicId">父题目id</param>
        /// <returns></returns>
        private static string CreateTopicFile(List<TopicFile> files, long caseId, long topicId, long createById, List<SxTopicFile> topicFiles, long taskId, long parentTopicId)
        {
            if (files == null || files.Count == 0)//附件为空           
                return null;

            StringBuilder stringBuilder = new StringBuilder();

            if (topicFiles == null)
                topicFiles = new List<SxTopicFile>();
            var loop = 0;
            foreach (var item in files)
            {
                SxTopicFile file = item.MapTo<SxTopicFile>();

                file.Id = IdWorker.NextId();
                file.CaseId = caseId;
                file.TaskId = taskId;
                file.TopicId = topicId;
                file.ParentTopicId = parentTopicId;
                file.CreateBy = createById;
                if (file.Sort == 0)
                    file.Sort = ++loop;

                topicFiles.Add(file);
                stringBuilder.Append(file.Id).Append(",");
            }

            return stringBuilder.ToString().Remove(stringBuilder.Length - 1, 1);
        }
        #endregion

        #region 结转损益题

        /// <summary>
        /// 新增结转损益题
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddProfitLossTopic(ProfitLossTopicDto model, UserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);

            #region 构建题目相关数据


            var tup = CreateProfitLossTopic(model, currentUser);
            SxTopic topic = tup.Item1;
            SxCertificateTopic yssxCore = tup.Item2;

            // 构建录题凭证数据记录表
            var yssxCertificateData = CreateCertificateDataRecord(model.DataRecords, yssxCore);

            // 构建结转损益题的分开结转答案数据
            List<SxCertificateAnswer> certificateAnswers = CreateCertificateAnswer(model, currentUser, topic);

            //科目辅助核算信息
            List<SxCertificateAssistAccounting> assistAccountings = CreateCertificateAssistAccounting(model.AssistAccountings, topic.CaseId, topic.Id, currentUser.Id, topic.TaskId);

            #endregion

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    await uow.GetGuidRepository<SxTopic>().InsertAsync(topic);//记账凭证内容
                    await uow.GetGuidRepository<SxCertificateTopic>().InsertAsync(yssxCore);//记账凭证内容
                    if (yssxCertificateData != null)
                        await uow.GetGuidRepository<SxCertificateDataRecord>().InsertAsync(yssxCertificateData);//记账凭证内容
                    if (certificateAnswers != null)
                        await uow.GetGuidRepository<SxCertificateAnswer>().InsertAsync(certificateAnswers);//分开结转答案
                    if (assistAccountings != null)
                        await uow.GetGuidRepository<SxCertificateAssistAccounting>().InsertAsync(assistAccountings);//科目辅助核算信息

                    // 修改任务题目总数
                    UpdateTaskTopicCount(uow, topic.TaskId, 1, currentUser);
                    // 修改任务题目总数
                    UpdateCaseTopicTotalScore(uow, topic.CaseId, topic.Score, currentUser);

                    uow.Commit();
                    result.Msg = topic.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            return result;
        }

        /// <summary>
        /// 修改结转损益题
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UpdateProfitLossTopic(ProfitLossTopicDto model, UserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);

            var repo_ct2 = DbContext.FreeSql.GetRepository<SxCertificateTopic>();

            var hastopic = await DbContext.FreeSql.GetRepository<SxTopic>()
                .Where(e => e.Id == model.Id && e.IsDelete != CommonConstants.IsDelete).AnyAsync();

            if (!hastopic)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"题目不存在，Id:{model.Id}", false);

            var yssxCoreid = await repo_ct2
                               .Where(e => e.TopicId == model.Id && e.IsDelete != CommonConstants.IsDelete)
                               .FirstAsync(a => a.Id);

            if (yssxCoreid <= 0)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"题目不存在，SxCertificateTopic topicId:{model.Id}", false);

            var tup = CreateProfitLossTopic(model, currentUser, yssxCoreid);
            SxTopic topic = tup.Item1;
            SxCertificateTopic yssxCore = tup.Item2;

            // 构建录题凭证数据记录表
            var yssxCertificateData = CreateCertificateDataRecord(model.DataRecords, yssxCore);

            //构建结转损益题的分开结转答案数据
            List<SxCertificateAnswer> certificateAnswers = CreateCertificateAnswer(model, currentUser, topic);
            //科目辅助核算信息
            List<SxCertificateAssistAccounting> assistAccountings = CreateCertificateAssistAccounting(model.AssistAccountings, topic.CaseId, topic.Id, currentUser.Id, topic.TaskId);

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo_t = uow.GetRepository<SxTopic>();
                    var repo_ct = uow.GetRepository<SxCertificateTopic>();
                    var repo_tr = uow.GetRepository<SxTopicRelation>();
                    var repo_cdr = uow.GetRepository<SxCertificateDataRecord>();
                    var repo_ca = uow.GetRepository<SxCertificateAnswer>();
                    var repo_caa = uow.GetGuidRepository<SxCertificateAssistAccounting>();
                    //修改题目内容
                    await repo_t.UpdateDiy.SetSource(topic).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.CaseId, a.TaskId }).ExecuteAffrowsAsync();
                    //修改记账凭证内容
                    await repo_ct.UpdateDiy.SetSource(yssxCore).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, a.CaseId, a.TaskId }).ExecuteAffrowsAsync();
                    //删除旧的录题凭证数据记录                               
                    await repo_cdr.UpdateDiy.Set(c => new SxCertificateDataRecord
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(e => e.CertificateTopicId == yssxCore.Id && e.IsDelete == CommonConstants.IsNotDelete).ExecuteAffrowsAsync();

                    if (yssxCertificateData != null)
                        await repo_cdr.InsertAsync(yssxCertificateData);//添加新的录题凭证数据

                    //删除分开结转答案                                                                  
                    //await repo_ca.DeleteAsync(e => e.TopicId == topic.Id && e.IsDelete == CommonConstants.IsNotDelete);
                    await repo_ca.UpdateDiy.Set(c => new SxCertificateAnswer
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(e => e.TopicId == topic.Id && e.IsDelete == CommonConstants.IsNotDelete).ExecuteAffrowsAsync();
                    if (certificateAnswers != null)//保存分开结转答案 
                        await repo_ca.InsertAsync(certificateAnswers);

                    //删除科目辅助核算信息
                    //await repo_caa.DeleteAsync(e => e.TopicId == topic.Id && e.IsDelete == CommonConstants.IsNotDelete);
                    await repo_caa.UpdateDiy.Set(c => new SxCertificateAssistAccounting
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(e => e.TopicId == topic.Id && e.IsDelete == CommonConstants.IsNotDelete).ExecuteAffrowsAsync();
                    if (assistAccountings != null)
                        await repo_caa.InsertAsync(assistAccountings);//科目辅助核算信息

                    uow.Commit();
                    result.Msg = topic.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            return result;
        }

        private static Tuple<SxTopic, SxCertificateTopic> CreateProfitLossTopic(ProfitLossTopicDto model, UserTicket currentUser, long certificateTopicId = 0)
        {
            //记账凭证
            SxTopic topic = model.MapTo<SxTopic>();
            topic.QuestionType = QuestionType.ProfitLoss;

            if (topic.Id <= 0)//新增
            {
                topic.Id = IdWorker.NextId();//获取唯一id
                topic.CreateBy = currentUser.Id;
            }
            else//修改
            {
                topic.UpdateBy = currentUser.Id;
                topic.UpdateTime = DateTime.Now;
            }

            SxCertificateTopic yssxCore = CreateSxCertificateTopic(model, topic, certificateTopicId);

            return new Tuple<SxTopic, SxCertificateTopic>(topic, yssxCore);
        }
        /// <summary>
        /// 构建分录单独表数据 SxCertificateTopic 新增和修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="topic"></param>
        /// <param name="certificateTopicId">原SxCertificateTopic id 修改使用</param>
        /// <returns></returns>
        private static SxCertificateTopic CreateSxCertificateTopic(ProfitLossTopicDto model, SxTopic topic, long certificateTopicId)
        {
            SxCertificateTopic certificateTopic = model.MapTo<SxCertificateTopic>();

            certificateTopic.Title = topic.Title;
            certificateTopic.TopicId = topic.Id;
            certificateTopic.CaseId = topic.CaseId;
            certificateTopic.TaskId = topic.TaskId;

            if (certificateTopic.Id <= 0)//新增
            {
                certificateTopic.Id = IdWorker.NextId();//获取唯一id
                certificateTopic.CreateBy = topic.CreateBy;
            }
            else//修改
            {
                certificateTopic.Id = certificateTopicId;//修改的时候赋值原id
                certificateTopic.UpdateBy = topic.Id;
                certificateTopic.UpdateTime = DateTime.Now;
            }

            return certificateTopic;
        }

        /// <summary>
        /// 构建结转损益题的分开结转答案数据
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <param name="topic"></param>
        /// <returns></returns>
        private static List<SxCertificateAnswer> CreateCertificateAnswer(ProfitLossTopicDto model, UserTicket currentUser, SxTopic topic)
        {
            //分开结转
            List<SxCertificateAnswer> certificateAnswers = null;
            if (model.Answers != null && model.Answers.Count > 0)
            {
                certificateAnswers = new List<SxCertificateAnswer>();
                model.Answers.ForEach(e =>
                {
                    var item = e.MapTo<SxCertificateAnswer>();
                    item.Id = IdWorker.NextId();//设置id
                    item.CreateBy = currentUser.Id;
                    item.TopicId = topic.Id;
                    item.CaseId = topic.CaseId;
                    item.TaskId = topic.TaskId;
                    certificateAnswers.Add(item);
                });
            }
            return certificateAnswers;
        }


        /// <summary>
        /// 获取结转损益题信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<ProfitLossTopicDto>> GetProfitLossTopicInofById(long id)
        {
            var result = new ResponseContext<ProfitLossTopicDto>(CommonConstants.ErrorCode, "", null);

            //题目内容
            var topic = await DbContext.FreeSql.GetGuidRepository<SxTopic>()
                .Where(e => e.Id == id && e.IsDelete != CommonConstants.IsDelete)
                .FirstAsync();

            if (topic == null)
            {
                result.Msg = "查询不到改题目";
                return result;
            }

            //映射成dto
            var topicDto = topic.MapTo<ProfitLossTopicDto>();

            //查询分录单独表数据
            var sxCertificateTopic = await DbContext.FreeSql.GetGuidRepository<SxCertificateTopic>()
                  .Where(e => e.TopicId == topic.Id && e.IsDelete == CommonConstants.IsNotDelete)
                  .FirstAsync();

            if (sxCertificateTopic == null)
            {
                result.Msg = "凭证题目查询不到数据";
                return result;
            }

            topicDto.AccountingManager = sxCertificateTopic.AccountingManager;
            topicDto.Auditor = sxCertificateTopic.Auditor;
            topicDto.Cashier = sxCertificateTopic.Cashier;
            topicDto.IsDisableAM = sxCertificateTopic.IsDisableAM;
            topicDto.IsDisableAuditor = sxCertificateTopic.IsDisableAuditor;
            topicDto.IsDisableCashier = sxCertificateTopic.IsDisableCashier;
            topicDto.IsDisableCreator = sxCertificateTopic.IsDisableCreator;
            topicDto.Creator = sxCertificateTopic.Creator;
            topicDto.CertificateNo = sxCertificateTopic.CertificateNo;
            topicDto.CertificateWord = sxCertificateTopic.CertificateWord;
            topicDto.CertificateDate = sxCertificateTopic.CertificateDate;
            topicDto.CertificateWord = sxCertificateTopic.CertificateWord;

            //查询录题凭证数据记录表
            topicDto.DataRecords = await DbContext.FreeSql.GetGuidRepository<SxCertificateDataRecord>()
                .Where(e => e.CertificateTopicId == sxCertificateTopic.Id && e.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync<CertificateDataRecord>();
            //分录凭证分开结转答案
            topicDto.Answers = await DbContext.FreeSql.GetGuidRepository<SxCertificateAnswer>()
                .Where(e => e.TopicId == topic.Id && e.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync<CertificateAnswerDto>();
            //科目辅助核算信息
            topicDto.AssistAccountings = await DbContext.FreeSql.GetGuidRepository<SxCertificateAssistAccounting>()
                .Where(e => e.TopicId == topic.Id && e.IsDelete == CommonConstants.IsNotDelete)
                .ToListAsync<TopicAssistAccounting>();

            result.Data = topicDto;
            result.Code = CommonConstants.SuccessCode;
            return result;
        }

        #endregion

        #region 题目查询

        /// <summary>
        /// 获取综合分录题 信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<CertificateMainSubTopic>> GetCertificateMainSubTopicInfoById(long id)
        {
            var result = new ResponseContext<CertificateMainSubTopic>(CommonConstants.ErrorCode, "", null);
            try
            {
                CertificateMainSubTopic obj = new CertificateMainSubTopic();

                var repo_t = DbContext.FreeSql.GetGuidRepository<SxTopic>();
                var repo_tr = DbContext.FreeSql.GetGuidRepository<SxTopicRelation>();

                //主题目内容
                var mainTopic = await repo_t.Where(e => e.Id == id && e.IsDelete != CommonConstants.IsDelete)
                    .FirstAsync();

                if (mainTopic == null)
                {
                    result.Msg = "题目不存在。";
                    return result;
                }

                obj.MainTopic = mainTopic.MapTo<CertificateMainTopic>();

                //题目关联
                obj.MainTopic.TopicRelations = await repo_tr
                    .Where(e => e.TopicId == id && e.IsDelete != CommonConstants.IsDelete)
                    .ToListAsync(a => new TopicRelation
                    {
                        TopicId = a.PreTopicId,
                        TopicParentId = a.PreTopicParentId,
                        CaseId = a.CaseId,
                        IsShow = a.IsShow,
                        RelationTopicType = a.RelationTopicType,
                    });

                var subTopics = await repo_t.Where(e => e.ParentId == id && e.IsDelete != CommonConstants.IsDelete)
                      .ToListAsync<SxTopic>();
                //题目附件，包含主题和子题的所有附件
                List<SxTopicFile> topicFiles = null;
                if (!string.IsNullOrWhiteSpace(mainTopic.TopicFileIds))//文件不为空，获取文件信息
                {
                    topicFiles = await DbContext.FreeSql.GetGuidRepository<SxTopicFile>()
                    .Where(e => e.ParentTopicId == mainTopic.Id && e.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync();//题目附件，包含主题和子题的所有附件
                }
                //题目银行收付款账户信息
                List<SxTopicPayAccountInfo> topicPayAccountInfos = await DbContext.FreeSql.GetGuidRepository<SxTopicPayAccountInfo>()
                   .Where(e => e.ParentTopicId == mainTopic.Id && e.IsDelete == CommonConstants.IsNotDelete)
                   .ToListAsync();

                #region 子题目赋值
                if (subTopics != null && subTopics.Count > 0)
                {
                    obj.ReceiptTopics = new List<ReceiptTopic>();
                    obj.PayTopics = new List<PayTopic>();
                    foreach (var item in subTopics)
                    {
                        switch (item.QuestionType)
                        {
                            case QuestionType.CertifMSub_AccountEntry://记账凭证内容
                                obj.CertificateTopic = item.MapTo<CertificateTopic>();//记账凭证内容

                                var sxCertificateTopic = await DbContext.FreeSql.GetGuidRepository<SxCertificateTopic>()
                                      .Where(e => e.TopicId == item.Id && e.IsDelete != CommonConstants.IsDelete)
                                      .FirstAsync();
                                if (sxCertificateTopic != null)
                                {
                                    obj.CertificateTopic.AccountingManager = sxCertificateTopic.AccountingManager;
                                    obj.CertificateTopic.Auditor = sxCertificateTopic.Auditor;
                                    obj.CertificateTopic.Cashier = sxCertificateTopic.Cashier;
                                    obj.CertificateTopic.IsDisableAM = sxCertificateTopic.IsDisableAM;
                                    obj.CertificateTopic.IsDisableAuditor = sxCertificateTopic.IsDisableAuditor;
                                    obj.CertificateTopic.IsDisableCashier = sxCertificateTopic.IsDisableCashier;
                                    obj.CertificateTopic.IsDisableCreator = sxCertificateTopic.IsDisableCreator;
                                    obj.CertificateTopic.Creator = sxCertificateTopic.Creator;
                                    obj.CertificateTopic.CertificateNo = sxCertificateTopic.CertificateNo;
                                    obj.CertificateTopic.CertificateWord = sxCertificateTopic.CertificateWord;
                                    obj.CertificateTopic.CertificateDate = sxCertificateTopic.CertificateDate;
                                    obj.CertificateTopic.CertificateWord = sxCertificateTopic.CertificateWord;
                                    obj.CertificateTopic.SummaryInfos = sxCertificateTopic.SummaryInfos;

                                    //单据数据记录
                                    obj.CertificateTopic.DataRecords = await DbContext.FreeSql.GetGuidRepository<SxCertificateDataRecord>()
                                        .Where(e => e.CertificateTopicId == sxCertificateTopic.Id && e.IsDelete != CommonConstants.IsDelete)
                                        .ToListAsync<CertificateDataRecord>();

                                    //科目辅助核算信息
                                    obj.CertificateTopic.AssistAccountings = await DbContext.FreeSql.GetGuidRepository<SxCertificateAssistAccounting>()
                                        .Where(e => e.TopicId == item.Id && e.IsDelete == CommonConstants.IsNotDelete)
                                        .ToListAsync<TopicAssistAccounting>();
                                }
                                break;
                            case QuestionType.TeticketTopic://票据内容
                                var tTopic = item.MapTo<TeticketTopic>();
                                if (!string.IsNullOrWhiteSpace(item.TopicFileIds) && topicFiles != null && topicFiles.Count > 0)
                                    tTopic.Files = topicFiles.Where(e => e.TopicId == tTopic.Id).Select(e => e.MapTo<TopicFile>()).ToList();
                                obj.TeticketTopic = tTopic;//记账凭证内容
                                break;
                            case QuestionType.ReceiptTopic://收款题
                                var rTopic = item.MapTo<ReceiptTopic>();
                                if (!string.IsNullOrWhiteSpace(item.TopicFileIds) && topicFiles != null && topicFiles.Count > 0)
                                    rTopic.Files = topicFiles.Where(e => e.TopicId == rTopic.Id).Select(e => e.MapTo<TopicFile>()).ToList();
                                //题目银行收付款账户信息
                                var topicPayAccountInfo = topicPayAccountInfos.Where(e => e.TopicId == item.Id).FirstOrDefault();
                                ReceiptTopicPayAccountInfo(rTopic, topicPayAccountInfo);

                                obj.ReceiptTopics.Add(rTopic);
                                break;
                            case QuestionType.PayTopic://付款题
                                var pTopic = item.MapTo<PayTopic>();
                                if (!string.IsNullOrWhiteSpace(item.TopicFileIds) && topicFiles != null && topicFiles.Count > 0)
                                    pTopic.Files = topicFiles.Where(e => e.TopicId == pTopic.Id).Select(e => e.MapTo<TopicFile>()).ToList();
                                //题目银行收付款账户信息 
                                var topicPayAccountInfo2 = topicPayAccountInfos.Where(e => e.TopicId == item.Id).FirstOrDefault();
                                PayTopicPayAccountInfo(pTopic, topicPayAccountInfo2);

                                obj.PayTopics.Add(pTopic);
                                break;
                        }
                    }
                }
                #endregion
                result.Data = obj;
                result.Code = CommonConstants.SuccessCode;
            }
            catch (Exception ex)
            {
                result.Msg = ex.ToString();
            }
            return result;
        }
        private void PayTopicPayAccountInfo(PayTopic topic, SxTopicPayAccountInfo topicPayAccountInfo)
        {
            if (topic != null && topicPayAccountInfo != null)
            {
                topic.PayBankId = topicPayAccountInfo.PayBankId;
                topic.PayeeCompanyId = topicPayAccountInfo.PayeeCompanyId;
                topic.Amount = topicPayAccountInfo.Amount;
            }
        }
        private void ReceiptTopicPayAccountInfo(ReceiptTopic topic, SxTopicPayAccountInfo topicPayAccountInfo)
        {
            if (topic != null && topicPayAccountInfo != null)
            {
                topic.PayeeBankId = topicPayAccountInfo.PayeeBankId;
                topic.PayCompanyId = topicPayAccountInfo.PayCompanyId;
                topic.Amount = topicPayAccountInfo.Amount;
            }
        }

        /// <summary>
        /// 根据id获取题目详情
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TopicInfoDto>> GetTopicInfoById(long id)
        {
            return await Task.Run(() =>
            {
                var question = DbContext.FreeSql.GetRepository<SxTopic>()
                .Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete)
                .First();

                if (question == null)
                {
                    return new ResponseContext<TopicInfoDto> { Code = CommonConstants.ErrorCode, Msg = "未找到相关题目" };
                }

                var questionDTO = question.MapTo<TopicInfoDto>();

                if (questionDTO.QuestionType == QuestionType.SingleChoice
                    || questionDTO.QuestionType == QuestionType.MultiChoice
                    || questionDTO.QuestionType == QuestionType.Judge)
                {
                    //简单题型,设置答案
                    //questionDTO.AnswerValue = _optionRep.First(a => a.TopicId == id && a.IsAnswer)?.AnswerOption;
                    //questionDTO.AnswerValue = questionDTO.AnswerValue;
                    //添加选项信息
                    var options = DbContext.FreeSql.Select<SxAnswerOption>()
                .Where(m => m.TopicId == id && m.IsDelete == CommonConstants.IsNotDelete)
                .ToList();
                    questionDTO.Options = new List<ChoiceOption>();
                    foreach (var item in options)
                    {
                        questionDTO.Options.Add(new ChoiceOption()
                        {
                            Id = item.Id,
                            Name = item.Name,
                            Text = item.AnswerOption,
                            AttatchImgUrl = item.AnswerFileUrl,
                            Sort = item.Sort
                        });
                    }
                }
                else if (questionDTO.QuestionType == QuestionType.FinancialStatements)//财务报表题
                {
                    GetFinancialStatements(question, questionDTO);
                }
                else
                {
                    //复杂题型
                    //questionDTO.AnswerValue = _optionRep.First(a => a.TopicId == id)?.AnswerKeysContent;
                    //添加附件信息
                    questionDTO.QuestionFile = new List<TopicFile>();

                    if (!string.IsNullOrEmpty(question.TopicFileIds))
                    {
                        var fileIds = question.TopicFileIds.SplitToArray<long>(',');
                        var files = DbContext.FreeSql.Select<SxTopicFile>()
                        .Where(a => fileIds.Contains(a.Id) && a.IsDelete == CommonConstants.IsNotDelete)
                        .OrderBy(m => m.Sort)
                        .OrderBy(m => m.CreateTime)
                        .ToList();
                        foreach (var item in files)
                        {
                            questionDTO.QuestionFile.Add(new TopicFile()
                            {
                                Url = item.Url,
                                Name = item.Name
                                //Sort = item.Sort
                            });
                        }
                    }
                    if (questionDTO.QuestionType == QuestionType.MainSubQuestion)
                    {
                        var subQuestions = DbContext.FreeSql.Select<SxTopic>()
                            .Where(m => m.ParentId == questionDTO.Id && m.IsDelete == CommonConstants.IsNotDelete)
                            .OrderBy(m => m.Sort)
                            .OrderBy(m => m.CreateTime)
                            .ToList(a => new SubQuestion(a.Id,
                                                         a.QuestionType,
                                                         a.QuestionContentType,
                                                         "",
                                                         a.Hint, a.Content,
                                                         a.TopicContent,
                                                         a.FullContent,
                                                         a.AnswerValue,
                                                         a.Sort, a.Score,
                                                         a.CalculationType));

                        var choiceQuestionIds = subQuestions
                            .Where(a => a.QuestionType == QuestionType.SingleChoice
                                || a.QuestionType == QuestionType.MultiChoice || a.QuestionType == QuestionType.Judge)
                            .Select(a => a.Id)
                            .ToArray();

                        if (choiceQuestionIds.Any())
                        {
                            var optionAll = DbContext.FreeSql.Select<SxAnswerOption>().
                            Where(a => choiceQuestionIds.Contains(a.TopicId) && a.IsDelete == CommonConstants.IsNotDelete)
                            .ToList();

                            subQuestions.ForEach(a =>
                            {
                                var options = optionAll.Where(c => c.TopicId == a.Id).Select(d => new ChoiceOption()
                                {
                                    Id = d.Id,
                                    Name = d.Name,
                                    Text = d.AnswerOption,
                                    AttatchImgUrl = d.AnswerFileUrl,
                                    Sort = d.Sort
                                });
                                if (options.Any())
                                {
                                    a.Options = new List<ChoiceOption>();
                                    a.Options.AddRange(options);
                                }
                            });
                        }

                        questionDTO.SubQuestion = new List<SubQuestion>();
                        questionDTO.SubQuestion.AddRange(subQuestions);
                    }
                }

                return new ResponseContext<TopicInfoDto> { Code = CommonConstants.SuccessCode, Data = questionDTO };
            });
        }
        /// <summary>
        /// 获取财务报表题相关信息
        /// </summary>
        /// <param name="question"></param>
        /// <param name="questionDTO"></param>
        private static void GetFinancialStatements(SxTopic question, TopicInfoDto questionDTO)
        {

            //题目附件，包含主题和子题的所有附件
            if (!string.IsNullOrWhiteSpace(question.TopicFileIds))//文件不为空，获取文件信息
            {
                questionDTO.QuestionFile = DbContext.FreeSql.GetGuidRepository<SxTopicFile>()
                .Where(e => e.TopicId == question.Id && e.IsDelete == CommonConstants.IsNotDelete)
                .ToList<TopicFile>();//题目附件，包含主题和子题的所有附件
            }

            var fSDTopic = DbContext.FreeSql.GetRepository<SxTopic>(m => m.IsDelete == CommonConstants.IsNotDelete)
               .Where(m => m.GroupId == question.GroupId && m.QuestionType == QuestionType.FinancialStatementsDeclaration)
               .First();
            if (fSDTopic != null)
            {
                questionDTO.DeclarationScore = fSDTopic.Score;
                questionDTO.DeclarationPositionId = fSDTopic.PositionId;
                questionDTO.DeclarationPointPositionId = fSDTopic.PointPositionId;
                questionDTO.DeclarationSkillId = fSDTopic.SkillId;
            }
        }

        /// <summary>
        /// 根据申报表id获取申报表题目详情
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<TopicInfoDto>> GetTopicInfoByDeclarationId(long declarationId)
        {
            var question = await DbContext.FreeSql.Select<SxTopic>()
                  .Where(m => m.DeclarationId == declarationId && m.IsDelete == CommonConstants.IsNotDelete)
                  .FirstAsync();

            if (question == null)
            {
                return new ResponseContext<TopicInfoDto>
                {
                    Code = CommonConstants.SuccessCode,
                    Msg = "未找到相关题目",
                    Data = null,
                };
            }

            var questionDTO = question.MapTo<TopicInfoDto>();

            return new ResponseContext<TopicInfoDto>
            {
                Code = CommonConstants.SuccessCode,
                Data = questionDTO
            };
        }

        /// <summary>
        /// 查询题目列表 分页
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<TopicListPageDto>> GetTopicListPage(TopicQueryPageDto queryDto)
        {
            var result = new ResponseContext<TopicListPageDto>(CommonConstants.SuccessCode, "", null);

            if (queryDto.CaseId <= 0)
            {
                result.Msg = "CaseId无效。";
                result.Code = CommonConstants.ErrorCode;
                return result;
            }
            long totalCount = 0;
            //分页查询符合条件的数据
            var rTopicData = await DbContext.FreeSql.GetRepository<SxTopic>(e => e.IsDelete == CommonConstants.IsNotDelete && e.CaseId == queryDto.CaseId)
                .Where(e => e.ParentId == 0 && e.QuestionType != QuestionType.FinancialStatementsDeclaration)
                .WhereIf(queryDto.TaskId > 0, e => e.TaskId == queryDto.TaskId)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Title), e => e.Title.Contains(queryDto.Title))
                .WhereIf(queryDto.QuestionType.HasValue, e => e.QuestionType == queryDto.QuestionType.Value)
                .Count(out totalCount)
                .OrderBy(m => m.Sort)
                .OrderBy(m => m.Id)
                .From<SxPositionManage>((a, b) => a.LeftJoin(aa => aa.PositionId == b.Id))
                .Page(queryDto.PageIndex, queryDto.PageSize)
                .ToListAsync((a, b) => new TopicListDataDto
                {
                    Id = a.Id,
                    BusinessDate = a.BusinessDate,
                    QuestionType = a.QuestionType,
                    PositionId = a.PositionId,
                    Score = a.Score + a.DeclarationScore,
                    Status = a.Status,
                    Title = a.Title,
                    Sort = a.Sort,
                    PositionName = b.Name,
                });

            var statisticsTuple = await PageListStatistics(queryDto);//统计信息

            //构建返回值
            result.Data = new TopicListPageDto
            {
                TotalCount = totalCount,
                TotalScore = statisticsTuple.Item2,
                PositionQuestionDetail = statisticsTuple.Item1,
                Detail = new PageResponse<TopicListDataDto>
                {
                    PageSize = queryDto.PageSize,
                    PageIndex = queryDto.PageIndex,
                    RecordCount = totalCount,
                    Data = rTopicData
                }
            };

            return result;
        }
        /// <summary>
        /// 分页查询统计信息
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        private async Task<Tuple<List<TopicQueryPositionTotalScore>, decimal>> PageListStatistics(TopicQueryPageDto queryDto)
        {
            bool isam = false;//是否统计 会计主管审核分数 出纳审核分数
            //数据统计和总数统计
            var selectStatistics = DbContext.FreeSql.GetRepository<SxTopic>(e => e.IsDelete == CommonConstants.IsNotDelete && e.CaseId == queryDto.CaseId)
                .WhereIf(queryDto.TaskId > 0, e => e.TaskId == queryDto.TaskId)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Title), e => e.Title.Contains(queryDto.Title))
                .WhereIf(queryDto.BusinessStartDate.HasValue, e => e.BusinessDate >= queryDto.BusinessStartDate.Value)
                .WhereIf(queryDto.BusinessEndDate.HasValue, e => e.BusinessDate <= queryDto.BusinessEndDate.Value);

            if (queryDto.QuestionType.HasValue)//按照题目类型过滤
            {
                if (queryDto.QuestionType == QuestionType.CertificateMainSubTopic)//综合分录题 查询所有子题，岗位保存在子题中。
                {
                    List<QuestionType> questionTypes = new List<QuestionType> { QuestionType.TeticketTopic, QuestionType.ReceiptTopic, QuestionType.PayTopic, QuestionType.CertifMSub_AccountEntry };
                    selectStatistics.Where(e => questionTypes.Contains(e.QuestionType));
                    isam = true;
                }
                else
                {
                    selectStatistics.Where(e => e.QuestionType == queryDto.QuestionType.Value && e.ParentId == 0);
                }
            }
            else
            {
                selectStatistics.Where(e => e.QuestionType != QuestionType.CertificateMainSubTopic && e.QuestionType != QuestionType.MainSubQuestion && e.QuestionType != QuestionType.FinancialStatementsDeclaration);
                isam = true;
            }

            //统计每个公司岗位题目分数
            var statistics = await selectStatistics
                .GroupBy(a => a.PositionId)//按照岗位进行统计
                .ToListAsync(a => new TopicQueryPositionTotalScore
                {
                    Id = a.Key,
                    Score = a.Sum(a.Value.Score + a.Value.DeclarationScore),
                    //Count = a.Count(),
                });

            var positions = DbContext.FreeSql.GetRepository<SxCasePosition>()
                .Where(a => a.CaseId == queryDto.CaseId && a.IsDelete == CommonConstants.IsNotDelete)
                .From<SxPositionManage>((a, b) => a.LeftJoin(aa => aa.PositionId == b.Id))
                .ToList((a, b) => new TopicQueryPositionTotalScore
                {
                    Id = a.PositionId,
                    Name = b.Name,
                    //Count = a.Count(),
                });

            positions.ForEach(a =>
            {
                var p = statistics.Where(b => b.Id == a.Id).FirstOrDefault();
                if (p != null)
                    a.Score = p.Score;
            });
            decimal unknownpositionScore = 0;//未知岗位
            statistics.ForEach(a =>
            {
                if (!positions.Any(b => b.Id == a.Id))
                {
                    unknownpositionScore += a.Score;
                }
            });
            if (unknownpositionScore > 0)
                positions.Add(new TopicQueryPositionTotalScore { Name = "无岗位", Score = unknownpositionScore, Sort = -1 });

            if (isam)//统计会计主管审核分数 出纳审核分数
                await PageListStatisticsAM(queryDto, positions);

            positions = positions.OrderByDescending(a => a.Sort).ToList();

            decimal totalScore = positions.Sum(e => e.Score);//统计总分

            return new Tuple<List<TopicQueryPositionTotalScore>, decimal>(positions, totalScore);

        }
        /// <summary>
        /// 统计会计主管审核分数 出纳审核分数
        /// </summary>
        /// <param name="queryDto"></param>
        /// <param name="positions"></param>
        /// <returns></returns>
        private async Task PageListStatisticsAM(TopicQueryPageDto queryDto, List<TopicQueryPositionTotalScore> positions)
        {
            var counta = DbContext.FreeSql.GetRepository<SxTopic>(e => e.IsDelete == CommonConstants.IsNotDelete && e.CaseId == queryDto.CaseId && e.QuestionType == QuestionType.CertifMSub_AccountEntry)
                .WhereIf(queryDto.TaskId > 0, e => e.TaskId == queryDto.TaskId)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Title), e => e.Title.Contains(queryDto.Title))
                .WhereIf(queryDto.BusinessStartDate.HasValue, e => e.BusinessDate >= queryDto.BusinessStartDate.Value)
                .WhereIf(queryDto.BusinessEndDate.HasValue, e => e.BusinessDate <= queryDto.BusinessEndDate.Value)
                .ToSql(" sum(a.DisableCashierScore) as DisableCashierScore ,sum(a.DisableAMScore) as DisableAMScore ");

            var amStatistics = await DbContext.FreeSql.Ado.QueryAsync<TopicQueryAMTotalScore>(counta);

            if (amStatistics != null && amStatistics.Count > 0)
            {
                positions.Add(new TopicQueryPositionTotalScore { Name = "出纳审核", Score = amStatistics[0].DisableCashierScore });
                positions.Add(new TopicQueryPositionTotalScore { Name = "会计主管审核", Score = amStatistics[0].DisableAMScore });
            }
        }

        /// <summary>
        /// 题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<TopicListDataDto>>> GetTopicList(TopicQueryDto queryDto)
        {
            //分页查询符合条件的数据
            var selectData = DbContext.FreeSql.GetRepository<SxTopic>()
                .Where(e => e.IsDelete == CommonConstants.IsNotDelete && e.ParentId == 0)
                .WhereIf(queryDto.CaseId > 0, e => e.CaseId == queryDto.CaseId)
                .WhereIf(queryDto.TaskId > 0, e => e.TaskId == queryDto.TaskId)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Title), e => e.Title.Contains(queryDto.Title))
                .WhereIf(queryDto.QuestionType.HasValue, e => e.QuestionType == queryDto.QuestionType.Value)
                .WhereIf(queryDto.BusinessStartDate.HasValue, e => e.BusinessDate >= queryDto.BusinessStartDate.Value)
                .WhereIf(queryDto.BusinessEndDate.HasValue, e => e.BusinessDate <= queryDto.BusinessEndDate.Value)
                .OrderBy(e => e.Sort)
                .OrderBy(e => e.CreateTime);

            //分页查询符合条件的数据
            var rTopicData = await selectData
                .ToListAsync(a => new TopicListDataDto
                {
                    Id = a.Id,
                    BusinessDate = a.BusinessDate,
                    QuestionType = a.QuestionType,
                    PositionId = a.PositionId,
                    Score = a.Score,
                    Status = a.Status,
                    Title = a.Title,
                    Sort = a.Sort,
                });

            return new ResponseContext<List<TopicListDataDto>>(CommonConstants.SuccessCode, "", rTopicData);
        }

        /// <summary>
        /// 获取所有题目的名称（包含子题目）
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<TopicNamesResponseDto>>> GetAllTopicNames(TopicNamesDto queryDto)
        {
            //分页查询符合条件的数据
            var list = await DbContext.FreeSql.GetRepository<SxTopic>()
                .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(queryDto.CaseId > 0, e => e.CaseId == queryDto.CaseId)
                .WhereIf(queryDto.TaskId > 0, e => e.TaskId == queryDto.TaskId)
                .WhereIf(!string.IsNullOrWhiteSpace(queryDto.Title), e => e.Title.Contains(queryDto.Title))
                .WhereIf(queryDto.QuestionType.HasValue, e => e.QuestionType == queryDto.QuestionType.Value)
                .OrderBy(e => e.Sort)
                .OrderBy(e => e.CreateTime)
                .ToListAsync<TopicNamesResponseDto>();

            var rTopicData = list.Where(a => a.ParentId == 0).ToList();//查询父题目

            rTopicData.ForEach(a =>
            {
                a.Subs = list.Where(b => b.ParentId == a.Id).Select(b => new TopicNamesSubResponseDto
                {
                    Id = b.Id,
                    QuestionTypeName = b.QuestionType.GetDescription(),
                    ParentId = b.ParentId,
                    QuestionType = b.QuestionType,
                    Title = b.Title
                }).ToList();

            });

            return new ResponseContext<List<TopicNamesResponseDto>>(CommonConstants.SuccessCode, "", rTopicData);
        }

        #endregion

        #region 题目删除

        /// <summary>
        /// 题目删除 根据Id删除题目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTopicById(long id, UserTicket currentUser)
        {
            var r = new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };

            //校验
            //TODO-题目删除限制，临时去掉！！！！
            //var any = DbContext.FreeSql.Select<sxExamStudentGradeDetail>().Any(a => a.QuestionId == id && a.IsDelete == CommonConstants.IsNotDelete);
            //if (any)
            //{
            //    return new ResponseContext<bool>(CommonConstants.ErrorCode, "该题目已经被用户使用，无法删除！", false);
            //}

            #region 删除题目
            //取题目
            var question = await DbContext.FreeSql.Select<SxTopic>().Where(m => m.Id == id && m.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (question == null)
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到题目信息" };
            }

            switch (question.QuestionType)
            {
                case QuestionType.CertificateMainSubTopic://删除综合分录题
                    r = await DeleteCertificateMainSubTopic(question, currentUser);
                    break;
                case QuestionType.ProfitLoss:// 结转损益题
                    r = await DeleteProfitLossTopic(question, currentUser);
                    break;
                case QuestionType.FinancialStatements:
                    r = await DeleteFinancialStatementsTopic(question, currentUser);
                    break;
                case QuestionType.SingleChoice:
                case QuestionType.MultiChoice:
                case QuestionType.Judge:
                case QuestionType.FillGrid:
                case QuestionType.FillBlank:
                case QuestionType.FillGraphGrid:
                case QuestionType.MainSubQuestion:
                case QuestionType.SettleAccounts:
                case QuestionType.GridFillBank:
                default:
                    r = await DeleteTopicById(question, currentUser);
                    break;
            }

            return r;

        }
        /// <summary>
        /// 默认删除题目方法
        /// </summary>
        /// <param name="question"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> DeleteTopicById(SxTopic question, UserTicket currentUser)
        {
            long opreationId = currentUser.Id;//操作人ID
            DateTime dtNow = DateTime.Now;
            long id = question.Id;

            question.IsDelete = CommonConstants.IsDelete;
            question.UpdateBy = opreationId;
            question.UpdateTime = dtNow;
            //取选项
            var option = new List<SxAnswerOption>();
            if (question.QuestionType == QuestionType.SingleChoice || question.QuestionType == QuestionType.MultiChoice || question.QuestionType == QuestionType.Judge)
            {
                option = await DbContext.FreeSql.Select<SxAnswerOption>().Where(m => m.TopicId == id && m.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                if (option.Count == 0)
                {
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到选项信息" };
                }
                option.ForEach(a =>
                                {
                                    a.IsDelete = CommonConstants.IsDelete;
                                    a.UpdateBy = opreationId;
                                    a.UpdateTime = dtNow;
                                });
            }
            //取子题目（题目列表、题目选项、题目附件）
            var subQuestion = new List<SxTopic>();
            var subOptionList = new List<SxAnswerOption>();
            var subTopicFileList = new List<SxTopicFile>();

            //综合题
            if (question.QuestionType == QuestionType.MainSubQuestion)
            {
                subQuestion = await DbContext.FreeSql.Select<SxTopic>()
                    .Where(m => m.ParentId == id && m.IsDelete == CommonConstants.IsNotDelete)
                    .ToListAsync();

                if (subQuestion.Count == 0) { return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到子题目信息" }; }
                foreach (var itemSub in subQuestion)
                {
                    itemSub.IsDelete = CommonConstants.IsDelete;
                    itemSub.UpdateBy = opreationId;
                    itemSub.UpdateTime = dtNow;

                    //取子题目 - 选项
                    if (itemSub.QuestionType == QuestionType.SingleChoice || itemSub.QuestionType == QuestionType.MultiChoice || itemSub.QuestionType == QuestionType.Judge)
                    {
                        //简单题
                        var subOption = await DbContext.FreeSql.Select<SxAnswerOption>()
                            .Where(m => m.TopicId == itemSub.Id && m.IsDelete == CommonConstants.IsNotDelete)
                            .ToListAsync();
                        if (subOption.Count > 0) subOptionList.AddRange(subOption);
                    }
                    //取文件
                    if (!string.IsNullOrEmpty(itemSub.TopicFileIds))
                    {
                        var idArr = itemSub.TopicFileIds.Split(',').Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                        if (idArr.Any())
                        {
                            var subTopicFile = await DbContext.FreeSql.Select<SxTopicFile>()
                                .Where(m => idArr.Contains(m.Id) && m.IsDelete == CommonConstants.IsNotDelete)
                                .ToListAsync();
                            if (subTopicFile.Count > 0) subTopicFileList.AddRange(subTopicFile);
                        }
                    }
                }
                //更新字段
                if (subOptionList.Count > 0)
                {
                    subOptionList.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                }
                if (subTopicFileList.Count > 0)
                {
                    subTopicFileList.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                }
            }
            //取文件
            var topicFile = new List<SxTopicFile>();
            if (!string.IsNullOrEmpty(question.TopicFileIds))
            {
                var idArr = question.TopicFileIds.Split(',').Select(a => long.Parse(a)).Where(a => a > 0).ToArray();
                if (idArr.Any())
                {
                    topicFile = await DbContext.FreeSql.Select<SxTopicFile>().Where(m => idArr.Contains(m.Id) && m.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                }
                if (topicFile.Count > 0)
                {
                    topicFile.ForEach(a =>
                    {
                        a.IsDelete = CommonConstants.IsDelete;
                        a.UpdateBy = opreationId;
                        a.UpdateTime = dtNow;
                    });
                }
            }
            #endregion
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                var repo_t = uow.GetRepository<SxTopic>();
                var repo_ao = uow.GetRepository<SxAnswerOption>();
                var repo_tf = uow.GetRepository<SxTopicFile>();

                //删除题目
                await repo_t.UpdateDiy.SetSource(question).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                //删除选项
                if (question.QuestionType == QuestionType.SingleChoice || question.QuestionType == QuestionType.MultiChoice || question.QuestionType == QuestionType.Judge)
                {
                    await repo_ao.UpdateDiy.SetSource(option).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                }
                //删除文件
                if (topicFile.Count > 0)
                {
                    await repo_tf.UpdateDiy.SetSource(topicFile).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                }

                //删除【综合题】
                if (question.QuestionType == QuestionType.MainSubQuestion)
                {
                    //删除题目
                    await repo_t.UpdateDiy.SetSource(subQuestion).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();

                    //删除简单题·选项
                    if (subOptionList.Count > 0)
                        await repo_ao.UpdateDiy.SetSource(subOptionList).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();

                    //删除文件
                    if (subTopicFileList.Count > 0)
                        await repo_tf.UpdateDiy.SetSource(subTopicFileList).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrowsAsync();
                }
                // 修改任务题目总数
                UpdateTaskTopicCount(uow, question.TaskId, -1, currentUser);
                // 修改案例题目总分
                UpdateCaseTopicTotalScore(uow, question.CaseId, -question.Score, currentUser);

                uow.Commit();
            }
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = question.Id.ToString() };
        }
        /// <summary>
        /// 删除综合分录题
        /// </summary>
        /// <param name="mainTopic"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> DeleteCertificateMainSubTopic(SxTopic mainTopic, UserTicket currentUser)
        {
            long mainTopicId = mainTopic.Id;

            //题目关联信息
            var topics = await DbContext.FreeSql.Select<SxTopic, SxTopicRelation, SxTopic>()
                .Where((a, b, c) => b.IsDelete == CommonConstants.IsNotDelete && a.ParentId == mainTopicId)
                .InnerJoin((a, b, c) => a.Id == b.PreTopicId)
                .InnerJoin((a, b, c) => c.Id == b.TopicId)
                 .ToListAsync((a, b, c) => new SxTopic { Id = c.Id, Sort = c.Sort, Title = c.Title, QuestionType = a.QuestionType });
            if (topics.Count > 0)
            {
                string t = "";
                topics.ForEach(e =>
                {
                    t += $"{e.Sort}.{e.Title}[{e.QuestionType.GetDescription()}]<br/>";
                });

                string msg = "该题目为一下题目的前置题，请解除题目关联在删除：<br/>" + t;
                return new ResponseContext<bool>(CommonConstants.ErrorCode, msg, false);
            }

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo_t = uow.GetRepository<SxTopic>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_tf = uow.GetRepository<SxTopicFile>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_cft = uow.GetRepository<SxCertificateTopic>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_cdr = uow.GetRepository<SxCertificateDataRecord>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_caa = uow.GetGuidRepository<SxCertificateAssistAccounting>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_tr = uow.GetRepository<SxTopicRelation>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_tpai = uow.GetRepository<SxTopicPayAccountInfo>(c => c.IsDelete == CommonConstants.IsNotDelete);

                    //获取综合分录题子分录题Id
                    var cTopicId = await repo_t.Where(a => a.ParentId == mainTopicId && a.QuestionType == QuestionType.CertifMSub_AccountEntry).FirstAsync(a => a.Id);

                    await repo_t.UpdateDiy.Set(c => new SxTopic
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(c => c.Id == mainTopicId || c.ParentId == mainTopicId).ExecuteAffrowsAsync();

                    //删除题目图片
                    if (!string.IsNullOrWhiteSpace(mainTopic.TopicFileIds))
                    {
                        await repo_tf.UpdateDiy.Set(c => new SxTopicFile
                        {
                            IsDelete = CommonConstants.IsDelete,
                            UpdateTime = DateTime.Now,
                            UpdateBy = currentUser.Id,
                        }).Where(c => c.ParentTopicId == mainTopicId).ExecuteAffrowsAsync();
                    }

                    if (cTopicId > 0) //删除综合分录题子分录题
                    {
                        await repo_cft.UpdateDiy.Set(c => new SxCertificateTopic
                        {
                            IsDelete = CommonConstants.IsDelete,
                            UpdateTime = DateTime.Now,
                            UpdateBy = currentUser.Id,
                        }).Where(x => x.TopicId == cTopicId).ExecuteAffrowsAsync();
                        await repo_cdr.UpdateDiy.Set(c => new SxCertificateDataRecord
                        {
                            IsDelete = CommonConstants.IsDelete,
                            UpdateTime = DateTime.Now,
                            UpdateBy = currentUser.Id,
                        }).Where(x => x.TopicId == cTopicId).ExecuteAffrowsAsync();
                        //科目辅助核算信息
                        await repo_caa.UpdateDiy.Set(c => new SxCertificateAssistAccounting
                        {
                            IsDelete = CommonConstants.IsDelete,
                            UpdateTime = DateTime.Now,
                            UpdateBy = currentUser.Id,
                        }).Where(x => x.TopicId == cTopicId).ExecuteAffrowsAsync();

                    }

                    //删除收付款银行账户信息
                    await repo_tpai.UpdateDiy.Set(c => new SxTopicPayAccountInfo
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(x => x.ParentTopicId == mainTopicId).ExecuteAffrowsAsync();
                    await repo_tr.UpdateDiy.Set(c => new SxTopicRelation
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(x => x.TopicId == mainTopicId).ExecuteAffrowsAsync();

                    // 修改任务题目总数
                    UpdateTaskTopicCount(uow, mainTopic.TaskId, -1, currentUser);
                    // 修改案例题目总分
                    UpdateCaseTopicTotalScore(uow, mainTopic.CaseId, -mainTopic.Score, currentUser);

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = mainTopicId.ToString() };
        }

        /// <summary>
        /// 删除财务报表录题
        /// </summary>
        /// <param name="mainTopic"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> DeleteFinancialStatementsTopic(SxTopic topic, UserTicket currentUser)
        {
            long topicId = topic.Id;

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo_t = uow.GetRepository<SxTopic>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_tf = uow.GetRepository<SxTopicFile>(c => c.IsDelete == CommonConstants.IsNotDelete);

                    await repo_t.UpdateDiy.Set(c => new SxTopic
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(c => c.GroupId == topic.GroupId).ExecuteAffrowsAsync();

                    //删除题目图片
                    if (!string.IsNullOrWhiteSpace(topic.TopicFileIds))
                    {
                        await repo_tf.UpdateDiy.Set(c => new SxTopicFile
                        {
                            IsDelete = CommonConstants.IsDelete,
                            UpdateTime = DateTime.Now,
                            UpdateBy = currentUser.Id,
                        }).Where(c => c.TopicId == topicId).ExecuteAffrowsAsync();
                    }

                    // 修改任务题目总数
                    UpdateTaskTopicCount(uow, topic.TaskId, -1, currentUser);
                    // 修改案例题目总分
                    UpdateCaseTopicTotalScore(uow, topic.CaseId, -topic.Score, currentUser);

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true, Msg = topicId.ToString() };
        }

        /// <summary>
        /// 删除结转损益题
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> DeleteProfitLossTopic(SxTopic topic, UserTicket currentUser)
        {
            var topicId = topic.Id;

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo_t = uow.GetRepository<SxTopic>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_cft = uow.GetRepository<SxCertificateTopic>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_cdr = uow.GetRepository<SxCertificateDataRecord>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_caa = uow.GetGuidRepository<SxCertificateAssistAccounting>(c => c.IsDelete == CommonConstants.IsNotDelete);
                    var repo_ca = uow.GetRepository<SxCertificateAnswer>(c => c.IsDelete == CommonConstants.IsNotDelete);

                    //修改题目状态
                    await repo_t.UpdateDiy.Set(c => new SxTopic
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(c => c.Id == topicId).ExecuteAffrowsAsync();
                    //删除分录题凭证信息
                    await repo_cft.UpdateDiy.Set(c => new SxCertificateTopic
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(x => x.TopicId == topicId).ExecuteAffrowsAsync();
                    //删除录题凭证数据记录表
                    await repo_cdr.UpdateDiy.Set(c => new SxCertificateDataRecord
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(x => x.TopicId == topicId).ExecuteAffrowsAsync();
                    //科目辅助核算信息
                    await repo_caa.UpdateDiy.Set(c => new SxCertificateAssistAccounting
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(x => x.TopicId == topicId).ExecuteAffrowsAsync();
                    //删除分开结转答案
                    await repo_ca.UpdateDiy.Set(c => new SxCertificateAnswer
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id,
                    }).Where(x => x.TopicId == topicId).ExecuteAffrowsAsync();

                    // 修改任务题目总数
                    UpdateTaskTopicCount(uow, topic.TaskId, -1, currentUser);
                    // 修改案例题目总分
                    UpdateCaseTopicTotalScore(uow, topic.CaseId, -topic.Score, currentUser);

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            return new ResponseContext<bool>(CommonConstants.SuccessCode, topicId.ToString(), true);
        }

        /// <summary>
        /// 修改任务题目总数
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="taskId"></param>
        /// <param name="num">增加的题目数量（减时传负数）</param>
        /// <param name="currentUser"></param>
        private async void UpdateTaskTopicCount(FreeSql.IRepositoryUnitOfWork uow, long taskId, int num, UserTicket currentUser)
        {
            if (num == 0)
                return;
            if (uow == null)
                throw new ArgumentNullException("IRepositoryUnitOfWork uow");

            if (taskId == 0)
                return;

            if (currentUser == null)
                throw new NullReferenceException("UserTicket currentUser");

            var repo_task = uow.GetRepository<SxTask>(c => c.IsDelete == CommonConstants.IsNotDelete);

            await repo_task.UpdateDiy.Set(c => new SxTask
            {
                TopicCount = c.TopicCount + num,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(x => x.Id == taskId).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 修改公司题目总分
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="caseId"></param>
        /// <param name="score">增加的题目分数（减时传负数）</param>
        /// <param name="currentUser"></param>
        private async void UpdateCaseTopicTotalScore(FreeSql.IRepositoryUnitOfWork uow, long caseId, decimal score, UserTicket currentUser)
        {
            if (score == 0)
                return;
            if (uow == null)
                throw new ArgumentNullException("IRepositoryUnitOfWork uow");

            if (caseId == 0)
                throw new ArgumentException("long caseId 无效");

            if (currentUser == null)
                throw new NullReferenceException("UserTicket currentUser");

            var repo_task = uow.GetRepository<SxCase>(c => c.IsDelete == CommonConstants.IsNotDelete);

            await repo_task.UpdateDiy.Set(c => new SxCase
            {
                TotalScore = c.TotalScore + score,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(x => x.Id == caseId).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 修改公司题目总分
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="caseId"></param>
        /// <param name="score">增加的题目分数（减时传负数）</param>
        /// <param name="currentUser"></param>
        private async void UpdateDeclarationTopicTotalScore(FreeSql.IRepositoryUnitOfWork uow, long declarationId, long parentDeclarationId, decimal score, UserTicket currentUser)
        {
            if (score == 0)
                return;
            if (uow == null)
                throw new ArgumentNullException("IRepositoryUnitOfWork uow");

            if (declarationId == 0)
                throw new ArgumentException("long declarationId 无效");
            if (parentDeclarationId == 0)
                throw new ArgumentException("long parentDeclarationId 无效");

            if (currentUser == null)
                throw new NullReferenceException("UserTicket currentUser");

            var repo_dp = uow.GetRepository<SxDeclarationParent>(c => c.IsDelete == CommonConstants.IsNotDelete);
            var repo_dc = uow.GetRepository<SxDeclarationChild>(c => c.IsDelete == CommonConstants.IsNotDelete);

            await repo_dp.UpdateDiy.Set(c => new SxDeclarationParent
            {
                TotalScore = c.TotalScore + score,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(x => x.Id == parentDeclarationId).ExecuteAffrowsAsync();

            await repo_dc.UpdateDiy.Set(c => new SxDeclarationChild
            {
                Score = c.Score + score,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(x => x.Id == declarationId).ExecuteAffrowsAsync();
        }

        #endregion
    }
}
