﻿using NPOI.SS.Formula.Functions;
using System;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    public class TagService : ITagService
    {
        /// <summary>
        /// 根据id删除标签
        /// </summary>
        /// <param name="id">标签id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTag(long id, UserTicket currentUser)
        {
            SxTag sxTag = await DbContext.FreeSql.Select<SxTag>().Where(t => t.Id == id && t.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (sxTag == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode,"找不到标签",false);
            DbContext.FreeSql.GetRepository<SxTag>().UpdateDiy.Set(t => new SxTag
            {
                IsDelete = CommonConstants.IsDelete,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id
            }).Where(t => t.Id == id).ExecuteAffrows();
            return new ResponseContext<bool>(CommonConstants.SuccessCode,"",true);
        }

        /// <summary>
        /// 启用或禁用标签
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Toggle(ToggleInput input, UserTicket currentUser)
        {
            var sxTag = await DbContext.FreeSql.Select<SxTag>().Where(t => t.Id == input.Id && t.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if(sxTag == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到标签", false);
            DbContext.FreeSql.GetRepository<SxTag>().UpdateDiy.Set(t => new SxTag
            {
                IsActive = input.IsActive,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id
            }).Where(t => t.Id == input.Id).ExecuteAffrows();
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        /// <summary>
        /// 根据条件查询标签
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResponse<TagDto>> GetTagList(GetTagInput input)
        {
            var query = DbContext.FreeSql.Select<SxTag>()
                .Where(t => t.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Name), t => t.Name.Contains(input.Name))
                .WhereIf(input.CaseId > 0, t => DbContext.FreeSql.Select<SxCaseTag>().WhereIf(input.CaseId.HasValue, ct => ct.CaseId== input.CaseId).ToList(ct => ct.TagId).Contains(t.Id));
            var totalCount = query.Count();
            var sql = query.Page(input.PageIndex, input.PageSize).ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<TagDto>(sql);

            var result = new PageResponse<TagDto>() { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items };
            return result;
        }

        /// <summary>
        /// 保存标签
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveTag(TagDto input, UserTicket currentUser)
        {
            if (input == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "参数错误", false);
            SxTag sxTag = input.MapTo<SxTag>();
            if(input.Id == 0)
            {
                sxTag.Id = IdWorker.NextId();
                sxTag.CreateBy = currentUser.Id;
                sxTag.CreateTime = DateTime.Now;
                await DbContext.FreeSql.GetRepository<SxTag>().InsertAsync(sxTag);
            }
            else
            {
                if (!DbContext.FreeSql.Select<SxTag>().Any(t => t.Id == sxTag.Id))
                    return new ResponseContext<bool>(CommonConstants.ErrorCode,"该标签不存在！",false);
                sxTag.UpdateBy = currentUser.Id;
                sxTag.UpdateTime = DateTime.Now;
                DbContext.FreeSql.Update<SxTag>(sxTag).SetSource(sxTag).ExecuteAffrows();
            }
            return new ResponseContext<bool>(CommonConstants.SuccessCode,"",true);
        }
    }
}
