﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    /// <summary>
    /// 资源设置
    /// </summary>
    public class ResourceSettingService : IResourceSettingService
    {
        #region 行业管理
        /// <summary>
        /// 获取行业列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<IndustryDto>> GetIndustryList(IndustryRequest model)
        {
            long totalCount;
            var list = await DbContext.FreeSql.GetGuidRepository<SxIndustry>()
                 .Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                 .WhereIf(!string.IsNullOrEmpty(model.Name), x => x.Name.Contains(model.Name))
                 .WhereIf(model.Id > 0, x => x.Id == model.Id)
                 .OrderBy(x => x.Sort)
                 .OrderByDescending(x => x.CreateTime)
                 .Count(out totalCount)
                 .Page(model.PageIndex, model.PageSize)
                 .ToListAsync<IndustryDto>();

            return new PageResponse<IndustryDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = list };
        }

        /// <summary>
        /// 新增或修改行业
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditIndustry(IndustryDto model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "行业名称不能为空" };
            if (DbContext.FreeSql.Select<SxIndustry>().Any(m => m.Name == model.Name && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "行业名称重复" };

            bool state = true;
            if (model.Id == 0)
            {
                var entity = await DbContext.FreeSql.GetRepository<SxIndustry>().InsertAsync(new SxIndustry
                {
                    Id = IdWorker.NextId(),
                    Name = model.Name,
                    Sort = model.Sort,
                    Map = model.Map,
                    Xcoordinate = model.Xcoordinate,
                    Ycoordinate = model.Ycoordinate,
                    VideoUrl = model.VideoUrl,
                    Synopsis = model.Synopsis,
                    Introduce = model.Introduce,
                    Status = model.Status,
                    CreateBy = currentUserId,
                    MapLogo3D = model.MapLogo3D,
                });
                state = entity != null;
            }
            else
            {
                SxIndustry entity = await DbContext.FreeSql.GetRepository<SxIndustry>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该行业!" };

                entity.Name = model.Name;
                entity.Sort = model.Sort;
                entity.Map = model.Map;
                entity.Xcoordinate = model.Xcoordinate;
                entity.Ycoordinate = model.Ycoordinate;
                entity.VideoUrl = model.VideoUrl;
                entity.Synopsis = model.Synopsis;
                entity.Introduce = model.Introduce;
                entity.Status = model.Status;
                entity.UpdateBy = currentUserId;
                entity.UpdateTime = DateTime.Now;
                entity.MapLogo3D = model.MapLogo3D;
                var resUpd = await DbContext.FreeSql.Update<SxIndustry>().SetSource(entity).UpdateColumns(a => new
                {
                    a.Name,
                    a.Sort,
                    a.Map,
                    a.Xcoordinate,
                    a.Ycoordinate,
                    a.VideoUrl,
                    a.Synopsis,
                    a.Introduce,
                    a.UpdateBy,
                    a.UpdateTime,
                    a.Status,
                    a.MapLogo3D
                }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// 删除行业
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveIndustry(long id, long currentUserId)
        {
            SxIndustry entity = await DbContext.FreeSql.GetRepository<SxIndustry>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该行业!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<SxIndustry>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }
        #endregion

        #region 岗位管理

        /// <summary>
        /// 根据实习模式获取岗位列表（不分页）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<PositionManageDto>>> GetPositionListByPracticePattern(PositionType positionType = PositionType.composite)
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetRepository<SxPositionManage>()
                .Where(m => m.IsDelete == CommonConstants.IsNotDelete && m.PositionType == positionType)
                .Select(m => new PositionManageDto
                {
                    Id = m.Id,
                    Name = m.Name,
                    ThumbnailText = m.ThumbnailText,
                    Remark = m.Remark,
                    FileUrl = m.FileUrl
                }).ToList();

                return new ResponseContext<List<PositionManageDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
            });
        }

        /// <summary>
        /// 获取岗位列表（不分页）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<PositionManageDto>>> GetPositionList()
        {
            return await Task.Run(() =>
            {
                var selectData = DbContext.FreeSql.GetRepository<SxPositionManage>()
                    .Where(m => m.IsDelete == CommonConstants.IsNotDelete)
                    .ToList<PositionManageDto>();

                return new ResponseContext<List<PositionManageDto>> { Code = CommonConstants.SuccessCode, Data = selectData };
            });
        }
        /// <summary>
        /// 获取岗位列表（分页）
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<PositionManageDto>>> GetPositionListByPage(PositionManageRequest model)
        {
            var totalCount = 0L;
            var items = await DbContext.FreeSql.GetRepository<SxPositionManage>()
                .Where(m => m.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Name), m => m.Name.Contains(model.Name) || m.Remark.Contains(model.Name))
                .Count(out totalCount)
                .Page(model.PageIndex, model.PageSize)
                .OrderByDescending(m => m.CreateTime)
                .ToListAsync<PositionManageDto>();

            var res = new PageResponse<PositionManageDto>
            {
                PageSize = model.PageSize,
                PageIndex = model.PageIndex,
                RecordCount = totalCount,
                Data = items
            };

            return new ResponseContext<PageResponse<PositionManageDto>> { Code = CommonConstants.SuccessCode, Data = res };
        }

        /// <summary>
        /// 保存岗位
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SavePosition(PositionManageDto model, long userId)
        {
            long opreationId = userId;//操作人ID

            if (model.Id > 0)
            {
                return await Task.Run(() =>
                {
                    return UpdatePosition(model, userId);
                });
            }
            if (string.IsNullOrEmpty(model.Name))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };
            }
            if (string.IsNullOrEmpty(model.FileUrl))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "岗位图标不能为空" };
            }
            if (DbContext.FreeSql.Select<SxPositionManage>().Any(m => m.IsDelete == CommonConstants.IsNotDelete && m.Name == model.Name && m.PositionType == model.PositionType))
            {
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能重复" };
            }

            var dateNow = DateTime.Now;
            SxPositionManage position = new SxPositionManage
            {
                Id = IdWorker.NextId(),
                Name = model.Name,
                ThumbnailText = model.ThumbnailText,
                Remark = model.Remark,
                FileUrl = model.FileUrl,
                IsDelete = CommonConstants.IsNotDelete,
                CreateBy = opreationId,
                CreateTime = dateNow,
                PositionType = model.PositionType,
            };

            return await Task.Run(() =>
            {
                using (var tran = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Insert(position).ExecuteAffrows();
                    tran.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }

        /// <summary>
        /// 修改岗位
        /// </summary>
        /// <returns></returns>
        private ResponseContext<bool> UpdatePosition(PositionManageDto model, long userId)
        {
            long opreationId = userId;//操作人ID

            if (string.IsNullOrEmpty(model.Name))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能为空" };

            if (string.IsNullOrEmpty(model.FileUrl))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "岗位图标不能为空" };

            var repo = DbContext.FreeSql.GetRepository<SxPositionManage>(m => m.IsDelete == CommonConstants.IsNotDelete);
            var position = repo.Where(m => m.Id == model.Id).First();
            if (position == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "修改失败，找不到原始信息" };

            if (DbContext.FreeSql.Select<SxPositionManage>().Any(m => m.IsDelete == CommonConstants.IsNotDelete && m.Name == model.Name && m.Id != model.Id && m.PositionType == model.PositionType))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "名称不能重复" };

            //该岗位是否已被使用
            //bool isUsedPosition = DbContext.FreeSql.Select<YssxCasePosition>().Any(m => m.PostionId == model.Id);

            var dateNow = DateTime.Now;

            position.Name = model.Name;
            position.Remark = model.Remark;

            position.ThumbnailText = model.ThumbnailText;
            position.FileUrl = model.FileUrl;
            position.UpdateBy = opreationId;
            position.UpdateTime = dateNow;
            position.PositionType = model.PositionType;

            using (var tran = DbContext.FreeSql.CreateUnitOfWork())
            {
                DbContext.FreeSql.Update<SxPositionManage>().SetSource(position).UpdateColumns(m => new { m.PositionType, m.Name, m.Remark, m.ThumbnailText, m.FileUrl, m.UpdateBy, m.UpdateTime }).ExecuteAffrows();
                tran.Commit();
            }

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 删除岗位
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeletePosition(long id, long userId)
        {
            long opreationId = userId;//操作人ID

            if (id == 0)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，入参信息有误" };
            var student = DbContext.FreeSql.Select<SxPositionManage>().Where(m => m.Id == id).First();
            if (student == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，找不到原始信息" };

            //if (DbContext.FreeSql.Select<YssxCasePosition>().Any(m => m.PostionId == id && m.IsDelete == CommonConstants.IsNotDelete))
            //    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "该岗位已被使用，不允许删除" };

            student.IsDelete = CommonConstants.IsDelete;
            student.UpdateBy = opreationId;
            student.UpdateTime = DateTime.Now;

            return await Task.Run(() =>
            {
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    DbContext.FreeSql.Update<SxPositionManage>().SetSource(student).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsDelete }).ExecuteAffrows();
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
            });
        }
        #endregion

        #region 理由管理
        /// <summary>
        /// 获取理由分类列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<ReasonDto>> GetReasonList(ReasonRequest model)
        {
            return await Task.Run(() =>
            {

                var selectData = DbContext.FreeSql.GetRepository<SxReason>().Where(m => m.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Classification), x => x.Classification.Contains(model.Classification)).OrderByDescending(m => m.CreateTime);
                var totalCount = selectData.Count();
                var sql = selectData.Page(model.PageIndex, model.PageSize).ToSql("Id, Classification, IsDelete, CreateBy, CreateTime, UpdateBy, UpdateTime,(SELECT COUNT(*) FROM sx_reasonDetails WHERE a.id=TenantId AND IsDelete=0) DetailsNumber");
                var items = DbContext.FreeSql.Ado.Query<ReasonDto>(sql).Select(x => new ReasonDto
                {
                    Id = x.Id,
                    DetailsNumber = x.DetailsNumber,
                    Classification = x.Classification
                }).ToList();

                return new PageResponse<ReasonDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };

            });


        }

        /// <summary>
        /// 新增或修改理由分类
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditReason(ReasonAddOrEditDto model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.Classification))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "理由分类不能为空" };
            if (DbContext.FreeSql.Select<SxReason>().Any(m => m.Classification == model.Classification && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "理由分类名称重复" };

            bool state = true;
            if (model.Id == 0)
            {
                var entity = await DbContext.FreeSql.GetRepository<SxReason>().InsertAsync(new SxReason
                {
                    Id = IdWorker.NextId(),
                    Classification = model.Classification,
                    CreateBy = currentUserId
                });
                state = entity != null;
            }
            else
            {
                SxReason entity = await DbContext.FreeSql.GetRepository<SxReason>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该理由分类!" };
                entity.Classification = model.Classification;
                entity.UpdateBy = currentUserId;
                entity.UpdateTime = DateTime.Now;
                var resUpd = await DbContext.FreeSql.Update<SxReason>().SetSource(entity).UpdateColumns(a => new { a.Classification, a.UpdateBy, a.UpdateTime }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// 删除理由分类
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveReason(long id, long currentUserId)
        {
            SxReason entity = await DbContext.FreeSql.GetRepository<SxReason>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该理由分类!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<SxReason>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;

            //删除对应理由详情
            var items = DbContext.FreeSql.Ado.Query<SxReasonDetails>($"SELECT * FROM sx_ReasonDetails WHERE TenantId={id}");
            var boo = DbContext.FreeSql.Update<SxReasonDetails>().SetSource(items).Set(x => x.IsDelete, CommonConstants.IsDelete).Set(x => x.UpdateBy, currentUserId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows() > 0;//var  e = DbContext.FreeSql.GetRepository<SxReasonDetails>().UpdateDiy.SetSource(items).Set(a => a.UpdateTime, DateTime.Now).ExecuteAffrows()>0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }

        #region >>>理由详情
        /// <summary>
        /// 获取理由详情列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<ReasonDetailsDto>> GetReasonDetailsList(ReasonDetailsRequest model)
        {
            List<SxReasonDetails> list = await DbContext.FreeSql.GetGuidRepository<SxReasonDetails>().Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Details), x => x.Details.Contains(model.Details))
                .WhereIf(model.TenantId > 0, x => x.TenantId == model.TenantId)
                .OrderByDescending(x => x.CreateTime).ToListAsync();
            var totalCount = list.Count();
            var selectData = list.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).Select(x => new ReasonDetailsDto
            {
                Id = x.Id,
                TenantId = x.TenantId,
                Details = x.Details

            }).ToList();
            return new PageResponse<ReasonDetailsDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
        }

        /// <summary>
        /// 新增或修改理由详情
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditReasonDetails(ReasonDetailsDto model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.Details))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "理由详情不能为空" };
            if (DbContext.FreeSql.Select<SxReasonDetails>().Any(m => m.Details == model.Details && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "理由详情名称重复" };

            bool state = true;
            if (model.Id == 0)
            {
                var entity = await DbContext.FreeSql.GetRepository<SxReasonDetails>().InsertAsync(new SxReasonDetails
                {
                    Id = IdWorker.NextId(),
                    TenantId = model.TenantId,
                    Details = model.Details,
                    CreateBy = currentUserId
                });
                state = entity != null;
            }
            else
            {
                SxReasonDetails entity = await DbContext.FreeSql.GetRepository<SxReasonDetails>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该理由详情!" };
                entity.TenantId = model.TenantId;
                entity.Details = model.Details;
                entity.UpdateBy = currentUserId;
                entity.UpdateTime = DateTime.Now;
                var resUpd = await DbContext.FreeSql.Update<SxReasonDetails>().SetSource(entity).UpdateColumns(a => new { a.TenantId, a.Details, a.UpdateBy, a.UpdateTime }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// </summary>
        /// 删除理由详情
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveReasonDetails(long id, long currentUserId)
        {
            SxReasonDetails entity = await DbContext.FreeSql.GetRepository<SxReasonDetails>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该理由详情!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<SxReasonDetails>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }
        #endregion

        /// <summary>
        /// </summary>
        /// 根据Id获取理由详情
        /// <returns></returns>
        public async Task<ResponseContext<ReasonDetailsDto>> GetReasonDetailsById(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<SxReasonDetails>()
                .Where(x => x.Id == id)
                .FirstAsync<ReasonDetailsDto>();

            return new ResponseContext<ReasonDetailsDto> { Code = CommonConstants.SuccessCode, Data = entity };
        }

        #endregion

        #region 师傅评语管理
        /// <summary>
        /// 获取师傅评语列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<MastersCommentsDto>> GetMastersCommentsList(MastersCommentsRequest model)
        {
            List<SxMastersComments> list = await DbContext.FreeSql.GetGuidRepository<SxMastersComments>().Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Details), x => x.Details.Contains(model.Details)).OrderByDescending(x => x.CreateTime).ToListAsync();
            var totalCount = list.Count();
            var selectData = list.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).Select(x => new MastersCommentsDto
            {
                Id = x.Id,
                Details = x.Details,
                MaxScore = x.MaxScore,
                MinScore = x.MinScore,
            }).ToList();
            return new PageResponse<MastersCommentsDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
        }

        /// <summary>
        /// 新增或修改师傅评语
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditMastersComments(MastersCommentsDto model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.Details))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "师傅评语不能为空" };
            if (DbContext.FreeSql.Select<SxMastersComments>().Any(m => m.Details == model.Details && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "师傅评语名称重复" };

            bool state = true;
            if (model.Id == 0)
            {
                var entity = await DbContext.FreeSql.GetRepository<SxMastersComments>().InsertAsync(new SxMastersComments
                {
                    Id = IdWorker.NextId(),
                    Details = model.Details,
                    MaxScore = model.MaxScore,
                    MinScore = model.MinScore,
                    CreateBy = currentUserId
                });
                state = entity != null;
            }
            else
            {
                SxMastersComments entity = await DbContext.FreeSql.GetRepository<SxMastersComments>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该师傅评语!" };
                entity.Details = model.Details;
                entity.MaxScore = model.MaxScore;
                entity.MinScore = model.MinScore;
                entity.UpdateBy = currentUserId;
                entity.UpdateTime = DateTime.Now;
                var resUpd = await DbContext.FreeSql.Update<SxMastersComments>().SetSource(entity).UpdateColumns(a => new { a.Details, a.MinScore, a.MaxScore, a.UpdateBy, a.UpdateTime }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// 删除师傅评语
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveMastersComments(long id, long currentUserId)
        {
            SxMastersComments entity = await DbContext.FreeSql.GetRepository<SxMastersComments>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该师傅评语!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<SxMastersComments>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }
        #endregion

        #region 突发事件管理
        /// <summary>
        /// 获取突发事件列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<EmergencyDto>> GetEmergencyList(EmergencyRequest model)
        {
            return await Task.Run(() =>
            {

                var selectData = DbContext.FreeSql.GetRepository<SxEmergency>().Where(m => m.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Classification), x => x.Classification.Contains(model.Classification)).OrderByDescending(m => m.CreateTime);
                var totalCount = selectData.Count();
                var sql = selectData.Page(model.PageIndex, model.PageSize).ToSql("Id, Classification,CategoryMark,Status,IsDelete, CreateBy, CreateTime, UpdateBy, UpdateTime,(SELECT COUNT(*) FROM sx_emergencydetails WHERE a.id=TenantId AND IsDelete=0) DetailsNumber");
                var items = DbContext.FreeSql.Ado.Query<EmergencyDto>(sql).Select(x => new EmergencyDto
                {
                    Id = x.Id,
                    DetailsNumber = x.DetailsNumber,
                    Classification = x.Classification,
                    CategoryMark = x.CategoryMark,
                    Status = x.Status
                }).ToList();

                return new PageResponse<EmergencyDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = items };

            });
        }

        /// <summary>
        /// 新增或修改突发事件
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditEmergency(EmergencyAddOrEditDto model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.Classification))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "突发类别不能为空" };
            if (DbContext.FreeSql.Select<SxEmergency>().Any(m => m.Classification == model.Classification && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "突发类别名称重复" };

            bool state = true;
            if (model.Id == 0)
            {
                var entity = await DbContext.FreeSql.GetRepository<SxEmergency>().InsertAsync(new SxEmergency
                {
                    Id = IdWorker.NextId(),
                    Classification = model.Classification,
                    CategoryMark = 0,
                    Status = model.Status,
                    CreateBy = currentUserId
                });
                state = entity != null;
            }
            else
            {
                SxEmergency entity = await DbContext.FreeSql.GetRepository<SxEmergency>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该突发类别!" };
                entity.Classification = model.Classification;
                entity.Status = model.Status;
                entity.UpdateBy = currentUserId;
                entity.UpdateTime = DateTime.Now;
                var resUpd = await DbContext.FreeSql.Update<SxEmergency>().SetSource(entity).UpdateColumns(a => new { a.Classification, a.Status, a.UpdateBy, a.UpdateTime }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// 删除突发事件
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveEmergency(long id, long currentUserId)
        {
            SxEmergency entity = await DbContext.FreeSql.GetRepository<SxEmergency>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该突发类别!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<SxEmergency>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;

            //删除对应突发事件详情
            var items = DbContext.FreeSql.Ado.Query<SxEmergencyDetails>($"SELECT * FROM sx_emergencydetails WHERE TenantId={id}");
            var boo = DbContext.FreeSql.Update<SxEmergencyDetails>().SetSource(items).Set(x => x.IsDelete, CommonConstants.IsDelete).Set(x => x.UpdateBy, currentUserId).Set(x => x.UpdateTime, DateTime.Now).ExecuteAffrows() > 0;

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }

        #region >>>突发事件详情
        /// <summary>
        /// 获取突发事件详情列表
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<EmergencyDetailsDto>> GetEmergencyDetailsList(EmergencyDetailsRequest model)
        {
            List<SxEmergencyDetails> list = await DbContext.FreeSql.GetGuidRepository<SxEmergencyDetails>().Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrEmpty(model.Details), x => x.Details.Contains(model.Details))
                .WhereIf(model.TenantId > 0, x => x.TenantId == model.TenantId)
                .OrderByDescending(x => x.CreateTime).ToListAsync();
            var totalCount = list.Count();
            var selectData = list.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).Select(x => new EmergencyDetailsDto
            {
                Id = x.Id,
                TenantId = x.TenantId,
                Details = x.Details,
                ImgUrl = x.ImgUrl,
                Status = x.Status

            }).ToList();
            return new PageResponse<EmergencyDetailsDto> { Code = CommonConstants.SuccessCode, PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = selectData };
        }

        /// <summary>
        /// 新增或修改突发事件详情
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddOrEditEmergencyDetails(EmergencyDetailsDto model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.Details))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "突发详情不能为空" };
            if (DbContext.FreeSql.Select<SxEmergencyDetails>().Any(m => m.Details == model.Details && m.Id != model.Id && m.IsDelete == CommonConstants.IsNotDelete))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "突发详情名称重复" };

            bool state = true;
            if (model.Id == 0)
            {
                var entity = await DbContext.FreeSql.GetRepository<SxEmergencyDetails>().InsertAsync(new SxEmergencyDetails
                {
                    Id = IdWorker.NextId(),
                    TenantId = model.TenantId,
                    Details = model.Details,
                    ImgUrl = model.ImgUrl,
                    Status = model.Status,
                    CreateBy = currentUserId
                });
                state = entity != null;
            }
            else
            {
                SxEmergencyDetails entity = await DbContext.FreeSql.GetRepository<SxEmergencyDetails>().Where(x => x.Id == model.Id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
                if (entity == null)
                    return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "操作失败，未找到该突发详情!" };
                entity.TenantId = model.TenantId;
                entity.Details = model.Details;
                entity.ImgUrl = model.ImgUrl;
                entity.Status = model.Status;
                entity.UpdateBy = currentUserId;
                entity.UpdateTime = DateTime.Now;
                var resUpd = await DbContext.FreeSql.Update<SxEmergencyDetails>().SetSource(entity).UpdateColumns(a => new { a.TenantId, a.Details, a.ImgUrl, a.Status, a.UpdateBy, a.UpdateTime }).ExecuteAffrowsAsync();
                state = resUpd > 0;
            }
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// </summary>
        /// 删除突发事件详情
        /// <returns></returns>
        public async Task<ResponseContext<bool>> RemoveEmergencyDetails(long id, long currentUserId)
        {
            SxEmergencyDetails entity = await DbContext.FreeSql.GetRepository<SxEmergencyDetails>().Where(x => x.Id == id && x.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "删除失败，未找到该突发详情!" };
            entity.IsDelete = CommonConstants.IsDelete;
            entity.UpdateBy = currentUserId;
            entity.UpdateTime = DateTime.Now;

            bool state = DbContext.FreeSql.GetRepository<SxEmergencyDetails>().UpdateDiy.SetSource(entity).UpdateColumns(x => new { x.IsDelete, x.UpdateBy, x.UpdateTime }).ExecuteAffrows() > 0;
            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功" : "删除失败!" };
        }
        #endregion
        #endregion

        #region 功能提示管理

        #endregion

        #region 开票管理（暂不做）

        #endregion

        #region 致用户信
        /// <summary>
        /// 新增致用户信
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddLetterToUsers(LetterToUsersDto model, long currentUserId)
        {
            //验证
            if (string.IsNullOrEmpty(model.Content))
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Msg = "致用户信内容不能为空" };
            //添加数据
            var entity = await DbContext.FreeSql.GetRepository<SxLetterToUsers>().InsertAsync(new SxLetterToUsers
            {
                Id = IdWorker.NextId(),
                Content = model.Content,
                CreateBy = currentUserId,
                CreateTime = DateTime.Now
            });
            var state = entity != null;
            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = state, Msg = state ? "成功" : "操作失败!" };
        }

        /// <summary>
        /// 获取最新致用户信
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<LetterToUsersDto>> GetLetterToUsers()
        {
            var selectData = await DbContext.FreeSql.GetGuidRepository<SxLetterToUsers>()
                .Where(x => x.IsDelete == CommonConstants.IsNotDelete)
                .OrderByDescending(x => x.CreateTime)
                .FirstAsync(x => new LetterToUsersDto { Content = x.Content });

            return new ResponseContext<LetterToUsersDto> { Code = CommonConstants.SuccessCode, Data = selectData, Msg = "成功" };
        }
        #endregion
    }
}
