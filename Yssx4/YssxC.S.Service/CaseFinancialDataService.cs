using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using System.Linq;

namespace YssxC.S.Service
{
    /// <summary>
    /// 案例财务数据服务
    /// </summary>
    public class CaseFinancialDataService : ICaseFinancialDataService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> SaveCaseFinancialData(CaseFinancialDataDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<SxCaseFinancialData>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasName = await repo
                         .Where(e => e.Name == model.Name)
                         .AnyAsync();

            if (hasName)
            {
                result.Msg = $"案例财务数据名称重复。";
                return result;
            }

            #endregion

            var obj = model.MapTo<SxCaseFinancialData>();
            obj.Id = IdWorker.NextId();//获取唯一Id
            obj.CreateBy = currentUserId;

            #region 保存到数据库中

            await repo.InsertAsync(obj);

            #endregion

            result.SetSuccess(true, obj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 案例财务数据 修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> UpdateCaseFinancialData(CaseFinancialDataDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<SxCaseFinancialData>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasobj = await repo
                   .Where(e => e.Id == model.Id)
                   .AnyAsync();

            if (!hasobj)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"案例财务数据不存在，Id:{model.Id}", false);

            var hasName = await repo
                         .Where(e => e.Name == model.Name && e.Id != model.Id)
                         .AnyAsync();

            if (hasName)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"案例财务数据名称重复。", false);

            #endregion
            //赋值
            var updateObj = model.MapTo<SxCaseFinancialData>();
            updateObj.UpdateBy = currentUserId;
            updateObj.UpdateTime = DateTime.Now;

            #region 保存到数据库中

            await repo.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

            //await repo.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_f = uow.GetRepository<SxCaseFinancialData>();

            //        await repo_f.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

            //        //await repo_f.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}
            #endregion

            result.SetSuccess(true, updateObj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> DeleteCaseFinancialData(long currentUserId, params long[] id)
        {
            //删除
            await DbContext.FreeSql.GetRepository<SxCaseFinancialData>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(c => new SxCaseFinancialData
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUserId,
                }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            return new ResponseContext<bool>(true);
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<CaseFinancialDataListResponseDto>> GetCaseFinancialDataList(long caseId)
        {
            var entitys = await DbContext.FreeSql.GetRepository<SxCaseFinancialData>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .Where(e => e.CaseId == caseId)
                   .OrderByDescending(e => e.Sort)
                   .OrderByDescending(e => e.Id)
                   .ToListAsync<CaseFinancialDataListResponseDto>();

            return new ListResponse<CaseFinancialDataListResponseDto>(entitys);
        }
    }
}