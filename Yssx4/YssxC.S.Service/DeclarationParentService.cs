﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Utils;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    public class DeclarationParentService : IDeclarationParentService
    {
        /// <summary>
        /// 修改主表信息
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Update(UpdateDeclarationParentInput input, UserTicket currentUser)
        {
            SxDeclarationParent sxDeclarationParent = await DbContext.FreeSql.Select<SxDeclarationParent>().Where(d => d.Id == input.Id && d.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (sxDeclarationParent != null)
            {
                sxDeclarationParent.StartDate = input.StartDate;
                sxDeclarationParent.EndDate = input.EndDate;
                sxDeclarationParent.DeclarationDate = input.DeclarationDate;
                sxDeclarationParent.CreateBy = currentUser.Id;
                sxDeclarationParent.CreateTime = DateTime.Now;
                DbContext.FreeSql.Update<SxDeclarationParent>(sxDeclarationParent).SetSource(sxDeclarationParent).ExecuteAffrows();
                return new ResponseContext<bool>(CommonConstants.SuccessCode,"",true);
            }
            return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到该申报主表！", false);
        }

        /// <summary>
        /// 删除申报表主表
        /// </summary>
        /// <param name="id">申报表主表id</param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Delete(long id, UserTicket currentUser)
        {
            SxDeclarationParent sxDeclarationParent = await DbContext.FreeSql.Select<SxDeclarationParent>().Where(d => d.Id == id && d.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if(sxDeclarationParent == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到该申报主表！", false);

            //查询案例Id
            var caseid = await DbContext.FreeSql.Select<SxTask>().Where(d => d.Id == sxDeclarationParent.TaskId && d.IsDelete == CommonConstants.IsNotDelete).FirstAsync(e=>e.CaseId);

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    DbContext.FreeSql.GetRepository<SxDeclarationParent>().UpdateDiy.Set(d => new SxDeclarationParent
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id
                    }).Where(d => d.Id == id).ExecuteAffrows();
                    DbContext.FreeSql.GetRepository<SxDeclarationChild>().UpdateDiy.Set(d => new SxDeclarationChild
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id
                    }).Where(d => d.IsDelete == CommonConstants.IsNotDelete && d.ParentId == id).ExecuteAffrows();
                    //删除对应的题目
                    DbContext.FreeSql.GetRepository<SxTopic>().UpdateDiy.Set(d => new SxTopic
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id
                    }).Where(d => d.IsDelete == CommonConstants.IsNotDelete && d.ParentDeclarationId == id).ExecuteAffrows();

                    // 修改任务题目总数
                    UpdateTaskTopicCount(uow, sxDeclarationParent.TaskId, -sxDeclarationParent.Count, currentUser);
                    
                    // 修改案例题目总分
                    UpdateCaseTopicTotalScore(uow, caseid, -sxDeclarationParent.TotalScore, currentUser);

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "删除时异常", false);
                }
            }
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        /// <summary>
        /// 修改任务题目总数
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="taskId"></param>
        /// <param name="num">增加的题目数量（减时传负数）</param>
        /// <param name="currentUser"></param>
        private async void UpdateTaskTopicCount(FreeSql.IRepositoryUnitOfWork uow, long taskId, int num, UserTicket currentUser)
        {
            if (num == 0)
                return;
            if (uow == null)
                throw new ArgumentNullException("IRepositoryUnitOfWork uow");

            if (taskId == 0)
                return;

            if (currentUser == null)
                throw new NullReferenceException("UserTicket currentUser");

            var repo_task = uow.GetRepository<SxTask>(c => c.IsDelete == CommonConstants.IsNotDelete);

            await repo_task.UpdateDiy.Set(c => new SxTask
            {
                TopicCount = c.TopicCount + num,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(x => x.Id == taskId).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 修改公司题目总分
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="caseId"></param>
        /// <param name="score">增加的题目分数（减时传负数）</param>
        /// <param name="currentUser"></param>
        private async void UpdateCaseTopicTotalScore(FreeSql.IRepositoryUnitOfWork uow, long caseId, decimal score, UserTicket currentUser)
        {
            if (score == 0)
                return;
            if (uow == null)
                throw new ArgumentNullException("IRepositoryUnitOfWork uow");

            if (caseId == 0)
                throw new ArgumentException("long caseId 无效");

            if (currentUser == null)
                throw new NullReferenceException("UserTicket currentUser");

            var repo_task = uow.GetRepository<SxCase>(c => c.IsDelete == CommonConstants.IsNotDelete);

            await repo_task.UpdateDiy.Set(c => new SxCase
            {
                TotalScore = c.TotalScore + score,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(x => x.Id == caseId).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 根据id获取申报表主表
        /// </summary>
        /// <param name="id">申报表主表id</param>
        /// <returns></returns>
        public async Task<ResponseContext<DeclarationParentDto>> GetById(long id)
        {
            var query = DbContext.FreeSql.Select<SxDeclarationParent>().Where(d => d.Id == id && d.IsDelete == CommonConstants.IsNotDelete);
            var declarationParentDtos = await DbContext.FreeSql.Ado.QueryAsync<DeclarationParentDto>(query.ToSql("*"));
            if(declarationParentDtos != null && declarationParentDtos.Count > 0)
            {
                var declarationParentDto = declarationParentDtos[0];
                var select = DbContext.FreeSql.Select<SxDeclarationChild>().Where(d => d.ParentId == id && d.IsDelete == CommonConstants.IsNotDelete);
                var declarationChildDtos = await DbContext.FreeSql.Ado.QueryAsync<DeclarationChildDto>(select.ToSql("*"));
                declarationParentDto.Children = declarationChildDtos;
                return new ResponseContext<DeclarationParentDto>(CommonConstants.SuccessCode,"", declarationParentDto);
            }
            return new ResponseContext<DeclarationParentDto>(CommonConstants.SuccessCode, "没有找到申报表主表！", null);
        }

        /// <summary>
        /// 根据条件获取申报表主表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<DeclarationParentDto>>> GetList(GetDeclarationParentInput input)
        {
            var query = DbContext.FreeSql.Select<SxDeclarationParent>().Where(d => d.TaskId == input.TaskId && d.IsDelete == CommonConstants.IsNotDelete).OrderBy(d => d.Sort);
            var declarationParentDtos = await DbContext.FreeSql.Ado.QueryAsync<DeclarationParentDto>(query.ToSql("*"));
            if (declarationParentDtos != null && declarationParentDtos.Count > 0)
            {
                //申报主表Ids
                List<long> declareIds = declarationParentDtos.Select(x => x.Id).ToList();
                //查询申报记录表
                List<SxDeclarationRecord> records = await DbContext.FreeSql.GetRepository<SxDeclarationRecord>().Where(x => x.GradeId == input.GradeId
                     && declareIds.Contains(x.DeclareParentId) && x.IsDelete == CommonConstants.IsNotDelete).ToListAsync();
                declarationParentDtos.ForEach(x =>
                {
                    SxDeclarationRecord model = records.FirstOrDefault(a => a.DeclareParentId == x.Id);

                    //未填写
                    if (model == null)
                        x.DeclareStatus = (int)DeclareStatus.NotFilled;
                    else
                        x.DeclareStatus = model.DeclareStatus;
                });

                return new ResponseContext<List<DeclarationParentDto>>(CommonConstants.SuccessCode, "", declarationParentDtos);
            }
            return new ResponseContext<List<DeclarationParentDto>>(CommonConstants.SuccessCode, "没有找到申报表主表!", null);
        }

        /// <summary>
        /// 保存申报表
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Save(CreateDeclarationParentInput[] input, UserTicket currentUser)
        {
            if(input == null || input.Length == 0)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "参数错误！", false);
            // 查询主表的模板
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    foreach (var item in input)
                    {
                        var parentTemplate = DbContext.FreeSql.Select<SxDeclarationTemplateParent>().Where(d => d.Id == item.ParentTemplateId).First();

                        if (parentTemplate != null)
                        {
                            var ids = item.Children.Select(c => c.ChildTemplateId).ToArray();
                            var childTemplates = DbContext.FreeSql.Select<SxDeclarationTemplateChild>().Where(d => d.DeclarationTypeTemplateId == parentTemplate.Id).Where(d => ids.Contains(d.Id)).ToList();
                            SxDeclarationParent parent = new SxDeclarationParent();
                            parent.Id = IdWorker.NextId();
                            parent.Name = parentTemplate.Name;
                            parent.Content = parentTemplate.Content;
                            parent.TaskId = item.TaskId;
                            parent.Count = 0;
                            parent.CreateBy = currentUser.Id;
                            parent.CreateTime = DateTime.Now;
                            List<SxDeclarationChild> children = new List<SxDeclarationChild>();
                            if (childTemplates != null && childTemplates.Count > 0)
                            {
                                parent.Count = childTemplates.Count;
                                childTemplates.ForEach(c =>
                                {
                                    SxDeclarationChild child = new SxDeclarationChild();
                                    child.Id = IdWorker.NextId();
                                    child.Name = c.Name;
                                    child.Content = c.Content;
                                    child.ParentId = parent.Id;
                                    child.CreateBy = currentUser.Id;
                                    child.CreateTime = DateTime.Now;
                                    children.Add(child);
                                });
                                await DbContext.FreeSql.GetRepository<SxDeclarationChild>().InsertAsync(children);
                            }
                            await DbContext.FreeSql.GetRepository<SxDeclarationParent>().InsertAsync(parent);
                        }
                    }

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            
            return new ResponseContext<bool>(CommonConstants.SuccessCode,"",true);
        }
    }
}
