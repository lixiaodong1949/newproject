using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using System.Linq;
using Yssx.Repository.Extensions;

namespace YssxC.S.Service
{
    /// <summary>
    /// 游戏场景服务
    /// </summary>
    public class GameSceneService : IGameSceneService
    {
        #region 后台

        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Save(GameSceneDto model, long currentUserId)
        {
            if (model.Id <= 0)//新增
            {
                return await Add(model, currentUserId);
            }
            else //修改
            {
                return await Update(model, currentUserId);
            }
        }

        /// <summary>
        /// 游戏场景 新增
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Add(GameSceneDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<SxGameScene>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasName = await repo
                         .Where(e => e.Name == model.Name)
                         .AnyAsync();

            if (hasName)
            {
                result.Msg = $"游戏场景名称重复。";
                return result;
            }

            #endregion

            var obj = model.MapTo<SxGameScene>();
            obj.Id = IdWorker.NextId();//获取唯一Id
            obj.CreateBy = currentUserId;

            #region 保存到数据库中

            await repo.InsertAsync(obj);

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_f = uow.GetRepository<SxGameScene>();

            //        await repo_f.InsertAsync(obj);//添加到数据库

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}
            #endregion

            result.SetSuccess(true, obj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 游戏场景 修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Update(GameSceneDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<SxGameScene>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasobj = await repo
                   .Where(e => e.Id == model.Id)
                   .AnyAsync();

            if (!hasobj)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"游戏场景不存在，Id:{model.Id}", false);

            var hasName = await repo
                         .Where(e => e.Name == model.Name && e.Id != model.Id)
                         .AnyAsync();

            if (hasName)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"游戏场景名称重复。", false);

            #endregion
            //赋值
            var updateObj = model.MapTo<SxGameScene>();
            updateObj.UpdateBy = currentUserId;
            updateObj.UpdateTime = DateTime.Now;

            #region 保存到数据库中

            await repo.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

            //await repo.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_f = uow.GetRepository<SxGameScene>();

            //        await repo_f.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

            //        //await repo_f.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}
            #endregion

            result.SetSuccess(true, updateObj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 添加场景会话信息
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> AddGameSceneDialogues(GameSceneDialoguesDto query)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<SxGameScene>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasobj = await repo
                   .Where(e => e.Id == query.Id)
                   .AnyAsync();

            if (!hasobj)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"游戏场景不存在，Id:{query.Id}", false);

            await DbContext.FreeSql.GetRepository<SxGameScene>(e => e.IsDelete == CommonConstants.IsNotDelete)
            .UpdateDiy.Set(c => new SxGameScene
            {
                Dialogues = query.Dialogues,
                UpdateTime = DateTime.Now,
                //UpdateBy = currentUserId,
            }).Where(e => e.Id == query.Id).ExecuteAffrowsAsync();

            #endregion

            result.SetSuccess(true);//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<GameSceneDto>> Get(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<SxGameScene>(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .Where(e => e.Id == id)
                      .FirstAsync<GameSceneDto>();

            return new ResponseContext<GameSceneDto>(entity);
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Delete(long currentUserId, params long[] id)
        {
            //删除
            await DbContext.FreeSql.GetRepository<SxGameScene>(e => e.IsDelete == CommonConstants.IsNotDelete)
                .UpdateDiy.Set(c => new SxGameScene
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUserId,
                }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_t = uow.GetRepository<SxGameScene>(e => e.IsDelete == CommonConstants.IsNotDelete);
            //        //删除
            //        await repo_t.UpdateDiy.Set(c => new SxGameScene
            //        {
            //            IsDelete = CommonConstants.IsDelete,
            //            UpdateTime = DateTime.Now,
            //            UpdateBy = currentUserId,
            //        }).Where(e => id.Contains(e.Id)).ExecuteAffrowsAsync();

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}

            return new ResponseContext<bool>(true);
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<GameScenePageResponseDto>> ListPage(GameScenePageRequestDto query)
        {
            long count = 0;
            var entitys = await DbContext.FreeSql.GetRepository<SxGameScene>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderByDescending(e => e.CreateTime)
                   .Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .ToListAsync<GameScenePageResponseDto>();

            return new PageResponse<GameScenePageResponseDto>(query.PageIndex, query.PageSize, count, entitys);
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<GameSceneListResponseDto>> List(GameSceneListRequestDto query)
        {
            var entitys = await DbContext.FreeSql.GetRepository<SxGameScene>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderByDescending(e => e.CreateTime)
                   .ToListAsync<GameSceneListResponseDto>();

            return new ListResponse<GameSceneListResponseDto>(entitys);
        }

        #endregion

        #region 前台

        public async Task<ListResponse<GameScenesResponseDto>> GetGameSceneList()
        {
            var entitys = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetGameSceneList_C3D}",
                () => DbContext.FreeSql.GetRepository<SxGameScene>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                   .OrderByDescending(e => e.Sort)
                   .ToList<GameScenesResponseDto>(), 10 * 60, false, false);

            //var entitys = await DbContext.FreeSql.GetRepository<SxGameScene>()
            //       .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
            //       .OrderByDescending(e => e.Sort)
            //       .ToListAsync<GameScenesResponseDto>();

            return new ListResponse<GameScenesResponseDto>(entitys);
        }

        public async Task<ResponseContext<GameSceneBaseDto>> GetGameScene(long id)
        {
            var entity = FreeSqlCacheExtension.GetCache($"{CommonConstants.Cache_GetGameSceneList_C3D}:{id}",
                () => DbContext.FreeSql.GetRepository<SxGameScene>(e => e.IsDelete == CommonConstants.IsNotDelete)
                       .Where(e => e.Id == id)
                       .First<GameSceneBaseDto>(), 10 * 60, true, false);

            //var entity = await DbContext.FreeSql.GetRepository<SxGameScene>(e => e.IsDelete == CommonConstants.IsNotDelete)
            //           .Where(e => e.Id == id)
            //           .FirstAsync<GameSceneBaseDto>();

            return new ResponseContext<GameSceneBaseDto>(entity);
        }

        #endregion


    }
}