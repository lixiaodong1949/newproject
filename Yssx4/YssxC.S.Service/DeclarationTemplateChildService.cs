﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Dal;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    public class DeclarationTemplateChildService : IDeclarationTemplateChildService
    {
        /// <summary>
        /// 根据条件查询申报表子表模板
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<DeclarationTemplateChildDto>>> GetList(GetDeclarationTemplateInput  input)
        {
            var query = DbContext.FreeSql.Select<SxDeclarationTemplateChild>().Where( d => d.DeclarationTypeTemplateId == input.DeclarationTypeTemplateId);
            var items = await DbContext.FreeSql.Ado.QueryAsync<DeclarationTemplateChildDto>(query.ToSql("*"));
            return new ResponseContext<List<DeclarationTemplateChildDto>>(CommonConstants.SuccessCode,"",items);
        }
    }
}
