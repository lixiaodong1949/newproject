﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    public class DeclarationChildService : IDeclarationChildService
    {
        /// <summary>
        /// 根据id删除申报子表
        /// </summary>
        /// <param name="id">子表id</param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Delete(long id, UserTicket currentUser)
        {
            SxDeclarationChild sxDeclarationChild = await DbContext.FreeSql.Select<SxDeclarationChild>().Where(d => d.Id == id && d.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (sxDeclarationChild == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到该子表", false);

            var sxDeclarationParent = await DbContext.FreeSql.GetRepository<SxDeclarationParent>()
                         .Where(d => d.Id == sxDeclarationChild.ParentId)
                         .FirstAsync();

            //查询案例Id
            var caseid = await DbContext.FreeSql.Select<SxTask>()
                .Where(d => d.Id == sxDeclarationParent.TaskId && d.IsDelete == CommonConstants.IsNotDelete)
                .FirstAsync(e => e.CaseId);

            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    DbContext.FreeSql.GetRepository<SxDeclarationChild>().UpdateDiy.Set(d => new SxDeclarationChild
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id
                    }).Where(d => d.Id == id).ExecuteAffrows();

                    sxDeclarationParent.Count -= 1;
                    sxDeclarationParent.UpdateBy = currentUser.Id;
                    sxDeclarationParent.UpdateTime = DateTime.Now;
                    DbContext.FreeSql.Update<SxDeclarationParent>(sxDeclarationParent).SetSource(sxDeclarationParent).ExecuteAffrows();

                    //删除对应的题目
                    DbContext.FreeSql.GetRepository<SxTopic>().UpdateDiy.Set(d => new SxTopic
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id
                    }).Where(d => d.IsDelete == CommonConstants.IsNotDelete && d.DeclarationId == id).ExecuteAffrows();

                    // 修改任务题目总数
                    UpdateTaskTopicCount(uow, sxDeclarationParent.TaskId, -1, currentUser);

                    // 修改案例题目总分
                    UpdateCaseTopicTotalScore(uow, caseid, -sxDeclarationChild.Score, currentUser);

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "删除时异常！", false);
                }
            }


            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        /// <summary>
        /// 修改任务题目总数
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="taskId"></param>
        /// <param name="num">增加的题目数量（减时传负数）</param>
        /// <param name="currentUser"></param>
        private async void UpdateTaskTopicCount(FreeSql.IRepositoryUnitOfWork uow, long taskId, int num, UserTicket currentUser)
        {
            if (num == 0)
                return;
            if (uow == null)
                throw new ArgumentNullException("IRepositoryUnitOfWork uow");

            if (taskId == 0)
                return;

            if (currentUser == null)
                throw new NullReferenceException("UserTicket currentUser");

            var repo_task = uow.GetRepository<SxTask>(c => c.IsDelete == CommonConstants.IsNotDelete);

            await repo_task.UpdateDiy.Set(c => new SxTask
            {
                TopicCount = c.TopicCount + num,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(x => x.Id == taskId).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 修改公司题目总分
        /// </summary>
        /// <param name="uow"></param>
        /// <param name="caseId"></param>
        /// <param name="score">增加的题目分数（减时传负数）</param>
        /// <param name="currentUser"></param>
        private async void UpdateCaseTopicTotalScore(FreeSql.IRepositoryUnitOfWork uow, long caseId, decimal score, UserTicket currentUser)
        {
            if (score == 0)
                return;
            if (uow == null)
                throw new ArgumentNullException("IRepositoryUnitOfWork uow");

            if (caseId == 0)
                throw new ArgumentException("long caseId 无效");

            if (currentUser == null)
                throw new NullReferenceException("UserTicket currentUser");

            var repo_task = uow.GetRepository<SxCase>(c => c.IsDelete == CommonConstants.IsNotDelete);

            await repo_task.UpdateDiy.Set(c => new SxCase
            {
                TotalScore = c.TotalScore + score,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(x => x.Id == caseId).ExecuteAffrowsAsync();
        }

        /// <summary>
        /// 根据id获取子表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<DeclarationChildDto>> GetById(long id)
        {
            var query = DbContext.FreeSql.Select<SxDeclarationChild>().Where(d => d.Id == id && d.IsDelete == CommonConstants.IsNotDelete);
            var declarationChildDtos = await DbContext.FreeSql.Ado.QueryAsync<DeclarationChildDto>(query.ToSql("*"));
            if (declarationChildDtos != null && declarationChildDtos.Count > 0)
            {
                return new ResponseContext<DeclarationChildDto>(CommonConstants.SuccessCode, "", declarationChildDtos[0]);
            }
            return new ResponseContext<DeclarationChildDto>(CommonConstants.SuccessCode, "没有找到子表！", null);
        }

        /// <summary>
        /// 根据条件获取子表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<DeclarationChildDto>>> GetList(GetDeclarationChildDto input)
        {
            var declarationChildDtos = await DbContext.FreeSql.Select<SxDeclarationChild>().From<SxTopic, SxExamUserGradeDetail>
                ((a, b, c) => a.LeftJoin(aa => aa.Id == b.DeclarationId)
                .LeftJoin(aa => b.Id == c.QuestionId && c.GradeId == input.GradeId))
                .Where((a, b, c) => a.ParentId == input.ParentId && a.IsDelete == CommonConstants.IsNotDelete)
                .OrderBy((a, b, c) => a.Sort)
                .ToListAsync((a, b, c) => new DeclarationChildDto
                {
                    Id = a.Id,
                    Name = a.Name,
                    Sort = a.Sort,
                    Content = a.Content,
                    Score = a.Score,
                    ParentId = a.ParentId,
                    AnswerValue = a.AnswerValue,
                    CalculationType = a.CalculationType,
                    FullContent = a.FullContent,
                    HeaderContent = a.HeaderContent,
                    IsAnswered = c.GradeId > 0
                });
            //var declarationChildDtos = await DbContext.FreeSql.Ado.QueryAsync<DeclarationChildDto>(query.ToSql("*"));
            if (declarationChildDtos != null && declarationChildDtos.Count > 0)
                return new ResponseContext<List<DeclarationChildDto>>(CommonConstants.SuccessCode, "", declarationChildDtos);
            return new ResponseContext<List<DeclarationChildDto>>(CommonConstants.SuccessCode, "没有找到子表！", null);
        }

        /// <summary>
        /// 保存申报子表
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Save(DeclarationChildDto input, UserTicket currentUser)
        {
            if (input == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "参数错误！", false);
            SxDeclarationChild sxDeclarationChild = input.MapTo<SxDeclarationChild>();
            if (string.IsNullOrWhiteSpace(sxDeclarationChild.Content))
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "Content参数不能为空！", false);
            if (input.Id == 0)
            {
                // 新增
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        sxDeclarationChild.Id = IdWorker.NextId();
                        sxDeclarationChild.CreateBy = currentUser.Id;
                        sxDeclarationChild.CreateTime = DateTime.Now;
                        await DbContext.FreeSql.GetRepository<SxDeclarationChild>().InsertAsync(sxDeclarationChild);
                        SxDeclarationParent sxDeclarationParent = await DbContext.FreeSql.GetRepository<SxDeclarationParent>().Where(d => d.Id == input.ParentId).FirstAsync();
                        if (sxDeclarationParent == null)
                            throw new Exception("找不到该子表的主表！");
                        sxDeclarationParent.Count += 1;
                        sxDeclarationParent.TotalScore += sxDeclarationChild.Score;
                        sxDeclarationParent.UpdateBy = currentUser.Id;
                        sxDeclarationParent.UpdateTime = DateTime.Now;
                        DbContext.FreeSql.Update<SxDeclarationParent>(sxDeclarationParent).SetSource(sxDeclarationParent).ExecuteAffrows();
                    }
                    catch (Exception ex)
                    {
                        uow.Rollback();
                        return new ResponseContext<bool>(CommonConstants.ErrorCode, ex.Message, false);
                    }
                }
            }
            else
            {
                // 修改
                sxDeclarationChild.UpdateBy = currentUser.Id;
                sxDeclarationChild.UpdateTime = DateTime.Now;
                var origial = await DbContext.FreeSql.Select<SxDeclarationChild>().Where(d => d.Id == input.Id).FirstAsync();
                if (origial == null)
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到申报子表", false);
                SxDeclarationParent sxDeclarationParent = await DbContext.FreeSql.GetRepository<SxDeclarationParent>().Where(d => d.Id == origial.ParentId).FirstAsync();
                if (sxDeclarationParent == null)
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到该子表的主表", false);
                sxDeclarationParent.TotalScore -= origial.Score;
                sxDeclarationParent.TotalScore += sxDeclarationChild.Score;
                sxDeclarationParent.UpdateBy = currentUser.Id;
                sxDeclarationParent.UpdateTime = DateTime.Now;
                DbContext.FreeSql.Update<SxDeclarationChild>(sxDeclarationChild).SetSource(sxDeclarationChild).ExecuteAffrows();
                DbContext.FreeSql.Update<SxDeclarationParent>(sxDeclarationParent).SetSource(sxDeclarationParent).ExecuteAffrows();
            }
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
    }
}
