﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Utils;
using Yssx.Repository.Extensions;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    /// <summary>
    /// 公司规章制度业务服务
    /// </summary>
    public class RegulationService : IRegulationService
    {
        /// <summary>
        /// 复制规章制度
        /// </summary>
        /// <param name="id">规章制度唯一id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Copy(long id, UserTicket currentUser)
        {
            var entity = await DbContext.FreeSql.GetRepository<SxRegulation>()
                  .Where(e => e.IsDelete != CommonConstants.IsDelete && e.Id == id)
                  .FirstAsync();

            if (entity == null)
                return new ResponseContext<bool> { Code = CommonConstants.BadRequest, Data = true };

            SxRegulation sxRegulation = new SxRegulation
            {
                Id = IdWorker.NextId(),//获取唯一Id
                CreateBy = currentUser.Id,
                Name = entity.Name,
                Description = entity.Description,
                Content = entity.Content,
                ParentId = entity.ParentId,
            };

            await DbContext.FreeSql.GetRepository<SxRegulation>().InsertAsync(sxRegulation);

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }

        /// <summary>
        /// 保存规章制度 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveCaseUseRegulation(CaseUseRegulationDto model, UserTicket currentUser)
        {
            //查询制度模板信息
            var repo_r = DbContext.FreeSql.GetRepository<SxRegulation>(e => e.IsDelete == CommonConstants.IsNotDelete);
            var repo_cr = DbContext.FreeSql.GetRepository<SxCaseRegulation>(e => e.IsDelete == CommonConstants.IsNotDelete);
            var regulation = await repo_r
                .Where(a => a.Id == model.RegulationId)
                .FirstAsync();

            if (regulation == null)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"制度模板不存在，Id:{model.RegulationId}", false);

            var hascaseRegulation = await repo_cr
                .Where(a => a.RegulationId == model.RegulationId && a.CaseId == model.CaseId)
                .AnyAsync();

            if (hascaseRegulation)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"公司已经应用了该制度模板。", false);

            //构建公司规章制度对象
            SxCaseRegulation entity = CaseRegulationService.CreateCaseRegulation(regulation, model.CaseId, currentUser);

            //保存到数据库中
            await DbContext.FreeSql.GetRepository<SxCaseRegulation>().InsertAsync(entity);

            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        /// <summary>
        /// 保存规章制度 新增和修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Save(RegulationDto model, UserTicket currentUser)
        {
            if (model.Id <= 0)//新增
            {
                return await Add(model, currentUser);
            }
            else //修改
            {
                return await Update(model, currentUser);
            }
        }

        /// <summary>
        /// 保存规章制度 新增
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Add(RegulationDto model, UserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<SxRegulation>(e => e.IsDelete == CommonConstants.IsNotDelete);
            var repo_cr = DbContext.FreeSql.GetRepository<SxCaseRegulation>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasName = await repo
             .Where(e => e.Name == model.Name && e.ParentId == model.ParentId)
             .AnyAsync();

            if (hasName)
            {
                result.Msg = $"制度模板名称重复。";
                return result;
            }

            ////根据案例Id 获取案例的规章制度
            //if (model.CaseIds != null && model.CaseIds.Count > 0)
            //{
            //    var cases = repo_cr
            //                     .Where(a => model.CaseIds.Contains(a.CaseId))
            //                     .From<SxCase>((a, b) => a.InnerJoin(aa => aa.CaseId == b.Id))
            //                     .ToList((a, b) => new { CaseName = b.Name, CaseId = b.Id, a.RegulationId });

            //    if (cases != null && cases.Count > 0)
            //    {
            //        var caseNames = cases.Select(e => e.CaseName).ToList();
            //        result.Msg = $"所选择的公司已经添加了制度，请重新选择：" + caseNames.Join("<br/>");
            //        return result;
            //    }
            //}
            #endregion

            var regulation = model.MapTo<SxRegulation>();
            regulation.Id = IdWorker.NextId();//获取唯一Id
            regulation.CreateBy = currentUser.Id;

            // 构建案例应用制度模板数据
            List<SxCaseRegulation> caseRegulations = CreateCaseRegulation(model, regulation, currentUser);

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo2 = uow.GetRepository<SxRegulation>();
                    var repo_cr2 = uow.GetRepository<SxCaseRegulation>();

                    await repo2.InsertAsync(regulation);

                    if (caseRegulations != null && caseRegulations.Count > 0)
                        await repo_cr2.InsertAsync(caseRegulations);

                    uow.Commit();
                    result.Msg = regulation.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            result.Code = CommonConstants.SuccessCode;
            result.Data = true;
            return result;
        }
        /// <summary>
        /// 保存规章制度 修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Update(RegulationDto model, UserTicket currentUser)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);
            var repo = DbContext.FreeSql.GetRepository<SxRegulation>(e => e.IsDelete == CommonConstants.IsNotDelete);
            var repo_cr = DbContext.FreeSql.GetRepository<SxCaseRegulation>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasregulation = await repo
                   .Where(e => e.Id == model.Id)
                   .AnyAsync();

            if (!hasregulation)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"制度模板不存在，Id:{model.Id}", false);

            var hasName = await repo
             .Where(e => e.Name == model.Name && e.ParentId == model.ParentId && e.Id != model.Id)
             .AnyAsync();

            if (hasName)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"制度模板名称重复。", false);

            ////根据案例Id 获取案例的规章制度
            //if (model.CaseIds != null && model.CaseIds.Count > 0)
            //{
            //    var cases2 = repo_cr
            //                     .Where(a => model.CaseIds.Contains(a.CaseId))
            //                     .From<SxCase>((a, b) => a.InnerJoin(aa => aa.CaseId == b.Id))
            //                     .ToList((a, b) => new { CaseName = b.Name, CaseId = b.Id, a.RegulationId });

            //    if (cases2 != null && cases2.Count > 0)
            //    {
            //        var caseNames = cases2.Select(e => e.CaseName).ToList();
            //        result.Msg = $"所选择的公司已经添加了制度，请重新选择：" + caseNames.Join("<br/>");
            //        return result;
            //    }
            //}

            #endregion

            var regulation = model.MapTo<SxRegulation>();
            regulation.UpdateBy = currentUser.Id;
            regulation.UpdateTime = DateTime.Now;

            List<long> delCaseId = null;
            List<SxCaseRegulation> addcaseRegulations = new List<SxCaseRegulation>();

            //根据案例Id 获取案例的规章制度

            //根据制度模板id获取该模板下所有应用的公司
            var cases = await repo_cr.Where(a => a.RegulationId == regulation.Id)
                         .From<SxCase>((a, b) => a.LeftJoin(aa => aa.CaseId == b.Id))
                         .ToListAsync((a, b) => new { CaseName = b.Name, a.CaseId, a.RegulationId });

            // 构建案例应用制度模板数据
            List<SxCaseRegulation> caseRegulations = CreateCaseRegulation(model, regulation, currentUser);

            if (cases != null && cases.Count > 0)
            {

                //删除的案例id
                if (model.CaseIds != null && model.CaseIds.Count > 0)// 有选择案例，删除取消应用的案例id
                {
                    //删除 需要跟模板解除应用更新的案例Id
                    delCaseId = cases
                        .Where(e => e.RegulationId == regulation.Id && !model.CaseIds.Contains(e.CaseId))
                        .Select(e => e.CaseId)
                        .ToList();
                }
                else//没有选择案例,删除所有的
                {
                    delCaseId = await repo_cr.Where(e => e.RegulationId == regulation.Id).ToListAsync(e => e.CaseId);
                }
                //筛选出新添加的，添加到数据库中
                if (caseRegulations != null)
                {
                    caseRegulations.ForEach(e =>
                        {
                            var isnew = cases.Where(b => b.CaseId == e.CaseId).Any();
                            if (!isnew)
                                addcaseRegulations.Add(e);
                        });
                }
            }
            else//如果规章制度之前没有选择任何公司，所有现在选择直接添加进数据库
            {
                addcaseRegulations = caseRegulations;
            }

            #region 保存到数据库中
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元保存多条数据
            {
                try
                {
                    var repo2 = uow.GetRepository<SxRegulation>();
                    var repo_cr2 = uow.GetRepository<SxCaseRegulation>(e => e.IsDelete == CommonConstants.IsNotDelete);

                    await repo2.UpdateAsync(regulation);
                    //需要跟模板解除应用更新的案例Id,删除旧的案例应用制度模板数据    
                    if (delCaseId != null && delCaseId.Count > 0)
                    {
                        await repo_cr2.UpdateDiy.Set(c => new SxCaseRegulation
                        {
                            RegulationId = 0,
                            RegulationParentId = 0,
                            UpdateBy = currentUser.Id,
                            UpdateTime = DateTime.Now
                        }).Where(e => e.RegulationId == regulation.Id && delCaseId.Contains(e.CaseId)).ExecuteAffrowsAsync();
                    }

                    if (addcaseRegulations != null && addcaseRegulations.Count > 0)
                        await repo_cr2.InsertAsync(addcaseRegulations);//添加新的案例应用制度模板数据

                    uow.Commit();
                    result.Msg = regulation.Id.ToString();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            #endregion

            result.Code = CommonConstants.SuccessCode;
            result.Data = true;

            return result;
        }
        /// <summary>
        /// 构建案例应用制度模板数据
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <param name="regulation"></param>
        /// <returns></returns>
        private static List<SxCaseRegulation> CreateCaseRegulation(RegulationDto model, SxRegulation regulation, UserTicket currentUser)
        {
            //构建案例应用制度模板数据
            List<SxCaseRegulation> caseRegulations = null;
            if (model.CaseIds != null && model.CaseIds.Count > 0)
            {
                caseRegulations = new List<SxCaseRegulation>();
                model.CaseIds.ForEach(a =>
                {
                    var cr = model.MapTo<SxCaseRegulation>();
                    cr.Id = IdWorker.NextId();//获取唯一Id
                    cr.CreateBy = currentUser.Id;
                    cr.RegulationId = regulation.Id;
                    cr.CaseId = a;
                    cr.RegulationParentId = regulation.ParentId;
                    caseRegulations.Add(cr);
                });
            }

            return caseRegulations;
        }

        /// <summary>
        /// 根据Id删除案例规章制度
        /// </summary>
        /// <param name="id">案例规章制度Id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteById(long id, UserTicket currentUser)
        {
            //删除
            await DbContext.FreeSql.GetRepository<SxRegulation>().UpdateDiy.Set(c => new SxRegulation
            {
                IsDelete = CommonConstants.IsDelete,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id,
            }).Where(c => c.Id == id || c.ParentId == id).ExecuteAffrowsAsync();

            return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Data = true };
        }
        /// <summary>
        /// 根据id获取规章制度信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<RegulationDto>> GetInfoById(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<SxRegulation>().Select
               .Where(e => e.Id == id)
               .FirstAsync<RegulationDto>();

            return new ResponseContext<RegulationDto> { Code = CommonConstants.SuccessCode, Data = entity };
        }
        /// <summary>
        /// 获取父级规章制度列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<PageResponse<RegulationListDto>>> GetList(RegulationQueryDto query)
        {
            long count = 0;

            var entity = await DbContext.FreeSql.GetRepository<SxRegulation>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete && e.ParentId == 0)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderBy(e => e.CreateTime)
                   .Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .ToListAsync<RegulationListDto>();

            var data = new PageResponse<RegulationListDto>
            {
                Data = entity,
                RecordCount = count,
                PageIndex = query.PageIndex,
                PageSize = query.PageSize
            };
            return new ResponseContext<PageResponse<RegulationListDto>> { Code = CommonConstants.SuccessCode, Data = data };
        }

        /// <summary>
        /// 根据案例id获取案例所有的规章制度信息
        /// </summary>
        /// <param name="id">案例id</param>
        /// <returns></returns>
        public async Task<ResponseContext<List<RegulationListDto>>> GetListByParentId(long parentId)
        {
            var list = await DbContext.FreeSql.GetRepository<SxRegulation>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete && e.ParentId == parentId)
                   .OrderBy(e => e.CreateTime)
                   .ToListAsync<RegulationListDto>();

            return new ResponseContext<List<RegulationListDto>> { Code = CommonConstants.SuccessCode, Data = list };
        }

        /// <summary>
        /// 根据规章制度id获取应用改规章制度的案例列表,分页
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public async Task<PageResponse<GetCaseListByRegulationIdResponse>> GetCaseListPageByRegulationId(GetCaseListByRegulationIdDto query)
        {
            long count = 0;
            var list = await DbContext.FreeSql.GetRepository<SxCaseRegulation>()
                      .Where(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .WhereIf(query.RegulationId > 0, e => e.RegulationId == query.RegulationId)
                      .Count(out count)
                      .OrderBy(e => e.CreateTime)
                      .Page(query.PageIndex, query.PageSize)
                      .From<SxCase>((a, b) => a.LeftJoin(aa => aa.CaseId == b.Id))//连接查询案例表
                      .ToListAsync((a, b) => new GetCaseListByRegulationIdResponse
                      {
                          CaseId = a.CaseId,
                          Id = a.Id,
                          CreateBy = a.CreateBy,
                          UseDate = a.UseDate,
                          CreateTime = a.CreateTime,
                          CaseName = b.Name,
                      });

            var data = new PageResponse<GetCaseListByRegulationIdResponse>(query.PageIndex, query.PageSize, count, list);

            return data;
        }
        /// <summary>
        /// 获取规章制度模板名称列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<RegulationNamesListDto>>> GetNamesList(RegulationNamesListQueryDto query)
        {
            var result = new ResponseContext<List<RegulationNamesListDto>>(CommonConstants.SuccessCode, "", null);

            var regulations = await DbContext.FreeSql.GetRepository<SxRegulation>(a => a.IsDelete == CommonConstants.IsNotDelete)
                .Select
                .ToListAsync();

            if (regulations == null)
                return result;

            var pr = regulations
                .Where(a => a.ParentId == 0)
                .Select(a => new RegulationNamesListDto { Id = a.Id, Name = a.Name })
                .ToList();

            if (pr == null)
                return result;

            pr.ForEach(b =>
            {
                b.Subs = regulations
                    .Where(a => a.ParentId == b.Id)
                    .Select(a => new RegulationNameSubDto { Id = a.Id, Name = a.Name })
                    .ToList();

            });

            result.Data = pr;
            return result;
        }
    }
}
