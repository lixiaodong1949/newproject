using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;
using YssxC.S.Pocos.Subject;

namespace YssxC.S.Service
{
    public class CaseService : ICaseService
    {
        /// <summary>
        /// 根据id删除案例
        /// </summary>
        /// <param name="id">案例id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteCase(long id, UserTicket currentUser)
        {
            SxCase sxCase = await DbContext.FreeSql.Select<SxCase>().Where(c => c.Id == id).FirstAsync();
            if (sxCase == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到该案例！", false);
            DbContext.FreeSql.Transaction(() =>
            {
                DbContext.FreeSql.GetRepository<SxCase>().UpdateDiy.Set(c => new SxCase
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUser.Id
                }).Where(c => c.Id == id).ExecuteAffrows();
                // 删除案例标签关联表
                DbContext.FreeSql.Delete<SxCaseTag>().Where(ct => ct.CaseId == id).ExecuteAffrows();
                // 删除案例岗位关联
                DbContext.FreeSql.Delete<SxCasePosition>().Where(cp => cp.CaseId == id).ExecuteAffrows();
            });
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        /// <summary>
        /// 根据id获取案例
        /// </summary>
        /// <param name="id">案例id</param>
        /// <returns></returns>
        public async Task<ResponseContext<CaseDto>> GetCase(long id)
        {
            var querySxCase = DbContext.FreeSql.Select<SxCase>().Where(c => c.Id == id && c.IsDelete == CommonConstants.IsNotDelete);
            var caseDtos = await DbContext.FreeSql.Ado.QueryAsync<CaseDto>(querySxCase.ToSql("*"));
            if (caseDtos != null && caseDtos.Count > 0)
            {
                var caseDto = caseDtos[0];
                var ids = DbContext.FreeSql.Select<SxCaseTag>().Where(ct => ct.CaseId == id).ToList(ct => ct.TagId);
                var sql = DbContext.FreeSql.Select<SxTag>().Where(t => ids.Contains(t.Id)).ToSql();
                caseDto.TagDtos = await DbContext.FreeSql.Ado.QueryAsync<TagDto>(sql);

                ids = DbContext.FreeSql.GetRepository<SxCasePosition>(c => c.IsDelete == CommonConstants.IsNotDelete)
                    .Where(cp => cp.CaseId == id).ToList(cp => cp.PositionId);
                var sql2 = DbContext.FreeSql.Select<SxPositionManage>().Where(p => ids.Contains(p.Id)).ToSql();

                caseDto.PositionDtos = await DbContext.FreeSql.Ado.QueryAsync<PositionManageDto>(sql2);

                var gameResources = await DbContext.FreeSql.GetRepository<SxGameResourceConfig>()
                        .Where(e => e.IsDelete == CommonConstants.IsNotDelete && e.CaseId == id)
                        .ToListAsync<GameResourceConfigDto>();

                caseDto.GameResources = gameResources;

                return new ResponseContext<CaseDto>(CommonConstants.SuccessCode, "", caseDto);
            }
            return new ResponseContext<CaseDto>(CommonConstants.SuccessCode, "没有找到案例", null);
        }

        /// <summary>
        /// 根据查询条件获取案例
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResponse<CaseDto>> GetCaseList(GetCaseInput input)
        {
            var query = DbContext.FreeSql.Select<SxCase>()
                .Where(c => c.IsDelete == CommonConstants.IsNotDelete);
            query.WhereIf(!string.IsNullOrWhiteSpace(input.Name), c => c.Name.Contains(input.Name))
                .WhereIf(input.IndustryId.HasValue, c => c.IndustryId == input.IndustryId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Scale), c => c.Scale.Contains(input.Scale))
                .WhereIf(input.Taxpayer.HasValue, c => c.Taxpayer == input.Taxpayer);

            var totalCount = query.Count();
            var fields = "Id,Name,BriefName,IndustryId,Scale,Taxpayer,TotalScore,IsActive,XCoordinate,YCoordinate";
            var sql = query.Page(input.PageIndex, input.PageSize).OrderBy(a => a.Sort).ToSql(fields);
            var items = await DbContext.FreeSql.Ado.QueryAsync<CaseDto>(sql);

            var pageData = new PageResponse<CaseDto>() { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items };
            return pageData;
        }

        /// <summary>
        /// 查询企业列表 - 关联产品
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<GetCaseListByNameDto>> GetCaseListByName(GetCaseInput model)
        {
            var query = DbContext.FreeSql.GetRepository<SxCase>()
                .Where(c => c.IsDelete == CommonConstants.IsNotDelete)
                .WhereIf(!string.IsNullOrWhiteSpace(model.Name), c => c.Name.Contains(model.Name));

            long totalCount = 0;

            var list = await query
                .Count(out totalCount)
                .OrderBy(a => a.Sort)
                .OrderBy(a => a.Id)
                .Page(model.PageIndex, model.PageSize)
                .ToListAsync<GetCaseListByNameDto>();
            //月度任务
            foreach (var item in list)
            {
                var monthData = DbContext.FreeSql.GetGuidRepository<SxTask>().Where(x => x.CaseId == item.Id && x.IsActive && x.IsDelete == CommonConstants.IsNotDelete)
                    .OrderBy(x => x.IssueDate).ToList(x => new CaseInfoMonthDetail
                    {
                        Id = x.Id,
                        Name = x.Name
                    });
                item.MonthDetail = monthData;
            }

            return new PageResponse<GetCaseListByNameDto>() { PageSize = model.PageSize, PageIndex = model.PageIndex, RecordCount = totalCount, Data = list };
        }

        public async Task<ResponseContext<bool>> SaveCase(CaseDto input, UserTicket currentUser)
        {

            SxCase sxCase = input.MapTo<SxCase>();
            List<SxCaseTag> sxCaseTags = new List<SxCaseTag>();
            List<SxCasePosition> sxCasePositions = new List<SxCasePosition>();
            if (input.Id == 0)
            {
                sxCase.Id = IdWorker.NextId();
            }

            if (input.TagDtos != null && input.TagDtos.Count > 0)
            {
                input.TagDtos.ForEach(t =>
                {
                    // 公司标签关联
                    SxCaseTag sxCaseTag = new SxCaseTag();
                    sxCaseTag.Id = IdWorker.NextId();
                    sxCaseTag.CaseId = sxCase.Id;
                    sxCaseTag.TagId = t.Id;
                    sxCaseTag.CreateBy = currentUser.Id;
                    sxCaseTag.CreateTime = DateTime.Now;
                    sxCaseTags.Add(sxCaseTag);
                });
            }
            if (input.PositionDtos != null && input.PositionDtos.Count > 0)
            {
                input.PositionDtos.ForEach(p =>
                {
                    SxCasePosition sxCasePosition = new SxCasePosition();
                    sxCasePosition.Id = IdWorker.NextId();
                    sxCasePosition.CaseId = sxCase.Id;
                    sxCasePosition.PositionId = p.Id;
                    sxCasePosition.PositionType = p.PositionType;
                    sxCasePosition.CreateBy = currentUser.Id;
                    sxCasePosition.CreateTime = DateTime.Now;
                    sxCasePositions.Add(sxCasePosition);
                });
            }
            var result = input.Id > 0 ? await UpdateCompany(sxCase, sxCaseTags, sxCasePositions) : await AddCompany(sxCase, sxCaseTags, sxCasePositions);

            if (input.GameResources != null && input.GameResources.Count > 0)
                await GameResourceConfigService.SetGameResourceConfig(input.GameResources, sxCase.Id);

            return result;

        }

        private async Task<ResponseContext<bool>> AddCompany(SxCase sxCase,
            List<SxCaseTag> sxCaseTags,
            List<SxCasePosition> sxCasePositions)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            List<SxSubject> subjects = GetSubjects(sxCase.Id);
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    if (sxCase != null)
                        await DbContext.FreeSql.Insert<SxCase>(sxCase).ExecuteAffrowsAsync();
                    await DbContext.FreeSql.Insert<SxSubject>(subjects).ExecuteAffrowsAsync();
                    if (sxCaseTags != null && sxCaseTags.Count > 0)
                    {
                        await DbContext.FreeSql.Delete<SxCaseTag>().Where(ct => ct.CaseId == sxCase.Id).ExecuteAffrowsAsync();
                        await DbContext.FreeSql.Insert<SxCaseTag>(sxCaseTags).ExecuteAffrowsAsync();
                    }
                    if (sxCasePositions != null && sxCasePositions.Count > 0)
                    {
                        await DbContext.FreeSql.GetRepository<SxCasePosition>().UpdateDiy.Set(c => new SxCasePosition
                        {
                            IsDelete = CommonConstants.IsDelete,
                            UpdateTime = DateTime.Now
                        }).Where(cp => cp.CaseId == sxCase.Id).ExecuteAffrowsAsync();

                        await DbContext.FreeSql.Insert<SxCasePosition>(sxCasePositions).ExecuteAffrowsAsync();
                    }
                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }
            return result;
        }

        private async Task<ResponseContext<bool>> UpdateCompany(SxCase sxCase,
            List<SxCaseTag> sxCaseTags,
            List<SxCasePosition> sxCasePositions)
        {
            var result = new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    if (!DbContext.FreeSql.Select<SxCase>().Any(c => c.Id == sxCase.Id))
                        return new ResponseContext<bool>(CommonConstants.ErrorCode, "该公司不存在！", false);
                    await DbContext.FreeSql.Update<SxCase>(sxCase).SetSource(sxCase).ExecuteAffrowsAsync();

                    if (sxCaseTags != null && sxCaseTags.Count > 0)
                    {
                        await DbContext.FreeSql.Delete<SxCaseTag>().Where(ct => ct.CaseId == sxCase.Id).ExecuteAffrowsAsync();
                        await DbContext.FreeSql.Insert<SxCaseTag>(sxCaseTags).ExecuteAffrowsAsync();
                    }
                    if (sxCasePositions != null && sxCasePositions.Count > 0)
                    {
                        await DbContext.FreeSql.GetRepository<SxCasePosition>().UpdateDiy.Set(c => new SxCasePosition
                        {
                            IsDelete = CommonConstants.IsDelete,
                            UpdateTime = DateTime.Now
                        }).Where(cp => cp.CaseId == sxCase.Id).ExecuteAffrowsAsync();

                        await DbContext.FreeSql.Insert<SxCasePosition>(sxCasePositions).ExecuteAffrowsAsync();
                    }

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    result = new ResponseContext<bool>(CommonConstants.ErrorCode, "保存是异常！", false);
                }
            }
            return result;
        }

        /// <summary>
        /// 启用或禁用案例
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> Toggle(ToggleInput input, UserTicket currentUser)
        {
            var sxCase = await DbContext.FreeSql.GetRepository<SxCase>().Where(c => c.Id == input.Id && c.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (sxCase == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "该案例不存在！", false);
            sxCase.UpdateBy = currentUser.Id;
            sxCase.UpdateTime = DateTime.Now;
            sxCase.IsActive = input.IsActive;
            DbContext.FreeSql.Update<SxCase>(sxCase).SetSource(sxCase).ExecuteAffrows();
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        private List<SxSubject> GetSubjects(long caseId)
        {
            List<SxSubjectTemp> list = DbContext.FreeSql.GetRepository<SxSubjectTemp, long>().Select.ToList();
            List<SxSubject> subjects = list.Select(x => new SxSubject
            {
                Id = IdWorker.NextId(),
                AssistCode = x.AssistCode,
                SubjectCode = x.SubjectCode,
                SubjectName = x.SubjectName,
                SubjectType = x.SubjectType,
                Code1 = x.Code1,
                Direction = x.Direction,
                CaseId = caseId,
                Code2 = "",
                Code3 = "",
                Code4 = ""
            }).ToList();
            return subjects;
        }

    
        /// <summary>
        /// 获取所有案例列表
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<List<CaseGetAllCaseListResponseDto>>> GetAllCaseList(CaseGetAllCaseListRequestDto query)
        {
            var entitys = await DbContext.FreeSql.GetRepository<SxCase>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete)
                   //.WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderByDescending(e => e.CreateTime)
                   .ToListAsync<CaseGetAllCaseListResponseDto>();

            return new ResponseContext<List<CaseGetAllCaseListResponseDto>>(entitys);
        }
    }
}