﻿using System;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    public class AbstractService : IAbstractService
    {
        /// <summary>
        /// 根据id删除摘要
        /// </summary>
        /// <param name="id">摘要id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteAbstract(long id, UserTicket currentUser)
        {
            SxAbstract sxAbstract = await DbContext.FreeSql.Select<SxAbstract>().Where(a => a.Id == id && a.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (sxAbstract == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不要该摘要！", false);
            DbContext.FreeSql.GetRepository<SxAbstract>().UpdateDiy.Set(a => new SxAbstract
            {
                IsDelete = CommonConstants.IsDelete,
                UpdateTime = DateTime.Now,
                UpdateBy = currentUser.Id
            }).Where(a => a.Id == id).ExecuteAffrows();
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        /// <summary>
        /// 根据id获取摘要信息
        /// </summary>
        /// <param name="id">摘要id</param>
        /// <returns></returns>
        public async Task<ResponseContext<AbstractDto>> GetAbstract(long id)
        {
            var query = DbContext.FreeSql.Select<SxAbstract>().Where(a => a.Id == id && a.IsDelete == CommonConstants.IsNotDelete);
            var sxAbstract = await DbContext.FreeSql.Ado.QueryAsync<AbstractDto>(query.ToSql("*"));
            if (sxAbstract != null && sxAbstract.Count > 0)
            {
                return new ResponseContext<AbstractDto>(CommonConstants.SuccessCode, "", sxAbstract[0]);
            }
            return new ResponseContext<AbstractDto>(CommonConstants.SuccessCode, "没有找到摘要信息", null);
        }

        /// <summary>
        /// 根据条件查询摘要
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResponse<AbstractDto>> GetAbstractList(GetAbstractInput input)
        {
            var query = DbContext.FreeSql.Select<SxAbstract>().Where(a => a.CaseId == input.CaseId && a.IsDelete == CommonConstants.IsNotDelete);
            var totalCount = query.Count();
            var sql = query.Page(input.PageIndex, input.PageSize).OrderBy(a => a.Sort).ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<AbstractDto>(sql);

            var result = new PageResponse<AbstractDto>() { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items };
            return result;
        }

        /// <summary>
        /// 保存摘要信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveAbstract(AbstractDto input, UserTicket currentUser)
        {
            if (input == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "参数为空", false);
            SxAbstract sxAbstract = input.MapTo<SxAbstract>();
            if (input.Id == 0)
            {
                if (DbContext.FreeSql.Select<SxAbstract>().Any(a => a.Name == sxAbstract.Name && a.CaseId == input.CaseId))
                {
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "该摘要已经存在！", false);
                }
                sxAbstract.Id = IdWorker.NextId();
                sxAbstract.CreateBy = currentUser.Id;
                sxAbstract.CreateTime = DateTime.Now;
                await DbContext.FreeSql.GetRepository<SxAbstract>().InsertAsync(sxAbstract);
            }
            else
            {
                if (!DbContext.FreeSql.Select<SxAbstract>().Any(a => a.Id == sxAbstract.Id))
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "该摘要不存在！", false);
                sxAbstract.UpdateBy = currentUser.Id;
                sxAbstract.UpdateTime = DateTime.Now;
                DbContext.FreeSql.Update<SxAbstract>(sxAbstract).SetSource(sxAbstract).ExecuteAffrows();
            }
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
    }
}
