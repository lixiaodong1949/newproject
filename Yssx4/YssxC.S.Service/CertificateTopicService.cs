﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using YssxC.S.Dto;
using YssxC.S.IServices;

namespace YssxC.S.Service
{
    /// <summary>
    /// 凭证题Service
    /// </summary>
    public class CertificateTopicService : ICertificateTopicService
    {
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model">请求数据</param>
        /// <returns></returns>
        public Task<ResponseContext<bool>> DeleteCertificateTopicById(long id)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 根据Id获取凭证题信息
        /// </summary>
        /// <param name="id">凭证题Id</param>
        /// <returns></returns>
        public Task<ResponseContext<CertificateTopicRequest>> GetCoreCertificateTopic(long id)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// 根据Id删除凭证题
        /// </summary>
        /// <param name="id">凭证题Id</param>
        /// <returns></returns>
        public Task<ResponseContext<bool>> SaveCertificateTopic(CertificateTopicRequest model)
        {
            throw new NotImplementedException();
        }
    }
}
