﻿using System;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    public class BankAccountService : IBankAccountService
    {
        /// <summary>
        /// 根据id删除银行账户
        /// </summary>
        /// <param name="id">银行账户id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteBankAccount(long id, UserTicket currentUser)
        {
            SxBankAccount sxBankAccount = await DbContext.FreeSql.Select<SxBankAccount>().Where(b => b.Id == id).FirstAsync();
            if (sxBankAccount == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到该银行账户！", false);
            DbContext.FreeSql.Transaction(() =>
            {
                DbContext.FreeSql.GetRepository<SxBankAccount>().UpdateDiy.Set(b => new SxBankAccount
                {
                    IsDelete = CommonConstants.IsDelete,
                    UpdateTime = DateTime.Now,
                    UpdateBy = currentUser.Id
                }).Where(b => b.Id == id).ExecuteAffrows();
            });
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        /// <summary>
        /// 根据id获取银行账户信息
        /// </summary>
        /// <param name="id">银行账户id</param>
        /// <returns></returns>
        public async Task<ResponseContext<BankAccountDto>> GetBankAccount(long id)
        {
            var query = DbContext.FreeSql.Select<SxBankAccount>().Where(b => b.Id == id && b.IsDelete == CommonConstants.IsNotDelete);
            var sxBankAccount = await DbContext.FreeSql.Ado.QueryAsync<BankAccountDto>(query.ToSql("*"));
            if (sxBankAccount != null && sxBankAccount.Count > 0)
            {
                return new ResponseContext<BankAccountDto>(CommonConstants.SuccessCode,"", sxBankAccount[0]);
            }
            return new ResponseContext<BankAccountDto>(CommonConstants.SuccessCode,"没有找到银行账户",null);
        }

        /// <summary>
        /// 根据条件查询银行账户
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResponse<BankAccountDto>> GetBankAccountList(GetBankAccountInput input)
        {
            var query = DbContext.FreeSql.Select<SxBankAccount>().Where(b => b.CaseId == input.CaseId && b.IsDelete == CommonConstants.IsNotDelete);
            var totalCount = query.Count();
            var sql = query.Page(input.PageIndex, input.PageSize).OrderBy(b => b.CreateTime).ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<BankAccountDto>(sql);

            var result = new PageResponse<BankAccountDto>() { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items};
            return result;
        }

        /// <summary>
        /// 保存银行账户信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveBankAccount(BankAccountDto input, UserTicket currentUser)
        {
            if (input == null)
                return new ResponseContext<bool>() { Code = CommonConstants.ErrorCode, Data = false, Msg = "参数为空"};
            SxBankAccount sxBankAccount = input.MapTo<SxBankAccount>();
            if (input.Id == 0)
            {
                sxBankAccount.Id = IdWorker.NextId();
                sxBankAccount.CreateBy = currentUser.Id;
                sxBankAccount.CreateTime = DateTime.Now;
                await DbContext.FreeSql.GetRepository<SxBankAccount>().InsertAsync(sxBankAccount);
            }
            else
            {
                if (!DbContext.FreeSql.Select<SxBankAccount>().Any(b => b.Id == sxBankAccount.Id))
                    return new ResponseContext<bool>(CommonConstants.ErrorCode,"该账户不存在！",false);
                sxBankAccount.UpdateBy = currentUser.Id;
                sxBankAccount.UpdateTime = DateTime.Now;
                DbContext.FreeSql.Update<SxBankAccount>(sxBankAccount).SetSource(sxBankAccount).ExecuteAffrows();
            }
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }
    }
}
