using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;
using Yssx.Framework;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using Yssx.Framework.Logger;
using System.Linq;

namespace YssxC.S.Service
{
    /// <summary>
    /// 游戏关卡服务
    /// </summary>
    public class GameLevelService : IGameLevelService
    {
        #region 后台
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Save(GameLevelDto model, long currentUserId)
        {
            if (model.Id <= 0)//新增
            {
                return await Add(model, currentUserId);
            }
            else //修改
            {
                return await Update(model, currentUserId);
            }
        }

        /// <summary>
        /// 游戏关卡 新增
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Add(GameLevelDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<SxGameLevel>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasName = await repo
                         .Where(e => e.Name == model.Name && e.TaskId == model.TaskId)
                         .AnyAsync();

            if (hasName)
            {
                result.Msg = $"游戏关卡名称重复。";
                return result;
            }

            #endregion

            var obj = model.MapTo<SxGameLevel>();
            obj.Id = IdWorker.NextId();//获取唯一Id
            obj.CreateBy = currentUserId;
            //if (model.GameTopicNodes != null && model.GameTopicNodes.Length > 0)
            //    obj.TopicNodes = string.Join(',', model.GameTopicNodes);

            #region 保存到数据库中

            await repo.InsertAsync(obj);

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_f = uow.GetRepository<YcsGameLevel>();

            //        await repo_f.InsertAsync(obj);//添加到数据库

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}
            #endregion

            result.SetSuccess(true, obj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 游戏关卡 修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        private async Task<ResponseContext<bool>> Update(GameLevelDto model, long currentUserId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "", false);

            var repo = DbContext.FreeSql.GetRepository<SxGameLevel>(e => e.IsDelete == CommonConstants.IsNotDelete);

            #region 参数校验
            var hasobj = await repo
                   .Where(e => e.Id == model.Id)
                   .AnyAsync();

            if (!hasobj)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"游戏关卡不存在，Id:{model.Id}", false);

            var hasName = await repo
                         .Where(e => e.Name == model.Name && e.Id != model.Id && e.TaskId == model.TaskId)
                         .AnyAsync();

            if (hasName)
                return new ResponseContext<bool>(CommonConstants.BadRequest, $"游戏关卡名称重复。", false);

            #endregion
            //赋值
            var updateObj = model.MapTo<SxGameLevel>();
            updateObj.UpdateBy = currentUserId;
            updateObj.UpdateTime = DateTime.Now;
            //if (model.GameTopicNodes != null && model.GameTopicNodes.Length > 0)
            //    updateObj.TopicNodes = string.Join(',', model.GameTopicNodes);

            #region 保存到数据库中

            await repo.UpdateDiy.SetSource(updateObj)
                .UpdateColumns(a => new { a.Name, a.Content, a.Point, a.Sort, a.TotalMinutes, a.Free })
                .ExecuteAffrowsAsync();//修改内容

            //await repo.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            //{
            //    try
            //    {
            //        var repo_f = uow.GetRepository<YcsGameLevel>();

            //        await repo_f.UpdateDiy.SetSource(updateObj).IgnoreColumns(a => new { a.CreateBy, a.CreateTime, }).ExecuteAffrowsAsync();//修改内容

            //        //await repo_f.UpdateDiy.SetSource(updateObj).UpdateColumns(a => new { a.Name, }).ExecuteAffrowsAsync();//修改内容

            //        uow.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        uow.Rollback();
            //        CommonLogger.Error(ex.ToString());//写错误日志
            //        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
            //    }
            //}
            #endregion

            result.SetSuccess(true, updateObj.Id.ToString());//设置返回值为成功

            return result;
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResponseContext<GameLevelDto>> Get(long id)
        {
            var entity = await DbContext.FreeSql.GetRepository<SxGameLevel>(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .Where(e => e.Id == id)
                      .FirstAsync<GameLevelDto>();

            return new ResponseContext<GameLevelDto>(entity);
        }

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        public async Task<ResponseContext<bool>> Delete(long id, long currentUserId)
        {
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())//工作单元
            {
                try
                {
                    //删除
                    await uow.GetRepository<SxGameLevel>(e => e.IsDelete == CommonConstants.IsNotDelete).UpdateDiy.Set(c => new SxGameLevel
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUserId,
                    }).Where(c => c.Id == id).ExecuteAffrowsAsync();

                    await uow.GetRepository<SxGameTopic>(e => e.IsDelete == CommonConstants.IsNotDelete).UpdateDiy.Set(c => new SxGameTopic
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUserId,
                    }).Where(c => c.GameLevelId == id).ExecuteAffrowsAsync();

                    uow.Commit();
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    CommonLogger.Error(ex.ToString());//写错误日志
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                }
            }

            return new ResponseContext<bool>(true);
        }

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        public async Task<PageResponse<GameLevelPageResponseDto>> ListPage(GameLevelPageRequestDto query)
        {
            long count = 0;
            var entitys = await DbContext.FreeSql.GetRepository<SxGameLevel>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderBy(e => e.Sort)
                   .Count(out count)
                   .Page(query.PageIndex, query.PageSize)
                   .ToListAsync<GameLevelPageResponseDto>();

            return new PageResponse<GameLevelPageResponseDto>(query.PageIndex, query.PageSize, count, entitys);
        }

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<GameLevelListResponseDto>> List(GameLevelListRequestDto query)
        {
            var entitys = await DbContext.FreeSql.GetRepository<SxGameLevel>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete)
                   .WhereIf(query.CaseId > 0, e => e.CaseId == query.CaseId)
                   .WhereIf(query.TaskId > 0, e => e.TaskId == query.TaskId)
                   .WhereIf(!string.IsNullOrWhiteSpace(query.Name), e => e.Name.Contains(query.Name))
                   .OrderBy(e => e.Sort)
                   .ToListAsync<GameLevelListResponseDto>();

            return new ListResponse<GameLevelListResponseDto>(entitys);
        }

        #endregion

        #region 前台

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        public async Task<ListResponse<GameLevelsResponseDto>> GetGameLevelList(long taskId, long userId)
        {
            var gameLevels = await DbContext.FreeSql.GetRepository<SxGameLevel>()
                   .Where(e => e.IsDelete != CommonConstants.IsDelete && e.TaskId == taskId)
                   .OrderBy(e => e.Sort)
                   .ToListAsync<GameLevelsResponseDto>();



            var gameGrades = await DbContext.FreeSql.GetRepository<SxGameUserGrade>()
                   .Where(e => e.IsDelete == CommonConstants.IsNotDelete && e.TaskId == taskId && e.UserId == userId)
                   .ToListAsync(e => new SxGameUserGrade { Id = e.Id, Status = e.Status, GameLevelId = e.GameLevelId, IsNew = e.IsNew });

            var prevGameGradeId = 0L;//上一关卡是否通关
            //作答记录，赋值最新的作答记录，设置每个关卡的最新作答状态
            gameLevels.ForEach(e =>
            {
                //获取最新的作答记录
                var gameGrade = gameGrades.Where(f => f.GameLevelId == e.Id && f.IsNew == true).FirstOrDefault();
                if (gameGrade != null)//当前关卡有作答记录
                {
                    if (gameGrade.Status == StudentExamStatus.Started)
                        e.Status = GameLevelStatus.Doing;
                    else if (gameGrade.Status == StudentExamStatus.End)
                        e.Status = GameLevelStatus.Done;
                }
                else if (e.Sort == 1)//当前关卡没有作答记录时，如果为第一个默认解锁，
                {
                    e.Status = GameLevelStatus.UnLock;
                }
                else//当前关卡没有作答记录时，如果上一关有已经通关记录即当前关卡即可解锁
                {
                    var prevLevelDone = gameGrades.Where(f => f.GameLevelId == prevGameGradeId && f.Status == StudentExamStatus.End).Any();//上一关卡是否有作答结束的记录
                    if (prevLevelDone) e.Status = GameLevelStatus.UnLock;
                }
                prevGameGradeId = e.Id;
            });

            return new ListResponse<GameLevelsResponseDto>(gameLevels);
        }

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id">关卡Id</param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<GameLevelResponseDto>> GetGameLevel(long id, long userId)
        {
            var result = new ResponseContext<GameLevelResponseDto>();

            var gameLevelBaseInfo = await DbContext.FreeSql.GetRepository<SxGameLevel>(e => e.IsDelete == CommonConstants.IsNotDelete)
                      .Where(e => e.Id == id)
                      .FirstAsync<GameLevelBaseInfoDto>();

            if (gameLevelBaseInfo == null)
            {
                result.SetError("关卡不存在。");
                return result;
            }

            //用户当前关卡历史最佳记录
            //var userGameLevelGradeNo1 = await GameTopicService.UserGameLevelGradeNo1(gameLevelBaseInfo.Id, userId);

            //当前关卡历史最佳记录
            var currentGameLevelGradeNo1 = await GameTopicService.CurrentGameLevelGradeNo1(gameLevelBaseInfo.Id);

            var r = new GameLevelResponseDto
            {
                GameLevelBaseInfo = gameLevelBaseInfo,
                CurrentUserRank = null,
                RankingFirst = currentGameLevelGradeNo1,
            };

            result.SetSuccess(r);

            return result;
        }

        #endregion

        /// <summary>
        /// 培训完成
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> TrainFinish(long taskId, long userId)
        {
            var result = new ResponseContext<bool>(CommonConstants.ErrorCode, "");

            if (userId == CommonConstants.DefaultUserId)//游客
            {
                result.SetSuccess(true);//设置返回值为成功
                return result;
            }
            #region 参数校验
            var task = await DbContext.FreeSql.GetRepository<SxTask>(e => e.IsDelete == CommonConstants.IsNotDelete)
                         .Where(e => e.Id == taskId)
                         .FirstAsync(e => new SxTask { Id = e.Id, CaseId = e.CaseId });

            if (task == null)
            {
                result.Msg = $"任务不存在。";
                return result;
            }

            #endregion

            var obj = new SxGameUserTrain
            {
                Id = IdWorker.NextId(),
                CaseId = task.CaseId,
                TaskId = task.Id,
                UserId = userId,
            };


            #region 保存到数据库中

            await DbContext.FreeSql.GetRepository<SxGameUserTrain>().InsertAsync(obj);

            #endregion

            result.SetSuccess(true, obj.Id.ToString());//设置返回值为成功

            return result;
        }
    }
}