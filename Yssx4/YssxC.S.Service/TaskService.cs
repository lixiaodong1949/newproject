﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.AutoMapper;
using Yssx.Framework.Dal;
using Yssx.Framework.Entity;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    /// <summary>
    /// 任务
    /// </summary>
    public class TaskService : ITaskService
    {
        /// <summary>
        /// 根据id删除任务
        /// </summary>
        /// <param name="id">任务id</param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> DeleteTask(long id, UserTicket currentUser)
        {
            SxTask sxTask = await DbContext.FreeSql.Select<SxTask>().Where(t => t.Id == id && t.IsDelete == CommonConstants.IsNotDelete).FirstAsync();
            if (sxTask == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "找不到该任务！", false);
            using (var uow = DbContext.FreeSql.CreateUnitOfWork())
            {
                try
                {
                    DbContext.FreeSql.GetRepository<SxTask>().UpdateDiy.Set(t => new SxTask
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id
                    }).Where(t => t.Id == id).ExecuteAffrows();
                    DbContext.FreeSql.GetRepository<SxTaskItem>().UpdateDiy.Set(t => new SxTaskItem
                    {
                        IsDelete = CommonConstants.IsDelete,
                        UpdateTime = DateTime.Now,
                        UpdateBy = currentUser.Id
                    }).Where(t => t.TaskId == id);

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                    return new ResponseContext<bool>(CommonConstants.ErrorCode, "删除时异常", false);
                }
            }
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        /// <summary>
        /// 根据id获取任务
        /// </summary>
        /// <param name="id">任务id</param>
        /// <returns></returns>
        public async Task<ResponseContext<TaskDto>> GetTask(long id)
        {
            var query = DbContext.FreeSql.Select<SxTask>().Where(t => t.Id == id && t.IsDelete == CommonConstants.IsNotDelete);
            var taskDtos = await DbContext.FreeSql.Ado.QueryAsync<TaskDto>(query.ToSql("*"));
            if (taskDtos != null && taskDtos.Count > 0)
            {
                var taskDto = taskDtos[0];
                var select = DbContext.FreeSql.Select<SxTaskItem>().Where(ti => ti.TaskId == id && ti.IsDelete == CommonConstants.IsNotDelete);
                var taskItemDtos = await DbContext.FreeSql.Ado.QueryAsync<TaskItemDto>(select.ToSql("*"));
                if (taskItemDtos != null && taskItemDtos.Count > 0)
                {
                    taskDto.TaskItems = taskItemDtos;
                }
                return new ResponseContext<TaskDto>(CommonConstants.SuccessCode, "", taskDto);
            }
            return new ResponseContext<TaskDto>(CommonConstants.SuccessCode, "没有找到任务！", null);
        }

        /// <summary>
        /// 根据条件查询任务
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<PageResponse<TaskDto>> GetTaskList(GetTaskInput input)
        {
            var query = DbContext.FreeSql.Select<SxTask>().Where(t => t.CaseId == input.CaseId && t.IsDelete == CommonConstants.IsNotDelete).WhereIf(!string.IsNullOrWhiteSpace(input.Name), t => t.Name.Contains(input.Name));
            var totalCount = query.Count();
            var sql = query.Page(input.PageIndex, input.PageSize).OrderBy(t => t.IssueDate).ToSql("*");
            var items = await DbContext.FreeSql.Ado.QueryAsync<TaskDto>(sql);

            var result = new PageResponse<TaskDto>() { PageSize = input.PageSize, PageIndex = input.PageIndex, RecordCount = totalCount, Data = items };
            return result;
        }

        /// <summary>
        /// 保存任务
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> SaveTask(TaskDto input, UserTicket currentUser)
        {
            DateTime dtNow = DateTime.Now;
            if (input == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "参数错误！", false);
            SxTask sxTask = input.MapTo<SxTask>();
            if (input.Id == 0)
            {
                // 新增
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        //任务
                        sxTask.Id = IdWorker.NextId();
                        sxTask.CreateBy = currentUser.Id;
                        sxTask.CreateTime = dtNow;
                        sxTask.TaskItemCount = input.TaskItems == null ? 0 : input.TaskItems.Count;
                        await DbContext.FreeSql.GetRepository<SxTask>().InsertAsync(sxTask);
                        //任务明细
                        List<SxTaskItem> sxTaskItems = new List<SxTaskItem>();
                        if (input.TaskItems != null)
                        {
                            input.TaskItems.ForEach(ti =>
                            {
                                SxTaskItem sxTaskItem = ti.MapTo<SxTaskItem>();
                                sxTaskItem.TaskId = sxTask.Id;
                                sxTaskItem.Id = IdWorker.NextId();
                                sxTaskItem.CreateBy = currentUser.Id;
                                sxTaskItem.CreateTime = dtNow;
                                sxTaskItems.Add(sxTaskItem);
                            });
                        }
                        await DbContext.FreeSql.GetRepository<SxTaskItem>().InsertAsync(sxTaskItems);
                        //生成试卷 - 默认用户
                        SxExamPaper sxExamPaper = new SxExamPaper
                        {
                            Id = IdWorker.NextId(),
                            Name = sxTask.Name,
                            UserId = CommonConstants.DefaultUserId,
                            CaseId = sxTask.CaseId,
                            MonthTaskId = sxTask.Id,
                            ExamType = ExamType.PracticeTest,
                            CompetitionType = CompetitionType.IndividualCompetition,
                            CanShowAnswerBeforeEnd = true,
                            TotalScore = 0,
                            PassScore = 0,
                            TotalQuestionCount = 0,
                            BeginTime = dtNow,
                            Sort = 1,
                            IsRelease = true,
                            Status = ExamStatus.Wait,
                            ExamSourceType = SxExamSourceType.Order,
                            CreateBy = currentUser.Id,
                            CreateTime = dtNow
                        };
                        //试卷题目
                        //var rTopicList = DbContext.FreeSql.Select<SxTopic>().Where(x=>x.TaskId==) 

                        await DbContext.FreeSql.GetRepository<SxExamPaper>().InsertAsync(sxExamPaper);

                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                    }
                }
            }
            else
            {
                // 修改
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    try
                    {
                        if (!DbContext.FreeSql.Select<SxTask>().Any(t => t.Id == input.Id))
                            return new ResponseContext<bool>(CommonConstants.ErrorCode, "该任务不存在！", false);
                        sxTask.UpdateBy = currentUser.Id;
                        sxTask.UpdateTime = dtNow;
                        sxTask.TaskItemCount = input.TaskItems == null ? 0 : input.TaskItems.Count;

                        DbContext.FreeSql.Update<SxTask>(sxTask).SetSource(sxTask)
                            .IgnoreColumns(e => new { e.CaseId, e.CreateBy, e.CreateTime, })
                            .ExecuteAffrows();
                        
                        DbContext.FreeSql.Delete<SxTaskItem>().Where(ti => ti.TaskId == sxTask.Id).ExecuteAffrows();
                        if (input.TaskItems != null)
                        {
                            input.TaskItems.ForEach(ti =>
                            {
                                SxTaskItem sxTaskItem = ti.MapTo<SxTaskItem>();
                                if (sxTaskItem.Id == 0)
                                {
                                    sxTaskItem.Id = IdWorker.NextId();
                                    sxTaskItem.TaskId = sxTask.Id;
                                    sxTaskItem.CreateBy = currentUser.Id;
                                    sxTaskItem.CreateTime = dtNow;
                                    DbContext.FreeSql.Insert<SxTaskItem>(sxTaskItem).ExecuteAffrows();
                                }
                                else
                                {
                                    sxTaskItem.CreateBy = currentUser.Id;
                                    sxTaskItem.CreateTime = dtNow;
                                    sxTaskItem.TaskId = sxTask.Id;
                                    DbContext.FreeSql.Insert<SxTaskItem>(sxTaskItem).ExecuteAffrows();
                                }
                            });
                        }

                        uow.Commit();
                    }
                    catch (Exception)
                    {
                        uow.Rollback();
                        return new ResponseContext<bool>(CommonConstants.ErrorCode, "保存时异常！", false);
                    }
                }
            }
            return new ResponseContext<bool>(CommonConstants.SuccessCode, "", true);
        }

        /// <summary>
        /// 开启或关闭任务
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> ToggleStatus(ToggleInput input, long currentUserId)
        {
            DateTime dtNow = DateTime.Now;
            //开启任务
            var sxTask = DbContext.FreeSql.GetRepository<SxTask>().Where(x => x.Id == input.Id && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (sxTask == null)
                return new ResponseContext<bool>(CommonConstants.ErrorCode, "该任务不存在", false);
            sxTask.UpdateBy = currentUserId;
            sxTask.UpdateTime = DateTime.Now;
            sxTask.IsActive = input.IsActive;
            //生成试卷 - 默认用户
            var sxExamPaper = new SxExamPaper();
            var rSxExamPaper = DbContext.FreeSql.GetRepository<SxExamPaper>().Where(x => x.MonthTaskId == sxTask.Id && x.UserId == CommonConstants.DefaultUserId && x.IsDelete == CommonConstants.IsNotDelete).First();
            if (rSxExamPaper == null)
            {
                sxExamPaper = new SxExamPaper
                {
                    Id = IdWorker.NextId(),
                    Name = sxTask.Name,
                    UserId = CommonConstants.DefaultUserId,
                    CaseId = sxTask.CaseId,
                    MonthTaskId = sxTask.Id,
                    ExamType = ExamType.PracticeTest,
                    CompetitionType = CompetitionType.IndividualCompetition,
                    CanShowAnswerBeforeEnd = true,
                    TotalScore = 0,
                    PassScore = 0,
                    TotalQuestionCount = 0,
                    BeginTime = dtNow,
                    Sort = 1,
                    IsRelease = true,
                    Status = ExamStatus.Wait,
                    ExamSourceType = SxExamSourceType.Order,
                    CreateBy = currentUserId,
                    CreateTime = dtNow
                };
                //试卷题目
                var rTopicList = DbContext.FreeSql.Select<SxTopic>().Where(x => x.TaskId == sxTask.Id && x.ParentId == 0 && x.IsDelete == CommonConstants.IsNotDelete).ToList();
                if (rTopicList.Count > 0)
                {
                    var rTotalScore = rTopicList.Sum(x => x.Score);
                    sxExamPaper.TotalScore = rTotalScore;
                    sxExamPaper.PassScore = rTotalScore * (decimal)0.6;
                    sxExamPaper.TotalQuestionCount = rTopicList.Count;
                }
            }

            return await Task.Run(() =>
            {
                var state = false;
                using (var uow = DbContext.FreeSql.CreateUnitOfWork())
                {
                    state = DbContext.FreeSql.Update<SxTask>().SetSource(sxTask).UpdateColumns(m => new { m.UpdateTime, m.UpdateBy, m.IsActive }).ExecuteAffrows() > 0;
                    if (state && rSxExamPaper == null)
                        DbContext.FreeSql.GetRepository<SxExamPaper>().InsertAsync(sxExamPaper);
                    uow.Commit();
                }
                return new ResponseContext<bool> { Code = CommonConstants.SuccessCode, Msg = "操作成功", Data = state };
            });
        }

    }
}
