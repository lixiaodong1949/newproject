﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Dal;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    public class DeclarationTemplateParentService : IDeclarationTemplateParentService
    {
        /// <summary>
        /// 根据条件查询申报表主表模板
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<DeclarationTemplateParentDto>>> GetAll()
        {
            var query = DbContext.FreeSql.Select<SxDeclarationTemplateParent>();
            var items = await DbContext.FreeSql.Ado.QueryAsync<DeclarationTemplateParentDto>(query.ToSql("*"));
            return new ResponseContext<List<DeclarationTemplateParentDto>>(CommonConstants.SuccessCode,"",items);
        }
    }
}
