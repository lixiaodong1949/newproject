﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Dal;
using YssxC.S.Dto;
using YssxC.S.IServices;
using YssxC.S.Pocos;

namespace YssxC.S.Service
{
    /// <summary>
    /// 题目公共基础服务
    /// </summary>
    public class TopicBaseService : ITopicBaseService
    {
        #region 用户标记/取消标记题目操作
        /// <summary>
        /// 用户标记/取消标记题目操作
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ResponseContext<bool>> UserMarkOrUnMarkQuestion(UserMarkQuestionDto dto, UserTicket user)
        {
            bool state = false;

            if (dto == null || dto.GradeId == 0 || dto.QuestionId == 0 || user == null)
                return new ResponseContext<bool> { Code = CommonConstants.ErrorCode, Data = state, Msg = "请传有效参数!" };

            SxTopicMark markData = await DbContext.FreeSql.GetRepository<SxTopicMark>().Where(x => x.GradeId == dto.GradeId && x.UserId == user.Id
                && x.QuestionId == dto.QuestionId).FirstAsync();

            //标记
            if (markData == null)
            {
                markData = new SxTopicMark
                {
                    Id = IdWorker.NextId(),
                    UserId = user.Id,
                    GradeId = dto.GradeId,
                    QuestionId = dto.QuestionId,
                    CreateTime = DateTime.Now
                };

                var retuenData = await DbContext.FreeSql.GetRepository<SxTopicMark>().InsertAsync(markData);

                state = retuenData != null;
            }
            //删除标记
            else
            {
                int retuenCount = await DbContext.FreeSql.GetRepository<SxTopicMark>().DeleteAsync(markData);

                state = retuenCount > 0;
            }

            return new ResponseContext<bool> { Code = state ? CommonConstants.SuccessCode : CommonConstants.ErrorCode, Data = state, Msg = state ? "成功!" : "操作失败!" };
        }
        #endregion

        #region 获取公司下岗位列表
        /// <summary>
        /// 获取公司下岗位列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<UserCasePositionViewModel>>> GetUserCasePositionList(long caseId, PositionType positionType = PositionType.composite)
        {
            ResponseContext<List<UserCasePositionViewModel>> response = new ResponseContext<List<UserCasePositionViewModel>>();

            response.Data = await DbContext.FreeSql.Select<SxCasePosition>().From<SxPositionManage>(
                   (a, b) =>
                   a.InnerJoin(x => x.PositionId == b.Id))
                    .Where((a, b) => a.CaseId == caseId && a.IsDelete == CommonConstants.IsNotDelete && a.PositionType == positionType)
                    .ToListAsync((a, b) => new UserCasePositionViewModel
                    {
                        PositionId = a.PositionId,
                        PositionName = b.Name,
                    });

            return response;
        }
        #endregion

        #region 获取公司客户列表
        /// <summary>
        /// 获取公司客户列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        public async Task<ResponseContext<List<UserCaseCustomerViewModel>>> GetUserCaseCustomerList(long caseId)
        {
            ResponseContext<List<UserCaseCustomerViewModel>> response = new ResponseContext<List<UserCaseCustomerViewModel>>();
            List<UserCaseCustomerViewModel> data = new List<UserCaseCustomerViewModel>();

            List<SxAssistAccounting> sourceData = await DbContext.FreeSql.Select<SxAssistAccounting>().Where(x => x.CaseId == caseId &&
                x.IsDelete == CommonConstants.IsNotDelete && x.Type != null).ToListAsync();

            sourceData.ForEach(x =>
            {
                data.Add(new UserCaseCustomerViewModel
                {
                    CustomerId = x.Id,
                    Name = x.Name,
                    Type = (Nullable<int>)x.Type,
                    BankAccountNo = x.BankAccountNo,
                    BankName = x.BankName,
                    BankBranchName = x.BankBranchName,
                });
            });

            response.Data = data;

            return response;
        }
        #endregion

    }
}
