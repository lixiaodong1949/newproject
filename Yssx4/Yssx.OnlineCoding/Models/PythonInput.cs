﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yssx.OnlineCoding.Models
{
    public class PythonInput
    {
        /// <summary>
        /// 代码
        /// </summary>
        public string Code { get; set; }
    }
}
