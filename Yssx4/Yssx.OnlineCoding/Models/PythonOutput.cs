﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yssx.OnlineCoding.Models
{
    public class PythonOutput
    {
        /// <summary>
        /// 代码执行输出文本
        /// </summary>
        public string TextOutput { get; set; }

        /// <summary>
        /// 代码执行输出文件
        /// </summary>
        public List<FileMap> FileOutput { get; set; }
    }

    public class FileMap
    {
        /// <summary>
        /// 原文件
        /// </summary>
        public string SrcFile { get; set; }

        /// <summary>
        /// 目标文件
        /// </summary>
        public string TargetFile { get; set; }
    }
}
