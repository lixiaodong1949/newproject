﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Yssx.OnlineCoding.Models;

namespace Yssx.OnlineCoding.Controllers
{
    /// <summary>
    /// 在线 Python 解释器
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class PythonOnlineController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="configuration"></param>
        public PythonOnlineController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// 在线运行 Python 代码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<PythonOutput>> Post(PythonInput input)
        {
            return await Task.Run(() =>
            {
                var temp = Path.Combine(Environment.CurrentDirectory, "Temp");
                if (Directory.Exists(temp))
                {
                    var fileInfos = new DirectoryInfo(temp).GetFiles();
                    foreach (var fileInfo in fileInfos)
                    {
                        if (fileInfo.LastAccessTime <= DateTime.Now.AddDays(-1))
                        {
                            fileInfo.Delete();
                        }
                    }
                }
                else
                {
                    Directory.CreateDirectory(temp);
                }


                var result = string.Empty;
                if (string.IsNullOrEmpty(input.Code))
                    return new ActionResult<PythonOutput>(new PythonOutput());

                List<FileMap> files = new List<FileMap>();
                var matches = Regex.Matches(input.Code, "[\"'][\\S]+\\.(xlsx|xls|csv|png)");
                foreach (var match in matches)
                {
                    var matchString = match.ToString().Trim(new char[] { '\'', '\"' });
                    var targetFile = Guid.NewGuid().ToString();
                    if (matchString.ToString().EndsWith(".xls", StringComparison.OrdinalIgnoreCase))
                    {
                        targetFile += ".xls";
                    }
                    else if (matchString.ToString().EndsWith(".xlsx", StringComparison.OrdinalIgnoreCase))
                    {
                        targetFile += ".xlsx";
                    }
                    else if (matchString.ToString().EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
                    {
                        targetFile += ".csv";
                    }
                    else if (matchString.ToString().EndsWith(".png", StringComparison.OrdinalIgnoreCase))
                    {
                        targetFile += ".png";
                    }
                    else
                    {
                        continue;
                    }
                    var file = Path.Combine(temp, targetFile);
                    input.Code = input.Code.Replace(matchString.ToString(), file.Replace("\\", "\\\\"));
                    FileMap fileMap = new FileMap();
                    fileMap.SrcFile = matchString;
                    fileMap.TargetFile = targetFile;
                    files.Add(fileMap);
                }

                var filename = Path.Combine(temp, Guid.NewGuid().ToString() + ".py");
                System.IO.File.WriteAllText(filename, input.Code);
                var pythonInterpreter = _configuration.GetValue<string>("Python.Interpreter");
                ProcessStartInfo startInfo = new ProcessStartInfo(pythonInterpreter);
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.Arguments = filename;
                Process process = Process.Start(startInfo);
                using (StreamReader reader = process.StandardOutput)
                {
                    result = reader.ReadToEnd();
                }
                using (StreamReader errorReader = process.StandardError)
                {
                    result += errorReader.ReadToEnd();
                }
                System.IO.File.Delete(filename);

                var fileOutput = new List<FileMap>();
                for (int i = 0; i < files.Count; i++)
                {
                    if (System.IO.File.Exists(Path.Combine(temp, files[i].TargetFile)))
                    {
                        fileOutput.Add(files[i]);
                    }
                }

                return new ActionResult<PythonOutput>(new PythonOutput() { TextOutput = result, FileOutput = fileOutput });
            });

        }
    }
}
