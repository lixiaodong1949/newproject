﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Yssx.OnlineCoding.Models;

namespace Yssx.OnlineCoding.Controllers
{
    /// <summary>
    /// 文件
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class FileController : ControllerBase
    {
        /// <summary>
        /// 获取文件
        /// </summary>
        /// <param name="fileMap"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<FileResult> Get([FromQuery] FileMap fileMap)
        {
            return await Task.Run(() =>
            {
                StringSegment stringSegment = new StringSegment("text/csv");
                if (fileMap.TargetFile.EndsWith(".xls"))
                {
                    stringSegment = new StringSegment("application/vnd.ms-excel");
                }
                else if (fileMap.TargetFile.EndsWith(".xlsx"))
                {
                    stringSegment = new StringSegment("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                }
                else if (fileMap.TargetFile.EndsWith(".csv"))
                {
                    stringSegment = new StringSegment("text/csv");
                }
                else if (fileMap.TargetFile.EndsWith(".png"))
                {
                    stringSegment = new StringSegment("image/png");
                }
                FileStream fileStream = new FileStream(Path.Combine(Environment.CurrentDirectory, "Temp", fileMap.TargetFile), FileMode.Open);
                var fileStreamResult = new FileStreamResult(fileStream, new Microsoft.Net.Http.Headers.MediaTypeHeaderValue(stringSegment));
                fileStreamResult.FileDownloadName = fileMap.SrcFile;
                return fileStreamResult;
            });
        }
    }
}
