﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto.ResourcePackge;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 资源套餐
    /// </summary>
    public interface IResoursePackService
    {
        /// <summary>
        /// 套餐列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<ResoursePeckDto>>> GetResoursePeckList(int eType);

        /// <summary>
        /// 添加套餐
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddResoursePeck(ResoursePeckDto dto);

        /// <summary>
        /// 删除套餐
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteResoursePeck(long id);
    }

    /// <summary>
    /// 套餐资源详情
    /// </summary>
    public interface IResoursePackDetailsService: IResoursePackService
    {
        /// <summary>
        /// 资源详情列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<List<ResourcePackDetailDto>>> GetResourcePackDetailsList(long id);

        /// <summary>
        /// 添加套餐资源
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddResourceDetails(AddResourceDetailsDto dto);


        /// <summary>
        /// 删除资源
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteResourceDetails(long id);
    }

    /// <summary>
    /// 绑定资源到学校
    /// </summary>
    public interface IBindingResourseToSchool
    {
        /// <summary>
        /// 绑定学校资源
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BindingResourse(BindingResourseDto dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schoolId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<BindingResourseDto>>> GetSchoolResourseList(long schoolId);
    }
}
