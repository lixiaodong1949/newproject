﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.Dto.OptimizeContent;

namespace Yssx.S.IServices
{
    /// <summary>
    /// App接口
    /// </summary>
    public interface IAppService
    {
        /// <summary>
        /// 用户是否已存在预览课程行为
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> IsExistUserPreviewCourseAction(UserTicket user);

        /// <summary>
        /// 获取用户预览课程信息
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<UserPreviewCourseInfoViewModel>> GetUserPreviewCourseInfoForApp(UserTicket user);

        /// <summary>
        /// 获取学校课程订单数据(App)
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<OrderCourseDto>>> GetCourseListBySchoolPlatForApp(CourseSearchDto dto, UserTicket user);

        /// <summary>
        /// 获取学校课程任务下课程数据(App)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<OrderCourseDto>>> GetCourseListBySchoolCourseTaskForApp(UserTicket user);

        /// <summary>
        /// 获取学校课程订单数据(不分学历层级)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxCourseListDto>>> GetSchoolBuyCourseListForApp(UserTicket user);

        /// <summary>
        /// 记录用户预览课程记录
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RecordUserCoursePreviewAction(UserCoursePreviewRequestModel dto, UserTicket user);

        /// <summary>
        /// 记录用户发布课程任务记录
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RecordUserPublishCourseTaskAction(UserPublishCourseTaskRequestModel dto, UserTicket user);

    }
}
