﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Home;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 主页接口
    /// </summary>
    public interface IHomeService
    {
        /// <summary>
        /// 获取推荐会计实训案例列表接口
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<AccountantTrainingDto>> GetAccountantTrainingList(PageRequest model);

        /// <summary>
        /// 获取推荐综合实训案例列表接口
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<SynthesizeTrainingDto>> GetSynthesizeTrainingList(PageRequest model);

        /// <summary>
        /// 获取推荐情景实训列表接口
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<HomeSceneTrainingDto>> GetSceneTrainingList(PageRequest model);

        /// <summary>
        /// 获取推荐课程列表接口
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<CourseTrainingDto>> GetCourseTrainingList(PageRequest model);

        /// <summary>
        /// 获取课程详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<CourseTrainingDto>> GetCourseInfoById(long id);

        /// <summary>
        /// 推荐案例列表接口
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<CaseTrainingDto>> GetCaseTrainingList(PageRequest model);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ip"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ReadStatistics(long id, string ip);

        /// <summary>
        /// 511提醒
        /// </summary>
        /// <param name="moblie"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Remind11(string moblie);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<long>> GetReadCount();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<ReadStatisticsDto>>> GetReadList();
    }
}
