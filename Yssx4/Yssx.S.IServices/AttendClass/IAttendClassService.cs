﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 上课服务接口
    /// </summary>
    public interface IAttendClassService
    {
        /// <summary>
        /// 上课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> CreateAttendClassRecord(AttendClassDto dto, UserTicket user);

        /// <summary>
        /// 获取课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxClassRecordViewModel>>> GetClassRecordList(YssxClassRecordQuery query, UserTicket user);

        /// <summary>
        /// 删除课堂记录
        /// </summary>
        /// <param name="identifyCode"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveClassRecord(long identifyCode, UserTicket user);

        /// <summary>
        /// 导出课堂记录
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<IActionResult> ExportClassRecord(YssxClassRecordQuery query, UserTicket user);

    }
}
