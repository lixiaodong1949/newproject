﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.Pocos;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 上传案例接口服务
    /// </summary>
    public interface IUploadCaseService
    {

        #region 上传案例
        /// <summary>
        /// 添加 更新 案例
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateCase(UploadCaseDto dto, long Id);

        /// <summary>
        /// 删除 案例
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteUpdateCase(long Id, long oId);

        /// <summary>
        /// 获取 案例列表
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<PageResponse<UploadCaseResult>> FindUpdateCase(UploadCaseQueryDao model,long Id);

        /// <summary>
        /// 获取 商城案例列表--无用户登陆
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<NoUserUploadCaseListResponse>> NoUserUploadCaseList(NoUserUploadCaseQueryDao model);

        /// <summary>
        /// 获取 商城案例预览--无用户登陆
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<NoUserUploadCaseResponse>> NoUserUploadCase(long Id);

        /// <summary>
        /// 上架 案例 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> CaseRack(CaseRackDao model, long oId);

        /// <summary>
        /// 下架 案例
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> CaseFrame(long Id, long oId);

        /// <summary>
        /// 撤销 案例
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Revoke(long Id, long oId);

        /// <summary>
        /// 预览 案例
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<UploadCaseResponse>> Preview(long Id, long oId);

        /// <summary>
        /// 获取 标签列表
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<PageResponse<UploadCaseTag>> GetTagLib(UploadCaseTagLibDao model);

        /// <summary>
        /// 点赞  商城案例
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Fabulous(CaseFabulousDao model, long oId);

        /// <summary>
        /// 上传 证书
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Certificate(CertificateDao model, long oId);

        /// <summary>
        /// 获取 商城好文推荐
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<GoodArticle>> GoodCase();

        /// <summary>
        /// 更新 订单交易记录和状态
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> OrderTransactions(long Id, long oId);

        /// <summary>
        /// 获取 交易记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<TransactionRecords>> GetTransactionRecords(TransactionRecordsDao model);

        /// <summary>
        /// 添加 更新 批注(追加批注)
        /// </summary>
        /// <param name="md"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateComments(CommentsDao md, long Id);

        /// <summary>
        ///  获取 批注
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CommentsResponse>>> GetComments(long Id);

        /// <summary>
        /// 删除 批注
        /// </summary>
        /// <param name="md"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteComments(DeleteCommentsDao md, long oId);

        /// <summary>
        /// 获取 购买案例
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<PageResponse<UploadCaseResponse>> FindPurchaseCase(PurchUploadCaseDao model, long oId);

        /// <summary>
        /// 批量 上传案例
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BatchUploadCase(BatchUploadCaseDao model, long oId);
        #endregion

        #region 生成试卷
        /// <summary>
        /// 购买后自动生成试卷 - 经典案例
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AutoGenerateTestPaper(AutoGenerateTestPaperDto model);
        #endregion

    }
}
