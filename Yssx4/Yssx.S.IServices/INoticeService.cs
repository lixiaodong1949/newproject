﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 公告
    /// </summary>
    public interface INoticeService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEdit(NoticeDto dto);

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<NoticeDto>>> GetList();

        /// <summary>
        /// 设置状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetStatus(long id, Status status);

        /// <summary>
        /// 根据平台获取列表
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        Task<ResponseContext<List<NoticeDto>>> GetListByPlatform(int platform);
    }
}
