﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 课件库服务
    /// </summary>
    public interface ICourceFilesService
    {
        /// <summary>
        /// 新增课件
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditCourceFile(YssxCourceFilesDto dto, UserTicket user);

        /// <summary>
        /// 获取课件列表
        /// </summary>
        /// <param name="user"></param>
        /// <param name="userType">0:普通用户,1:审核用户</param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxCourceFilesDto>>> GetCourceFilesList(UserTicket user,int userType);

        /// <summary>
        ///  获取课件列表带分页
        /// </summary>
        /// <param name="user"></param>
        /// <param name="keyword"></param>
        /// <param name="fileType"></param>
        /// <param name="status"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourceFilesDto>> GetCourceFilesListByPage(UserTicket user,string keyword,int fileType,int status, int pageIndex,int pageSize);

        /// <summary>
        /// 搜索课件
        /// </summary>
        /// <param name="user"></param>
        /// <param name="fileName">名称</param>
        /// <param name="fileType">文件类型</param>
        /// <param name="klgId">知识点Id</param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourceFilesDto>> SearchFilesListByPage(UserTicket user, string fileName, int fileType, long klgId, int pageIndex, int pageSize);

        /// <summary>
        /// 获取单个课件
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<YssxCourceFilesDto>> GetCourceFileOne(long id);

        /// <summary>
        /// 删除课件
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveCourceFile(long uid,string ids);

        /// <summary>
        /// 获取课件列表(购买/上传)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseFilesViewModel>> GetCourseFilesByBuyOrOneself(YssxCourseFilesQuery query, UserTicket user);

        /// <summary>
        /// 新增资源文件
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> AddOrEditResourceFile(YssxCourceFilesDto dto, UserTicket user);
    }
}
