﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Pocos;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 验证码
    /// </summary>
    public interface IRegisteredService
    {
        /// <summary>
        /// 发送手机验证码
        /// </summary>
        /// <returns></returns>
        //Task<ResponseContext<bool>> SendVerificationCode(string mb);
        /// <summary>
        /// 校验手机验证码
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> CheckVerificationCode(string mb, string verificationCode);
        /// <summary>
        /// 验证手机号码
        /// </summary>
        /// <param name="mb">手机号码</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> VerifyPhoneNumber(string mb);
        /// <summary>
        /// 注册用户
        /// </summary>
        /// <param name="mb">电话号码</param>
        /// <param name="password">密码</param>
        /// <param name="againPassword">再次输入的密码</param>
        /// <param name="nikename">昵称</param>
        /// <returns></returns>
        Task<ResponseContext<YssxUser>> RegisteredUsers(string mb, string password, string againPassword, string nikename);

        /// <summary>
        /// 用户注册(学生身份) 通过教师邀请 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<YssxUser>> RegisterStudentByInvite(RegisterUserRequestModel dto);
        
    }
}
