﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 课程下备课接口
    /// </summary>
    public interface ILessonPlanningService
    {
        /// <summary>
        /// 新增/编辑课程备课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> AddOrEditLessonPlanning(LessonPlanningRequestModel dto, UserTicket user);

        /// <summary>
        /// 获取课程备课列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxLessonPlanningViewModel>>> GetLessonPlanningList(YssxLessonPlanningQuery query);

        /// <summary>
        /// 删除课程备课
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteLessonPlanning(List<long> ids, UserTicket user);

        /// <summary>
        /// 新增课程备课素材、教案
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddLessonPlanningFiles(LessonPlanningFilesRequestModel dto, UserTicket user);

        /// <summary>
        /// 获取课程备课素材、教案列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxLessonPlanningFilesViewModel>>> GetLessonPlanningFilesList(YssxLessonPlanningFilesQuery query);

        /// <summary>
        /// 删除课程备课素材、教案
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveLessonPlanningFiles(long id, UserTicket user);

        /// <summary>
        /// 新增课程备课习题
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddLessonPlanningTopic(LessonPlanningTopicRequestModel dto, UserTicket user);

        /// <summary>
        /// 获取课程备课习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxLessonPlanningTopicViewModel>>> GetLessonPlanningTopicList(YssxLessonPlanningInfoQuery query);

        /// <summary>
        /// 删除课程备课习题
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveLessonPlanningTopic(long id, UserTicket user);

        /// <summary>
        /// 获取课程备课素材、习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxLessonPlanningCdrAndTopicViewModel>>> GetLessonPlanningCdrAndTopicList(YssxLessonPlanningInfoQuery query);

        /// <summary>
        /// 更新课程备课素材、习题排序
        /// </summary>
        /// <param name="list"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateLessonPlanningCdrAndTopicSort(List<LessonPlanningCdrAndTopicRequestModel> list, UserTicket user);

    }
}
