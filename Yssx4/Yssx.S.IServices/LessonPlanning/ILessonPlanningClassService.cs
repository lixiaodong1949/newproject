﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 教师班级管理接口
    /// </summary>
    public interface ILessonPlanningClassService
    {
        /// <summary>
        /// 教师绑定班级
        /// </summary>
        /// <param name="requestList">班级列表</param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BindingTeacherClassRelation(List<TeacherClassRequestModel> requestList, UserTicket user);

        /// <summary>
        /// 教师绑定班级(App)
        /// </summary>
        /// <param name="requestList">班级列表</param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BindingTeacherClassRelationForApp(List<TeacherClassRequestModel> requestList, UserTicket user);

        /// <summary>
        /// 解绑单一教师班级
        /// </summary>
        /// <param name="teacherClassId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UnlinkTeacherClass(long teacherClassId, UserTicket user);

        /// <summary>
        /// 获取教师班级列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxTeacherClassRelationViewModel>>> GetTeacherClassRelationList(YssxTeacherClassRequestModel query, UserTicket user);

        /// <summary>
        /// 根据班级获取班级下学生信息
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxTeacherClassStudentViewModel>>> GetStudentListByClassId(YssxTeacherClassStudentRequestModel query);

        /// <summary>
        /// 获取教师下所有班级学生
        /// </summary>
        /// <param name="year">入学年份</param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<AllClassStudentByTeacherViewModel>>> GetTeacherAllStudentList(int year, UserTicket user);

        /// <summary>
        /// 根据班级Ids获取学生Id、Name
        /// </summary>
        /// <param name="classIds"></param>
        /// <returns></returns>
        Task<ResponseContext<List<StudentInfoByClassIdsViewModel>>> GetStudentNameByClassIds(List<long> classIds);

        /// <summary>
        /// 学生退班(通过教师操作)
        /// </summary>
        /// <param name="studentUserId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> StudentQuitClassByTeacher(long studentUserId, UserTicket user);

        /// <summary>
        /// 获取教师是否有课程、班级(是否可以发布任务)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<TeacherIsCanPublishTask>> GetTeacherIsCanPublishTask(UserTicket user);

        /// <summary>
        /// 编辑学生信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> EditStudentByTeacher(EditStudentByTeacherRequestModel request, UserTicket user);

        /// <summary>
        /// 教师导入学生名单
        /// </summary>
        /// <param name="file"></param>
        /// <param name="classId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<ImportStudentResultModel>> ImportStudentByTeacher(IFormFile file, long classId, UserTicket user);

        /// <summary>
        /// 教师手动添加学生
        /// </summary>
        /// <param name="request"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ManualAddStudentByTeacher(TeacherAddStudentRequestModel request, UserTicket user);

    }
}
