﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 自然人
    /// </summary>
    public interface INaturalPersonsService
    {
        /// <summary>
        ///获取列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<GetListResponse>> GetPersonnelList(PersonnelListDao model, long Id);

        /// <summary>
        /// 查看资料
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<ViewInfoResponse>> GetPersonnelDataList(PersonnelListDao model);

        /// <summary>
        /// 添加 更新
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdate(NotesDao model, long Id);

        /// <summary>
        /// 报送
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Submitted(IdsDao model, long oId);

        /// <summary>
        /// 获取反馈
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Feedback(IdsDao model, long oId);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Delete(IdsDao model, long oId);

        /// <summary>
        /// 清空重做
        /// </summary>
        /// <param name="CaseId"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Redo(long CaseId, long oId);




        /// <summary>
        /// 获取 人员列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<PersonnelListResponse>> GetPersonnelbkList(PersonnelListBkDao model);

        /// <summary>
        /// 获取 子女教育列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<ChildrenListResponse>> GetChildrenbkList(DeductionIncomeListBkDao model);

        /// <summary>
        /// 获取 继续教育支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<ContinueListResponse>> GetContinuebkList(DeductionIncomeListBkDao model);

        /// <summary>
        /// 获取 住房贷款利息支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<InterestListResponse>> GetInterestbkList(DeductionIncomeListBkDao model);

        /// <summary>
        /// 获取 住房租金支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<RentListResponse>> GetRentbkList(DeductionIncomeListBkDao model);

        /// <summary>
        /// 获取 赡养老人支出列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<SupportListResponse>> GetSupportbkList(DeductionIncomeListBkDao model);

        /// <summary>
        /// 获取 综合所得申报列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<IncomeListResponse>> GetIncomebkList(DeductionIncomeListBkDao model);



        /// <summary>
        /// 添加 更新 人员
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdatePersonnel(PersonnelDao model, long oid);

        /// <summary>
        ///  添加 更新 子女教育支出
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateChildren(ChildrenDao model, long oid);

        /// <summary>
        /// 添加 更新 继续教育支出
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateContinue(ContinueDao model, long oid);

        /// <summary>
        /// 添加 更新 住房贷款利息支出
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateInterest(InterestDao model, long oid);

        /// <summary>
        /// 添加 更新 住房租金支出
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateRent(RentDao model, long oid);

        /// <summary>
        /// 添加 更新 赡养老人支出
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateSupport(SupportDao model, long oid);

        /// <summary>
        /// 添加 更新 综合所得申报
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oid"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateIncome(IncomeDao model, long oid);



        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BkDelete(IdDao model, long oId);



        /// <summary>
        /// 批量导入 自然人各模块
        /// </summary>
        /// <param name="form"></param>
        /// <param name="caseid"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ImportNaturalPersons(IFormFile form, long caseid, long Id);
    }
}
