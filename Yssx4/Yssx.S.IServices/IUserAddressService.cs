using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.S.Dto;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 用户收货地址接口服务类
    /// </summary>
    public interface IUserAddressService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> Save(UserAddressDto model, long currentUserId);

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<UserAddressDto>> Get(long id);

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> Delete(long currentUserId, params long[] id);

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<UserAddressListResponseDto>> List(long userId);
    
        /// <summary>
        /// 设置默认收货地址
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetDefault(long id, long currentUserId);
    
        /// <summary>
        /// 获取用户默认收货地址
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<UserDefaultAddressDto>> GetUserDefaultAddress(long userId);
    }
}