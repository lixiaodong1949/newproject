﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 岗位人员服务接口
    /// </summary>
    public interface IPostPersonService
    {
        /// <summary>
        /// 保存岗位人员
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SavePostPerson(PostPersonRequest model,long userId);
        /// <summary>
        /// 删除岗位人员
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeletePostPerson(long id);
        /// <summary>
        /// 获取岗位人员列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<PostPersonDto>> GetPostPersonList(PostPersonListRequest model);
        /// <summary>
        /// 调整顺序  isAscending:true升序   false降序
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ChangeSort(long id, bool isAscending = true);
    }
}
