﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 部门服务接口
    /// </summary>
    public interface IDepartmentService
    {
        /// <summary>
        /// 保存部门
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveDepartment(DepartmentRequest model,long userId);
        /// <summary>
        /// 删除部门
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteDepartment(long id);
        /// <summary>
        /// 获取部门列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<DepartmentDto>> GetDepartmentList(DepartmentListRequest model);
        /// <summary>
        /// 调整顺序
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ChangeSort(long id, bool isAscending = true);
    }
}
