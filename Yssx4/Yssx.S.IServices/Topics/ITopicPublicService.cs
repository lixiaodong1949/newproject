﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices.Topics
{
    /// <summary>
    /// 公共题目相关接口
    /// </summary>
    public interface ITopicPublicService
    {
        #region 添加题目

        /// <summary>
        /// 添加 更新 题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> SaveSimpleQuestion(SimpleQuestionPubRequest model, long currentUserId);

        /// <summary>
        /// 添加 更新 复杂题目（分录题、表格题、案例题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> SaveComplexQuestion(ComplexQuestionPubRequest model, long currentUserId);

        /// <summary>
        /// 添加 更新 综合题
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> SaveMainSubQuestion(MainSubQuestionPubRequest model, long currentUserId);

        #endregion

        #region 获取题目信息

        /// <summary>
        /// 获取题目信息
        /// </summary>
        /// <param name="id">题目ID</param>
        /// <returns></returns>
        Task<ResponseContext<TopicPubInfoDto>> GetQuestion(long id);

        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<TopicPubListView>> GetQuestionByPage(GetQuestionByPageRequest model, long currentUserId);

        /// <summary>
        /// 查询题目列表 - 所有人创建的
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<TopicPubListView>> GetAllQuestionByPage(GetPubQuestionByPageRequest model);

        #endregion

        #region 删除题目、选项、文件
        /// <summary>
        /// 删除题目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteQuestion(long id, long currentUserId);

        /// <summary>
        /// 删除附件
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTopicFile(long id, long currentUserId);

        /// <summary>
        /// 删除选项
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteOption(long topicId, long id, long currentUserId);

        /// <summary>
        /// 设置题目状态
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="status">0：禁用,1:启用</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetQuestionStatuts(long topicId, int status, long currentUserId);

        #endregion

    }
}
