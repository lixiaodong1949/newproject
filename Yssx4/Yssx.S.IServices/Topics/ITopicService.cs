﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;

namespace Yssx.S.IServices.Topics
{
    /// <summary>
    /// 案例题目相关接口 
    /// </summary> 
    public interface ITopicService
    {
        #region 添加题目

        /// <summary>
        /// 添加 更新 题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveSimpleQuestion(SimpleQuestionRequest model, UserTicket CurrentUser);

        /// <summary>
        /// 添加 更新 复杂题目（分录题、表格题、案例题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveComplexQuestion(ComplexQuestionRequest model, UserTicket CurrentUser);

        /// <summary>
        /// 添加 更新 综合题
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveMainSubQuestion(MainSubQuestionRequest model, UserTicket CurrentUser);

        #endregion

        #region 获取题目信息

        /// <summary>
        /// 获取题目信息
        /// </summary>
        /// <param name="id">题目ID</param>
        /// <returns></returns>
        Task<ResponseContext<TopicInfoDto>> GetQuestion(long id);

        /// <summary>
        /// 根据CaseId查询题目
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<TopicStatisticsView>> GetQuestionByCase(GetQuestionByCaseRequest model);

        /// <summary>
        /// 获取图表题导入的EXCEL数据
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<TopicFillGridDto>>> GetQuestionInFillGrid(long caseId);

        /// <summary>
        /// 查询缓存题目列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<TopicListView>>> GetQuestionByCache(long caseId);

        #endregion

        #region 删除题目、选项、文件
        /// <summary>
        /// 删除题目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteQuestion(long id, UserTicket CurrentUser);

        /// <summary>
        /// 删除附件
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTopicFile(long id, UserTicket CurrentUser);

        /// <summary>
        /// 删除选项
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteOption(long topicId, long id, UserTicket CurrentUser);

        /// <summary>
        /// 设置题目状态
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="status">0：禁用,1:启用</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetQuestionStatuts(long topicId, int status, UserTicket CurrentUser);


        ///// <summary>
        ///// 模板导入
        ///// </summary>
        ///// <returns></returns>
        //Task<ResponseContext<DataTable>> ImportExcelData(List<HttpFormFile> form);
        #endregion

        #region 修改题目信息 - 案例操作

        /// <summary>
        /// 案例修改分值题目获取
        /// </summary>
        /// <param name="CaseId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<TopicInfoDto>>> GetQuestionByCaseId(long CaseId);

        /// <summary>
        /// 案例修改分值批量修改
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateQuestionByCaseId(List<TopicInfoDto> model);

        /// <summary>
        /// 一键修改分数
        /// </summary>
        /// <param name="caseId">案例Id</param>
        /// <param name="x">比例值</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetQuetionScore(long caseId, decimal x);
        #endregion
    }
}
