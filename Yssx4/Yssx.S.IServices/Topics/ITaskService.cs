﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Task;

namespace Yssx.S.IServices.Topics
{
    /// <summary>
    /// 课业任务管理
    /// </summary>
    public interface ITaskService
    {
        #region 课业任务

        #region old
        /// <summary>
        /// 查询课业任务列表 - 教师端
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<TaskView>> GetTaskByPage(GetTaskByPageRequest model, long currentUserId);

        /// <summary>
        /// 查询课业任务列表 - 学生端
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<StudentTaskView>> GetStudentTaskByPage(GetTaskByPageRequest model, long currentUserId);

        /// <summary>
        /// 查询指定课业任务信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<TaskInfoDto>> GetTaskById(long taskId, long currentUserId);

        /// <summary>
        /// 修改课业任务
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> EditTask(TaskDto model, long currentUserId);

        #endregion

        #region new
        /// <summary>
        /// 查询课业任务列表 - 教师端 - new
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<TaskView>> GetTaskByPageAll(GetTaskByPageAllRequest model, long currentUserId);

        /// <summary>
        /// 查询课业任务列表 - 学生端 - new
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<StudentTaskView>> GetStudentTaskByPageAll(GetTaskByPageAllRequest model, long currentUserId);

        /// <summary>
        /// 查询指定课业任务信息 - new
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<TaskInfoDto>> GetTaskInfoById(long taskId, long currentUserId);

        /// <summary>
        /// 修改课业任务 - new
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> EditTaskInfo(TaskDto model, long currentUserId);

        #endregion

        /// <summary>
        /// 查询指定课业任务状态
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<LessonsTaskStatus>> GetTaskStatusById(long taskId, long GradeId);

        /// <summary>
        /// 删除课业任务
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveTask(long id, long currentUserId);

        /// <summary>
        /// 结束课业任务
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> ClosedTask(long id, long currentUserId);

        #endregion

        #region 组卷 - old
        /// <summary>
        /// 手动组卷
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> ManualOrgPaper(TaskManualOrgPaperDto model, long currentUserId, long tenantId);


        /// <summary>
        /// 查询题目类型数量统计（1.知识点 2.课程章节）
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<TaskTopicGroupDto>> GetTaskTopicById(GetTaskTopicByIdRequest model, long currentUserId);

        /// <summary>
        /// 智能组卷
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RandomOrgPaper(TaskRandomOrgPaperDto model, long currentUserId, long tenantId);

        #endregion

        #region 组卷 - new
        /// <summary>
        /// 手动组卷 - new
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> ManualCreatePaper(TaskManualCreatePaperDto model, long currentUserId, long tenantId);


        /// <summary>
        /// 查询题目类型数量统计（1.知识点 2.课程章节） - new
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<TaskTopicGroupDto>> GetTaskTopicGroup(GetTaskTopicGroupRequest model, long currentUserId);

        /// <summary>
        /// 智能组卷 - new
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RandomCreatePaper(TaskRandomCreatePaperDto model, long currentUserId, long tenantId);

        #endregion

        #region 课业统计
        /// <summary>
        /// 课业任务统计信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<TaskGradeReportDto>> GetTaskGradeReport(long taskId, long currentUserId);

        /// <summary>
        /// 课业任务统计信息 - 移动端
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<TaskGradeReportGradeDto>> GetTaskGradeReportByApp(TaskGradeReportByAppDto model);

        /// <summary>
        /// 导出课业任务统计数据
        /// </summary>
        /// <returns></returns>      
        Task<ResponseContext<TaskGradeReportDto>> ExportTaskGrade(long taskId);

        #endregion

        #region 课堂任务

        /// <summary>
        /// 查询课堂测验列表 - 教师端 - new
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<TestingTaskView>> GetTestingTaskByPage(GetTestingTaskByPageDto model, long currentUserId);

        /// <summary>
        /// 查询课堂测验列表 - 学生端 - new
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<PageResponse<StudentTestingTaskView>> GetStudentTestingTaskByPage(GetTestingTaskByPageDto model, long currentUserId);


        #region old
        /// <summary>
        /// 开始做题
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<string>> StartCourseTestingTask(TaskDto model, long currentUserId);

        /// <summary>
        /// 收题
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<CourseTestingTaskAnalysis>> StopCourseTestingTask(List<long> TaskID, long currentUserId);

        /// <summary>
        /// 刷新查看
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<CourseTestingTaskAnalysis>> RefreshCourseTestingTask(List<long> TaskID);

        ///// <summary>
        ///// 讲解习题
        ///// </summary>
        ///// <returns></returns>
        //Task<ResponseContext<object>> ExplainCourseTestingTask(long TopicID);

        /// <summary>
        /// 获取课堂任务题目列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<object>> GetCourseTestingTaskTopicList(long TaskID);

        /// <summary>
        /// 获取我的任务课程
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<MyCourseTaskDto>> GetMyCourseTaskByPage(long currentUserId, int pageIndex, int pageSize);

        /// <summary>
        ///  获取课程任务试卷题目列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourseTaskExamTopicList(long TaskID);
        #endregion

        #endregion


        /// <summary>
        /// 重新开启任务
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetTaskStatus(long taskId, DateTime endDate, bool isCanCheckExam);

        /// <summary>
        /// 开启学生试卷做题状态
        /// </summary>
        /// <param name="gradeId"></param>

        /// <returns></returns>
        Task<ResponseContext<bool>> SetStudentStatus(long gradeId);

        Task<ResponseContext<TaskGradeReportDto>> GetCourseGradeDetailByTask(long taskId,string classIds, long currentUserId);

        /// <summary>
        /// 一键交卷
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> BatchSubmitExam(long id, long currentUserId);

        /// <summary>
        /// 打开或关闭课程任务学生查看试卷入口
        /// </summary>
        /// <param name="request"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> OpenOrCloseCourseTaskCheckExam(CloseCheckExamRequestModel request, long currentUserId);

    }
}
