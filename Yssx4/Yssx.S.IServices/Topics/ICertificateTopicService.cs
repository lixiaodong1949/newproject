﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.IServices.Topics
{
    /// <summary>
    /// 凭证录入
    /// </summary>
    public interface ICertificateTopicService
    {
        /// <summary>
        /// 添加凭证（分录题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveCertificateTopic(CertificateTopicRequest model);
        /// <summary>
        /// 获取单个分录题
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<CertificateTopicRequest>> GetCoreCertificateTopic(long id);
        /// <summary>
        /// 根据凭证id删除凭证(分录题)
        /// </summary>
        /// <param name="id">分录题id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCertificateTopicById(long id);

        /// <summary>
        /// 根据题库id删除凭证(分录题)
        /// </summary>
        /// <param name="id">题库id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCertificateTopicByTopicId(long id);
    }
}
