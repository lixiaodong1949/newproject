﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Topics;

namespace Yssx.S.IServices.Topics
{
    public interface ICaseService
    {
        /// <summary>
        /// 查询单个对象
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<ResponseContext<CaseDto>> GetCase(long caseId);
        /// <summary>
        /// 查询案例列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<CaseDto>> GetCaseList(CaseListRequest model);
        /// <summary>
        /// 查询案例列表+区域
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<CaseDto>> GetCaseAreaList(CaseListRequest model);
        /// <summary>
        /// 保存案例
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveCase(CaseRequest model);
        /// <summary>
        /// 删除案例
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCase(long caseId);
        /// <summary>
        /// 查找所有，返回案例id和名称
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<CaseNameDto>>> GetCaseNameList();
        /// <summary>
        /// 复制案例
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> CloneCase(CloneCaseRequest model,long userId);
        /// <summary>
        /// 一键更新案例下分录题的AnswerValue
        /// </summary>
        /// <param name="caseId">id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ReplaceAnswerValue(long caseId,long userId);
        /// <summary>
        /// 设置案例的区域
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetCaseRegion(long id, string region,long userId);
        /// <summary>
        /// 设置案例场景实训封面图
        /// </summary>
        /// <param name="id"></param>
        /// <param name="fileUrl"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetCaseSceneImg(long id,string fileUrl);

        /// <summary>
        /// 获取教师购买/赠送的实训套题列表(只获取有行业的套题)
        /// </summary>
        /// <param name="user"></param>

        /// <returns></returns>
        Task<ResponseContext<List<YssxJobTrainingViewModel>>> GetTeacherJobTrainingList(UserTicket user);

        /// <summary>
        ///  获取教师购买实训套题列表(只获取有行业的套题)
        /// </summary>
        /// <param name="user"></param>
        /// <param name="year">入学年份</param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxJobTrainingViewModel>>> GetTeacherJobTrainingList(UserTicket user,int year);

        /// <summary>
        /// 获取教师购买/赠送的实训套题列表(App使用)(只获取有行业的套题)
        /// </summary>
        /// <param name="user"></param>
        /// <param name="year">入学年份</param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxJobTrainingViewModelForApp>>> GetTeacherJobTrainingListForApp(UserTicket user,int year);

    }
}
