﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.CourseExam;
using Yssx.S.Dto.ExamPaper;

namespace Yssx.S.IServices.Examination
{
    /// <summary>
    /// 课程练习做题相关服务
    /// </summary> 
    public interface ICourseExercisesService
    {
        /// <summary>
        /// 获取当前用户课程（自己的课程和购买的课程）
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CourseInfoDto>>> GetCourseInfoByUser(UserTicket currentUser);
        /// <summary>
        /// 根据课程id获取章节信息
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        Task<PageResponse<SectionInfoDto>> GetSectionListByCourse(long courseId, int pageSize, int pageIndex, UserTicket user);

        /// <summary>
        /// 根据章Id获取节信息
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SubSectionInfoDto>>> GetSubSectionList(long sectionId, long userId);

        /// <summary>
        /// 获取试卷信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourseExamPaperBasicInfo(long courseId, long sectionId, UserTicket user, long gradeId = 0);

        /// <summary>
        /// 获取任务试卷信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperCourseBasicInfo>> GetTaskExamPaperBasicInfo(long taskId, UserTicket user, long gradeId = 0);

        /// <summary>
        /// 获取题目信息--接口方法
        /// </summary>
        /// <param name=""></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamCourseQuestionInfo>> GetCourseQuestionInfo(long gradeId, CourseExamType examType, long questionId, UserTicket userTicket);

        /// <summary>
        /// 预览题目作答信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamCourseQuestionInfo>> GetCourseQuestionInfoView(long gradeId, long questionId, UserTicket userTicket);

        /// <summary>
        /// 提交答案--接口方法
        /// </summary>
        /// <param name="answer">作答信息</param>
        /// <returns>作答成功信息</returns>
        Task<ResponseContext<QuestionResult>> SubmitAnswer(CourseQuestionAnswer model, CourseExamType examType, UserTicket userTicket);
        /// <summary>
        /// 获取未答题数
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        Task<ResponseContext<int>> GetBlankCount(long gradeId);
        /// <summary>
        /// 提交考卷---接口方法 
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        Task<ResponseContext<GradeInfo>> SubmitExam(long gradeId, UserTicket user);

        /// <summary>
        /// 预览试卷基本信息--接口方法
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperCourseBasicInfo>> GetTaskExamPaperInfoView(long examId, UserTicket userTicket);

        /// <summary>
        /// 获取当前用户所在备课的课程
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<List<PrepareCourseInfoDto>>> GetPrepareCourseInfoByUser(UserTicket currentUser);
        /// <summary>
        /// 根据备课课程id获取章节信息
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        Task<PageResponse<SectionInfoDto>> GetPrepareSectionListByCourse(long courseId, int pageSize, int pageIndex, UserTicket user);
        /// <summary>
        /// 获取试卷信息--接口方法（包括试卷基本信息和试卷包含的题目信息）备课课程练习
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperCourseBasicInfo>> GetPrepareCourseExamPaperBasicInfo(long courseId, long sectionId, UserTicket user, long gradeId = 0);

        /// <summary>
        /// 预览试卷作答信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperCourseBasicInfo>> GetExamPaperGradeInfoView(CourseExamType examType, long gradeId, UserTicket userTicket);

        /// <summary>
        /// 课题任务获取试卷信息
        /// </summary>
        /// <param name="examId"></param>
        /// <param name="gradeId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourceOnlineExamPaperBasicInfo(long examId, long gradeId, UserTicket user);

        /// <summary>
        /// 课程实训历史详情
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<ExamCourseDetailsDto>>> GetExamCourseDetailsList(long sectionId, UserTicket user);
        /// <summary>
        /// 课程练习/预览
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<CourseSectionInfoDto>> GetCourseInfoByCourseId(long courseId, long userId);

        /// <summary>
        /// 根据章节Id获取节作答信息
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<SubSectionInfoDto>> GetSectionExamInfo(long sectionId, long userId);

        /// <summary>
        /// 案例分析题打分
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SubmitAnswerByTeacher(ShortQuestionAnswer answer, long userId);

        /// <summary>
        /// 根据任务查询简答题列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        Task<ResponseContext<TaskShortQuestionDto>> GetShortQuestionListByTask(long taskId);
        /// <summary>
        /// 根据案例分析题ID获取学生作答记录列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<ShortQuestionAnswerDto>>> GetStudentAnswerList(long taskId, long questionId, long classId, int answerStatus, string keywords);
    }
}
