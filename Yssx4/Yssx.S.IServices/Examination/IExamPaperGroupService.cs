﻿using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto.ExamPaper;

namespace Yssx.S.IServices.Examination
{
    /// <summary>
    /// 考试分组相关服务 
    /// </summary> 
    public interface IExamPaperGroupService
    {
        /// <summary>
        /// 获取考试分组列表 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<ExamPaperGroupListView>> GetExamPaperGroupListByPage(PageRequest<ExamPaperGroupQueryDto> model,long userId);

        /// <summary>
        /// 获取考试分组信息
        /// </summary>
        /// <param name="model">groupId</param>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperGroupListView>> GetExamPaperGroupInfo(PageRequest<long> model,long userId);

        /// <summary>
        /// 创建分组 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperGroupListView>> CreateExamGroup(ExamPaperGroupUpdateDto model, UserTicket userTicket);

        /// <summary>
        /// 加入分组 
        /// </summary>
        /// <param name="groupId">分组id</param>
        /// <param name="postionId">岗位id</param>
        /// <param name="password">分组密码</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> JoinExamGroup(long groupId, long postionId, UserTicket userTicket,string password = "");

        /// <summary>
        /// 切换岗位 
        /// </summary>
        /// <param name="groupId">分组id</param>
        /// <param name="postionId">岗位id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ChangeExamGroupPosition(long groupId, long postionId, UserTicket userTicket);

        /// <summary>
        /// 退出分组(如果是创建人退出，则解散分组)
        /// </summary>
        /// <param name="groupId">分组Id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ExitExamGroup(long groupId, long userId);

        /// <summary>
        /// 更改准备状态
        /// </summary>
        /// <param name="groupId">groupId</param>
        /// <param name="IsReady">true:已准备，false:未准备</param>
        /// <returns>bool,true表示分组成员全部准备完毕，否则未全部准备</returns>
        Task<ResponseContext<bool>> UpdateReadyStatus(long groupId, UserTicket userTicket,bool IsReady = false);

    }
}
