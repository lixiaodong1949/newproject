﻿using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.ExamPaper;

namespace Yssx.S.IServices.Examination
{
    /// <summary>
    /// 用户练习做题相关服务
    /// </summary> 
    public interface IUserExercisesService
    {
        #region 答题相关接口

        /// <summary>
        /// 获取题目信息
        /// </summary>
        /// <param name="progressId"></param>
        /// <param name="exercisesType"></param>
        /// <param name="questionId"></param>
        /// <param name="userTicket"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfo(long progressId, ExercisesType exercisesType, long questionId, UserTicket userTicket);

        /// <summary>
        /// 提交答案
        /// </summary>
        /// <param name="model">作答信息</param>
        /// <returns>作答成功信息</returns>
        Task<ResponseContext<QuestionResult>> SubmitAnswer(QuestionAnswer model, long caseId, ExercisesType exercisesType, UserTicket userTicket);

        #endregion
        /// <summary>
        /// 通关记录新增
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddGradeRecord(OaxGradeRecordRequestDto model);
    }
}
