﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.ExamPaper;
using Yssx.S.Dto.SceneTraining;

namespace Yssx.S.IServices.Examination
{
    /// <summary>
    /// 学生端考试相关服务
    /// </summary> 
    public interface IStudentExamService
    {
        #region 获取试卷列表接口

        /// <summary>
        /// 获取自主练习试卷列表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="CypName"></param>
        /// <param name="IdtName"></param>
        /// <param name="userTicket"></param>
        /// <returns></returns>
        Task<PageResponse<PracticeExamListView>> GetPracticeExamListByPage(Practice model, UserTicket userTicket);

        /// <summary>
        /// 获取真题模拟试卷列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<EmulationExamListView>> GetEmulationExamListByPage(PageRequest model, UserTicket userTicket);

        #endregion

        #region 预览

        /// <summary>
        /// 预览试卷基本信息--接口方法
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperBasicInfo>> GetExamPaperInfoView(long examId, UserTicket userTicket);

        /// <summary>
        /// 预览试卷作答信息--接口方法（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperBasicInfo>> GetExamPaperBasicInfoView(long gradeId, UserTicket userTicket);

        /// <summary>
        /// 预览试卷题目 信息--接口方法
        /// </summary>
        /// <param name="examId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamQuestionInfo>> GetExamQuestionInfoView(long examId, long questionId, UserTicket userTicket);

        /// <summary>
        /// 预览题目作答信息--接口方法
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfoView(long gradeId, long questionId, UserTicket userTicket);
        /// <summary>
        /// 预览题目信息
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <param name="userTicket"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamQuestionInfo>> GetQuestionBasicInfoView(long questionId);
        #endregion

        #region 操作

        /// <summary>
        /// 自主练习清空作答记录
        /// </summary>
        /// <param name="examId">examId>0，表示只清空当前测试的记录，否则清空所有测试记录</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ClearExamGradeInfo(UserTicket userTicket,long examId = 0);

        /// <summary>
        /// 自主练习清空单条作答记录
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ClearGradeInfo(long gradeId, UserTicket userTicket);

        ///// <summary>
        ///// 标记题目（已禁用）
        ///// </summary>
        ///// <param name="gradeId"></param>
        ///// <param name="questionId"></param>
        ///// <returns></returns>
        //Task<ResponseContext<bool>> MarkQuestion(long gradeId, long questionId);

        ///// <summary>
        ///// 分录题退回制单(已禁用 )
        ///// </summary>
        ///// <param name="gradeId"></param>
        ///// <param name="questionId"></param>
        ///// <returns></returns>
        //Task<ResponseContext<AccountEntryStatus>> AccountEntryCancel(long gradeId, long questionId);

        #endregion

        #region 答题相关接口

        /// <summary>
        /// 获取试卷信息（包括试卷基本信息和试卷包含的题目信息）
        /// </summary>
        /// <param name="examId"></param>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperBasicInfo>> GetExamPaperBasicInfo(long examId, UserTicket userTicket, long gradeId = 0);

        /// <summary>
        /// 获取题目信息
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="questionId"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamQuestionInfo>> GetQuestionInfo(long gradeId, long questionId, UserTicket userTicket);

        /// <summary>
        /// 提交答案
        /// </summary>
        /// <param name="model">作答信息</param>
        /// <returns>作答成功信息</returns>
        Task<ResponseContext<QuestionResult>> SubmitAnswer(QuestionAnswer model, UserTicket userTicket);

        /// <summary>
        /// 提交考卷 
        /// </summary>
        /// <param name="gradeId"></param>
        /// <param name="isAutoSubmit">是否自动提交，默认false</param>
        /// <returns></returns>
        Task<ResponseContext<GradeInfo>> SubmitExam(long gradeId, UserTicket userTicket, bool isAutoSubmit = false);

        /// <summary>
        /// 一键交卷
        /// </summary>
        /// <param name="examId"></param>
        Task<ResponseContext<long>> SubmitExamBatch(long examId);

        /// <summary>
        /// 自动交卷-定时任务
        /// </summary>
        Task<ResponseContext<long>> SubmitExamTask(UserTicket userTicket);

        #endregion

        #region 行业,业务场景实训,课程实训
        /// <summary>
        /// 行业下拉列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<IndustryDetails>>> Industry();

        /// <summary>
        /// 4个实训 开关
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="OnOff"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Switch(long Id, bool OnOff, long oId);

        /// <summary>
        /// 业务场景实训列表
        /// </summary>
        /// <param name="dto">搜索</param>
        /// <returns></returns>
        Task<PageResponse<BusinessTraining>> TrainingServiceList(PracticeScene dto, UserTicket user);

        /// <summary>
        /// 获取当前用户课程（自己的课程和购买的课程）
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<PageResponse<CourseInfoList>> GetCourseInfoByUser(PracticeCourse dto,UserTicket currentUser);

        #region 扩展
        /// <summary>
        /// 根据课程id获取章节信息
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        Task<PageResponse<SectionInfoDto>> GetSectionListByCourse(long courseId, int pageSize, int pageIndex, UserTicket user);

        /// <summary>
        /// 根据章Id获取节信息
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<SubSectionInfoDto>>> GetSubSectionList(long sectionId, long userId);
        #endregion
        #endregion

        /// <summary>
        /// 获取会计证课程
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<PageResponse<CourseInfoList>> GetZhenShuCourseList(UserTicket currentUser);

        #region 试卷实训配置
        /// <summary>
        /// 获取 岗位实训列表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userTicket"></param>
        /// <returns></returns>
        Task<ResponseContext<List<PositionPracticeInfo>>> GetPositionPracticeList(PositionPracticeDto model, UserTicket userTicket);

        /// <summary>
        /// 获取 岗位/课程绑定的班级列表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userTicket"></param>
        /// <returns></returns>
        Task<ResponseContext<List<BindClassInfo>>> GetBindClassList(UserTicket userTicket);

        /// <summary>
        /// 添加 更新 岗位/课程实训
        /// </summary>
        /// <param name="model"></param>
        /// <param name="CurrentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdatePosition(PositionBindClassDto model, UserTicket CurrentUser);

        /// <summary>
        /// 获取 课程实训列表
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CoursePracticeInfo>>> GetCoursePracticeList(int type,UserTicket currentUser);

        /// <summary>
        /// 章 根据课程id获取章列表
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <param name="classId">班级Id</param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CourseChapterInfo>>> GetChapterList(long courseId, long classId, UserTicket currentUser);

        /// <summary>
        /// 添加 更新 课程实训
        /// </summary>
        /// <param name="model"></param>
        /// <param name="CurrentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateCheckCourse(CourseBindClassDto model, UserTicket CurrentUser);

        /// <summary>
        /// 获取 自主实训操作记录
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<PageResponse<OperationRecordInfo>> OperationRecord(OperationRecordDto model, UserTicket currentUser);

        /// <summary>
        /// 获取 未绑定列表
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<List<UnboundClassInfo>>> GetUnboundClass(UserTicket currentUser);
        #endregion
    }
}
