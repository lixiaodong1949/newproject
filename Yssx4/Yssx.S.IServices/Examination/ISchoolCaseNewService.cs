﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Topics;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 订单相关服务(给学校添加案例并生成试卷)
    /// </summary>
    public interface ISchoolCaseNewService
    {
        /// <summary>
        /// 获取学校订单列表 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<SchoolListView>> GetSchoolListByPage(CaseOrderListRequest model);

        /// <summary>
        /// 创建订单
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddCaseOrder(CaseOrderRequest model);
        /// <summary>
        /// 删除订单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCaseOrder(long id);
        /// <summary>
        /// 订单开启/禁用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status">状态：0禁用, 1启用 </param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateCaseOrderStatus(long id, Status status, long userId);
        /// <summary>
        /// 获取学校案例列表 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<SchoolCaseListView>> GetSchoolCaseListByPage(SchoolCaseListRequest model);
        /// <summary>
        /// 给学校添加/修改案例（添加是批量，修改时是单条记录，对应的caseids传一个就好了）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SchoolCaseSet(SchoolCaseRequest model, long userId);

        /// <summary>
        /// 删除订单明细
        /// </summary>
        /// <param name="id">订单明细id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteSchoolCase(long id);
        /// <summary>
        /// 订单明细开启/禁用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status">状态：0禁用, 1启用 </param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateSchoolCaseStatus(long id, Status status, long userId);

        /// <summary>
        /// 开启案例（生成试卷）
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> OpenSchoolCase(ShoolCaseStatusSetRequest model, long userId);
    }
}
