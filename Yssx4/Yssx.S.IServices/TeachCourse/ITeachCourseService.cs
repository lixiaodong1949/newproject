﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 课程上课接口
    /// </summary>
    public interface ITeachCourseService
    {
        /// <summary>
        /// 添加教师上课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> AddTeacherTeachCourse(TeacherTeachCourseRequestModel dto, UserTicket user);

        /// <summary>
        /// 获取教师课程上课列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<TeachCourseTeacherViewModel>> GetTeachCourseTeacherList(TeacherTeachCourseQuery query, UserTicket user);

        /// <summary>
        /// 教师手动结束上课
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> FinishTeacherTeachCourse(long id, UserTicket user);

        /// <summary>
        /// 删除课程上课(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTeachCourseById(long id, UserTicket user);

        /// <summary>
        /// 获取课中上课资源列表(文件、题目)【用于上课】
        /// </summary>
        /// <param name="couPrepareId">课程备课Id</param>
        /// <returns></returns>
        Task<ResponseContext<CourseTestResourceViewModel>> GetCourseTestResourceList(long couPrepareId);

        /// <summary>
        /// 删除课程备课题目(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCoursePrepareTopicById(long id, UserTicket user);

        /// <summary>
        /// 发布课堂测验任务
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> PublishCourseTestTask(CourseTestTaskRequestModel dto, UserTicket user);

        /// <summary>
        /// 获取课堂测验题目操作状态
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<CourseTestTopicOperateViewModel>> GetCourseTestTopicOperateStatus(CourseTestTopicOperateQuery query);

        /// <summary>
        /// 教师手动结束测验任务
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> FinishCourseTestTask(long taskId, UserTicket user);

        /// <summary>
        /// 获取学生课堂测验任务列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<StudentCourseTestTaskViewModel>> GetStudentCourseTestTaskList(StudentCourseTestTaskQuery query, UserTicket user);

        /// <summary>
        /// 获取进行中课堂测验详情列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CourseTestDetailsInProgressViewModel>>> GetInProgressCourseTestDetailsList(CourseTestDetailsInProgressQuery query);

        /// <summary>
        /// 获取已结束课堂测验统计列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CourseTestStatisticsFinishedViewModel>>> GetFinishedCourseTestStatisticsList(CourseTestStatisticsFinishedQuery query);

        /// <summary>
        /// 获取已结束课堂测验简答题作答列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CourseTestShortAnswerFinishedViewModel>>> GetFinishedCourseTestShortAnswerList(CourseTestShortAnswerFinishedQuery query);

    }
}
