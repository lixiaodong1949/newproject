using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.S.Dto;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 用户积分接口服务类
    /// </summary>
    public interface IIntegralService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> Save(IntegralDetailsDto model, long currentUserId);

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<IntegralDetailsDto>> Get(long id);

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> Delete(long currentUserId, long id);

        Task<ResponseContext<long>> GetSum(long id);

        /// <summary>
        /// 获取当前用户积分总分和商品兑换总数
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<IntegralStatistics>> GetStatistics(long userid);        

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<IntegralPageResponseDto>> ListPage(IntegralPageRequestDto query);

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<IntegralListResponseDto>> List(IntegralListRequestDto query);

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<IntegralDetailsListPageResponseDto>> ListDetailsPageByUserId(IntegralDetailsListPageRequestDto query);
    
        /// <summary>
        /// 获取用户积分排行
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<IntegralGetTopResponseDto>> GetTop(PageRequest page);

        /// <summary>
        /// 获取用户的积分详情列表信息 分页
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<IntegralDetailsUserPageResponseDto>> DetailsListByUserId(IntegralDetailsUserRequestDto query);
    }
}