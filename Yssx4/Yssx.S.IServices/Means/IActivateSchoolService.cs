﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.Pocos;

namespace Yssx.S.IServices.Means
{
    /// <summary>
    /// 激活学校
    /// </summary>
    public interface IActivateSchoolService
    {
        /// <summary>
        /// 验证学下激活码
        /// </summary>
        /// <param name="schoolCode">学校激活码</param>
        /// <returns></returns>
        Task<ResponseContext<SchoolActivationSuccessfulDto>> VerifySchoolActivationCode(string schoolCode);

        /// <summary>
        /// 验证角色激活码
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<VerifyRoleActivationCodeDto>> VerifyRoleActivationCode(string roleCode, long currentUserId);

        /// <summary>
        /// 填写激活信息
        /// </summary>
        /// <param name="dto">激活信息</param>
        /// <param name="tenantId">租户ID</param>
        /// <returns></returns>
        Task<ResponseContext<FillInActivAtionInformationDto>> FillInActivAtionInformation(YssxActivationInformationDto dto, UserTicket user);

        /// <summary>
        /// 下拉数据
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="typeId">1，学校，2，院系，3，专业，4，班级</param>
        /// <returns></returns>
        Task<ResponseContext<List<DropdownListDto>>> DropdownList(long id, int typeId);

        /// <summary>
        /// 获取用户激活信息
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        Task<ResponseContext<ActivationRecordDto>> ActivationRecord(long userId);

        /// <summary>
        /// 解绑激活码
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> UntieActivationCode(long userId, long currentUserId);


        /// <summary>
        /// 验证角色激活码 - new
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<VerifyRoleActivationCodeDto>> VerifyRoleActCode(string roleCode, int roleType, long currentUserId);

        /// <summary>
        /// 补充激活信息 - new
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<FillInActivAtionInformationDto>> FillInActInformation(ActivationInfoRequest model, long currentUserId, int loginType);

    }
}
