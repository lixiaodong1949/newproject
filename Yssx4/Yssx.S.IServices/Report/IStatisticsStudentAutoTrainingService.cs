﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 统计学生自主实训服务接口
    /// </summary>
    public interface IStatisticsStudentAutoTrainingService
    {
        /// <summary>
        /// 获取学校企业(案例)列表
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SchoolCaseViewModel>>> GetSchoolCaseList(UserTicket user);

        /// <summary>
        /// 获取学生自主实训已完成列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<StudentAutoTrainingFinishedViewModel>> GetStudentAutoTrainingFinishedList(StudentAutoTrainingQuery query, UserTicket user);

        /// <summary>
        /// 导出学生自主实训已完成列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<StudentAutoTrainingFinishedViewModel>>> ExportStudentAutoTrainingFinishedList(StudentAutoTrainingQuery query, UserTicket user);        
    }
}
