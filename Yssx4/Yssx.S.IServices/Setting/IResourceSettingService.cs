﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 资源配置接口
    /// </summary>
    public interface IResourceSettingService
    {

        #region 案例标签
        /// <summary>
        /// 获取案例标签列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<TagDto>> GetTagList(TagRequest model);

        /// <summary>
        /// 新增或修改案例标签
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<long>> AddOrEditTag(TagDto model, long currentUserId);

        /// <summary>
        /// 删除案例标签
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveTag(long id, long currentUserId);

        #endregion

        #region 知识点
        /// <summary>
        /// 获取知识点下拉列表
        /// </summary>
        /// <param name="ignoreId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<KnowledgePointDto>>> GetKnowledgePointList(long ignoreId);

        /// <summary>
        /// 新增，编辑知识点
        /// </summary>
        /// <param name="model"></param>
        /// <param name="CurrentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> AddOrEditKnowledgePoint(KnowledgePointDto model, long CurrentUserId);

        /// <summary>
        /// 删除知识点
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveKnowledgePoint(long id, long currentUserId);

        #endregion

        #region 行业
        /// <summary>
        /// 获取行业列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<IndustryDto>> GetIndustryList(IndustryRequest model);

        /// <summary>
        /// 新增或修改行业
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditIndustry(IndustryDto model, long currentUserId);

        /// <summary>
        /// 删除行业
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveIndustry(long id, long currentUserId);

        #endregion

        #region 业务场景
        /// <summary>
        /// 获取业务场景列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<BusinessSceneDto>> GetBusinessSceneList(BusinessSceneRequest model);

        /// <summary>
        /// 新增或修改业务场景
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditBusinessScene(BusinessSceneDto model, long currentUserId);

        /// <summary>
        /// 删除业务场景
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveBusinessScene(long id, long currentUserId);

        #endregion

        #region 操作日志
        /// <summary>
        /// 获取 操作日志
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<PageResponse<OperationLogDto>> OperationLog(OperationLogRequest model, long oId);
        #endregion

        #region Python接口Token

        /// <summary>
        /// 获取Python接口Token
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<string>> GetPythonToken();
        #endregion

    }
}
