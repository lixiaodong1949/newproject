﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.TrainingCompany;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 实训公司
    /// </summary>
    public interface ITrainingCompanyService
    {
        #region 公司
        /// <summary>
        /// 添加 更新 公司
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateSchool(TrainingCompanyDto model, long Id);
        #endregion
    }
}
