﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 公司新闻
    /// </summary>
    public interface INewsService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditNews(YssxNewsDto dto);

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<YssxNewsDto>>> GetNewsList();

        /// <summary>
        /// 设置状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetStatus(long id,int status);

        /// <summary>
        /// 根据平台获取新闻列表
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxNewsDto>>> GetNewsListByPlatform(int platform);
    }
}
