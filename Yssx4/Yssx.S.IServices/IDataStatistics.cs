﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto.Statistics;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 平台数据统计
    /// </summary>
    public interface IDataStatistics
    {
        /// <summary>
        /// 平台资源统计
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<ResourceStatisticsDto>>> GetResourceStatistics();

        /// <summary>
        /// 每天活跃人数
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<UserStatisticsDto>>> GetUserStatistics();

        /// <summary>
        /// 学校登录每天统计数
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<UserStatisticsDto>>> GetSchoolUseStatistics();

        /// <summary>
        /// 学校使用排名
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<SchoolUseStatis>>> GetSchoolUseDataOrderBy();

        /// <summary>
        /// 学校登录每月统计数
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<UserStatisticsDto>>> GetSchoolUseMonthStat();
        /// <summary>
        /// 每月活跃用户数
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<UserStatisticsDto>>> GetUserByMonthStatistics();

        /// <summary>
        /// 每天做题量
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<StudentTopicDetailDto>>> GetStudentTopicDetails();

        /// <summary>
        /// 任务发布数
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<TaskStatisticsDto>>> GetTaskStatistics();

        /// <summary>
        /// 各个省份访问量数据统计接口
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        Task<ResponseContext<List<RegionDataDto>>> GetRegionStatistics(string date);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<string>>> GetDates();

        /// <summary>
        /// 学校数据统计分析
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SchoolStatisticsDto>>> GetSchoolStatisticsList(string date);
    }
}
