﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 案例
    /// </summary>
    public interface ICaseServiceNew
    {
        #region 案例
        /// <summary>
        /// 保存案例
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveCase(CaseDtoNew input, bkUserTicket currentUser);

        /// <summary>
        /// 根据id获取案例
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<CaseDtoNew>> GetCase(long id);

        /// <summary>
        /// 查询案例
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<GetCaseListDtoNew>> GetCaseList(GetCaseInput input);

        /// <summary>
        /// 根据id删除案例
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCase(long id, bkUserTicket currentUser);

        /// <summary>
        /// 启用或禁用 true:启用 false:禁用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Toggle(ToggleInput input, bkUserTicket currentUser);

        /// <summary>
        /// 查询案例名称列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<GetCaseNameListDto>>> GetCaseNameList(QueryCaseNameRequest input);
        #endregion

        #region 生成试卷
        /// <summary>
        /// 购买后自动生成试卷 - 同步实训
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AutoGenerateTestPaper(AutoGenerateTestPaperDto model);
        #endregion

        /// <summary>
        /// 获取案例(同步实训)个人购买列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<PageResponse<CaseIndividualPurchaseViewModel>> GetCaseIndividualPurchaseList(CaseIndividualPurchaseQueryModel query, long currentUserId);

    }
}
