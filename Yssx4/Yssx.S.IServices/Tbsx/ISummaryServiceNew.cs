﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 同步实训 摘要业务接口
    /// </summary> 
    public interface ISummaryServiceNew
    {
        /// <summary>
        /// 新增摘要
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<SummaryDtoNew>> AddSummary(SummaryDtoNew model);

        /// <summary>
        /// 获取摘要列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SummaryDtoNew>>> GetSummaryList(long id);

        /// <summary>
        /// 删除摘要
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSummary(long id);
    }

    /// <summary>
    /// 同步实训 课程摘要业务接口
    /// </summary>
    public interface ICourseSummaryServiceNew
    {
        /// <summary>
        /// 新增摘要
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<SummaryDtoNew>> AddSummary(SummaryDtoNew model);

        /// <summary>
        /// 获取摘要列表
        /// </summary>
        /// <param name="cid">课程id</param>
        /// <returns></returns>
        Task<ResponseContext<List<SummaryDtoNew>>> GetSummaryList(long cid);

        /// <summary>
        /// 删除摘要
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSummary(long id);
    }
}
