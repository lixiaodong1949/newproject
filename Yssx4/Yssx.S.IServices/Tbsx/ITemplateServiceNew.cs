﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 同步实训 模板管理
    /// </summary>
    public interface ITemplateServiceNew
    {
        /// <summary>
        /// 获取模板树列表
        /// </summary>
        Task<PageResponse<TemplateTreeItemDtoNew>> GetTemplateTree();

        /// <summary>
        /// 获取指定模板类型
        /// </summary>
        /// <param name="id"></param> 
        /// <returns></returns>
        Task<ResponseContext<TemplateTreeItemDtoNew>> GetTemplateTypeById(long id);

        /// <summary>
        /// 添加/编辑模板类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateTemplateType(TemplateTreeItemDtoNew model, long currentUserId);

        /// <summary>
        /// 删除模板类型
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTemplateType(long tId, long currentUserId);




        /// <summary>
        /// 获取模板列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<TemplateDtoNew>> GetTemplates(GetTemplatesRequestNew model);

        /// <summary>
        /// 获取指定模板
        /// </summary>
        /// <param name="id"></param> 
        /// <returns></returns>
        Task<ResponseContext<TemplateDtoNew>> GetTemplateById(long id);

        /// <summary>
        /// 添加模板信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateTemplate(TemplateDtoNew model, long currentUserId);

        /// <summary>
        /// 删除指定模板
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTemplate(long tId, long currentUserId);

        /// <summary>
        /// 获取模板树（下拉框）
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<LayuiTreeDtoNew>>> GetTemplateSelectTreeDatas();

    }
}
