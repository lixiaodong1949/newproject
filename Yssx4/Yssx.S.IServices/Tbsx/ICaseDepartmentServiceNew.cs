﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 同步实训 资源配置接口
    /// </summary>
    public interface ICaseDepartmentServiceNew
    {

        #region 部门管理（同步实训）
        /// <summary>
        /// 获取部门管理列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<DepartmentDtoNew>> GetDepartmentList(DepartmentRequestNew model);

        /// <summary>
        /// 新增或修改部门管理
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditDepartment(DepartmentDtoNew model, long currentUserId);

        /// <summary>
        /// 删除部门管理
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveDepartment(long id, long currentUserId);

        /// <summary>
        /// 获取部门管理列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<DepartmentDtoNew>>> GetDepartmentAll(DepartmentRequestNew model);

        #endregion

        #region 岗位管理（同步实训）
        /// <summary>
        /// 获取岗位管理列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<PositionDtoNew>> GetPositionList(PositionRequestNew model);

        /// <summary>
        /// 根据Id获取岗位管理详情
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PositionDtoNew>> GetPositionById(long id);

        /// <summary>
        /// 新增或修改岗位管理
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditPosition(PositionDtoNew model, long currentUserId);

        /// <summary>
        /// 删除岗位管理
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemovePosition(long id, long currentUserId);
        #endregion

    }
}
