﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 题目业务服务接口
    /// </summary>
    public interface ITopicServiceNew
    {
        /// <summary>
        /// 添加 更新 题目（单选题、多选题、判断题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveSimpleQuestion(SimpleQuestionRequestNew model, bkUserTicket CurrentUser);

        /// <summary>
        /// 添加 更新 复杂题目（表格题、填空题、票据题）
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveComplexQuestion(ComplexQuestionRequestNew model, bkUserTicket CurrentUser);

        /// <summary>
        /// 添加 更新 综合题
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveMainSubQuestion(MainSubQuestionRequestNew model, bkUserTicket CurrentUser);

        /// <summary>
        /// 分录题  添加 更新
        /// </summary>
        /// <param name="model">model</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveCertificateTopic(CertificateTopicRequestNew model, bkUserTicket CurrentUser);

        /// <summary>
        /// 根据id获取题目详情
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<TopicInfoDtoNew>> GetTopicInfoById(long id);

        /// <summary>
        /// 查询题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<TopicListPageDto>> GetTopicListPage(TopicQueryPageDto queryDto);
        /// <summary>
        /// 题目删除 根据Id删除题目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTopicById(long id, bkUserTicket currentUser);
        /// <summary>
        /// 题目列表
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<List<TopicListDataDto>>> GetTopicList(TopicQueryDto queryDto);
        /// <summary>
        /// 根据条件查询案例题目列表 前台
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<List<QueryCaseTopicDto>>> GetTopicList(QueryTopicRequestDto queryDto); 
        /// <summary>
        /// 获取所有题目的名称（包含子题目）
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
        Task<ResponseContext<List<TopicNamesResponseDto>>> GetAllTopicNames(TopicNamesDto queryDto);
        /// <summary>
        /// 获取分录题 信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<CertificateTopicRequestNew>> GetCertificateById(long id);
    }
}
