﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;
using Yssx.S.Pocos;

namespace Yssx.S.IServices.ExamineAndVerify
{
    /// <summary>
    /// 审核
    /// </summary>
    public interface IExamineAndVerifyServiceNew
    {
        /// <summary>
        /// 审核
        /// </summary>
        /// <param name="dto">审核dto</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ExamineAndVerify(ExamineAndVerifyDtoNew dto, long oId);

        /// <summary>
        /// 上下架
        /// </summary>
        Task<ResponseContext<bool>> UpperAndLowerFrames(UpperAndLowerFramesDtoNew dto, long oId);
    }
}
