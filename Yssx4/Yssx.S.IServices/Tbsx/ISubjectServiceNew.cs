﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 同步实训 科目业务接口
    /// </summary> 
    public interface ISubjectServiceNew
    {
        /// <summary>
        /// 新增科目
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddSubject(SubjectDtoNew model);

        /// <summary>
        /// 删除科目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSubject(long id);

        /// <summary>
        /// 新增科目获取子科目的序号
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<string>> GetNextNum(long id);

        /// <summary>
        /// 根据案例Id获取科目列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SubjectDtoNew>>> GetSubjectsList(long id);


        /// <summary>
        /// 根据案例Id获取最后一级科目
        /// </summary>
        /// <param name="id">案例Id</param>
        /// <param name="type">类型Type</param>
        /// <param name="keyword">类型Type</param>
        /// <returns></returns>
        Task<ResponseContext<List<SubjectDtoNew>>> GetSubjectLastNodeList(long id, int type, string keyword);

        /// <summary>
        /// 根据案例科目类型获取科目列表
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SubjectDtoNew>>> GetSubjectByType(long id, int type);

        /// <summary>
        /// 添加期初数据
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddValues(SubjectDataDtoNew model);

        /// <summary>
        /// 试算平衡
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<ResponseContext<TrialBalancingDtoNew>> TrialBalancing(long caseId);
    }

    /// <summary>
    /// 同步实训 课程科目业务
    /// </summary>
    public interface ICourseSubjectServiceNew
    {
        /// <summary>
        /// 新增科目
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddSubject(SubjectDtoNew model);

        /// <summary>
        /// 删除科目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSubject(long id);

        /// <summary>
        /// 新增科目获取子科目的序号
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<string>> GetNextNum(long id);

        /// <summary>
        /// 根据案例Id获取科目列表
        /// </summary>
        /// <param name="cid">课程Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<SubjectDtoNew>>> GetSubjectsList(long cid);


        /// <summary>
        /// 根据案例Id获取最后一级科目
        /// </summary>
        /// <param name="cid">课程ID</param>
        /// <param name="type">类型Type</param>
        /// <param name="keyword">类型Type</param>
        /// <returns></returns>
        Task<ResponseContext<List<SubjectDtoNew>>> GetSubjectLastNodeList(long cid, int type, string keyword);

        /// <summary>
        /// 根据案例科目类型获取科目列表
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SubjectDtoNew>>> GetSubjectByType(long cid, int type);

        /// <summary>
        /// 添加期初数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddValues(SubjectDataDtoNew model);

        /// <summary>
        /// 试算平衡
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        Task<ResponseContext<TrialBalancingDtoNew>> TrialBalancing(long cid);
    }
}
