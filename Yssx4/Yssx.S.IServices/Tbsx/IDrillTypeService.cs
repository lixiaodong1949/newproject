﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    ///实训类型业务服务接口
    /// </summary>
    public interface IDrillTypeService
    {
        /// <summary>
        /// 删除实训类型信息
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteById(long id, bkUserTicket currentUser);
        /// <summary>
        /// 根据Id获取单条实训类型信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<DrillTypeDto>> GetInfoById(long id);
        /// <summary>
        /// 保存实训类型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Save(DrillTypeDto model, bkUserTicket currentUser);

        /// <summary>
        /// 获取父级实训类型列表 分页
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<DrillTypeListDto>>> GetList(DrillTypeQueryDto query);
        /// <summary>
        /// 获取实训类型名称列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<DrillTypeNameListDto>>> GetNameList(DrillTypeNameListRequestDto query);
        /// <summary>
        /// 指导编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetGuideInfo(SetGuideInfoDto model, bkUserTicket currentUser);
        /// <summary>
        /// 获取指导编辑信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<SetGuideInfoDto>> GetGuideInfoById(long dillTypeId);
    }
}
