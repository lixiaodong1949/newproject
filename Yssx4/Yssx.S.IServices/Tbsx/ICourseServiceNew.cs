﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 同步实训课程接口服务
    /// </summary>
    public interface ICourseServiceNew
    {
        /// <summary>
        /// 新增,修改课程
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> AddOrEditCourse(YssxCourseDtoNew dto,UserTicket user);

        /// <summary>
        /// 复制课程
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> CloneCourse(YssxCourseDtoNew dto, UserTicket user);

        /// <summary>
        /// 克隆课程定时任务
        /// </summary>
        /// <returns></returns>
        Task<bool> CloneCourseTaskJob();

        /// <summary>
        /// 删除课程
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveCourse(long id);

        /// <summary>
        /// 课程列表
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseDtoNew>> GetCourseListByPage(CourseSearchDtoNew dto,UserTicket user);

        /// <summary>
        /// 获取课程详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<YssxCourseDtoNew>> GetCourseById(long id);

        /// <summary>
        /// 获取快捷显示列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseListDtoNew>> GetShowList(PageRequest page, UserTicket user);

        /// <summary>
        /// 设置快捷显示
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isShow">0:取消显示,1:显示</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetIsShow(long id, int isShow);

        /// <summary>
        /// 课程列表查询
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseListDtoNew>> GetCourseList(CourseSearchDtoNew dto, UserTicket user);

        /// <summary>
        /// 商城首页课程列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortType"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseDtoNew>> GetCourseListPage(int pageIndex,int pageSize, int sortType);

        /// <summary>
        /// 购买课程(测试接口)
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BuyClassTest(long courseId, UserTicket user);

        /// <summary>
        /// 获取购买课程列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseOrderViewModelNew>> GetCourseOrderList(YssxCourseOrderQueryNew query, UserTicket user);

        /// <summary>
        /// 获取课程列表(App)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseForAppViewModelNew>> GetCourseForAppList(YssxCourseForAppQueryNew query, UserTicket user);

        /// <summary>
        /// 引用平台习题(深度拷贝购买课程下的某一道习题)
        /// </summary>
        /// <param name="courseId">习题即将存放的课程Id</param>
        /// <param name="topicId">需要引用的习题Id</param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> QuotationPlatformsTopic(long courseId, long topicId, UserTicket user);

        /// <summary>
        /// 绑定案例到课程
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> BandCases(BandCasesDto input, UserTicket user);
        /// <summary>
        /// 绑定案例到课程
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> BandCase(long courseId, long caseId, UserTicket user);
        /// <summary>
        /// 删除课程绑定的案例
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCases(long CourseId, long CaseId);

    }
}
