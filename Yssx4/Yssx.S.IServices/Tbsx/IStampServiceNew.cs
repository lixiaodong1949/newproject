﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 同步实训 印章服务接口
    /// </summary>
    public interface IStampServiceNew
    {
        /// <summary>
        /// 保存印章
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveStamp(StampRequestNew model,long userId);
        /// <summary>
        /// 删除印章
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteStamp(long id);
        /// <summary>
        /// 获取印章列表
        /// </summary>
        /// <param name="caseId"></param>
        /// <returns></returns>
        Task<PageResponse<StampDtoNew>> GetStampList(StampListRequestNew model, long userId);


    }
}
