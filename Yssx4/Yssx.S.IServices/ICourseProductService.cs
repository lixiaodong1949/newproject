﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 商品课程服务
    /// </summary>
    public interface ICourseProductService
    {
        /// <summary>
        /// 课程详情首页
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        Task<ResponseContext<int>> CourseDetailsPage(long cid,long uid);

        /// <summary>
        /// 获取课程下面的所有章节
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <param name="uid">课程Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionDto>>> GetSectionListByUid(long courseId,long uid);


        /// <summary>
        ///  获取课程下面的课件,教案,视频按类型筛选
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="uid"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesListByUid(long courseId,long uid,int fileType);

        /// <summary>
        ///  获取课程下面的课件,教案,视频按类型筛选
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesListById(long cid, int fileType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CourseCaseDto>>> GetCourseCaseListById(long cid);

        /// <summary>
        /// 获取习题列表
        /// </summary>
        /// <param name="sid">章节id</param>
        /// <param name="uid"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionTopicDto>>> GetYssxSectionTopicListByUid(long sid, long uid);
    }
}
