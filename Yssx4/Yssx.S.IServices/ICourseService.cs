﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;
using Yssx.S.Dto.OptimizeContent;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 课程接口服务
    /// </summary>
    public interface ICourseService
    {
        #region 课程
        /// <summary>
        /// 新增,修改课程
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> AddOrEditCourse(YssxCourseDto dto,UserTicket user);

        /// <summary>
        /// 复制课程
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> CloneCourse(YssxCourseDto dto, UserTicket user);

        /// <summary>
        /// 克隆课程定时任务
        /// </summary>
        /// <returns></returns>
        Task<bool> CloneCourseTaskJob();

        /// <summary>
        /// 删除课程
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveCourse(long id);

        /// <summary>
        /// 课程列表
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseDto>> GetCourseListByPage(CourseSearchDto dto,UserTicket user);

        /// <summary>
        /// 获取课程详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<YssxCourseDto>> GetCourseById(long id);

        /// <summary>
        /// 获取快捷显示列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseListDto>> GetShowList(PageRequest page, UserTicket user);
        /// <summary>
        /// 获取快捷显示列表 (查全部)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxCourseListDto>>> GetShowAllList(UserTicket user);

        /// <summary>
        /// 设置快捷显示
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isShow">0:取消显示,1:显示</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetIsShow(long id, int isShow);

        /// <summary>
        /// 课程列表查询
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseListDto>> GetCourseList(CourseSearchDto dto, UserTicket user);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseListDto>> GetCourseListBySchool(CourseSearchDto dto, UserTicket user);

        /// <summary>
        /// 商城首页课程列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortType"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseDto>> GetCourseListPage(int pageIndex,int pageSize, int sortType);

        /// <summary>
        /// 购买课程(测试接口)
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BuyClassTest(long courseId, UserTicket user);

        /// <summary>
        /// 获取购买课程列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseOrderViewModel>> GetCourseOrderList(YssxCourseOrderQuery query, UserTicket user);

        /// <summary>
        /// 获取课程列表(App)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseForAppViewModel>> GetCourseForAppList(YssxCourseForAppQuery query, UserTicket user);

        /// <summary>
        /// 引用平台习题(深度拷贝购买课程下的某一道习题)
        /// </summary>
        /// <param name="courseId">习题即将存放的课程Id</param>
        /// <param name="topicId">需要引用的习题Id</param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> QuotationPlatformsTopic(long courseId, long topicId, UserTicket user);
        #endregion


        #region 初级会计考证
        /// <summary>
        /// 购买后自动生成试卷 - 初级会计考证
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AutoGenerateTestPaperPrimary(AutoGenerateTestPaperDto model, long currentUserId = 0);

        /// <summary>
        /// 考证资源列表 - 初级会计考证
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<GetCourseJuniorOrderListDto>> GetCourseJuniorOrderList(GetCourseJuniorOrderListRequest model, long currentUserId);


        #endregion

        /// <summary>
        /// 购买后自动生成课程
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AutoGenerateCourse(AutoGenerateTestPaperDto model, long currentUserId);

        /// <summary>
        /// 获取购买课程列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<PageResponse<BuyCourseInfoViewModel>> GetBuyCourseList(BuyCourseInfoQuery query, long currentUserId);

        /// <summary>
        /// 绑定课程知识点
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BindingCourseKnowledgePoint(CourseKnowledgePointDto dto, UserTicket user);

        /// <summary>
        /// 获取课程知识点列表
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CourseKnowledgePointViewModel>>> GetCourseKnowledgePointList(long courseId);

        /// <summary>
        /// 删除课程知识点(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCourseKnowledgePointById(long id, UserTicket user);

        /// <summary>
        /// 删除课程知识点(根据课程、知识点主键)
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <param name="knowId">知识点Id</param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCourseKnowledgePointByCourseAndKnowId(long courseId, long knowId, UserTicket user);

        /// <summary>
        /// 获取学校课程引用数据
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxCourseListDto>> GetCourseListBySchoolOrder(CourseSearchDto dto, UserTicket user);

        /// <summary>
        /// 获取学校课程订单数据
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<OrderCourseDto>>> GetCourseListBySchoolPlat(CourseSearchDto dto, UserTicket user);

        /// <summary>
        /// 移除/引用课程  
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveCourseByOrder(RemoveCourseDto model, UserTicket user);

        /// <summary>
        /// 根据课程id获取知识点parentids
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        Task<ResponseContext<string>> GetKnowledgePointIdByCourse(long courseId);

    }
}
