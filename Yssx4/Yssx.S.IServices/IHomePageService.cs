﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 首页信息相关服务
    /// </summary>
    public interface IHomePageService
    {
        /// <summary>
        /// 获取移动端首页信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<McHomePageDto>> GetMcHomePageInfo();

        /// <summary>
        /// 广告位信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<BannerInfo>>> GetBannerInfo();

        /// <summary>
        /// 获取IOS发布信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<ReleaseInfoIOSViewModel>> GetReleaseInfoIOS();
    }
}
