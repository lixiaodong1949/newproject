﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 公司信息发布
    /// </summary>
    public interface IInformation
    {
        /// <summary>
        /// 新增修改信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>

        Task<ResponseContext<bool>> AddOrEditInfo(InfoDto dto);

        /// <summary>
        /// 获取消息列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<InfoDto>>> GetInformationList();
    }
}
