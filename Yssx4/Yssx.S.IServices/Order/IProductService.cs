﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 产品
    /// </summary>
    public interface IProductService
    {
        #region 抢购商品
        /// <summary>
        /// 抢购商品
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<ProductDto>>> GetCasProducts();
        #endregion

        #region 产品

        /// <summary>
        /// 保存产品
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveProduct(SaveProductDto model, long currentUserId);

        /// <summary>
        /// 删除产品
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteProduct(long id, long currentUserId);

        /// <summary>
        /// 获取产品列表
        /// </summary>
        Task<PageResponse<GetProductByPageDto>> GetProductByPage(GetProductByPageRequest model);

        /// <summary>
        /// 获取指定产品数据
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<SaveProductDto>> GetProductInfoById(long id);


        /// <summary>
        /// 保存产品明细
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveProductDetail(SaveProductDetailDto model, long currentUserId);

        /// <summary>
        /// 删除产品明细
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteProductDetail(long id, long productId, long currentUserId);

        #endregion

    }
}
