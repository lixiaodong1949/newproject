﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IProductCouponsService
    {

        /// <summary>
        /// 验证产品卡券
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> ExchangeCouponsCode(string code, string mobilePhone, long currentUserId);

        /// <summary>
        /// 自动验证产品卡券
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AutoExchangeCouponsCode(string ignoreMobilePhone, long currentUserId);




    }
}
