﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto.Order;

namespace Yssx.S.IServices.Order
{
    public interface IUserOrderService
    {
        /// <summary>
        /// 迁移抢购订单数据
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> Migrate();

        /// <summary>
        /// 订单统计
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<UserOrderStatsDto>> GetStats();

        /// <summary>
        /// 获取单个用户的订单信息
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<SingleUserOrder>> GetSingleUserOrders(GetSingleUserOrderDto input);

        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PageResponse<UserOrderDto>> GetOrderList(GetUserOrdersDto input);
    }
}
