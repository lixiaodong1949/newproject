﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Task;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICourseTestOnLineTaskService
    {
        /// <summary>
        /// 新增课题任务
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<AddTestTaskDto>> AddTestTask(TaskDto model, long currentUserId);

        /// <summary>
        /// 收题
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> StopTestTask(long taskId);

        /// <summary>
        /// 任务详情
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="examId"></param>
        /// <returns></returns>
        Task<ResponseContext<TestTaskDetailsDto>> TestTaskDetails(long taskId, long examId);

        /// <summary>
        /// 任务结果分析
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="examId"></param>
        /// <returns></returns>
        Task<ResponseContext<AnalyzeTaskResultDto>> AnalyzeTaskResult(long taskId, long examId);

        /// <summary>
        /// 显示做题学生列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<AnalyzeQuestionDetailsStudentsDto>>> AnalyzeQuestionStudents(long taskId, long examId);


        /// <summary>
        /// 我的任务列表
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<PageResponse<MyCourseTaskDto>> GetMyCourseTaskList(UserTicket user, int pageIndex, int pageSize,string courseName);

        /// <summary>
        /// 任务题目列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        Task<ResponseContext<ExamPaperCourseBasicInfo>> GetCourseTaskExamTopicList(long taskId, long examId);
    }
}
