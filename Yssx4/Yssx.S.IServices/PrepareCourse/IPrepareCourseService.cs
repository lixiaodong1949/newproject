﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 课程备课
    /// </summary>
    public interface IPrepareCourseService
    {
        /// <summary>
        /// 选中备课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> SelectedLessonPrepare(SelectedLessonPrepareDto dto, UserTicket user);
        
        /// <summary>
        /// 取消选中备课
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UncheckLessonPrepare(List<long> ids, UserTicket user);

        /// <summary>
        /// 获取备课课程列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxPrepareCourseViewModel>> GetPrepareCourseList(YssxPrepareCourseQuery query, UserTicket user);

        /// <summary>
        /// 编辑备课课程
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> EditPrepareCourse(PrepareCourseDto dto, UserTicket user);

        /// <summary>
        /// 编辑备课课程学期
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> EditPrepareCourseSemester(PrepareCourseSemesterDto dto, UserTicket user);
    }

}
