﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 备课课程章节
    /// </summary>
    public interface IPrepareSectionService
    {
        /// <summary>
        /// 新增/编辑备课课程章节
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditPrepareSection(PrepareSectionDto dto, UserTicket user);

        /// <summary>
        /// 获取备课课程章节列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxPrepareSectionViewModel>>> GetPrepareSectionList(YssxPrepareSectionQuery query);

        /// <summary>
        /// 删除备课课程章节
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemovePrepareSection(long id, UserTicket user);

        /// <summary>
        /// 删除备课课程本章所有小节
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemovePrepareAllSectionOfChapter(long id, UserTicket user);

        /// <summary>
        /// 新增备课课程节习题
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddPrepareSectionTopic(PrepareSectionTopicDto dto, UserTicket user);

        /// <summary>
        /// 获取备课课程节习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxPrepareSectionTopicViewModel>>> GetPrepareSectionTopicList(YssxPrepareInfoQuery query);

        /// <summary>
        /// 删除备课课程节习题
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemovePrepareSectionTopic(long id, UserTicket user);

        /// <summary>
        /// 新增备课课程节场景实训
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddPrepareSectionSceneTraining(PrepareSectionSceneTrainingDto dto, UserTicket user);

        /// <summary>
        /// 获取备课课程节场景实训列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxPrepareSectionSceneTrainingViewModel>>> GetPrepareSectionSceneTrainingList(YssxPrepareInfoQuery query);

        /// <summary>
        /// 删除备课课程节场景实训
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemovePrepareSectionSceneTraining(long id, UserTicket user);

        /// <summary>
        /// 新增/编辑备课课程节教材
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditPrepareSectionTextBook(PrepareSectionTextBookDto dto, UserTicket user);

        /// <summary>
        /// 获取备课课程某一节教材详情
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxPrepareSectionTextBookViewModel>>> GetPrepareSectionTextBookDetail(YssxPrepareInfoQuery query);

        /// <summary>
        /// 删除备课课程节教材
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemovePrepareSectionTextBook(long id, UserTicket user);

        /// <summary>
        /// 新增备课课程课件、视频、教案、授课计划
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddPrepareSectionFiles(PrepareSectionFilesDto dto, UserTicket user);

        /// <summary>
        /// 获取备课课程课件、视频、教案、授课计划列表(App)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxPrepareSectionFilesViewModel>>> GetPrepareSectionFilesListForApp(YssxPrepareSectionFilesQuery query);

        /// <summary>
        /// 获取备课课程课件、视频、教案、授课计划列表(PC)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<YssxPrepareSectionFilesForPCViewModel>> GetPrepareSectionFilesListForPC(YssxPrepareSectionFilesQuery query); 

        /// <summary>
        /// 删除备课课程课件、视频、教案、授课计划
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemovePrepareSectionFiles(long id, UserTicket user);

    }

}
