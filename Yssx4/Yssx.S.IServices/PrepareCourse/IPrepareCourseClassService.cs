﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 课程班级服务接口
    /// </summary>
    public interface IPrepareCourseClassService
    {
        /// <summary>
        /// 获取教师班级
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxTeacherClassViewModel>>> GetTeacherClassList(YssxTeacherClassQuery query, UserTicket user);

        /// <summary>
        /// 绑定课程班级(PC)
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BindingCourseClassForPC(PrepareCourseClassDto dto, UserTicket user);

        /// <summary>
        /// 绑定课程班级(App)
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> BindingCourseClassForApp(PrepareCourseClassForAppDto dto, UserTicket user);

        /// <summary>
        /// 解绑单一课程班级
        /// </summary>
        /// <param name="courseClassId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UnlinkCourseClass(long courseClassId, UserTicket user);

        /// <summary>
        /// 获取学期班级列表(班级管理)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSemesterClassViewModel>>> GetSemesterClassList(UserTicket user);

        /// <summary>
        /// 根据班级获取学生信息
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxCourseClassStudentViewModel>>> GetStudentListByClass(YssxClassStudentQuery query);

        /// <summary>
        /// 根据教师课程绑定班级获取学生信息
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxCourseClassStudentViewModel>>> GetStudentListByTeacherCourseClass(YssxCourseClassStudentQuery query, UserTicket user);

        /// <summary>
        /// 根据某一个课程获取绑定班级的学生实体
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxClassStudentByCourseViewModel>>> GetStudentListByCourse(YssxCourseClassStudentByCourseQuery query, UserTicket user);

        /// <summary>
        /// 获取教师绑定课程班级列表
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxTeacherClassViewModel>>> GetTeacherBindingCourseClassList(UserTicket user);

    }
}
