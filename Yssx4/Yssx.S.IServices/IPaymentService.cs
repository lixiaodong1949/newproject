﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto.Order;

namespace Yssx.S.IServices
{
    public interface IPaymentService
    {
        /// <summary>
        /// 支付宝
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<AlipayResponse>> Alipay(CreateRtpOrderDto input,UserTicket currentUser);

        /// <summary>
        /// 支付宝手机支付
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<AlipayResponse>> AlipayWap(CreateRtpOrderDto input, UserTicket currentUser);

        /// <summary>
        /// 异步回调
        /// </summary>
        /// <param name="input"></param>
        void AlipayNotify(Dictionary<string, string> input);

        /// <summary>
        /// 微信
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<WebchatResponse>> Webchat(CreateRtpOrderDto input, UserTicket currentUser);

        /// <summary>
        /// 微信H5
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<WebchatResponse>> WebchatWap(CreateRtpOrderDto input, UserTicket currentUser);

        /// <summary>
        /// 微信
        /// </summary>
        /// <param name="xmlString"></param>
        void WebchatNotify(string xmlString);

        /// <summary>
        /// 查询订单
        /// </summary>
        Task<PageResponse<RtpOrderDto>> GetOrderList(GetRtpOrderDto input);

        /// <summary>
        /// 统计
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<OrderStatsDto>> GetStats();

        /// <summary>
        /// 获取是否已下过订单
        /// </summary>
        /// <param name="caseId">案例id</param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> GetCaseOrderStatus(long caseId, UserTicket currentUser);

        /// <summary>
        /// 获取用户的所有订单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<UserRtpOrder>> GetUserOrders(GetUserRtpOrderDto input);

        /// <summary>
        /// 取消订单
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> CancelOrder(CancelOrderInputDto input, bkUserTicket currentUser);
    }
}
