﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.SceneTraining;
using Yssx.S.Pocos;
using Yssx.S.Pocos.SceneTraining;

namespace Yssx.S.IServices.SceneTraining
{
    /// <summary>
    /// 场景实训
    /// </summary>
    public interface ISceneTrainingService
    {
        /// <summary>
        /// 上传场景实训
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddTrainingService(AddTrainingServiceDto dto,long userId);

        /// <summary>
        /// 下拉数据
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="typeId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<DropDownDataDto>>> DropDownData(long id, int typeId);

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="dto">搜索</param>
        /// <returns></returns>
        Task<PageResponse<YssxSceneTrainingDto>> TrainingServiceList(TrainingServicePageRequest dto,UserTicket user);

        /// <summary>
        /// 描述
        /// </summary>
        /// <param name="describe">描述</param>
        /// <param name="id">场景实训id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Describe(string describe,long id);

        /// <summary>
        /// 场景实训详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<TrainingServiceDetailsDto>> TrainingServiceDetails(long id);

        /// <summary>
        /// 预览
        /// </summary>
        /// <param name="id">场景实训id</param>
        /// <returns></returns>
        Task<ResponseContext<TrainingServicePreviewDto>> TrainingServicePreview(long id);

        /// <summary>
        /// 做题开始详情
        /// </summary>
        /// <param name="id">场景实训id</param>
        /// <param name="progressId">实训记录Id</param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<ProblemDetailsDto>> ProblemDetails(long id, long progressId,long userId);

        /// <summary>
        /// 场景实训流程详情
        /// </summary>
        /// <param name="id">场景实训流程id</param>
        /// <returns></returns>
        Task<ResponseContext<ProcessDetails>> SceneTrainingDetails(long id);

        /// <summary>
        /// 公司下面的场景实训列表
        /// </summary>
        /// <param name="id">公司id</param>
        /// <returns></returns>
        Task<ResponseContext<TrainingServicePreviewListDto>> TrainingServicePreviewList(long id);

        /// <summary>
        /// 添加实训进度
        /// </summary>
        /// <param name="dto">添加实训进度dto</param>
        /// <param name="userId">用户id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddProgress(AddProgressDto dto, long userId);

        /// <summary>
        /// 实训进度详情
        /// </summary>
        /// <param name="id">场景实训id</param>
        /// <param name="userId">用户id</param>
        /// <returns></returns>
        Task<ResponseContext<ProgressDetailsDto>> ProgressDetails(long id, long userId);

        /// <summary>
        /// 复盘总结-业务列表
        /// </summary>
        /// <param name="id">场景实训id</param>
        /// <returns></returns>
        Task<ResponseContext<CheckingSummaryListDto>> CheckingSummaryList(long id);

        /// <summary>
        ///修改进度状态
        /// </summary>
        /// <param name="id">实训进度id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateProgress(long id);
    }
}
