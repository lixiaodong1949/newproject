﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 场景实训-首页商城
    /// </summary>
    public interface ISceneTrainingMallService
    {
        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<SceneTrainingMallResult>> Find(SceneTrainingMallDto model);

        /// <summary>
        /// 详情
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<SceneTrainingMallRequest>> Details(long Id);

        /// <summary>
        /// 点赞
        /// </summary>
        /// <param name="model"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Fabulous(FabulousDao model, long oId);
    }
}
