﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices.SceneTraining
{
    /// <summary>
    /// 场景实训订单服务
    /// </summary>
    public interface ISceneTrainingOrderService
    {
        /// <summary>
        /// 获取购买场景实训列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxSceneTrainingViewModel>> GetSceneTrainingByBuy(YssxSceneTrainingQuery query, UserTicket user);

    }
}
