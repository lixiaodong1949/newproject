﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 岗位实训任务服务接口
    /// </summary>
    public interface IJobTrainingTaskService
    {
        /// <summary>
        /// 发布或修改岗位实训任务
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> PublishOrUpdateJobTraingingTask(JobTrainingTaskDto dto, UserTicket user);

        /// <summary>
        /// 获取教师岗位实训任务列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxJobTrainingTaskTeacherViewModel>> GetJobTrainingTaskTeacherList(YssxJobTrainingTaskTeacherQuery query, UserTicket user);

        /// <summary>
        /// 获取学生岗位实训任务列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<YssxJobTrainingTaskStudentViewModel>> GetJobTrainingTaskStudentList(YssxJobTrainingTaskStudentQuery query, UserTicket user);

        /// <summary>
        /// 验证岗位实训任务密码
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ValidateJobTraingingTaskPassword(long id, string password);

        /// <summary>
        /// 结束岗位实训任务
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> FinishJobTraingingTask(long id, UserTicket user);

        /// <summary>
        /// 删除岗位实训任务
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveJobTraingingTask(long id, UserTicket user);

        /// <summary>
        /// 获取岗位实训任务试卷作答记录
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxJobTrainingTaskExamAnswerRecordViewModel>>> GetJobTrainingTaskExamAnswerRecord(YssxJobTrainingTaskExamAnswerRecordQuery query);

        /// <summary>
        /// 根据岗位实训任务获取绑定班级的学生列表
        /// </summary>
        /// <param name="jobTrainingTaskId">任务Id</param>
        /// <returns></returns>
        Task<ResponseContext<YssxJobTrainingTaskBindingStudentViewModel>> GetJobTrainingTaskBindingStudentList(long jobTrainingTaskId);

        /// <summary>
        /// 根据试卷Id统一交卷、统计分数
        /// </summary>
        /// <param name="examId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SubmitPaperAndCalculateScoreByExamId(long examId, UserTicket user);

        /// <summary>
        /// 重新开启任务
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetTaskStatus(long taskId, DateTime endDate, bool isCanCheckExam);

        /// <summary>
        /// 开启学生试卷做题状态
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetStudentStatus(long taskId, long userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetStudentStatus(long gradeId, StudentExamStatus status);


        /// <summary>
        /// 获取岗位实训任务套题(案例)列表
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<JobTrainingCaseViewModel>>> GetJobTrainingCaseList(long taskId);

        /// <summary>
        /// 获取岗位实训任务试卷学生实时作答详情
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<JobTrainingTaskExamStudentAnswerRecordViewModel>> GetJobTrainingTaskExamStudentAnswerRecord(JobTrainingTaskExamStudentAnswerRecordQuery query);

        /// <summary>
        /// 取消交卷
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> CancelExamSubmit(CancelSubmitRequestModel dto, UserTicket user);

        /// <summary>
        /// 打开或关闭学生查看试卷入口
        /// </summary>
        /// <param name="request"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> OpenOrCloseJobTrainingTaskCheckExam(CloseCheckExamRequestModel request, UserTicket user);

        /// <summary>
        /// 获取岗位实训任务套题(案例)列表【学生端】
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxJobTrainingViewModel>>> GetStudentTaskCaseList(long taskId, UserTicket user);

    }
}
