﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 实习证书接口
    /// </summary>
    public interface IInternshipProveService
    {
        #region 实习证书
        /// <summary>
        /// 获取生成实习证书的资料
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<InternshipProveInfoDto>> GetInternshipProveInfo(long gradeId, long currentUserId);

        /// <summary>
        /// 生成实习证书
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddInternshipProve(AddInternshipProveDto model, long currentUserId);

        /// <summary>
        /// 查询证书列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<InternshipProveListDto>> GetInternshipProveList(InternshipProveListRequest model, long currentUserId);

        /// <summary>
        /// 实习证书查询
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<InternshipProveListDto>> GetInternshipProveBySN(string sn);
        #endregion

        #region 实训列表
        /// <summary>
        /// 实训列表 - 岗位实训
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<GetPaidJobInternshipListDto>> GetPaidJobInternshipList(GetPaidJobInternshipListRequest model, long currentUserId);

        /// <summary>
        /// 查询通关记录
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<JobTrainingEndRecordDto>> GetJobTrainingEndRecord(JobTrainingEndRecordRequest model, long currentUserId);
        #endregion

        #region 生成试卷
        /// <summary>
        /// 购买后自动生成试卷 - 岗位实训
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AutoGenerateTestPaper(AutoGenerateTestPaperDto model);
        #endregion


    }
}
