﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 课程备课接口
    /// </summary>
    public interface ICoursePrepareService
    {
        /// <summary>
        /// 新增/编辑课程备课
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> AddOrEditCoursePrepare(CoursePrepareRequestModel dto, UserTicket user);

        /// <summary>
        /// 获取课程备课列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CoursePrepareViewModel>>> GetCoursePrepareList(CoursePrepareQuery query, UserTicket user);

        /// <summary>
        /// 删除课程备课
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCoursePrepareById(long id, UserTicket user);

        /// <summary>
        /// 新增课程备课文件(引用课程资源)
        /// </summary>
        /// <param name="requestList"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddCoursePrepareFiles(List<CoursePrepareFilesRequestModel> requestList, UserTicket user);

        /// <summary>
        /// 获取课程预习/备课文件列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CoursePrepareFilesViewModel>>> GetCoursePrepareFilesList(CoursePrepareFilesQuery query);

        /// <summary>
        /// 获取课程备课选中课程题目列表
        /// </summary>
        /// <param name="couPrepareId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CoursePrepareSelectCourseTopicViewModel>>> GetPrepareSelectCourseTopicList(long couPrepareId);

        /// <summary>
        /// 获取课程备课选中案例题目列表
        /// </summary>
        /// <param name="couPrepareId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<CoursePrepareSelectCaseTopicIdsViewModel>>> GetPrepareSelectCaseTopicIdList(long couPrepareId);

        /// <summary>
        /// 删除课程备课文件(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCoursePrepareFilesById(long id, UserTicket user);

        /// <summary>
        /// 删除课程备课文件(根据文件主键)
        /// </summary>
        /// <param name="couPreId">课程备课Id</param>
        /// <param name="fileId">课件库Id</param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCoursePrepareFilesByFileId(long couPreId, long fileId, UserTicket user);

        /// <summary>
        /// 新增课程备课题目(引用课程题目)
        /// </summary>
        /// <param name="requestList"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddCoursePrepareTopic(List<CoursePrepareTopicRequestModel> requestList, UserTicket user);

        /// <summary>
        /// 发布预习
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> PublishCoursePreview(CoursePreviewRequestModel dto, UserTicket user);

        /// <summary>
        /// 获取课程教师预习列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<CoursePreviewToTeacherViewModel>> GetTeacherCoursePreviewList(CoursePreviewToTeacherQuery query, UserTicket user);

        /// <summary>
        /// 删除课程预习(根据主键)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteCoursePreviewById(long id, UserTicket user);

        /// <summary>
        /// 获取课程学生预习列表
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<PageResponse<CoursePreviewToStudentViewModel>> GetStudentCoursePreviewList(CoursePreviewToStudentQuery query, UserTicket user);

        /// <summary>
        /// 学生首次进入课程预习记录
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> StudentFirstCheckCoursePreview(StudentViewPreviewRequestModel dto, UserTicket user);

        /// <summary>
        /// 获取学生查看预习详情列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<StudentPreviewCourseDetailViewModel>> GetStudentPreviewCourseDetailList(StudentPreviewCourseDetailQuery query);

        /// <summary>
        /// 获取学生数量及查看预习数量记录
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<StudentNumberAndHadCheckNumberViewModel>> GetStudentNumberAndHadCheckNumberRecord(StudentNumberAndHadCheckNumberQuery query);

        /// <summary>
        /// 添加学生课程资源不懂记录
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddStudentDoubtCourseFileRecord(StudentDoubtCourseFileRequestModel dto, UserTicket user);

        /// <summary>
        /// 获取学生不懂(留言)详情列表
        /// </summary>
        /// <param name="couPreviewId">课程预习Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<StudentDoubtDetailViewModel>>> GetStudentDoubtDetailList(long couPreviewId);

        /// <summary>
        /// 获取预习某一资源 学生不懂详情记录
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Task<ResponseContext<List<FileStudentDoubtInfoViewModel>>> GetFileStudentDoubtInfoRecord(FileStudentDoubtInfoQuery query, UserTicket user);

        /// <summary>
        /// 获取课程备课题目列表(课程资源来源)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<CoursePrepareTopicViewModel>> GetCoursePrepareTopicList(CoursePrepareTopicQuery query);

        /// <summary>
        /// 获取课程备课文件资源列表(课程资源来源)
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<CourseFilesResourceViewModel>> GetCoursePrepareFileList(CourseFilesResourceQuery query);

    }
}
