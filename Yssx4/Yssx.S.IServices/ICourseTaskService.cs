﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 课程任务
    /// </summary>
    public interface ICourseTaskService
    {
        /// <summary>
        /// 根据查询结果获取题型列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<QuestionTypeData>>> GetQuestionTypesByQuery(CourseTopicQuery query);

        /// <summary>
        /// 获取习题列表
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<PageResponse<CourseTaskTopicDto>> GetCourseTopicList(CourseTopicQuery query,long userId);

        /// <summary>
        /// 加入任务篮
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddTaskCart(TaskCartRequest model, long userId);
        /// <summary>
        /// 批量加入任务篮
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddTaskCartBatch(BatchTaskCartRequest model, long userId);
        /// <summary>
        /// 根据Id移除任务篮
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTaskCart(DeleteTaskCartRequest model, long userId);
        /// <summary>
        /// 根据课程Id和题型移除任务篮
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteTaskCartByQuestionType(long courseId, QuestionType questionType, long userId);

        /// <summary>
        /// 清空任务篮
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ClearTaskCart(long courseId, long userId);

        /// <summary>
        /// 获取任务篮列表
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        Task<ResponseContext<List<TaskCartTopicDto>>> GetTaskCartList(long userId, long courseId);

        /// <summary>
        /// 根据课程查找任务篮题目数量
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <returns></returns>
        Task<ResponseContext<long>> GetTaskCartCountByCourseId(long userId, long courseId);

        /// <summary>
        /// 任务篮弹窗
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        Task<ResponseContext<List<TaskCartQuestionCountDto>>> GetTaskCarts(long userId, long courseId);

        /// <summary>
        /// 智能组卷时根据查询条件返回题型及题目数量
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<RandomTaskCartQuestionTypeDto>>> GetRandomQuestionType(RandomTaskCartRequest model, long userId);

        /// <summary>
        /// 智能组卷生成题目
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RandomAddTaskCarts(RandomTaskCartRequest model, long userId);

        /// <summary>
        /// 发布任务
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <param name="tenantId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> PublishCourseTask(PublicCourseTaskDto model, long currentUserId, long tenantId);

        /// <summary>
        /// 查询课业任务列表 - 教师端
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<CourseTaskListDto>> GetTeacherCourseTaskList(CourseTaskQueryRequest model, long currentUserId);

        /// <summary>
        /// 课程任务基础信息修改
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> EditTaskBasicInfo(EditTaskBasicInfoDto model, long currentUserId);
        /// <summary>
        /// 查询课业任务列表 - 学生端
        /// </summary>
        /// <param name="model"></param>
        /// <param name="currentUserId"></param>
        /// <returns></returns>
        Task<PageResponse<StudentCourseListDto>> GetStudentCourseTaskList(StudentCourseTaskRequest model, long currentUserId);

        /// <summary>
        /// 删除课业任务
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveTask(long id, long currentUserId);
        /// <summary>
        /// 结束课业任务
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> ClosedTask(long id, long currentUserId);
        /// <summary>
        /// 一键交卷
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> BatchSubmitExam(long id, long currentUserId);

        /// <summary>
        /// 预览课程任务信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<CourseTaskInfoDto>> GetCourseTaskInfoById(long taskId, long currentUserId);

        /// <summary>
        /// 课业任务统计信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<CourseTaskGradeReportDto>> GetCourseGradeDetailByTask(long taskId, string classIds, long currentUserId);
        /// <summary>
        /// 获取学生答题对错详情
        /// </summary>
        /// <param name="gradeId"></param>
        /// <returns></returns>
        Task<ResponseContext<StudentGradeInfoDto>> GetTaskTopicGradeDetailList(long gradeId, long taskId);
        /// <summary>
        /// 章节习题分析
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<TaskSectionGradeInfoDto>>> GetTaskSectionGradeDetailList(long taskId);
    }
}
