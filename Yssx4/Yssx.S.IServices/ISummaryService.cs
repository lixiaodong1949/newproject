﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 摘要业务接口
    /// </summary> 
    public interface ISummaryService
    {
        /// <summary>
        /// 新增摘要
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<SummaryDto>> AddSummary(SummaryDto model);

        /// <summary>
        /// 获取摘要列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<List<SummaryDto>>> GetSummaryList(long id);

        /// <summary>
        /// 删除摘要
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSummary(long id);
    }

    /// <summary>
    /// 课程摘要业务接口
    /// </summary>
    public interface ICourseSummaryService
    {
        /// <summary>
        /// 新增摘要
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<SummaryDto>> AddSummary(SummaryDto model);

        /// <summary>
        /// 获取摘要列表
        /// </summary>
        /// <param name="cid">课程id</param>
        /// <returns></returns>
        Task<ResponseContext<List<SummaryDto>>> GetSummaryList(long cid);

        /// <summary>
        /// 删除摘要
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSummary(long id);
    }
}
