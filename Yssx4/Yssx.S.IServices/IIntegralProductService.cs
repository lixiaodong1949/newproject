using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.S.Dto;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 积分商品接口服务类
    /// </summary>
    public interface IIntegralProductService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> Save(IntegralProductDto model, long currentUserId);

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<IntegralProductDto>> Get(long id);

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> Delete(long currentUserId, params long[] id);

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<IntegralProductPageResponseDto>> ListPage(IntegralProductPageRequestDto query);

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<IntegralProductListResponseDto>> List(IntegralProductListRequestDto query);

        /// <summary>
        /// 积分商品上下架
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> Active(long id, bool IsActive, long currentUserId);

        /// <summary>
        /// 查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<IntegralProductBaseInfoDto>> BaseListPage();
        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<IntegralProductInfoDto>> Info(long id);
    }
}