﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 课程章节
    /// </summary>
    public interface ISectionService
    {
        /// <summary>
        /// 新增,编辑章节
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditSection(YssxSectionDto dto);

        /// <summary>
        ///  新增,编辑课程课件,教案
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditSectionFile(YssxSectionFilesDto dto,UserTicket user);

        /// <summary>
        /// 新增,编辑教材
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>

        Task<ResponseContext<bool>> AddOrEditSectionTextBook(YssxSectionTextBookDto dto);

        /// <summary>
        /// 新增课程教案
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditSectionCase(YssxSectionCaseDto dto);

        /// <summary>
        ///  新增,编辑课程习题
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditSectionTopic(YssxSectionTopicDto dto);

        /// <summary>
        /// 删除章节
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSection(long id);

        /// <summary>
        /// 删除教案
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSectionCase(long id);

        /// <summary>
        /// 删除题目
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSectionTopic(long id);

        /// <summary>
        /// 删除课件，教案，视频
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSectionFile(long id);


        /// <summary>
        /// 获取习题列表
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionTopicDto>>> GetYssxSectionTopicList(long sid);

        /// <summary>
        /// 根据节获取习题
        /// 一般情况下，获取到所有的习题后，PC端可以自己筛选数据，不需要再次请求接口进行查询
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionTopicDto>>> GetYssxSectionTopicistForPC(SectionTopicQuery query);

        /// <summary>
        /// 获取课程下面的所有章节
        /// </summary>
        /// <param name="courseId">课程Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionDto>>> GetSectionList(long courseId);

        /// <summary>
        /// 获取章节下面的课件,教案,视频
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sid"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesList(long courseId, long sid);

        /// <summary>
        ///  获取章节下面的课件,教案,视频按类型筛选
        /// </summary>
        /// <param name="courseId"></param>
        /// <param name="sid"></param>
        /// <param name="fileType"></param>
        /// <returns></returns>
        Task<ResponseContext<SectionFileListDto>> GetSectionFilesListByType(long courseId, long sid, int fileType);

        /// <summary>
        ///  获取章节下面的课件,教案,视频按类型筛选
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionFilesDto>>> GetSectionFilesListByCourseId(long courseId);

        /// <summary>
        /// 删除教材
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSectionTextBook(long id);

        /// <summary>
        /// 获取教材内容
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionTextBookDto>>> GetSectionTextBookList(long sid);

        /// <summary>
        /// 获取章节下面的概要
        /// </summary>
        /// <param name="sid">章节Id</param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionSummaryDto>>> GetCourseSummaryList(long sid);

        /// <summary>
        ///  新增,编辑课程场景实训
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddOrEditSectionSceneTraining(YssxSectionSceneTrainingDto dto);

        /// <summary>
        /// 删除课程场景实训
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RemoveSectionSceneTraininge(long id);

        /// <summary>
        /// 获取课程场景实训列表(根据章节Id)
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        Task<ResponseContext<List<YssxSectionSceneTrainingViewModel>>> GetYssxSectionSceneTrainingList(long sid);

        /// <summary>
        /// 根据课程id查询章节列表信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<SectionListDto>>> GetSectionListByCourseId(long courseId);

        /// <summary>
        /// 根据章节id获取附件集合
        /// </summary>
        /// <param name="sid"></param>
        /// <returns></returns>
        Task<ResponseContext<SectionFileListNewDto>> GetSectionFilesListBySectionId(long sid);
        /// <summary>
        /// 根据课程id获取教材列表
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        Task<ResponseContext<YssxSectionFilesDto>> GetCourseFilesListByCourseId(long courseId);
        /// <summary>
        /// 新增/修改课程章节
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveSection(YssxSectionDto dto);
    }
   
}

