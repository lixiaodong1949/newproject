﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto.Tax;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 个税计算
    /// </summary>
    public interface ITaxCalculatorService
    {
       /// <summary>
       /// 所得税计算
       /// </summary>
       /// <param name="taxType">类型0:特权,1:稿酬,2:劳务,3:工资,4:财产,5:偶然</param>
       /// <param name="x1"></param>
       /// <param name="x2"></param>
       /// <returns></returns>
        Task<ResponseContext<TaxCalculatorDto>> TaxCalculator(int taxType,decimal x1,decimal x2, int monthType);

        /// <summary>
        /// <param name="taxType">类型0:特权,1:稿酬,2:劳务,3:工资,4:财产,5:偶然</param>
        /// </summary>
        /// <param name="amount">金额</param>
        /// <returns></returns>
        Task<ResponseContext<TaxCalculatorSignDto>> TaxCalculatorSign(int taxType,decimal amount);
    }
}
