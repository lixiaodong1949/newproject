using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.S.Dto;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.Framework.Entity;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 积分商品兑换接口服务类
    /// </summary>
    public interface IIntegralProductExchangeService
    {
        /// <summary>
        /// 添加和修改
        /// </summary>
        /// <returns></returns>
        /// <param name="model"></param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> Save(IntegralProductExchangeDto model, UserTicket currentUser);

        /// <summary>
        /// 根据Id获取一条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<IntegralProductExchangeDto>> Get(long id);

        /// <summary>
        /// 根据Id删除数据
        /// </summary>
        /// <param name="id">唯一Id</param>
        /// <param name="currentUserId">当前登录用户Id</param>
        Task<ResponseContext<bool>> Delete(long currentUserId, params long[] id);

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<IntegralProductExchangePageResponseDto>> ListPage(IntegralProductExchangePageRequestDto query);

        /// <summary>
        /// 根据条件查询列表信息 分页
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<IntegralProductExchangePageByUserIdResponseDto>> ListPageByUserId(IntegralProductExchangePageRequestDto query);

        /// <summary>
        /// 获取列表(不分页)
        /// </summary>
        /// <returns></returns>
        Task<ListResponse<IntegralProductExchangeListResponseDto>> List(IntegralProductExchangeListRequestDto query);
    
        /// <summary>
        /// 修改发货信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateDeliveryInfo(UpdateDeliveryInfoRequestDto model, long currentUserId);
    
        /// <summary>
        /// 修改收货地址
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> UpdateAddressInfo(ProductExchangeAddressRequestDto model, long currentUserId);
    }
}