﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;

namespace Yssx.S.IServices.Mall
{
    /// <summary>
    /// 订单
    /// </summary>
    public interface IMallOrderService
    {
        #region 订单
        Task MigrateAsync();

        Task<ResponseContext<UserMallOrder>> GetUserOrders(GetUserMallOrder input);

        Task<ResponseContext<MallOrderDto>> GetUserOrder(long orderNo);

        Task<PageResponse<MallOrderDto>> GetOrderList(GetMallOrderDto input);

        Task<ResponseContext<MallOrderStatsDto>> GetStats();
        #endregion

        #region APP
        /// <summary>
        /// 查询用户订单列表 - APP
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<GetUserOrderListAppDto>> GetUserOrderListApp(GetUserOrderListAppRequest model, long currentUserId);

        /// <summary>
        /// 查询指定订单 - APP
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<MallOrderDto>> GetOrderByIdApp(long id, long currentUserId);

        #endregion

        #region 手动配置记录
        /// <summary>
        /// 获取手动配置记录列表（分页）
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<FreeTestPaperDto>> GetFreeTestPaper(GetFreeTestPaperRequest model);
        #endregion

    }
}
