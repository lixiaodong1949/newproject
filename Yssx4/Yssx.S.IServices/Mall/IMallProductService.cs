﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Dto.Mall;

namespace Yssx.S.IServices.Mall
{
    /// <summary>
    /// 商品
    /// </summary>
    public interface IMallProductService
    {
        Task<PageResponse<MallProductDto>> GetProductList(GetMallProductDto input);


        #region 商品

        /// <summary>
        /// 获取商品价格
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<List<GetProductPriceOutputDto>>> GetProductPrices(GetProductPriceInputDto input);

        /// <summary>
        /// 保存商品
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveProduct(SaveProductDto model, long currentUserId);

        /// <summary>
        /// 删除商品
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteProduct(long id, long currentUserId);

        /// <summary>
        /// 获取商品列表
        /// </summary>
        Task<PageResponse<GetProductByPageDto>> GetProductByPage(GetProductByPageRequest model);

        /// <summary>
        /// 获取指定商品数据
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<SaveProductDto>> GetProductInfoById(long id);

        /// <summary>
        /// 商品上下架
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetProductStatus(long id, bool isActive, long currentUserId);


        /// <summary>
        /// 保存商品明细
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveProductDetail(SaveProductDetailDto model, long currentUserId);

        /// <summary>
        /// 删除商品明细
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteProductDetail(long id, long productId, long currentUserId);

        /// <summary>
        /// 商品移出指定分类
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteProductCategory(long id, long categoryId, long currentUserId);

        /// <summary>
        /// 设置商品分类信息
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetProductCategoryInfo(SetProductCategoryInfoDto model, long currentUserId);

        /// <summary>
        /// 设置商品排序
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetProductSortInfo(SetProductSortInfoDto model, long currentUserId);

        /// <summary>
        /// 商品点赞
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetProductLikesNumber(long id, long currentUserId);

        #endregion

        #region 商品分类
        /// <summary>
        /// 获取商品分类列表
        /// </summary>
        Task<PageResponse<GetMallCategoryListDto>> GetMallCategoryList();

        /// <summary>
        /// 保存商品分类
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SaveMallCategory(SaveMallCategoryDto model, long currentUserId);

        /// <summary>
        /// 删除商品分类
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteMallCategory(long id, long currentUserId);

        /// <summary>
        /// 获取二级分类下商品列表
        /// </summary>
        Task<PageResponse<GetSecondLevelProductByPageDto>> GetSecondLevelProductByPage(GetSecondLevelProductByPageRequest model);

        /// <summary>
        /// 分类升为一级分类
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetMallCategoryLevel(long id, long currentUserId);



        #endregion

        #region 获取关联数据
        /// <summary>
        /// 获取关联数据 课程列表
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<YssxCourseDto>> GetBindDataInCourse(GetBindDataInCourseDto model);

        #endregion

        #region APP商城

        /// <summary>
        /// 获取商品分类列表 - APP
        /// </summary>
        Task<PageResponse<GetMallCategoryListDto>> GetMallCategoryListApp();

        /// <summary>
        /// 获取商品二级分类列表 - APP
        /// </summary>
        Task<PageResponse<GetMallCategoryListDetailDto>> GetMallCategoryByParentIdApp(long id);


        /// <summary>
        /// 商品列表
        /// </summary>
        /// <param name="top"></param>
        /// <returns></returns>
        Task<PageResponse<ProductBaseResponseDto>> QueryProductsApp(QueryProductsRequestDto model);
        
        /// <summary>
        /// 首页获取一级分类下TOP部分商品
        /// </summary>
        /// <param name="top"></param>
        /// <returns></returns>
        Task<ResponseContext<List<GetCategoryProductsTopToHomeResponseDto>>> GetCategoryProductsTopToHomeApp(int top = 4);


        #endregion

    }
}
