﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;
using Yssx.S.Pocos;

namespace Yssx.M.IServices
{
    /// <summary>
    /// 用户管理相关接口
    /// </summary>
    public interface IbkUserService
    {

        #region 前端用户
        /// <summary>
        /// 新增 更新 用户信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUserinfo(YssxbkUserDao dto, long Id);

        Task<ResponseContext<bool>> AddUserList(UserListInfoDto dto);

        /// <summary>
        /// 删除 前端用户
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteUserinfo(long Id, long currentUserId);

        /// <summary>
        /// 获取 前端用户【废弃】
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<YssxUserResponse>>> FindUserByName(YssxQuerybkUserDao model);

        /// <summary>
        /// 获取用户列表（分页）
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<UserDto>> GetUserList(GetUserListRequest model);

        /// <summary>
        /// 查询用户统计列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<GetUserCountByPageDto>> GetUserCountByPage(GetUserCountByPageRequest model);


        /// <summary>
        /// 重置 前端用户密码
        /// </summary>
        /// <param name="md"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> ResetPassword(ResetPassword md, long Id);

        /// <summary>
        ///  启用禁用 前端用户状态 
        /// </summary>
        /// <param name="md"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> StateControl(StateControl md, long Id);

        /// <summary>
        /// 批量生成 前端用户
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddBatchUserinfo(YssxbkUserDao dto);
        #endregion

        #region 激活码
        /// <summary>
        /// 随机 生成学校激活码
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<YssxRandomCodeDao>> GetRandomCodeDao();

        /// <summary>
        /// 添加 更新 禁用 学校激活码
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddActivationCode(YssxActivationCodeListDto dto, long Id);

        /// <summary>
        /// 学生激活码
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddStudentActivCode(YssxActivationCodeDto dto,long uid);

        /// <summary>
        /// 获取 学校激活码
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<YssxActivationCodeDto>>> GetActivationCode(YssxQueryActivationCode model);

        /// <summary>
        /// 获取 学校学生激活码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<PageResponse<YssxActivationCodeDto>>> GetActivationCodeByTypeId(YssxQueryActivationCode model);

        /// <summary>
        ///  删除 学校激活码
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteActivationCode(long Id, long oId);

        /// <summary>
        /// 导出 学校激活码
        /// </summary>
        /// <returns></returns>
        Task<IActionResult> ExportActivationCode();
        #endregion

        #region 后端用户
        /// <summary>
        /// 添加 更新 后端用户
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddBackUserinfo(YssxbkbkUserDao dto, long Id);
        /// <summary>
        /// 重置 后端用户密码
        /// </summary>
        /// <param name="md"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> BkResetPassword(BkResetPassword md, long Id);
        #endregion

        #region 绑定手机号、微信
        /// <summary>
        /// 绑定手机号
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> BindPhoneByUser(string mobilePhone, string verificationCode, long currentUserId);

        /// <summary>
        /// 绑定微信
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> BindWechartByUser(string wechart, string unionid, long currentUserId);

        #endregion


        /// <summary>
        /// 合伙人名单
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<PartnerDto>>> GetPartnerList();

        /// <summary>
        /// 设置是否已经联系合伙人
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetPartnerStatus(long id);
    }
}
