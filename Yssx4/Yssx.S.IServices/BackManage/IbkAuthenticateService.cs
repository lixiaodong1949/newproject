﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 系统认证
    /// </summary>
    public interface IbkAuthenticateService
    {
        /// <summary>
        /// 登陆认证
        /// </summary>
        /// <param name="dto">dto</param>
        /// <param name="loginIp">loginIp</param>
        /// <param name="_requirement">_requirement</param>
        /// <returns></returns>
        Task<ResponseContext<bkUserTicket>> bkUserAuthentication(UserbkAuthentication dto,string loginIp, PermissionRequirement _requirement);
    }
}
