﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;

namespace Yssx.M.IServices
{
    /// <summary>
    /// 学生管理相关接口
    /// </summary>
    public interface IStudentService
    {
        /// <summary>
        /// 学生信息审核
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> StudentAudit(StudentDto model, long Id);

        /// <summary>
        /// 添加 更新 学生
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateStudent(StudentDto model, long Id);

        /// <summary>
        /// 获取学生列表
        /// </summary>
        Task<ResponseContext<StudentDtoSummary>> GetStudents(GetStudentRequest model, long Id);

        /// <summary>
        /// 删除学生
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteStudent(StudentDto model, long Id);


        /// <summary>
        /// 获取班级学生列表
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<ResponseContext<List<StudentClassListDto>>> GetStudentsByClassIds(List<long> ids);

        /// <summary>
        /// 导出学生
        /// </summary>
        /// <returns></returns>
        //[Microsoft.AspNetCore.Authorization.AllowAnonymous]
        //Task<IActionResult> ExportStudent(long classId, long entranceYear, string keyWord, string mobilePhone);

        /// <summary>
        /// 导入学生
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<ImportStudentResponse>>> ImportStudent(IFormFile form, long Id);

    }
}
