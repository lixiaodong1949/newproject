﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    /// <summary>
    /// Banner图服务接口
    /// </summary>
    public interface IBannerService
    {
        /// <summary>
        /// 增加/修改
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateBanner(BannerRequest model, long userId);
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> DeleteBanner(long id);
        /// <summary>
        /// 查询Banner列表
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<BannerInfoDto>>> GetBannerInfoList(ClientType? clientType);


    }
}
