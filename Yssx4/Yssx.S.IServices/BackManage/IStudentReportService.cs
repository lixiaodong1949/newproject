﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto.Report;

namespace Yssx.M.IServices
{
    /// <summary>
    /// 成绩统计相关
    /// </summary> 
    public interface IStudentReportService
    {
        #region 历史成绩

        /// <summary>
        /// 获取最近一次成绩
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<StudentGradeInfoDto>> GetStudentLastGrade(long examId);

        /// <summary>
        /// 获取所有历史成绩
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<StudentGradeInfoDto>> GetStudentGradeList(StudentGradeListRequest model);

        #endregion

        #region 团队成绩

        /// <summary>
        /// 最近一次团队成绩
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<StudentTeamGradeInfoDto>> GetTeamLastGrade(long examId);

        /// <summary>
        /// 历史团队成绩
        /// </summary>
        /// <returns></returns>
        Task<PageResponse<StudentTeamGradeInfoDto>> GetTeamGradeList(StudentGradeListRequest model);
        /// <summary>
        ///  错题分析
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<StudentErrorQuestionDto>>> GetErrorQuestionList(long examId);
        #endregion
    }
}
