﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Entity;
using Yssx.S.Dto;

namespace Yssx.S.IServices
{
    public interface IAdvertisingSpaceService
    {

        /// <summary>
        /// 获取 广告位列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<AdvertisingSpaceListResponse>> GetAdvertisingSpaceList(AdvertisingSpaceListDao model);

        /// <summary>
        /// 添加 更新 广告位
        /// </summary>
        /// <param name="md"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateAdvertisingSpace(AdvertisingSpaceDao md, long Id);

        /// <summary>
        /// 获取 广告位列表-详情
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<PageResponse<AdvertisingSpaceDetailsListResponse>> GetAdvertisingSpaceDetailList(AdvertisingSpaceDetailsListDao model, long Id);

        /// <summary>
        /// 添加 更新 广告位-详情
        /// </summary>
        /// <param name="md"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> AddUpdateAdvertisingSpaceDetails(AdvertisingSpaceDetailsDao md, long Id);

        /// <summary>
        /// 删除 广告位-详情
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="oId"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> Delete(long Id, long oId);

        /// <summary>
        /// 状态 广告位-详情
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="State"></param>
        /// <param name="Oid"></param>
        /// <returns></returns>
        Task<ResponseContext<bool>> State(long Id, int State,long Oid);




        /// <summary>
        /// 获取 广告位和详情
        /// </summary>
        /// <param name="PlatformType"></param>
        /// <returns></returns>
        Task<ResponseContext<AdvertisingSpaceChartResponse>> AdvertisingSpaceChart(int PlatformType);
    }
}
