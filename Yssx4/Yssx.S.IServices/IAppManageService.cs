﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.S.Dto;
using Yssx.S.Dto.Order;

namespace Yssx.S.IServices
{
    /// <summary>
    /// App管理
    /// </summary>
    public interface IAppManageService
    {
        /// <summary>
        /// 获取最新版本信息
        /// </summary>
        /// <returns>支付表单</returns>
        Task<ResponseContext<AppNewVersionInfoDto>> NewVersionInfo(AppIdEnum appid, int type);
    }
}
