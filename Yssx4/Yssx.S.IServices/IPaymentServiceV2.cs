﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto.Mall;

namespace Yssx.S.IServices
{
    public interface IPaymentServiceV2
    {
        /// <summary>
        /// 支付宝
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<PayResponse>> Alipay(CreateMallOrderDto input,UserTicket currentUser);

        /// <summary>
        /// 支付宝手机支付
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        Task<ResponseContext<PayResponse>> AlipayWap(CreateMallOrderDto input, UserTicket currentUser);

        /// <summary>
        /// 支付宝App支付
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PayResponse>> AlipayApp(CreateMallOrderDto input, int appEnum, UserTicket currentUser);

        /// <summary>
        /// 支付宝App支付 - 待支付订单
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PayResponse>> AlipayAppContinue(long id, long currentUserId);

        /// <summary>
        /// 支付宝异步回调
        /// </summary>
        /// <param name="input"></param>
        /// <param name="payType"></param>
        void AlipayNotify(Dictionary<string, string> input, int payType);

        /// <summary>
        /// 微信
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<PayResponse>> Webchat(CreateMallOrderDto input, UserTicket currentUser);

        /// <summary>
        /// 微信H5
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<PayResponse>> WebchatWap(CreateMallOrderDto input, UserTicket currentUser);

        /// <summary>
        /// 微信App
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<PayWechatResponse>> WechatApp(CreateMallOrderDto input, UserTicket currentUser);

        /// <summary>
        /// 微信App - 3d云实习
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponseContext<PayWechatResponse>> WechatApp3d(CreateMallOrderDto input, UserTicket currentUser);

        /// <summary>
        /// 微信小程序
        /// </summary>
        /// <param name="input"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>

        Task<ResponseContext<PayWechatResponse>> WechatJsapi(CreateMallOrderDto input, UserTicket currentUser);

        /// <summary>
        /// 微信App - 待支付订单
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<PayWechatResponse>> WechatAppContinue(long id, long currentUserId);

        /// <summary>
        /// 微信异步回调
        /// </summary>
        /// <param name="xmlString"></param>
        void WebchatNotify(string xmlString);
    }
}
