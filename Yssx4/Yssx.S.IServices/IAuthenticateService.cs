﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Yssx.Framework;
using Yssx.Framework.Auth;
using Yssx.Framework.Authorizations;
using Yssx.S.Dto;
using Yssx.S.Dto.Means;

namespace Yssx.S.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAuthenticateService
    {
        /// <summary>
        /// 用户密码登陆
        /// </summary>
        /// <param name="dto">用户信息</param>
        /// <param name="loginIp">IP地址</param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        Task<ResponseContext<UserTicket>> UserAuthentication(UserAuthentication dto, string loginIp, PermissionRequirement _requirement);

        /// <summary>
        /// 电话号码登陆
        /// </summary>
        /// <param name="mb">电话号码</param>
        /// <param name="verificationCode">验证码</param>
        /// <param name="loginIp">IP地址</param>
        /// <param name="_requirement"></param>
        /// <param name="loginType">登录方式 0:PC 1:App 2.小程序</param>
        /// <returns></returns>
        Task<ResponseContext<UserTicket>> NumberUserPhoneAuthentication(string mb, string verificationCode, string loginIp, PermissionRequirement _requirement, int loginType);

        /// <summary>
        /// 微信登陆
        /// </summary>
        /// <param name="wx">微信openid</param>
        /// <param name="unionid">微信unionid</param>
        /// <param name="nickname">微信昵称</param>
        /// <param name="loginIp">用户IP</param>
        /// <param name="_requirement"></param>
        /// <param name="loginType">登录方式 0:PC 1:App</param>
        /// <returns></returns>
        Task<ResponseContext<WeChartTicket>> WeChartAuthentication(string wx, string unionid, string nickname, string loginIp, PermissionRequirement _requirement, int loginType);

        /// <summary>
        /// 小程序登陆
        /// </summary>
        /// <param name="model"></param>
        /// <param name="loginIp">用户IP</param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        Task<ResponseContext<WeChartTicket>> WeChartAppletsAuthentication(WeChartAppletsDto model, string loginIp, PermissionRequirement _requirement);

        /// <summary>
        /// 工学号登陆获取学校
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<List<DropdownListDto>>> DropdownList();

        /// <summary>
        /// 找回密码
        /// </summary>
        /// <param name="mb">电话</param>
        /// <param name="verificationCode">验证码</param>
        /// <param name="password">密码</param>
        /// <param name="againPassword">再次密码</param>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> RetrievePassword(string mb, string verificationCode, string password, string againPassword);
        /// <summary>
        ///  设置密码
        /// </summary>
        /// <param name="verificationCode">验证码</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        Task<ResponseContext<bool>> SetPassword(string password, UserTicket CurrentUser, string verificationCode = null);

        /// <summary>
        /// 工学号登陆
        /// </summary>
        /// <param name="schoolId">学校</param>
        /// <param name="engineeringnumber">工学号</param>
        /// <param name="password">密码</param>
        /// <param name="loginIp">IP地址</param>
        /// <param name="_requirement"></param>
        /// <param name="loginType">登录方式 0:PC 1:App</param>
        /// <returns></returns>
        Task<ResponseContext<UserTicket>> EngineeringNumberLogin(long schoolId, string engineeringnumber, string password, string loginIp, PermissionRequirement _requirement, int loginType);

        /// <summary>
        /// 获取token - 游客
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<string>> GetExperienceToken(PermissionRequirement _requirement);

        Task<ResponseContext<UserTicket>> GetExperienceToken2(PermissionRequirement _requirement);

        /// <summary>
        /// 检查当前用户的用户类型是否改变
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<CheckUserRoleResponseDto>> CheckUserRole(UserTicket user, string loginIp, int loginType);

        /// <summary>
        /// 获取新token - 当前用户
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<string>> GetCurrentUserToken(PermissionRequirement _requirement, long currentUserId, string loginIp, int loginType);

        /// <summary>
        /// 获取新token - 当前用户 - new
        /// </summary>
        /// <returns></returns>
        Task<ResponseContext<UserTicket>> GetCurrentUserNewToken(PermissionRequirement _requirement, long currentUserId, string loginIp, int loginType);


        /// <summary>
        /// 用户密码登录(学生身份) 通过教师邀请
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="loginIp"></param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        Task<ResponseContext<UserTicket>> UserPasswordLoginByInvite(UserLoginRequestModel dto, string loginIp, PermissionRequirement _requirement);

        /// <summary>
        /// 用户验证码登录(学生身份) 通过教师邀请
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="loginIp">用户IP</param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        Task<ResponseContext<UserTicket>> UserVerificationCodeLoginByInvite(UserLoginRequestModel dto, string loginIp, PermissionRequirement _requirement);

        /// <summary>
        /// 用户微信登录(学生身份) 通过教师邀请
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="loginIp">用户IP</param>
        /// <param name="_requirement"></param>
        /// <returns></returns>
        Task<ResponseContext<WeChartTicket>> UserWeChatLoginByInvite(UserLoginRequestModel dto, string loginIp, PermissionRequirement _requirement);
    }
}
