﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    [Table(Name = "loginfo")]
    public class LogInfo : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 日志信息
        /// </summary>
        [Column(Name = "Message", DbType = "varchar(255)", IsNullable = true)]
        public string Message { get; set; }

        public string Name11 { get; set; }

    }
    [Table(Name = "loginfo_detail")]
    public class LogInfoDetail : TenantBizBaseEntity<long>
    {
        public long LogId { get; set; }
        /// <summary>
        /// 日志信息
        /// </summary>
        [Column(Name = "MessageDetail", DbType = "varchar(255)", IsNullable = true)]
        public string MessageDetail { get; set; }


    }
}
