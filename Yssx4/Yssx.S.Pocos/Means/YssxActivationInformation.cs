﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 激活信息
    /// </summary>
    [Table(Name = "yssx_activationcode_information")]
    public class YssxActivationInformation : TenantBaseEntity<long>
    {
        /// <summary>
        /// 姓名
        /// </summary>
        public string Nmae { get; set; }
        /// <summary>
        /// 工号或者学号
        /// </summary>
        public string WorkNumber { get; set; }
        /// <summary>
        /// 院系
        /// </summary>
        public long DepartmentsId { get; set; }
        /// <summary>
        /// 专业
        /// </summary>
        public long ProfessionalId { get; set; }
        /// <summary>
        /// 班级
        /// </summary>
        public long? ClassId { get; set; }
        /// <summary>
        /// 类型(0管理员,1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户)
        /// </summary>
        public int TypeId { get; set; }
        /// <summary>
        /// 类型名称
        /// </summary>
        public string TypeName { get; set; }
        /// <summary>
        /// 激活码Id
        /// </summary>
        public long ActivationCodeId { get; set; }
    }
}
