﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Mall
{
    /// <summary>
    /// 商城产品项
    /// </summary>
    [Table(Name = "yssx_mall_product_item")]
    public class YssxMallProductItem : BizBaseEntity<long>
    {
        /// <summary>
        /// 产品id
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// 分类 1 案例 2 月度任务
        /// </summary>
        public int Category { get; set; }

        /// <summary>
        /// 标的物Id
        /// </summary>
        public long TargetId { get; set; }

        /// <summary>
        /// 标的物名称
        /// </summary>
        public string TargetName { get; set; }

        /// <summary>
        /// 来源 （1.云实习真账实操 2.课程同步实训 3.业税财实训 4.云上实训之岗位实训 5.课程实训 6.经验案例 7.初级会计考证）
        /// </summary>
        public int Source { get; set; }
    }
}
