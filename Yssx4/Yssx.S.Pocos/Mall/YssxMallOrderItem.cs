﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Mall
{
    /// <summary>
    /// 商城订单项
    /// </summary>
    [Table(Name = "yssx_mall_order_item")]
    public class YssxMallOrderItem : BizBaseEntity<long>
    {
        /// <summary>
        /// 订单id
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 商品id
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// 单价
        /// </summary>
        public decimal UnitPrice { get; set; }

        /// <summary>
        /// 优惠金额
        /// </summary>
        public decimal DiscountAmount { get; set; }
    }
}
