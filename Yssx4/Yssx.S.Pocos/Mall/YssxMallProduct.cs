﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Mall
{
    /// <summary>
    /// 商城产品
    /// </summary>
    [Table(Name = "yssx_mall_product")]
    public class YssxMallProduct : BizBaseEntity<long>
    {
        /// <summary>
        /// 商品名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string ShortDesc { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(Name = "FullDesc", DbType = "LongText", IsNullable = true)]
        public string FullDesc { get; set; }

        /// <summary>
        /// 现价
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal OldPrice { get; set; }

        /// <summary>
        /// 状态：
        /// * true 上架
        /// * false 下架
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 开售状态（0.立即开售 1.定时开售）
        /// </summary> 
        public int SaleStatus { get; set; }

        /// <summary>
        /// 定时开售时间
        /// </summary>
        public DateTime? RegularSaleTime { get; set; }

        /// <summary>
        /// 定时开售价格
        /// </summary>
        public decimal RegularSalePrice { get; set; }

        /// <summary>
        /// 点赞数
        /// </summary> 
        public int LikesNumber { get; set; }

        /// <summary>
        /// 观看数
        /// </summary> 
        public int ViewsNumber { get; set; }

    }
}
