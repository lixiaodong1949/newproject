﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 商品分类关联表
    /// </summary>
    [Table(Name = "yssx_mall_product_category")]
    public class YssxMallProductCategory : BizBaseEntity<long>
    {
        /// <summary>
        /// 产品ID
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// 一级分类ID
        /// </summary>
        public long FirstCategoryId { get; set; }

        /// <summary>
        /// 二级分类ID
        /// </summary>
        public long SecondCategoryId { get; set; }

    }
}
