﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Mall
{
    /// <summary>
    /// 商城订单
    /// </summary>
    [Table(Name = "yssx_mall_order")]
    public class YssxMallOrder : BizBaseEntity<long>
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 支付渠道
        /// * 1 微信 
        /// * 2 支付宝
        /// </summary>
        public int PaymentType { get; set; }

        /// <summary>
        /// 支付时间
        /// </summary>
        public DateTime? PaymentTime { get; set; }

        /// <summary>
        /// 订单来源
        /// * 1 抢购活动
        /// * 2 微店
        /// * 3 前台购买
        /// </summary>
        public int Source { get; set; }

        /// <summary>
        /// 订单实付金额
        /// </summary>
        public decimal OrderTotal { get; set; }

        /// <summary>
        /// 优惠总金额
        /// </summary>
        public decimal OrderDiscount { get; set; }

        /// <summary>
        /// 状态（0.未支付 1.已取消 2.已付款）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 第三方支付交易号
        /// </summary>
        public string TransactionNo { get; set; }

        /// <summary>
        /// 推广编码
        /// </summary>
        public long AffiliateId { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
