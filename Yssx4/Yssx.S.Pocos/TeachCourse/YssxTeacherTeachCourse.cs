﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 教师上课表
    /// </summary>    
    [Table(Name = "yssx_teacher_teach_course")]
    public class YssxTeacherTeachCourse : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 上课名称
        /// </summary>
        public string TeachCourseName { get; set; }

        /// <summary>
        /// 上课状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        public TeachCourseStatus TeachCourseStatus { get; set; }
    }
}
