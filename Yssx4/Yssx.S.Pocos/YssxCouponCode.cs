﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 优惠券
    /// </summary>
    [Table(Name = "yssx_coupon_code")]
    public class YssxCouponCode
    {
        public long Id { get; set; }

        public string Code { get; set; }

        public int Index { get; set; }

        public DateTime CreateTime { get; set; }

    }
}
