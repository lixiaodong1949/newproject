﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.UserPractice
{
    /// <summary>
    /// 试卷表
    /// </summary>
    [Table(Name = "zdap_exam_paper")]
    public class ZdapExamPaper : TenantBizBaseEntity<long>
    {
        public ZdapExamPaper()
        {
        }

        /// <summary>
        /// 试卷命名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 试卷所属人Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 公司(案例)Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 月份任务Id
        /// </summary>
        public long MonthTaskId { get; set; }

        /// <summary>
        /// 考试类型
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public ExamType ExamType { get; set; }

        /// <summary>
        /// 赛事类型（0 个人赛 1团体赛）
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public CompetitionType CompetitionType { get; set; }

        /// <summary>
        /// 结束之前是否允许查看解析
        /// </summary>
        public bool CanShowAnswerBeforeEnd { get; set; }

        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 合格分数
        /// </summary>
        public decimal PassScore { get; set; }

        /// <summary>
        /// 总题数
        /// </summary>
        public int TotalQuestionCount { get; set; }

        /// <summary>
        /// 考试开始时间
        /// </summary>
        public DateTime? BeginTime { get; set; }

        /// <summary>
        /// 考试结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 是否已发布
        /// 发布后学生任务列表可见
        /// </summary>
        public bool IsRelease { get; set; } = false;

        /// <summary>
        /// 考试状态  0:未开始 1:已开始 2:已结束
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public ExamStatus Status { get; set; }

        /// <summary>
        /// 试卷来源类型 0:订单购买 1:平台免费
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public SxExamSourceType ExamSourceType { get; set; }

    }
}
