﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 公告
    /// </summary>
    [Table(Name = "yssx_notice")]
    public class YssxNotice
    {
        public long Id { get; set; }

        public string Title { get; set; }

        /// <summary>
        /// 详细内容
        /// </summary>
        [Column(Name = "Content", DbType = "VARCHAR(8000)", IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 0：所有平台 1:技能抽查,2:技能竞赛,3:专一网
        /// </summary>
        public int Platform { get; set; }

        public int Sort { get; set; }

        [Column(Name = "Status", MapType = typeof(int))]
        public Status Status { get; set; }

        public int IsDelete { get; set; } = 0;

        public DateTime CreateTime { get; set; }
    }
}
