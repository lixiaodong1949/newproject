﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 用户积分
    /// </summary>
    [Table(Name = "yssx_integral")]
    public class YssxIntegral
    {
        public long Id { get; set; }

        public long UserId { get; set; }
        /// <summary>
        /// 当前可用总分
        /// </summary>
        public long Number { get; set; }

        /// <summary>
        /// 已使用积分
        /// </summary>
        public long UsedNumber { get; set; } = 0;

        /// <summary>
        /// 商品兑换次数
        /// </summary>
        public int ProductExchangeNumber { get; set; } = 0;

        public DateTime UpdateTime { get; set; }

        public int IsDelete { get; set; } = 0;

    }

    /// <summary>
    /// 用户积分排行
    /// </summary>
    [Table(Name = "yssx_integral_top")]
    public class YssxIntegralTop
    {
        public long Id { get; set; }

        public long UserId { get; set; }
        /// <summary>
        /// 当前可用总分
        /// </summary>
        public long Number { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [Column(Name = "MobilePhone", DbType = "varchar(255)", IsNullable = true)]
        public string MobilePhone { get; set; }
        /// <summary>
        /// 性别
        /// </summary> 
        public string Sex { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        [Column(Name = "RealName", DbType = "varchar(255)", IsNullable = true)]
        public string RealName { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        [Column(Name = "SchoolName", DbType = "varchar(255)", IsNullable = true)]
        public string SchoolName { get; set; }

        [Column(Name = "CreateTime", IsNullable = true)]
        public DateTime CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 用户注册来源那个系统
        /// </summary>
        [Column(Name = "RegisterSysId", MapType = typeof(int), DbType = "int(11)")]
        public RegisterSourceEnum RegisterSource { get; set; }

    }

    /// <summary>
    /// 积分详情
    /// </summary>
    [Table(Name = "yssx_integral_datails")]
    public class YssxIntegralDetails
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public long Number { get; set; }

        /// <summary>
        /// 当前总分
        /// </summary>
        public long IntegralSum { get; set; }

        /// <summary>
        /// 状态：1：收入，2消费
        /// </summary>
        public int StatusType { get; set; }

        /// <summary>
        /// 来源 0:消费 1、每日登录
        ///2、答题
        ///3、关注公众号
        ///4、公众号设星标及置顶
        ///5、文章转载或阅读
        ///6、撰写文章
        ///7、推荐好友
        ///8、下载服务
        ///9、签到
        ///10、购买商品
        ///11、浏览商品
        /// </summary>
        public int FromType { get; set; }

        /// <summary>
        /// 收入和消费描述
        /// </summary>
        public string Desc { get; set; }

        /// <summary>
        /// 关联Id
        /// </summary>
        public long RelatedId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 日期格式：DateTime.Now.ToString("yyyy-MM-dd")
        /// </summary>
        public string Date { get; set; }

        public int IsDelete { get; set; } = 0;
    }
}
