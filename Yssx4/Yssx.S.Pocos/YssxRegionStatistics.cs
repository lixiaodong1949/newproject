﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Pocos
{
    [Table(Name = "yssx_region_statistics")]
    public class YssxRegionStatistics
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public long Count { get; set; }

        public string Date { get; set; }

        /// <summary>
        /// 月份
        /// </summary>
        public int Month { get; set; }
    }
}

