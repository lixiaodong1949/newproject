using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 签到详情
    /// </summary>
    [Table(Name = "yssx_signin_details")]
    public class YssxSigninDetails : BizBaseEntity<long>
    {
        /// <summary>
        /// 合同分类Id
        /// </summary>
        [Column(Name = "UserId", DbType = "bigint(20)")]
        public long UserId { get; set; }

        /// <summary>
        /// 日期
        /// </summary>
        [Column(Name = "Date", DbType = "LongText", IsNullable = true)]
        public string Date { get; set; }

    }
}