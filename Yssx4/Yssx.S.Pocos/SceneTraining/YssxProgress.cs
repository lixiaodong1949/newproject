﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.SceneTraining
{
    /// <summary>
    /// 实训记录表
    /// </summary>
    [Table(Name = "yssx_progress")]
    public class YssxProgress:BizBaseEntity<long>
    {
        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long CjsxId { get; set; }
        /// <summary>
        /// 流程详情Id
        /// </summary>
        public long LcxqId { get; set; }
        /// <summary>
        /// 步数
        /// </summary>
        public int Bs { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>
        public long? TmId { get; set; }
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// 实训状态
        /// </summary>
        [Column(DbType = "int")]
        public TrainType TrainType { get; set; }
    }
}
