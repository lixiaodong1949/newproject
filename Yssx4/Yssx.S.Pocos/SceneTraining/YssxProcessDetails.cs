﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.SceneTraining
{
    /// <summary>
    /// 流程详情
    /// </summary>
    [Table(Name = "yssx_process_details")]
    public class YssxProcessDetails : BizBaseEntity<long>
    {
        /// <summary>
        /// 流程名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选择部门
        /// </summary>
        public long? Xzbm { get; set; }
        /// <summary>
        /// 选择人员
        /// </summary>
        public long? Xzry { get; set; }
        /// <summary>
        /// 上传资料
        /// </summary>
        [Column(Name = "Sczl", DbType = "text(0)", IsNullable = true)]
        public string Sczl { get; set; }
        /// <summary>
        /// 资料图片
        /// </summary>
        public string Zltp { get; set; }
        /// <summary>
        /// 添加题目
        /// </summary>
        [Column(Name = "TjtmId", DbType = "text(0)", IsNullable = true)]
        public string TjtmId { get; set; }
        /// <summary>
        /// 掌握技能和注意事项
        /// </summary>
        public string Jnsx { get; set; }
        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long CjsxId { get; set; }
        /// <summary>
        /// 流程序号
        /// </summary>
        public int? Lcxh { get; set; }
    }
}
