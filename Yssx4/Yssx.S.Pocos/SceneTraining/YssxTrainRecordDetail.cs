﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.SceneTraining
{
    /// <summary>
    /// 实训记录表
    /// </summary>
    [Table(Name = "yssx_trainrecord_detail")]
    public class YssxTrainRecordDetail : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 实训记录ID
        /// </summary>
        public long ProgressId { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 学生ID
        /// </summary>
        public long StudentId { get; set; }
        /// <summary>
        /// 场景实训ID
        /// </summary>
        public long TrainId { get; set; }
        /// <summary>
        /// 题目ID
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 父级题目ID
        /// </summary>
        public long ParentQuestionId { get; set; }
        /// <summary>
        /// 答案解析
        /// </summary>
        [Column(DbType = "longtext")]
        public string Answer { get; set; }
        /// <summary>
        /// 作答与正确答案的对比信息
        /// </summary>
        [Column(DbType = "longtext")]
        public string AnswerCompareInfo { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column(DbType = "int")]
        public AnswerResultStatus Status { get; set; }

        ///// <summary>
        ///// 分录题状态
        ///// </summary>
        //public AccountEntryStatus AccountEntryStatus { get; set; }

        ///// <summary>
        ///// 分录题审核状态
        ///// </summary>
        //public AccountEntryAuditStatus AccountEntryAuditStatus { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        [Column(DbType = "decimal(10,4)")]
        public decimal Score { get; set; }

        ///// <summary>
        ///// 是否已交卷
        ///// </summary>
        //public bool IsSubmit { get; set; } = false;
    }
}
