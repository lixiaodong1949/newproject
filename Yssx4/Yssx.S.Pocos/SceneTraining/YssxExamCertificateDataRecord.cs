﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 用户练习凭证记录数据
    /// </summary>
    [Table(Name = "yssx_course_certificate_datarecord")]
    public class YssxCourseCertificateDataRecord : TenantBizBaseEntity<long>
    {
        ///// <summary>
        ///// 考试ID
        ///// </summary>
        //public long ExamId { get; set; }
        /// <summary>
        /// 作答ID
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 作答明细Id
        /// </summary>
        public long GradeDetailId { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }

        /// <summary>
        /// 主ID（对应数据维度id，场景实训对应场景实训中的公司caseid，课程对应courseid） 
        /// </summary>
        public long MasterId { get; set; }

        /// <summary>
        /// 摘要信息
        /// </summary>
        public string SummaryInfo { get; set; }

        /// <summary>
        /// 科目Id
        /// </summary>
        public long SubjectId { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        [Column(DbType = "decimal(12,2)")]
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        [Column(DbType = "decimal(12,2)")]
        public decimal CreditorAmount { get; set; }

        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertificateDate { get; set; }
    }
}
