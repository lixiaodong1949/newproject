﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 点赞
    /// </summary>
    [Table(Name = "yssx_scene_training_fabulous")]
    public class SceneTrainingFabulous : BizBaseEntity<long>
    {
        /// <summary>
        /// 租户Id
        /// </summary>
        public long TenantId { get; set; }

    }
}
