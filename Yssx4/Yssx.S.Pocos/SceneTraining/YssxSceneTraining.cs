﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.SceneTraining
{
    /// <summary>
    /// 场景实训
    /// </summary>
    [Table(Name = "yssx_scene_training")]
    public class YssxSceneTraining: BizBaseEntity<long>
    {
        /// <summary>
        /// 实训名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选择公司Id
        /// </summary>
        public long? SelectCompanyId { get; set; }
        /// <summary>
        /// 业务场景
        /// </summary>
        public long? YwcjId { get; set; }
        /// <summary>
        /// 排序序号
        /// </summary>
        public long? Pxxh { get; set; }
        /// <summary>
        /// 业务往来
        /// </summary>
        [Column(DbType = "text")]
        public string Ywwl { get; set; }
        /// <summary>
        /// 流程信息
        /// </summary>
        //public long LcxxId { get; set; }
        /// <summary>
        /// 作者寄语
        /// </summary>
        [Column(DbType = "text")]
        public string Zzjy { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Describe { get; set; }
        /// <summary>
        /// 审核状态(0,待审核，1，审核成功，2，审核拒绝)
        /// </summary>
        public int AuditStatus { get; set; } = 0;

        /// <summary>
        /// 审核说明
        /// </summary>
        public string AuditStatusExplain { get; set; }

        /// <summary>
        /// 是否上架(0,待上架，1，上架待审，2，上架)
        /// </summary>
        public int Uppershelf { get; set; } = 0;

        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; }
        /// <summary>
        /// 阅读数量
        /// </summary>
        public int Read { get; set; }
        /// <summary>
        /// 点赞
        /// </summary>
        public int Fabulous { get; set; }
        /// <summary>
        /// 使用次数
        /// </summary>
        public int UseNumber { get; set; }
        /// <summary>
        /// 来源(1,云自营,2,其他)
        /// </summary>
        public int? LyId { get; set; }
        /// <summary>
        /// 通关次数
        /// </summary>
        public int Tgcs { get; set; }
    }
}
