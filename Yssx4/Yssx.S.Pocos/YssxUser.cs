﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    [Table(Name = "yssx_user")]
    public class YssxUser : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 邮箱
        /// </summary>
        [Column(Name = "Email", DbType = "varchar(255)", IsNullable = true)]
        public string Email { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [Column(Name = "MobilePhone", DbType = "varchar(255)", IsNullable = true)]
        public string MobilePhone { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [Column(Name = "NikeName", DbType = "varchar(255)", IsNullable = true)]
        public string NikeName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Column(Name = "Password", DbType = "varchar(255)", IsNullable = true)]
        public string Password { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        [Column(Name = "Photo", DbType = "varchar(255)", IsNullable = true)]
        public string Photo { get; set; }

        /// <summary>
        /// QQ
        /// </summary>
        [Column(Name = "QQ", DbType = "varchar(255)", IsNullable = true)]
        public string QQ { get; set; }

        /// <summary>
        /// 真实姓名
        /// </summary>
        [Column(Name = "RealName", DbType = "varchar(255)", IsNullable = true)]
        public string RealName { get; set; }

        /// <summary>
        /// 用户状态 0禁用1启用2激活码禁用
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 用户账号
        /// </summary>
        [Column(Name = "UserName", DbType = "varchar(255)", IsNullable = true)]
        public string UserName { get; set; }

        /// <summary>
        /// 用户类型0管理员,1学生,2教师,3教务,4专家,5开发,6专业组,7普通用户
        /// </summary> 
        public int UserType { get; set; }

        /// <summary>
        /// 微信
        /// </summary>
        [Column(Name = "WeChat", DbType = "varchar(255)", IsNullable = true)]
        public string WeChat { get; set; }

        /// <summary>
        /// 微信平台唯一身份ID
        /// </summary>
        [Column(Name = "Unionid", DbType = "varchar(255)", IsNullable = true)]
        public string Unionid { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        [Column(Name = "IdNumber", DbType = "varchar(20)", IsNullable = true)]
        public string IdNumber { get; set; }

        /// <summary>
        /// 客户端id
        /// </summary>
        public Guid ClientId { get; set; }

        /// <summary>
        /// 注册类型(1,用户激活，2，后台添加 3、老师手动添加)
        /// </summary>
        public int RegistrationType { get; set; }

        /// <summary>
        /// 激活时间
        /// </summary>     
        public DateTime? ActivationTime { get; set; }

        /// <summary>
        /// 用户注册来源那个系统
        /// </summary>
        [Column(Name = "RegisterSysId", MapType = typeof(int), DbType = "int(11)")]
        public RegisterSourceEnum RegisterSource { get; set; } = RegisterSourceEnum.Yssx;

        /// <summary>
        /// 身份证号码（加密）
        /// </summary>
        [Column(Name = "IdCard", DbType = "LongText", IsNullable = true)]
        public string IdCard { get; set; }

        /// <summary>
        /// 身份证 - 正面照片
        /// </summary>
        [Column(Name = "IdCardFrontPhoto", DbType = "varchar(255)", IsNullable = true)]
        public string IdCardFrontPhoto { get; set; }

        /// <summary>
        /// 身份证 - 反面照片
        /// </summary>
        [Column(Name = "IdCardReversePhoto", DbType = "varchar(255)", IsNullable = true)]
        public string IdCardReversePhoto { get; set; }

        /// <summary>
        /// 证件照
        /// </summary>
        [Column(Name = "IdPhoto", DbType = "varchar(255)", IsNullable = true)]
        public string IdPhoto { get; set; }

        /// <summary>
        /// 证件照有效期
        /// </summary>
        public DateTime? IdPhotoValidity { get; set; }

        /// <summary>
        /// 证件照长期有效（0 否 1是）
        /// </summary>
        public int IsLongTermEffective { get; set; }


    }
}
