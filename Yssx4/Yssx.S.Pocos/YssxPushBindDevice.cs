﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    [Table(Name = "yssx_push_device_info")]
    public class YssxPushDeviceInfo : TenantBaseEntity<long>
    {
        public long UserId { get; set; }

        public string DeviceId { get; set; }

        public string PushCid { get; set; }
        /// <summary>
        /// 是否启用：默认true启用
        /// </summary>
        public bool Enable { get; set; } = true;
    }
}
