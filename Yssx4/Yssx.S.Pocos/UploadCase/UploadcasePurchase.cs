﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 上传案例-购买信息
    /// </summary>
    [Table(Name = "yssx_uploadcase_purchase")]
    public class UploadcasePurchase : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 购买类型 0赠送1购买
        /// </summary>
        public int PurchaseType { get; set; }

        /// <summary>
        /// 状态（0.未支付 1.已取消 2.已付款）
        /// </summary>
        public int Status { get; set; }

    }

    /// <summary>
    /// 上传案例-购买信息-输入
    /// </summary>
    [Table(Name = "yssx_uploadcase_purchase_input")]
    public class UploadcasePurchaseInput : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 购买顾客
        /// </summary>
        public string BuyingCustomers { get; set; }

        /// <summary>
        /// 角色
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// 所属学校
        /// </summary>
        public string School { get; set; }

    }
}
