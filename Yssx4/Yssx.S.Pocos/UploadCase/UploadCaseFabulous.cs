﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 点赞
    /// </summary>
    [Table(Name = "yssx_uploadcase_fabulous")]
    public class UploadCaseFabulous : BizBaseEntity<long>
    {
        /// <summary>
        /// 租户Id
        /// </summary>
        public long TenantId { get; set; }

    }
}
