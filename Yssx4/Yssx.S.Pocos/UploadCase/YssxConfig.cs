﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 配置
    /// </summary>
    [Table(Name = "yssx_config")]
    public class YssxConfig : BizBaseEntity<long>
    {
        /// <summary>
        /// key
        /// </summary>
        [Column(Name = "ConfigName", DbType = "varchar(123)", IsNullable = true)]
        public string ConfigName { get; set; }

        /// <summary>
        /// value
        /// </summary>
        [Column(Name = "ConfigValue", DbType = "varchar(999)", IsNullable = true)]
        public string ConfigValue { get; set; }

    }
}
