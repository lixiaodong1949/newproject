﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 上传案例
    /// </summary>
    [Table(Name = "yssx_uploadcase")]
    public class YssxUploadCase : BizBaseEntity<long>
    {
        /// <summary>
        /// 版权 true阅读 false没阅读
        /// </summary>
        public bool Copyright { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Cover { get; set; }

        /// <summary>
        /// 摘要
        /// </summary>
        [Column(Name = "Summary", DbType = "longtext", IsNullable = true)]
        public string Summary { get; set; }

        /// <summary>
        /// 价值点
        /// </summary>
        [Column(Name = "ValuePoint", DbType = "longtext", IsNullable = true)]
        public string ValuePoint { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string CertificateNumber { get; set; }

        /// <summary>
        /// 证书图片 格式：http://xxxxxxxxxx.png
        /// </summary>
        public string Certificate { get; set; }

        /// <summary>
        /// 点赞
        /// </summary>
        public int Fabulous { get; set; }

        /// <summary>
        /// 拒绝时间
        /// </summary>
        public DateTime? RejectionTime { get; set; }

        /// <summary>
        /// 上架时间
        /// </summary>
        public DateTime? ShelfTime { get; set; }

        /// <summary>
        /// 案例等级 1A 2B 3C 4D 5E 6F
        /// </summary>
        //[Column(Name = "CaseGrade", DbType = "int", IsNullable = true)]
        //public CaseGrade? CaseGrade { get; set; }
        public int? CaseGrade { get; set; }


        /// <summary>
        /// 上传者
        /// </summary>
        public string Uploads { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Column(Name = "Remarks", DbType = "longtext", IsNullable = true)]
        public string Remarks { get; set; }

        /// <summary>
        /// 状态 保存后默认0待上架 1上架待审 2已上架 3已拒绝  4全部(不传也全部)
        /// </summary>
        //[Column(Name = "CaseStatus", DbType = "int", IsNullable = true)]
        //public CaseStatus? CaseStatus { get; set; }
        public int? CaseStatus { get; set; }

        /// <summary>
        /// 提交时间
        /// </summary>
        public DateTime? SubmissionTime { get; set; }

        /// <summary>
        /// 案例标签 {"list":[{"name":"测试8888","id":982580666236938}]}
        /// </summary>
        [Column(Name = "CaseLabel", DbType = "text", IsNullable = true)]
        public string CaseLabel { get; set; }

        /// <summary>
        /// 阅读数量
        /// </summary>
        public int Read { get; set; }

        /// <summary>
        /// 案例类型 0会计核算 1财务管理 2审计 3税务 4全部(不传也全部)
        /// </summary>
        //[Column(Name = "Classification", DbType = "int", IsNullable = true)]
        //public Classification? Classification { get; set; }
        public int? Classification { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [Column(Name = "Title", DbType = "text", IsNullable = true)]
        public string Title { get; set; }
        /// <summary>
        /// 案例概述
        /// </summary>
        [Column(Name = "CaseSummary", DbType = "longtext", IsNullable = true)]
        public string CaseSummary { get; set; }
        /// <summary>
        /// 提出方案
        /// </summary>
        [Column(Name = "ProposedScheme", DbType = "longtext", IsNullable = true)]
        public string ProposedScheme { get; set; }
        /// <summary>
        /// 执行方案
        /// </summary>
        [Column(Name = "Implementation", DbType = "longtext", IsNullable = true)]
        public string Implementation { get; set; }
        /// <summary>
        /// 执行结果
        /// </summary>
        [Column(Name = "ExecutionResult", DbType = "longtext", IsNullable = true)]
        public string ExecutionResult { get; set; }
        /// <summary>
        /// 决策依据
        /// </summary>
        [Column(Name = "DecisionMakingBasis", DbType = "longtext", IsNullable = true)]
        public string DecisionMakingBasis { get; set; }
        /// <summary>
        /// 案例概述图片 {"list":[{"name":"demo1.jpg","url":"http://cdn-ccc32.jpg!w1200"}]}
        /// </summary>
        [Column(Name = "CaseSummaryImg", DbType = "longtext", IsNullable = true)]
        public string CaseSummaryImg { get; set; }
        /// <summary>
        /// 提出方案图片  
        /// </summary>
        [Column(Name = "ProposedSchemeImg", DbType = "longtext", IsNullable = true)]
        public string ProposedSchemeImg { get; set; }
        /// <summary>
        /// 执行方案图片  
        /// </summary>
        [Column(Name = "ImplementationImg", DbType = "longtext", IsNullable = true)]
        public string ImplementationImg { get; set; }
        /// <summary>
        /// 执行结果图片  
        /// </summary>
        [Column(Name = "ExecutionResultImg", DbType = "longtext", IsNullable = true)]
        public string ExecutionResultImg { get; set; }
        /// <summary>
        /// 决策依据图片  
        /// </summary>
        [Column(Name = "DecisionMakingBasisImg", DbType = "longtext", IsNullable = true)]
        public string DecisionMakingBasisImg { get; set; }
        /// <summary>
        /// 资料上传   {"list":[{"name":"22.xlsx","url":"http://cdnb2e5.xlsx"}]}
        /// </summary>
        [Column(Name = "DataUpload", DbType = "longtext", IsNullable = true)]
        public string DataUpload { get; set; }

        /// <summary>
        /// 审核状态(0,待审核，1，审核成功，2，审核拒绝)
        /// </summary>
        public int? AuditStatus { get; set; }

        /// <summary>
        /// 审核说明
        /// </summary>
        [Column(Name = "AuditStatusExplain", DbType = "longtext", IsNullable = true)]
        public string AuditStatusExplain { get; set; }

        /// <summary>
        /// 是否上架(0,待上架，1，上架待审，2，上架)
        /// </summary>
        public int? Uppershelf { get; set; }

        /// <summary>
        /// 上架说明
        /// </summary>
        [Column(Name = "UppershelfExplain", DbType = "longtext", IsNullable = true)]
        public string UppershelfExplain { get; set; }
    }
}
