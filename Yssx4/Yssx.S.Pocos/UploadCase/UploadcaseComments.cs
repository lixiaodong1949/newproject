﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 案例 批注
    /// </summary>
    [Table(Name = "yssx_uploadcase_comments")]
    public class UploadcaseComments : BizBaseEntity<long>
    {
        /// <summary>
        /// 追加Id
        /// 案例Id
        /// </summary>
        public long Parent { get; set; }

        /// <summary>
        /// Y坐标
        /// </summary>
        public int YCoordinate { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [Column(Name = "Comment", DbType = "longtext", IsNullable = true)]
        public string Comment { get; set; }

    }
}
