﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程知识点
    /// </summary>
    [Table(Name = "yssx_course_knowledge_point")]
    public class YssxCourseKnowledgePoint : BizBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public long KnowledgePointId { get; set; }

        /// <summary>
        /// 知识点名称
        /// </summary>
        public string KnowledgePointName { get; set; }
    }
}
