﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{

    /// <summary>
    /// 订单
    /// </summary>
    [Table(Name = "yssx_case_order")]
    public class CaseOrder : BizBaseEntity<long>
    {
        /// <summary>
        /// 所属学校Id
        /// </summary>
        public long SchoolId { get; set; }
        /// <summary>
        /// 所属学校名称
        /// </summary>
        public string SchoolName { get; set; }
        /// <summary>
        /// 状态 0禁用 1启用
        /// </summary>
        [Column(DbType = "int")]
        public Status Status { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 订单对应的年级
        /// </summary>
        public int Year { get; set; }


    }
}
