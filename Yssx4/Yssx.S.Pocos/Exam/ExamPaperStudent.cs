﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Exam
{
    /// <summary>
    /// 试卷学生表
    /// 教务正式考试--抽查学生表
    /// </summary>
    [Table(Name = "exam_paper_student")]
    public class ExamPaperStudent : TenantBaseEntity<long>
    {
        public ExamPaperStudent()
        {
        }

        public ExamPaperStudent(long id, long examId, long classId, long userId, long studentId)
        {
            Id = id;
            ExamId = examId;
            ClassId = classId;
            UserId = userId;
            StudentId = studentId;
        }

        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }
        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 学生ID
        /// </summary>
        public long StudentId { get; set; }
        /// <summary>
        /// 组号
        /// </summary>
        public int GroupNum { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

    }
}
