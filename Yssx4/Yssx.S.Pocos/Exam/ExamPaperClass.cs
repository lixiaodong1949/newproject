﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Exam
{
    /// <summary>
    /// 试卷考核班级表
    /// 老师组卷-考核班级使用
    /// </summary>
    [Table(Name = "exam_paper_class")]
    public class ExamPaperClass : TenantBaseEntity<long>
    {
        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 课程ID
        /// </summary>
        public long ClassId { get; set; }

    }
}
