﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Pocos
{
    [Table(Name = "yssx_product")]
    public class YssxProduct
    {
        public long Id { get; set; }

        public long CaseId { get; set; }

        public string CaseName { get; set; }

        public decimal OriginalPrice { get; set; }

        public decimal DiscountPrice { get; set; }

        public int OrderType { get; set; }

        public int Date { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
