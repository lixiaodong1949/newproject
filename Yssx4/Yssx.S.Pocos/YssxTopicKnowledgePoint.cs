﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 题目知识点
    /// </summary>
    [Table(Name = "yssx_topic_knowledge_point")]
    public class YssxTopicKnowledgePoint
    {
        /// <summary>
        /// 所属题目Id
        /// </summary>
        public long TopicId { get; set; }

        /// <summary>
        /// 题目父级Id
        /// </summary>
        public long ParentTopicId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public long KnowledgePointId { get; set; }
    }
}
