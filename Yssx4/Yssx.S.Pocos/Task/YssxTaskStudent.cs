﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{

    /// <summary>
    /// 课业任务 - 学生
    /// </summary>
    [Table(Name = "yssx_task_student")]
    public class YssxTaskStudent : BizBaseEntity<long>
    {
        /// <summary>
        /// 任务ID
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 班级ID
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 学生ID【废弃不用，用UserId】2020.02.11
        /// </summary>
        public long StudentId { get; set; }

    }
}
