﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课业任务
    /// </summary>
	[Table(Name = "yssx_task")]
    public class YssxTask : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime CloseTime { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        [Column(DbType = "int")]
        public TaskType TaskType { get; set; }

        /// <summary>
        /// 任务时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 备课课程ID
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 源课程ID
        /// </summary>
        public long OriginalCourseId { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Column(DbType = "int")]
        public LessonsTaskStatus Status { get; set; } = LessonsTaskStatus.Wait;

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Column(Name = "Content", DbType = "varchar(500)", IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 组Id
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// 租户Id
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 结束后是否可以查看试卷 false:不可以查看  true:可以查看 【默认为true】
        /// </summary>
        public bool IsCanCheckExam { get; set; } = true;
    }
}
