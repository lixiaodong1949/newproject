﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 实习证书
    /// </summary>
    [Table(Name = "yssx_internship_prove")]
    public class YssxInternshipProve : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 作答记录ID
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string SN { get; set; }

        /// <summary>
        /// 证书地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 通关时间
        /// </summary>
        public DateTime SubmitTime { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 企业ID
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 实训分类（0.会计实训 1.出纳实训 2.大数据 3.业财税）
        /// </summary>
        public int SxType { get; set; }

        /// <summary>
        /// 实训分类名称
        /// </summary>
        public string SxTypeName { get; set; }

        /// <summary>
        /// 所属行业
        /// </summary>
        public long Industry { get; set; }

        /// <summary>
        /// 所属行业名称
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamPaperScore { get; set; }

        /// <summary>
        /// 级别（0.合格[60-69%] 1.良好[70-79%] 2.优秀[>=80%]）
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// 级别名称
        /// </summary>
        public string LevelName { get; set; }

    }
}
