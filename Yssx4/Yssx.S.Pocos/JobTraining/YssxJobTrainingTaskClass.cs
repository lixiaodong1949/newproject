﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 岗位实训任务班级关系表
    /// </summary>
    [Table(Name = "yssx_jobtraining_task_class")]
    public class YssxJobTrainingTaskClass : BizBaseEntity<long>
    {
        /// <summary>
        /// 岗位实训任务Id
        /// </summary>
        public long JobTrainingTaskId { get; set; }

        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 实训班级Id
        /// </summary>
        public long ClassId { get; set; }
        
        /// <summary>
        /// 实训班级名称
        /// </summary>
        public string ClassName { get; set; }
    }
}
