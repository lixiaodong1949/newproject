﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 信息发布说明
    /// </summary>
    [Table(Name = "yssx_information")]
    public class YssxInformation
    {
        public long Id { get; set; }

        public int MassgeType { get; set; }

        public string TitleImg { get; set; }

        public string TagJson { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public DateTime PublishDate { get; set; }

        [Column(Name = "Content", DbType = "text", IsNullable = true)]
        public string Content { get; set; }

        public int IsDelete { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
