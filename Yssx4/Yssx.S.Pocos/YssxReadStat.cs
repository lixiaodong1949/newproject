﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Pocos
{
    [Table(Name = "yssx_read_stat")]
    public class YssxReadStat
    {
        public long Id { get; set; }

        public long UserType { get; set; }

        public string Ip { get; set; }

        public DateTime CreateTime { get; set; }

    }

    [Table(Name = "yssx_remind")]
    public class YssxRemind11
    {
        public long Id { get; set; }
        public string Moblie { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
