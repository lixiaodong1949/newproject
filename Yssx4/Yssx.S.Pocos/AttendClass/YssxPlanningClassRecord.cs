﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 备课上课记录表
    /// </summary>
    [Table(Name = "yssx_planning_attendclass_record")]
    public class YssxPlanningAttendClassRecord : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 上课课程Id
        /// </summary> 
        public long CourseId { get; set; }

        /// <summary>
        /// 上课课程名称
        /// </summary> 
        public string CourseName { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long LessonPlanId { get; set; }

        /// <summary>
        /// 课程备课名称
        /// </summary> 
        public string LessonPlanName { get; set; }

        /// <summary>
        /// 学期主键
        /// </summary>
        [Column(Name = "SemesterId", IsNullable = true)]
        public Nullable<long> SemesterId { get; set; }

        /// <summary>
        /// 学期名称
        /// </summary>
        public string SemesterName { get; set; }

        /// <summary>
        /// 教师Id
        /// </summary> 
        public long TeacherUserId { get; set; }

    }
}
