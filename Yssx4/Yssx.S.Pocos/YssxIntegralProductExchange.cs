using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 积分商品兑换表
    /// </summary>
    [Table(Name = "yssx_integral_product_exchange")]
    public class YssxIntegralProductExchange : BizBaseEntity<long>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        [Column(Name = "UserId", DbType = "bigint(20)")]
        public long UserId { get; set; }

        /// <summary>
        /// 积分商品Id
        /// </summary>
        [Column(Name = "IntegralProductId", DbType = "bigint(20)")]
        public long IntegralProductId { get; set; }

        /// <summary>
        /// 积分商品名称
        /// </summary>
        [Column(Name = "IntegralProductName", DbType = "varchar(1000)", IsNullable = true)]
        public string IntegralProductName { get; set; }
        /// <summary>
        /// 积分商品图片
        /// </summary>
        [Column(Name = "IntegralProductLogo", DbType = "varchar(1000)", IsNullable = true)]
        public string IntegralProductLogo { get; set; }

        /// <summary>
        /// 兑换积分
        /// </summary>
        [Column(Name = "IntegralNumber", DbType = "int(11)", IsNullable = true)]
        public int IntegralNumber { get; set; }

        /// <summary>
        /// 用户收货地址
        /// </summary>
        [Column(Name = "ShippingAddress", DbType = "varchar(1000)", IsNullable = true)]
        public string ShippingAddress { get; set; }

        /// <summary>
        /// 用户收货人姓名
        /// </summary>
        [Column(Name = "ShippingName", DbType = "varchar(50)", IsNullable = true)]
        public string ShippingName { get; set; }

        /// <summary>
        /// 用户收货人联系方式
        /// </summary>
        [Column(Name = "UserContact", DbType = "varchar(50)", IsNullable = true)]
        public string UserContact { get; set; }

        /// <summary>
        /// 发货状态：0未发货，1已发货 2已收货
        /// </summary>
        [Column(Name = "DeliveryStatus", DbType = "varchar(50)", IsNullable = true)]
        public int DeliveryStatus { get; set; } = 0;
        /// <summary>
        /// 发货时间
        /// </summary>
        [Column(Name = "DeliveryDate", IsNullable = true)]
        public DateTime DeliveryDate { get; set; }

        /// <summary>
        /// 订单号
        /// </summary>
        [Column(Name = "OrderCode", IsNullable = true)]
        public string OrderCode { get; set; }

        /// <summary>
        /// 积分商品来源
        /// </summary>
        [Column(Name = "ProductSource", DbType = "varchar(50)", IsNullable = true)]
        public string ProductSource { get; set; }

        /// <summary>
        /// 积分商品链接
        /// </summary>
        [Column(Name = "ProductLink", DbType = "varchar(500)", IsNullable = true)]
        public string ProductLink { get; set; }

        /// <summary>
        /// 商品数量
        /// </summary>
        public int ProductNumber { get; set; }
        /// <summary>
        /// 商品类型: 1：实物 2虚拟物品
        /// </summary>
        public int ProductType { get; set; } = 1;


    }
}