﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 公共题目表
    /// </summary>
    [Table(Name = "yssx_topic_public")]
    public class YssxTopicPublic : BizBaseEntity<long>
    {
        /// <summary>
        /// 题目标题
        /// </summary>
        [Column(Name = "Title", DbType = "varchar(255)", IsNullable = true)]
        public string Title { get; set; }
        /// <summary>
        /// 题干内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        [Column(DbType = "int")]
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 是否结账题（0 否 1是）
        /// </summary>
        public int IsSettlement { get; set; }
        /// <summary>
        /// 是否压缩（0 否 1是）
        /// </summary>
        public int IsGzip { get; set; }

        /// <summary>
        /// 题目内容类型(1:纯文字,2:表格,3:图片)
        /// </summary>
        [Column(DbType = "int")]
        public QuestionContentType QuestionContentType { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string Keywords { get; set; }
        /// <summary>
        /// 题目知识点id（多个id用,隔开）
        /// </summary>
        [Column(Name = "KnowledgePointIds", DbType = "varchar(500)", IsNullable = true)]
        public string KnowledgePointIds { get; set; }

        /// <summary>
        /// 模板数据源内容(不含答案)
        /// </summary>
        [Column(Name = "TopicContent", DbType = "LongText", IsNullable = true)]
        public string TopicContent { get; set; }
        /// <summary>
        /// 模板数据源内容(含答案)
        /// </summary>
        [Column(Name = "FullContent", DbType = "LongText", IsNullable = true)]
        public string FullContent { get; set; }

        /// <summary>
        /// 答案 内容（选择题答案a,b,c等，判断题true/false，表格json等）
        /// </summary>
        [Column(Name = "AnswerValue", DbType = "LongText", IsNullable = true)]
        public string AnswerValue { get; set; }
        /// <summary>
        /// 答案提示
        /// </summary>
        [Column(Name = "Hint", DbType = "Text", IsNullable = true)]
        public string Hint { get; set; }

        /// <summary>
        /// 父节点ID-用于多题型(一个题目 多个作答区)
        /// </summary>
        [Column(Name = "ParentId", DbType = "bigint(20)", IsNullable = true)]
        public long ParentId { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10,4)")]
        public decimal Score { get; set; }
        /// <summary>
        /// 非完整性得分
        /// </summary>
        [Column(Name = "PartialScore", DbType = "decimal(10,4)", IsNullable = true)]
        public decimal PartialScore { get; set; } = 0;
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科）
        /// </summary>
        public int Education { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column(DbType = "int")]
        public Status Status { get; set; } = Status.Enable;
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 是否场景实训题目（0.否 1.是）
        /// </summary>
        public int IsSceneTraining { get; set; } = CommonConstants.IsNo;

        /// <summary>
        /// 题目附件文件id（多个id用,隔开）
        /// </summary>
        [Column(Name = "TopicFileIds", DbType = "varchar(500)", IsNullable = true)]
        public string TopicFileIds { get; set; }
        /// <summary>
        /// 表格题-计算方式（按行计算、按列计算、按单元格平均计算、自由计算）
        /// </summary>
        [Column(DbType = "int")]
        public CalculationType CalculationType { get; set; } = CalculationType.None;
        /// <summary>
        /// 表格题-是否开启复制Excel数据（0 否 1是）
        /// </summary>
        public int IsCopy { get; set; } = CommonConstants.IsNoCopy;
        /// <summary>
        /// 表格题-是否开启复制大数据（0 否 1是）
        /// </summary>
        public int IsCopyBigData { get; set; } = CommonConstants.IsNoCopy;
        /// <summary>
        /// 表格题-是否无序（0 否 1是）
        /// </summary>
        public int IsDisorder { get; set; } = CommonConstants.IsNoDisorder;

        /// <summary>
        /// 审核状态（0 未审核 1已审核）
        /// </summary>
        public int IsApproval { get; set; } = CommonConstants.IsNoApproval;
        /// <summary>
        /// 审核备注
        /// </summary>
        public string ApprovalComments { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }
        
        /// <summary>
        /// 数据新增方式 0:自建数据 1:拷贝自有数据(深度拷贝) 2:引用平台数据(通过购买行为、深度拷贝)
        /// </summary>
        public int DataCreateWay { get; set; }

        /// <summary>
        /// 原始数据Id
        /// </summary>
        public long OriginalDataId { get; set; }

        /// <summary>
        /// 题目知识点id（多个id用,隔开）
        /// </summary>
        public string CenterKnowledgePointIds { get; set; }
        /// <summary>
        /// 知识点名称集合
        /// </summary>
        public string CenterKnowledgePointNames { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>
        public int DifficultyLevel { get; set; }

    }
}
