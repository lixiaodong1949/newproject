﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程
    /// </summary>
    [Table(Name = "yssx_course_type")]
    public class YssxCourseType : BizBaseEntity<long>
    {
        public string Name { get; set; }

    }
}
