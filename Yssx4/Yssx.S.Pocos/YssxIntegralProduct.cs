using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 积分商品表
    /// </summary>
    [Table(Name = "yssx_integral_product")]
    public class YssxIntegralProduct : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)")]
        public string Name { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Logo", DbType = "varchar(1000)", IsNullable = true)]
        public string Logo { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        [Column(Name = "Price", DbType = "decimal(10, 2)", IsNullable = true)]
        public decimal Price { get; set; }

        /// <summary>
        /// 类型：1：实物 2虚拟物品
        /// </summary>
        [Column(Name = "Type", DbType = "int(11)", IsNullable = true)]
        public int Type { get; set; }

        /// <summary>
        /// 详情内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 兑换积分
        /// </summary>
        [Column(Name = "Integral", DbType = "int(11)", IsNullable = true)]
        public int Integral { get; set; }

        /// <summary>
        /// 库存
        /// </summary>
        [Column(Name = "Inventory", DbType = "int(11)", IsNullable = true)]
        public int Inventory { get; set; }

        /// <summary>
        /// 是否已经上架:true 上架  false 下架
        /// </summary>
        public bool IsActive { get; set; } = false;

        /// <summary>
        /// 上架时间
        /// </summary>
        [Column(Name = "ActiveDateTime", IsNullable = true)]
        public DateTime ActiveDateTime { get; set; }

        /// <summary>
        /// 兑换数量
        /// </summary>
        [Column(Name = "ExchangeNum", DbType = "int(11)", IsNullable = true)]
        public int ExchangeNum { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        [Column(Name = "Sort", DbType = "int(11)")]
        public int Sort { get; set; } = 0;

    }
}