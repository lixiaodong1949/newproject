﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程相关作答信息主表
    /// </summary>
    [Table(Name = "exam_course_user_grade")]
    public class ExamCourseUserGrade : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 学生ID
        /// </summary>
        public long StudentId { get; set; }
        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }
        #region paper中的值
        /// <summary>
        /// 课程id
        /// </summary>
        public long CourseId { get; set; }
        /// <summary>
        /// 源课程ID
        /// </summary>
        public long OriginalCourseId { get; set; }
        /// <summary>
        /// 章节id
        /// </summary>
        public long SectionId { get; set; }
        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }
        #endregion

        ///// <summary>
        ///// 分组id
        ///// </summary>
        //public long GroupId { get; set; }
        /// <summary>
        /// 得分
        /// </summary>
        [Column(DbType = "decimal(10,4)")]
        public decimal Score { get; set; }
        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamPaperScore { get; set; }

        ///// <summary>
        ///// 作答过的题目ID
        ///// </summary>
        //[Column(Name = "ExamPaperQuestionIds", DbType = "text")]
        //public string ExamPaperQuestionIds { get; set; }

        /// <summary>
        /// 正确题数
        /// </summary>
        public int CorrectCount { get; set; }
        /// <summary>
        /// 错误题数
        /// </summary>
        public int ErrorCount { get; set; }
        /// <summary>
        /// 部分对题数
        /// </summary>
        public int PartRightCount { get; set; }
        /// <summary>
        /// 未作答题数
        /// </summary>
        public int BlankCount { get; set; }
        /// <summary>
        /// 最后一次查看的题目ID
        /// </summary>
        public long LastViewQuestionId { get; set; }

        /// <summary>
        /// 是否手动提交考卷
        /// </summary>
        public bool IsManualSubmit { get; set; } = true;
        /// <summary>
        /// 交卷时间
        /// </summary>
        public DateTime? SubmitTime { get; set; }

        /// <summary>
        /// 作答总用时
        /// </summary>
        public double UsedSeconds { get; set; }

        /// <summary>
        /// 剩余时间：秒
        /// </summary>
        public double LeftSeconds { get; set; }

        ///// <summary>
        ///// 是否已结账
        ///// </summary>
        //public bool IsSettled { get; set; }

        /// <summary>
        /// 考试状态
        /// </summary>
        [Column(DbType = "int")]
        public StudentExamStatus Status { get; set; }

        /// <summary>
        /// 考试开始及暂停时保存时间点 
        /// </summary>
        public DateTime RecordTime { get; set; }
        /// <summary>
        /// 是否预先生成（作业、任务、考试等预先生成grade的做题形式，根据此属性判断是否预先生成，当用户进入答题时，修改此属性为false，并记录时间）
        /// </summary>
        public bool IsFirstTime { get; set; } = true;
    }
}
