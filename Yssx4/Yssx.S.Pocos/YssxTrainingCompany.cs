﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.BackManage
{
    public class YssxTrainingCompany:TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 行业
        /// </summary>
        public Industry Industry { get; set; }
        /// <summary>
        /// 纳税人方式
        /// </summary>
        public TaxpayerMethod TaxpayerMethod { get; set; }
        /// <summary>
        /// 公司规模
        /// </summary>
        public CompanySize CompanySize { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string Introduction { get; set; }
        /// <summary>
        /// 上传公司图
        /// </summary>
        public string CompanyFigure { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public int CompanyNamePicture { get; set; }
        /// <summary>
        /// 印章名称
        /// </summary>
        public int SealNamePicture { get; set; }
    }
}
