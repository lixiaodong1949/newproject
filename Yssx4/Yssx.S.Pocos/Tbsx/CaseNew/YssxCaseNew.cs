﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 案例表
    /// </summary>
    [Table(Name = "yssx_case_new")]
    public class YssxCaseNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 公司介绍
        /// </summary>
        [Column(Name = "BriefName", DbType = "LongText", IsNullable = true)]
        public string BriefName { get; set; }
        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }
        /// <summary>
        /// 自定义数据
        /// </summary>
        [Column(Name = "CustomData", DbType = "LongText", IsNullable = true)]
        public string CustomData { get; set; }
        /// <summary>
        /// 序号（标号）
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// 产品流程是否启用
        /// </summary>
        public bool EnabledProductFlow { get; set; }
        /// <summary>
        /// 产品流程
        /// </summary>
        [Column(Name = "ProductFlow", DbType = "LongText", IsNullable = true)]
        public string ProductFlow { get; set; }
        /// <summary>
        /// 会计主管
        /// </summary>
        [Column(Name = "AccountantOfficer", DbType = "varchar(200)", IsNullable = true)]
        public string AccountantOfficer { get; set; }

        /// <summary>
        /// 出纳
        /// </summary>
        [Column(Name = "Cashier", DbType = "varchar(200)", IsNullable = true)]
        public string Cashier { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        [Column(Name = "Auditor", DbType = "varchar(200)", IsNullable = true)]
        public string Auditor { get; set; }

        /// <summary>
        /// 业务会计
        /// </summary>
        [Column(Name = "BusinessAccountant", DbType = "varchar(200)", IsNullable = true)]
        public string BusinessAccountant { get; set; }

        /// <summary>
        /// 税务会计
        /// </summary>
        [Column(Name = "TaxAccountant", DbType = "varchar(200)", IsNullable = true)]
        public string TaxAccountant { get; set; }

        /// <summary>
        /// 成本会计
        /// </summary>
        [Column(Name = "CostAccountant", DbType = "varchar(200)", IsNullable = true)]
        public string CostAccountant { get; set; }
        public int TotalTime { get; set; } = 0;
        /// <summary>
        /// 公司logo
        /// </summary>
        public string LogoUrl { get; set; }
        /// <summary>
        /// 纳税人识别号
        /// </summary>
        [Column(Name = "TaxpayerNumber", DbType = "varchar(200)", IsNullable = true)]
        public string TaxpayerNumber { get; set; }
    }
}
