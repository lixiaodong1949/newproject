﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 流程
    /// </summary>
    [Table(Name = "yssx_flow")]
    public class YssxFlow : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 实训类型Id
        /// </summary>
        public long DrillTypeId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "VARCHAR(255)")]
        public string Name { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column(Name = "Status", DbType = "int", IsNullable = true)]
        public Status Status { get; set; } = Status.Enable;
        /// <summary>
        /// 题目总数
        /// </summary>
        public int TopicTotalNum { get; set; }
        /// <summary>
        /// 题目总分
        /// </summary>
        public decimal TopicTotalSore { get; set; }
    }
}
