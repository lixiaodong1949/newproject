﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 同步类型
    /// </summary>
    [Table(Name = "yssx_drill_type")]
    public class YssxDrillType : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "VARCHAR(255)")]
        public string Name { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Column(Name = "Status", DbType = "int", IsNullable = true)]
        public Status Status { get; set; } = Status.Enable;
        /// <summary>
        /// 流程描述
        /// </summary>
        [Column(Name = "FlowDescription", DbType = "LONGTEXT")]
        public string FlowDescription { get; set; }
        /// <summary>
        /// 任务描述
        /// </summary>
        [Column(Name = "TaskDescription", DbType = "LONGTEXT")]
        public string TaskDescription { get; set; }

        /// <summary>
        /// 题目总分
        /// </summary>
        [Column(Name = "TopicTotalSore", DbType = "DECIMAL(10,2)", IsNullable = true)]
        public decimal TopicTotalSore { get; set; }
    }
}
