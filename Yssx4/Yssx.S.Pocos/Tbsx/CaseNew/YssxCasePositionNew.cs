﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 案例岗位关联
    /// </summary>
    [Table(Name = "yssx_case_position_new")]
    public class YssxCasePositionNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 岗位id
        /// </summary>
        public long PostionId { get; set; }
        /// <summary>
        /// 岗位id
        /// </summary>
        public string PostionName { get; set; }
        public int TimeMinutes { get; set; } = 0;
    }
}
