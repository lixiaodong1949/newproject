﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 部门管理
    /// </summary>
    [Table(Name = "yssx_department_new")]
    public class YssxDepartmentNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 排序
        /// </summary>
        [Column(Name = "Sort")]
        public int Sort { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        [Column(Name = "CaseId")]
        public long CaseId { get; set; }

        /// <summary>
        /// 部门名称
        /// </summary>
        [Column(Name = "DepartmentName")]
        public string DepartmentName { get; set; }
        /// <summary>
        /// 部门描述
        /// </summary>
        [Column(Name = "Description", DbType = "LongText", IsNullable = true)]
        public string Description { get; set; }
    }
}
