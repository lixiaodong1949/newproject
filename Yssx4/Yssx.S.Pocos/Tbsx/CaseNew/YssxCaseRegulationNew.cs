﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 案例规章制度
    /// </summary>
    [Table(Name = "yssx_case_regulation_new")]
    public class YssxCaseRegulationNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 制度名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(200)", IsNullable = true)]
        public string Name { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }
        /// <summary>
        /// 公司Id
        /// </summary>
        public long CaseId { get; set; }
    }
}
