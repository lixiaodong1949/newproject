﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 题目附件表
    /// </summary>
    [Table(Name = "yssx_topic_file_new")]
    public class YssxTopicFileNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>
        public long TopicId { get; set; }
        /// <summary>
        /// 实训类型 Id
        /// </summary>
        public long DrillTypeId { get; set; }
        /// <summary>
        /// 流程Id
        /// </summary>
        public long FlowId { get; set; }
    }
}
