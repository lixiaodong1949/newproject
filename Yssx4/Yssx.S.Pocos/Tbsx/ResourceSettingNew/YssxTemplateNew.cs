﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    [Table(Name = "yssx_template_new")]
    /// <summary>
    /// 凭证模板
    /// </summary>
    public class YssxTemplateNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 分类Id
        /// </summary>
        public long TemplateTypeId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 全部HTML内容
        /// </summary>
        [Column(Name = "FullHtml", DbType = "Text", IsNullable = true)]
        public string FullHtml { get; set; }
        /// <summary>
        /// 清除样式的HTML
        /// </summary>
        [Column(Name = "ClearStyleHtml", DbType = "Text", IsNullable = true)]
        public string ClearStyleHtml { get; set; }
        /// <summary>
        /// 答案数据json
        /// </summary>
        [Column(Name = "AnswerJson", DbType = "Text", IsNullable = true)]
        public string AnswerJson { get; set; }
    }
}
