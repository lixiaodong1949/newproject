﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 岗位管理 - 同步实训
    /// </summary>
    [Table(Name = "yssx_position_new")]
    public class YssxPositionNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(Name = "Describe", DbType = "text", IsNullable = true)]
        public string Describe { get; set; }
    }
}
