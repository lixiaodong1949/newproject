﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "exam_paper_group_student_new")]
    public class ExamPaperGroupStudentNew : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 分组Id
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// 考试id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 考试关联的案例id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 成绩主表id，如果为0，表示组团还未成功 
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 组员ID
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 角色Id
        /// </summary>
        public long PostionId { get; set; }

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 是否手动提交考卷
        /// </summary>
        public bool IsManualSubmit { get; set; } = true;

        /// <summary>
        /// 交卷时间
        /// </summary>
        public DateTime? SubmitTime { get; set; }

        /// <summary>
        /// 作答总用时
        /// </summary>
        public double UsedSeconds { get; set; }

        /// <summary>
        /// 剩余时间：秒
        /// </summary>
        public double LeftSeconds { get; set; }

        /// <summary>
        /// 交卷状态
        /// </summary>
        [Column(DbType = "int")]
        public StudentExamStatus StudentExamStatus { get; set; }

        /// <summary>
        /// 考试开始及暂停时保存时间点 
        /// </summary>
        public DateTime RecordTime { get; set; }

        /// <summary>
        /// 正确题数
        /// </summary>
        public int CorrectCount { get; set; }
        /// <summary>
        /// 错误题数
        /// </summary>
        public int ErrorCount { get; set; }
        /// <summary>
        /// 部分对题数
        /// </summary>
        public int PartRightCount { get; set; }
        /// <summary>
        /// 未作答题数
        /// </summary>
        public int BlankCount { get; set; }

        /// <summary>
        /// 最后一次查看的题目ID
        /// </summary>
        public long LastViewQuestionId { get; set; }


        /// <summary>
        /// 分组状态(0.已开始，等待成员加入；1.成员已满，等待房间创建人启动开始考试按钮（此时成员人有可能退出房间，创建人也能解散）；2.已完成，开始考试（成员不能再退出）；3.已结束 )
        /// </summary>
        [Column(DbType = "int")]
        public GroupStatus Status { get; set; }

        /// <summary>
        /// 是否已准备完毕 
        /// </summary>
        public bool IsReady { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        [Column(DbType = "decimal(10,4)")]
        public decimal Score { get; set; }
        /// <summary>
        /// 是否已进入考试(正式竞赛用)
        /// </summary>
        public bool IsStarted { get; set; } = false;
    }
}
