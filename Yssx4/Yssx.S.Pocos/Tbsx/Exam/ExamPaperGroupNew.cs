﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 试卷分组 - 同步实训
    /// </summary>
    [Table(Name = "exam_paper_group_new")]
    public class ExamPaperGroupNew : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 考试id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 房号
        /// </summary>
        public long GroupNo { get; set; }

        /// <summary>
        /// 考试关联的案例id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 成绩主表id，如果为0，表示组团还未成功 
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 分组状态(0.已开始，等待成员加入；1.成员已满，等待房间创建人启动开始考试按钮（此时成员人有可能退出房间，创建人也能解散）；2.已完成，开始考试（成员不能再退出）；3.已结束 )
        /// </summary>
        [Column(DbType = "int")]
        public GroupStatus Status { get; set; }

    }
}
