﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 试卷表 - 同步实训
    /// </summary>
    [Table(Name = "exam_paper_new")]
    public class ExamPaperNew : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 试卷命名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 试卷所属人Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 案例ID
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 考试类型
        /// </summary>
        [Column(DbType = "int")]
        public ExamType ExamType { get; set; }

        /// <summary>
        /// 赛事类型（0 个人赛 1团体赛）
        /// </summary>
        [Column(DbType = "int")]
        public CompetitionType CompetitionType { get; set; }

        /// <summary>
        /// 结束之前是否允许查看解析
        /// </summary>
        public bool CanShowAnswerBeforeEnd { get; set; }

        /// <summary>
        /// 学生分配方式
        /// </summary>
        [Column(DbType = "int")]
        public StudentDistribute DistributeMethod { get; set; }

        /// <summary>
        /// 机位数
        /// </summary>
        public int SeatCount { get; set; }

        /// <summary>
        /// 随机数量--实际的学生数量
        /// </summary>
        public int RandomCount { get; set; }

        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 合格分数
        /// </summary>
        public decimal PassScore { get; set; } = 60;

        /// <summary>
        /// 总题数
        /// </summary>
        public int TotalQuestionCount { get; set; }

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 考试开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }
        /// <summary>
        /// 考试结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime? ReleaseTime { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 是否已发布
        /// 发布后学生任务列表可见
        /// </summary>
        public bool IsRelease { get; set; } = false;


        /// <summary>
        /// 数据库对应字段
        /// </summary>
        [Column(DbType = "int")]
        public ExamStatus Status { get; set; }
        /// <summary>
        /// 状态 0未开始，1已开始，2已结束
        /// </summary>

        //校内赛任务id
        public long TaskId { get; set; }
        /// <summary>
        /// 场次id
        /// </summary>
        public long VenuesId { get; set; }
        /// <summary>
        /// 试卷来源类型 0:订单购买 1:赛前训练免费赠送 2:校内赛任务 3:省赛任务 4:岗位实训任务
        /// </summary>
        [Column(DbType = "int")]
        public ExamSourceType ExamSourceType { get; set; }
    }
}
