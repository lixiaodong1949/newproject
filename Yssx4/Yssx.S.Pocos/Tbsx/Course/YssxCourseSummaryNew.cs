﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程摘要信息
    /// </summary>
    [Table(Name = "yssx_course_summary_info_new")]
    public class YssxCourseSummaryNew : BizBaseEntity<long>
    {
        /// <summary>
        /// 摘要
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        public int Type { get; set; }
    }
}
