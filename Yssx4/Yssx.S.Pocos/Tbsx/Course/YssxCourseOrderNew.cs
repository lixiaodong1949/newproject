﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程订单表
    /// </summary>
    [Table(Name = ("yssx_course_order_new"))]
    public class YssxCourseOrderNew: TenantBaseEntity<long>
    {
        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNumber { get; set; }
        
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 下单人Id
        /// </summary>
        public long CustomerId { get; set; }

        /// <summary>
        /// 下单人名称
        /// </summary>
        public string CustomerByName { get; set; }

        /// <summary>
        /// 支付方式 0:支付宝 1:微信 2:网银
        /// </summary>
        public Nullable<int> PaymentMethod { get; set; }

        /// <summary>
        /// 购买时间
        /// </summary>
        public Nullable<DateTime> PaymentTime { get; set; }

        /// <summary>
        /// 订单金额
        /// </summary>
        public Nullable<decimal>  OrderMoney { get; set; }

        /// <summary>
        /// 优惠金额
        /// </summary>
        public Nullable<decimal> DiscountMoney { get; set; }

        /// <summary>
        /// 支付金额
        /// </summary>
        public Nullable<decimal> PaymentMoney { get; set; }

        /// <summary>
        /// 订单状态 0:系统创建(赠送课程订单)【Obsolete】 1:创建订单(未支付) 2:取消订单 3:删除取消订单 4:支付成功订单(已支付) 5:删除支付成功订单 6:删除失效订单
        /// </summary>
        public Nullable<int> OrderStatus { get; set; }

        /// <summary>
        /// 订单来源 0:系统赠送 1:客户创建(购买)
        /// </summary>
        public Nullable<int> OrderSource { get; set; }

        /// <summary>
        /// 有效期
        /// </summary>
        public Nullable<DateTime> EffectiveDate { get; set; }

    }
}
