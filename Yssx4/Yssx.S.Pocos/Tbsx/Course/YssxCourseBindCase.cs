﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程
    /// </summary>
    [Table(Name = "yssx_course_bind_case")]
    public class YssxCourseBindCase : TenantBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        [Column(Name = "CourseId")]
        public long CourseId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        [Column(Name = "CaseId")]
        public long CaseId { get; set; }
    }
}
