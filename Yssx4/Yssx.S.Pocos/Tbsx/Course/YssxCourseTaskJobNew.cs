﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程克隆任务
    /// </summary>
    [Table(Name = "yssx_course_job_new")]
    public class YssxCourseTaskJobNew
    {
        [Column(Name = "Id", IsPrimary = true)]
        public long Id { get; set; }

        /// <summary>
        /// 原课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 新课程Id
        /// </summary>
        public long NewCourseId { get; set; }

        public string CourseName { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 租户Id
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 0:未处理,1:已处理
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 0:拷贝课程,1:引用课程
        /// </summary>
        public int TaskType { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
    }
}
