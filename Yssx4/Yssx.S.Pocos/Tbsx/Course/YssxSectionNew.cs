﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程章节
    /// </summary>
    [Table(Name = "yssx_section_new")]
    public class YssxSectionNew : BizBaseEntity<long>
    {
        public string SectionName { get; set; }

        public string SectionTitle { get; set; }

        public long CourseId { get; set; }

        public int Sort { get; set; }

        public long ParentId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 0:章，1：节
        /// </summary>
        public int SectionType { get; set; }
    }

    /// <summary>
    /// 复制课程用的
    /// </summary>
    public class YssxSectionCloneNew : BizBaseEntity<long>
    {
        public long NewId { get; set; }
        public string SectionName { get; set; }

        public string SectionTitle { get; set; }

        public long CourseId { get; set; }

        public int Sort { get; set; }

        public long ParentId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 0:章，1：节
        /// </summary>
        public int SectionType { get; set; }
    }

    /// <summary>
    /// 教材
    /// </summary>
    [Table(Name = "yssx_section_textbook_new")]
    public class YssxSectionTextBookNew : BizBaseEntity<long>
    {
        public long SectionId { get; set; }

        public long CourseId { get; set; }

        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }

        public long KnowledgePointId { get; set; }

    }

    /// <summary>
    /// 课程习题
    /// </summary>
    [Table(Name = "yssx_section_topic_new")]
    public class YssxSectionTopicNew : BizBaseEntity<long>
    {
        public long SectionId { get; set; }


        public string QuestionName { get; set; }

        public long CourseId { get; set; }

        public long QuestionId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public long KnowledgePointId { get; set; }

        public int Sort { get; set; }
    }

    /// <summary>
    /// 课程案例
    /// </summary>
    [Table(Name = "yssx_section_case_new")]
    public class YssxSectionCaseNew : BizBaseEntity<long>
    {
        public long SectionId { get; set; }

        public long CourseId { get; set; }

        public string KnowledgePointId { get; set; }

        public long CaseId { get; set; }

        public int Sort { get; set; }
    }

    /// <summary>
    /// 课程课件
    /// </summary>
    [Table(Name = "yssx_section_files_new")]
    public class YssxSectionFilesNew : BizBaseEntity<long>
    {
        public long SectionId { get; set; }


        public long CourseId { get; set; }

        public string FileName { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 课件库Id
        /// </summary>
        public long CourseFileId { get; set; }

        public string File { get; set; }

        /// <summary>
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public int FilesType { get; set; }

        /// <summary>
        /// 1:课件 2:教案 3:视频 4:授课计划 5:素材(课程备课)
        /// </summary>
        public int SectionType { get; set; }

        public int Sort { get; set; }
    }

    /// <summary>
    /// 课程章节汇总概要
    /// </summary>
    [Table(Name = "yssx_section_summary_new")]
    public class YssxSectionSummaryNew
    {
        public long Id { get; set; }

        public long CourseId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SummaryType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //public string SummaryName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 操作更新时间
        /// </summary>
        public DateTime UpdateTime { get; set; }
        //public string JsonContent { get; set; }
    }

    /// <summary>
    /// 课程场景实训
    /// </summary>
    [Table(Name = "yssx_section_scenetraining_new")]
    public class YssxSectionSceneTrainingNew: BizBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程章节Id
        /// </summary>
        public long SectionId { get; set; }

        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long SceneTrainingId { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

    }

}
