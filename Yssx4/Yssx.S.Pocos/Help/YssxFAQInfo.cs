﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 常见问题
    /// </summary>
    [Table(Name = "yssx_faq_info")]
    public class YssxFAQInfo : BizBaseEntity<long>
    {
        /// <summary>
        /// 标题
        /// </summary>
        [Column(Name = "Title", DbType = "varchar(255)", IsNullable = true)]
        public string Title { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }

        /// <summary>
        /// 问题分类ID
        /// </summary>
        public long FaqClassId { get; set; }

        /// <summary>
        /// 角色分类（1.普通用户 2.教师 3.学生）
        /// </summary>
        public int RoleType { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadNumber { get; set; }

    }
}
