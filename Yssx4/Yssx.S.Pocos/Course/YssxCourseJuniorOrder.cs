﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 初级会计考证课程订单表
    /// </summary>
    [Table(Name = ("yssx_course_junior_order"))]
    public class YssxCourseJuniorOrder : BizBaseEntity<long>
    {
        /// <summary>
        /// 试卷所属人Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程名称
        /// </summary>
        [Column(Name = "CourseName", DbType = "varchar(255)", IsNullable = true)]
        public string CourseName { get; set; }

        /// <summary>
        /// 状态（0.未支付 1.已取消 2.已付款）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 订单来源 0:系统赠送 1:客户购买
        /// </summary>
        public int Source { get; set; }

        /// <summary>
        /// 有效期
        /// </summary>
        public DateTime EffectiveDate { get; set; }


    }
}
