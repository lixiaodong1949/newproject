﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程预习表
    /// </summary>    
    [Table(Name = "yssx_course_preview")]
    public class YssxCoursePreview : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 课程预习名称
        /// </summary>
        public string CoursePreviewName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Column(Name = "Remark", DbType = "LongText", IsNullable = true)]
        public string Remark { get; set; }
    }
}
