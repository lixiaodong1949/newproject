﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 学生不懂课程资源记录表
    /// </summary>
    [Table(Name = "yssx_student_doubt_coursefile_record")]
    public class YssxStudentDoubtCourseFileRecord : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 课程预习Id
        /// </summary>
        public long CoursePreviewId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 课件Id
        /// </summary>
        public long FileId { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件地址
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public FilesType FilesType { get; set; }

        /// <summary>
        /// 资源类型 0:课件 1:教案 2:视频 3:思政案例 4:个人素材
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public CoursePrepareResourceType ResourceType { get; set; }

        /// <summary>
        /// 是否附件  区分超链接
        /// </summary>
        public bool IsFile { get; set; }

        /// <summary>
        /// 疑问(不懂)描述
        /// </summary>
        public string QuestionDescribe { get; set; }
    }
}
