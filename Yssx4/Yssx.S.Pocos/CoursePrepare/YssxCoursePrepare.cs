﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程备课表
    /// </summary>
    [Table(Name = "yssx_course_prepare")]
    public class YssxCoursePrepare : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 备课名称
        /// </summary>
        public string CoursePrepareName { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }
    }
}
