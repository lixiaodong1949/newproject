﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程备课资料库
    /// </summary>
    [Table(Name = "yssx_course_prepare_files")]
    public class YssxCoursePrepareFiles : BizBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程备课Id
        /// </summary>
        public long CoursePrepareId { get; set; }

        /// <summary>
        /// 课件库Id
        /// </summary>
        public long CourseFileId { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件地址
        /// </summary>
        public string File { get; set; }

        /// <summary>
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public FilesType FilesType { get; set; }

        /// <summary>
        /// 资源类型 0:课件 1:教案 2:视频 3:思政案例 4:个人素材
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public CoursePrepareResourceType ResourceType { get; set; }

        /// <summary>
        /// 资源归属 0:课前预习 1:课中上课
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public CoursePrepareResourcesBelong ResourceBelong { get; set; }

        /// <summary>
        /// 是否附件  区分超链接 true:附件  false:超链接
        /// </summary>
        public bool IsFile { get; set; } = true;

        /// <summary>
        /// 知识点Ids
        /// </summary>
        public string KnowledgePointIds { get; set; }

        /// <summary>
        /// 知识点名称
        /// </summary>
        public string KnowledgePointNames { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Column(Name = "Remark", DbType = "LongText", IsNullable = true)]
        public string Remark { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
