﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 学生课堂测验案例任务作答信息表
    /// </summary>
	[Table(Name = "yssx_student_casetask_gradeinfo")]
    public class YssxStudentCaseTaskGradeInfo : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        [Column(DbType = "decimal(10,4)")]
        public decimal Score { get; set; }
        
        /// <summary>
        /// 作答状态
        /// </summary>
        [Column(DbType = "int")]
        public AnswerResultStatus AnswerStatus { get; set; }

        /// <summary>
        /// 是否已交卷 false:未交卷 true:已交卷
        /// </summary>
        public bool IsSubmitExam { get; set; }

        /// <summary>
        /// 简单题(案例分析题)作答详情
        /// </summary>
        [Column(DbType = "longtext")]
        public string ShortAnswerValue { get; set; }
    }
}
