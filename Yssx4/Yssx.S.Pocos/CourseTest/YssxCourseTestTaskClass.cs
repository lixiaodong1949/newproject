﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课堂测验任务班级关系表
    /// </summary>
    [Table(Name = "yssx_course_test_task_class")]
    public class YssxCourseTestTaskClass : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 课堂测验任务Id
        /// </summary>
        public long TestTaskId { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary>
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }
    }
}
