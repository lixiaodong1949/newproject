﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课堂测验题目表
    /// </summary>
    [Table(Name = "yssx_course_test_topic")]
    public class YssxCourseTestTopic : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 课程上课Id
        /// </summary>
        public long TeachCourseId { get; set; }

        /// <summary>
        /// 课堂测验任务Id
        /// </summary>
        public long TestTaskId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long TopicId { get; set; }

        /// <summary>
        /// 题目类型
        /// </summary>
        [Column(DbType = "int")]
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        [Column(Name = "Score", DbType = "decimal(10,4)")]
        public decimal Score { get; set; }

        /// <summary>
        /// 题目来源 0:课程 1:案例
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public CoursePrepareTopicSource TopicSource { get; set; }
    }
}
