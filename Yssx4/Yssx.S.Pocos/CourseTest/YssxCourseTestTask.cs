﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课堂测验任务表
    /// </summary>
	[Table(Name = "yssx_course_test_task")]
    public class YssxCourseTestTask : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 任务类型
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public TaskType TaskType { get; set; }

        /// <summary>
        /// 任务时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 课程上课Id
        /// </summary>
        public long TeachCourseId { get; set; }

        /// <summary>
        /// 状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public LessonsTaskStatus Status { get; set; }

        /// <summary>
        /// 任务来源 0:课程 1:案例
        /// </summary>
        [Column(DbType = "int", MapType = typeof(int))]
        public CourseTestTaskSource TaskSource { get; set; }

        /// <summary>
        /// 选中课程或案例Id
        /// </summary>
        public long CourseOrCaseId { get; set; }

        /// <summary>
        /// 0:实习 1:实训
        /// </summary>
        public Nullable<int> ModuleType { get; set; }

        /// <summary>
        /// 任务总分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 结束后是否可以查看试卷 false:不可以查看  true:可以查看 【默认为true】
        /// </summary>
        public bool IsCanCheckExam { get; set; } = true;
    }
}
