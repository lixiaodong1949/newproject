﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程
    /// </summary>
    [Table(Name = "yssx_course")]
    public class YssxCourse: TenantBaseEntity<long>
    {
        /// <summary>
        /// 课程名称
        /// </summary>
        [Column(Name = "CourseName", DbType = "varchar(255)", IsNullable = true)]
        public string CourseName { get; set; }

        /// <summary>
        /// 课程类型0:常规课 1:精品课
        /// </summary>
        public int CourseType { get; set; }

        /// <summary>
        /// 特点副标
        /// </summary>
        [Column(Name = "CourseTitle", DbType = "varchar(255)", IsNullable = true)]
        public string CourseTitle { get; set; }

        /// <summary>
        /// 课程类型
        /// </summary>
        public long CourseTypeId { get; set; }

        /// <summary>
        /// 知识点
        /// </summary>
        public long KnowledgePointId { get; set; }

        /// <summary>
        /// 学历标识
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        [Column(Name = "Author", DbType = "varchar(100)", IsNullable = true)]
        public string Author { get; set; }

        /// <summary>
        /// 出版社
        /// </summary>
        [Column(Name = "PublishingHouse", DbType = "varchar(100)", IsNullable = true)]
        public string PublishingHouse { get; set; }

        /// <summary>
        /// 出版时间
        /// </summary>
        public string PublishingDate { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// 审核状态(0,待审核，1，审核成功，2，审核拒绝)
        /// </summary>
        public int AuditStatus { get; set; }

        /// <summary>
        /// 审核说明
        /// </summary>
        public string AuditStatusExplain { get; set; }

        /// <summary>
        /// 是否上架(0,待上架，1，上架待审，2，上架)
        /// </summary>
        public int Uppershelf { get; set; }

        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; }


        /// <summary>
        /// 简介
        /// </summary>
        [Column(Name = "Intro", DbType = "varchar(2000)", IsNullable = true)]
        public string Intro { get; set; }

        /// <summary>
        /// 描述备注
        /// </summary>
        [Column(Name = "Description", DbType = "text", IsNullable = true)]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
       public string CreateByName { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadCount { get; set; }

        /// <summary>
        /// 数据新增方式 0:自建数据 1:拷贝自有数据(深度拷贝) 2:引用平台数据(通过购买行为、深度拷贝)
        /// </summary>
        public int DataCreateWay { get; set; }

        /// <summary>
        /// 原始数据Id
        /// </summary>
        public long OriginalDataId { get; set; }

        /// <summary>
        /// 显示在快捷栏
        /// </summary>
        public int IsShow { get; set; }

        /// <summary>
        /// 0:正常状态,1:正在克隆
        /// </summary>
        public int CloneStatus { get; set; }

        /// <summary>
        /// 是否编辑权限
        /// </summary>
        public int IsEdit { get; set; }
    }

    /// <summary>
    /// 课程统计数据表
    /// </summary>
    [Table(Name ="yssx_course_count")]

    public class YssxCourseCount: TenantBaseEntity<long>
    {
        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadCount { get; set; }

        /// <summary>
        /// 订单数
        /// </summary>
        public int OrderCount { get; set; }

        /// <summary>
        /// 点赞数
        /// </summary>
        public int DianZanCount { get; set; }
    }
    /// <summary>
    /// 课件库
    /// </summary>
    [Table(Name = "yssx_course_files")]
    public class YssxCourceFiles: TenantAndUserEntity<long>
    {
        /// <summary>
        /// 文件名称
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// 文件路径
        /// </summary>
        public string File { get; set; }

        public int Sort { get; set; }

        /// <summary>
        /// 知识点Id
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 知识点组Id
        /// </summary>
        public long KlgpGroupId { get; set; }

        /// <summary>
        /// 创建人姓名
        /// </summary>
        public string CreateByName { get; set; }
        
        /// <summary>        
        /// 文件类型 0:Excel 1:Word 2:PDF 3:PPT 4:Img 5:ZIP 6:mp4 7:3gp 8:wmv 9:rmvb 10:avi 11:dat 12:mkv 13:flv 14:vob 15:m4v
        /// </summary>
        public int FilesType { get; set; }

        /// <summary>
        /// 0:课件,1:视频,2:教案 3:授课计划 4:素材(课程备课) 5:思政案例 6:教材
        /// </summary>
        public int CategoryType { get; set; }

        /// <summary>
        /// 审核状态(0,待审核，1，审核成功，2，审核拒绝)
        /// </summary>
        public int AuditStatus { get; set; }

        /// <summary>
        /// 审核说明
        /// </summary>
        public string AuditStatusExplain { get; set; }

        /// <summary>
        /// 是否上架(0,待上架，1，上架待审，2，上架)
        /// </summary>
        public int Uppershelf { get; set; }

        /// <summary>
        /// 上架说明
        /// </summary>
        public string UppershelfExplain { get; set; }


        /// <summary>
        /// 文件大小
        /// </summary>
        public int FileSize { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadCount { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Column(Name = "Remark", DbType = "LongText", IsNullable = true)]
        public string Remark { get; set; }
    }

}
