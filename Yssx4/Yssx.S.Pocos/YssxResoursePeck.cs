﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 资源套餐
    /// </summary>
    [Table(Name = "yssx_resoursePeck")]
    public class YssxResoursePeck : BizBaseEntity<long>
    {

        /// <summary>
        /// 套餐名称
        /// </summary>
        public string PackgeName { get; set; }

        /// <summary>
        /// 学历
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }

    /// <summary>
    /// 套餐资源详情
    /// </summary>
    [Table(Name = "yssx_resoursePeck_details")]
    public class YssxResourcePeckDetails : BizBaseEntity<long>
    {
        /// <summary>
        /// 套餐Id
        /// </summary>
        public long ResourceId { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 资源模块名称
        /// </summary>
        public string ModuleName { get; set; }

        /// <summary>
        /// 资源模块类型
        /// </summary>
        public int ModuleType { get; set; }

        /// <summary>
        /// 入学年级
        /// </summary>
        //public int Year { get; set; }


        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }
}
