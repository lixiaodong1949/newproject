﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 备课课程
    /// </summary>
    [Table(Name = ("yssx_prepare_course"))]
    public class YssxPrepareCourse : TenantBaseEntity<long>
    {
        /// <summary>
        /// 课程名称
        /// </summary>
        [Column(Name = "CourseName", DbType = "varchar(255)", IsNullable = true)]
        public string CourseName { get; set; }

        /// <summary>
        /// 课程类型0:常规课
        /// </summary>
        public int CourseType { get; set; }

        /// <summary>
        /// 特点副标
        /// </summary>
        [Column(Name = "CourseTitle", DbType = "varchar(255)", IsNullable = true)]
        public string CourseTitle { get; set; }

        /// <summary>
        /// 课程类型
        /// </summary>
        public long CourseTypeId { get; set; }

        /// <summary>
        /// 知识点
        /// </summary>
        public string KnowledgePointId { get; set; }

        /// <summary>
        /// 学历标识
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        [Column(Name = "Author", DbType = "varchar(100)", IsNullable = true)]
        public string Author { get; set; }

        /// <summary>
        /// 出版社
        /// </summary>
        [Column(Name = "PublishingHouse", DbType = "varchar(100)", IsNullable = true)]
        public string PublishingHouse { get; set; }

        /// <summary>
        /// 出版时间
        /// </summary>
        public string PublishingDate { get; set; }

        /// <summary>
        /// 封面
        /// </summary>
        public string Images { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        [Column(Name = "Intro", DbType = "varchar(2000)", IsNullable = true)]
        public string Intro { get; set; }

        /// <summary>
        /// 描述备注
        /// </summary>
        [Column(Name = "Description", DbType = "text", IsNullable = true)]
        public string Description { get; set; }

        /// <summary>
        /// 创建人名称
        /// </summary>
        public string CreateByName { get; set; }

        /// <summary>
        /// 阅读次数
        /// </summary>
        public int ReadCount { get; set; }

        /// <summary>
        /// 更新用户Id
        /// </summary>
        public long UpdateById { get; set; }

        /// <summary>
        /// 更新用户名称
        /// </summary>
        public string UpdateByName { get; set; }


        /// <summary>
        /// 原始课程Id
        /// </summary>
        [Column(Name = "OriginalCourseId", IsNullable = true)]
        public Nullable<long> OriginalCourseId { get; set; }

        /// <summary>
        /// 课程来源 0:系统赠送 1:客户创建(购买) 2:上传私有
        /// </summary>
        [Column(Name = "CourseSource", IsNullable = true)]
        public Nullable<int> CourseSource { get; set; }

        /// <summary>
        /// 订单主键Id
        /// </summary>
        [Column(Name = "OrderId", IsNullable = true)]
        public Nullable<long> OrderId { get; set; }

        /// <summary>
        /// 课程有效期(赠送课程有有效期,选中备课后同有有效期)
        /// </summary>
        [Column(Name = "EffectiveDate", IsNullable = true)]
        public Nullable<DateTime> EffectiveDate { get; set; }

        /// <summary>
        /// 学期主键
        /// </summary>
        [Column(Name = "SemesterId", IsNullable = true)]
        public Nullable<long> SemesterId { get; set; }

    }
}
