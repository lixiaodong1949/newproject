﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FreeSql.DataAnnotations;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// LPR均值数据
    /// </summary>
    [Table(Name = "yssx_bigdata_lprmeandata")]

    public class YssxBdLPRMeanData
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("日期")]
        public string Rq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("1年LPR5日均值")]
        public string LPR_5 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("1年LPR10日均值")]
        public string LPR_10 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("1年LPR20日均值")]
        public string LPR_20 { get; set; }
    }
}
