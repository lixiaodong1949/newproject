﻿using FreeSql.DataAnnotations;
using System.ComponentModel;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 活跃债券统计
    /// </summary>
    [Table(Name = "yssx_bigdata_activebondstatistics")]
    public class YssxBdActiveBondStatistics
    {
        [Description("名次")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Mc { get; set; }

        [Description("债券简称")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Zqjc { get; set; }

        [Description("成交金额(亿元)")]

        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Cjje { get; set; }

        [Description("到期收益率")]

        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Dqsyl { get; set; }

        [Description("日期")]

        [Column(DbType = "varchar(50)", IsNullable = true)]

        public int Rq { get; set; }

        [Description("比数")]

        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Bs { get; set; }
    }
}
