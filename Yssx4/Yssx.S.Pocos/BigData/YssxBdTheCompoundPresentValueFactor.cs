﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 复利现值系数
    /// </summary>
    [Table(Name = "yssx_bigdata_presentvaluecoefficient")]

    public class YssxBdTheCompoundPresentValueFactor
    {
        [Column(DbType = "varchar(50)", IsNullable = true)][Description("期数")]
        public string Qs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("1%")]
        public string One { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("2%")]
        public string Two { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("3%")]
        public string Three { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("4%")]
        public string Four { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("5%")]
        public string Five { get; set; }


        [Column(DbType = "varchar(50)", IsNullable = true)][Description("6%")]
        public string Six { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("7%")]
        public string Seven { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("8%")]
        public string Eight { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("9%")]
        public string Nine { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("10%")]
        public string Ten { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("11%")]
        public string Eleven { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("12%")]
        public string Twelve { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("13%")]
        public string Thirteen { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("14%")]
        public string Fourteen { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("15%")]
        public string Fifteen { get; set; }


        [Column(DbType = "varchar(50)", IsNullable = true)][Description("16%")]
        public string sixteen { get; set; }


        [Column(DbType = "varchar(50)", IsNullable = true)][Description("17%")]
        public string seventeen { get; set; }


        [Column(DbType = "varchar(50)", IsNullable = true)][Description("18%")]
        public string eighteen { get; set; }


        [Column(DbType = "varchar(50)", IsNullable = true)][Description("19%")]
        public string nineteen { get; set; }


        [Column(DbType = "varchar(50)", IsNullable = true)][Description("20%")]
        public string twenty { get; set; }


        [Column(DbType = "varchar(50)", IsNullable = true)][Description("21%")]
        public string TwentyOne { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("22%")]
        public string TwentyTwo { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("23%")]
        public string TwentyThree { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("24%")]
        public string TwentyFour { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("25%")]
        public string TwentyFive { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("26%")]
        public string TwentySix { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("27%")]
        public string TwentySeven { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("28%")]
        public string TwentyEight { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("29%")]
        public string TwentyNine { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("30%")]
        public string Thirty { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("31%")]
        public string ThirtyOne { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("32%")]
        public string ThirtyTwo { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("33%")]
        public string ThirtyThree { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("34%")]
        public string ThirtyFour { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("35%")]
        public string ThirtyFive { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("36%")]
        public string ThirtySix { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("37%")]
        public string ThirtySeven { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("38%")]
        public string ThirtyEight { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("39%")]
        public string ThirtyNine { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("40%")]
        public string Forty { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("41%")]
        public string FortyOne { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("42%")]
        public string FortyTwo { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("43%")]
        public string FortyThree { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("44%")]
        public string FortyFour { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("45%")]
        public string FortyFive { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("46%")]
        public string FortySix { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("47%")]
        public string FortySeven { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("48%")]
        public string FortyEight { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("49%")]
        public string FortyNine { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("50%")]
        public string Fifty { get; set; }
    }
}
