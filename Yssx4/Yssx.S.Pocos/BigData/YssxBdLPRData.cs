﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FreeSql.DataAnnotations;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// LPR数据
    /// </summary>
    [Table(Name = "yssx_bigdata_lprdata")]

    public class YssxBdLPRData
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("日期")]
        public string Rq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("1年期LPR（%）")]
        public string LPR_1 { get; set; }
    }
}
