﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FreeSql.DataAnnotations;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 其他指数
    /// </summary>
    [Table(Name = "yssx_bigdata_otherindicators")]

    public class YssxBdOtherIndicators
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("指数代码")]
        public string Zsdm { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("月度标识")]
        public string Ydbs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("月开盘日期")]
        public string Ykprq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("月收盘日期")]
        public string Ysprq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("开盘指数")]
        public string Kpzs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("最高指数")]
        public string Zgzs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("最低指数")]
        public string Zdzs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("收盘指数")]
        public string Spzs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("成份证券成交量")]
        public string Cfzjcjl { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("成份证券成交金额")]
        public string Cfzjvjje { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
       [Description("指数回避率")]
        public string Zshbl { get; set; }
    }
}
