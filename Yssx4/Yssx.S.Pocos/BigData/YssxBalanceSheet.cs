﻿using FreeSql.DataAnnotations;
using System.ComponentModel;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 大数据资产负债
    /// </summary>
    [Table(Name = "yssx_bigdata_balancesheet")]
    public class YssxBalanceSheet 
    {
        [Description("证券简称")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string SecurityShort { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("行业名称")]
        public string Name { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("公司名称")]
        public string CompanyName { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("证券代码")]
        public string SecurityCode { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("会计期间")]
        public string AccountPeriod { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("报表类型")]
        public string ReportType { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("货币资金")]
        public string Monetary { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("客户资金存款")]
        public string CustmerDeposit { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("结算备付金")]
        public string DepositRFB { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("客户备付金")]
        public string CustmerRfB { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("现金及存放中央银行款项")]
        public string CashCentBankItem { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("存放同业款项")]
        public string BankMoney { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("贵金属")]
        public string PreciousMetal { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("拆出资金净额")]
        public string OutMoney { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("交易性金融资产")]
        public string DealBankingAsset { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("衍生金融资产")]
        public string DerivativeFinancialAsset { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("短期投资净额")]
        public string NetBOCI { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收票据净额")]
        public string NotesReceivableAmount { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收账款净额")]
        public string AccountReceivableAmount { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("预付款项净额")]
        public string PrepaymentAmount { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收保费净额")]
        public string ReceivablePremiumAmount { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收分保账款净额")]
        public string ReinsuranceAccountsReceivable { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收代位追偿款净额")]
        public string ReceivableDWZC { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收分保合同准备金净额")]
        public string ReceivableFBHTZBJ { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收分保未到期责任准备金净额")]
        public string ReceivableFBWDQZRZBJ { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收分保未决赔款准备金净额")]
        public string ReceivableFBWJPCZBJ { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收分保寿险责任准备金净额")]
        public string ReceivableFBSXZRZBJ { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收分保长期健康险责任准备金净额")]
        public string ReceivableFBCQJKXZRZBJ { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收利息净额")]
        public string InterestReceivableAmount { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应收股利净额")]
        public string DividendReceivableAmount { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("其他应收款净额")]
        public string OtherReceivableAmount { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("买入返售金融资产净额")]
        public string BuyResaleMonetaryAssets { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("存货净额")]
        public string AmountOfInvertory { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("一年内到期的非流动资产")]
        public string Ncatmwoy { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("存出保证金")]
        public string Ccbzj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("其他流动资产")]
        public string Qtldzc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("流动资产合计")]
        public string Ldzjhj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("保户质押贷款净额")]
        public string Bhzydk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("定期存款")]
        public string Dqck { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("发放贷款及垫款净额")]
        public string Ffdkjdk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("可供出售金融资产净额")]
        public string Kgcsjr { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("持有至到期投资净额")]
        public string Cyzdqtz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("长期应收款净额")]
        public string Cqysk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("长期股权投资净额")]
        public string Cqgqtz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("长期债权投资净额")]
        public string Cqzrtz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("长期投资净额")]
        public string Cqtz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("存出资本保证金")]
        public string Cczbbzj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("独立账户资产")]
        public string Dlzhzb { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("投资性房地产净额")]
        public string Tzxfdc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("固定资产净额")]
        public string Gdzc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("在建工程净额")]
        public string Zjgc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("工程物资")]
        public string Gcwz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("固定资产清理")]
        public string Gdzcql { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("生产性生物资产净额")]
        public string Swxswzc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("油气资产净额")]
        public string Qyzc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("无形资产净额")]
        public string Wxzc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("交易席位费")]
        public string Jyxwf { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("开发支出")]
        public string Kfzc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("商誉净额")]
        public string Syze { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("长期待摊费用")]
        public string Cqdtfy { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("递延所得税资产")]
        public string Dysdszc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("其他非流动资产")]
        public string Qtfldzc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("非流动资产合计")]
        public string Fldzchj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("其他资产")]
        public string Qtzc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("资产总计")]
        public string Zczj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("短期借款")]
        public string Dqjk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("质押借款")]
        public string Zydk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("向中央银行借款")]
        public string Xzyyhjk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("吸收存款及同业存放")]
        public string Xsckjtycf { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("同业及其他金融机构存放款项")]
        public string Tyjqtjrjgcfkx { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("吸收存款")]
        public string Xsck { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("拆入资金")]
        public string Crzj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("交易性金融负债")]
        public string Jyxjrfz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("衍生金融负债")]
        public string Ysjrfz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应付票据")]
        public string Yfpj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应付账款")]
        public string Yfzk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("预收款项")]
        public string Yskx { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("卖出回购金融资产款")]
        public string Mchgjrzck { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应付手续费及佣金")]
        public string Yfsxfjyj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应付职工薪酬")]
        public string Yfzgxc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应交税费")]
        public string Yjsf { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应付利息")]
        public string Yflx { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应付股利")]
        public string Yfgl { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应付赔付款")]
        public string Yfpfk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应付保单红利")]
        public string Yfbdhl { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("保户储金及投资款")]
        public string Bhcjjtzk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("保险合同准备金")]
        public string Bxhtzbj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("未到期责任准备金")]
        public string Wdqzrzbj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("未决赔款准备金")]
        public string Wjpkzbj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("寿险责任准备金")]
        public string Sxzrzbj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("长期健康险责任准备金")]
        public string Cqjkxzrzbj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("其他应付款")]
        public string Qtyfk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应付分保账款")]
        public string Yffbzk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("代理买卖证券款")]
        public string Dlmmzqk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("代理承销证券款")]
        public string Dlcszqk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("预收保费")]
        public string Ysbf { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("一年内到期的非流动负债")]
        public string Ynndqdfldfz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("其他流动负债")]
        public string Qtldfz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("递延收益-流动负债")]
        public string Dysyldfz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("流动负债合计")]
        public string Ldfzhj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("长期借款")]
        public string Cqjk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("独立账户负债")]
        public string Dlzhfz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("应付债券")]
        public string Yfzq { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("长期应付款")]
        public string Cqyfk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("专项应付款")]
        public string Zxyfk { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("长期负债合计")]
        public string Cqfzhj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("预计负债")]
        public string Yjfz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("递延所得税负债")]
        public string Dysdsfz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("其他非流动负债")]
        public string Qtfldfz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("递延收益-非流动负债")]
        public string Dysyfldfz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("非流动负债合计")]
        public string Fldfzhj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("其他负债")]
        public string Qtfz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("负债合计")]
        public string Fzhj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("实收资本(或股本)")]
        public string Sszb { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("其他权益工具")]
        public string Qtqygj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("优先股")]
        public string Yxg { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("永续债")]
        public string Yxz { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("其他")]
        public string Qt { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("资本公积")]
        public string Zbgj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("库存股")]
        public string Kcg { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("盈余公积")]
        public string Yygj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("一般风险准备")]
        public string Ybfxzb { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("未分配利润")]
        public string Wfplr { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("外币报表折算差额")]
        public string Wbbbzsce { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("未确认的投资损失")]
        public string Wqrdtzss { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("交易风险准备")]
        public string Jyfxzb { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("专项储备")]
        public string Zxcb { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("其他综合收益")]
        public string Qtzhsy { get; set; }
        [Description("归属于母公司所有者权益合计")]
        public string Gsymgssyzqyhj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("少数股东权益")]
        public string Ssgdqy { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("所有者权益合计")]
        public string Sszqyhj { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("负债与所有者权益总计")]
        public string Fzysyzqyzj { get; set; }
    }
}
