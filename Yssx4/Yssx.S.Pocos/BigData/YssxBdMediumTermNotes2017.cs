﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FreeSql.DataAnnotations;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 中期票据(AAA)2017
    /// </summary>
    [Table(Name = "yssx_bigdata_mediumtermnotes2017")]

    public class YssxBdMediumTermNotes2017
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("日期")]
        public string Rq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("标准期限(年)")]
        public string Bzqxn { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("基准债券")]
        public string Zzzw { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("剩余期限(年)")]
        public string Syqx { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("报买入收益率(%)")]
        public string Bmrsyl { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("报卖出收益率(%)")]
        public string Bmcsyl { get; set; }
    }
}
