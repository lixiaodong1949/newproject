﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FreeSql.DataAnnotations;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 披露财务指标
    /// </summary>
    [Table(Name = "yssx_bigdata_financialindicators")]

    public class YssxBdDisclosureOfFinancialIndicators
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("股票代码")]
        public string Gpdm { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("截止日期")]
        public string Jzrq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("非经常性损益")]
        public string Fjcxsy { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("归属于上市公司股东的扣除非经常性损益的净利润")]
        public string Gsyssgsgd { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("加权平均净资产收益率")]
        public string Jqpjjzcsyl { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("扣除非经常性损益后的加权平均净资产收益率")]
        public string Kcfjcx { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("扣除非经常性损益后的基本每股收益")]
        public string Kcfjcxmgsy { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("每股经营活动产生的现金流量净额")]
        public string Mgjyhdcsdxjllje { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("归属于上市公司股东的每股净资产")]
        public string Gsyssgsgddmgjzc { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("基本每股收益")]
        public string Jbmgsy { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("稀释每股收益")]
        public string Xsmgsy { get; set; }
    }
}
