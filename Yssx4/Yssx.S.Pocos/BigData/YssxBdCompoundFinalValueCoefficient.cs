﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FreeSql.DataAnnotations;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 复利终值系数
    /// </summary>
    [Table(Name = "yssx_bigdata_compoundfinalvaluecoefficient")]

    public class YssxBdCompoundFinalValueCoefficient
    {
        [Description("期数")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Qs { get; set; }

        [Description("1%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string One { get; set; }

        [Description("2%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Two { get; set; }

        [Description("3%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Three { get; set; }

        [Description("4%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Four { get; set; }

        [Description("5%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Five { get; set; }


        [Description("6%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Six { get; set; }

        [Description("7%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Seven { get; set; }

        [Description("8%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Eight { get; set; }

        [Description("9%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Nine { get; set; }

        [Description("10%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Ten { get; set; }

        [Description("11%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Eleven { get; set; }

        [Description("12%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Twelve { get; set; }

        [Description("13%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Thirteen { get; set; }

        [Description("14%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Fourteen { get; set; }

        [Description("15%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Fifteen { get; set; }


        [Description("16%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string sixteen { get; set; }


        [Description("17%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string seventeen { get; set; }


        [Description("18%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string eighteen { get; set; }


        [Description("19%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string nineteen { get; set; }


        [Description("20%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string twenty { get; set; }


        [Description("21%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string TwentyOne { get; set; }

        [Description("22%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string TwentyTwo { get; set; }

        [Description("23%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string TwentyThree { get; set; }

        [Description("24%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string TwentyFour { get; set; }

        [Description("25%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string TwentyFive { get; set; }

        [Description("26%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string TwentySix { get; set; }

        [Description("27%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string TwentySeven { get; set; }

        [Description("28%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string TwentyEight { get; set; }

        [Description("29%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string TwentyNine { get; set; }

        [Description("30%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Thirty { get; set; }

        [Description("31%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string ThirtyOne { get; set; }

        [Description("32%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string ThirtyTwo { get; set; }

        [Description("33%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string ThirtyThree { get; set; }

        [Description("34%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string ThirtyFour { get; set; }

        [Description("35%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string ThirtyFive { get; set; }

        [Description("36%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string ThirtySix { get; set; }

        [Description("37%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string ThirtySeven { get; set; }

        [Description("38%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string ThirtyEight { get; set; }

        [Description("39%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string ThirtyNine { get; set; }

        [Description("40%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Forty { get; set; }

        [Description("41%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string FortyOne { get; set; }

        [Description("42%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string FortyTwo { get; set; }

        [Description("43%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string FortyThree { get; set; }

        [Description("44%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string FortyFour { get; set; }

        [Description("45%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string FortyFive { get; set; }

        [Description("46%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string FortySix { get; set; }

        [Description("47%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string FortySeven { get; set; }

        [Description("48%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string FortyEight { get; set; }

        [Description("49%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string FortyNine { get; set; }

        [Description("50%")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Fifty { get; set; }
    }
}
