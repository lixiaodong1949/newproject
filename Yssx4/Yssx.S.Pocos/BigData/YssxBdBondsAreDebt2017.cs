﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 国债
    /// </summary>
    [Table(Name = "yssx_bigdata_bondsaredebt2017")]

    public class YssxBdBondsAreDebt2017
    {
        [Column(DbType = "varchar(50)", IsNullable = true)][Description("证券代码")]
        public string Zqdm { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("名称")]
        public string Mc { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("收盘价（元）")]
        public string Spj { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("到期收益率（%）")]
        public string Dqsyl { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("应计利息额（元）")]
        public string Yjlse { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("全价（元）")]
        public string Qj { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("实际发行量")]
        public string Sjfsc { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("发行价格（元）")]
        public string fXJG { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("期限（年）")]
        public string Qx { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("到期日")]
        public string Dqr { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("票面利率（%）")]
        public string Pmll { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("国债付息方式")]
        public string Gzdxfs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("计息日期")]
        public string Jxrq { get; set; }
    }
}
