﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FreeSql.DataAnnotations;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 企业债
    /// </summary>
    [Table(Name = "yssx_bigdata_corporatebonds")]

    public class YssxBdCorporateBonds
    {
        [Description("证券代码")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Zqdm { get; set; }

        [Description("名称")]

        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Mc { get; set; }

        [Description("收盘价（元）")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Spj { get; set; }

        [Description("到期收益率（%）")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Dqsyl { get; set; }

        [Description("应计利息额（元）")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Yjlxe { get; set; }

        [Description("全价（元）")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Qj { get; set; }

        [Description("发行人")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Fxr { get; set; }

        [Description("发行总额(亿元)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Fxze { get; set; }

        [Description("期限(年)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Qx { get; set; }

        [Description("发行价格(元)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Fxjg { get; set; }

        [Description("票面利率（%）")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Pmll { get; set; }

        [Description("付息频率")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Fxpl { get; set; }

        [Description("发行日期")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Fxrq { get; set; }

        [Description("付息日说明")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Fxrsm { get; set; }

        [Description("到期日期")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Dqrq { get; set; }

        [Description("债券信用评级")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Zqxypj { get; set; }
    }
}
