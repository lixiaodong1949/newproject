﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 国债2017
    /// </summary>
    [Table(Name = "yssx_bigdata_treasurybonds2017")]

    public class YssxBdTheYieldTreasuryBonds2017Model
    {
        //public long Id { get; set; }
        //[Column(DbType = "varchar(50)", IsNullable = true)][Column(DbType = "varchar(50)", IsNullable = true)]//("日期")][Column(DbType = "varchar(50)", IsNullable = true)]
        //public string Rq { get; set; }

        //[Column(DbType = "varchar(50)", IsNullable = true)][Column(DbType = "varchar(50)", IsNullable = true)]//("标准期限(年)")][Column(DbType = "varchar(50)", IsNullable = true)]
        //public string Bzqx { get; set; }

        //[Column(DbType = "varchar(50)", IsNullable = true)][Column(DbType = "varchar(50)", IsNullable = true)]//("基准债券")][Column(DbType = "varchar(50)", IsNullable = true)]
        //public string Jzzw { get; set; }

        //[Column(DbType = "varchar(50)", IsNullable = true)][Column(DbType = "varchar(50)", IsNullable = true)]//("剩余期限(年)")][Column(DbType = "varchar(50)", IsNullable = true)]
        //public string Syqx { get; set; }

        //[Column(DbType = "varchar(50)", IsNullable = true)][Column(DbType = "varchar(50)", IsNullable = true)]//("报买入收益率(%)")][Column(DbType = "varchar(50)", IsNullable = true)]
        //public string Bmrsyl { get; set; }

        //[Column(DbType = "varchar(50)", IsNullable = true)][Column(DbType = "varchar(50)", IsNullable = true)]//("报卖出收益率(%)")][Column(DbType = "varchar(50)", IsNullable = true)]
        //public string Bmcsyl { get; set; }
        [Description("日期")][Column(DbType = "varchar(50)", IsNullable = true)]
        public string Rq { get; set; }

        [Description("标准期限(年)")][Column(DbType = "varchar(50)", IsNullable = true)]
        public string Bzqx { get; set; }

        [Description("基准债券")][Column(DbType = "varchar(50)", IsNullable = true)]
        public string Jzzw { get; set; }

        [Description("剩余期限(年)")][Column(DbType = "varchar(50)", IsNullable = true)]
        public string Syqx { get; set; }

        [Description("报买入收益率(%)")][Column(DbType = "varchar(50)", IsNullable = true)]
        public string Bmrsyl { get; set; }

        [Description("报卖出收益率(%)")][Column(DbType = "varchar(50)", IsNullable = true)]
        public string Bmcsyl { get; set; }
    }
}
