﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 人民币参考汇率_2017
    /// </summary>
    [Table(Name = "yssx_bigdata_referencerateofrmb2017")]

    public class YssxBdReferenceRateOfRMB2017
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("币种")]
        public string Bz { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-12")]
        public string Years2017_12 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-11")]
        public string Years2017_11 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-10")]
        public string Years2017_10 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-09")]
        public string Years2017_09 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-08")]
        public string Years2017_08 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-07")]
        public string Years2017_07 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-06")]
        public string Years2017_06 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-05")]
        public string Years2017_05 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-04")]
        public string Years2017_04 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-03")]
        public string Years2017_03 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-03")]
        public string Years2017_02 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2017-03")]
        public string Years2017_01 { get; set; }

    }
}
