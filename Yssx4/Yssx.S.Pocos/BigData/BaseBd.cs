﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    public class BaseBd
    {
        [Description("年份")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Year { get; set; }
    }
}
