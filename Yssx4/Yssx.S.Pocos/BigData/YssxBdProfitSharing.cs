﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 红利分配表
    /// </summary>
    [Table(Name = "yssx_bigdata_profit_sharing")]
    public class YssxBdProfitSharing
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]

        [Description("证券代码")]
        public string Zqdm { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("证券简称")]
        public string Zqjc { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("财政年度")]
        public string Cznd { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("分配期标识")]
        public string Fpqbs { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("预案公告日期")]
        public string Yaggrq { get; set; }
        [Column(DbType = "varchar(200)", IsNullable = true)]
        [Description("预案内容")]
        public string Yanr { get; set; }
        [Column(DbType = "varchar(200)", IsNullable = true)]
        [Description("方案实施进度")]
        public string Fassjd { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("股东大会召开日期")]
        public string Gddhzkrq { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("股东大会公告发布日期")]
        public string Gddhggfbrq { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("股东大会公告刊物")]
        public string Gddhggkw { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("实施方案公告日期")]
        public string Ssfaggrq { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("分配方案公告刊物")]
        public string Fpfaggkw { get; set; }
        [Column(DbType = "varchar(200)", IsNullable = true)]
        [Description("实施方案内容")]
        public string Ssfanr { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("送股比")]
        public string Sgb { get; set; }
        [Column(DbType = "varchar(200)", IsNullable = true)]
        [Description("转增比")]
        public string Zzb { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("派息比税前")]
        public string Pxbsq { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("派息比税后")]
        public string Pxbsh { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("送转数量")]
        public string Szsl { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("派息数")]
        public string Pxs { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("股权登记日")]
        public string Gqdjr { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("最后交易日")]
        public string Zhjyr { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("除权除息日")]
        public string Cqcxr { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("红股转增股上市日")]
        public string Hgzzgssr { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("派息日")]
        public string Pxr { get; set; }
        [Column(DbType = "varchar(200)", IsNullable = true)]
        [Description("分配对象")]
        public string Fpdx { get; set; }
        [Column(DbType = "varchar(200)", IsNullable = true)]
        [Description("股权登记日收盘价")]
        public string Gqdjrspj { get; set; }
        [Column(DbType = "varchar(200)", IsNullable = true)]
        [Description("除权除息日收盘价")]
        public string Cqcxspj { get; set; }
    }
}
