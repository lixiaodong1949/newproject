﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 人民币汇率指数
    /// </summary>
    [Table(Name = "yssx_bigdata_rmbexchangrateindex")]

    public class YssxBdRMBExchangRateIndex
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("日期")]
        public string Rq { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("CFETS人民币汇率指数")]
        public string CFETS_Rmbhlzs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("BIS货币篮子人民币汇率指数")]
        public string BIS_Hblzrmbhlzs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("SDR货币篮子人民币汇率指数")]
        public string SDR_Hblzrmbhlzs { get; set; }
    }
}
