﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using FreeSql.DataAnnotations;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 金融机构人民币存款基准利率
    /// </summary>
    [Table(Name = "yssx_bigdata_depositinterestrate")]
    public class YssxBdBenchmarkRMBDepositRateOfFinancialInstitutions
    {
        [Description("调整时间")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Tzsj { get; set; }

        [Description("活期存款")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Hqck { get; set; }

        [Description("三个月定期存款")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Sgydqck { get; set; }

        [Description("半年定期存款")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Bndqck { get; set; }

        [Description("一年定期存款")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Yndqck { get; set; }

        [Description("二年定期存款")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Lndqck { get; set; }

        [Description("三年定期存款")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Sndqck { get; set; }

        [Description("五年定期存款")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Wndqck { get; set; }
    }
}
