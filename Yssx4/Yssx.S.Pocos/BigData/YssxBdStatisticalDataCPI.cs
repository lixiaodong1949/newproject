﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 统计数据CPI
    /// </summary>
    [Table(Name = "yssx_bigdata_statisticaldatacpi")]

    public class YssxBdStatisticalDataCPI
    {
        [Column(DbType = "varchar(50)", IsNullable = true)][Description("统计期间")]
        public string Tjqj { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("居民消费价格指数（上年=100）")]
        public string Jmxfjgzs { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("居民消费价格指数-食品（上年=100）")]
        public string Jmxfjgzs_sp { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("居民消费价格指数-烟酒及用品（上年=100）")]
        public string Jmxfjgzs_yjjyp { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("居民消费价格指数-衣着（上年=100）")]
        public string Jmxfjgzs_yz { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("居民消费价格指数-家庭设备用品及服务（上年=100）")]
        public string Jmxfjgzs_jtsbypjfw { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("居民消费价格指数-医疗保健及个人用品（上年=100）")]
        public string Jmxfjgzs_ylbjjgryp { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("居民消费价格指数-交通和通信（上年=100）")]
        public string Jmxfjgzs_jthtx { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("居民消费价格指数-娱乐教育文化用品及服务（上年=100）")]
        public string Jmxfjgzs_yljywhypjfw { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)][Description("居民消费价格指数-居住（上年=100）")]
        public string Jmxfjgzs_jz { get; set; }
    }
}
