﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 答案
    /// </summary>
    [Table(Name = "yssx_bigdata_columnscopy")]
    public class YssxBdColumnsCopy
    {
        /// <summary>
        /// Id
        /// </summary>
        [Column(DbType = "bigint", IsNullable = false)]
        public long Id { get; set; }
        /// <summary>
        /// 试卷Id
        /// </summary>
        [Column(DbType = "varchar(50)", IsNullable = false)]
        public string ExamId { get; set; }
        /// <summary>
        /// 与学生关联字段
        /// </summary>
        [Column(DbType = "varchar(50)", IsNullable = false)]
        public string GradeId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        [Column(DbType = "varchar(50)", IsNullable = false)]
        public string CoseId { get; set; }
        /// <summary>
        /// 题目Id
        /// </summary>
        [Column(DbType = "varchar(50)", IsNullable = false)]
        public string QuestionId { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        [Column(DbType = "text", IsNullable = false)]
        public string Answer { get; set; }
        /// <summary>
        /// 列名
        /// </summary>
        [Column(DbType = "varchar(50)", IsNullable = false)]
        public string ColumnName { get; set; }
    }
}
