﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 人民币参考汇率_2016
    /// </summary>
    [Table(Name = "yssx_bigdata_referencerateofrmb2016")]

    public class YssxBdReferenceRateOfRMB2016
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("币种")]
        public string Bz { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-12")]
        public string Years2016_12 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-11")]
        public string Years2016_11 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-10")]
        public string Years2016_10 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-09")]
        public string Years2016_09 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-08")]
        public string Years2016_08 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-07")]
        public string Years2016_07 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-06")]
        public string Years2016_06 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-05")]
        public string Years2016_05 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-04")]
        public string Years2016_04 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-03")]
        public string Years2016_03 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-03")]
        public string Years2016_02 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2016-03")]
        public string Years2016_01 { get; set; }

    }
}
