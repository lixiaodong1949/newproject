﻿using System;
using System.Collections.Generic;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 模块表
    /// </summary>
    [Table(Name = "yssx_bigdata_parentlist")]
    public class YssxBdParentList
    {
        public long Id { get; set; }
        [Column(DbType = "varchar(50)", IsNullable = true)]//("模块名称")]
        public string Name { get; set; }

        [Column(DbType = "bigint(20)", IsNullable = true)]//("表ID")]
        public int TableId { get; set; }

        [Column(DbType = "bigint(20)", IsNullable = true)]//("父模块Id")]
        public int ParentId { get; set; }

        [Column(DbType = "varchar(20)", IsNullable = true)]//("年份")]
        public string Year { get; set; }
    }
}
