﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 人民币参考汇率_2015
    /// </summary>
    [Table(Name = "yssx_bigdata_referencerateofrmb2015")]

    public class YssxBdReferenceRateOfRMB2015
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("币种")]
        public string Bz { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-12")]
        public string Years2015_12 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-11")]
        public string Years2015_11 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-10")]
        public string Years2015_10 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-09")]
        public string Years2015_09 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-08")]
        public string Years2015_08 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-07")]
        public string Years2015_07 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-06")]
        public string Years2015_06 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-05")]
        public string Years2015_05 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-04")]
        public string Years2015_04 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-03")]
        public string Years2015_03 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-03")]
        public string Years2015_02 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2015-03")]
        public string Years2015_01 { get; set; }
    }
}
