﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 价格指数
    /// </summary>
    [Table(Name = "yssx_bigdata_capi")]
    public class YssxBdCAPI
    {
        [Description("指标")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Zb { get; set; }

        [Description("居民消费价格指数(上年=100)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Jmxfjgzs { get; set; }

        [Description("城市居民消费价格指数(上年=100)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Csjmxfjgzs { get; set; }

        [Description("农村居民消费价格指数(上年=100)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Ncjmxfjgzs { get; set; }

        [Description("商品零售价格指数(上年=100)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Splsjgzs { get; set; }

        [Description("工业生产者出厂价格指数(上年=100)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Gysczccjgzs { get; set; }

        [Description("工业生产者购进价格指数(上年=100)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]

        public string Gysczgjjgzs { get; set; }

        [Description("固定资产投资价格指数(上年=100)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Gdzctzjgzs { get; set; }
    }
}
