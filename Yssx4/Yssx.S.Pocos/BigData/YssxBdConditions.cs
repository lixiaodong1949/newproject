﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;


namespace Yssx.S.Pocos.BigData
{

    [Table(Name = "yssx_bigdata_conditions")]
    public class YssxBdConditions
    {
        /// <summary>
        /// Id
        /// </summary>
        [Column(DbType = "bigint", IsNullable = false)]
        public long Id { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        [Column(DbType = "varchar(50)", IsNullable = false)]
        public string Name { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        [Column(DbType = "varchar(50)", IsNullable = false)]
        public string Value { get; set; }
        /// <summary>
        /// 符号
        /// </summary>
        [Column(DbType = "varchar(50)", IsNullable = false)]
        public string Symbol { get; set; }
    }
}
