﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 人民币参考汇率_2014
    /// </summary>
    [Table(Name = "yssx_bigdata_referencerateofrmb2014")]

    public class YssxBdReferenceRateOfRMB2014
    {
        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("币种")]
        public string Bz { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2014-12")]
        public string Years2014_12 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2014-11")]
        public string Years2014_11 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2014-10")]
        public string Years2014_10 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2014-09")]
        public string Years2014_09 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2014-08")]
        public string Years2014_08 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2014-07")]
        public string Years2014_07 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2014-06")]
        public string Years2014_06 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2014-05")]
        public string Years2014_05 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2014-04")]
        public string Years2014_04 { get; set; }

        [Column(DbType = "varchar(50)", IsNullable = true)]
        [Description("2014-03")]
        public string Years2014_03 { get; set; }

    }
}
