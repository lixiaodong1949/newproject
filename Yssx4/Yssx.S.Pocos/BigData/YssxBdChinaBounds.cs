﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Yssx.S.Pocos.BigData
{
    /// <summary>
    /// 中国国债收益率表
    /// </summary>
    [Table(Name = "yssx_bigdata_chinabounds")]
    public class YssxBdChinaBounds
    {
        [Description("统计日期")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Tjrq { get; set; }

        [Description("1年期国债收益率(%)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Ynqgzsyl { get; set; }

        [Description("10年期国债收益率(%)")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string Snqgzsyl { get; set; }
    }
}
