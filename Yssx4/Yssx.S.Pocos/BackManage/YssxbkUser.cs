﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    [Table(Name = "yssx_bk_user")]
    public class YssxbkUser : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [Column(Name = "UserName", DbType = "varchar(255)", IsNullable = true)]
        public string UserName { get; set; }

        /// <summary>
        /// 密码MD5加密
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 0普通用户1管理员
        /// </summary>
        public int UserType { get; set; }
    }
}
