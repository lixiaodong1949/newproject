﻿using System;
using System.Collections.Generic;
using System.Text;
using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.BackManage
{
    [Table(Name = "yssx_banner")]
    public class YssxBanner : BizBaseEntity<long>
    {
        /// <summary>
        /// 跳转url
        /// </summary>
        [Column(Name = "BannerUrl", DbType = "varchar(255)", IsNullable = true)]
        public string BannerUrl { get; set; }
        /// <summary>
        /// 图片地址
        /// </summary>
        [Column(Name = "ImageUrl", DbType = "varchar(255)", IsNullable = true)]
        public string ImageUrl { get; set; }
        /// <summary>
        /// 终端类型
        /// </summary>
        [Column(DbType = "int")]
        public ClientType ClientType { get; set; }

        /// <summary>
        /// 商品Id
        /// </summary>
        public long ProductId { get; set; }
    }
    
}
