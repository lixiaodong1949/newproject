﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 教师备课课程班级关系表
    /// </summary>
    [Table(Name = "yssx_teacher_course_class")]
    public class YssxTeacherCourseClass : BizBaseEntity<long>
    {
        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        /// 院系名称
        /// </summary>
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 教师Id
        /// </summary> 
        public long TeacherId { get; set; }

        /// <summary>
        /// 备课课程Id
        /// </summary> 
        public long CourseId { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary>
        public string ClassName { get; set; }

    }
}
