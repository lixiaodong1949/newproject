﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 学校购买案例记录表
    /// </summary>
    [Table(Name="yssx_school_case")]
    public class YssxSchoolCase: BizBaseEntity<long>
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public long OrderId { get; set; }
        /// <summary>
        /// 学校Id
        /// </summary>
        public long SchoolId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科）
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 购买角色  0教务 1教师
        /// </summary>
        [Column(DbType = "int")]
        public CaseUserTypeEnums CaseUserType { get; set; }
        /// <summary>
        /// 购买账号
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 开通账号
        /// </summary>
        [Column(DbType = "int")]
        public OrderUserEnum OrderUser { get; set; }
        /// <summary>
        /// 开通时间
        /// </summary>
        public DateTime OpenTime { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>
        public DateTime ExpireTime { get; set; }
        /// <summary>
        /// 支付状态
        /// </summary>
        [Column(DbType = "int")]
        public PayStatus PayStatus { get; set; }
        /// <summary>
        /// 跟进人
        /// </summary>
        public string PersonInCharge { get; set; }
        /// <summary>
        /// 0关闭 1开启
        /// </summary>
        [Column(DbType = "int")]
        public Status OpenStatus { get; set; }

        /// <summary>
        /// 赛前训练启用状态（0 不启用 1启用）
        /// </summary>
        public int TestStatus { get; set; }
        /// <summary>
        /// 正式赛启用状态（0 不启用 1启用）
        /// </summary>
        public int OfficialStatus { get; set; }

        /// <summary>
        /// 序号 
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 模块Id
        /// </summary>
        public int ModuleId { get; set; }
    }

    [Table(Name = "yssx_school_resource")]
    public class YssxSchoolResource : BizBaseEntity<long>
    {
        public long SchoolId { get; set; }

        public long ResourceId { get; set; }

        public int Sort { get; set; }

        public string ResourceName { get; set; }

        public int Year { get; set; }

        public int ModuleId { get; set; }
    }
}
