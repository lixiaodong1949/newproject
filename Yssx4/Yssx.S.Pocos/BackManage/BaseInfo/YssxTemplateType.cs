﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    [Table(Name = "yssx_template_type")]
    /// <summary>
    /// 凭证模板分类
    /// </summary>
    public class YssxTemplateType : BizBaseEntity<long>
    {
        /// <summary>
        /// 父Id
        /// </summary>
        public long ParentId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
