﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 学院专业
    /// </summary>
    [Table(Name = "yssx_profession")]
    public class YssxProfession : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 专业名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 院系Id
        /// </summary>
        public long CollegeId { get; set; }
    }
}
