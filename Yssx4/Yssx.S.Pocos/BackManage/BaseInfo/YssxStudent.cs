﻿using FreeSql.DataAnnotations;
using System;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 学生
    /// </summary>
    [Table(Name = "yssx_student")]
    public class YssxStudent : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary> 
        public long UserId { get; set; }

        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        ///院系Id
        /// </summary> 
        public long CollegeId { get; set; }

        /// <summary>
        ///院系名称
        /// </summary> 
        public string CollegeName { get; set; }

        /// <summary>
        /// 专业Id
        /// </summary> 
        public long ProfessionId { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary> 
        public string ProfessionName { get; set; }

        /// <summary>
        /// 班级Id
        /// </summary> 
        public long ClassId { get; set; }

        /// <summary>
        /// 班级名称
        /// </summary> 
        public string ClassName { get; set; }

        /// <summary>
        /// 过期日期
        /// </summary>
        public DateTime ExpiredDate { get; set; }

        /// <summary>
        /// 入学年份
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// 用户注册来源那个系统
        /// </summary>
        [Column(Name = "RegisterSysId", MapType = typeof(int), DbType = "int(11)")]
        public RegisterSourceEnum RegisterSource { get; set; } = RegisterSourceEnum.Yssx;
    }
}
