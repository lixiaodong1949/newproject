﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 教师
    /// </summary>
    [Table(Name = "yssx_teacher")]
    public class YssxTeacher : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 工号
        /// </summary>
        public string TeacherNo { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 用户id
        /// </summary> 
        public long UserId { get; set; }

        /// <summary>
        /// 职称
        /// </summary> 
        public string Titles { get; set; }

        /// <summary>
        /// 老师类型
        /// </summary>
        [Column(DbType ="int")]
        public TeacherType Type { get; set; }
    }
}
