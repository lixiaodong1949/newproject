﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 员工表
    /// </summary>
    [Table(Name = "yssx_admin_employee")]
    public class YssxAdminEmployee : BizBaseEntity<long>
    {
        /// <summary>
        /// 工号
        /// </summary>
        public string EmpNo { get; set; }
        /// <summary>
        /// 员工名称
        /// </summary>
        public string EmpName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        [Newtonsoft.Json.JsonIgnore]
        public string Pwd { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 办公电话
        /// </summary>
        public string OfficeTel { get; set; }
        /// <summary>
        /// 传真
        /// </summary>
        public string OfficeFax { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        ///性别
        /// </summary>
        public Sex Sex { get; set; }
        /// <summary>
        /// 角色ID，多个以,隔开
        /// </summary>
        public int RoleId { get; set; }

    }
}
