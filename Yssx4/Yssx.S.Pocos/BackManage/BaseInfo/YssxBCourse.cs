﻿using FreeSql.DataAnnotations;
using System.ComponentModel;

namespace Yssx.S.Pocos.BackManage.BaseInfo
{
    /// <summary>
    /// 课程
    /// </summary>
    [Table(Name = "yssx_course")]
    public class YssxBCourse
    {
        [Description("课程名称")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string kcmc { get; set; }

        [Description("特点副标")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string tdbf { get; set; }

        [Description("课程类型")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string kclx { get; set; }

        [Description("课程学历")]
        [Column(DbType = "varchar(50)", IsNullable = true)]
        public string kcxl { get; set; }
    }
}
