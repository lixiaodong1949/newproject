﻿using FreeSql.DataAnnotations;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 院系
    /// </summary>
    [Table(Name = "yssx_college")]
    public class YssxCollege : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 院系长
        /// </summary>
        public string President { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(DbType = "int")]
        public Status Status { get; set; }

    }
}
