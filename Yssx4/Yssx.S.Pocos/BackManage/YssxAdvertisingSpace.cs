﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 广告位
    /// </summary>
    [Table(Name = "yssx_advertisingspace")]
    public class YssxAdvertisingSpace : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 类型
        /// 0图片 1商品
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 0是启动页 1首页轮播图
        /// </summary>
        public int Difference { get; set; }

        /// <summary>
        /// 平台类型
        /// 1专一网PC 11专一网APP 2技能抽查PC 22技能抽查APP  3技能竞赛PC 33技能竞赛APP 44云实习APP
        /// </summary>
        public int PlatformType { get; set; }
    }

    /// <summary>
    /// 广告位-详情
    /// </summary>
    [Table(Name = "yssx_advertisingspace_details")]
    public class YssxAdvertisingSpaceDetails : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 副标题
        /// </summary>
        public string Subtitle { get; set; }

        /// <summary>
        /// 广告图
        /// </summary>
        public string AdvertisingMap { get; set; }

        /// <summary>
        /// 访问链接
        /// </summary>
        public string Visitlink { get; set; }

        /// <summary>
        /// 活动价
        /// </summary>
        public decimal ActivityPrice { get; set; }

        /// <summary>
        /// 划线价
        /// </summary>
        public decimal CrossedPrice { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 状态
        /// 0生效 1失效
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 产品Id
        /// </summary>
        public long ProductId { get; set; }
    }
}
