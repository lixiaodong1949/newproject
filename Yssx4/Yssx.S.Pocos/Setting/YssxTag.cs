﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 标签
    /// </summary>
	[Table(Name = "yssx_tag")]
    public class YssxTag : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 数据来源
        /// </summary>
        public TagSourceType TagSourceType { get; set; }

        /// <summary>
        /// 案例ID
        /// </summary>
        public long CaseId { get; set; } = 0;

        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; } = Status.Enable;
    }
}
