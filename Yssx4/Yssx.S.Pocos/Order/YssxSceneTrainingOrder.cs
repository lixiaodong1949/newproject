﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 场景实训订单表
    /// </summary>
    [Table(Name = ("yssx_scene_training_order"))]
    public class YssxSceneTrainingOrder
    {
        /// <summary>
        /// 订单主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string OrderNumber { get; set; }

        /// <summary>
        /// 场景实训Id
        /// </summary>
        public long SceneTrainingId { get; set; }

        /// <summary>
        /// 下单人Id
        /// </summary>
        public long CustomerId { get; set; }

        /// <summary>
        /// 下单人名称
        /// </summary>
        public string CreateByName { get; set; }

        /// <summary>
        /// 支付方式 0:支付宝 1:微信 2:网银
        /// </summary>
        public Nullable<int> PaymentMethod { get; set; }

        /// <summary>
        /// 购买时间
        /// </summary>
        public Nullable<DateTime> PaymentTime { get; set; }

        /// <summary>
        /// 订单金额
        /// </summary>
        public decimal OrderMoney { get; set; }

        /// <summary>
        /// 优惠金额
        /// </summary>
        public decimal DiscountMoney { get; set; }

        /// <summary>
        /// 支付金额
        /// </summary>
        public decimal PaymentMoney { get; set; }

        /// <summary>
        /// 订单状态 0:系统创建(赠送场景实训订单)【Obsolete】 1:创建订单(未支付) 2:取消订单 3:删除取消订单 4:支付成功订单(已支付) 5:删除支付成功订单 6:删除失效订单
        /// </summary>
        public Nullable<int> OrderStatus { get; set; }

        /// <summary>
        /// 订单来源 0:系统赠送 1:客户创建(购买)
        /// </summary>
        public Nullable<int> OrderSource { get; set; }

        /// <summary>
        /// 有效期
        /// </summary>
        public Nullable<DateTime> EffectiveDate { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public Nullable<DateTime> CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 更新时间
        /// </summary>
        public Nullable<DateTime> UpdateTime { get; set; }

    }
}
