﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 用户订单 - 明细
    /// </summary>
    [Table(Name = "yssx_user_order_detail")]
    public class YssxUserOrderDetail : BizBaseEntity<long>
    {
        /// <summary>
        /// 订单Id
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 产品Id
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// 案例类型（1.云实习真账实操 2.课程同步实训圆梦造纸 3.业税财实训）
        /// </summary>
        public int CaseType { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal OriginalPrice { get; set; }

        /// <summary>
        /// 折扣价
        /// </summary>
        public decimal DiscountPrice { get; set; }

    }
}
