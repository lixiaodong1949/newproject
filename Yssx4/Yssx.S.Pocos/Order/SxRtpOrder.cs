﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Order
{
    /// <summary>
    /// 云上实习抢购订单（临时）
    /// </summary>
    [Table(Name = "sx_rtp_order")]
    public class SxRtpOrder : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 用户id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 推广人id
        /// </summary>
        public long PromoterId { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 订单编号（字母+年月日+6位随机）【ZZSC.云实习真账实操  KCTB.课程同步实训圆梦造纸 YESC.业税财实训】
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 交易号
        /// </summary>
        public string TransactionNo { get; set; }

        /// <summary>
        /// 订单描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 状态（0.未支付 1.已取消 2.已付款）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 支付渠道（1.微信 2.支付宝 3.微店）
        /// </summary>
        public int PaymentType { get; set; }

        /// <summary>
        /// 付款时间
        /// </summary>
        public DateTime? PaymentTime { get; set; }

        /// <summary>
        /// 平台 - 登录方式（0.PC 1.App 2.小程序 3.H5）
        /// </summary>
        public int LoginType { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal OriginalPrice { get; set; }

        /// <summary>
        /// 折扣价
        /// </summary>
        public decimal DiscountPrice { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
