﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 用户订单
    /// </summary>
    [Table(Name = "yssx_user_order")]
    public class YssxUserOrder : BizBaseEntity<long>
    {
        /// <summary>
        /// 订单编号（字母+年月日+6位随机）【ZZSC.云实习真账实操  KCTB.课程同步实训圆梦造纸 YESC.业税财实训】
        /// </summary>
        public string OrderNo { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 订单描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 状态（0.未支付 1.已取消 2.已付款）
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 支付渠道（1.微信 2.支付宝）
        /// </summary>
        public int PaymentType { get; set; }

        /// <summary>
        /// 付款时间
        /// </summary>
        public DateTime? PaymentTime { get; set; }

        /// <summary>
        /// 平台 - 登录方式（0.PC 1.App 2.小程序 3.H5 4.微店）
        /// </summary>
        public int LoginType { get; set; }

        /// <summary>
        /// 订单总额
        /// </summary>
        public decimal OrderTotal { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Qty { get; set; }

        /// <summary>
        /// 源订单ID（赠送的需要赋值）
        /// </summary>
        public long OriginalOrderId { get; set; }

        /// <summary>
        /// 交易号
        /// </summary>
        public string TransactionNo { get; set; }

    }
}
