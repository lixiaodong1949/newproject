﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 产品兑换券
    /// </summary>
    [Table(Name = "yssx_product_coupons")]
    public class YssxProductCoupons : BizBaseEntity<long>
    {
        /// <summary>
        /// 订单ID
        /// </summary>
        public long OrderId { get; set; }

        /// <summary>
        /// 卡券编号
        /// </summary>
        public string CouponsNo { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 实付款
        /// </summary>
        public decimal ActualPayment { get; set; }

        /// <summary>
        /// 付款时间
        /// </summary>
        public DateTime PaymentTime { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 是否已兑换
        /// </summary>
        public bool IsExchange { get; set; }

    }
}
