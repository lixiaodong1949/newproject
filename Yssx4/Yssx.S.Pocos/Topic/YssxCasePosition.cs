﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 案例--岗位中间表
    /// </summary>
    [Table(Name = "yssx_case_position")]
    public class YssxCasePosition : BizBaseEntity<long>
    {
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 岗位id
        /// </summary>
        public long PostionId { get; set; }
        /// <summary>
        /// 岗位id
        /// </summary>
        public string PostionName { get; set; }
        /// <summary>
        /// 时长
        /// </summary>
        public int TimeMinutes { get; set; }
    }
}
