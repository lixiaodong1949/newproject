﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 部门管理
    /// </summary>
    [Table(Name = "yssx_department")]
    public class YssxDepartment : BizBaseEntity<long>
    {
        /// <summary>
        /// 部门名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 图标路径
        /// </summary>
        public string FileUrl { get; set; }

        /// <summary>
        /// 案例ID(所属公司)
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        
    }
}
