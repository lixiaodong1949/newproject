﻿using FreeSql.DataAnnotations;
using System;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 图表题excel导入数据
    /// </summary>
    [Table(Name = "yssx_topic_fill_grid")]
    public class YssxTopicFillGrid
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 案例
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 图表模板名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)", IsNullable = true)]
        public string Name { get; set; }
        /// <summary>
        /// 导入的内容
        /// </summary>
        [Column(Name = "Content", DbType = "LongText", IsNullable = true)]
        public string Content { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        public int IsDelete { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
