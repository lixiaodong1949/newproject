﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos.Topics
{
    [Table(Name = "yssx_position")]
    public class YssxPosition : BizBaseEntity<long>
    {
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 缩略图标文字
        /// </summary>
        public string ThumbnailText { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 岗位图标地址
        /// </summary>
        public string FileUrl { get; set; }
    }
}
