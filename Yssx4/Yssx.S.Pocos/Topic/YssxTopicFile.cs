﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 题目附件表
    /// </summary>
    [Table(Name = "yssx_topic_file")]
    public class YssxTopicFile : BizBaseEntity<long>
    {
        ///public long TopicId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }
    }
}
