﻿using FreeSql.DataAnnotations;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 录题凭证数据记录表
    /// </summary>
    [Table(Name = "yssx_certificate_datarecord")]
    public class YssxCertificateDataRecord : BizBaseEntity<long>
    {
        /// <summary>
        /// 凭证题目Id
        /// </summary>
        public long CertificateTopicId { get; set; }
        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }

        /// <summary>
        /// 企业Id
        /// </summary>
        public long EnterpriseId { get; set; }

        /// <summary>
        /// 摘要信息
        /// </summary>
        public string SummaryInfo { get; set; }

        /// <summary>
        /// 科目Id
        /// </summary>
        public long SubjectId { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        [Column(Name = "BorrowAmount", DbType = "decimal(18,4)", IsNullable = true)]
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        [Column(Name = "CreditorAmount", DbType = "decimal(18,4)", IsNullable = true)]
        public decimal CreditorAmount { get; set; }

        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertificateDate { get; set; }
    }
}
