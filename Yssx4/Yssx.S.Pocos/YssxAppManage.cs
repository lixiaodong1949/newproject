﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 
    /// </summary>
    [Table(Name = "yssx_app_manage")]
    public class YssxAppManage : BizBaseEntity<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Column(Name = "Name", DbType = "varchar(255)", IsNullable = true)]
        public string Name { get; set; }

        /// <summary>
        /// 版本号
        /// </summary>
        [Column(Name = "version", DbType = "varchar(255)", IsNullable = true)]
        public string Version { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Column(Name = "description", DbType = "varchar(4000)", IsNullable = true)]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Column(Name = "appId", DbType = "int", IsNullable = true)]
        public AppIdEnum AppId { get; set; }

        /// <summary>
        /// 类型：1 ISO，2 Android
        /// </summary>
        [Column(Name = "type", DbType = "int", IsNullable = true)]
        public int Type { get; set; }

        /// <summary>
        /// 是否最新版本
        /// </summary>
        [Column(Name = "isNew")]
        public bool IsNew { get; set; } = true;

        /// <summary>
        /// 是否强制更新
        /// </summary>
        [Column(Name = "IsForcedUpdate")]
        public bool IsForcedUpdate { get; set; }

        /// <summary>
        /// 是否审核通过
        /// </summary>
        [Column(Name = "IsReview")]
        public bool IsReview { get; set; }
    }
}
