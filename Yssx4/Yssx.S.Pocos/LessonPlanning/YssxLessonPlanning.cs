﻿using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 课程备课表
    /// </summary>
    [Table(Name = "yssx_lesson_planning")]
    public class YssxLessonPlanning : TenantBizBaseEntity<long>
    {
        /// <summary>
        /// 备课名称
        /// </summary>
        public string LessonPlanName { get; set; }

        /// <summary>
        /// 课程Id
        /// </summary>
        public long CourseId { get; set; }

        /// <summary>
        /// 学生是否可看 0:禁止(不可看) 1:可看
        /// </summary>
        public int IsStudentLook { get; set; } = CommonConstants.IsNotLook;

        /// <summary>
        /// 学期主键
        /// </summary>
        [Column(Name = "SemesterId", IsNullable = true)]
        public Nullable<long> SemesterId { get; set; }
        
        /// <summary>
        /// 排序字段
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 数据新增方式 0:自建数据 1:拷贝自有数据(深度拷贝) 2:引用平台数据(通过购买行为、深度拷贝)
        /// </summary>
        public int DataCreateWay { get; set; }

        /// <summary>
        /// 原始数据Id
        /// </summary>
        public long OriginalDataId { get; set; }
    }
}
