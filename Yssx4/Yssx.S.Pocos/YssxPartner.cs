﻿using Aop.Api.Domain;
using FreeSql.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace Yssx.S.Pocos
{
    /// <summary>
    /// 合伙人信息
    /// </summary>
    [Table(Name ="yssx_partner")]
    public class YssxPartner: BaseEntity<long>
    {
        public string Name { get; set; }

        public string Mobile { get; set; }

        /// <summary>
        /// 0：还未联系,1:已联系
        /// </summary>
        public int Status { get; set; }

        public DateTime CreateTime { get; set; } = DateTime.Now;
    }
}
