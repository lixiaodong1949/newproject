using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏关卡 Dto
    /// </summary>
    public class GameLevelDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Required(ErrorMessage = "名称不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 积分
        /// </summary>
        [Required(ErrorMessage = "积分[Point]不能为空")]
        public int Point { get; set; } = 0;

        /// <summary>
        /// 排序
        /// </summary>
        [Required(ErrorMessage = "排序[Sort]不能为空")]
        public int Sort { get; set; } = 0;

        /// <summary>
        /// 用时
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 是否免费：true免费，false 收费
        /// </summary>
        public bool Free { get; set; }

        ///// <summary>
        ///// 当前关卡拥有的题目节点
        ///// </summary>
        //public GameTopicNode[] GameTopicNodes { get;set;}

}
}