using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏关卡 Dto
    /// </summary>
    public class GameLevelResponseDto
    {
        /// <summary>
        /// 关卡相关信息
        /// </summary>
        public GameLevelBaseInfoDto GameLevelBaseInfo { get; set; }

        /// <summary>
        /// 排名第一
        /// </summary>
        public CurrentGameLevelGradeNo1 RankingFirst { get; set; }

        /// <summary>
        /// 当前用户排名
        /// </summary>
        public GameGradeInfoDto CurrentUserRank { get; set; }

    }

    /// <summary>
    /// 
    /// </summary>
    public class GameLevelBaseInfoDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 积分
        /// </summary>
        public int Point { get; set; }

        /// <summary>
        /// 用时
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }

    }

    public class UserGameLevelBaseInfoDto
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// 用户名称
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string UserName { get; set; }

        /// <summary>
        /// 用户头像
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Logo { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public string Score { get; set; }

        /// <summary>
        /// 积分
        /// </summary>
        public int Point { get; set; }

        /// <summary>
        /// 排名
        /// </summary>
        public int Rank { get; set; }

        /// <summary>
        /// 用时
        /// </summary>
        public int TotalMinutes { get; set; }

        public DateTime CreateTime { get; set; }

        ///// <summary>
        ///// 案例Id ranking first 
        ///// </summary>
        //public long CaseId { get; set; }

        ///// <summary>
        ///// 任务id
        ///// </summary>
        //public long TaskId { get; set; }


    }


}