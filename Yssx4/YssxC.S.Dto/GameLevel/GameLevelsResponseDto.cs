using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏关卡 返回值Dto
    /// </summary>
    public class GameLevelsResponseDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 是否免费：true免费，false 收费
        /// </summary>
        public bool Free { get; set; }

        /// <summary>
        /// 作答记录 0锁住 1闯关中 2通关
        /// </summary>
        public GameLevelStatus Status { get; set; }
    }
}