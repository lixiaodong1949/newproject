using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏关卡 返回值Dto
    /// </summary>
    public class GameLevelListResponseDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 积分
        /// </summary>
        public int Point { get; set; } = 0;

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; } = 0;

        /// <summary>
        /// 用时
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 是否免费：true免费，false 收费
        /// </summary>
        public bool Free { get; set; }
    }
}