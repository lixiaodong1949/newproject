﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 用户日历下收付款题查询实体
    /// </summary>
    public class UserCalendarReceivingQuery : CalendarTopicRequest
    {
        ///// <summary>
        ///// 月度任务Id
        ///// </summary>
        //public long MonthTaskId { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 题目类型 1:付款题 2:收款题
        /// </summary>
        public int TopicType { get; set; }

    }

    /// <summary>
    /// 3D闯关 收付款题查询实体
    /// </summary>
    public class GameUserCalendarReceivingQuery
    {
        /// <summary>
        /// 关卡Id
        /// </summary>
        [Required]
        public long GameLevelId { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        [Required]
        public long GradeId { get; set; }

        /// <summary>
        /// 题目类型 1:付款题 2:收款题
        /// </summary>
        [Required]
        public int TopicType { get; set; }

        /// <summary>
        /// 岗位Id
        /// </summary>
        public Nullable<long> PositionId { get; set; }

        ///// <summary>
        ///// 日历开始日期
        ///// </summary>
        //public Nullable<DateTime> StartDate { get; set; }

        ///// <summary>
        ///// 日历结束日期
        ///// </summary>
        //public Nullable<DateTime> EndDate { get; set; }

    }
}
