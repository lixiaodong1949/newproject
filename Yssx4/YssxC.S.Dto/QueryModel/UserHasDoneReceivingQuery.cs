﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 用户已做收付款题查询实体
    /// </summary>
    public class UserHasDoneReceivingQuery : CalendarTopicRequest
    {
        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

    }

    /// <summary>
    /// 用户已做收付款题查询实体
    /// </summary>
    public class GameUserHasDoneReceivingQuery
    {

        ///// <summary>
        ///// 岗位Id
        ///// </summary>
        //public Nullable<long> PositionId { get; set; }
        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

    }
}
