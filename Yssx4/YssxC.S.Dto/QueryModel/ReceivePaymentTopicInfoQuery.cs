﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 收付款题查询实体
    /// </summary>
    public class ReceivePaymentTopicInfoQuery
    {
        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 父级题目Id
        /// </summary>
        public long ParentQuestionId { get; set; }
    }


    /// <summary>
    /// 收付款题查询实体
    /// </summary>
    public class GameReceivePaymentTopicInfoQuery
    {
        /// <summary>
        /// 关卡Id
        /// </summary>
        [Required]
        public long GameLevelId { get; set; }
        /// <summary>
        /// 主作答记录Id
        /// </summary>
        [Required]
        public long GradeId { get; set; }

        /// <summary>
        /// 父级题目Id
        /// </summary>
        public long ParentQuestionId { get; set; }
    }
}
