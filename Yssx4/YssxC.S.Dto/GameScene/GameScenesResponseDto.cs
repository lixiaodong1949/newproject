using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏场景 返回值Dto
    /// </summary>
    public class GameScenesResponseDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 3D场景文件（Json字符串）
        /// </summary>
        public string File3D { get; set; }
        
        /// <summary>
        /// 视频封面
        /// </summary>
        public string Logo { get; set; }
    }
}