using System;
using System.ComponentModel.DataAnnotations;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏场景 Dto
    /// </summary>
    public class GameSceneDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>        
        [Required(ErrorMessage ="名称[Name]不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 3D场景文件
        /// </summary>
        public string File3D { get; set; }

        /// <summary>
        /// 3D场景文件总时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 会话信息（Json字符串）
        /// </summary>
        public string Dialogues { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 视频封面
        /// </summary>
        public string Logo { get; set; }
    }

    /// <summary>
    /// 游戏场景 Dto
    /// </summary>
    public class GameSceneDialoguesDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 会话信息（Json字符串）
        /// </summary>
        public string Dialogues { get; set; }
    }



}