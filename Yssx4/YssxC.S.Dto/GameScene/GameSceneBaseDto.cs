using System;
using System.ComponentModel.DataAnnotations;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏场景 Dto
    /// </summary>
    public class GameSceneBaseDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 会话信息（Json字符串）
        /// </summary>
        public string Dialogues { get; set; }

        /// <summary>
        /// 3D场景文件总时长
        /// </summary>
        public int TotalMinutes { get; set; }
    }
}