﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 根据caseId查询辅助核算名称列表返回值DTO
    /// </summary>
   public class GetNameListByCaseIdResponseDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
