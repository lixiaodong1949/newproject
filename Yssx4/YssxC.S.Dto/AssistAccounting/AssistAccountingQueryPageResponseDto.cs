﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 公司账簿-辅助核算分页返回参数
    /// </summary>
    public class AssistAccountingQueryPageResponseDto
    {
        /// <summary>
        /// 唯一id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 类别（供应商 0、职员 1、客户 2,部门 3、项目 4）
        /// </summary>
        public AssistAccountingType? Type { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 科目名称
        /// </summary>
        public string SubjectName { get; set; }
        /// <summary>
        /// 账户
        /// </summary>
        public string BankAccountNo { get; set; }
        /// <summary>
        /// 开户行名称
        /// </summary>
        public string BankName { get; set; }
        /// <summary>
        /// 支行名称
        /// </summary>
        public string BankBranchName { get; set; }
        /// <summary>
        /// 公司账簿-辅助核算子类
        /// </summary>
        public List<SubAssistAccountingTypeDto> Subs { get; set; }
    }
}
