﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 公司账簿-辅助核算请求参数
    /// </summary>
    public class AssistAccountingDto
    {
        /// <summary>
        /// 唯一id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 类别（供应商 0、职员 1、客户 2,部门 3、项目 4,其他 5）
        /// </summary>
        public AssistAccountingType? Type { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 科目Id
        /// </summary>
        public long? SubjectId { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 账户
        /// </summary>
        public string BankAccountNo { get; set; }
        /// <summary>
        /// 开户行名称
        /// </summary>
        public string BankName { get; set; }
        /// <summary>
        /// 支行名称
        /// </summary>
        public string BankBranchName { get; set; }
        /// <summary>
        /// 公司账簿-辅助核算子类
        /// </summary>
        public List<SubAssistAccountingTypeDto> Subs { get; set; }
    }
    /// <summary>
    /// 辅助核算子类
    /// </summary>
    public class SubAssistAccountingTypeDto
    {
        /// <summary>
        /// 父id
        /// </summary>      
        [JsonIgnore]
        public long ParentId { get; set; }
        /// <summary>
        /// 类别（供应商 0、职员 1、客户 2,部门 3、项目 4）
        /// </summary>
        public AssistAccountingType Type { get; set; }
        /// <summary>
        /// 类别名称（Type选择部门和项目后输入的名称）
        /// </summary>
        public string Name { get; set; }
    }
}
