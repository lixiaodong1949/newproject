﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 根据公司id获取账簿-辅助核算名称列表
    /// </summary>
    public class GetNameListResponseDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 类别（供应商 0、职员 1、客户 2,部门 3、项目 4）
        /// </summary>
        public AssistAccountingType? Type { get; set; }
        /// <summary>
        /// 账簿-辅助核算信息
        /// </summary>
        public List<SubAssistAccountingNameResponseDto> Children { get; set; }
    }
    /// <summary>
    ///  账簿-辅助核算信息
    /// </summary>
    public class SubAssistAccountingNameResponseDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 类别名称（Type选择部门和项目后输入的名称）
        /// </summary>
        public string Name { get; set; }
    }
}
