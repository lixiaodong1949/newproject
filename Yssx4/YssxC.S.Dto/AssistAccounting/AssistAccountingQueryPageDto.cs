﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 辅助核算分页查询Dto
    /// </summary>
    public class AssistAccountingQueryPageDto : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 类别（供应商 0、职员 1、客户 2,部门 3、项目 4）
        /// </summary>
        public AssistAccountingType? Type { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
    }
}
