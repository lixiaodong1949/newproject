﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 辅助核算查询Dto
    /// </summary>
    public class AssistAccountingQueryDto
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 类别（供应商 0、职员 1、客户 2,部门 3、项目 4）
        /// </summary>
        public List<int> Types { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
    }
}
