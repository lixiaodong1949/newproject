﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 用户申报请求实体
    /// </summary>
    public class UserDeclareDto
    {
        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }
        
        /// <summary>
        /// 申报主表Id
        /// </summary>
        public long DeclareParentId { get; set; }
    }
}
