﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 散户作答记录计数请求实体
    /// </summary>
    public class RetailGradeCountDto
    {
        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }
    }
}
