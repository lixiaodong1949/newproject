﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 用户审核票据请求实体
    /// </summary>
    public class UserCheckBillTopicDto
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 岗位Id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 票据题目Id
        /// </summary>
        public long BillQuestionId { get; set; }

        /// <summary>
        /// 审核票据操作类型 0:审核通过 1:退回
        /// </summary>
        public int CheckBillType { get; set; }

        /// <summary>
        /// 是否是推送票据 false:不是 true:是  (推送票据只能审核通过)
        /// </summary>
        public bool IsPushBill { get; set; }

        /// <summary>
        /// 退回原因Id
        /// </summary>
        public Nullable<long> SendBackReasonId { get; set; }
    }
}
