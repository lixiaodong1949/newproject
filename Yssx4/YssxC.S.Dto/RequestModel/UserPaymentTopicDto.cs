﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 用户付款题请求实体
    /// </summary>
    public class UserPaymentTopicDto
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 岗位Id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 付款题目Id
        /// </summary>
        public long PaymentQuestionId { get; set; }

        /// <summary>
        /// 付款金额
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// 收款单位
        /// </summary>
        public long ReceivingBankId { get; set; }

    }
}
