﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 用户收款题请求实体
    /// </summary>
    public class UserReceiveTopicDto
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 岗位Id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 收款题目Id
        /// </summary>
        public long ReceiveQuestionId { get; set; }
    }
}
