﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 用户生成主作答记录请求实体
    /// </summary>
    public class UserCreateMasterAnswerDto
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 是否购买过 false:没有购买 true:购买过
        /// </summary>
        public bool IsBought { get; set; }

        /// <summary>
        /// 平台：0：网页版 ，1：3D版,兼容以前网页版系统必须设置默认值0
        /// </summary>
        public int Platform { get; set; } = 0;

        ///// <summary>
        ///// 是否创建账套 false:不创建 true:创建
        ///// </summary>
        //public bool IsCreateSetBook { get; set; }

    }
}
