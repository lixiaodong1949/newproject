﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 云实习 - 通关记录
    /// </summary>
    public class GetCloudInternshipRecordDto
    {
        /// <summary>
        /// 作答记录ID
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 企业名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 月度任务名称
        /// </summary>
        public string MonthTaskName { get; set; }

        /// <summary>
        /// 通关时间
        /// </summary>
        public DateTime SubmitTime { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamPaperScore { get; set; }

    }

    /// <summary>
    /// 云实习 - 通关记录查询
    /// </summary>
    public class GetCloudInternshipRecordRequest : PageRequest
    {
        /// <summary>
        /// 试卷ID
        /// </summary>
        public long ExamId { get; set; }

    }

}
