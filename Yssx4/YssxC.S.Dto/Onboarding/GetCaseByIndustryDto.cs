﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 选择行业-企业
    /// </summary>
    public class GetCaseByIndustryDto
    {
        /// <summary>
        /// 公司ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 公司简称
        /// </summary>
        public string BriefName { get; set; }

        /// <summary>
        /// 公司logo
        /// </summary>
        public string LogoUrl { get; set; }

        /// <summary>
        /// X 坐标
        /// </summary>
        public int XCoordinate { get; set; }

        /// <summary>
        /// Y 坐标
        /// </summary>
        public int YCoordinate { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }


    }
}
