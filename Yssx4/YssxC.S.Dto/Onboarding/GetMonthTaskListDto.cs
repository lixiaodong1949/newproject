﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 月度任务
    /// </summary>
    public class GetMonthTaskListDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 任务详情
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 任务目标
        /// </summary>
        public string Target { get; set; }

        /// <summary>
        /// 是否需要新建账套
        /// </summary>
        public bool RequireAccountSet { get; set; }

        /// <summary>
        /// 积分
        /// </summary>
        public int Integral { get; set; }

        /// <summary>
        /// 是否已完成
        /// </summary>
        public bool IsCompleted { get; set; }

        /// <summary>
        /// 是否已付费
        /// </summary>
        public bool IsPaid { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 建议时长
        /// </summary>
        public string SuggestDuration { get; set; }

    }

    /// <summary>
    /// 月度任务
    /// </summary>
    public class GetGameMonthTaskListDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 任务详情
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 任务目标
        /// </summary>
        public string Target { get; set; }

        /// <summary>
        /// 是否已付费
        /// </summary>
        public bool IsPaid { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 是否完成培训
        /// </summary>
        public bool IsTrainFinish { get; set; }

        /// <summary>
        /// 用户培训Id
        /// </summary>
        [JsonIgnore]
        public long TrainFinishId { get; set; }

    }
}
