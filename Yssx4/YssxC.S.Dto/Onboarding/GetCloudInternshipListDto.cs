﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 实习列表 - 顶岗实习
    /// </summary>
    public class GetCloudInternshipListDto
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 月度任务Id
        /// </summary>
        public long MonthTaskId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }

        /// <summary>
        /// 月度任务名称
        /// </summary>
        public string MonthTaskName { get; set; }

        /// <summary>
        /// 案例简介
        /// </summary>
        public string ShortDesc { get; set; }

        /// <summary>
        /// 有效期开始
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 有效期结束
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// 所属行业ID
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// 所属行业名称
        /// </summary>
        public string IndustryName { get; set; }

    }

    /// <summary>
    /// 实习列表查询 - 顶岗实习
    /// </summary>
    public class GetCloudInternshipListRequest : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

    }
}
