﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 获取指定企业详细数据
    /// </summary>
    public class GetCaseInfoOnboardingDto
    {
        /// <summary>
        /// 企业Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 公司简称
        /// </summary>
        public string BriefName { get; set; }

        /// <summary>
        /// 公司规模
        /// </summary>
        public string Scale { get; set; }

        /// <summary>
        /// 行业Id
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// 行业名称
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 会计准则 0:小企业 1：企业 2：事业单位 3：非营利组织
        /// </summary>
        public AccountingStandard AccountingStandard { get; set; }

        /// <summary>
        /// 纳税主体
        /// </summary>
        public Taxpayer Taxpayer { get; set; }

        /// <summary>
        /// 法定代表人
        /// </summary>
        public string LegalRepresentative { get; set; }

        /// <summary>
        /// 统一社会信用代码
        /// </summary>
        public string UniformCode { get; set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        public string RegisteredTime { get; set; }

        /// <summary>
        /// 注册资本
        /// </summary>
        public string RegisteredCapital { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 公司地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 公司描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 经营范围
        /// </summary>
        public string BusinessScope { get; set; }

        /// <summary>
        /// 特殊说明
        /// </summary>
        public string SpecialInstructions { get; set; }

        /// <summary>
        /// 难度系数
        /// </summary>
        public int Rate { get; set; }

        /// <summary>
        /// 公司logo
        /// </summary>
        public string LogoUrl { get; set; }

        /// <summary>
        /// 描述视频
        /// </summary>
        public string VideoUrl { get; set; }

        /// <summary>
        /// 推荐语
        /// </summary>
        public string RecommendedLanguage { get; set; }

        /// <summary>
        /// 真帐描述
        /// </summary>
        public string RealDescription { get; set; }

        /// <summary>
        /// 开户行 - 默认
        /// </summary>
        public string OpeningBank { get; set; }

        /// <summary>
        /// 支行
        /// </summary>
        public string Branch { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string AccountNo { get; set; }

        /// <summary>
        /// 自定义数据
        /// </summary>
        public string CustomData { get; set; }

        /// <summary>
        /// 登记注册类型
        /// </summary>
        public string RegisterType { get; set; }

        /// <summary>
        /// 背景图
        /// </summary>
        public string BackgroundImage { get; set; }

        /// <summary>
        /// 企业 - 月度任务
        /// </summary>
        public List<CaseInfoMonthDetail> MonthDetail { get; set; }

        /// <summary>
        /// 企业 - 银行账户列表
        /// </summary>
        public List<BankAccountDetail> BankAccountDetail { get; set; }

    }
    /// <summary>
    /// 企业 - 月度任务
    /// </summary>
    public class CaseInfoMonthDetail
    {
        /// <summary>
        /// 任务ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }

    }
    /// <summary>
    /// 企业 - 银行账户列表
    /// </summary>
    public class BankAccountDetail
    {
        /// <summary>
        /// 开户行
        /// </summary>
        public string OpeningBank { get; set; }

        /// <summary>
        /// 支行
        /// </summary>
        public string Branch { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string AccountNo { get; set; }
    }

}
