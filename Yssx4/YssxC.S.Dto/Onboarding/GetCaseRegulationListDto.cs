﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 根据企业查询部门介绍
    /// </summary>
    public class GetCaseRegulationListDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 明细
        /// </summary>
        public List<GetCaseRegulationDetailDto> itemDetail { get; set; }

    }

    /// <summary>
    /// 根据企业查询部门介绍 - 明细
    /// </summary>
    public class GetCaseRegulationDetailDto
    {

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }

    }
}
