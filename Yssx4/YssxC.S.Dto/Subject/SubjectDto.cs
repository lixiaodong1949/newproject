﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto.Subject
{
    public class SubjectDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>
        public long SubjectCode { get; set; }

        /// <summary>
        /// 父级ID 最多四级
        /// </summary>

        public long ParentId { get; set; }

        /// <summary>
        /// 一级代码
        /// </summary>

        public string Code1 { get; set; }

        /// <summary>
        /// 二级代码
        /// </summary>

        public string Code2 { get; set; }

        /// <summary>
        /// 三级代码
        /// </summary>

        public string Code3 { get; set; }

        /// <summary>
        /// 四级代码
        /// </summary>

        public string Code4 { get; set; }

        /// <summary>
        /// 科目名称
        /// </summary>

        public string SubjectName { get; set; }

        /// <summary>
        /// 准则类型 0:小企业1:企业会计2:事业单位3:非盈利组织
        /// </summary>
        public int NormType { get; set; }

        /// <summary>
        /// 科目类型(1:资产,2:负债)
        /// </summary>
        public int SubjectType { get; set; }

        /// <summary>
        /// 余额方向(借,贷)
        /// </summary>
        public string Direction { get; set; }

        /// <summary>
        /// 助记码
        /// </summary>
        public string AssistCode { get; set; }

        /// <summary>
        /// 企业ID
       // / </summary>
        public long CaseId { get; set; }


        /// <summary>
        /// 借方金额
        /// </summary>
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        public decimal CreditorAmount { get; set; }


        /// <summary>
        /// 是否最后节点(1是,0不是)
        /// </summary>
        public int IsLastNode { get; set; }
        /// <summary>
        /// 是否有核算0:没有 1:有
        /// </summary>
        public int IsHaveCheck { get; set; }

        /// <summary>
        /// 核算Id
        /// </summary>
        public long AssistacId { get; set; }

        /// <summary>
        /// 损溢科目类别 0-A类 1-B类 2-C类 3-D类 4-E类
        /// </summary>
        public ProfitLossSubjectType ProfitLossSubjectType { get; set; }
    }

    public class SxAssistSubjectDto
    {
        public string Name { get; set; }

        public int Type { get; set; }
    }
}
