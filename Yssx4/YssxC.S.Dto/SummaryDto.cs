﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 摘要
    /// </summary>
    public class SummaryDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 企业Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 摘要名称
        /// </summary>
        public string Name { get; set; }

        public int Sort { get; set; }
    }
}
