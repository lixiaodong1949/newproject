﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 模板
    /// </summary>
    public class TemplateDto
    {
        /// <summary>
        /// 模板ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 模板类型ID
        /// </summary>
        public long TemplateTypeId { get; set; }
        /// <summary>
        /// 模板类型名称
        /// </summary>
        public string TemplateTypeName { get; set; }

        /// <summary>
        /// 全部HTML内容
        /// </summary>
        public string FullHtml { get; set; }
        /// <summary>
        /// 清除样式的HTML
        /// </summary>
        public string ClearStyleHtml { get; set; }
        /// <summary>
        /// 答案数据json
        /// </summary>
        public string AnswerJson { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
