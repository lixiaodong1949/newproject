﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 单选题、多选题、判断题
    /// </summary>
    public class SimpleQuestionRequest : BaseQuestion
    {
        /// <summary>
        /// 选项
        /// </summary>
        public List<ChoiceOption> Options { get; set; }
        /// <summary>
        /// 答案 内容（选择题答案a,b,c等，判断题true/false，表格json等）
        /// </summary>
        public override string AnswerValue { get; set; }
    }
}
