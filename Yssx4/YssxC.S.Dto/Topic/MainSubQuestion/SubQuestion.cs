﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 综合题子题目
    /// </summary>
    public class SubQuestion
    {
        public SubQuestion()
        {
        }

        public SubQuestion(long id, QuestionType questionType, QuestionContentType questionContentType, string templateId, string hint,
            string content, string topicContent, string fullContent, string answerValue, int sort, decimal score, CalculationType calculationType)
        {
            Id = id;
            QuestionType = questionType;
            QuestionContentType = questionContentType;
            TemplateId = templateId;
            Hint = hint;
            Content = content;
            TopicContent = topicContent;
            FullContent = fullContent;
            AnswerValue = answerValue;
            Sort = sort;
            Score = score;
            CalculationType = calculationType;
        }


        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 题型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 题目内容类型(1:纯文字,2:表格,3:图片)
        /// </summary>
        public QuestionContentType QuestionContentType { get; set; }

        /// <summary>
        /// 选项--多题型要支持选择题
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ChoiceOption> Options { get; set; }
        /// <summary>
        /// 模板ID
        /// </summary>
        public string TemplateId { get; set; }

        /// <summary>
        /// 答案解析
        /// </summary>
        public virtual string Hint { get; set; }
        /// <summary>
        /// 标题
        /// </summary> 
        public string Content { get; set; }
        /// <summary>
        /// 题目数据源内容(不含答案)
        /// </summary> 
        public string TopicContent { get; set; }

        /// <summary>
        /// 题目数据源内容(含答案)
        /// </summary>
        public string FullContent { get; set; }

        /// <summary>
        /// 答案
        /// </summary>
        public virtual string AnswerValue { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }

        #region 表格题
        /// <summary>
        /// 表格题-计算方式（按行计算、按列计算、按单元格平均计算、自由计算）
        /// </summary>
        public CalculationType CalculationType { get; set; }
        /// <summary>
        /// 表格题-是否开启复制Excel数据（0 否 1是）
        /// </summary>
        public int IsCopy { get; set; }
        /// <summary>
        /// 表格题-是否开启复制大数据（0 否 1是）
        /// </summary>
        public int IsCopyBigData { get; set; }
        /// <summary>
        /// 表格题-是否无序（0 否 1是）
        /// </summary>
        public int IsDisorder { get; set; }
        #endregion

        #region 分录题专用
        /*
        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }
        /// <summary>
        /// 凭证日期
        /// </summary>
        public string CertificateDate { get; set; }
        /// <summary>
        /// 单据数据记录
        /// </summary>
        public List<CertificateDataRecord> DataRecords { get; set; }
        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountingManager { get; set; }
        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }
        /// <summary>
        /// 制单人
        /// </summary>
        public string Creator { get; set; }
        /// <summary>
        /// 会计主管是否禁用
        /// </summary>
        public Status IsDisableAM { get; set; }
        /// <summary>
        /// 出纳是否禁用
        /// </summary>
        public Status IsDisableCashier { get; set; }
        /// <summary>
        /// 审核人是否禁用
        /// </summary>
        public Status IsDisableAuditor { get; set; }
        /// <summary>
        /// 制单人是否禁用
        /// </summary>
        public Status IsDisableCreator { get; set; }
        /// <summary>
        /// 计分方式 （0 统一设置 1 单独设置 2 单元格计分）
        /// </summary>
        public AccountEntryCalculationType CalculationScoreType { get; set; }
        /// <summary>
        /// 外围
        /// </summary>
        public int OutVal { get; set; }
        /// <summary>
        /// 里围
        /// </summary>
        public int InVal { get; set; }

        /// <summary>
        /// 凭证题目相关设置
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CertificateTopicView CertificateTopic { get; set; }

        /// <summary>
        /// 摘要是否计分
        /// </summary>
        public bool IsSummaryCalculation { get; set; }
        */
        #endregion
    }
}
