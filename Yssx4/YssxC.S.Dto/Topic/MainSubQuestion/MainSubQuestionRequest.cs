﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 多题型（单个题干-多个子题目）
    /// </summary>
    public class MainSubQuestionRequest : MainQuestion
    {
        /// <summary>
        /// 子题目
        /// </summary>
        public List<SubQuestion> SubQuestion { get; set; }
    }
}
