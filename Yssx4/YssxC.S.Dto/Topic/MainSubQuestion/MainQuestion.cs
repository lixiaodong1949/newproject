﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 综合题 主题干
    /// </summary>
    public class MainQuestion
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime BusinessDate { get; set; }

        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 是否结账题（0 否 1是）
        /// </summary>
        public int IsSettlement { get; set; }
        /// <summary>
        /// 具体分类ID：基本技能类别ID，课程ID等
        /// </summary>
        public int QuestionTagTypeId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 解析
        /// </summary>
        public string Hint { get; set; }
        /// <summary>
        /// 师傅提示
        /// </summary>
        public string TeacherHint { get; set; }
        /// <summary>
        /// 题干内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 题目附件
        /// </summary>
        public List<TopicFile> QuestionFile { get; set; }
        /// <summary>
        /// 题型
        /// </summary>
        [JsonIgnore]
        public QuestionType QuestionType => QuestionType.MainSubQuestion;

        /// <summary>
        /// 题目难度
        /// </summary>
        public int? DifficultyLevel { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 学历标记（0 中职 1高职 2 本科） 此处不可编辑，来自案例主表学历标记
        /// </summary>
        public int Education { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string Keywords { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 会计期间
        /// </summary>
        public string AccountingPeriodDate { get; set; }

        /// <summary>
        /// 分岗所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PointPositionId { get; set; } = 0;

        /// <summary>
        /// 技能Id
        /// </summary>
        public long SkillId { get; set; }

    }
}
