﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 题目查询返回集合Dto
    /// </summary>
    public class TopicListPageDto
    {
        /// <summary>
        /// 案例总题数
        /// </summary>
        public long TotalCount { get; set; }

        /// <summary>
        /// 案例总分
        /// </summary>
        public decimal TotalScore { get; set; }
        /// <summary>
        /// 审核总分
        /// </summary>

        public decimal AuditScore { get; set; }

        /// <summary>
        /// 岗位总分数统计
        /// </summary>
        public List<TopicQueryPositionTotalScore> PositionQuestionDetail { get; set; }

        /// <summary>
        /// 查询列表数据
        /// </summary>
        public PageResponse<TopicListDataDto> Detail { get; set; }
    }
    /// <summary>
    /// 每个岗位总分数
    /// </summary>
    public class TopicQueryPositionTotalScore
    {
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 所属岗位名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 题目总分数
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 题目总数
        /// </summary>
        public int Count { get; set; }
        public int Sort { get; set; } = 0;
    }
    /// <summary>
    /// AM统计总分数
    /// </summary>
    public class TopicQueryAMTotalScore
    {
        /// <summary>
        /// 出纳审核总分数
        /// </summary>
        public decimal DisableCashierScore { get; set; }
        /// <summary>
        /// 会计主管审核分数
        /// </summary>
        public decimal DisableAMScore { get; set; }
    }
}
