﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 题目基类
    /// </summary>
    public class BaseQuestion
    {
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime BusinessDate { get; set; }
        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 父节点ID-用于多题型(一个题目 多个作答区)
        /// </summary>
        public long ParentId { get; set; } = 0;
        /// <summary>
        /// 是否结账题（0 否 1是）
        /// </summary>
        public int IsSettlement { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }


        /// <summary>
        /// 题型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 题目内容类型(1:纯文字,2:表格,3:图片)
        /// </summary>
        public QuestionContentType QuestionContentType { get; set; }
        /// <summary>
        /// 题干内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 题目数据源内容(不含答案)
        /// </summary> 
        public string TopicContent { get; set; }

        /// <summary>
        /// 题目数据源内容(含答案)
        /// </summary>
        public string FullContent { get; set; }

        /// <summary>
        /// 题目难度
        /// </summary>
        public int? DifficultyLevel { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public virtual string AnswerValue { get; set; }
        /// <summary>
        /// 答案解析
        /// </summary>
        public virtual string Hint { get; set; }
        /// <summary>
        /// 师傅提示
        /// </summary>
        public string TeacherHint { get; set; }

        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 题目关联
        /// </summary>
        [Newtonsoft.Json.JsonProperty(NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public List<TopicRelation> TopicRelations { get; set; }

        /// <summary>
        /// 分岗所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PointPositionId { get; set; } = 0;

        /// <summary>
        /// 技能Id
        /// </summary>
        public long SkillId { get; set; }
    }
}
