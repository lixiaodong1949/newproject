﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 题目关联
    /// </summary>
    public class TopicRelation
    {
        /// <summary>
        /// 选择前置题目的题目Id
        /// </summary>
        public long TopicId { get; set; }
        /// <summary>
        /// 选择前置题目的父题目Id
        /// </summary>
        public long TopicParentId { get; set; }
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 关联的题目类型
        /// </summary>
        public int RelationTopicType { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        public bool IsShow { get; set; }
        //public long ParentTopicId { get; set; }
        //public List<long> RelationOptionIds { get; set; }
        //public List<string> RelationOptionNames { get; set; }
    }
}
