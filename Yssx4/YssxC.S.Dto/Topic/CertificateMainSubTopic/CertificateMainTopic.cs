﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 综合分录题 主题干
    /// </summary>
    public class CertificateMainTopic
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 总分值
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime BusinessDate { get; set; }

        /// <summary>
        /// 查看答案
        /// </summary>
        public bool IsShowAnswer { get; set; }
        /// <summary>
        /// 查看解析
        /// </summary>
        public bool IsShowHint { get; set; }
        /// <summary>
        /// 题目关联状态是否开启
        /// </summary>
        public bool TopicRelationStatus { get; set; }
        /// <summary>
        /// 题目关联
        /// </summary>
        public List<TopicRelation> TopicRelations { get; set; }
        /// <summary>
        /// 结账类型（0 转账 1现金）
        /// </summary>
        public int SettlementType { get; set; }
    }
}
