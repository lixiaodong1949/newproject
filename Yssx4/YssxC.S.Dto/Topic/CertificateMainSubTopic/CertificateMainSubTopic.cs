﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 综合分录题 
    /// </summary>
    public class CertificateMainSubTopic
    {
        /// <summary>
        /// 主题目内容
        /// </summary>
        public CertificateMainTopic MainTopic { get; set; }

        /// <summary>
        /// 业务票据
        /// </summary>
        public TeticketTopic TeticketTopic { get; set; }

        /// <summary>
        /// 记账凭证（分录题）
        /// </summary>
        public CertificateTopic CertificateTopic { get; set; }

        /// <summary>
        /// 收款题
        /// </summary>
        public List<ReceiptTopic> ReceiptTopics { get; set; }

        /// <summary>
        /// 付款题
        /// </summary>
        public List<PayTopic> PayTopics { get; set; }

    }
}
