﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 付款
    /// </summary>
   public class PayTopic
    {
        /// <summary>
        /// 唯一id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 分值
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public string AnswerValue { get; set; }
        /// <summary>
        /// 付款人银行Id
        /// </summary>
        public long PayBankId { get; set; }
        /// <summary>
        /// 收款单位（公司、个人）
        /// </summary>
        public long PayeeCompanyId { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 回执单
        /// </summary>
        public List<TopicFile> Files { get; set; }
        /// <summary>
        /// 分岗所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PointPositionId { get; set; } = 0;

        /// <summary>
        /// 技能Id
        /// </summary>
        public long SkillId { get; set; }
    }
}
    