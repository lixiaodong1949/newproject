﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 票据
    /// </summary>
    public class TeticketTopic
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 答案解析
        /// </summary>
        public virtual string Hint { get; set; }
        /// <summary>
        /// 师傅提示
        /// </summary>
        public string TeacherHint { get; set; }
        /// <summary>
        /// 题目数据源内容(不含答案)
        /// </summary> 
        public string TopicContent { get; set; }
        /// <summary>
        /// 题目数据源内容(含答案)
        /// </summary>
        public string FullContent { get; set; }
        /// <summary>
        /// 答案
        /// </summary>
        public string AnswerValue { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 附件
        /// </summary>
        public List<TopicFile> Files { get; set; }
        /// <summary>
        /// 推送时间
        /// </summary>
        public DateTime? PushDate { get; set; }
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 分岗所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PointPositionId { get; set; } = 0;

        /// <summary>
        /// 技能Id
        /// </summary>
        public long SkillId { get; set; }
    }
}
