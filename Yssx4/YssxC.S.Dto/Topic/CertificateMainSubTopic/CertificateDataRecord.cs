﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 单据数据记录
    /// </summary>
    public class CertificateDataRecord
    {
        /// <summary>
        /// 摘要信息
        /// </summary>
        public string SummaryInfo { get; set; }
        /// <summary>
        /// 科目Id
        /// </summary>
        public long SubjectId { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        public decimal CreditorAmount { get; set; }
    }
}
