﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 选择题，选项内容
    /// </summary>
    public class ChoiceOption
    {
        /// <summary>
        /// 选项ID
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 选项名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 选型内容
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// 附件图片--技能抽查用不到，其他竞赛要用到
        /// </summary>
        public string AttatchImgUrl { get; set; }
        /// <summary>
        /// 附件图片--技能抽查用不到，其他竞赛要用到
        /// </summary>
        public int Sort { get; set; }
    }
}
