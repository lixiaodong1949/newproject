﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 复杂题型
    /// </summary>
    public class ComplexQuestionRequest : BaseQuestion
    {
        /// <summary>
        /// 题目附件
        /// </summary>
        public List<TopicFile> QuestionFile { get; set; }
        /// <summary>
        /// 模板ID
        /// </summary>
        public string TemplateId { get; set; }
        ///// <summary>
        ///// 非完整性得分
        ///// </summary>
        //public decimal PartialScore { get; set; } = 0;
        /// <summary>
        /// 表格题-计算方式（按行计算、按列计算、按单元格平均计算、自由计算）
        /// </summary>
        public CalculationType CalculationType { get; set; } = CalculationType.None;
        /// <summary>
        /// 表格题-是否开启复制Excel数据（0 否 1是）
        /// </summary>
        public int IsCopy { get; set; } = CommonConstants.IsNoCopy;
        /// <summary>
        /// 表格题-是否开启复制大数据（0 否 1是）
        /// </summary>
        public int IsCopyBigData { get; set; } = CommonConstants.IsNoCopy;
        /// <summary>
        /// 表格题-是否无序（0 否 1是）
        /// </summary>
        public int IsDisorder { get; set; } = CommonConstants.IsNoDisorder;

        #region 财务表报题 专用
        /// <summary>
        /// 纳税申报报表分数 财务表报题
        /// </summary>
        public decimal DeclarationScore { get; set; }

        /// <summary>
        /// 纳税申报报表所属岗位 财务表报题
        /// </summary>
        public decimal DeclarationPositionId { get; set; }

        /// <summary>
        /// 纳税申报所属期起
        /// </summary>
        public DateTime? DeclarationDateStart { get; set; }
        /// <summary>
        /// 纳税申报所属期止
        /// </summary>
        public DateTime? DeclarationDateEnd { get; set; }
        /// <summary>
        /// 纳税申报期限
        /// </summary>
        public DateTime? DeclarationLimitDate { get; set; }

        #endregion

        #region 申报表题 专用

        /// <summary>
        /// 申报表Id 申报表题
        /// </summary>
        public long DeclarationId { get; set; }

        #endregion
    }
}
