﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 获取所有题目的名称（包含子题目）返回值
    /// </summary>
    public class TopicNamesResponseDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 父Id
        /// </summary>
        [JsonIgnore]
        public long ParentId { get; set; }
        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 子题目
        /// </summary>

        public List<TopicNamesSubResponseDto> Subs { get; set; }
    }
    /// <summary>
    /// 获取所有题目的名称（包含子题目）返回值
    /// </summary>
    public class TopicNamesSubResponseDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 父Id
        /// </summary>
        public long ParentId { get; set; }
        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public string QuestionTypeName { get; set; }
    }
}
