﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 保存修改师傅评语入参类（查询）输出类
    /// </summary>
    public class MastersCommentsDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 详情
        /// </summary>
        public string Details { get; set; }

        /// <summary>
        /// 最小分数
        /// </summary>
        public decimal MinScore { get; set; }
        /// <summary>
        /// 最大分数
        /// </summary>
        public decimal MaxScore { get; set; }
    }

    /// <summary>
    /// 师傅评语入参类
    /// </summary>
    public class MastersCommentsRequest : PageRequest
    {
        /// <summary>
        /// 详情
        /// </summary>
        public string Details { get; set; }
    }
}
