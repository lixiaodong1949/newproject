﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 保存修改行业入参类（查询）输出类
    /// </summary>
    public class IndustryDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 地图
        /// </summary>
        public string Map { get; set; }

        /// <summary>
        /// X坐标
        /// </summary>
        public int Xcoordinate { get; set; }

        /// <summary>
        /// Y坐标
        /// </summary>
        public int Ycoordinate { get; set; }

        /// <summary>
        /// 描述视频
        /// </summary>
        public string VideoUrl { get; set; }

        /// <summary>
        /// 简介
        /// </summary>
        public string Synopsis { get; set; }

        /// <summary>
        /// 介绍
        /// </summary>
        public string Introduce { get; set; }

        /// <summary>
        /// 状态 0开启1关闭
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// 地图logo
        /// </summary>
        public string MapLogo3D { get; set; }
    }

    /// <summary>
    /// 行业入参类
    /// </summary>
    public class IndustryRequest : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 主键Id (修改专用?=Id其他不用传)
        /// </summary>
        public long Id { get; set; }
    }
}
