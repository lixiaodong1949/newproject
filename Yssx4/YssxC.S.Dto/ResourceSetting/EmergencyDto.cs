﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{

    #region 突发事件
    /// <summary>
    /// 查询输出类
    /// </summary>
    public class EmergencyDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 类别名称
        /// </summary>
        public string Classification { get; set; }

        /// <summary>
        /// 类别标记 只是显示字段，如果改数字要在数据库改
        /// </summary>
        public int CategoryMark { get; set; }

        /// <summary>
        /// 突发详情数量
        /// </summary>
        public int DetailsNumber { get; set; }

        /// <summary>
        /// 状态 0开启1关闭
        /// </summary>
        public int Status { get; set; }
    }

    /// <summary>
    /// 保存修改突发入参类
    /// </summary>
    public class EmergencyAddOrEditDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 类别名称
        /// </summary>
        public string Classification { get; set; }

        /// <summary>
        /// 状态 0开启1关闭
        /// </summary>
        public int Status { get; set; }
    }

    /// <summary>
    /// 突发入参类
    /// </summary>
    public class EmergencyRequest : PageRequest
    {
        /// <summary>
        /// 类别名称
        /// </summary>
        public string Classification { get; set; }
    }
    #endregion

    #region 突发详情
    /// <summary>
    /// 保存修改突发详情入参类（查询）输出类
    /// </summary>
    public class EmergencyDetailsDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 租户Id（突发类别Id）
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 突发详情
        /// </summary>
        public string Details { get; set; }

        /// <summary>
        /// 图片
        /// </summary>
        public string ImgUrl { get; set; }

        /// <summary>
        /// 0开启 1关闭
        /// </summary>
        public int Status { get; set; }
    }

    /// <summary>
    /// 突发详情入参类
    /// </summary>
    public class EmergencyDetailsRequest : PageRequest
    {
        /// <summary>
        /// 租户Id（突发事件Id）
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 突发详情
        /// </summary>
        public string Details { get; set; }
    }
    #endregion
}
