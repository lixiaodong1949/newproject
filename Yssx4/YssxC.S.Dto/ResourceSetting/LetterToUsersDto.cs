﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 致用户信
    /// </summary>
    public class LetterToUsersDto
    {
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }

    }
}
