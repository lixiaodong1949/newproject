﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{

    #region 理由分类
    /// <summary>
    /// 查询输出类
    /// </summary>
    public class ReasonDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string Classification { get; set; }

        /// <summary>
        /// 理由详情数量
        /// </summary>
        public int DetailsNumber { get; set; }
    }

    /// <summary>
    /// 保存修改理由入参类
    /// </summary>
    public class ReasonAddOrEditDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        public string Classification { get; set; }
    }

    /// <summary>
    /// 理由入参类
    /// </summary>
    public class ReasonRequest : PageRequest
    {
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Classification { get; set; }
    }
    #endregion

    #region 理由详情
    /// <summary>
    /// 保存修改理由详情入参类（查询）输出类
    /// </summary>
    public class ReasonDetailsDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 租户Id（理由分类Id）
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 理由详情
        /// </summary>
        public string Details { get; set; }
    }

    /// <summary>
    /// 理由详情入参类
    /// </summary>
    public class ReasonDetailsRequest : PageRequest
    {
        /// <summary>
        /// 租户Id（理由分类Id）
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 理由详情
        /// </summary>
        public string Details { get; set; }
    }
    #endregion
}
