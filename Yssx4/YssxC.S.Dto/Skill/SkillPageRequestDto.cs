using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 能力维度分页查询 请求Dto
    /// </summary>
    public class SkillPageRequestDto : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}