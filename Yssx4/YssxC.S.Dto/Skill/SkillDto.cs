using System;
using System.ComponentModel.DataAnnotations;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 能力维度 Dto
    /// </summary>
    public class SkillDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>        
        [Required(ErrorMessage ="名称[Name]不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 最大值
        /// </summary>
        public int Max { get; set; }
    }
}