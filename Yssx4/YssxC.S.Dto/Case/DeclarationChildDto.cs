﻿namespace YssxC.S.Dto
{
    /// <summary>
    /// 申报表子表
    /// </summary>
    public class DeclarationChildDto
    {
        /// <summary>
        /// 申报表子表id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 子表名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 报表内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 分值
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 主表id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 答案
        /// </summary>
        public string AnswerValue { get; set; }

        /// <summary>
        /// 3:单元格计分  4:自由计分
        /// </summary>
        public int CalculationType { get; set; }

        /// <summary>
        /// 报表内容包含答案
        /// </summary>
        public string FullContent { get; set; }

        /// <summary>
        /// 表头内容
        /// </summary>
        public string HeaderContent { get; set; }
        /// <summary>
        /// 是否已作答
        /// </summary>
        public bool IsAnswered { get; set; }
    }

    public class GetDeclarationChildDto
    {
        /// <summary>
        /// 主表id
        /// </summary>
        public long ParentId { get; set; }
        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }
    }
}
