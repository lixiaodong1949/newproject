﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 查询企业列表
    /// </summary>
    public class GetCaseListByNameDto
    {
        /// <summary>
        /// 主键，公司Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 序号（标号）
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsActive { get; set; }


        /// <summary>
        /// 企业 - 月度任务
        /// </summary>
        public List<CaseInfoMonthDetail> MonthDetail { get; set; }

    }
}
