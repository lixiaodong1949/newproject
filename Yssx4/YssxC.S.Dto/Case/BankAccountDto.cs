﻿using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    public class BankAccountDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 开户行
        /// </summary>
        public string OpeningBank { get; set; }

        /// <summary>
        /// 支行
        /// </summary>
        public string Branch { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        public string AccountNo { get; set; }

        /// <summary>
        /// 余额
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 设置默认账户
        /// </summary>
        public bool Default { get; set; }
    }

    public class GetBankAccountInput : PageRequest
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
    }
}
