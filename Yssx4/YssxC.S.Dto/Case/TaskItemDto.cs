﻿using System;

namespace YssxC.S.Dto
{
    public class TaskItemDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 任务日期
        /// </summary>
        public DateTime IssueDate { get; set; }

        /// <summary>
        /// 任务描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 外键，任务Id
        /// </summary>
        public long TaskId { get; set; }
    }
}
