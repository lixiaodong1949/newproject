﻿using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    public class AbstractDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 摘要名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
    }

    public class GetAbstractInput : PageRequest
    {
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
    }
}
