﻿using System.Collections.Generic;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    public class CaseDto
    {
        /// <summary>
        /// 主键，公司Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 公司简称
        /// </summary>
        public string BriefName { get; set; }

        /// <summary>
        /// 外键，行业Id
        /// </summary>
        public long IndustryId { get; set; }

        /// <summary>
        /// 会计准则 0:小企业 1：企业 2：事业单位 3：非营利组织
        /// </summary>
        public AccountingStandard AccountingStandard { get; set; }

        /// <summary>
        /// 公司规模
        /// </summary>
        public string Scale { get; set; }

        /// <summary>
        /// 纳税主体
        /// </summary>
        public Taxpayer Taxpayer { get; set; }

        /// <summary>
        /// 法定代表人
        /// </summary>
        public string LegalRepresentative { get; set; }

        /// <summary>
        /// 统一社会信用代码
        /// </summary>
        public string UniformCode { get; set; }

        /// <summary>
        /// 注册时间
        /// </summary>
        public string RegisteredTime { get; set; }

        /// <summary>
        /// 注册资本
        /// </summary>
        public string RegisteredCapital { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 公司地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// X 坐标
        /// </summary>
        public int XCoordinate { get; set; }

        /// <summary>
        /// Y 坐标
        /// </summary>
        public int YCoordinate { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// 公司描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 经营范围
        /// </summary>
        public string BusinessScope { get; set; }

        /// <summary>
        /// 组织结构
        /// </summary>
        public string Organization { get; set; }

        /// <summary>
        /// 自定义数据
        /// </summary>
        public string CustomData { get; set; }

        /// <summary>
        /// 实习模式
        /// </summary>
        public PracticePattern PracticePattern { get; set; }

        /// <summary>
        /// 游戏实习模式
        /// </summary>
        public PracticePattern GamePracticePattern { get; set; }

        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountantOfficer { get; set; }

        /// <summary>
        /// 出纳
        /// </summary>
        public string Cashier { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        public string Auditor { get; set; }

        /// <summary>
        /// 业务会计
        /// </summary>
        public string BusinessAccountant { get; set; }

        /// <summary>
        /// 税务会计
        /// </summary>
        public string TaxAccountant { get; set; }

        /// <summary>
        /// 成本会计
        /// </summary>
        public string CostAccountant { get; set; }

        /// <summary>
        /// 难度系数
        /// </summary>
        public int Rate { get; set; }

        /// <summary>
        /// 公司logo
        /// </summary>
        public string LogoUrl { get; set; }

        /// <summary>
        /// 推荐语
        /// </summary>
        public string RecommendedLanguage { get; set; }


        /// <summary>
        /// 真帐描述
        /// </summary>
        public string RealDescription { get; set; }

        /// <summary>
        /// 描述视频
        /// </summary>
        public string VideoUrl { get; set; }

        /// <summary>
        /// 总分，题目管理中的总分+申报表总分计算所得
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 序号（标号）
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 标签
        /// </summary>
        public List<TagDto> TagDtos { get; set; }

        /// <summary>
        /// 岗位
        /// </summary>
        public List<PositionManageDto> PositionDtos { get; set; }

        /// <summary>
        /// 登记注册类型
        /// </summary>
        public string RegisterType { get; set; }
        /// <summary>
        /// 背景图
        /// </summary>
        public string BackgroundImage { get; set; }

        /// <summary>
        /// 游戏资源配置
        /// </summary>
        public List<GameResourceConfigDto> GameResources { get; set; }  

    }

    public class GetCaseInput : PageRequest
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 行业Id
        /// </summary>
        public long? IndustryId { get; set; }

        /// <summary>
        /// 公司规模
        /// </summary>
        public string Scale { get; set; }

        /// <summary>
        /// 纳税主体 0:一般纳税人   1:小规模纳税人
        /// </summary>
        public Taxpayer? Taxpayer { get; set; }
    }
}
