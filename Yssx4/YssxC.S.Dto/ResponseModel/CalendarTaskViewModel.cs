﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回日历任务实体
    /// </summary>
    public class CalendarTaskViewModel
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string TaskName { get; set; }

        /// <summary>
        /// 任务日期
        /// </summary>
        public DateTime TaskDate { get; set; }

        /// <summary>
        /// 审票模式 0:日 1:周 2:月
        /// </summary>
        public int ReviewPattern { get; set; }

        /// <summary>
        /// 日期列表
        /// </summary>
        public List<CalendarDateModel> CalendarDateList { get; set; }
    }

    /// <summary>
    /// 日期列表
    /// </summary>
    public class CalendarDateModel
    {
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// 工作描述
        /// </summary>
        public string JobDescription { get; set; }

        /// <summary>
        /// 工作状态 0:未完成 1:已完成
        /// </summary>
        public int JobStatus { get; set; }
    }

}
