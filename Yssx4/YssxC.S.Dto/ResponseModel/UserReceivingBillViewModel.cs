﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回用户收付款票据列表实体
    /// </summary>
    public class UserReceivingBillViewModel
    {
        /// <summary>
        /// 父级题目Id
        /// </summary>
        public long ParentQuestionId { get; set; }

        /// <summary>
        /// 题目名称
        /// </summary>
        public string TopicName { get; set; }

        /// <summary>
        /// 题目类型 1:付款题 2:收款题
        /// </summary>
        public int TopicType { get; set; }

        /// <summary>
        /// 票据回单详情列表
        /// </summary>
        public List<BillReceiptFileViewModel> BillReceiptFileList { get; set; } = new List<BillReceiptFileViewModel>();
    }

    /// <summary>
    /// 票据回单详情实体
    /// </summary>
    public class BillReceiptFileViewModel
    {
        /// <summary>
        /// 图片路径
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 封面底面：1:封面 2:底面
        /// </summary>        
        public Nullable<int> CoverBack { get; set; }

        ///// <summary>
        ///// 类型 0:票据 1:回单
        ///// </summary>
        //public int FileType { get; set; }
    }
}
