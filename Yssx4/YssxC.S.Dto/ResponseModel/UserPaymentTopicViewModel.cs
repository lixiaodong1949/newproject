﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回付款题作答后实体
    /// </summary>
    public class UserPaymentTopicViewModel
    {
        /// <summary>
        /// 作答是否正确 false:错误 true:正确
        /// </summary>
        public bool IsRightAnswers { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 正确金额
        /// </summary>
        public decimal RightAmount { get; set; }

        /// <summary>
        /// 正确收款单位
        /// </summary>
        public string ReceivingBankName { get; set; }

        /// <summary>
        /// 回单
        /// </summary>
        public string ReceiptUrl { get; set; }

        /// <summary>
        /// 错误文案
        /// </summary>
        public string ErrorWriting { get; set; }

        /// <summary>
        /// 文案图片
        /// </summary>
        public string ImgUrl { get; set; }
    }
}
