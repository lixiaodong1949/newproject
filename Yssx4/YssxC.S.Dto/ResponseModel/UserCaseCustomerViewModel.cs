﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回用户公司客户列表实体
    /// </summary>
    public class UserCaseCustomerViewModel
    {
        /// <summary>
        /// 客户Id
        /// </summary>
        public long CustomerId { get; set; }
        
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 类别(0:供应商 1:职员 2:客户 3:部门 4:项目)
        /// </summary>        
        public Nullable<int> Type { get; set; }

        /// <summary>
        /// 账户
        /// </summary>
        public string BankAccountNo { get; set; }

        /// <summary>
        /// 开户行名称
        /// </summary>
        public string BankName { get; set; }

        /// <summary>
        /// 支行名称
        /// </summary>
        public string BankBranchName { get; set; }
    }
}
