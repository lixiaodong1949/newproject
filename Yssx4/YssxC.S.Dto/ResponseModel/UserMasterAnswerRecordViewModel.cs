﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回用户创建主作答记录实体
    /// </summary>
    public class UserMasterAnswerRecordViewModel
    {
        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 账套创建状态 0:不需要创建 1:未创建 2:已创建
        /// </summary>
        public int CreateSetBookStatus { get; set; }
    }
}
