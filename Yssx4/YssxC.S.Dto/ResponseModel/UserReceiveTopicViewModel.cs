﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回收款题作答后实体
    /// </summary>
    public class UserReceiveTopicViewModel
    {
        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }
        
        /// <summary>
        /// 回单
        /// </summary>
        public string ReceiptUrl { get; set; }
    }
}
