﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回付款题列表实体
    /// </summary>
    public class PaymentTopicInfoViewModel
    {
        /// <summary>
        /// 付款题目Id
        /// </summary>
        public long PaymentQuestionId { get; set; }

        /// <summary>
        /// 收款单位Id
        /// </summary>
        public long ReceivingBankId { get; set; }

        /// <summary>
        /// 类别(0:供应商 1:职员 2:客户 3:部门 4:项目)
        /// </summary>        
        public Nullable<int> Type { get; set; }

        /// <summary>
        /// 收款单位名称
        /// </summary>
        public string ReceivingBankName { get; set; }

        /// <summary>
        /// 收款开户行
        /// </summary>
        public string ReceivingOpenBank { get; set; }

        /// <summary>
        /// 收款支行
        /// </summary>
        public string ReceivingBranchBank { get; set; }

        /// <summary>
        /// 收款账号
        /// </summary>
        public string ReceivingAccount { get; set; }

        /// <summary>
        /// 付款银行
        /// </summary>
        public string PaymentOpenBank { get; set; }

        /// <summary>
        /// 付款支行
        /// </summary>
        public string PaymentSubBank { get; set; }

        /// <summary>
        /// 付款账号
        /// </summary>
        public string PaymentAccount { get; set; }

        /// <summary>
        /// 付款账号余额
        /// </summary>
        public decimal PaymentAccountBalance { get; set; }

        /// <summary>
        /// 回执单
        /// </summary>
        public string ReceiptUrl { get; set; }

        /// <summary>
        /// 是否已付款 false:未付 true:已付
        /// </summary>
        public bool IsPaid { get; set; }
    }
}
