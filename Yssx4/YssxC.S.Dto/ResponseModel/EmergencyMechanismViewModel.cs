﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回突发机制结果实体
    /// </summary>
    public class EmergencyMechanismViewModel
    {
        /// <summary>
        /// 文案
        /// </summary>
        public string Details { get; set; }

        /// <summary>
        /// 文案图片
        /// </summary>
        public string ImgUrl { get; set; }
    }
}
