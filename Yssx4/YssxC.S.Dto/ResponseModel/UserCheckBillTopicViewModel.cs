﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回用户审核票据题结果实体
    /// </summary>
    public class UserCheckBillTopicViewModel
    {
        /// <summary>
        /// 作答是否正确 false:错误 true:正确
        /// </summary>
        public bool IsRightAnswers { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 票据类型 0:非干扰票 1:干扰票 2:推送票据
        /// </summary>
        public int BillTopicType { get; set; }

        /// <summary>
        /// 正确答案(干扰票使用)
        /// </summary>
        public string RightAnswers { get; set; }

        /// <summary>
        /// 用户作答答案
        /// </summary>
        public string UserAnswer { get; set; }

        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime BusinessDate { get; set; }

        /// <summary>
        /// 推送日期(干扰票使用)
        /// </summary>
        public Nullable<DateTime> PushDate { get; set; }

        /// <summary>
        /// 是否即刻推送(干扰票使用) false:不是 true:是
        /// </summary>
        public bool IsRightAwayPush { get; set; }

        /// <summary>
        /// 错误文案(干扰票使用)
        /// </summary>
        public string ErrorWriting { get; set; }

        /// <summary>
        /// 文案图片(干扰票使用)
        /// </summary>
        public string ImgUrl { get; set; }

    }
}
