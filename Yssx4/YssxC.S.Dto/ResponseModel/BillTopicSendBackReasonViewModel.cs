﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回票据题退回理由实体
    /// </summary>
    public class BillTopicSendBackReasonViewModel
    {
        /// <summary>
        /// 退回原因Id
        /// </summary>
        public long SendBackReasonId { get; set; }
        
        /// <summary>
        /// 退回原因详情
        /// </summary>
        public string SendBackDetail { get; set; }
    }
}
