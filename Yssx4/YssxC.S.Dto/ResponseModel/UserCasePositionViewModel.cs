﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 返回用户公司岗位列表实体
    /// </summary>
    public class UserCasePositionViewModel
    {
        /// <summary>
        /// 岗位Id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PositionName { get; set; }

    }
}
