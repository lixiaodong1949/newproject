﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 关卡提交
    /// </summary>
    public class SubmitGameExamRequestDto
    {
        /// <summary>
        /// 作答记录id
        /// </summary>
        [Required]
        public long GradeId { get; set; }
        /// <summary>
        /// 用户id
        /// </summary>
        //public long UserId { get; set; }
        /// <summary>
        /// 关卡Id
        /// </summary>
        [Required]
        public long GameLevelId { get; set; }
        /// <summary>
        /// 关卡作答主记录Id
        /// </summary>
        public long GameGradeId { get; set; }
        ///// <summary>
        ///// 是否结束游戏
        ///// </summary>
        //public bool GameOver { get; set; }
        /// <summary>
        /// 是否自动提交
        /// </summary>
        public bool IsAutoSubmit { get; set; }
    }
}
