using System;
using System.ComponentModel.DataAnnotations;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏关卡题目 Dto
    /// </summary>
    public class GameTopicDto
    {
        /// <summary>
        /// 关卡Id
        /// </summary>
        [Required]
        [Range(0, long.MaxValue)]
        public long GameLevelId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        [Required]
        public long[] TopicIds { get; set; }

    }
}