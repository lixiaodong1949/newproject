﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    public class PositionAndQuestionType
    {
        public long PointPositionId { get; set; }
        public QuestionType QuestionType { get; set; }
    }

    /// <summary>
    /// 用户作答记录
    /// </summary>
    public class UserGameLevelGrade
    {
        /// <summary>
        /// 关卡作答主记录Id
        /// </summary>
        public long GameGradeId { get; set; }
        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 是否需要创建账套
        /// </summary>
        public bool IsCreateSetBook { get; set; }

        /// <summary>
        /// 作答使用时间
        /// </summary>
        public double UsedSeconds { get; set; }

        /// <summary>
        /// 关卡题目节点信息（包含 该节点首次作答的开始操作岗位，由于分录节点有多个步骤）
        /// </summary>
        public List<GameLevelTopicNodeDto> TopicNodes { get; set; }

        /// <summary>
        /// 用户当前节点 (为NULL 表示所有任务都已经做完)
        /// </summary>
        public List<GameLevelTopicNodeDto> UserCurrentNodes { get; set; }

    }
    /// <summary>
    /// 关卡题目节点信息
    /// </summary>
    public class GameLevelTopicNodeDto
    {
        /// <summary>
        /// 用户当前闯关的节点 1审票 2收付款 3分录 出纳审核 主管审核  4 结转损益 5结账 100其他
        /// </summary>
        public GameTopicNode TopicNode { get; set; }
        /// <summary>
        /// 拥有的题目类型(不能使用该字段)
        /// </summary>
        [JsonIgnore]
        public List<QuestionType> QuestionTypes { get; set; } = new List<QuestionType>();
        /// <summary>
        /// 题目节点下拥有的岗位id
        /// </summary>
        public List<PositionIdAndOperation> PositionIds { get; set; } = new List<PositionIdAndOperation>();
    }

    public class PositionIdAndOperation
    {
        /// <summary>
        /// 题目节点下拥有的岗位id
        /// </summary>
        public long PositionId { get; set; }

        /// <summary>
        /// 类型：0 作答 1 仅修改
        /// </summary>
        public int Type { get; set; } = 0;

        public QuestionType QuestionType { get; set; }

    }

    public class GameLevelQuestionBaseDto
    {
        /// <summary>
        /// 
        /// </summary>
        public long TopicId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsHasReceivePayment { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SettlementType { get; set; }

        /// <summary>
        /// 分录题状态
        /// </summary>
        public AccountEntryStatus AccountEntryStatus { get; set; }

        /// <summary>
        /// 分录题审核状态
        /// </summary>
        public AccountEntryAuditStatus AccountEntryAuditStatus { get; set; }
        /// <summary>
        ///  
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
