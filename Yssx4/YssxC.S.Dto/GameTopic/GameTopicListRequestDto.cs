using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏关卡题目 请求Dto
    /// </summary>
    public class GameTopicListRequestDto
    {
        /// <summary>
        /// 关卡Id
        /// </summary>
        [Required]
        public long GameLevelId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string TopicName { get; set; }
    }
}