using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏关卡题目 请求Dto
    /// </summary>
    public class UserGameTopicListRequestDto
    {
        /// <summary>
        /// 关卡Id
        /// </summary>
        [Required]
        public long GameLevelId { get; set; }
        /// <summary>
        /// 岗位Id
        /// </summary>
        [Required]
        public long? PositionId { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 票据状态 0:未审核 1:已审核 2:已退回
        /// </summary>
        public int BillStatus { get; set; }
    }
}