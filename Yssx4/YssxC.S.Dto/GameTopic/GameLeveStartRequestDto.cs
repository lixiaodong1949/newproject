using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 开始闯关 Dto
    /// </summary>
    public class GameLeveStartRequestDto
    {
        ///// <summary>
        ///// 作答记录id
        ///// </summary>
        //[Required]
        //[Range(1, long.MaxValue)]
        //public long GradeId { get; set; }

        /// <summary>
        /// 关卡Id
        /// </summary>
        [Required]
        [Range(1, long.MaxValue)]
        public long GameLevelId { get; set; }


        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 是否购买过 false:没有购买 true:购买过
        /// </summary>
        public bool IsBought { get; set; }

        ///// <summary>
        ///// 平台：0：网页版 ，1：3D版,兼容以前网页版系统必须设置默认值0
        ///// </summary>
        //public int Platform { get; set; } = 0;

        ///// <summary>
        ///// 是否重新闯关
        ///// </summary>
        //public bool IsNew { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GameExamPaperDto
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 是否建账套
        /// </summary>
        public bool IsCreateSetBook { get; set; }
        /// <summary>
        /// 总分
        /// </summary>
        public decimal TotalScore { get; set; }


    }
}