using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏关卡题目 返回值Dto
    /// </summary>
    public class GameTopicListResponseDto
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 任务id
        /// </summary>
        public long TaskId { get; set; }

        /// <summary>
        /// 关卡Id
        /// </summary>
        public long GameLevelId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long TopicId { get; set; }

        /// <summary>
        /// 题目名称
        /// </summary>
        public string TopicTitle { get; set; }

        /// <summary>
        /// 分岗所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PointPositionId { get; set; }

        /// <summary>
        /// 积分
        /// </summary>
        public int Point { get; set; }

        /// <summary>
        /// 题目排序
        /// </summary>
        public int Sort { get; set; }
    }
}