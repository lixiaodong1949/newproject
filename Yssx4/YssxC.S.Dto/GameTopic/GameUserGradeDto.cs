﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
   public class GameUserGradeDto
    {
        ///// <summary>
        ///// 案例Id
        ///// </summary>
        //public long CaseId { get; set; }

        ///// <summary>
        ///// 任务id
        ///// </summary>
        //public long TaskId { get; set; }
        /// <summary>
        /// 关卡作答主记录Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 关卡Id
        /// </summary>
        public long GameLevelId { get; set; }
        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 用时 单位：s
        /// </summary>
        public double UsedSeconds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
