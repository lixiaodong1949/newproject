using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 获取任务中可选的题目列表 Dto
    /// </summary>
    public class GameChoosableTopicDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }
        ///// <summary>
        ///// 父Id
        ///// </summary>
        //public long ParentId { get; set; }
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime BusinessDate { get; set; }
        /// <summary>
        /// 题目分组Id
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 所属岗位（此处数据来源于案例表所选的岗位集合）
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 所属岗位名称
        /// </summary>  
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string PositionName { get; set; }
        /// <summary>
        /// 分数
        /// </summary>
        public decimal Score { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        public QuestionType QuestionType { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 已选择
        /// </summary>
        public bool IsSelected { get; set; }
    }
}