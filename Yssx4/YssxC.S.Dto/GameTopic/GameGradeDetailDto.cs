﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    public class GameGradeDetailDto
    {
        public long QuestionId { get; set; }
        public QuestionType QuestionType { get; set; }
        public AnswerResultStatus Status { get; set; }
        public decimal Score { get; set; }
        public long SkillId { get; set; }
    }
}
