﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 实习分录题查询入参
    /// </summary>
    public class GameOtherTopicRequest
    {
        /// <summary>
        /// 关卡Id
        /// </summary>
        [Required]
        public long GameLevelId { get; set; }
        /// <summary>
        /// 作答记录id
        /// </summary>
        [Required]
        public long GradeId { get; set; }
        /// <summary>
        /// 岗位id
        /// </summary>
        [Required]
        public long PositionId { get; set; }
        /// <summary>
        /// 纳税申报Id
        /// </summary>
        public long DeclarationId { get; set; }
        /// <summary>
        /// 题目类型
        /// </summary>
        [Required]
        public QuestionType QuestionType { get; set; }
    }
}
