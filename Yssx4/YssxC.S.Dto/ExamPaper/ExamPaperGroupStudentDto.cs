﻿namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 
    /// </summary>
    public class ExamPaperGroupStudentDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 分组Id
        /// </summary>
        public long GroupId { get; set; }

        /// <summary>
        /// 岗位Id
        /// </summary>
        public long PostionId { get; set; }

        /// <summary>
        /// 成绩主表id，如果为0，表示组团还未成功 
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 组员ID
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// tenantID
        /// </summary>
        public long TenantId { get; set; }

        /// <summary>
        /// 试卷总时长
        /// </summary>
        public int TotalTime { get; set; }

        /// <summary>
        /// 岗位考试时长
        /// </summary>
        public int TimeMinutes { get; set; }

        
    }
}
