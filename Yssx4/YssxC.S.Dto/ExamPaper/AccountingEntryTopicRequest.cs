﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 实习分录题查询入参
    /// </summary>
    public class AccountingEntryTopicRequest : PageRequest
    {
        /// <summary>
        /// 开始业务日期
        /// </summary>
        public DateTime StartDate { get; set; } = DateTime.Now;
        /// <summary>
        /// 结束业务日期
        /// </summary>
        public DateTime EndDate { get; set; } = DateTime.Now;
        /// <summary>
        /// 作答记录id
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 岗位id
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 关键字查询（凭证号或者业务名称）
        /// </summary>
        public string KeyWords { get; set; }

    }
    /// <summary>
    /// 实习分录题查询入参
    /// </summary>
    public class GameAccountingEntryTopicRequest : PageRequest
    {
        /// <summary>
        /// 作答记录id
        /// </summary>
        public long GradeId { get; set; }
        ///// <summary>
        ///// 岗位id
        ///// </summary>
        //public long PositionId { get; set; }
        /// <summary>
        /// 关键字查询（凭证号或者业务名称）
        /// </summary>
        public string KeyWords { get; set; }

        public int AuditType { get; set; } = -1;

    }
}
