﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 其他题目信息
    /// </summary>
    public class ExamOtherQuestionInfo
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目岗位Id
        /// </summary>
        public long PositionId { get; set; }
        /// <summary>
        /// 业务名称
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 师傅提示
        /// </summary>
        public string TeacherHint { get; set; }

        /// <summary>
        /// 纳税申报所属期起
        /// </summary>
        public DateTime? DeclarationDateStart { get; set; }
        /// <summary>
        /// 纳税申报所属期止
        /// </summary>
        public DateTime? DeclarationDateEnd { get; set; }
        /// <summary>
        /// 纳税申报期限
        /// </summary>
        public DateTime? DeclarationLimitDate { get; set; }
    }
}
