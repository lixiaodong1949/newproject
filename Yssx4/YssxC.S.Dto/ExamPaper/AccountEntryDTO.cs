﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 岗位核心题：录入凭证数据(查看账簿需要使用此数据)
    /// </summary>
    public class AccountEntryDTO
    {
        /// <summary>
        /// 凭证号
        /// </summary>
        public string CertificateNo { get; set; }

        /// <summary>
        /// 摘要信息
        /// </summary>
        public string SummaryInfo { get; set; }

        /// <summary>
        /// 科目Id
        /// </summary>
        public long SubjectId { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        public decimal CreditorAmount { get; set; }

        /// <summary>
        /// 凭证日期
        /// </summary>
        public DateTime? CertificateDate { get; set; }
        /// <summary>
        /// 辅助核算ID
        /// </summary>
        public long AssistId { get; set; }
        
    }
}
