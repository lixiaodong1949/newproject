﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Yssx.Framework;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 试卷基本信息 
    /// </summary>
    public class ExamPaperBasicInfo
    {
        #region 考试基本信息

        /// <summary>
        /// 考试ID
        /// </summary>
        public long ExamId { get; set; }

        ///// <summary>
        ///// 卷面形式（可空，0核算会计 1 方法会计）
        ///// </summary>
        //public int? RollUpType { get; set; }

        /// <summary>
        /// 作答记录ID
        /// </summary>
        public long GradeId { get; set; }

        ///// <summary>
        ///// 企业信息 
        ///// </summary>
        //public string EnterpriseInfo { get; set; }

        /// <summary>
        /// 学生作答状态
        /// </summary>
        public StudentExamStatus StudentExamStatus { get; set; }

        /// <summary>
        /// 考试总分
        /// </summary>
        public decimal ExamScore { get; set; }

        /// <summary>
        /// 合格分数
        /// </summary>
        public decimal PassScore { get; set; } = 60;

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 考试剩余时长
        /// </summary>
        public long LeftSeconds { get; set; }

        /// <summary>
        /// 作答总用时(秒)
        /// </summary>
        public long UsedSeconds { get; set; }

        #endregion

        ///// <summary>
        ///// 学生信息
        ///// </summary>
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        //public StudentInfo StudentInfo { get; set; }

        /// <summary>
        /// 当前用户的组员信息
        /// </summary>
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        //public List<ExamPaperGroupStudentView> ExamPaperGroupStudents { get; set; }

        /// <summary>
        /// 学生成绩信息
        /// </summary>
        public StudentGradeInfo StudentGradeInfo { get; set; }

        /// <summary>
        /// 业务题 （综合分录题、单选题、多选题、判断题、表格题、填空题、结账题）
        /// </summary>
        public List<BusinessQuestionInfo> BusinessTopics { get; set; }
        /// <summary>
        /// 报表题（财务分析题、财务报表题、结转损益题）
        /// </summary>
        public List<ExamRecordQuestionInfo> FinancialTopics { get; set; }
        /// <summary>
        /// 纳税申报表题（申报表题、财务报表申报题）
        /// </summary>
        public List<ExamRecordQuestionInfo> DeclarationTpoics { get; set; }
        /// <summary>
        /// 案例信息
        /// </summary>
        public ExamCaseInfo CaseInfo { get; set; }
    }

    /// <summary>
    /// 业务题
    /// </summary>
    public class BusinessQuestionInfo
    {
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime BusinessDate { get; set; }
        /// <summary>
        /// 试卷题目信息
        /// </summary>
        public List<ExamRecordQuestionInfo> QuestionInfos { get; set; }
    }

    /// <summary>
    /// 案例信息
    /// </summary>
    public class ExamCaseInfo
    {
        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }
        /// <summary>
        /// 所属行业名称
        /// </summary>
        public string IndustryName { get; set; }
    }

    /// <summary>
    /// 考试作答记录的问题信息
    /// </summary>
    public class ExamRecordQuestionInfo
    {
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime BusinessDate { get; set; }
        /// <summary>
        /// 题目ID
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 父题目ID
        /// </summary>
        public long ParentQuestionId { get; set; }

        ///// <summary>
        ///// 岗位Id
        ///// </summary>
        //public long PositionId { get; set; }

        ///// <summary>
        ///// 岗位名称 
        ///// </summary>
        //public string PositionName { get; set; }

        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }

        ///// <summary>
        ///// 是否属于当前答题人的题目（团队赛题型专用）
        ///// </summary>
        //public bool IsOwner { get; set; } = true;

        ///// <summary>
        ///// 是否标记
        ///// </summary>
        //[Obsolete]
        //[JsonIgnore]
        //public bool IsMark { get; set; }

        /// <summary>
        /// 题目类型:0单选题,1多选题，2判断题，3表格题，4分录题，5填空题，6图表题，7综合题，8岗位核心技能，9表格填空题
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 作答状态(0未作答,1已作答,2答对，3答错 4部分正确)
        /// </summary>

        public AnswerDTOStatus QuestionAnswerStatus { get; set; }

        ///// <summary>
        ///// 分录题状态
        ///// </summary>
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        //public List<Tuple<long, AccountEntryStatus, AccountEntryAuditStatus, int>> AccountEntryStatusDic { get; set; }

        /// <summary>
        /// 分值
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 答题得分
        /// </summary>
        public decimal AnswerScore { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }
        /// <summary>
        /// 纳税申报表Id
        /// </summary>
        public long DeclarationId { get; set; }
        /// <summary>
        /// 纳税申报表父Id
        /// </summary>
        public long ParentDeclarationId { get; set; }

    }
}
