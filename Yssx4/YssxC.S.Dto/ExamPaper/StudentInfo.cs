﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 学生信息
    /// </summary>
    public class StudentInfo
    {
        /// <summary>
        /// 学号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 名称
        /// </summary> 
        public string Name { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary> 
        public string SchoolName { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IdNumber { get; set; }
    }
}
