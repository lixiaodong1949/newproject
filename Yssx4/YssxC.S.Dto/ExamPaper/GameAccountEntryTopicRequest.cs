﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 实习分录题查询入参
    /// </summary>
    public class GameAccountEntryTopicRequest
    {
        /// <summary>
        /// 关卡Id
        /// </summary>
        [Required]
        public long GameLevelId { get; set; }
        /// <summary>
        /// 作答记录id
        /// </summary>
        [Required]
        public long GradeId { get; set; }
        /// <summary>
        /// 岗位id
        /// </summary>
        [Required]
        public long PositionId { get; set; }

    }

    /// <summary>
    /// 实习分录题查询入参
    /// </summary>
    public class GameAuditAccountEntryTopicRequest
    {
        /// <summary>
        /// 关卡Id
        /// </summary>
        [Required]
        [Range(1, long.MaxValue)]
        public long GameLevelId { get; set; }
        /// <summary>
        /// 作答记录id
        /// </summary>
        [Required]
        [Range(1, long.MaxValue)]
        public long GradeId { get; set; }

        /// <summary>
        /// 岗位id
        /// </summary>
        [Required]
        [Range(1,long.MaxValue)]
        public long PositionId { get; set; }
        /// <summary>
        /// （0代表出纳  1代表会计主管）
        /// </summary>
        [Required]
        public int AuditType { get; set; }

    }
}
