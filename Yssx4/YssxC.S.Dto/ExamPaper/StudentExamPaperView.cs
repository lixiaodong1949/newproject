﻿using System;
using Yssx.Framework;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 学生试卷抽查视图
    /// </summary>
    public class StudentExamPaperView
    {
        public long StudentId { get; set; }
        public DateTime BeginTime { get; set; }
        public DateTime? EndTime { get; set; }

        public ExamStatus Status
        {
            get
            {
                if (DateTime.Now <= BeginTime)
                    return ExamStatus.Wait;
                if (BeginTime <= DateTime.Now && (!EndTime.HasValue || DateTime.Now < EndTime))
                    return ExamStatus.Started;
                if (EndTime.HasValue && EndTime <= DateTime.Now)
                    return ExamStatus.End;
                return ExamStatus.Wait;
            }
        }
    }
}
