﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto.ExamPaper
{
    public class AppSubmitAnswerDto
    {
        public QuestionAnswer Answer { get; set; }

        public CourseExamType ExamType { get; set; }

        /// <summary>
        /// 0:岗位实训，1：课程实训
        /// </summary>
        public int Option { get; set; }
    }
}
