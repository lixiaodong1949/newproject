﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 实习分录详情dto
    /// </summary>
    public class AccountEntryInfoDto
    {
        /// <summary>
        /// 所属案例
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 题目岗位Id
        /// </summary>
        public long PositionId { get; set; }


        /// <summary>
        /// 题目标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 题干内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 作答区内容(表格模板信息)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TopicContent { get; set; }

        /// <summary>
        /// 分值
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 附件
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<QuestionFile> QuestionFile { get; set; }

        /// <summary>
        /// 用户作答信息
        /// </summary>
        public string StudentAnswer { get; set; }

        /// <summary>
        /// 作答记录与正确答案的对比结果
        /// </summary>
        public string AnswerCompareInfo { get; set; }

        /// <summary>
        /// 分录题审核状态
        /// </summary>
        public AccountEntryStatus AccountEntryStatus { get; set; }

        /// <summary>
        /// 模板数据源内容(含答案)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FullContent { get; set; }

        /// <summary>
        /// 答案
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AnswerValue { get; set; }

        /// <summary>
        /// 答案解析
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Hint { get; set; }

        /// <summary>
        /// 是否已结账
        /// </summary>
        public bool IsSettled { get; set; }

        /// <summary>
        /// 凭证题目相关设置
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CertificateTopicView CertificateTopic { get; set; }

        /// <summary>
        /// 当前岗位考试状态
        /// </summary>
        public StudentExamStatus StudentExamStatus { get; set; }


    }

}
