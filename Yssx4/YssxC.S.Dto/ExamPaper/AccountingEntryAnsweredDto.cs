﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 已作答实习分录dto
    /// </summary>
    public class AccountingEntryAnsweredDto
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 业务日期
        /// </summary>
        public DateTime? BusinessDate { get; set; }

        /// <summary>
        /// 凭证字
        /// </summary>
        public CertificateWord CertificateWord { get; set; }
        /// <summary>
        /// 凭证号
        /// </summary>
        public int CertificateNo { get; set; }
        /// <summary>
        /// 摘要
        /// </summary>
        public string SummaryInfo { get; set; }
        /// <summary>
        /// 借方金额
        /// </summary>
        
        public decimal TotalBorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        
        public decimal TotalCreditorAmount { get; set; }
        /// <summary>
        /// 票据名称
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 是否标记
        /// </summary>
        public bool IsMark { get; set; }
        /// <summary>
        /// 分录状态
        /// </summary>

        public AccountEntryStatus AccountEntryStatus { get; set; }

    }

}
