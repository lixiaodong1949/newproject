﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace YssxC.S.Dto.ExamPaper
{
    public class GameDeclarationRequest
    {
        /// <summary>
        /// 关卡Id
        /// </summary>
        [Required]
        public long GameLevelId { get; set; }
        /// <summary>
        /// 岗位id
        /// </summary>
        [Required]
        public long PositionId { get; set; }

        /// <summary>
        /// 主作答记录Id
        /// </summary>
        [Required]
        public long GradeId { get; set; }
    }
}
