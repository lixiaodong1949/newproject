﻿namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 拒绝记录dto
    /// </summary>
    public class RefusalRecordDto
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 作答主记录Id
        /// </summary>
        public long GradeId { get; set; }
        /// <summary>
        /// 题目id
        /// </summary>
        public long QuestionId { get; set; }

        /// <summary>
        /// 拒绝人 0 出纳 1主管
        /// </summary>
        public int RefusalRole { get; set; }
        /// <summary>
        /// 拒绝原因
        /// </summary>
        public string RefusalCause { get; set; }

    }
}
