﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto.ExamPaper
{
    /// <summary>
    /// 结转损溢dto
    /// </summary>
    public class ExamProfitLossTopicDto
    {
        /// <summary>
        /// 题目Id
        /// </summary>
        public long QuestionId { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        public decimal TotalAmount { get; set; }
        /// <summary>
        /// 是否已结转
        /// </summary>
        public bool IsCarryForward { get; set; }
        /// <summary>
        /// 损溢科目信息(不分开结转)
        /// </summary>
        public List<ProfitLossTopicSubjectInfo> MergeProfitLossTopicSubjectInfos { get; set; }
        /// <summary>
        /// 损溢科目信息（分开结转）
        /// </summary>
        public List<List<ProfitLossTopicSubjectInfo>> PartProfitLossTopicSubjectInfos { get; set; }

    }
    /// <summary>
    /// 科目信息
    /// </summary>
    public class ProfitLossTopicSubjectInfo
    { 
        /// <summary>
        /// 科目Id
        /// </summary>
        public long SubjectId { get; set; }

        /// <summary>
        /// 借方金额
        /// </summary>
        public decimal BorrowAmount { get; set; }

        /// <summary>
        /// 贷方金额
        /// </summary>
        public decimal CreditorAmount { get; set; }
        /// <summary>
        /// 金额为负数则显示在借方，正数则显示在贷方
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 损溢科目类别 0-A类 1-B类 2-C类 3-D类 4-E类
        /// </summary>
        public ProfitLossSubjectType ProfitLossSubjectType { get; set; }
    }

}
