﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 获取其他实习工作列表 返回值Dto
    /// </summary>
    public class GetOtherTasksResponseDto
    {
        /// <summary>
        /// 案例Id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 案例名称
        /// </summary>
        public string CaseName { get; set; }
        /// <summary>
        /// 任务Id
        /// </summary>
        public long TaskId { get; set; }
        /// <summary>
        /// 任务月度
        /// </summary>
        public int TaskMonth { get; set; }
        /// <summary>
        /// 任务详情
        /// </summary>
        public string TaskDescription { get; set; }
        /// <summary>
        /// 任务可获得积分
        /// </summary>
        public int Integral { get; set; }
        /// <summary>
        /// 难度系数
        /// </summary>
        public int Rate { get; set; }
        /// <summary>
        /// 行业名称
        /// </summary>
        public string IndustryName { get; set; }
    }
}
