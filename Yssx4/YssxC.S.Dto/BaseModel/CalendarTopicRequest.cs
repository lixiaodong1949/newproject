﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Yssx.Framework.Entity
{
    /// <summary>
    /// 日历题目基类
    /// </summary>
    public class CalendarTopicRequest
    {
        /// <summary>
        /// 岗位Id
        /// </summary>
        public Nullable<long> PositionId { get; set; }

        /// <summary>
        /// 日历开始日期
        /// </summary>
        public Nullable<DateTime> StartDate { get; set; }

        /// <summary>
        /// 日历结束日期
        /// </summary>
        public Nullable<DateTime> EndDate { get; set; }

    }
}
