﻿using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 标签
    /// </summary>
    public class TagDto
    {
        public long Id { get; set; }   

        /// <summary>
        /// 标签名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 启用或禁用标签
        /// </summary>
        public bool IsActive { get; set; }
    }

    public class GetTagInput : PageRequest
    {
        /// <summary>
        /// 案例id
        /// </summary>
        public long? CaseId { get; set; }

        /// <summary>
        /// 标签名称
        /// </summary>
        public string Name { get; set; }
    }
}
