﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 规章制度基础数据
    /// </summary>
    public class RegulationListDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 制度名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 制度名称
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public long CreateBy { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }

      

    }
}
