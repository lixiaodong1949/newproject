﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 规章制度末班查询Dto
    /// </summary>
   public class RegulationQueryDto : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }
}
