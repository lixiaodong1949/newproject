﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 获取规章制度模板名称列表 返回值Dto
    /// </summary>
    public class RegulationNamesListDto
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 子项
        /// </summary>
        public List< RegulationNameSubDto> Subs { get; set; }
    }
    /// <summary>
    /// 获取规章制度模板名称列表 返回值Dto
    /// </summary>
    public class RegulationNameSubDto
    {
        /// <summary>
        /// 
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        
    }
}
