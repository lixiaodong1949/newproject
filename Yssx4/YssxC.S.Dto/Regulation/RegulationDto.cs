﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 规章制度基础数据
    /// </summary>
    public class RegulationDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 制度名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 制度描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 父节点
        /// </summary>
        public long ParentId { get; set; }
        /// <summary>
        /// 案例ids
        /// </summary>
        public List<long> CaseIds { get; set; }

    }
}
