﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 案例应用制度模板
    /// </summary>
    public class CaseUseRegulationDto
    {
        /// <summary>
        /// 规章制度模板Id
        /// </summary>
        public long RegulationId { get; set; }
        /// <summary>
        /// 规章制度模板Id
        /// </summary>
        public long CaseId { get; set; }
    }
}
