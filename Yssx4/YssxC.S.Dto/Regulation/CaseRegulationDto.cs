﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 公司规章制度基础数据
    /// </summary>
    public class CaseRegulationDto
    {
        public long Id { get; set; }
        /// <summary>
        /// 案例id
        /// </summary>
        public long CaseId { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        ///// <summary>
        ///// 描述
        ///// </summary>
        //public string Description { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 规章制度模板Id
        /// </summary>
        public long RegulationId { get; set; }
        /// <summary>
        /// 应用时间
        /// </summary>
        public string UseDate { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }

    }
}
