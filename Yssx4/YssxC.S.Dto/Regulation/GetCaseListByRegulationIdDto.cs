﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 根据规章制度id获取应用改规章制度的案例列表Dto
    /// </summary>
    public class GetCaseListByRegulationIdDto : PageRequest
    {
        /// <summary>
        /// 规章制度id
        /// </summary>
        public long RegulationId{ get; set; }
    }
}
