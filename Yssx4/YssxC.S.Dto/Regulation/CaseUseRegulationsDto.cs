﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 案例应用制度模板
    /// </summary>
    public class CaseUseRegulationsDto
    {
        /// <summary>
        /// 规章制度模板Id
        /// </summary>
        public List<long> RegulationIds { get; set; }
        /// <summary>
        /// 规章制度模板Id
        /// </summary>
        public long CaseId { get; set; }
    }
}

