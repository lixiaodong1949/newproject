using System;
using System.ComponentModel.DataAnnotations;
using Yssx.Framework;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏资源设置 Dto
    /// </summary>
    public class GameResourceConfigDto
    {
        /// <summary>
        /// 资源key
        /// </summary>
        public GameResourceKey GameResourceKey { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }
    }
}