using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;
using YssxC.S.Pocos;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏资源设置 返回值Dto
    /// </summary>
    public class GameResourceConfigResponseDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 资源key
        /// </summary>
        public GameResourceKey GameResourceKey { get; set; }
        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }
    }
}