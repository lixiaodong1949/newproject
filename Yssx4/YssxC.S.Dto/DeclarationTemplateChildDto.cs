﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 申报表子表模板
    /// </summary>
    public class DeclarationTemplateChildDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 报表内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 申报表主表模板id
        /// </summary>
        public long DeclarationTypeTemplateId { get; set; }

        /// <summary>
        /// 表头内容
        /// </summary>
        public string HeaderContent { get; set; }
    }

    public class GetDeclarationTemplateInput
    {
        /// <summary>
        /// 申报表主表模板id
        /// </summary>
        public long DeclarationTypeTemplateId { get; set; }
    }
}
