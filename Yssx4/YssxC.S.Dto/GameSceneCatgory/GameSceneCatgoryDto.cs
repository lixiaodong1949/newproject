using System;
using System.ComponentModel.DataAnnotations;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 游戏场景分类 Dto
    /// </summary>
    public class GameSceneCatgoryDto
    {
        /// <summary>
        /// 唯一Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>        
        [Required(ErrorMessage ="名称[Name]不能为空")]
        public string Name { get; set; }
    }
}