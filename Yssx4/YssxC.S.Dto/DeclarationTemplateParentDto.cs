﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace YssxC.S.Dto
{
    /// <summary>
    /// 申报表主表模板
    /// </summary>
    public class DeclarationTemplateParentDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 数量
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 序号
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 报表内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 子表
        /// </summary>
        public List<DeclarationTemplateChildDto> Children { get; set; }
    }

    public class GetDeclarationTemplateParentInput : PageRequest
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
    }

    public class ToggleInput
    {
        /// <summary>
        /// id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 是否启用 true:启用  false:禁用
        /// </summary>
        public bool IsActive { get; set; }

    }
}
