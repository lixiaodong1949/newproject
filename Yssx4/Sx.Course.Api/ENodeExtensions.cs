﻿using ECommon.Socketing;
using ENode.Commanding;
using ENode.Configurations;
using ENode.EQueue;
using EQueue.Clients.Producers;
using EQueue.Configurations;
using System.Collections.Generic;
using System.Net;

namespace Sx.Course.Api
{
    public static class ENodeExtensions
    {
        private static CommandService _commandService;

        public static ENodeConfiguration BuildContainer(this ENodeConfiguration enodeConfiguration)
        {
            enodeConfiguration.GetCommonConfiguration().BuildContainer();
            return enodeConfiguration;
        }

        public static ENodeConfiguration UseEQueue(this ENodeConfiguration enodeConfiguration)
        {
            var configuration = enodeConfiguration.GetCommonConfiguration();
            configuration.RegisterEQueueComponents();

            _commandService = new CommandService();
            configuration.SetDefault<ICommandService, CommandService>(_commandService);

            return enodeConfiguration;
        }
        public static ENodeConfiguration StartEQueue(this ENodeConfiguration enodeConfiguration)
        {
            _commandService.InitializeEQueue(new CommandResultProcessor().Initialize(new IPEndPoint(ConfigSettings.CommandResultAddress, ConfigSettings.CommandResultPort)), new ProducerSetting
            {
                NameServerList = new List<IPEndPoint> { new IPEndPoint(ConfigSettings.NameServerAddress, ConfigSettings.NameServerPort) }
            });
            _commandService.Start();
            return enodeConfiguration;
        }
    }
}
