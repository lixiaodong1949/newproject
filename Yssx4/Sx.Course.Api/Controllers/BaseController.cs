﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Sx.Course.Api.Filters;
using Yssx.Framework;
using Yssx.Framework.Authorizations;

namespace Sx.Course.Api.Controllers
{
    [Produces("application/json")]
    [ApiVersion("1.0", Deprecated = false)]
    //[Route("api/v{api-version:apiVersion}/[controller]/[action]")]
    [Route("sxcourse/[controller]/[action]")]
    [ApiController]
    [EnableCors("AnyOrigin")]//跨域
    [ServiceFilter(typeof(CustomExceptionFilter))]
    public class BaseController : Controller
    {
        protected UserTicket CurrentUser { get; private set; }
        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            HttpContext.Items.TryGetValue(CommonConstants.ContextUserPropertyKey, out var requestUser);
            CurrentUser = requestUser as UserTicket;
        }
    }
}