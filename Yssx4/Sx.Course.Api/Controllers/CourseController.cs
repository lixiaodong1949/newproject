﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommon.Extensions;
using ECommon.Utilities;
using ENode.Commanding;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sx.Course.Commands;
using Sx.Course.QueryServices.FreeSqldb;
using Sx.Course.QueryServices.FreeSqldb.Dto;
using Tas.Common.IdGenerate;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace Sx.Course.Api.Controllers
{
    [Route("sxcourse/[controller]/[action]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private ICommandService _commandService;
        private CourseQueryService _queryService;
        public CourseController(ICommandService service, CourseQueryService queryService)
        {
            _commandService = service;
            _queryService = queryService;
        }

        /// <summary>
        /// 创建课程
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> CreateCourse(CreateCourse info)
        {
            if (null == info) return new ResponseContext<bool> { Code = (int)CommandStatus.Failed, Msg = "对象是空的", Data = false };

            string massge = "OK";
            try
            {
                info.Id = ObjectId.GenerateNewStringId();
                info.CourseId = IdWorker.NextId();
                info.AggregateRootId = info.CourseId;
                info.CourseId = IdWorker.NextId();
                var result = await ExecuteCommandAsync(info);
                if (result.Status != CommandStatus.Success)
                {
                    return new ResponseContext<bool> { Code = (int)CommandStatus.Failed, Msg = "服务繁忙,请稍后再试!", Data = false };
                }
            }
            catch (Exception ex)
            {
                massge = ex.Message;
                return new ResponseContext<bool> { Code = (int)CommandStatus.Failed, Msg = massge, Data = false };
            }


            return new ResponseContext<bool> { Code = (int)CommandStatus.Success, Msg = massge, Data = true };
        }

        /// <summary>
        /// 编辑课程信息
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> EditCourse(YssxCourseDto dto)
        {
            try
            {
                var result = await ExecuteCommandAsync(new UpdateCourse
                {
                    CourseId = dto.Id,
                    CourseTitle = dto.CourseTitle,
                    CourseName = dto.CourseName,
                    Author = dto.Author,
                    AggregateRootId = dto.Id,
                    CourseTypeId = dto.CourseTypeId,
                    Education = dto.Education,
                    Images = dto.Images,
                    KnowledgePointId = dto.KnowledgePointId,
                    PublishingDate = dto.PublishingDate,
                    PublishingHouse = dto.PublishingHouse,
                });

                if (result.Status != CommandStatus.Success)
                {
                    return new ResponseContext<bool> { Code = (int)CommandStatus.Failed, Msg = "服务繁忙,请稍后再试!", Data = false };
                }
            }
            catch (Exception ex)
            {
                return new ResponseContext<bool> { Code = (int)CommandStatus.Failed, Msg = ex.Message, Data = false };
            }

            return new ResponseContext<bool> { Code = (int)CommandStatus.Success, Msg = "成功", Data = true };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ResponseContext<bool>> DeleteCourse(long id)
        {
            var result = await ExecuteCommandAsync(new EditCourseStatus { CourseId = id, AggregateRootId = id }).ConfigureAwait(false);

            return new ResponseContext<bool> { Code = (int)CommandStatus.Success, Msg = "成功", Data = true };
        }

        /// <summary>
        /// 获取课程
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResponse<YssxCourseDto>> GetCourseListByPage([FromQuery]CourseSearchDto dto)
        {
            return await _queryService.GetCourseListByPage(dto, new Yssx.Framework.Authorizations.UserTicket()).ConfigureAwait(false);
        }

        private Task<CommandResult> ExecuteCommandAsync(ICommand command, int millisecondsDelay = 5000, CommandReturnType commandType = CommandReturnType.EventHandled)
        {
            return _commandService.ExecuteAsync(command, commandType).TimeoutAfter(millisecondsDelay);
        }

    }
}