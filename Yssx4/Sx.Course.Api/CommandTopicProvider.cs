﻿using ECommon.Components;
using ENode.Commanding;
using ENode.EQueue;
using Sx.Course.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sx.Course.Api
{
    [Component]
    public class CommandTopicProvider : AbstractTopicProvider<ICommand>
    {
        //public CommandTopicProvider()
        //{
        //    RegisterTopic("CourseCommandTopic", typeof(CreateCourse));
        //}
        public override string GetTopic(ICommand command)
        {
            return "CourseCommandTopic";
        }
    }
}
