﻿using ECommon.Socketing;
using ENode.Configurations;
using ENode.EQueue;
using ENode.Eventing;
using ENode.Messaging;
using EQueue.Clients.Consumers;
using EQueue.Clients.Producers;
using EQueue.Configurations;
using System.Collections.Generic;
using System.Configuration;
using System.Net;

namespace Yssx.CourseProcessorService
{
    public static class ENodeExtensions
    {
        private static CommandConsumer _commandConsumer;
        private static ApplicationMessagePublisher _applicationMessagePublisher;
        private static DomainEventPublisher _domainEventPublisher;
        private static DomainEventConsumer _eventConsumer;

        public static ENodeConfiguration BuildContainer(this ENodeConfiguration enodeConfiguration)
        {
            enodeConfiguration.GetCommonConfiguration().BuildContainer();
            return enodeConfiguration;
        }
        public static ENodeConfiguration UseEQueue(this ENodeConfiguration enodeConfiguration)
        {
            var configuration = enodeConfiguration.GetCommonConfiguration();

            configuration.RegisterEQueueComponents();

            _applicationMessagePublisher = new ApplicationMessagePublisher();
            _domainEventPublisher = new DomainEventPublisher();

          //  configuration.SetDefault<IMessagePublisher<IApplicationMessage>, ApplicationMessagePublisher>(_applicationMessagePublisher);
            configuration.SetDefault<IMessagePublisher<DomainEventStreamMessage>, DomainEventPublisher>(_domainEventPublisher);

            return enodeConfiguration;
        }
        public static ENodeConfiguration StartEQueue(this ENodeConfiguration enodeConfiguration)
        {

            var address = ConfigurationManager.AppSettings["nameServerAddress"];            //nameServer IP地址
            var nameServerPort = ConfigurationManager.AppSettings["nameServerPort"];        //nameServer 端口
            var nameServerAddress = string.IsNullOrEmpty(address) ? SocketUtils.GetLocalIPV4() : IPAddress.Parse(address);              //nameServer IP地址设置

            //topic
            var commandTopic = ConfigurationManager.AppSettings["commandTopic"];
            var eventTopic = ConfigurationManager.AppSettings["eventTopic"];

            //group
            var commandConsumerGroup = ConfigurationManager.AppSettings["commandConsumerGroup"]?? "DefaultCommandConsumerGroup";
            var eventConsumerGroup = ConfigurationManager.AppSettings["eventConsumerGroup"]?? "DefaultEventConsumerGroup";

            var producerSetting = new ProducerSetting
            {
                NameServerList = new List<IPEndPoint> { new IPEndPoint(nameServerAddress, int.Parse(nameServerPort)) }
            };

            var consumerSetting = new ConsumerSetting
            {
                NameServerList = new List<IPEndPoint> { new IPEndPoint(nameServerAddress, int.Parse(nameServerPort)) }
            };

            //_applicationMessagePublisher.InitializeEQueue(producerSetting);
            _domainEventPublisher.InitializeEQueue(producerSetting);

            _commandConsumer = new CommandConsumer().InitializeEQueue(commandConsumerGroup, consumerSetting).Subscribe(commandTopic);
            _eventConsumer = new DomainEventConsumer().InitializeEQueue(eventConsumerGroup, consumerSetting).Subscribe(eventTopic);

            _eventConsumer.Start();
            _commandConsumer.Start();
            //_applicationMessagePublisher.Start();
            _domainEventPublisher.Start();
            return enodeConfiguration;
        }
        public static ENodeConfiguration ShutdownEQueue(this ENodeConfiguration enodeConfiguration)
        {
            _applicationMessagePublisher.Shutdown();
            _domainEventPublisher.Shutdown();
            _commandConsumer.Shutdown();
            _eventConsumer.Shutdown();
            return enodeConfiguration;
        }
    }
}
