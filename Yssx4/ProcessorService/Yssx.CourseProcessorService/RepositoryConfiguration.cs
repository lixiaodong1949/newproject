﻿using ENode.Configurations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Yssx.Common;


namespace Yssx.CourseProcessorService
{
    public static class RepositoryConfiguration
    {
        public static ENodeConfiguration UseFreeSqlRepository(this ENodeConfiguration projectConfiguration, string sqlConnection, params string[] slaveConnectionString)
        {
            var fsql = new FreeSql.FreeSqlBuilder()
                .UseConnectionString(FreeSql.DataType.MySql, sqlConnection)
                .UseSlave(slaveConnectionString)
                                            .UseAutoSyncStructure(true) //自动同步实体结构【开发环境必备】                
                                                                        //.UseLogger(CommonLogger.CreateLogger("UseFreeSqlRepository"))
                                            .UseMonitorCommand(a =>
                                            {
                                                Trace.WriteLine(a.CommandText);
                                            })
                .Build();
            fsql.Aop.CurdAfter = (s, e) =>
            {
                Trace.WriteLine($"aop after {e.Sql}");
            };
            DbContext.RegisterFreeSql(fsql);
            return projectConfiguration;
        }
    }
}
