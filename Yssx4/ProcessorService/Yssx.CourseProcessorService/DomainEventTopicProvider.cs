﻿using ECommon.Components;
using ENode.EQueue;
using ENode.Eventing;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Yssx.CourseProcessorService
{
    [Component]
    public class DomainEventTopicProvider : AbstractTopicProvider<IDomainEvent>
    {
        public override string GetTopic(IDomainEvent evnt)
        {
            return ConfigurationManager.AppSettings["eventTopic"];
        }
    }
}
