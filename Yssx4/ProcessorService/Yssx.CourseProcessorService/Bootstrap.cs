﻿using ECommon.Configurations;
using ENode.Configurations;
using ENode.MySQL;
using System;
using System.Configuration;
using System.Reflection;
using Yssx.Common;
using ECommonConfiguration = ECommon.Configurations.Configuration;
namespace Yssx.CourseProcessorService
{
    public class Bootstrap
    {
        //private static ILogger _logger;
        private static ECommonConfiguration _ecommonConfiguration;
        private static ENodeConfiguration _enodeConfiguration;

        public static void Initialize()
        {
            InitializeECommon();
            InitializeENode();
        }
        public static void Start()
        {
            try
            {
                _enodeConfiguration.StartEQueue();
            }
            catch (Exception ex)
            {
                //_logger.Error("EQueue start failed.", ex);
                throw;
            }
        }
        public static void Stop()
        {
            try
            {
                _enodeConfiguration.ShutdownEQueue();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void InitializeECommon()
        {
            _ecommonConfiguration = ECommonConfiguration
                .Create()
                .UseAutofac()
                .RegisterCommonComponents()
                .UseSerilog()
                .UseJsonNet()
            
                .RegisterUnhandledExceptionHandler();
        }
        private static void InitializeENode()
        {
            var assemblies = new[]
            {
                Assembly.Load("Sx.Course.Commands"),
                Assembly.Load("Sx.Course.Domain"),
                Assembly.Load("Sx.Course.CommandHandlers"),
                Assembly.Load("Sx.Course.Denormalizer.FreeSql"),
                Assembly.Load("Sx.Course.Domain.FreeSql"),
                Assembly.Load("Sx.Course.Entitys"),
                Assembly.Load("Yssx.Common"),
                Assembly.GetExecutingAssembly()
            };

            ConfigSettings.Initialize();
            DataMergeHelper.Start();
            var connectionString = ConfigurationManager.ConnectionStrings["enode"].ConnectionString; //ConfigSettings.ConferenceENodeConnectionString;

            _enodeConfiguration = _ecommonConfiguration
                .CreateENode()
                .RegisterENodeComponents()
                .RegisterBusinessComponents(assemblies)
                .UseMySqlLockService()
                .UseMySqlEventStore()
                .UseMySqlPublishedVersionStore()
                .UseEQueue()
                .BuildContainer()
                .InitializeMySqlEventStore(connectionString)
                .InitializeMySqlPublishedVersionStore(connectionString)
                .InitializeMySqlLockService(connectionString)
                .UseFreeSqlRepository(ConfigurationManager.ConnectionStrings["yssx"].ConnectionString, ConfigurationManager.ConnectionStrings["yssx"].ConnectionString)
                .InitializeBusinessAssemblies(assemblies);

            //_logger = ObjectContainer.Resolve<ILoggerFactory>().Create(typeof(Bootstrap).FullName);
            //_logger.Info("ENode initialized.");
        }
    }
}
