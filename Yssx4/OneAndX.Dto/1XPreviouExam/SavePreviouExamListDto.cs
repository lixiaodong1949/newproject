﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 历届考试信息
    /// </summary>
    public class SavePreviouExamListDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 历届考试证书类型
        /// </summary>
        public PreviouExamType PreviouExamType { get; set; } = PreviouExamType.DEFAULT;

        /// <summary>
        /// 发证日期
        /// </summary>
        public DateTime IssueDate { get; set; }

        /// <summary>
        /// 报名开始时间
        /// </summary>
        public DateTime ApplyBeginTime { get; set; }

        /// <summary>
        /// 报名结束时间
        /// </summary>
        public DateTime ApplyEndTime { get; set; }

        /// <summary>
        /// 考试开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 考试结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 封面图片
        /// </summary>
        public string Img { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 报名人数
        /// </summary>
        public int ApplyQty { get; set; }

        /// <summary>
        /// 花絮
        /// </summary>
        public List<GetPreviouExamTidbitDto> TidbitDetail { get; set; }

        /// <summary>
        /// 培训资料
        /// </summary>
        public List<GetPreviouExamFileDto> FileDetail { get; set; }

        /// <summary>
        /// 正式图片模板
        /// </summary>
        public string XImg { get; set; }

    }

    /// <summary>
    /// 获取历届考试列表 - 花絮
    /// </summary>
    public class GetPreviouExamTidbitDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }

    /// <summary>
    /// 获取历届考试列表 - 培训资料
    /// </summary>
    public class GetPreviouExamFileDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 文件名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 文件地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }


}
