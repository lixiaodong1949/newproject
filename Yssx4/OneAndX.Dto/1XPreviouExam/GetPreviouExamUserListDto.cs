﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Dto
{
    /// <summary>
    /// 获取历届考试 - 报名用户列表
    /// </summary>
    public class GetPreviouExamUserListDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 考生姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 学籍号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 报名时间
        /// </summary>
        public DateTime CreateTime { get; set; }


    }

    /// <summary>
    /// 获取历届考试 - 报名用户列表 - 入参
    /// </summary>
    public class GetPreviouExamUserListRequest : PageRequest
    {
        /// <summary>
        /// 历届考试ID
        /// </summary>
        public long ExamId { get; set; }


    }

    /// <summary>
    /// 获取历届考试 - 证书列表
    /// </summary>
    public class GetPreviouExamCertificateListDto
    {
        /// <summary>
        /// 主键ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 考生姓名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 学籍号
        /// </summary>
        public string StudentNo { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 学校名称
        /// </summary>
        public string SchoolName { get; set; }

        /// <summary>
        /// 排名
        /// </summary>
        public int Ranking { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; } = Sex.None;

        /// <summary>
        /// 出生年月
        /// </summary>
        public string BirthDate { get; set; }

        /// <summary>
        /// 专业名称
        /// </summary>
        public string ProfessionName { get; set; }

        /// <summary>
        /// 证书编号
        /// </summary>
        public string CertificateNo { get; set; }

        /// <summary>
        /// 证书生成时间
        /// </summary>
        public DateTime CertificateDate { get; set; }

        /// <summary>
        /// 发证时间
        /// </summary>
        public DateTime IssueDate { get; set; }


        /// <summary>
        /// 证书类型
        /// </summary>
        public PreviouExamType PreviouExamType { get; set; } = PreviouExamType.DEFAULT;


    }

}
