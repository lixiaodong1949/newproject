﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto
{
    /// <summary>
    /// 证书考场查询实体
    /// </summary>
    public class CertificateExamRoomQuery
    {
        /// <summary>
        /// 证书类型 1:成本管控 2:业财税 3:财务管理
        /// </summary>
        public Nullable<int> CertificateType { get; set; }

        /// <summary>
        /// 证书等级 1:初级 2:中级 3:高级
        /// </summary>
        public Nullable<int> CertificateLevel { get; set; }

    }
}
