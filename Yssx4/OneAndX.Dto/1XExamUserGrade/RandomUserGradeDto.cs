﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto
{
    public class RandomUserGradeDto
    {
        public string IdNumber { get; set; }

        public string Name { get; set; }
    }
}
