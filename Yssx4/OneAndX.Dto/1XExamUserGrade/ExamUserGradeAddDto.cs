﻿using Aop.Api.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 考试成绩
    /// </summary>
    public class ExamUserGradeAddDto
    {
        ///// <summary>
        ///// 用户Id
        ///// </summary>
        //[JsonIgnore]
        //public long UserId { get; set; }

        /// <summary>
        /// 作答记录Id
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 考试计划Id
        /// </summary>
        public long ExamPlanId { get; set; }

        ///// <summary>
        ///// 用户身份证号码
        ///// </summary>
        //public string UserIDNumber { get; set; }

        ///// <summary>
        ///// 用户姓名
        ///// </summary>
        //public string UserName { get; set; }

        ///// <summary>
        ///// 准考证号
        ///// </summary>
        //public string ExamTicketNo { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        ///// <summary>
        ///// 证书
        ///// </summary>
        //public Certificate Certificate { get; set; }

        ///// <summary>
        ///// 证书等级
        ///// </summary>
        //public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal Score { get; set; }

        /// <summary>
        /// 试卷总分
        /// </summary>
        public decimal ExamPaperScore { get; set; }

        ///// <summary>
        ///// 考试试卷
        ///// </summary>
        //public DateTime ExamTime { get; set; }

        /// <summary>
        /// 案例原系统
        /// </summary>
        public CaseSourceSystem SourceSystem { get; set; }

        /// <summary>
        /// 原案例Id
        /// </summary>
        public long SourceCaseId { get; set; }

        /// <summary>
        /// 考试案例Id
        /// </summary>
        public long ExamCaseId { get; set; }

        /// <summary>
        /// 是否考试通过
        /// </summary>
        [JsonIgnore]
        public bool IsPass { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int RollUpType { get; set; }

        public string CaseYear { get; set; }

    }
}
