﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto
{
    /// <summary>
    /// 手机验证码请求实体
    /// </summary>
    public class MobilePhoneVerificationCodeDto
    {
        /// <summary>
        /// 手机号码
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        public string VerificationCode { get; set; }
    }
}
