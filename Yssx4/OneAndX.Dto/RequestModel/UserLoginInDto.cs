﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto
{
    /// <summary>
    /// 用户登录请求实体
    /// </summary>
    public class UserLoginInDto
    {
        /// <summary>
        /// 手机号码
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }
    }
}
