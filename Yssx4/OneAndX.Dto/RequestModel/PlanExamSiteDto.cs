﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto
{
    /// <summary>
    /// 考试计划考场请求实体
    /// </summary>
    public class PlanExamRoomDto
    {
        /// <summary>
        /// 计划Id
        /// </summary>
        public long PlanId { get; set; }

        /// <summary>
        /// 考场列表
        /// </summary>
        public List<PlanExamRoomDetail> ExamRoomList = new List<PlanExamRoomDetail>();

    }

    /// <summary>
    /// 考场详情
    /// </summary>
    public class PlanExamRoomDetail
    {
        /// <summary>
        /// 考点Id
        /// </summary>
        public long ExamSiteId { get; set; }

        /// <summary>
        /// 考场Id
        /// </summary>
        public long ExamRoomId { get; set; }
    }

}
