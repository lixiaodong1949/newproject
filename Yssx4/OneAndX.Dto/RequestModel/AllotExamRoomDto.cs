﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto
{
    /// <summary>
    /// 随机分配考场请求实体
    /// </summary>
    public class AllotExamRoomDto
    {
        /// <summary>
        /// 计划Id
        /// </summary>
        public long PlanId { get; set; }

        /// <summary>
        /// 用户Id
        /// </summary>
        public long UserId { get; set; }
    }
}
