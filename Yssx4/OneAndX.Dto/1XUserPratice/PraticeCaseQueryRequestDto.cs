﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Dto
{
    /// <summary>
    /// 
    /// </summary>
   public class PraticeCaseQueryRequestDto
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }

    }

    public class PraticeCaseQueryDto
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        public long Userid { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }

        public int Status { get; set; }
    }
}
