﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
namespace OneAndX.Dto.UserPratice
{
    public class AllCaseDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        public string TrainingName { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 案例所属系统
        /// </summary>
        public CaseSourceSystem SourceSystem { get; set; }

        /// <summary>
        /// 案例原系统中的Id
        /// </summary>
        public long SourceCaseId { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>
        public int Level { get; set; }

        public long ExamId { get; set; }

        public long GradeId { get; set; }

        /// <summary>
        /// 练习状态
        /// </summary>
        public int Status { get; set; }

        public string CaseYear { get; set; }

        public string Year { get; set; }

        public int RollUpType { get; set; }
    }

    public class RandomCaseDto
    {

        public string Name { get; set; }

        public string CaseYear { get; set; }



        public int RollUpType { get; set; }

        public long CaseId { get; set; }


        public long ExamId { get; set; }

        /// <summary>
        /// 案例所属系统
        /// </summary>
        public CaseSourceSystem SourceSystem { get; set; }
    }
}
