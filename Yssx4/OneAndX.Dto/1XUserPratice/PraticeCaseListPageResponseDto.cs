﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class PraticeCaseListResponseDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 案例所属系统
        /// </summary>
        public int SourceSystem { get; set; }

        /// <summary>
        /// 案例原系统中的Id
        /// </summary>
        public long SourceCaseId { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>
        public int Level { get; set; }
        /// <summary>
        /// Logo
        /// </summary>
        public string Logo { get; set; }

        /// <summary>
        /// 总时长
        /// </summary>
        public int TotalTime { get; set; }

        /// <summary>
        /// 自主训练名称
        /// </summary>
        public string TrainingName { get; set; }
    }
}
