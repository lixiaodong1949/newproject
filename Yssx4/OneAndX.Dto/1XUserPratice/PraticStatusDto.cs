﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto.UserPratice
{
    public class PraticStatusDto
    {
        public long CaseId { get; set; }

        public long ExamId { get; set; }

        public int RollUpType { get; set; }

        public string CaseYear { get; set; }


        public long GradeId { get; set; }

        public int Status { get; set; }
    }
}
