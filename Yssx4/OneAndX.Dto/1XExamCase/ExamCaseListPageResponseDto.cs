﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 试卷案例信息
    /// </summary>
    public class ExamCaseListPageResponseDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 试卷名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// 是否加入自主训练
        /// </summary>
        public bool IsCanTraining { get; set; }

        /// <summary>
        /// 案例所属系统
        /// </summary>
        public CaseSourceSystem SourceSystem { get; set; }

        /// <summary>
        /// 案例原系统中的Id
        /// </summary>
        public long SourceCaseId { get; set; }

        /// <summary>
        /// 难度等级
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// Logo
        /// </summary>
        public string Logo { get; set; }

        /// <summary>
        /// 考试案例拥有的题目类型名称
        /// </summary>
        public string QuestionTypeName { get; set; }

        /// <summary>
        /// 自主训练名称
        /// </summary>
        public string TrainingName { get; set; }

        /// <summary>
        /// 案例原系统中的名称
        /// </summary>
        public string SourceCaseName { get; set; }
    }
}
