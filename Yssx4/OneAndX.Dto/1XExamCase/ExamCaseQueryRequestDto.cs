﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Dto
{
    /// <summary>
    /// 
    /// </summary>
   public class ExamCaseQueryRequestDto : PageRequest
    {
        /// <summary>
        /// 公司名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public Status? Status { get; set; }

        /// <summary>
        /// 是否加入自主训练
        /// </summary>
        public bool? IsCanTraining { get; set; }
    }
}
