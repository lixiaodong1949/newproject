﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto
{
    /// <summary>
    /// 已报名考试计划考场返回实体
    /// </summary>
    public class SignUpExamRoomViewModel
    {
        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 已报考人数
        /// </summary>
        public int SignUpNumber { get; set; }

        /// <summary>
        /// 考点列表
        /// </summary>
        public List<SuExamSiteViewModel> ExamSiteList = new List<SuExamSiteViewModel>();
    }

    /// <summary>
    /// 考点实体
    /// </summary>
    public class SuExamSiteViewModel
    {
        /// <summary>
        /// 考点Id
        /// </summary>
        public long ExamSiteId { get; set; }

        /// <summary>
        /// 考点名称
        /// </summary>
        public string ExamSiteName { get; set; }

        /// <summary>
        /// 考点代号
        /// </summary>
        public string ExamSiteCode { get; set; }

        /// <summary>
        /// 具体地址
        /// </summary>
        public string SpecificAddress { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int SiteSort { get; set; }

        /// <summary>
        /// 考场列表
        /// </summary>
        public List<SuExamRoomViewModel> ExamRoomList = new List<SuExamRoomViewModel>();
    }

    /// <summary>
    /// 考场实体
    /// </summary>
    public class SuExamRoomViewModel
    {
        /// <summary>
        /// 考场Id
        /// </summary>
        public long ExamRoomId { get; set; }

        /// <summary>
        /// 考场名
        /// </summary>
        public string ExamRoomName { get; set; }

        /// <summary>
        /// 座位数
        /// </summary>
        public int SeatingCapacity { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int RoomSort { get; set; }
    }
}
