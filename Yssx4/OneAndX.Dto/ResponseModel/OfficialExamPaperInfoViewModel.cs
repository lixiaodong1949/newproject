﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 正式考试试卷信息返回实体
    /// </summary>
    public class OfficialExamPaperInfoViewModel
    {
        /// <summary>
        /// 作答明细Id(可空)
        /// </summary>
        public long GradeId { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 试卷命名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 公司(案例)Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 考试开始时间
        /// </summary>
        public Nullable<DateTime> BeginTime { get; set; }

        /// <summary>
        /// 考试结束时间
        /// </summary>
        public Nullable<DateTime> EndTime { get; set; }

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 试卷状态 0:未开始 1:进行中 2:已结束
        /// </summary>
        public int ExamPaperStatus
        {
            get
            {
                if (BeginTime.HasValue && EndTime.HasValue)
                {
                    if (DateTime.Now < BeginTime)
                        return (int)StudentExamStatus.Wait;
                    if (BeginTime <= DateTime.Now && DateTime.Now < EndTime)
                        return (int)StudentExamStatus.Started;
                    if (BeginTime <= DateTime.Now && EndTime <= DateTime.Now)
                        return (int)StudentExamStatus.End;
                }
                return (int)StudentExamStatus.Wait;
            }
        }

        /// <summary>
        /// 案例所属系统 1:竞赛 2:行业赛 3:云实训
        /// </summary>       
        public CaseSourceSystem SourceSystem { get; set; }
    }
}
