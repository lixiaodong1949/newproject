﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto
{
    /// <summary>
    /// 证书考场返回实体
    /// </summary>
    public class CertificateExamRoomViewModel
    {        
        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 考点列表
        /// </summary>
        public List<CerExamSiteViewModel> ExamSiteList = new List<CerExamSiteViewModel>();
    }

    /// <summary>
    /// 考点实体
    /// </summary>
    public class CerExamSiteViewModel
    {
        /// <summary>
        /// 考点Id
        /// </summary>
        public long ExamSiteId { get; set; }

        /// <summary>
        /// 考点名称
        /// </summary>
        public string ExamSiteName { get; set; }

        /// <summary>
        /// 考点代号
        /// </summary>
        public string ExamSiteCode { get; set; }

        /// <summary>
        /// 具体地址
        /// </summary>
        public string SpecificAddress { get; set; }

        /// <summary>
        /// 考场列表
        /// </summary>
        public List<CerExamRoomViewModel> ExamRoomList = new List<CerExamRoomViewModel>();
    }

    /// <summary>
    /// 考场实体
    /// </summary>
    public class CerExamRoomViewModel
    {
        /// <summary>
        /// 考场Id
        /// </summary>
        public long ExamRoomId { get; set; }

        /// <summary>
        /// 考场名
        /// </summary>
        public string ExamRoomName { get; set; }

        /// <summary>
        /// 座位数
        /// </summary>
        public int SeatingCapacity { get; set; }
    }
}
