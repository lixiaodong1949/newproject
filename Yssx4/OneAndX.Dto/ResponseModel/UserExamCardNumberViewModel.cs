﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 用户准考证返回实体
    /// </summary>
    public class UserExamCardNumberViewModel
    {
        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IDNumber { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 准考证号
        /// </summary>
        public string ExamCardNumber { get; set; }

        /// <summary>
        /// 考试密码
        /// </summary>
        public string ExamPassword { get; set; }

        /// <summary>
        /// 考点名称
        /// </summary>
        public string ExamSiteName { get; set; }

        /// <summary>
        /// 考点代号
        /// </summary>
        public string ExaminationSiteCode { get; set; }

        /// <summary>
        /// 具体地址
        /// </summary>
        public string SpecificAddress { get; set; }

        /// <summary>
        /// 考场名称
        /// </summary>
        public string ExamRoomName { get; set; }

        /// <summary>
        /// 座位号
        /// </summary>
        public Nullable<int> SeatNumber { get; set; }

        /// <summary>
        /// 考试计划Id
        /// </summary>
        public long ExamPlanId { get; set; }

        /// <summary>
        /// 报考证书 1:成本管控 2:大数据 3:财务管理
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 报考证书等级 1:初级 2:中级 3:高级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 考试时间
        /// </summary>
        public DateTime ExamTime { get; set; }

        /// <summary>
        /// 考试结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

    }
}
