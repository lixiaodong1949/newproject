﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 考试计划试卷信息返回实体
    /// </summary>
    public class PlanExamPaperInfoViewModel
    {
        /// <summary>
        /// 试卷Id
        /// </summary>
        public long ExamId { get; set; }

        /// <summary>
        /// 试卷命名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 针对座位号 1:奇数 2:偶数
        /// </summary>
        public int ServiceSeatType { get; set; }

        /// <summary>
        /// 公司(案例)Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 案例所属系统 1:竞赛 2:行业赛 3:云实训
        /// </summary>       
        public CaseSourceSystem SourceSystem { get; set; }

        /// <summary>
        /// 座位人数
        /// </summary>
        public int SeatCount { get; set; }
    }
}
