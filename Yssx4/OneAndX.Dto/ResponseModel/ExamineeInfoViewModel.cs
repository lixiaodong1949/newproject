﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 考生信息返回实体
    /// </summary>
    public class ExamineeInfoViewModel
    {
        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IDNumber { get; set; }
        
        /// <summary>
        /// 照片
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 准考证号
        /// </summary>
        public string ExamCardNumber { get; set; }

        /// <summary>
        /// 考试密码
        /// </summary>
        public string ExamPassword { get; set; }

        /// <summary>
        /// 考点名称
        /// </summary>
        public string ExamSiteName { get; set; }

        /// <summary>
        /// 考场名称
        /// </summary>
        public string ExamRoomName { get; set; }

        /// <summary>
        /// 座位号
        /// </summary>
        public Nullable<int> SeatNumber { get; set; }
        
        /// <summary>
        /// 考点排序
        /// </summary>
        public int SiteSort { get; set; }

        /// <summary>
        /// 考场排序
        /// </summary>
        public int RoomSort { get; set; }
    }
}
