﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 考试计划返回实体
    /// </summary>
    public class ExamPlanViewModel
    {
        /// <summary>
        /// 考试计划Id
        /// </summary>
        public long PlanId { get; set; }

        /// <summary>
        /// 考试计划名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证书类型 1:成本管控 2:业财税 3:财务管理
        /// </summary>
        public int CertificateType { get; set; }

        /// <summary>
        /// 证书等级 1:初级 2:中级 3:高级
        /// </summary>
        public int CertificateLevel { get; set; }

        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 考试开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 考试结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 任务状态 0:未开始 1:进行中 2:已结束
        /// </summary>        
        public int TaskFinalStatus
        {
            get
            {
                if (DateTime.Now < BeginTime)
                    return (int)ExamStatus.Wait;
                if (BeginTime <= DateTime.Now && DateTime.Now < EndTime)
                    return (int)ExamStatus.Started;
                if (BeginTime <= DateTime.Now && EndTime <= DateTime.Now)
                    return (int)ExamStatus.End;
                return (int)ExamStatus.Wait;
            }
        }

        /// <summary>
        /// 是否已分配考场
        /// </summary>
        public bool IsDistributedExamRoom { get; set; }

        /// <summary>
        /// 是否已生成准考证
        /// </summary>
        public bool IsCreatedExamCard { get; set; }

        /// <summary>
        /// 是否已生成试卷
        /// </summary>
        public bool IsCreatedExamPaper { get; set; }
    }
}
