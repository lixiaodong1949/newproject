﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 考生成绩返回实体
    /// </summary>
    public class ExamineeGradeViewModel
    {
        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IDNumber { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// 准考证号
        /// </summary>
        public string ExamCardNumber { get; set; }

        /// <summary>
        /// 考点名称
        /// </summary>
        public string ExamSiteName { get; set; }

        /// <summary>
        /// 考场名称
        /// </summary>
        public string ExamRoomName { get; set; }

        /// <summary>
        /// 座位号
        /// </summary>
        public Nullable<int> SeatNumber { get; set; }

        /// <summary>
        /// 得分
        /// </summary>
        public decimal TotalScore { get; set; }

        /// <summary>
        /// 是否考试通过
        /// </summary>
        public bool IsPass { get; set; }

        /// <summary>
        /// 交卷时间
        /// </summary>
        public Nullable<DateTime> SubmitTime { get; set; }

        /// <summary>
        /// 试卷Id
        /// </summary>
        public Nullable<long> ExamId { get; set; }

        /// <summary>
        /// 作答记录Id
        /// </summary>
        public Nullable<long> GradeId { get; set; }

        /// <summary>
        /// 试卷命名
        /// </summary>
        public string ExamName { get; set; }

        /// <summary>
        /// 公司(案例)Id
        /// </summary>
        public long CaseId { get; set; }

        /// <summary>
        /// 卷面形式（可空，0核算会计 1 方法会计）
        /// </summary>
        public int? RollUpType { get; set; }

        /// <summary>
        /// 年份
        /// </summary>
        public string CaseYear { get; set; }

        /// <summary>
        /// 案例所属系统 1:竞赛 2:行业赛 3:云实训
        /// </summary>       
        public CaseSourceSystem SourceSystem { get; set; }

    }
}
