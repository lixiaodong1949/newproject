﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    /// <summary>
    /// 考试计划
    /// </summary>
    public class ExamPlanListPageResponseDto
    {
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证书
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 证书等级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }
        /// <summary>
        /// 考试时长
        /// </summary>
        public int TotalMinutes { get; set; }

        /// <summary>
        /// 考试开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 考试结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 考试状态
        /// </summary>
        public ExamStatus Status { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int ApplyCount { get; set; }

        /// <summary>
        /// 报名开始时间
        /// </summary>
        public DateTime ApplyBeginTime { get; set; }

        /// <summary>
        /// 报名结束时间
        /// </summary>
        public DateTime ApplyEndTime { get; set; }

    }
}
