﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto
{
    public class ExamStudentBaseInfoDto
    {
        /// <summary>
        /// 准考证号
        /// </summary>
        public string ExamCardNumber { get; set; }

        /// <summary>
        /// 考试密码
        /// </summary>
        public string ExamPassword { get; set; }
    }
}
