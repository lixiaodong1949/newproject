﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;

namespace OneAndX.Dto
{
    public class GetSignUpListDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 报考证书
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 报考证书等级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 考试时间
        /// </summary>
        public DateTime ExamTime { get; set; }

        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// 证件类型
        /// </summary>
        public string DocumentType { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IDNumber { get; set; }

        /// <summary>
        /// 考试计划Id
        /// </summary>
        public long ExamPlanId { get; set; }

        /// <summary>
        /// 准考证号
        /// </summary>
        public string ExamCardNumber { get; set; }
    }
}
