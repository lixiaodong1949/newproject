﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneAndX.Dto
{
    /// <summary>
    /// 考试计划的考点基本信息
    /// </summary>
    public class ExamSiteBaseInfoDto
    {
        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 区
        /// </summary>
        public string Area { get; set; }

    }
    /// <summary>
    /// 考试计划的考点基本信息
    /// </summary>
    public class ExamSiteCityDto
    {
        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 区
        /// </summary>
        public List<ExamSiteBaseInfoDto> Area { get; set; }


    }
    /// <summary>
    /// 考试计划的考点基本信息
    /// </summary>
    public class ExamSiteProvinceDto
    {
        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public List<ExamSiteCityDto> City { get; set; }

    }

}
