﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Entity;

namespace OneAndX.Dto
{
    /// <summary>
    /// 保存修改获取报名入参类(查询)输出类
    /// </summary>
    public class SignUpDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 0学生 1社会人士
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 报考证书
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 报考证书等级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 考试时间
        /// </summary>
        public DateTime ExamTime { get; set; }

        /// <summary>
        /// 名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; }

        /// <summary>
        /// 证件类型
        /// </summary>
        public string DocumentType { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IDNumber { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 区
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// 学历
        /// </summary>
        public string Education { get; set; }

        /// <summary>
        /// 所属/毕业学校
        /// </summary>
        public string School { get; set; }

        /// <summary>
        /// 专业
        /// </summary>
        public string Major { get; set; }

        /// <summary>
        /// 正面身份证
        /// </summary>
        public string FrontIDCard { get; set; }

        /// <summary>
        /// 背面身份证
        /// </summary>
        public string ReverseSideIDCard { get; set; }

        /// <summary>
        /// 照片
        /// </summary>
        public string Photo { get; set; }

        /// <summary>
        /// 考试计划Id
        /// </summary>
        public long ExamPlanId { get; set; }

        /// <summary>
        /// 行业名称
        /// </summary>
        public string IndustryName { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>

        public string CompanyName { get; set; }

        /// <summary>
        /// 职位名称
        /// </summary>
        public string PositionName { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string Phone { get; set; }

    }


    /// <summary>
    /// 获取报名(查询)入参类
    /// </summary>
    public class SignUpRequest //: PageRequest
    {
        /// <summary>
        /// 主键Id(查单条传Id，查所有不传Id)
        /// </summary>
        public long Id { get; set; }
    }
}
