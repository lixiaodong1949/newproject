﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework;
using Yssx.Framework.Authorizations;

namespace OneAndX.Dto
{
    /// <summary>
    /// 当前考试信息
    /// </summary>
    public class ExamCurrentBaseInfo
    {
        /// <summary>
        /// 报考证书
        /// </summary>
        public Certificate Certificate { get; set; }

        /// <summary>
        /// 报考证书等级
        /// </summary>
        public CertificateLevel CertificateLevel { get; set; }

        /// <summary>
        /// 考生名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 证件类型
        /// </summary>
        public string DocumentType { get; set; }

        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IDNumber { get; set; }

        /// <summary>
        /// 准考证号
        /// </summary>
        public string ExamCardNumber { get; set; }

        /// <summary>
        /// 考试开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 考试结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// 考试状态
        /// </summary>
        public ExamStatus Status { get; set; }

        /// <summary>
        /// 考试计划Id
        /// </summary>
        public long ExamPlanId { get; set; }

        /// <summary>
        /// 登录信息
        /// </summary>
        public UserTicket Ticket { get; set; }
    }
}
