﻿using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace OneAndX.Dto
{
    /// <summary>
    /// 保存修改教材入参类
    /// </summary>
    public class TextBookDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 教材名称
        /// </summary>
        public string TextbookName { get; set; }

        /// <summary>
        /// 所属模块
        /// 1业财税融合成本管控职业技能等级证书  
        /// 2业财税融合大数据投融资分析职业技能等级证书 
        /// 3业财税融合项目财务管理职业技能等级证书
        /// </summary>
        public int Module { get; set; }

        /// <summary>
        /// 等级
        /// 0全部 1初级 2中级 3高级
        /// </summary>
        public int Grade { get; set; }

        /// <summary>
        /// 状态 0开启 1关闭
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 类型 0教材 1案例 2课件 3培训拓展资源
        /// </summary>
        public int Type { get; set; }

        ///// <summary>
        ///// 教材_详情(章节信息)
        ///// </summary>
        //public List<TextBookDetails> TextBookDetails { get; set; }
    }

    /// <summary>
    /// 保存修改教材章节入参类
    /// </summary>
    public class TextBookChapterDto
    {
        /// <summary>
        /// 教材_详情(章节信息)
        /// </summary>
        public List<TextBookDetails> TextBookDetails { get; set; }
    }

    /// <summary>
    /// 保存修改教材预览入参类
    /// </summary>
    public class TextBookChapterResponse
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 父级Id 0根Id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string ChapterName { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 教材内容
        /// </summary>
        public string TextbookContent { get; set; }

        /// <summary>
        /// 上传文件
        /// </summary>
        public string UploadFiles { get; set; }

        /// <summary>
        /// 教材Id
        /// </summary>
        public long TextbookId { get; set; }
    }

    /// <summary>
    /// 获取教材(查询)输出类
    /// </summary>
    public class TextBookResponse
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 教材名称
        /// </summary>
        public string TextbookName { get; set; }

        /// <summary>
        /// 所属模块
        /// 1业财税融合成本管控职业技能等级证书  
        /// 2业财税融合大数据投融资分析职业技能等级证书 
        /// 3业财税融合项目财务管理职业技能等级证书
        /// </summary>
        public int Module { get; set; }

        /// <summary>
        /// 等级
        /// 1初级 2中级 3高级
        /// </summary>
        public int Grade { get; set; }

        /// <summary>
        /// 状态 0开启 1关闭
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 类型 0教材 1案例 2课件 3培训拓展资源
        /// </summary>
        public int Type { get; set; }
    }

    /// <summary>
    /// 获取教材预览(查询)输出类
    /// </summary>
    public class TextBookPreviewResponse
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 教材名称
        /// </summary>
        public string TextbookName { get; set; }

        /// <summary>
        /// 所属模块
        /// 1业财税融合成本管控职业技能等级证书  
        /// 2业财税融合大数据投融资分析职业技能等级证书 
        /// 3业财税融合项目财务管理职业技能等级证书
        /// </summary>
        public int Module { get; set; }

        /// <summary>
        /// 等级
        /// 0全部 1初级 2中级 3高级
        /// </summary>
        public int Grade { get; set; }

        /// <summary>
        /// 状态  0开启 1关闭
        /// </summary>
        public int State { get; set; }


        /// <summary>
        /// 类型 0教材 1案例 2课件 3培训拓展资源
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 教材_详情(章节信息)
        /// </summary>
        public List<TextBookDetailsResponse> TextBookDetails { get; set; }
    }

    /// <summary>
    /// 获取教材详情预览(查询)输出类
    /// </summary>
    public class TextBookDetailsResponse
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 父级Id 0根Id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string ChapterName { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 教材内容
        /// </summary>
        public string TextbookContent { get; set; }

        /// <summary>
        /// 上传文件
        /// </summary>
        public string UploadFiles { get; set; }

        /// <summary>
        /// 教材Id
        /// </summary>
        public long TextbookId { get; set; }

        /// <summary>
        /// 教材_详情(章节信息)
        /// </summary>
        public List<TextBookDetailsResponse1> TextBookDetails { get; set; }
    }

    /// <summary>
    /// 获取教材详情预览(查询)输出类
    /// </summary>
    public class TextBookDetailsResponse1
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 父级Id 0根Id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string ChapterName { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 教材内容
        /// </summary>
        public string TextbookContent { get; set; }

        /// <summary>
        /// 上传文件
        /// </summary>
        public string UploadFiles { get; set; }

        /// <summary>
        /// 教材Id
        /// </summary>
        public long TextbookId { get; set; }

        /// <summary>
        /// 教材_详情(章节信息)
        /// </summary>
        public List<TextBookDetailsResponse1> TextBookDetails{ get; set; }
    }

    /// <summary>
    /// 获取教材(查询)入参类
    /// </summary>
    public class TextBookRequest : PageRequest
    {
        /// <summary>
        /// 教材名称
        /// </summary>
        public string TextbookName { get; set; }

        /// <summary>
        /// 所属模块
        /// 1业财税融合成本管控职业技能等级证书  
        /// 2业财税融合大数据投融资分析职业技能等级证书 
        /// 3业财税融合项目财务管理职业技能等级证书
        /// </summary>
        public int Module { get; set; }

        /// <summary>
        /// 等级
        /// 0全部 1初级 2中级 3高级
        /// </summary>
        public int? Grade { get; set; }

        /// <summary>
        /// 状态 0开启 1关闭
        /// </summary>
        public int State { get; set; }
    }



    /// <summary>
    /// 获取教材(查询)入参类（前台）
    /// </summary>
    public class TextBooksRequest
    {
        /// <summary>
        /// 所属模块
        /// 1业财税融合成本管控职业技能等级证书  
        /// 2业财税融合大数据投融资分析职业技能等级证书 
        /// 3业财税融合项目财务管理职业技能等级证书
        /// </summary>
        public int Module { get; set; }

        /// <summary>
        /// 等级
        /// 1初级 2中级 3高级
        /// </summary>
        public int Grade { get; set; }

        /// <summary>
        /// 类型 0教材 1案例 2课件 3培训拓展资源
        /// </summary>
        public int Type { get; set; }
    }

    /// <summary>
    /// 获取教材预览(查询)输出类（前台）
    /// </summary>
    public class TextBooksResponse
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 教材名称
        /// </summary>
        public string TextbookName { get; set; }

        /// <summary>
        /// 所属模块
        /// 1业财税融合成本管控职业技能等级证书  
        /// 2业财税融合大数据投融资分析职业技能等级证书 
        /// 3业财税融合项目财务管理职业技能等级证书
        /// </summary>
        public int Module { get; set; }

        /// <summary>
        /// 等级
        /// 0全部 1初级 2中级 3高级
        /// </summary>
        public int Grade { get; set; }

        /// <summary>
        /// 状态  0开启 1关闭
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// 类型 0教材 1案例 2课件 3培训拓展资源
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// 教材_详情(章节信息)
        /// </summary>
        public List<TextBookDetailsResponse> TextBookDetails { get; set; }
    }

    /// <summary>
    /// 获取教材详情预览(查询)输出类（前台）
    /// </summary>
    public class TextBooksDetailsResponse
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 父级Id 0根Id
        /// </summary>
        public long ParentId { get; set; }

        /// <summary>
        /// 章节名称
        /// </summary>
        public string ChapterName { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 教材内容
        /// </summary>
        public string TextbookContent { get; set; }

        /// <summary>
        /// 上传文件
        /// </summary>
        public string UploadFiles { get; set; }

        /// <summary>
        /// 教材Id
        /// </summary>
        public long TextbookId { get; set; }

        /// <summary>
        /// 教材_详情(章节信息)
        /// </summary>
        public List<TextBookDetailsResponse> TextBookDetails { get; set; }
    }

    /// <summary>
    /// 根据下拉模块带出数据----返回参数
    /// </summary>
    public class TextBooksAllModuleDto
    {
        /// <summary>
        /// 所属模块
        /// 1业财税融合成本管控职业技能等级证书  
        /// 2业财税融合大数据投融资分析职业技能等级证书 
        /// 3业财税融合项目财务管理职业技能等级证书
        /// </summary>
        public int Module { get; set; }

        /// <summary>
        /// 等级
        /// 0全部 1初级 2中级 3高级
        /// </summary>
        public int Grade { get; set; }

        /// <summary>
        /// 类型 0教材 1案例 2课件 3培训拓展资源
        /// </summary>
        public int Type { get; set; }

    }
}
