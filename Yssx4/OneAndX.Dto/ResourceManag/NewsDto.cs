﻿using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace OneAndX.Dto
{
    /// <summary>
    /// 保存修改教材入参类
    /// </summary>
    public class NewsDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 新闻内容
        /// </summary>
        public string NewsContent { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 状态 1开启 2关闭
        /// </summary>
        public int State { get; set; }
    }

    /// <summary>
    /// 获取新闻(查询)输出类
    /// </summary>
    public class NewsResponse
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 新闻内容
        /// </summary>
        public string NewsContent { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 状态 1开启 2关闭
        /// </summary>
        public int State { get; set; }
    }

    /// <summary>
    /// 获取新闻(查询)入参类
    /// </summary>
    public class NewsRequest : PageRequest
    {
        /// <summary>
        /// 主键Id (编辑专用)
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 新闻名称
        /// </summary>
        public string Title { get; set; }
    }



    /// <summary>
    /// 获取新闻(查询)输出类(前台首页)
    /// </summary>
    public class NewsHomeResponse
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 新闻内容
        /// </summary>
        public string NewsContent { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdateTime { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime { get; set; }
    }
}
