﻿using OneAndX.Pocos;
using System;
using System.Collections.Generic;
using System.Text;
using Yssx.Framework.Entity;

namespace OneAndX.Dto
{
    /// <summary>
    /// 保存修改获取考点入参类
    /// </summary>
    public class ExaminationSiteDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 考点名称
        /// </summary>
        public string ExaminationSiteName { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 区
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// 具体地址
        /// </summary>
        public string SpecificAddress { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        public string Contacts { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string ContactUs { get; set; }

        /// <summary>
        /// 考点代号
        /// </summary>
        public string ExaminationSiteCode { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 考场详情
        /// </summary>
        public List<ExaminationSiteDetails> ExaminationSiteDetails { get; set; }

    }

    /// <summary>
    /// 保存修改获取考点(查询)输出类
    /// </summary>
    public class ExaminationSiteResponse
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 考点名称
        /// </summary>
        public string ExaminationSiteName { get; set; }

        /// <summary>
        /// 考试地址
        /// </summary>
        public string ExaminationAddress { get; set; }

        /// <summary>
        /// 考场数量
        /// </summary>
        public int ExaminationNumber { get; set; }
        
        /// <summary>
        /// 座位数
        /// </summary>
        public int SeatingCapacity { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        public string Contacts { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string ContactUs { get; set; }

        /// <summary>
        /// 考点代号
        /// </summary>
        public string ExaminationSiteCode { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }
    }

    /// <summary>
    /// 获取考点(查询)入参类
    /// </summary>
    public class ExaminationSiteRequest : PageRequest
    {
        /// <summary>
        /// 考点名称
        /// </summary>
        public string ExaminationSiteName { get; set; }
    }

    /// <summary>
    /// 获取考点(查询)输出类
    /// </summary>
    public class ExaminationSiteDetailsRequest
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 考点名称
        /// </summary>
        public string ExaminationSiteName { get; set; }

        /// <summary>
        /// 省
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// 市
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// 区
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// 具体地址
        /// </summary>
        public string SpecificAddress { get; set; }

        /// <summary>
        /// 联系人
        /// </summary>
        public string Contacts { get; set; }

        /// <summary>
        /// 联系方式
        /// </summary>
        public string ContactUs { get; set; }

        /// <summary>
        /// 考点代号
        /// </summary>
        public string ExaminationSiteCode { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public int Sort { get; set; }

        /// <summary>
        /// 考场详情
        /// </summary>
        public List<ExaminationSiteDetails> ExaminationSiteDetails { get; set; }
    }
}
