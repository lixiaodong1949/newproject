﻿using OneAndX.Pocos;
using Yssx.Framework.AutoMapper;

namespace OneAndX.Dto
{
    public class DtoAutoMapperProfile : AutoMapperProfile
    {
        public DtoAutoMapperProfile()
        {
            this.BidirectMapIgnoreAllNonExisting<ExamPlan, ExamPlanAddDto>();
            this.BidirectMapIgnoreAllNonExisting<ExamPlan, ExamPlanUpdateDto>();
            this.BidirectMapIgnoreAllNonExisting<SignUp, SignUpDto>();  
            this.BidirectMapIgnoreAllNonExisting<ExamUserGrade, ExamUserGradeAddDto>();
            this.BidirectMapIgnoreAllNonExisting<GradeRecord, GradeRecordRequestDto>();
            this.BidirectMapIgnoreAllNonExisting<OneXPreviouExam, SavePreviouExamListDto>();
            this.BidirectMapIgnoreAllNonExisting<OneXPreviouExamUser, GetPreviouExamUserListDto>();
            this.BidirectMapIgnoreAllNonExisting<OneXPreviouExamUser, GetPreviouExamCertificateListDto>();
        }
    }
}
