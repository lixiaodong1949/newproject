﻿using ECommon.Components;
using ENode.Messaging;
using MySql.Data.MySqlClient;
using Sx.Course.Domain.Events;
using Sx.Course.Entitys;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Tas.Common.IdGenerate;
using Yssx.Common;

namespace Sx.Course.Denormalizer.Dapper
{
    /// <summary>
    /// 
    /// </summary>
    [Component]
    public class CourseDenormalizer :
        IMessageHandler<CourseCreated>,
        IMessageHandler<CourseUpdated>,
        IMessageHandler<CourseStatusEdited>
    {
        public Task HandleAsync(CourseCreated message)
        {
            return Task.Factory.StartNew(() =>
            {
                var info = message.Info;
                var data2 = new YssxCourse
                {
                    Id = IdWorker.NextId(),
                    CourseName = info.CourseName,
                    CourseTitle = info.CourseTitle,
                    CourseTypeId = info.CourseTypeId,
                    KnowledgePointId = info.KnowledgePointId,
                    Education = info.Education,
                    Author = info.Author,
                    PublishingHouse = info.PublishingHouse,
                    PublishingDate = info.PublishingDate,
                    Images = info.Images,
                    TenantId = info.TenantId,
                    CreateBy = info.CreateBy,
                    IsDelete = 0,
                    CreateTime = DateTime.Now
                };
                DataMergeHelper.PushData("InsertCourse-data", data2, (arr) =>
                {
                    try
                    {
                        var datas = arr.Cast<YssxCourse>().Where(s => s != null && s.Id > 0).ToList();
                        if (datas != null && datas.Count > 0)
                        {
                            DbContext.FreeSql.GetRepository<YssxCourse>().Insert(datas);
                        }
                    }
                    catch (MySqlException ex)
                    {
                        if (ex.Number == 2627)  //主键冲突，忽略即可；出现这种情况，是因为同一个消息的重复处理
                        {
                            return;
                        }
                        throw new IOException("course Insert record failed.", ex);
                    }
                });
            });
        }

        public async Task HandleAsync(CourseUpdated message)
        {
            var info = message.Info;
            var data = new YssxCourse
            {
                Id = info.CourseId,
                CourseName = info.CourseName,
                CourseTitle = info.CourseTitle,
                CourseTypeId = info.CourseTypeId,
                KnowledgePointId = info.KnowledgePointId,
                Education = info.Education,
                Author = info.Author,
                PublishingHouse = info.PublishingHouse,
                PublishingDate = info.PublishingDate,
                Images = info.Images,
                UpdateTime = DateTime.Now,
            };
            await DbContext.FreeSql.GetRepository<YssxCourse>().UpdateAsync(data);
            //return Task.Factory.StartNew(() =>
            //    DbContext.FreeSql.GetRepository<YssxCourse>().UpdateAsync(data);
            //);
        }

        public Task HandleAsync(CourseStatusEdited message)
        {
            return Task.Factory.StartNew(() =>
            {
                DbContext.FreeSql.GetRepository<YssxCourse>().UpdateDiy.Set(x => x.IsDelete, 1).Set(x => x.UpdateTime, DateTime.Now).Where(x => x.Id == message.CourseId).ExecuteAffrows();
            });
        }
    }
}
