﻿using System;
using System.Data;
using System.Threading.Tasks;
using ECommon.IO;
using MySql.Data.MySqlClient;
using Yssx.Common;

namespace Sx.Course.Denormalizer.Dapper
{
    public abstract class AbstractDenormalizer
    {
        protected async Task TryInsertRecordAsync(Func<IDbConnection, Task<long>> action)
        {
            try
            {
                using (var connection = GetConnection())
                {
                    await action(connection);
                }
            }
            catch (MySqlException ex)
            {
                if (ex.Number == 2627)  //主键冲突，忽略即可；出现这种情况，是因为同一个消息的重复处理
                {
                    return;
                }
                throw new IOException("Insert record failed.", ex);
            }
        }
        protected async Task TryUpdateRecordAsync(Func<IDbConnection, Task<int>> action)
        {
            try
            {
                using (var connection = GetConnection())
                {
                    await action(connection);

                }
            }
            catch (MySqlException ex)
            {
                throw new IOException("Update record failed.", ex);
            }
        }
        protected IDbConnection GetConnection()
        {
            return new MySqlConnection(ConfigSettings.TrainingCloudConnectionString);
        }
    }
}
